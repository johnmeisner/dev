/**
 * @file       tps_scheduler_test.c
 *
 * @brief      This modules is used to execute a test of the TPS scheduler for
 *             the Forever Tracker project.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdio.h>
#include "intrinsics.h"
#include "unity.h"
#include "error.h"
#include "tps_scheduler.h"

// This gives access to all the TPS scheduler static elements for all
// tests. Therefore, this module must come before all other modules.
#include "tps_scheduler.c" 
#include "tps_scheduler_test_fatally_low.h"
#include "tps_scheduler_test_highest_first.h"
#include "tps_scheduler_test_full_schedule.h"

#define TEST_SUCCESS 0
#define TEST_FAILURE 1
#define NUM_CLIENT_IN_CLIENT_MANAGER

void setUp(void)
{
    InitializeScheduler();
}

void tearDown(void)
{

}

void test_OnlyFatallyLowReceivers(void)
{
    CotaError_t errorCode;

    errorCode = COTA_ERROR_NONE;

    errorCode = TestFatallyLowReceivers();

    TEST_ASSERT_MESSAGE((errorCode == COTA_ERROR_NONE),
                        "Error code while testing OnlyFatallyLowReceivers test");
}

void test_HighestBatteryChargeFirst(void)
{
    CotaError_t errorCode;

    errorCode = COTA_ERROR_NONE;

    errorCode = TestHighestBatteryChargeFirst();

    TEST_ASSERT_MESSAGE((errorCode == COTA_ERROR_NONE),
                        "Error code while testing HighestBatteryChargeFirst test");
}

void test_FullSchedule(void)
{
    CotaError_t errorCode;

    errorCode = COTA_ERROR_NONE;

    errorCode = TestFullSchedule();

    TEST_ASSERT_MESSAGE((errorCode == COTA_ERROR_NONE),
                        "Error code while testing FullSchedule test");
}

int main(void)
{   
    UNITY_BEGIN();
    RUN_TEST(test_OnlyFatallyLowReceivers);
    RUN_TEST(test_HighestBatteryChargeFirst);
    RUN_TEST(test_FullSchedule);
    return UNITY_END();
}
