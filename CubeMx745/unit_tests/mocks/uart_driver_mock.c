/****************************************************************************//**
* @file      uart_driver.c
*
* @brief     Code implementing prototypes to provide thread safe access to the UART
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "main.h"
#include "error.h"
#include "string.h"
#include "uart_driver.h"
#include "projdefs.h"


void InitCotaUart(void)
{

}


cotaError_t CotaUartTransmit(uint8_t *pData, uint16_t size, uint32_t timeout)
{
    return COTA_ERROR_NONE;
}


cotaError_t  CotaUartWaitForRx(uint8_t *pData, uint16_t size, uint32_t timeout)
{
    return COTA_ERROR_NONE;
}

