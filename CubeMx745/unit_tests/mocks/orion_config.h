/**
 * @file       orion_config.h
 *
 * @brief      Mock code for orion config module 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef ORION_CONFIG_H
#define ORION_CONFIG_H

#include <stdint.h>
#include "error.h"

/*
  For an un-understood reason, this module does not work! I left this
  code in case another developer wants a jump-off point for this
  functionality.
 */

typedef enum _ConfigParamId_t
{
    CFG_CHG_PRIORITY_THRESHOLD = 0,
    CFG_TPS_NUM_CLIENTS = 1,
    CFG_TPS_NUM_POWER_SLOTS_PER_CLIENT = 2,
} ConfigParamId_t;

CotaError_t CfgGetParam(ConfigParamId_t paramId, void * paramVal, uint8_t size);
CotaError_t CfgSaveParam(ConfigParamId_t paramId, void * paramVal, uint8_t size);

#endif /* ORION_CONFIG_H */
