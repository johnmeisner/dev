/**
 * @file       orion_config.c
 *
 * @brief      Mock layer for orion config.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#include "orion_config.h"
#include <string.h>
#include <stdint.h>
#include "error.h"

/*
  For an un-understood reason, this module does not work! I left this
  code in case another developer wants a jump-off point for this
  functionality.
 */

static uint32_t idToValueArray[] =
{
    [CFG_CHG_PRIORITY_THRESHOLD]         = 20,
    [CFG_TPS_NUM_CLIENTS]                = 8, 
    [CFG_TPS_NUM_POWER_SLOTS_PER_CLIENT] = 1,
};

CotaError_t CfgGetParam(ConfigParamId_t paramId, void * paramVal, uint8_t size)
{
    uint32_t val = idToValueArray[paramId];
    memcpy(paramVal, (void*) &val, size);
    return COTA_ERROR_NONE;
}

CotaError_t CfgSaveParam(ConfigParamId_t paramId, void * paramVal, uint8_t size)
{
    (void) paramId;
    (void) paramVal;
    (void) size;

    // This is just a mock, saving a param is not supported
    return COTA_ERROR_NONE;
}
