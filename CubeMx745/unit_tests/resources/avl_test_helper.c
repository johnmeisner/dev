 /****************************************************************************//**
* @file      client_manager_test_helper.c
*
* @brief     This implements the client manager unit tests.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "unity.h"
#include "error.h"
#include "client_avl_tree.h"
#include "client_interface.h"
#include "fake_initial_clients.h"
#include "client.h"

static void printShortIdList(clientList_t clientList);
static void printLongId(ExtdAddr_t longId);
static bool isClientLongIsInClientList(ExtdAddr_t *longId, clientList_t clientList, cotaError_t *errorCode);

/**
 * @brief A test where clients are added passed past the maximum value, then
 *          removed less than the minimum. 
 * @return COTA_ERROR_NONE on success, other on failure.
 */
cotaError_t AddRemoveClients(void)
{
    cotaError_t ret;
    uint16_t size;
    fakeClientList_t clients;
    client_t client;
    client_t oclient;
    clientTree_t tree;

    size = 1024;
    ret = COTA_ERROR_NONE;
    createFakeClients(&clients, size);
    CreateStaticTree(&tree, size);
    
    // Add past the maximum number of clients by 1
    for (unsigned int i = 0; i < clients.arraySize; i++)
    {
        client.longId = clients.clientIdArray[i];
        ret = (ret == COTA_ERROR_NONE) ?
            InsertNode(client, &tree.head, &tree) : ret;
       
        SearchTree(client.longId, tree.head, &oclient);
        
        TEST_ASSERT_MESSAGE((LongCmp(client.longId, oclient.longId) == 0),
                       "Could not find client after inserting it.");
    }

    // Remove past the minimum number (0) of clients by 1
    for (unsigned int i = 0; i < clients.arraySize; i++)
    {
        ret = (ret == COTA_ERROR_NONE) ?
            DeleteNode(clients.clientIdArray[i], &tree.head, NULL) : ret;
        
        TEST_ASSERT_MESSAGE(
            (SearchTree(clients.clientIdArray[i], tree.head, &oclient) == false),
            "Found a client that should have been deleted.");
    }

    return ret;
}


// Since these are debug functitons, ignore that they are not used in nominal operation. 
#pragma GCC diagnostic ignored "-Wunused-function"
/**
 * @brief This is a debugging function to print a longID of type #ExtdAddr_t w/o newline
 * @param longId - A longId of a client that is desired to be printed.
 */
static void printLongId(ExtdAddr_t longId)
{
    printf("0x");
    for ( unsigned int j = 0; j < sizeof(ExtdAddr_t); j++ )
    {
        printf("%02X", longId.bytes[j]);
    }
}
