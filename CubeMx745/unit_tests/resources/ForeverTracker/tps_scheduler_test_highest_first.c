/**
 * @file       tps_scheduler_test_highest_first.c
 *
 * @brief      This is a unit_test module for testing tps_scheduler
 *             module. Here, we test that we always charge the highest
 *             battery first in the TPS schedule
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "unity.h"
#include "error.h"
#include "fake_initial_clients.h"
#include "tps_scheduler.h"
#include "client.h"

#define NUM_OF_DISTINCT_BATTERY_LEVELS 101 // from 0 to 100 is 101 total values. All are charge %'s
#define MAX_TRIES 1000 // Scheduling takes time. This dictates how long we wait for a successful condition

static void exitProg(void);
static void createRecevierDatabase(void);
static void createFakeClientTreeForHighestChargeFirstTest(FakeClientList_t *clientList);
static void printLongId(ExtdAddr_t longId);
static bool testCaseEqualsSuccessCase(void);
static void updateReceiverBatteriesInDatabase(ReceiverList_t *recList);
static void printCases(void);

///< These functions are provided by client_manager.c in the projects application folder
PRIVATE CotaError_t InitClientManager(void);
PRIVATE bool GetLongId(ShrtAddr_t shortId, ExtdAddr_t *longId);
PRIVATE bool GetClient(ExtdAddr_t longId, Client_t *client);
PRIVATE bool SaveClient(Client_t client);
PRIVATE bool RemoveClient(ExtdAddr_t longId);
PRIVATE void Walk(void (*func)(Client_t* client));
PRIVATE bool FindClientState(ClientState_t state, Client_t *client);
PRIVATE void RemoveAllClients(void);

extern void prepareForScheduling(void);
extern void populateListOfReceiversNeedingCharge(void);
extern ReceiverList_t gScheduleCache;
extern uint16_t gCurrentNumberOfReceiversInList;
extern uint16_t gTpsFatalLowHistMin;

/**
   The initial states for a collection of receivers
*/
static uint8_t InitialBatteryHighestFirst[NUM_OF_DISTINCT_BATTERY_LEVELS] =
{
    0, 1, 2, 3, 4,
    5, 6, 7, 8, 9,
    10, 11, 12, 13, 14,
    15, 16, 17, 18, 19,
    20, 21, 22, 23, 24,
    25, 26, 27, 28, 29,
    30, 31, 32, 33, 34,
    35, 36, 37, 38, 39,
    40, 41, 42, 43, 44,
    45, 46, 47, 48, 49,
    50, 51, 52, 53, 54,
    55, 56, 57, 58, 59,
    60, 61, 62, 63, 64,
    65, 66, 67, 68, 69,
    70, 71, 72, 73, 74,
    75, 76, 77, 78, 79,
    80, 81, 82, 83, 84,
    85, 86, 87, 88, 89,
    90, 91, 92, 93, 94,
    95, 96, 97, 98, 99,
    100
};

/**
 * Successful cases are only for receiver that do not fit the criteria
 * of being "fatally low" (<= 18 battery level). All other receivers
 * must charge until they reach above the maximum battery level of 83.
 */
static uint8_t SuccessBatteryHighestFirst[NUM_OF_DISTINCT_BATTERY_LEVELS] =
{
    0, 1, 2, 3, 4,
    5, 6, 7, 8, 9,
    10, 11, 12, 13, 14,
    15, 16, 17, 18, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 83,
    83, 83, 83, 83, 84,
    85, 86, 87, 88, 89,
    90, 91, 92, 93, 94,
    95, 96, 97, 98, 99,
    100
};

CotaError_t TestHighestBatteryChargeFirst(void)
{
    uint32_t numOfTries = 0;

    createRecevierDatabase();

    while(numOfTries < MAX_TRIES)
    {
        prepareForScheduling();
        populateListOfReceiversNeedingCharge();

        updateReceiverBatteriesInDatabase(&gScheduleCache);
        
        if (testCaseEqualsSuccessCase())
            TEST_PASS();

        numOfTries++;
    }

    if (numOfTries >= MAX_TRIES)
    {
        TEST_FAIL_MESSAGE("Failed to charge receivers to a successful case");
    }

    return COTA_ERROR_NONE;
}


static void createRecevierDatabase(void)
{
    uint16_t size = NUM_OF_DISTINCT_BATTERY_LEVELS;
    FakeClientList_t clientList;
    CotaError_t ret;
    
    CreateFakeClients(&clientList, size);
    ret = InitClientManager();

    if (ret == COTA_ERROR_NONE)
    {
        createFakeClientTreeForHighestChargeFirstTest(&clientList);
    }
}


static void createFakeClientTreeForHighestChargeFirstTest(FakeClientList_t *clientList)
{
    Client_t client = {0};
    Client_t oclient = {0};
    bool success = true;

    if (clientList == NULL) exitProg();

    for (unsigned int i = 0; i < clientList->arraySize; i++)
    {
        // Short ID is how we keep track of the order of the receivers for this test.
        client.shortId = i;
        client.longId = clientList->clientIdArray[i];
        client.state = RCVR_STATE_READY; // This indicates it can be scheduled.
        client.qStatus.status.batteryCharge = InitialBatteryHighestFirst[i];

        // Normally, #populateListOfFatallyLowReceviers() would do
        // this, but since we are isolating
        // #populateListOfReceiversNeedingCharge() for this test, we
        // must do it manually.
        if (InitialBatteryHighestFirst[i] <= gTpsFatalLowHistMin)
        {
            client.fatalLow = true;
        }
        else
        {
            client.fatalLow = false;
        }

        success = (success == true) ?
            SaveClient(client) : success;

        TEST_ASSERT_MESSAGE(GetClient(client.longId, &oclient),
                            "There was an issue with adding Receivers to the receiver tree");
    }
}

static void updateReceiverBatteriesInDatabase(ReceiverList_t *recList)
{
    ExtdAddr_t longId;
    ShrtAddr_t shortId;
    Client_t   oclient;
    uint16_t numOfReceivers = gCurrentNumberOfReceiversInList;

    for (unsigned int i = 0; i < numOfReceivers; ++i)
    {
        shortId = recList->cache[i].shortId;
        
        // Update the database so next time we schedule with an updated battery
        GetLongId(shortId, &longId);
        GetClient(longId, &oclient);
        oclient.qStatus.status.batteryCharge++;
        SaveClient(oclient);

        // This updates the test case which we use to check for a passed test
        InitialBatteryHighestFirst[shortId] = oclient.qStatus.status.batteryCharge;
    }
}

static bool testCaseEqualsSuccessCase(void)
{
    for (unsigned int i = 0; i < NUM_OF_DISTINCT_BATTERY_LEVELS; i++)
    {
        if (InitialBatteryHighestFirst[i] != SuccessBatteryHighestFirst[i])
            return false;
    }
    return true;
}

static void exitProg(void)
{
    printf("Failing, exiting program\n");
    exit(1);
}

// Kept because its useful for debugging
#pragma GCC diagnostic ignored "-Wunused-function"
static void printLongId(ExtdAddr_t longId)
{
    printf("0x");
    for ( unsigned int j = 0; j < sizeof(ExtdAddr_t); j++ )
    {
        printf("%02X", longId.bytes[j]);
    }
}

#pragma GCC diagnostic ignored "-Wunused-function"
static void printCases(void)
{
    printf("Receiver List: \n");
    for (unsigned int i = 0; i < NUM_OF_DISTINCT_BATTERY_LEVELS; i+=5) {
        printf("%d, %d, %d, %d, %d\n",
               InitialBatteryHighestFirst[i],
               InitialBatteryHighestFirst[i + 1],
               InitialBatteryHighestFirst[i + 2],
               InitialBatteryHighestFirst[i + 3],
               InitialBatteryHighestFirst[i + 4]);
    }
}
