/**
 * @file       tps_scheduler_test_full_schedule.h
 *
 * @brief      Header file for the tps schedule test module for the full scheduler
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef TPS_SCHEDULER_TEST_FULL_SCHEDULE_H
#define TPS_SCHEDULER_TEST_FULL_SCHEDULE_H

CotaError_t TestFullSchedule(void);

#endif /* TPS_SCHEDULER_TEST_FULL_SCHEDULE_H */
