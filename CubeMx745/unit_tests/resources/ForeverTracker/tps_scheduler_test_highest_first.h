/**
 * @file       tps_scheduler_test_highest_first.h
 *
 * @brief      Header file for the tps_scheduler highest receiver first. 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef TPS_SCHEDULER_TEST_HIGHEST_FIRST_H
#define TPS_SCHEDULER_TEST_HIGHEST_FIRST_H

CotaError_t TestHighestBatteryChargeFirst(void);

#endif /* TPS_SCHEDULER_TEST_HIGHEST_FIRST_H */
