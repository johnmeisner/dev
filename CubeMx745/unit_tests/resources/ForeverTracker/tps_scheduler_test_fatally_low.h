/**
 * @file       tps_scheduler_test_fatally_low.h
 *
 * @brief      Header file for the fatally low receiver test
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef TPS_SCHEDULER_TEST_FATALLY_LOW_H
#define TPS_SCHEDULER_TEST_FATALLY_LOW_H

CotaError_t TestFatallyLowReceivers(void);

#endif /* TPS_SCHEDULER_TEST_FATALLY_LOW_H */
