 /****************************************************************************//**
* @file      fake_initial_clients.c
*
* @brief     This implements an initial client important for doing proper unit test.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "fake_initial_clients.h"
#include "client.h"
#include "CotaCommonTypes.h"

static void randomClientId(ExtdAddr_t *id);
static void randomClientBatteryLevel(uint8_t *battery);

/**
 * @brief creates an array of fake clients to do unit test with
 * 
 * @param fakeClientList  A pointer to a fake client list struct
 * @param size            The number of clients that has been allocated. 
 */
int CreateFakeClients(FakeClientList_t *fakeClientList, unsigned int size)
{
    unsigned int i;
    time_t t;
  
    if (fakeClientList == NULL)
        return 0;

    if ((fakeClientList->clientIdArray = malloc((sizeof(ExtdAddr_t) * size))) == NULL)
        return 0;
  

    if ((fakeClientList->batteryLevelArray = malloc((sizeof(uint8_t) * size))) == NULL)
        return 0;


    // Initialization for rand();
    srand((unsigned) time(&t));
  
    for (i = 0; i < size; i++)
    {
        randomClientId(&fakeClientList->clientIdArray[i]);
        randomClientBatteryLevel(&fakeClientList->batteryLevelArray[i]);
    }
  
    fakeClientList->arraySize = size;

    return 1;
}

/**
 * @brief frees a list of dynamically allocated fake clients
 * @param the pointer to the fakeClientList
 */
void FreeFakeClientList(FakeClientList_t *fakeClientList)
{
  
    free(fakeClientList->clientIdArray);
    free(fakeClientList->batteryLevelArray);
    fakeClientList->arraySize = 0;
}

/**
 * @brief This function produces a fake client longId at the longId passed to this function.
 * @param id An output parameter pointint to the Id that needs to be set with random data. 
 */
static void randomClientId(ExtdAddr_t *id)
{
    unsigned int i;
  
    for ( i = 0; i < sizeof(ExtdAddr_t); i++ )
    {
        // To make up the bytes in an ID
        id->bytes[i] = (rand() & 0xff);
    }
}

/**
 * @brief This function assigns a random battery level to the client 
 * @param Pointer to a batter variable
 */
static void randomClientBatteryLevel(uint8_t *battery)
{
    //Batteries cannot be larger than 100
    *battery = (rand() & 0x7f); 

    if (*battery > 100)
        *battery -= 100;
}
