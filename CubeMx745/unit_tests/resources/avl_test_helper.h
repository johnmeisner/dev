/****************************************************************************//**
* @file      client_manager_test_helper.c
*
* @brief     This implements the client manager unit tests.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#ifndef _CLIENT_MANAGER_TEST_HELPER_
#define _CLIENT_MANAGER_TEST_HELPER_

#include "error.h"

cotaError_t AddRemoveClients(void);

#endif /* #ifndef _CLIENT_MANAGER_TEST_HELPER_ */
