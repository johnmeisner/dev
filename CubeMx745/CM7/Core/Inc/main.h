/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MCU_LED_4_Pin GPIO_PIN_6
#define MCU_LED_4_GPIO_Port GPIOI
#define MCU_LED_3_Pin GPIO_PIN_5
#define MCU_LED_3_GPIO_Port GPIOI
#define FAN_FULL_Pin GPIO_PIN_4
#define FAN_FULL_GPIO_Port GPIOI
#define WRMHL_SPI6_MOSI_AMB3_Pin GPIO_PIN_5
#define WRMHL_SPI6_MOSI_AMB3_GPIO_Port GPIOB
#define AMB2_PU_Pin GPIO_PIN_10
#define AMB2_PU_GPIO_Port GPIOG
#define WRMHL_SPI1_MISO_AMB2_Pin GPIO_PIN_9
#define WRMHL_SPI1_MISO_AMB2_GPIO_Port GPIOG
#define DEBUG_2_Pin GPIO_PIN_5
#define DEBUG_2_GPIO_Port GPIOD
#define PROXY_SCK_Pin GPIO_PIN_10
#define PROXY_SCK_GPIO_Port GPIOC
#define PROXY_MASTER_READY_Pin GPIO_PIN_15
#define PROXY_MASTER_READY_GPIO_Port GPIOA
#define HOST_SPI_SCK_Pin GPIO_PIN_1
#define HOST_SPI_SCK_GPIO_Port GPIOI
#define MCU_LED_5_Pin GPIO_PIN_7
#define MCU_LED_5_GPIO_Port GPIOI
#define UART_TX_Pin GPIO_PIN_1
#define UART_TX_GPIO_Port GPIOE
#define QSPI_NCS_Pin GPIO_PIN_6
#define QSPI_NCS_GPIO_Port GPIOB
#define JTAG_JTRST_Pin GPIO_PIN_4
#define JTAG_JTRST_GPIO_Port GPIOB
#define IRQ_C2_Pin GPIO_PIN_15
#define IRQ_C2_GPIO_Port GPIOJ
#define IRQ_C2_EXTI_IRQn EXTI15_10_IRQn
#define PROXY_MOSI_Pin GPIO_PIN_6
#define PROXY_MOSI_GPIO_Port GPIOD
#define PG3P3_Pin GPIO_PIN_3
#define PG3P3_GPIO_Port GPIOD
#define PROXY_MISO_Pin GPIO_PIN_11
#define PROXY_MISO_GPIO_Port GPIOC
#define JTAG_SWCLK_Pin GPIO_PIN_14
#define JTAG_SWCLK_GPIO_Port GPIOA
#define HOST_SPI_MISO_Pin GPIO_PIN_2
#define HOST_SPI_MISO_GPIO_Port GPIOI
#define PA_PWR_2_Pin GPIO_PIN_15
#define PA_PWR_2_GPIO_Port GPIOH
#define FDCAN1_RX_Pin GPIO_PIN_14
#define FDCAN1_RX_GPIO_Port GPIOH
#define XTAL_32_OUT_Pin GPIO_PIN_15
#define XTAL_32_OUT_GPIO_Port GPIOC
#define XTAL_32_IN_Pin GPIO_PIN_14
#define XTAL_32_IN_GPIO_Port GPIOC
#define WRMHL_SPI4_SCLK_AMB0_Pin GPIO_PIN_2
#define WRMHL_SPI4_SCLK_AMB0_GPIO_Port GPIOE
#define UART_RX_Pin GPIO_PIN_0
#define UART_RX_GPIO_Port GPIOE
#define EEP_SDA_Pin GPIO_PIN_7
#define EEP_SDA_GPIO_Port GPIOB
#define WRMHL_SPI6_SCLK_AMB3_Pin GPIO_PIN_3
#define WRMHL_SPI6_SCLK_AMB3_GPIO_Port GPIOB
#define PROXY_BEACON1_INT_Pin GPIO_PIN_3
#define PROXY_BEACON1_INT_GPIO_Port GPIOK
#define PROXY_BEACON1_INT_EXTI_IRQn EXTI3_IRQn
#define PGCCBA_Pin GPIO_PIN_12
#define PGCCBA_GPIO_Port GPIOG
#define WRMHL_SPI1_MOSI_AMB2_Pin GPIO_PIN_7
#define WRMHL_SPI1_MOSI_AMB2_GPIO_Port GPIOD
#define HOST_SPI_MOSI_Pin GPIO_PIN_3
#define HOST_SPI_MOSI_GPIO_Port GPIOI
#define JTAG_SWDIO_Pin GPIO_PIN_13
#define JTAG_SWDIO_GPIO_Port GPIOA
#define WRMHL_SPI4_MISO_AMB0_Pin GPIO_PIN_5
#define WRMHL_SPI4_MISO_AMB0_GPIO_Port GPIOE
#define DEBUG_11_Pin GPIO_PIN_3
#define DEBUG_11_GPIO_Port GPIOE
#define FDCAN1_TX_Pin GPIO_PIN_9
#define FDCAN1_TX_GPIO_Port GPIOB
#define EEP_SCL_Pin GPIO_PIN_8
#define EEP_SCL_GPIO_Port GPIOB
#define AMB3_PU_Pin GPIO_PIN_15
#define AMB3_PU_GPIO_Port GPIOG
#define IRQ_C1_Pin GPIO_PIN_14
#define IRQ_C1_GPIO_Port GPIOJ
#define IRQ_C1_EXTI_IRQn EXTI15_10_IRQn
#define PROXY_GO_Pin GPIO_PIN_2
#define PROXY_GO_GPIO_Port GPIOD
#define UART_PWREN_Pin GPIO_PIN_0
#define UART_PWREN_GPIO_Port GPIOD
#define PG1P8_Pin GPIO_PIN_10
#define PG1P8_GPIO_Port GPIOA
#define I2C_RESET_Pin GPIO_PIN_9
#define I2C_RESET_GPIO_Port GPIOA
#define PA_PWR_1_Pin GPIO_PIN_13
#define PA_PWR_1_GPIO_Port GPIOH
#define MCU_LED_6_Pin GPIO_PIN_8
#define MCU_LED_6_GPIO_Port GPIOI
#define WRMHL_SPI4_MOSI_AMB0_Pin GPIO_PIN_6
#define WRMHL_SPI4_MOSI_AMB0_GPIO_Port GPIOE
#define DEBUG_9_Pin GPIO_PIN_1
#define DEBUG_9_GPIO_Port GPIOD
#define PU_ALL_Pin GPIO_PIN_8
#define PU_ALL_GPIO_Port GPIOC
#define QSPI_IO0_Pin GPIO_PIN_9
#define QSPI_IO0_GPIO_Port GPIOC
#define AMB_RX_Pin GPIO_PIN_8
#define AMB_RX_GPIO_Port GPIOA
#define DEBUG_TX_Pin GPIO_PIN_12
#define DEBUG_TX_GPIO_Port GPIOA
#define DEBUG_RX_Pin GPIO_PIN_11
#define DEBUG_RX_GPIO_Port GPIOA
#define DEBUG_12_Pin GPIO_PIN_11
#define DEBUG_12_GPIO_Port GPIOI
#define UVPEN_3_Pin GPIO_PIN_6
#define UVPEN_3_GPIO_Port GPIOC
#define HOST_MR_Pin GPIO_PIN_8
#define HOST_MR_GPIO_Port GPIOG
#define HOST_MR_EXTI_IRQn EXTI9_5_IRQn
#define AMB1_PU_Pin GPIO_PIN_7
#define AMB1_PU_GPIO_Port GPIOG
#define PD_DONE_0_Pin GPIO_PIN_2
#define PD_DONE_0_GPIO_Port GPIOF
#define CLKD_SCK_Pin GPIO_PIN_1
#define CLKD_SCK_GPIO_Port GPIOF
#define CLKD_SDA_Pin GPIO_PIN_0
#define CLKD_SDA_GPIO_Port GPIOF
#define POE_2_ON_Pin GPIO_PIN_5
#define POE_2_ON_GPIO_Port GPIOG
#define AMB0_PU_Pin GPIO_PIN_6
#define AMB0_PU_GPIO_Port GPIOG
#define DEBUG_1_Pin GPIO_PIN_12
#define DEBUG_1_GPIO_Port GPIOI
#define DEBUG_3_Pin GPIO_PIN_13
#define DEBUG_3_GPIO_Port GPIOI
#define PROXY_BL_ENABLE_Pin GPIO_PIN_14
#define PROXY_BL_ENABLE_GPIO_Port GPIOI
#define PD_DONE_1_Pin GPIO_PIN_3
#define PD_DONE_1_GPIO_Port GPIOF
#define POE_1_ON_Pin GPIO_PIN_4
#define POE_1_ON_GPIO_Port GPIOG
#define SPI_DISABLE_3_Pin GPIO_PIN_3
#define SPI_DISABLE_3_GPIO_Port GPIOG
#define SPI_DISABLE_2_Pin GPIO_PIN_2
#define SPI_DISABLE_2_GPIO_Port GPIOG
#define XTAL_25MHZ_OSC_OUT_Pin GPIO_PIN_1
#define XTAL_25MHZ_OSC_OUT_GPIO_Port GPIOH
#define XTAL_25MHZ_OSC_IN_Pin GPIO_PIN_0
#define XTAL_25MHZ_OSC_IN_GPIO_Port GPIOH
#define PD_DONE_3_Pin GPIO_PIN_5
#define PD_DONE_3_GPIO_Port GPIOF
#define PD_DONE_2_Pin GPIO_PIN_4
#define PD_DONE_2_GPIO_Port GPIOF
#define WRMHL_SPI5_SCLK_AMB1_Pin GPIO_PIN_0
#define WRMHL_SPI5_SCLK_AMB1_GPIO_Port GPIOK
#define QSPI_IO3_Pin GPIO_PIN_6
#define QSPI_IO3_GPIO_Port GPIOF
#define QSPI_IO2_Pin GPIO_PIN_7
#define QSPI_IO2_GPIO_Port GPIOF
#define WRMHL_SPI5_MISO_AMB1_Pin GPIO_PIN_8
#define WRMHL_SPI5_MISO_AMB1_GPIO_Port GPIOF
#define DEBUG_7_Pin GPIO_PIN_11
#define DEBUG_7_GPIO_Port GPIOJ
#define UVPEN_0_Pin GPIO_PIN_0
#define UVPEN_0_GPIO_Port GPIOC
#define QSPI_CLK_Pin GPIO_PIN_10
#define QSPI_CLK_GPIO_Port GPIOF
#define WRMHL_SPI5_MOSI_AMB1_Pin GPIO_PIN_9
#define WRMHL_SPI5_MOSI_AMB1_GPIO_Port GPIOF
#define DEBUG_10_Pin GPIO_PIN_10
#define DEBUG_10_GPIO_Port GPIOJ
#define UVPEN_1_Pin GPIO_PIN_2
#define UVPEN_1_GPIO_Port GPIOC
#define UVPEN_2_Pin GPIO_PIN_3
#define UVPEN_2_GPIO_Port GPIOC
#define HOST_MCU_IRQ_Pin GPIO_PIN_2
#define HOST_MCU_IRQ_GPIO_Port GPIOH
#define LAN_RESET_Pin GPIO_PIN_0
#define LAN_RESET_GPIO_Port GPIOA
#define CSB_ADD0_Pin GPIO_PIN_0
#define CSB_ADD0_GPIO_Port GPIOJ
#define SPI_SINGLE_Pin GPIO_PIN_6
#define SPI_SINGLE_GPIO_Port GPIOJ
#define HOST_IRQ_Pin GPIO_PIN_3
#define HOST_IRQ_GPIO_Port GPIOH
#define HOST_SR_Pin GPIO_PIN_5
#define HOST_SR_GPIO_Port GPIOH
#define PROXY_RESET_Pin GPIO_PIN_15
#define PROXY_RESET_GPIO_Port GPIOI
#define CSB_ADD1_Pin GPIO_PIN_1
#define CSB_ADD1_GPIO_Port GPIOJ
#define AMB_EN_2_Pin GPIO_PIN_13
#define AMB_EN_2_GPIO_Port GPIOF
#define AMB_EN_3_Pin GPIO_PIN_14
#define AMB_EN_3_GPIO_Port GPIOF
#define COTA_BOX_Pin GPIO_PIN_9
#define COTA_BOX_GPIO_Port GPIOE
#define COTA_BOX_EXTI_IRQn EXTI9_5_IRQn
#define PROXY_SLAVE_READY_Pin GPIO_PIN_10
#define PROXY_SLAVE_READY_GPIO_Port GPIOB
#define PROXY_SLAVE_READY_EXTI_IRQn EXTI15_10_IRQn
#define PG5P0_Pin GPIO_PIN_11
#define PG5P0_GPIO_Port GPIOB
#define FDEC_Pin GPIO_PIN_10
#define FDEC_GPIO_Port GPIOH
#define FINC_Pin GPIO_PIN_11
#define FINC_GPIO_Port GPIOH
#define UART_RTS_Pin GPIO_PIN_15
#define UART_RTS_GPIO_Port GPIOD
#define UART_CTS_Pin GPIO_PIN_14
#define UART_CTS_GPIO_Port GPIOD
#define WRMHL_SPI6_MISO_AMB3_Pin GPIO_PIN_6
#define WRMHL_SPI6_MISO_AMB3_GPIO_Port GPIOA
#define AMB_EN_1_Pin GPIO_PIN_12
#define AMB_EN_1_GPIO_Port GPIOF
#define OAMB_PWR_Pin GPIO_PIN_12
#define OAMB_PWR_GPIO_Port GPIOH
#define SYSCLK_RESET_Pin GPIO_PIN_11
#define SYSCLK_RESET_GPIO_Port GPIOD
#define QSPI_IO1_Pin GPIO_PIN_12
#define QSPI_IO1_GPIO_Port GPIOD
#define DEBUG_8_Pin GPIO_PIN_13
#define DEBUG_8_GPIO_Port GPIOD
#define WRMHL_SPI1_SCLK_AMB2_Pin GPIO_PIN_5
#define WRMHL_SPI1_SCLK_AMB2_GPIO_Port GPIOA
#define CSB_ADD2_Pin GPIO_PIN_2
#define CSB_ADD2_GPIO_Port GPIOJ
#define AMB_EN_0_Pin GPIO_PIN_11
#define AMB_EN_0_GPIO_Port GPIOF
#define SPI_DISABLE_0_Pin GPIO_PIN_0
#define SPI_DISABLE_0_GPIO_Port GPIOG
#define AMB_TX_Pin GPIO_PIN_8
#define AMB_TX_GPIO_Port GPIOE
#define PROXY_BEACON2_INT_Pin GPIO_PIN_13
#define PROXY_BEACON2_INT_GPIO_Port GPIOE
#define PROXY_BEACON2_INT_EXTI_IRQn EXTI15_10_IRQn
#define P_SDA_Pin GPIO_PIN_8
#define P_SDA_GPIO_Port GPIOH
#define DEBUG_6_Pin GPIO_PIN_10
#define DEBUG_6_GPIO_Port GPIOD
#define DEBUG_5_Pin GPIO_PIN_9
#define DEBUG_5_GPIO_Port GPIOD
#define PGCCBB_Pin GPIO_PIN_3
#define PGCCBB_GPIO_Port GPIOA
#define WRMHL_CSB_Pin GPIO_PIN_4
#define WRMHL_CSB_GPIO_Port GPIOA
#define CSB_ADD3_Pin GPIO_PIN_3
#define CSB_ADD3_GPIO_Port GPIOJ
#define SPI_DISABLE_1_Pin GPIO_PIN_1
#define SPI_DISABLE_1_GPIO_Port GPIOG
#define MCU_LED_7_Pin GPIO_PIN_14
#define MCU_LED_7_GPIO_Port GPIOE
#define P_SCL_Pin GPIO_PIN_7
#define P_SCL_GPIO_Port GPIOH
#define PGPAA_Pin GPIO_PIN_13
#define PGPAA_GPIO_Port GPIOB
#define PGPAB_Pin GPIO_PIN_14
#define PGPAB_GPIO_Port GPIOB
#define DEBUG_4_Pin GPIO_PIN_8
#define DEBUG_4_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
