/**
 * @file       cqt_scheduler.h
 *
 * @brief      Header file for query table scheduler. 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CQT_SCHEDULER_H_
#define _CQT_SCHEDULER_H_

#include <stdbool.h>
#include "proxyHostMsgInf.h"

#define MAX_NUM_CLIENTS_TO_QUERY 8 ///< Maximum number of clients that can be queried at the same time.

typedef PACKED struct _CqtSchedule_t
{
  uint8_t               prxCmd;                  
  PrxHostCqtHdr_t       header;
  PrxHostCqTableEntry_t clientQueryEntry[MAX_NUM_CLIENTS_TO_QUERY];
} CqtSchedule_t;

CqtSchedule_t* GetCqtSchedule(void);
void UpdateMaxQueryCount(uint8_t queryCount);
void CalculateQuerySchedule(void);
void RemoveQueryClient(uint16_t shortId);
uint8_t GetRemainingQueries(void);
bool ExpectingQuery(uint16_t shortId);
bool DoRxStillNeedQuery(void);
void ClearClientQueryFlag(void);
#endif // _CQT_SCHEDULER_H_
