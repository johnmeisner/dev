/**
 * @file       cqt_scheduler.c
 *
 * @brief      Schedules a list clients for queries.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "error.h"
#include "uart_driver.h"
#include "orion_config.h"
#include "cli_task.h"
#include "cqt_scheduler.h"
#include "client.h"
#include "client_manager.h"
#include "orion_utilities.h"
#include "debug_on.h"

#define MAXIMUM_CLIENT_PRIORITY  100 ///< The maximum allowed client priority
#define FIRST_SPOT               0   ///< The first spot in an array

// These functions are provided by client_manager.c
PRIVATE bool GetLongId(ShrtAddr_t shortId, ExtdAddr_t *longId);
PRIVATE bool GetClient(ExtdAddr_t longId, Client_t *client);
PRIVATE bool SaveClient(Client_t client);
PRIVATE bool RemoveClient(ExtdAddr_t longId);
PRIVATE void Walk(void (*func)(Client_t* client));
PRIVATE bool FindClientState(ClientState_t state, Client_t *client);

static void initializeCqt(void);
static void populateQueryTable(void);


static void checkQueryFlag(Client_t *client);
static void clearQueryFlag(Client_t *client);
static void fillQueryTableWithShortIds(Client_t *client);

static CqtSchedule_t gCqtSchedule;                           ///< The structure holding the query schedule
static uint16_t      gQueryMaxQueryInTable;    ///< The maximum number of queries to send at one time

/**
 *   Indicates there are still clients needing a query during the query
 *   period.
 */
static bool gFoundRxNeedingQuery;

/**
 * @brief Retrieve the query schedule
 *
 * @return A pointer to a structure of type #CqtSchedule_t
 */
CqtSchedule_t* GetCqtSchedule(void)
{
    return &gCqtSchedule;
}

/**
 * @brief Updates the maximum Rx queries in a CQT schedule at a time.
 */
void UpdateMaxQueryCount(uint8_t queryCount)
{
    if (queryCount <= MAX_NUM_CLIENTS_TO_QUERY)
    {
        gQueryMaxQueryInTable = queryCount;
    }
    else
    {
        gQueryMaxQueryInTable = MAX_NUM_CLIENTS_TO_QUERY;
        LogWarning("Number queries may not exceed %d at one time\r\n", MAX_NUM_CLIENTS_TO_QUERY);
    }
}

/**
 * @brief Removes a client from query table so we can track what queries came back.
 *
 * @param shortId The short id to remove from the query table.
 */
void RemoveQueryClient(uint16_t shortId)
{
    uint8_t ii = 0;
    bool found = false;
    if (gCqtSchedule.header.totalNumClients > 0)
    {
        while (ii < gCqtSchedule.header.totalNumClients)
        {
            if (gCqtSchedule.clientQueryEntry[ii].shortAddr == shortId)
            {
                found = true;
                break;
            }
            ii++;
        }

        while (ii < gCqtSchedule.header.totalNumClients-1)
        {
            gCqtSchedule.clientQueryEntry[ii] = gCqtSchedule.clientQueryEntry[ii+1];
            ii++;
        }

        if (found)
        {
            gCqtSchedule.header.totalNumClients--;
        }

    }
}

/**
 * @brief This function indicates whether a query response is expected for a particular client
 *
 * @param shortId The short id of the client
 *
 * @return true if a query is expected; false if not.
 */
bool ExpectingQuery(uint16_t shortId)
{
    uint8_t ii = 0;
    bool found = false;
    if (gCqtSchedule.header.totalNumClients > 0)
    {
        while (ii < gCqtSchedule.header.totalNumClients)
        {
            if (gCqtSchedule.clientQueryEntry[ii].shortAddr == shortId)
            {
                found = true;
                break;
            }
            ii++;
        }
    }
    return found;
}

/**
 * @brief Determines how many queries are left to be received.
 *
 * @return The number of queries in the queries table
 */
uint8_t GetRemainingQueries(void)
{
    return gCqtSchedule.header.totalNumClients;
}

/**
 * @brief Calculates the query schedule.

 * @return return COTA_ERROR_NONE if successful, otherwise it returns an error code
 */
void CalculateQuerySchedule(void)
{
    initializeCqt();
    populateQueryTable();
}

/**
 * @brief Initializes the CQT schedule.
 */
static void initializeCqt(void)
{
    memset(&gCqtSchedule, 0, sizeof(gCqtSchedule));
    gCqtSchedule.header.totalNumClients = 0;
    gCqtSchedule.prxCmd = HOST_PRX_CQ_TABLE;
}

/**
 * @brief Populates the query table with clients that can be queried.
 */
static void populateQueryTable(void)
{
    Walk(fillQueryTableWithShortIds);
}

/**
 * @brief Fills the query table with shortIds needed for sending a
 *        query table.
 * @note  This function is intended to be used with #Walk()
 */
static void fillQueryTableWithShortIds(Client_t *client)
{
    PrxHostCqTableEntry_t *entry;
    uint8_t cqtIdx = gCqtSchedule.header.totalNumClients;

    if (client != NULL && (client->queryNeededFlag == true) &&
        (IsRcvrAbleToQuery(client)) && (cqtIdx < gQueryMaxQueryInTable))
    {
        client->queryNeededFlag = false;
        entry = &gCqtSchedule.clientQueryEntry[cqtIdx];
        entry->shortAddr = client->shortId;
        entry->queryType = client->qStatus.header.queryType;
        gCqtSchedule.header.totalNumClients++;
    }
}

/**
 * @brief Checks if there is a single client that needs a query left
 *        in the system.
 */
bool DoRxStillNeedQuery(void)
{
    gFoundRxNeedingQuery = false;
    Walk(checkQueryFlag);
    return gFoundRxNeedingQuery;
}

/**
 * @brief Checks if there is a single client that needs a query left
 *        in the system.
 * @note  This function is intended to be used with #Walk()
 */
static void checkQueryFlag(Client_t *client)
{
    if (client != NULL)
    {
        if ((client->queryNeededFlag == true) && IsRcvrAbleToQuery(client))
        {
            gFoundRxNeedingQuery = true;
        }
    }
}

/**
 * @brief Clears the query flag for each client.
 */
void ClearClientQueryFlag(void)
{
    Walk(clearQueryFlag);
}


/**
 * @brief Clears the query flag. Indicates that all clients that have
 *        been queried are ready for query again.
 * @param client The client that this function will apply to.
 * @note  This function is intended to be used with #Walk()
 */
static void clearQueryFlag(Client_t *client)
{
    if (client != NULL)
    {
        client->queryNeededFlag = true;
    }
}
