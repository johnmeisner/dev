/**
 * @file       client_avl_tree.c
 *
 * @brief      A module for organizing clients in an AVL binary search tree
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <string.h> 
#include "client_avl_tree.h"
#include "error.h"
#include "orion_config.h"

/*
 * Memory management functions
 */
static inline bool isNullNode(ClientNode_t *node);
static CotaError_t createNode(ClientNode_t **node, ClientTree_t *tree);
static CotaError_t destroyNode(ClientNode_t **nodeToDestroy);
static CotaError_t getNextValidIndex(ClientTree_t *tree);
static void copyClosestNodeToCurrentNode(ClientNode_t **node);

/*
 * Tree functions - Used for balancing the AVL tree. 
 */
static int16_t       treeMaxHeight         (ClientNode_t * );
static int16_t       treeHeightDifference  (ClientNode_t * );
static ClientNode_t* treeRotateRR          (ClientNode_t * );
static ClientNode_t* treeRotateLL          (ClientNode_t * );
static ClientNode_t* treeRotateLR          (ClientNode_t * );
static ClientNode_t* treeRotateRL          (ClientNode_t * );
static ClientNode_t* treeBalance           (ClientNode_t **);

/**
 * @brief Destroy tree will zero all client data in the nodeArray. 
 */
void DestroyTree(ClientTree_t *tree)
{   
    memset(((void *)tree->nodeArray), 0, (sizeof(ClientNode_t) * tree->size));
    tree->head = NULL;
}

/**
 * @brief This function inserts a node into a client tree. If the tree is empty,
 *        that node will be inserted at the tree's head.
 *
 *        This is an implementation of an AVL tree, An AVL tree will re-balance itself
 *          when the node is inserted. The simple equation is to traverse the tree until 
 *          a place to insert has been found, insert the node, then re-balance the tree.
 *          As we move back up the stack, we re-balance along the way.
 *
 * @note If this function is used to insert a node that has the same
 *          ID as a node already inserted into the AVL tree, that
 *          node's data (a client) will be overwritten. This is good
 *          for the modules that will use InsertNode(); 
 *
 * @param key The client data that is being inserted into the tree
 * @param node The node that this function is recursing to search the tree.
 * @param tree The tree that holds where the memory should be allocated.
 * @return return COTA_ERROR_NONE on success, and COTA_ERROR_RCVR_ALREADY_REGISTERED on failure
 */
CotaError_t InsertNode(Client_t key, ClientNode_t **node, ClientTree_t *tree)
{
    CotaError_t   retVal;
    
    if ( *node == NULL) // This indicates a root node
    {
        // This step finds the next chunk of allocated memory. 
        retVal = createNode(node, tree);
        if (retVal != COTA_ERROR_NONE)
            return retVal;
    
        (*node)->client = key;
        (*node)->left   = NULL;
        (*node)->right  = NULL;
    }
    else if (LongCmp(key.longId, (*node)->client.longId) < 0) // key < node.client
    {
        retVal = InsertNode(key, &(*node)->left, tree);
        (*node) = treeBalance(node);
    }
    else if (LongCmp(key.longId, (*node)->client.longId) > 0) // key > node.client
    {
        retVal = InsertNode(key, &(*node)->right, tree);
        (*node) = treeBalance(node);
    }else{ // key.longId == *node->client.longId

        // This insert node will save over existing data if it finds a
        // client that is already registered. This is not a normal
        // property of BSTs, but is useful for coded behavior at the
        // higher layers of abstraction. We will still send
        // COTA_ERROR_RCVR_ALREADY_REGISTERED up the stack, to make
        // it known this action has occurred.
        retVal = COTA_ERROR_RCVR_ALREADY_REGISTERED;
        memcpy((void*)&(*node)->client, &key, sizeof(Client_t));
    }
  
    return retVal;  
}


/**
 * @brief The recursive function that searches a tree for the target client to remove.
 * @param key       The long id of the client to remove
 * @param node      The node that is recursively acted on to search the tree.
 * @param oclient   If not NULL, The client data will be copied to this pointer before being destroyed. 
 *
 * @return Returns the client node that needs to be assigned up that chain.
 */
CotaError_t DeleteNode(ExtdAddr_t key, ClientNode_t **node, Client_t *oclient)
{
    ClientNode_t *tempNode; 
  
    if ( *node != NULL )
    {
        if (LongCmp(key, (*node)->client.longId) < 0)
        {
            DeleteNode(key, &(*node)->left, oclient);
            (*node) = treeBalance(node);
        }
        else if (LongCmp(key, (*node)->client.longId) > 0)
        {
            DeleteNode(key, &(*node)->right, oclient);
            (*node) = treeBalance(node);
        }
        else // The node to be removed is found in the tree 
        {
            if (((*node)->left == NULL) && ((*node)->right == NULL ))
            {
                if (oclient != NULL)
                    memcpy((void *) oclient,(void *) &(*node)->client, sizeof(Client_t));
                
                destroyNode(node);
                
            } 
            else if ((*node)->left == NULL)
            {
                if (oclient != NULL)
                    memcpy((void *) oclient,(void *) &(*node)->client, sizeof(Client_t));
                
                tempNode = (*node);
                (*node) = (*node)->right;
                destroyNode(&tempNode);
                
                
            }
            else if ((*node)->right == NULL)
            {
                if (oclient != NULL)
                    memcpy((void *) oclient, (void *) &(*node)->client, sizeof(Client_t));
                
                tempNode = (*node);
                (*node) = (*node)->left;
                destroyNode(&tempNode);
                
            }
            else
            {
                copyClosestNodeToCurrentNode(node);
        
                /*
                 * node now it's equivalent to left-most node of its right branch.
                 *   This is the node whose data is closest to the node that we are deleting and 
                 *   the tree remains correct after removal. After we re-balance up the stack,
                 *   the tree will also remain balanced.
                 */

                DeleteNode((*node)->client.longId, &(*node)->right, oclient);                
                (*node) = treeBalance(node);
                /*
                 * After the step above, the tree has two copies of the same node. We 
                 *   must remove the node we copied from. 
                 */
            }
        }
    }
    else
    {
        return COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
    }
    
    return COTA_ERROR_NONE;
}

/**
 * @brief This is a helper function for DeleteNode. This fcn gets the closest node to the param node
 *         from within its right branch.
 * @param node The right node of the curent node. The node to the furthest left of this point contains 
 *          the closest neighbor.
 */
static void copyClosestNodeToCurrentNode(ClientNode_t **node)
{
    ClientNode_t *temp;
  
    temp = (*node)->right;
 
    /*
     * We assume that that node is not NULL because it is called from a 
     *   function that already checks that.
     */
    while (true)
    {
        if (temp->left != NULL)
        {
            temp = temp->left;
        }
        else
        {
            break;
        }
    }

    // Pointer arithmetic is avoided by copying node clientData.
    memcpy((void *)(&(*node)->client), (void *)(&temp->client), sizeof(Client_t));
}

// Because the tree is organized by longId, this function must walk through the tree
//  to check the client's shortId. This inefficiency is the reason searching by longId
//  is preferred. 
bool SearchTreeShortId(ShrtAddr_t key, ClientNode_t *node, Client_t *oclient)
{
    bool ret = false;
    
    if (node != NULL)
    {
        if (key == node->client.shortId)
        {
            if (oclient != NULL)
            {
                memcpy((void *) oclient, (void *) &(node->client), sizeof(Client_t));
            }
            
            ret = true;
        }
        
        ret = SearchTreeShortId(key, node->left, oclient);

        // If going down the left node returned true, don't
        //  overwrite it the output by searching down the
        //  right path
        if (!ret)
        {
            ret = SearchTreeShortId(key, node->right, oclient);
        }
    }

    return ret;
}

/**
 * @brief This function searches the tree by shortId and copys the found client
 * @param key The id of the client that the user wishes to locate.
 * @param node  The node that the function is recursively searched on. Start with a head node
 * @param oclient When a client is found, its data will be memcpy'd to this address unless null
 * @return COTA_ERROR_NONE on success, COTA_ERROR_COULD_NOT_BE_FOUND on failure to find the desired node
 */
bool SearchTree(ExtdAddr_t key, ClientNode_t *node, Client_t *oclient)
{
    bool ret = false;
    
    if (node != NULL)
    {    
        if (LongCmp(key, node->client.longId) == 0)
        {
            if (oclient != NULL)
            {
                memcpy(((void *)oclient), (&node->client), sizeof(Client_t));
            }

            ret = true;
        }
        else if (LongCmp(key, node->client.longId) < 0) // key < client.longId
        {
            ret = SearchTree( key, node->left, oclient);
        }
        else if (LongCmp(key, node->client.longId) > 0) // key > client.longId
        {
            ret = SearchTree( key, node->right, oclient);
        }
    }
    
    return ret;
}


/**
 * @brief Updates all data in a client object. Handle with care.
 * @param key The shortId that distinquishes a client
 * @param node The node that this function is recursively searched on. Start with a head node.
 * @param client pointer to the client that has contains all reference data to update with. If
 *         NULL is passed in, no action is taken. 
 * @return  COTA_ERROR_NONE on success, COTA_ERROR_RCVR_COULD_NOT_BE_FOUND on failure 
 */
CotaError_t UpdateClient(ExtdAddr_t key, ClientNode_t *node, Client_t *client)
{
    CotaError_t ret; 

    ret = COTA_ERROR_NONE;
    
    if ( node != NULL)
    {    
        if (LongCmp(key, node->client.longId) == 0)
        {
            // We all copy all data from the passed in client. This puts the power
            //  in the programmers hands to break everything. 
            if (client != NULL)
                memcpy((void*) &(node->client), (void *) &client, sizeof(Client_t));
        }
        else if (LongCmp(key, node->client.longId) < 0)
        {
            ret = UpdateClient( key, node->left, client);
        }
        else if (LongCmp(key, node->client.longId) > 0)
        {
            ret = UpdateClient( key, node->right, client);
        }
    }
    else
    {
        ret = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
    }
    
    return ret;
}

/*
  This function is designed to be a general-use "do the thing to all the clients"
    function in this API. This function traverses the tree and does 'func' onto
    each client it finds.
 */
void WalkTree(ClientNode_t *node, void (*func)(Client_t* client))
{
    if (node != NULL)
    {
        func(&node->client);

        WalkTree(node->left, func);
        WalkTree(node->right, func);
    }
}

/**
 * @brief This function search through the tree to find any client at
 *          a specific state. This is function is the backbone for the
 *          get 'x' client functions that are at higher levels of
 *          abstraction.
 *
 * @note This function was produced because I was unable to create a
 *          proper function for WalkTree in the time I had to complete
 *          this work. Ideally, this type of functionality can be
 *          completed with WalkTree, but this will do for now.
 *
 * @param key - The state you are searching for.
 * @param node - The node that we are recurring on
 * @param oclient - When a client is found, its data will be memcpy'd to this address unless null
 * @return True if client with proper state was found, False if otherwise. 
 */
bool WalkToFindState(ClientState_t key, ClientNode_t *node, Client_t *oclient) 
{
    bool ret = false;
    
    if ( node != NULL )
    {
        if (key == node->client.state)
        {
            if (oclient != NULL)
            {
                memcpy((void *) oclient, (void *) &(node->client), sizeof(Client_t));
            }
            
            ret = true;
        }

        // If we found a client with the proper state, don't overwrite
        //  the output by searching down the another path
        if (!ret)
        {
            ret = WalkToFindState(key, node->left, oclient);
        }
        
        if (!ret)
        {
            ret = WalkToFindState(key, node->right, oclient);
        }
    }
    return ret;
}

/**
 * @brief Sets #ClientTree_t.lastClient to the next avaliable client location
 * @return COTA_ERROR_NONE on success, COTA_ERROR_RCVR_STORAGE_FULL on failure. 
 */
static CotaError_t getNextValidIndex(ClientTree_t *tree)
{
    uint16_t init;
  
    init = tree->lastClient;
    
    while (!isNullNode(&tree->nodeArray[tree->lastClient]))
    {
        if (tree->lastClient < tree->size)
        {
            tree->lastClient++;

            // If we reach here, we've looped over all index less
            //  than MAX_NUM_CLIENTS and a open space for a new
            //  client/receiver was not found.
            if (tree->lastClient == init)
            {
                return COTA_ERROR_RCVR_STORAGE_FULL;
            }
        }
        else
        {
            tree->lastClient = 0;
        }
    }
  
    return COTA_ERROR_NONE;
}

/**
 * @brief Determines if a node is a termination node 
 * @param node A node that we want to know if it is a termination node
 * @return true if it's a null node (all bytes are 0), false if non-zero.
 */
static inline bool isNullNode(ClientNode_t *node)
{
    uint8_t *temp = (uint8_t *) node;
    for (uint8_t i = 0; i < sizeof(ClientNode_t); i++)
    {
        if ( temp[i] != 0 )
            return false;
    }

    return true;
}

/**
 * @brief Searches through a node array locates the next non-term node available,
 *         then sets *node to point at that spot in memory. 
 * @note We are working with statically allocated memory and doing our own memory management. 
 *         For this reason, the memory management will be abstracted by functions that look
 *         like they are dynamically allocating memory. (like this one)
 * @param node A pointer of a node that needs to be allocated
 * @param tree The tree that holds where the data is allocated
 * @return COTA_ERROR_NONE on success, and COTA_ERROR_RCVR_STORAGE_FULL if memory could not be allocated
 */
static CotaError_t createNode(ClientNode_t **node, ClientTree_t *tree)
{
    CotaError_t ret;
  
    ret = getNextValidIndex(tree);
    if (ret != COTA_ERROR_NONE)
        return ret;

    *node = &(tree->nodeArray[tree->lastClient]);
  
    return COTA_ERROR_NONE;
}

/**
 * @brief Makes the inputted node a termination node and sets the node to null
 * @param nodeToDestroy The node that needs to be destroyed
 * @return COTA_ERROR_NONE
 */
static CotaError_t destroyNode(ClientNode_t **nodeToDestroy)
{
    CotaError_t ret;

    ret = COTA_ERROR_NONE;
    
    if ( *nodeToDestroy != NULL)
    {
        // Set all data in the ClientNode_t to 0 to mark it as
        //  unset data.
        memset((void *)*nodeToDestroy, 0, sizeof(ClientNode_t));
        (*nodeToDestroy) = NULL;
        ret = COTA_ERROR_NONE;
    }
    else
    {
        ret = COTA_ERROR_FAILED_TO_REMOVE_NODE;
    }
    return ret;
}

/**
 * @brief Calculate the maximum height of a tree starting at a node.
 * @param node The root node the function recursively works on. 
 * @return the maximum height between a nodes left and right tree
 */
static int16_t treeMaxHeight(ClientNode_t *node)
{
    int16_t maxHeight;
    int16_t leftHeight;
    int16_t rightHeight;
  
    maxHeight = 0;
  
    if (node != NULL)
    {
        leftHeight  = treeMaxHeight(node->left);
        rightHeight = treeMaxHeight(node->right);
      
        maxHeight = MAX(leftHeight, rightHeight) + 1;
    }
    
    return maxHeight;
}

/**
 * @brief Returns the difference between the maximum height of the left and right branches
 * @param node The node that the function recurses on 
 * @return the difference between the left and right branches of a node
 */
static int16_t treeHeightDifference(ClientNode_t *node)
{
    return (treeMaxHeight(node->left) - treeMaxHeight(node->right));
}

/**
 * @brief Performs a RR rotation on a given clientNode
 * @param node The node to perform the rotation on.
 * @return The clientNode that becomes the new node from where this function was called.
 */
static ClientNode_t* treeRotateRR(ClientNode_t *node)
{
    ClientNode_t *temp;
    temp = node->right;
    node->right = temp->left;
    temp->left  = node;
  
    return temp;
}

/**
 * @brief Performs a LL rotation on a given clientNode
 * @param node The node to perform the rotation on.
 * @return The clientNode that becomes the new node from where this function was called.
 */
static ClientNode_t* treeRotateLL(ClientNode_t *node)
{
    ClientNode_t *temp;
    temp = node->left;
    node->left = temp->right;
    temp->right = node;
  
    return temp;
}

/**
 * @brief Performs a LR rotation on a given clientNode
 * @param node The node that the function recurses on 
 * @return The clientNode that becomes the new node from where this function was called.
 */
static ClientNode_t* treeRotateLR(ClientNode_t *node)
{
    ClientNode_t *temp;
    temp = node->left;
    node->left = treeRotateRR(temp);
  
    return treeRotateLL(node);
}

/**
 * @brief Performs a RL rotation on a given clientNode
 * @param node The node to perform the rotation on.
 * @return The clientNode that becomes the new node from where this function was called.
 */
static ClientNode_t* treeRotateRL(ClientNode_t *node)
{
    ClientNode_t *temp;
    temp = node->right;
    node->right = treeRotateLL(temp);
  
    return treeRotateRR(node);
}

/**
 * @brief This function balances a tree (AVL tree)
 * @param node The node that the function recurses on 
 * @return The clientNode that becomes the new node from where this function was called.
 */
static ClientNode_t* treeBalance(ClientNode_t **node)
{
    int16_t balanceFactor;
  
    balanceFactor = treeHeightDifference(*node);
    if (balanceFactor > 1)
    {
        if ((treeHeightDifference((*node)->left) > 0 ))
        {
            (*node) = treeRotateLL(*node);
        }
        else
        {
            (*node) = treeRotateLR(*node);
        }
    }
    else if ( balanceFactor < -1 )
    {
        if ((treeHeightDifference((*node)->right) > 0 ))
        {
            (*node) = treeRotateRL(*node);
        }
        else
        {
            (*node) = treeRotateRR(*node);
        }
    }
  
    return (*node);
}
