/** 
 *
 * @file       print_format.c
 *
 * @brief      Helper function for all the print formatting needed for human readable output. 
 *

 * @note that the snprintf function requires more RAM than the defaul CubeMx configuration allows
 *
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */
#include "client.h"
#include "cli_task.h"
#include "CotaCommonTypes.h"
#include "print_format.h"
#include "version_from_source_control.h"

#define ERR_SRC_NONE    255   ///< "Last error" field value for "No errors"


/**
 * @brief Retrieve a pointer to a string representing the system state
 * @param state State value for the system state
 * @return pointer to constant string containing a name for the state.
 */
const char* GetSystemStateString(LightRingState_t state)
{
    switch (state)
    {
    case LR_STATE_IDLE:
        return "Idle";
        break;
    case LR_STATE_CALIB:
        return "Calibrating";
        break;
    case LR_STATE_WAIT:      
        return "Waiting for Clients";
        break;
    case LR_STATE_READY:
        return "Ready";
        break;
    case LR_STATE_TX: 
        return "Charging";
        break;    
    case LR_STATE_IDENT: 
        return "Identifying the Transmitter";
        break;
    case LR_STATE_DEBUG: 
        return "Debug/Pause";
        break;
    case LR_STATE_ERROR: 
        return "Error";
        break;
    default:
        break;
    }
    return "Unknown";
}

/**
 * @brief Retrieve a pointer to a string representing the client state
 * @param state State value for the client
 * @return pointer to constant string containing a name for the state.
 */
const char* GetClientStateString(ClientState_t state)
{
    switch (state)
    {
    case RCVR_STATE_STATE_UNKNOWN:
        return "Unknown            ";
        break;
#if defined(USE_DEMO_BUILD) 
    case RCVR_STATE_READY:
        return "Ready              ";
        break;
    case RCVR_STATE_CHARGING: 
        return "Charging           ";
        break;    
    case RCVR_STATE_DISCONNECT: 
        return "Disconnecting      ";
        break;
    case RCVR_STATE_SILENT: 
        return "Silent             ";
        break;
    case RCVR_STATE_DISCOVERY:
        return "Discovery          ";
        break;
    case RCVR_STATE_REGISTERED:
        return "Registered         ";
        break;
    case RCVR_STATE_JOINED:      
        return "Joined             ";
        break;
#elif defined(USE_FOREVER_TRACKER_BUILD)
    case RCVR_STATE_ANNOUNCED:      
        return "Announced          ";
        break;
    case RCVR_STATE_NOT_READY_TO_CHARGE:      
        return "Not Ready To Charge";
        break;
    case RCVR_STATE_READY_TO_CHARGE:      
        return "Ready To Charge    ";
        break;

    case RCVR_STATE_SCHEDULED:
        return "Scheduled to Charge";
        break;
    case RCVR_STATE_REMOVED_FROM_DATABASE:
        return "Will be Removed";
        break;
#endif
    default:
        break;
    }
    return "Undefined          ";
}
/**
 *  @brief Prints out client detail to the CLI
 * 
 *  @param client Pointer to a structure of type #Client_t containing client information
 */
void PrintClientDetail(Client_t* client)
{
    if (client != NULL)
    {
        PostPrintf("\r\n");
        PostPrintf("Short ID:        0x%04x\r\n", client->shortId);
        PostPrintf("Long ID:         0x%016llx\r\n", *(uint64_t*)&client->longId);
        PostPrintf("Version:         %d.%d\r\n", (client->version >> 8), (client->version & 0xff));
        PostPrintf("Model:           0x%x\r\n", client->hwModel);
        PostPrintf("State:           %s\r\n", GetClientStateString(client->state));
        PostPrintf("Error:           %d\r\n", client->qStatus.status.statusFlags.errorStatus);
        PostPrintf("Missed TPS:      %d\r\n", client->qStatus.status.statusFlags.missedTpsCycles);        
        PostPrintf("Battery:         %d\r\n", client->qStatus.status.batteryCharge);
#if defined(USE_DEMO_BUILD)
        PostPrintf("Power Requested: %s\r\n", client->qStatus.status.statusFlags.powerRq ? "Power requested": "Power not requested");        
        if (client->qStatus.status.avgPower == INVALID_POWER_VALUE)
        {
            PostPrintf("Average Power: ----\r\n", client->qStatus.status.avgPower);       
        }
        else
        {
            PostPrintf("Average Power:   %d dBm\r\n", client->qStatus.status.avgPower);         
        }
        
        if (client->qStatus.status.peakPower == INVALID_POWER_VALUE)
        {
            PostPrintf("Peak Power:    ----\r\n", client->qStatus.status.peakPower);          
        }
        else
        {
            PostPrintf("Peak Power:      %d dBm\r\n", client->qStatus.status.peakPower);          
        }
#elif defined(USE_FOREVER_TRACKER_BUILD)
        PostPrintf("Sleep flag:      %s\r\n", client->qStatus.status.trackerStatusFlags.sleepFlag ? "Yes": "No");        
#else
    #error Build target not defined properly
#endif
                
        PostPrintf("Net Current:     %d mA\r\n", client->qStatus.status.peakNetCurrent);
        PostPrintf("RSSI:            %d (dBm)\r\n", client->qStatus.header.rssi);
        PostPrintf("Link Quality:    %d\r\n", client->qStatus.header.linkQuality);
        
        uint32_t milliSeconds = TICKS_TO_MS(client->lastCommTimeTicks);
        uint32_t seconds = MS_TO_SECONDS(milliSeconds);
        uint32_t minutes = SEC_TO_MIN(seconds);
        uint32_t hours   = MIN_TO_HOURS(minutes);
        
        PostPrintf("Last query time: %02d:%02d:%02d\r\n", hours, minutes % 60, seconds % 60);
        
        if (client->qStatus.header.queryType == RCVR_QUERY_CUSTOM)
        {
            CustomQueryData_t* customQueryData = (CustomQueryData_t*)&client->qStatus.data[0];
            uint8_t antPortIdx;
            PostPrintf("\r\nCustom data:\r\n");
            PostPrintf("    Battery Voltage:  %d mV\r\n", customQueryData->batteryVoltage);

            for (antPortIdx = 0; antPortIdx < NUM_ANTENNA_PORTS; antPortIdx++)
            {
                PostPrintf("    Average Power %d:  %d counts/%d mdBm\r\n", 
                           antPortIdx + 1,   // port numbers are 1-based
                           customQueryData->avgRfPower[antPortIdx],
                           customQueryData->avgRfPowerMdBm[antPortIdx]); 
            }
            
            for (antPortIdx = 0; antPortIdx < NUM_ANTENNA_PORTS; antPortIdx++)
            {
                PostPrintf("    Maximum Power %d:  %d counts/%d mdBm\r\n",
                           antPortIdx + 1,   // port numbers are 1-based
                           customQueryData->maxRfPower[antPortIdx],
                           customQueryData->maxRfPowerMdBm[antPortIdx]); 
            }
            
            if (customQueryData->lastError != ERR_SRC_NONE)
            {
                PostPrintf("    Last Error:  %d\r\n", customQueryData->lastError);
            }
        }
		
        else if (client->qStatus.header.queryType == RCVR_QUERY_OPTIMIZED)
        {
            CustomQueryData_t* customQueryData = (CustomQueryData_t*)&client->qStatus.data[0];
            PostPrintf("Battery Voltage:  %d mV\r\n", customQueryData->batteryVoltage);
        }
    }
    else
    {
        PostPrintf("Error: NULL client pointer received\r\n");
    }
}

/**
 * @brief Print app command data
 * @param longId - A receiver's long id
 * @param appData - The receiver's appData for formatted output. 
 */
void PrintAppCommandData(ExtdAddr_t longId, CotaAppData_t appData)
{
    PostPrintf("Receiver     : 0x%llx\r\n", *(uint64_t*)&longId);
    PostPrintf("Message Type : 0x%02x (%d)\r\n", appData.data[0], appData.data[0]);
    PostPrintf("Data         : 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\r\n",
               appData.data[1], appData.data[2], appData.data[3],
               appData.data[4], appData.data[5], appData.data[6],
               appData.data[7], appData.data[8]);

    PostPrintf("16-bit       : %hd %hd %hd %hd\r\n",
               *(uint16_t *) &appData.data[1], *(uint16_t *) &appData.data[3],
               *(uint16_t *) &appData.data[5], *(uint16_t *) &appData.data[7]);

    PostPrintf("32-bit       : %u %u\r\n",
               *(uint32_t *) &appData.data[1],
               *(uint32_t *) &appData.data[5] );
    
    PostPrintf("64-bit       : %llu\r\n",
               *(uint64_t*)&appData.data[1]);
}

/**
 * @brief Print versions
 * @param PrxHostProxyFwInfo_t - Proxy info 
 */
void PrintVersionsCommand(PrxHostProxyFwInfo_t proxyInfo)
{ 
    BuildType_t buildType; 
    
    buildType = GetBuildType();
  
// This proxy revision is the concatenated 16-bit
//   major and minor version in one u32.
    uint16_t proxyMajor = (uint16_t)((proxyInfo.revision >> 16) & 0xffff);
    uint16_t proxyMinor = (uint16_t)((proxyInfo.revision) & 0xffff);  
  
    switch(buildType)
    {                
        case FOREVER_TRACKER:
            {
                PostPrintf("Built For:            Forever Tracker\r\n");
            }
            break;                  
        case DEMO:
            {
                PostPrintf("Built For:            Demo\r\n");
            }
            break;                     
        default:
        break;			
    }    
                  
    PostPrintf("Commit:               %08x\r\n", SOURCE_CONTROL_VERSION);
    PostPrintf("Major Version:        %04x\r\n", RELEASE_VERSION_MAJOR);
    PostPrintf("Minor Version:        %04x\r\n", RELEASE_VERSION_MINOR);
    PostPrintf("Proxy Major Revision: %02d\r\n", proxyMajor);
    PostPrintf("Proxy Minor Revision: %02d\r\n", proxyMinor);
    PostPrintf("Timestamp:            %s\r\n",   TIMESTAMP);
}


