/**
 * @file       log.c
 *
 * @brief      Allows the system to log system behavior and allows users to
 *             decide what type of log messages they will receive.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "log.h"
#include "ctrl_interface.h"
#include "rpi_msg_interface.h"
#include "cli_task.h"

#define LOG_BUF_SIZE    MAX_LOG_STRING_SIZE    ///< Maximum buffer size
#define LOG_PREFIX_SIZE 16                     ///< Maximum prefix size for entries in gLogPrefix

/** Given a log level (LogLevel_t), this returns a prefix string */
static char * gLogPrefix[] =
{
    "No Log - ",  ///< Should not be used, this indicates an error
    "FATAL - ",   ///< For log level LOG_FATAL  
    "ERROR - ",   ///< For log level LOG_ERROR  
    "WARNING - ", ///< For log level LOG_WARNING
    "INFO - ",    ///< For log level LOG_INFO   
    "DEBUG- ",    ///< For log level LOG_DEBUG  
};

/** Given a log level (LogLevel_t), this returns the control message type */
static CtrlMsgId_t gLogLevelToCtrlMessageType[] =
{
    CTRL_MSG_INVALID, ///< Should not be used, this indicates an error
    CTRL_LOG_FAULT,   ///< For log level LOG_FATAL  
    CTRL_LOG_ERROR,   ///< For log level LOG_ERROR  
    CTRL_LOG_WARN,    ///< For log level LOG_WARNING
    CTRL_LOG_INFO,    ///< For log level LOG_INFO   
    CTRL_LOG_DEBUG,   ///< For log level LOG_DEBUG  
};

static char gFormatBuffer[LOG_BUF_SIZE];   ///< For vsprintf formatting

/** For concatting the log prefix with the log for UART output */
static char gUartOutputBuffer[LOG_BUF_SIZE + LOG_PREFIX_SIZE]; 

/** The current log level that defines which messages the system will log.  */
static LogLevel_t gSystemLevel = LOG_NONE; 

#pragma inline=forced
static inline bool isLogLevelSet(LogLevel_t level);

#pragma inline=forced
static inline void xlogStringWithLogLevel(LogLevel_t level, char *string);

#pragma inline=forced
static inline void logToRPiWithLogLevel(LogLevel_t level, char *string);

#pragma inline=forced
static inline void logToUartWithLogLevel(LogLevel_t level, char *string);

#pragma inline=forced
static inline char* getStringPrexfix(LogLevel_t level);

#pragma inline=forced
static inline CtrlMsgId_t getMsgIdFromLogLevel(LogLevel_t level);

#pragma inline=forced
static inline bool isLevelGoodGivenSystemLevel(LogLevel_t level, LogLevel_t systemLevel);

#pragma inline=forced
static inline bool concatPrefixToString(char *prefix,
                                 char *string,
                                 char *outputBuffer,
                                 uint16_t outputBufferSize);



/**
 * @brief Initialize the logging unit.
 * @param systemLevel The log level of the system for initialization. 
 */
void LogInit(LogLevel_t systemLevel)
{
    SetSystemLevel(systemLevel);
}

/**
 * @brief Set log level of the system.
 * @param level The log level 
 */
void SetSystemLevel(LogLevel_t level)
{
    gSystemLevel = level;
}

/**
 * @brief Get log level of the system.
 * @return The system log level 
 */
LogLevel_t GetSystemLevel(void)
{
    return gSystemLevel;
}

/**
 * @brief Logs a Fatal type log
 * @param string - A string containing the log message
 * @param ...    - A list of arguments for printf 
 */
void LogFatal(const char *string, ...)
{
    if (isLogLevelSet(LOG_FATAL))
    {
        va_list  args;
        char *formattedString = gFormatBuffer;
        uint16_t maxStringSize = sizeof(gFormatBuffer);
    
        // Format the string with its arguments
        va_start(args, string);
        vsnprintf(formattedString, maxStringSize, string, args);
        va_end(args);

        xlogStringWithLogLevel(LOG_FATAL, formattedString);
    }
}

/**
 * @brief Logs a Error type log
 * @param string - A string containing the log message
 * @param ...    - A list of arguments for printf 
 */
void LogError(const char *string, ...)
{
    if (isLogLevelSet(LOG_ERROR))
    {
        va_list  args;
        char *formattedString = gFormatBuffer;
        uint16_t maxStringSize = sizeof(gFormatBuffer);

        // Format the string with its arguments
        va_start(args, string);
        vsnprintf(formattedString, maxStringSize, string, args);
        va_end(args);

        xlogStringWithLogLevel(LOG_ERROR, formattedString);
    }
}

/**
 * @brief Logs a Warning type log
 * @param string - A string containing the log message
 * @param ...    - A list of arguments for printf 
 */
void LogWarning(const char *string, ...)
{
    if (isLogLevelSet(LOG_WARNING))
    {
        va_list  args;
        char *formattedString = gFormatBuffer;
        uint16_t maxStringSize = sizeof(gFormatBuffer);

        // Format the string with its arguments
        va_start(args, string);
        vsnprintf(formattedString, maxStringSize, string, args);
        va_end(args);

        xlogStringWithLogLevel(LOG_WARNING, formattedString);
    }
}

/**
 * @brief Logs a Info type log
 * @param format - A string containing printf-style format specifiers
 *                 and having the same security implications as in printf
 * @param ...    - A list of arguments for printf 
 */
void LogInfo(const char *string, ...)
{
    if (isLogLevelSet(LOG_INFO))
    {
        va_list  args;
        char *formattedString = gFormatBuffer;
        uint16_t maxStringSize = sizeof(gFormatBuffer);

        // Format the string with its arguments
        va_start(args, string);
        vsnprintf(formattedString, maxStringSize, string, args);
        va_end(args);

        xlogStringWithLogLevel(LOG_INFO, formattedString);
    }
}


/**
 * @brief Logs a Debug type log
 * @param string - A string containing the log message
 * @param ...    - A list of arguments for printf 
 */
void LogDebug(const char *string, ...)
{
    if (isLogLevelSet(LOG_DEBUG))
    {
        va_list  args;
        char *formattedString = gFormatBuffer;
        uint16_t maxStringSize = sizeof(gFormatBuffer);

        // Format the string with its arguments
        va_start(args, string);
        vsnprintf(formattedString, maxStringSize, string, args);
        va_end(args);

        xlogStringWithLogLevel(LOG_DEBUG, formattedString);
    }
}

/**
 * @brief Sends formattedString to all systems that can accept the log.
 * @param level - The log level the message is being sent at.
 * @param string - The string that contains the log string 
 */
#pragma inline=forced
static inline void xlogStringWithLogLevel(LogLevel_t level, char *string)
{
    logToRPiWithLogLevel(level, string);
    logToUartWithLogLevel(level, string);
}

/**
 * @brief Determines if level is a good level for logging given the
 *        current level the system is set to.
 * @param level The log level
 * @return true if the level is good
 */
#pragma inline=forced
static inline bool isLogLevelSet(LogLevel_t level)
{
    return isLevelGoodGivenSystemLevel(level, gSystemLevel);

}
/**
 * @brief Log levels are enums where the severity decreases as the enum
 *        integer value increases. A level is good if it is considered
 *        more severe or equal to the system level.
 * @param level The log level
 * @param systemLevel The system log level
 * @return true if level is good.
 */
#pragma inline=forced
static inline bool isLevelGoodGivenSystemLevel(LogLevel_t level, LogLevel_t systemLevel)
{
    return ((uint8_t) level <= (uint8_t) systemLevel);
}

/**
 * @brief Logs the message string to the Raspberry Pi with its level
 * @param level - The level of the log
 * @param string - the message string to be output 
 */
#pragma inline=forced
static inline void logToRPiWithLogLevel(LogLevel_t level, char *string)
{
    CtrlResp_t  resp;
    uint16_t    len = 0;
    uint16_t    strLen = strlen(string) + 1; // +1 for null termination.
    CtrlMsgId_t    msgId; 

    strLen = MIN(strLen, LOG_BUF_SIZE);
    msgId = getMsgIdFromLogLevel(level);
 
    resp.id  = msgId;
    resp.err = COTA_ERROR_NONE;
    strncpy(resp.logMessage.buff, string, strLen);

    // Always add null-terminating character. 
    resp.logMessage.buff[strLen - 1] = '\0';

    // We use strLen because sizeof(RespLog_t) will
    // send extra, useless data
    len = sizeof(msgId) + 
          sizeof(resp.err) +
          strLen;

    RpiSendMessage((uint8_t*) &resp, len);
}

/**
 * @brief Gets the CtrlMsgId_t relative to the log Level
 * @param level - The level of the log
 * @return The CtrlMsgId_t that is relative to the log level 
 */
#pragma inline=forced
static inline CtrlMsgId_t getMsgIdFromLogLevel(LogLevel_t level)
{
    CtrlMsgId_t msgId;

    if (level >= LOG_LEVEL_COUNT)
    {
        msgId = CTRL_MSG_INVALID;
    }
    else
    {
        msgId = gLogLevelToCtrlMessageType[level];
    }
    return msgId;
}

/**
 * @brief Logs the message string to the UART with its level
 * @param level - The level of the log
 * @param string The message
 * @return The CtrlMsgId_t that is relative to the log level 
 */
#pragma inline=forced
static inline void logToUartWithLogLevel(LogLevel_t level, char *string)
{
    char *prefix = getStringPrexfix(level);
    bool success = false;
    
    if ((string != NULL) && (prefix != NULL))
    {
        success = concatPrefixToString(prefix,
                                       string,
                                       gUartOutputBuffer,
                                       sizeof(gUartOutputBuffer));

        if (success)
        {
            PostStringForTransmit(gUartOutputBuffer);
        }
        else
        {
            PostPrintf("Unable to output log due to string size\r\n");
        }
    }
}

/**
 * @brief concatenates prefix and a string and places it in the output buffer.
 * @param prefix - The prefex string
 * @param string - the target string
 * @param outputBuffer - the buffer that will get written to .
 * @param outputBufferSize The size of the output buffer
 * @return true on success, false on a failure. 
 */
#pragma inline=forced
static inline bool concatPrefixToString(char *prefix,
                                        char *string,
                                        char *outputBuffer,
                                        uint16_t outputBufferSize)
{
    bool success = false; 
    uint16_t strLen = strlen(string); 
    uint16_t preLen = strlen(prefix);
    
    // Otherwise the output buffer will not be big enough.
    if ((strLen + preLen) < outputBufferSize)
    {
        // Static global buffer is used to minimize stack usage.
        memset(gUartOutputBuffer, 0, outputBufferSize);
        strcat(gUartOutputBuffer, prefix);
        strcat(gUartOutputBuffer, string);

        success = true; 
    }

    return success; 
}

/**
 * @brief Gets the prefix string relative to the log Level
 * @param level - The level of the log
 * @return The string prefix that is relative to the log level 
 */
#pragma inline=forced
static inline char* getStringPrexfix(LogLevel_t level)
{
    char* string;
    if (level >= LOG_LEVEL_COUNT)
    {
        string = NULL;
    }
    else
    {
        string = gLogPrefix[level];
    }

    return string;
}
