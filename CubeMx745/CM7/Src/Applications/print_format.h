/****************************************************************************//**
* @file      print_format.h
*
* @brief     A header for the print_format module. 
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2020 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#ifndef _PRINT_FORMAT_H_
#define _PRINT_FORMAT_H_

#include "client.h"
#include "cli_task.h"

void PrintClientDetail(Client_t* state);
const char* GetClientStateString(ClientState_t state);
const char* GetSystemStateString(LightRingState_t state);
void PrintAppCommandData(ExtdAddr_t longId, CotaAppData_t appData);
void PrintVersionsCommand(PrxHostProxyFwInfo_t proxyInfo);

#endif // _PRINT_FORMAT_H_
