/**
* @file       error_task.c
*
* @brief      The purpose of this header is to supply an interface to communicate errors
*             to external entities such as a UART or a Raspberry PI.
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*/


#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "error.h"
#include "string.h"
#include "stdio.h"
#include "uart_driver.h"
#include "orion_config.h"
#include "cli_task.h"


#ifdef COTA_ERROR_ENABLED                     ///< must be defined in the compile line to enable error handling.


#define ERROR_HANDLER_QUEUE_DEPTH   5         ///< The maximum number of error codes the error queue can hold.
#define ERROR_HANDLER_BUFFER_SIZE   64        ///< The maximum size of an output message for errors
#define DONT_WAIT_FOR_QUEUE         0         ///< When posting to the queue we don't wait for available spots.


#ifdef USE_COTA_ERROR_STRINGS
/**
 * This array maps an error code to its mask and potentially a descriptive string.
 */
static char* COTA_ERROR_STRINGS[] = {
    "SUCCESS",                                                                                   ///< COTA_ERROR_NONE
    "Driver error.",                                                                             ///< COTA_ERROR_HAL
    "Error handler queue not created.",                                                          ///< COTA_ERROR_QUEUE_FAILED
    "Error handler task failed to create.",                                                      ///< COTA_ERROR_TASK_FAILED
    "A SPI transfer has failed.",                                                                ///< COTA_ERROR_SPI_TRANSFER_FAILED
    "A simultaneous SPI transfer has failed.",                                                   ///< COTA_ERROR_SPI_SIMUL_TRANS_FAILED
    "A simultaneous read of UVPs has overflowed the buffer.",                                    ///< COTA_ERROR_UVP_SIMUL_READ_TO_LARGE
    "A UART transmit failed.",                                                                   ///< COTA_ERROR_UART_TX_FAILED
    "A UVP lock could not be confirmed.",                                                        ///< COTA_ERROR_LOCK_DETECT_FAILED
    "UVP test failed.",                                                                          ///< COTA_ERROR_UVP_TEST_FAILED
    "An attempt to set a UVP value failed because the value couldn't fit in the mask.",          ///< COTA_ERROR_UVP_REG_VALUE_NOT_IN_MASK
    "An attempt was made to enable a UVP on an AMB not enabled.",                                ///< COTA_ERROR_AMB_NOT_ENABLED
    "An attempt was made to select an unknown UVP number.",                                      ///< COTA_ERROR_UVP_UNKNOWN
    "A UVP could not confirm a transmit occurred and timed out.",                                ///< COTA_ERROR_UVP_WAIT_FOR_TRANSMIT_TIMEOUT
    "An error occurred while waiting for a phase detect from a UVP.",                            ///< COTA_ERROR_UVP_WAIT_FOR_PHASE_DET_TIMEOUT
    "An error occurred while enabling UVP's.",                                                   ///< COTA_ERROR_UVP_ENABLE
    "An error occurred while selecting the page for the system clock.",                          ///< COTA_ERROR_SYSCLK_FAILED_TO_WRITE_PAGE
    "An error occurred while writing a register to the system clock.",                           ///< COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG
    "An error occurred while reading a register from the system clock.",                         ///< COTA_ERROR_SYSCLK_FAILED_TO_READ_REG
    "An error occurred because an invalid i2c multiplexer was initialized.",                     ///< COTA_ERROR_I2C_INVALID_MULTIPLEXER
    "An error occurred because an invalid i2c multiplexer channel was initialized.",             ///< COTA_ERROR_I2C_INVALID_MULTI_CHANNEL
    "An error occurred while selecting the channel on an i2c multiplexer.",                      ///< COTA_ERROR_I2C_FAILED_TO_WRITE_MULTI_CHAN
    "A failure occurred while writing to an i2c device.",                                        ///< COTA_ERROR_I2C_FAILED_TO_WRITE
    "A failure occurred while reading from an i2c device.",                                      ///< COTA_ERROR_I2C_FAILED_TO_READ
    "A command was called that is not implemented.",                                             ///< COTA_ERROR_NOT_IMPLEMENTED
    "An attempt was made to read a temperature from a thermometer that doesn't exist.",          ///< COTA_ERROR_INVALID_CCB_THERMOMETER
    "A bad parameter was passed to a function.",                                                 ///< COTA_ERROR_BAD_PARAMETER
    "A critical temperature has been exceeded.",                                                 ///< COTA_ERROR_OVER_CRITICAL_TEMPERATURE
    "The Receiver Manager failed to properly initialize.",                                       ///< COTA_ERROR_RCVR_MANAGER_INIT_FAILED
    "Receiver storage is full. The maximum number has been reached.",                            ///< COTA_ERROR_RCVR_STORAGE_FULL
    "Receiver Id is not recognized in the system.",                                              ///< COTA_ERROR_RCVR_COULD_NOT_BE_FOUND
    "Tried to add a Receiver that was already registered.",                                      ///< COTA_ERROR_RCVR_ALREADY_REGISTERED
    "A function received a null value where a non-null value was expected.",                     ///< COTA_ERROR_NULL_VALUE_PASSED
    "Error indicating that Receiver AVL tree failed to remove a node.",                          ///< COTA_ERROR_FAILED_TO_REMOVE_NODE
    "Failed to begin an acknowledgment timer to exit a state.",                                  ///< COTA_ERROR_COMM_TIMER_START_FAIL
    "A variable length queue could not be created.",                                             ///< COTA_ERROR_FAILED_TO_CREATE_VAR_QUEUE
    "A message could not be sent to a variable queue.",                                          ///< COTA_ERROR_FAILED_TO_SEND_TO_VAR_QUEUE
    "Failed to send a message to the CLI.",                                                      ///< COTA_ERROR_CLI_TX_FAILED
    "The mutex in the orion_config.c file failed to take.",                                      ///< COTA_ERROR_CONFIG_MUTEX_FAILED_TAKE
    "The mutex in the orion_config.c file failed to give.",                                      ///< COTA_ERROR_CONFIG_MUTEX_FAILED_GIVE
    "Failed to send a proxy message. The queue is likely full.",                                 ///< COTA_ERROR_FAILED_TO_SEND_PROXY_MSG
    "Ctrl Task failed to parse a proxy message.",                                                ///< COTA_ERROR_FAILED_TO_PARSE_PROXY_MSG
    "Failed to save Receiver data - NVM memory full.",                                           ///< COTA_ERROR_NVM_MEMORY_FULL
    "Receiver doesn't exist in NVM. Could not delete.",                                          ///< COTA_ERROR_NVM_INVALID_DELETE
    "Invalid EEPROM address specified for writing.",                                             ///< COTA_ERROR_EEPROM_INVALID_ADDRESS
    "Invalid configuration parameter specified for reading.",                                    ///< COTA_ERROR_CFG_PARAM_NOT_FOUND
    "Failed to load Receiver from the NVM.",                                                     ///< COTA_ERROR_FAILED_TO_LOAD_RCVR_FROM_NVM
    "Failed to create a schedule for tps.",                                                      ///< COTA_ERROR_TPS_SCHEDULER_FAILED
    "Failed to create a schedule for queries.",                                                  ///< COTA_ERROR_CQT_SCHEDULER_FAILED
    "A query confirmation failed.",                                                              ///< COTA_ERROR_QUERY_CONFIRM_FAILED
    "The control task timer failed to initialize.",                                              ///< COTA_ERROR_CTRL_TIMERS_INIT_FAILED
    "A semaphore or mutex failed to create.",                                                    ///< COTA_ERROR_SEMAPHORE_CREATION_FAILED
    "The power supplies failed to come up.",                                                     ///< COTA_ERROR_POWER_FAILURE
    "This command can only be run in debug mode. Send 'pause' first.",                           ///< COTA_ERROR_NOT_IN_DEBUG_MODE
    "Proxy FW update command failed.",                                                           ///< COTA_ERROR_PRX_FW_UPDATE_CMD
    "An RSSI Filtr command was issued without RSSI voting enabled.",                             ///< COTA_ERROR_RSSI_FILTER_NOT_ENABLED
    "AMB's must be enabled for calibration to run.",                                             ///< COTA_ERROR_CALIBRATION_NEEDS_AMBS
    "Door initialization failed.",                                                               ///< COTA_ERROR_DOOR_INIT_FAILED
    "Door open/close detection not supported.",                                                  ///< COTA_ERROR_DOOR_NOT_SUPPORTED
    "An invalid message type was received at the control interface.",                            ///< COTA_ERROR_INVALID_CTRL_MSG_RECEIVED
    "An app command cannot be issued because the control task is busy with other app commands.", ///< COTA_ERROR_APP_CMD_BUSY
    "Unable to find the selected Receiver NVM when searching EEPROM.",                           ///< COTA_ERROR_RCVR_NVM_NOT_FOUND
    "The Receiver is Discovered but not Registered.",                                            ///< COTA_ERROR_RCVR_NOT_REGISTERED
    "A RUN mode required command was received while in DEBUG mode.",                             ///< COTA_ERROR_RUN_MODE_REQUIRED
    "An attempt to receive from a message queue failed.",                                        ///< COTA_ERROR_FAILED_TO_RECEIVE_MSG_QUEUE
};

#define COTA_ERROR_MAP_SIZE (sizeof(COTA_ERROR_STRINGS)/sizeof(COTA_ERROR_STRINGS[0]))

// Ensure all error values defined in the enum in the header file have a corresponding description string defined in this file
static_assert(COTA_ERROR_MAP_SIZE == COTA_ERROR_COUNT, "The number of errors and error descriptions don't match");
#endif  // #ifdef USE_COTA_ERROR_STRINGS

/**
 * @brief Outputs error code information to the UART
 *
 * @param  errorCode An error code of type #CotaError_t
 */
static void OutputError(CotaError_t errorCode)
{
#ifdef USE_COTA_ERROR_STRINGS
    PostPrintf("ERROR %d: %s\r\n", errorCode, COTA_ERROR_STRINGS[errorCode]);
#else
    PostPrintf("ERROR %d\r\n", errorCode);
#endif
}

/**
 * @brief  Outputs an error to UART.
 *
 * @param  cotaError An error code of type #CotaError_t,  If it's COTA_ERROR_NONE, the error is not posted
 *
 * @return returns the cotaError passed so you can place this in the final return call of the function.
 */
CotaError_t PostCotaError(CotaError_t cotaError)
{
    if (cotaError != COTA_ERROR_NONE)
    {
        OutputError(cotaError);
    }
    return cotaError;
}

#endif //#ifdef COTA_ERROR_ENABLED
