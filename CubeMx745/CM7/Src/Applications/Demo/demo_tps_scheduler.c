/**
 * @file       demo_tps_scheduler.c
 *
 * @brief      Schedules a list of client for the next TPS task and creates the query table.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "stdlib.h"
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "error.h"
#include "string.h"
#include "stdio.h"
#include "uart_driver.h"
#include "orion_config.h"
#include "cli_task.h"
#include "tps_scheduler.h"
#include "client.h"
#include "client_manager.h"
#include "tps_scheduler.h"

#if defined(USE_DEMO_BUILD)

#define MAX_NUM_CLIENTS_TO_CHARGE 8   ///< Maximum number of clients that can be queried at the same time.
#define MAXIMUM_CLIENT_PRIORITY   100 ///< The maximum allowed client priority
#define FIRST_SPOT                0   ///< The first spot in an array

// These functions are provided by client_manager.c
PRIVATE bool GetLongId(ShrtAddr_t shortId, ExtdAddr_t *longId);
PRIVATE bool GetClient(ExtdAddr_t longId, Client_t *client);
PRIVATE bool SaveClient(Client_t client);
PRIVATE bool RemoveClient(ExtdAddr_t longId);
PRIVATE void Walk(void (*func)(Client_t* client));
PRIVATE bool FindClientState(ClientState_t state, Client_t *client);

static TonePowerSched_t gTonePowerSchedule;             ///< The tone power schedule sent to the proxy at the start of TPS
static TonePowerSched_t gLastTonePowerSchedule;         ///< The last tone power schedule, used to determine if need increment sequence number
static uint8_t gTpsPriority[MAX_NUM_CLIENTS_IN_TPS];    ///< Holds the priorities of the client in the tone power schedule
static uint8_t gClientChargePriorityThreshold;          ///< If a client false below thing charge level, its priority is elevated more.
static uint16_t gTpsMaxNumClients;                      ///< The maximum allowed clients in the schedule
static uint8_t gTpsNumPowerSlotsPerClient;              ///< The number of power slots per client
static CotaError_t gScheduleError;                      ///< The error code if a failure occurs

/**
 * @brief Initializes the sequence number of the tps schedule
 *
 * @note The proxy won't issue a new TPS message to the client unless the sequence 
 *       number changes from the previously issued sequence number in the host-to-proxy 
 *       TPS message, and since at start-up we don't know what the previous sequence number was, 
 *       we take a gamble and issue a random new sequence number. 
 */
void InitializeScheduler(void)
{
    gTonePowerSchedule.seqNum = rand() & 0xff;
}

/**
 * @brief Increments the sequence number
 */
void IncrementTpsSeqNum(void)
{
    gTonePowerSchedule.seqNum++;
}

/**
 * @brief Retrieve the tone power schedule 
 * 
 * @return A pointer to a structure of type #TonePowerSched_t
 */ 
TonePowerSched_t* GetTpsSchedule(void)
{
    return &gTonePowerSchedule;
}

/**
 * @brief Increments the tps priority for clients that are not included in the tps schedule
 *
 * @param client The client to increment the tps priority for
 */
static void adjustTpsPriority(Client_t* client)
{
    uint8_t ii = 0;
    uint8_t foundIndex = MAX_NUM_CLIENTS_TO_CHARGE;
    
    if ((client->state == RCVR_STATE_CHARGING) && (client->qStatus.status.statusFlags.powerRq) && (gScheduleError == COTA_ERROR_NONE))
    {      
        for (ii = 0; ii < gTonePowerSchedule.numClients; ii++)
        {
            if (gTonePowerSchedule.clientTable[ii].shortId == client->shortId)
            {
                foundIndex = ii;      
            }
        }   
    
        if (foundIndex >= MAX_NUM_CLIENTS_TO_CHARGE)
        { 
            //This client was eligible for tps but was not included
            client->priority++;
        }
        else
        {
            //This client is  being scheduled
            client->priority = 0;
        }
        
        if (client->qStatus.status.batteryCharge < gClientChargePriorityThreshold)
        {
            client->priority += (gClientChargePriorityThreshold - client->qStatus.status.batteryCharge);
        }
        if (client->priority > MAXIMUM_CLIENT_PRIORITY)
        {
            client->priority = MAXIMUM_CLIENT_PRIORITY; 
        }             
    }
}

/**
 * @brief Called with each client as the tree is walked by TPS scheduler
 */ 
static void scheduleTpsClients(Client_t* client)
{
    uint8_t currEntry = gTonePowerSchedule.numClients;
    uint8_t ii;
    if (IsRcvrReadyForCharging(client) && (gScheduleError == COTA_ERROR_NONE))
    {
        //Search the clientTable from the end, and find out where this client should go
        while (currEntry)
        {
            currEntry--;
            
            if (gTpsPriority[currEntry] > client->priority)
            {
                currEntry++;
                break;
            }
        }
        
        //Now that we know where to insert the client, let's make room for it.
        
        if (gTonePowerSchedule.numClients != gTpsMaxNumClients)
        {
            //The tps schedule is not full, so let's make room for the new item
            gTonePowerSchedule.numClients++;            
        }
         
        ii = gTonePowerSchedule.numClients - 1;
        
        if (currEntry < gTpsMaxNumClients)
        {
            //Move all the spots greater than currEntry up by one
            while (ii > currEntry)
            {
                gTonePowerSchedule.clientTable[ii].shortId = gTonePowerSchedule.clientTable[ii-1].shortId;
                gTpsPriority[ii] =  gTpsPriority[ii-1];
                ii--;
            }
                
            gTonePowerSchedule.clientTable[currEntry].shortId = client->shortId;
            gTpsPriority[currEntry] = client->priority;
        }
    }
}
/**
 * @brief Calculates the tone power schedule based on a priority of 0-100.
 * 
 * @note 100 is the highest priority, 0 is the lowest.  A priority is incremented if a 
 *       client is exceluded from the tps.  Also, its priority is incremented more if
 *       its battery level is below the battery threshold.
 *
 * @param chargeVirtual If set to true, the tps schedule will consist 
 *                      of a single "virtual" client.
 * @return return COTA_ERROR_NONE if successful, otherwise it returns an error code
 */
CotaError_t CalculateTpsSchedule(bool chargeVirtual)
{     
    uint8_t ii;
    
    CfgGetParam(CFG_CHG_PRIORITY_THRESHOLD, &gClientChargePriorityThreshold, sizeof(gClientChargePriorityThreshold));
    CfgGetParam(CFG_TPS_NUM_CLIENTS, &gTpsMaxNumClients, sizeof(gTpsMaxNumClients));
    CfgGetParam(CFG_TPS_NUM_POWER_SLOTS_PER_CLIENT, &gTpsNumPowerSlotsPerClient, sizeof(gTpsNumPowerSlotsPerClient));
   
    if (gTpsMaxNumClients > MAX_NUM_CLIENTS_IN_TPS)
    {
        gTpsMaxNumClients = MAX_NUM_CLIENTS_IN_TPS;
        PostPrintf("Number clients in tps may not exceed %d\r\n", MAX_NUM_CLIENTS_IN_TPS); 
        CfgSaveParam(CFG_TPS_NUM_CLIENTS, &gTpsMaxNumClients, sizeof(gTpsMaxNumClients));
    }
    
    if (chargeVirtual)
    {
        gTonePowerSchedule.numClients = 1;
        gTonePowerSchedule.prxCmd = HOST_PRX_TPS_TABLE;
        gScheduleError = COTA_ERROR_NONE;
        gTonePowerSchedule.repeatPeriod = gTonePowerSchedule.numClients; 
        gTonePowerSchedule.clientTable[FIRST_SPOT].shortId = INVALID_SHORT_ID_0;
        gTonePowerSchedule.clientTable[FIRST_SPOT].toneSlot = 1; //slots start at one
        gTonePowerSchedule.clientTable[FIRST_SPOT].tonePeriod = gTonePowerSchedule.numClients;
        gTonePowerSchedule.clientTable[FIRST_SPOT].powerSlot = 1;
        gTonePowerSchedule.clientTable[FIRST_SPOT].powerDuration = gTpsNumPowerSlotsPerClient;
        gTonePowerSchedule.clientTable[FIRST_SPOT].powerPeriod = gTpsNumPowerSlotsPerClient * gTonePowerSchedule.numClients;
    }
    else
    {
        gTonePowerSchedule.numClients = 0;
        gTonePowerSchedule.prxCmd = HOST_PRX_TPS_TABLE;
        gScheduleError = COTA_ERROR_NONE;
        
        Walk(scheduleTpsClients);
        
        Walk(adjustTpsPriority);
        
        if (gScheduleError == COTA_ERROR_NONE)
        {
            gTonePowerSchedule.repeatPeriod = gTonePowerSchedule.numClients; 
           
            //We now have a table of shortId's to charge, now we need to complete the table, and reset priorities
            for (ii=0; (ii <  gTonePowerSchedule.numClients) && (gScheduleError == COTA_ERROR_NONE); ii++)
            {
                gTonePowerSchedule.clientTable[ii].toneSlot = ii + 1; //slots start at one
                gTonePowerSchedule.clientTable[ii].tonePeriod = gTonePowerSchedule.numClients;
                gTonePowerSchedule.clientTable[ii].powerSlot = 1 + gTpsNumPowerSlotsPerClient * ii;
                gTonePowerSchedule.clientTable[ii].powerDuration = gTpsNumPowerSlotsPerClient;
                gTonePowerSchedule.clientTable[ii].powerPeriod = gTpsNumPowerSlotsPerClient * gTonePowerSchedule.numClients;
             }
        }
    }
    
    if (memcmp(&gTonePowerSchedule, &gLastTonePowerSchedule, sizeof(TonePowerSched_t)) != 0)
    {
        IncrementTpsSeqNum();
        memcpy(&gLastTonePowerSchedule, &gTonePowerSchedule, sizeof(TonePowerSched_t));
    }

    return POST_COTA_ERROR(gScheduleError);  
}

#endif // defined(USE_DEMO_BUILD)
