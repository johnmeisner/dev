/**
 * @file       demo_query.c
 *
 * @brief      Implementation of query control for Demo project.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2021 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "cqt_scheduler.h"
#include "client_interface.h"
#include "query_sm.h"
#include "query_sm_helper.h"
#include "client.h"
#include "debug_on.h"

#if defined(USE_DEMO_BUILD)

/**
 *  @brief Called for each query cycle
 *
 *  @brief Sets up for what happens next. To be called from QueryToRdyExit()
 */
void DoImplementationSpecificQueryCycle(void)
{
DBG5_SET(); //trigger logic probe: "Query of receiver"
    if (DoRxStillNeedQuery())
    {
        EnableQueryCycle(true);
    }
    else
    {
        // Only start the query timer when we finished send all of our
        // queries.
        WrapUpClientQueryState();
        StartQueryTimer();
        LogInfo("All clients have been sent queries as part of the query period\r\n");
    }
DBG5_RESET();
}

#endif // defined(USE_DEMO_BUILD)
