/**
 * @file       demo_client_interface.h
 *
 * @brief      Header file for the demo_client_interface.c module supporting DEMO build
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CTRL_INTERFACE_DEMO_H_
#define _CTRL_INTERFACE_DEMO_H_

#include <stdbool.h>
#include "error.h"
#include "client.h"

bool CmGetTpcClient(ShrtAddr_t *shortId);
bool CmGetJoinClient(ExtdAddr_t *longId, ShrtAddr_t *shortId);
void CmHandleUnresponsiveClients(void);
CotaError_t CmRegisterAllClients(CotaRcvrQuery_t queryType);
CotaError_t CmClientJoined(PrxHostClientJoinRspMsg_t joinAck);
bool CmGetChgClient(ShrtAddr_t *shortId);
CotaError_t CmRemoveClients(uint16_t numberOfClients);
CotaError_t CmMarkClientAsDisconnect(ExtdAddr_t* longId);
CotaError_t CmMarkAllClientsAsDisconnect(void);
bool CmGetDisconnectClient(Client_t *shortId);
CotaError_t CmStartChargingClient(ExtdAddr_t longId);
CotaError_t CmStopChargingClient(ExtdAddr_t longId);
void        CmSetAppCmdTriesLeftAll(uint8_t appCmdTriesLeft);
CotaError_t CmStartChargingAllClients(void);
CotaError_t CmSendChargingClientData(void);
CotaError_t CmStopChargingAllClients(void);
CotaError_t CmAddClientFromNvm(ClientSlotData_t nvmClient);
void CmClientList(bool all);
CotaError_t CmClientDiscovered(PrxHostDiscoveryMsg_t discAck);
CotaError_t CmRegisterClient(ExtdAddr_t longId);
CotaError_t CmClientTpcCfn(PrxHostTpcCfn_t tpcAck);

#endif /* _CTRL_INTERFACE_DEMO_H_ */
