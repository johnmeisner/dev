/**
 * @file       demo_client_interface.c
 *
 * @brief      Interface for managing clients for DEMO project.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include "FreeRTOS.h"
#include "projdefs.h"
#include "task.h"
#include "client.h"
#include "client_manager.h"
#include "error.h"
#include "CotaCommonTypes.h"
#include "uart_driver.h"
#include "print_format.h"
#include "nvm_driver.h"
#include "proxyClientMsgInf.h"
#include "cqt_scheduler.h"
#include "tps_scheduler.h"
#include "rpi_msg_interface.h"
#include "proxy_msg_interface.h"
#include "rpi_msg_interface.h"
#include "orion_utilities.h"
#include "log.h"
#include "ctrl_interface.h"
#include "client_interface.h"

#if defined(USE_DEMO_BUILD)
#include "Demo/demo_client_interface.h"

///< These functions are provided by client_manager.c
PRIVATE bool GetLongId(ShrtAddr_t shortId, ExtdAddr_t *longId);
PRIVATE bool GetClient(ExtdAddr_t longId, Client_t *client);
PRIVATE bool SaveClient(Client_t client);
PRIVATE bool RemoveClient(ExtdAddr_t longId);
PRIVATE void Walk(void (*func)(Client_t* client));
PRIVATE bool FindClientState(ClientState_t state, Client_t *client);

static CotaRcvrQuery_t gQueryTypeForRegister; ///< A global used to store query types during wehn all clients being registered
static Client_t        gExpiredClient;        ///< A global used to store an expired client, if found
static bool            gClientListAll;        ///< A global allowing a walk to know if client_list is called with the 'all' argument
static uint8_t         gAppCmdTriesLeft;      ///< The value of app cmd try count to set all register clients to

static bool isPowerModeOpen(void);
static bool isNewlyDiscoveredClient(ExtdAddr_t *longId);
static void registerClient(Client_t* client);
static void addNewlyDiscoveredClient(PrxHostDiscoveryMsg_t *discAck);
static void updateExistingClientFromDiscovery(PrxHostDiscoveryMsg_t *discAck);
static void updateSilentWalk(Client_t *client );

/**
 * @brief Walking function for #CmStopChargingAllClients
 *
 * @param client The current client in the walk.
 */
static void stopChargingWalk(Client_t* client)
{
    if (client->state == RCVR_STATE_CHARGING)
    {
        client->state = RCVR_STATE_READY;
        PostPrintf("Stop charging 0x%016llx\r\n", *(uint64_t*)&client->longId);
    }
    client->userChargeRequested = 0;
}

/**
 * @brief Changes any client in the charing state to the ready state.
 *        All clients 'userChargeRequested' flag is set to 0.
 */
CotaError_t CmStopChargingAllClients(void)
{
    Walk(stopChargingWalk);
    return COTA_ERROR_NONE;
}

/**
 * @brief This command sends all the data on charging clients to the
 *        MM that is meant to be sent after the Orion MCU receives and
 *        acts on a 'start charging' command.
 */
static void chargingClientDataWalk(Client_t* client)
{
    uint16_t len = 0;
    CtrlResp_t  msg;


    if (client->state == RCVR_STATE_CHARGING)
    {
        msg.id                              = CTRL_REDUCED_RCVR_INFO;
        msg.err                             = COTA_ERROR_NONE;
        msg.reducedRcvrInfo.longId        = client->longId;
        msg.reducedRcvrInfo.shortId       = client->shortId;
        msg.reducedRcvrInfo.state         = client->state;
        msg.reducedRcvrInfo.batteryCharge = client->qStatus.status.batteryCharge;
        msg.reducedRcvrInfo.statusFlags   = client->qStatus.status.statusFlags;
        msg.reducedRcvrInfo.avgPower      = client->qStatus.status.avgPower;
        msg.reducedRcvrInfo.peakPower     = client->qStatus.status.peakPower;
        msg.reducedRcvrInfo.netCurrent    = client->qStatus.status.peakNetCurrent;
        msg.reducedRcvrInfo.rssi          = client->qStatus.header.rssi;
        msg.reducedRcvrInfo.linkQuality   = client->qStatus.header.linkQuality;


        len = sizeof(msg.id) +
              sizeof(msg.err) +
              sizeof(RespReducedRcvrInfo_t);

        RpiSendMessage((uint8_t*) &msg, len);
        // To not overload the system with messages, we add a delay.
        vTaskDelay(pdMS_TO_TICKS(3));
    }
}

/**
 * @brief After we get a command to start charging a client via the
 *        message manager (RPi), we also need to return data back for
 *        a returned JSON message. This message will do that by going
 *        through each client that is charging and returning some data
 *        about them.
 */
CotaError_t CmSendChargingClientData(void)
{
    Walk(chargingClientDataWalk);
    return COTA_ERROR_NONE;
}

/**
 * @brief Walking function for #CmStartChargingAllClients
 *
 * @param client The current client in the walk.
 */
static void startChargingWalk(Client_t* client)
{
    if (client->state == RCVR_STATE_READY)
    {
        client->state = RCVR_STATE_CHARGING;
        client->userChargeRequested = 1;
        PostPrintf("Start charging 0x%016llx\r\n", *(uint64_t*)&client->longId);
    }
}

/**
 * @brief Changes any client in the ready  state to the charging state.
 *        The client 'userChargeRequested' flag is also set, but only in clients
 *        change to the charging state.
 */
CotaError_t CmStartChargingAllClients(void)
{
   Walk(startChargingWalk);
   return COTA_ERROR_NONE;
}

/**
 * @brief Changes a client's state to the ready state if it's in the charging state.
 *        The client 'userChargeRequested' flag is also set
 *
 * @param longId  The long id of the client
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t CmStopChargingClient(ExtdAddr_t longId)
{
    Client_t client;
    CotaError_t err = COTA_ERROR_NONE;

    if (GetClient(longId, &client))
    {
        client.userChargeRequested = 0;
        if (client.state == RCVR_STATE_CHARGING)
        {
            client.state = RCVR_STATE_READY;
            PostPrintf("Stop charging 0x%016llx\r\n", *(uint64_t*)&client.longId);
        }
        SaveClient(client);
    }
    else
    {
        err = COTA_ERROR_BAD_PARAMETER;
    }

    return  POST_COTA_ERROR(err);
}

/**
 * @brief Changes a client's state to the charging state if it's in the ready state.
 *        The client 'userChargeRequested' flag is also set
 *
 * @param longId  The long id of the client
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t CmStartChargingClient(ExtdAddr_t longId)
{
    Client_t client;
    CotaError_t err = COTA_ERROR_NONE;

    if (IsLongValid(longId))
    {
        if (GetClient(longId, &client))
        {
            client.userChargeRequested = 1;
            if (client.state == RCVR_STATE_READY)
            {
                client.state = RCVR_STATE_CHARGING;
            }
            SaveClient(client);
            PostPrintf("Start charging 0x%016llx\r\n", *(uint64_t*)&client.longId);
        }
        else
        {
            err = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
        }
    }
    else
    {
        err = COTA_ERROR_BAD_PARAMETER;
    }

    return  POST_COTA_ERROR(err);
}

/**
 * @brief Retrieve a client in the disconnect state if one exists
 *
 * @param client  A pointer to client structure of type #Client_t
 *
 * @return true if client is found; false if not found
 */
bool CmGetDisconnectClient(Client_t *client)
{
    if (client != NULL)
    {
        if (FindClientState(RCVR_STATE_DISCONNECT, client))
        {
            return true;
        }
    }
    return false;
}

/**
 * @brief A walk function designed to sets the app command try count
 * @param client A pointer to the client being considered.
 */
static void setAppCmdTriesLeftClientWalk(Client_t* client)
{
    if ( IsRcvrReadyForComm(client) )
    {
        client->appCmdTriesLeft = gAppCmdTriesLeft;
    }
}

/**
 * @brief Sets the app command try count for all registered clients
 *
 * @param appCmdTriesLeft The number of times left to attempt to send the app command
 */
void CmSetAppCmdTriesLeftAll(uint8_t appCmdTriesLeft)
{
    gAppCmdTriesLeft = appCmdTriesLeft;
    Walk(setAppCmdTriesLeftClientWalk);
}

/**
 * @brief Walking function for #CmMarkAllClientsAsDisconnect
 *
 * @param client The current client in the walk.
 */
static void disconnectWalk(Client_t* client)
{
    if (client->state != RCVR_STATE_DISCOVERY)
    {
        client->state = RCVR_STATE_DISCONNECT;
    }
}

/**
 * @brief Changes any client registered client to the disconnect state
 */
CotaError_t CmMarkAllClientsAsDisconnect(void)
{
     Walk(disconnectWalk);
     return COTA_ERROR_NONE;
}

/**
 * @brief Changes a client's state to the disconnect state
 *
 * @param longId  The long id of the client
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t CmMarkClientAsDisconnect(ExtdAddr_t* longId)
{
    Client_t client;
    CotaError_t err = COTA_ERROR_NONE;

    if (IsLongValid(*longId))
    {
        if (GetClient(*longId, &client) && (client.state != RCVR_STATE_DISCOVERY))
        {
            client.state = RCVR_STATE_DISCONNECT;

            SaveClient(client);
        }
        else
        {
            err = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
        }
    }
    else
    {
        err = COTA_ERROR_BAD_PARAMETER;
    }

    return  POST_COTA_ERROR(err);
}

/**
 * @brief Removes a number clients from client manager and sends a disconnect command
 *        This function is called by ctrlTask to slow the removal of many clients.
 *        By sending a few disconnects at a time, we will not overwhelm the proxy.
 * @param numberOfClients - The number of clients to remove
 * @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t CmRemoveClients(uint16_t numberOfClients)
{
    Client_t client;
    CotaError_t err = COTA_ERROR_NONE;
    CotaError_t errRet = COTA_ERROR_NONE;

    while((numberOfClients > 0) && CmGetDisconnectClient(&client))
    {
        if (RemoveClient(client.longId))
        {
            PrxSendDisconnect(client.shortId);
            //Slots start at 0, but shortId start at 1, so we subtract one

            err = NvmDeleteClient(client.shortId - 1);

            if (errRet == COTA_ERROR_NONE)
            {
                errRet = err;
            }

            LogInfo("Removed 0x%016llx\r\n", *(uint64_t*)&client.longId);
        }
        numberOfClients--;
    }

    return errRet;
}

/**
 * @brief Retrieve a client in the charging state if one exists
 *
 * @param shortId  A pointer to a short id of the client
 *
 * @return true if client is found; false if not found
 */
bool CmGetChgClient(ShrtAddr_t *shortId)
{
    Client_t client;
    if (shortId != NULL)
    {
        if (FindClientState(RCVR_STATE_CHARGING, &client))
        {
            memcpy((void *) shortId, (void *) &(client.shortId), sizeof(ShrtAddr_t));

            return true;
        }
    }
    return false;
}

/**
 * @brief This function is supposed to be used with Walk() from
 *          client_manager.h. Will print each client and their state
 *          to the CLI.
 * @param client - Client that will be printed
 */
static void printClient(Client_t* client)
{
    char avgPower[8];
    char peakPower[8];
    if (gClientListAll || (client->state != RCVR_STATE_SILENT))
    {
        if (client->qStatus.status.avgPower == INVALID_POWER_VALUE)
        {
            snprintf(avgPower, sizeof(avgPower), "----");
        }
        else
        {
            snprintf(avgPower, sizeof(avgPower), "%4d", client->qStatus.status.avgPower);
        }

        if (client->qStatus.status.peakPower == INVALID_POWER_VALUE)
        {
            snprintf(peakPower, sizeof(peakPower), "----");
        }
        else
        {
            snprintf(peakPower, sizeof(peakPower), "%4d", client->qStatus.status.peakPower);
        }

        PostPrintf("0x%016llx  0x%04x   %-20s  %4d  %s     %s     %4d  %4i    %3s        %llu ms\r\n",
                   *(uint64_t*)&client->longId, client->shortId, GetClientStateString(client->state),
                   client->qStatus.status.batteryCharge, avgPower, peakPower, client->priority,
                   client->qStatus.status.peakNetCurrent, client->qStatus.status.statusFlags.powerRq ? "yes" : "no ",
                   TICKS_TO_MS(client->lastCommTimeTicks));
    }
}

/**
 * @brief Send a basic client information to the RPi. Supposed to walk
 *          the client tree via function Walk()
 * @param client - Client data to be sent to RPi
 */
static void sendClientListToRpiWalk(Client_t *client)
{
    uint16_t    len = 0;
    CtrlResp_t  msg;

    if (gClientListAll || (client->state != RCVR_STATE_SILENT))
    {
        msg.id                              = CTRL_REDUCED_RCVR_INFO;
        msg.err                             = COTA_ERROR_NONE;
        msg.reducedRcvrInfo.longId        = client->longId;
        msg.reducedRcvrInfo.shortId       = client->shortId;
        msg.reducedRcvrInfo.state         = client->state;
        msg.reducedRcvrInfo.batteryCharge = client->qStatus.status.batteryCharge;
        msg.reducedRcvrInfo.statusFlags   = client->qStatus.status.statusFlags;
        msg.reducedRcvrInfo.avgPower      = client->qStatus.status.avgPower;
        msg.reducedRcvrInfo.peakPower     = client->qStatus.status.peakPower;
        msg.reducedRcvrInfo.netCurrent    = client->qStatus.status.peakNetCurrent;
        msg.reducedRcvrInfo.rssi          = client->qStatus.header.rssi;
        msg.reducedRcvrInfo.linkQuality   = client->qStatus.header.linkQuality;

        len = sizeof(msg.id) +
              sizeof(msg.err) +
              sizeof(RespReducedRcvrInfo_t);

        // To not overload the system with messages, we add a delay.
        RpiSendMessage((uint8_t*) &msg, len);
        vTaskDelay(pdMS_TO_TICKS(5));
    }
}

/**
 * @brief This function will walk the clients in the tree do the following:
 *          1. Print out the list of clients via UART
 *          2. Send the list of clients to the MM via the rpi_msg_interface API.
 */
void CmClientList(bool all)
{
    gClientListAll = all;
    PostStringForTransmit("Client List:\r\n");

    // For the CLI
    PostPrintf("Long-Id-----------  Short-Id State---------------  Batt  Avg-dBm- Peak-dBm Prio Current  Pwr-Rqst'd Last-Comm-Recv\r\n");
    Walk(printClient);

    // For the Host, RPi, Message Manager
    Walk(sendClientListToRpiWalk);
}

/**
 * @brief Takes TPC confirmation results and updates the client
 *         manager.
 *
 * @param tpcAck - PrxHostTpcCfn_t message received from a client (from
 *          the proxy)
 *
 * @return COTA_ERROR_NONE on success,
 *          COTA_ERROR_RCVR_COULD_NOT_BE_FOUND if client could not be
 *          found. COTA_ERROR_BAD_PARAMETER is the message was not
 *          successful.
 */
CotaError_t CmClientTpcCfn(PrxHostTpcCfn_t tpcAck)
{
    Client_t client;
    ExtdAddr_t longId;

    GetLongId(tpcAck.shortAddr, &longId);

    if (tpcAck.status == PRX_HOST_STAT_SUCCESS) // Indicates successful tpc
    {
        if (GetClient(longId, &client))
        {
            if (client.userChargeRequested)
            {
                client.state = RCVR_STATE_CHARGING;
            }
            else
            {
                client.state  = RCVR_STATE_READY;
            }
            client.queryNeededFlag      = true;
            client.lastCommTimeTicks    = xTaskGetTickCount();
            client.commFailCount        = 0;
            SaveClient(client);
            return COTA_ERROR_NONE;
        }
        return COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
    }
    else
    {
        LogError("TPC confirmation failed for 0x%016llx shortId 0x%04x\r\n",
                   *(uint64_t*)&longId, tpcAck.shortAddr);
        if (GetClient(longId, &client))
        {
            client.commFailCount++;
            SaveClient(client);
        }
    }

    return COTA_ERROR_BAD_PARAMETER;
}

/**
 * @brief This is a special function that is used by the NVM module to
 *        initialize the client manager based on the client data
 *        stored in the NVM module. This function should only be
 *        called from the nvm_driver.c module as part of
 *        initialization.
 *
 * @param nvmClient - A client slot data from the NVM module
 * @return COTA_ERROR_NONE w/ no error.
 */
CotaError_t CmAddClientFromNvm(ClientSlotData_t nvmClient)
{
    Client_t client = {0};

    // In the case when we are initializing after a pause, we
    // may be trying add clients that are already in the client tree.
    // We need to skip those cases.
    if (!GetClient(nvmClient.longId, &client))
    {
        client.state        = RCVR_STATE_SILENT;
        // This client must receive a query to become READY
        client.queryNeededFlag = true;

        // 0 is an invalid shortId belonging to newly discovered clients, so we add 1
        // to the slot number to create the shortId.  If we don't do this,
        // the firmware will search for shortId=0 and find a discovered client
        // rather than the first registered client.
        client.shortId      = nvmClient.nvmSlot+1;
        client.longId       = nvmClient.longId;
        client.version      = nvmClient.version;
        client.hwModel      = nvmClient.hwModel;

        client.qStatus.header.queryType = nvmClient.queryType;

        if (!SaveClient(client))
        {
            return COTA_ERROR_FAILED_TO_LOAD_RCVR_FROM_NVM;
        }
    }

    return COTA_ERROR_NONE;
}

/**
 * @brief This function gets a shortId in the RCVR_STATE_JOINED
 *        state and returns its shortId. This client will receive a
 *        TPC message from outside the scope of this module.
 *
 * @param shortId - An output shortId that will be sent a TPC message
 * @return bool - true if successfully found a JOINED client, false if otherwise.
 */
bool CmGetTpcClient(ShrtAddr_t *shortId)
{
    Client_t client;
    if (shortId != NULL)
    {
        if (FindClientState(RCVR_STATE_JOINED, &client))
        {
            memcpy((void *) shortId, (void *) &(client.shortId), sizeof(ShrtAddr_t));

            return true;
        }
    }
    return false;
}

/**
 * @brief Joined clients are input into the system by being discovered.
 * The must first be removed from the Discovery state, and then
 * put into the Joined state.
 *
 * @todo Remove Join state, it's useless. TPC state suffices
 */
CotaError_t CmClientJoined(PrxHostClientJoinRspMsg_t joinAck)
{
    Client_t client;

    if (joinAck.status == PRX_HOST_STAT_SUCCESS) // Indicates successful join
    {
        if (GetClient(joinAck.extAddr, &client))
        {
            /*
              The client has confirmed that it joined the network, now
              we need to send it a TPC message. This is done auto
              magically if we set the client state to TPC.
            */
            client.state = RCVR_STATE_JOINED;
            client.lastCommTimeTicks = xTaskGetTickCount();
            client.commFailCount = 0;
            SaveClient(client);
            return COTA_ERROR_NONE;
        }
        return COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
    }
    return COTA_ERROR_BAD_PARAMETER;
}

/**
 * @brief A walk function designed to find stale registered clients and mark them as silent
 * @param client A pointer to the client being considered.
 */
static void markSilentClientsWalk(Client_t* client)
{
    // TODO we must change this code to better handle SILENT
    // RCVRs. We don't know what state the RCVRs will be in after
    // they go SILENT, so it becomes difficult to assign a state to
    // the RCVRs as a result.
    if (IsRcvrReadyForComm(client))
    {
        if (client->commFailCount > CmGetCommMaxFailCount())
        {
            LogInfo("Unresponsive client 0x%016llx after %d failed queries is designated as silent\r\n",
                    *(uint64_t*)&client->longId,
                    client->commFailCount);
            client->state = RCVR_STATE_SILENT;
        }
    }
}

/**
 * @brief Handle a discovery acknowledgment message by adding the client to the database.
 * @param discAck - An acknowledgment message for a discovery
 * @return COTA_ERROR_NONE;
 */
CotaError_t CmClientDiscovered(PrxHostDiscoveryMsg_t discAck)
{
    if (isNewlyDiscoveredClient(&discAck.extAddr))
    {
        addNewlyDiscoveredClient(&discAck);
    }
    else
    {
        updateExistingClientFromDiscovery(&discAck);
    }

    return COTA_ERROR_NONE;
}

/**
 * @brief Checks if the client with the id 'longId' is in database already.
 * @param longId - the MAC address of the client.
 * @return true if the client is in the database, false if it is not.
 */
static bool isNewlyDiscoveredClient(ExtdAddr_t *longId)
{
    return !GetClient(*longId, NULL);
}

/**
 *  @brief Changes a client to the registered state and stores it
 *         in non-volatile memory
 *
 *  @param client - A client pointer that will become registered.
 */
static void registerClient(Client_t* client)
{
    ClientSlotData_t nvmClient = {0};

    // Update
    client->state = RCVR_STATE_REGISTERED;
    client->qStatus.status.batteryCharge = 0;

    // The NVM module tells us the shortId, so we must save the
    //   client in the NVM, get the shortId, then save the
    //   client in the CM.

    NvmClientDataInit(&nvmClient);
    nvmClient.longId = client->longId;
    nvmClient.version = client->version;
    nvmClient.hwModel = client->hwModel;
    nvmClient.priority = client->priority;
    nvmClient.queryType = client->qStatus.header.queryType;
    NvmAddClient(&nvmClient);

    client->shortId = nvmClient.nvmSlot+1;
}

/**
 * @brief Adds the client to the system as if it were a new client to the
 *        database.
 * @param discAck - An acknowledgment message for a discovery
 */
static void addNewlyDiscoveredClient(PrxHostDiscoveryMsg_t *discAck)
{
    Client_t client;

    memset((void *) &client, 0, sizeof(Client_t));

    client.qStatus.header.queryType   = RCVR_QUERY_STANDARD;
    client.longId                     = discAck->extAddr;
    client.version                    = discAck->version;
    client.hwModel                    = discAck->clientHwModel;
    client.qStatus.header.linkQuality = discAck->linkQuality;
    client.qStatus.header.rssi        = discAck->rssiRxPower;
    client.lastCommTimeTicks          = xTaskGetTickCount();
    client.commFailCount            = 0;

    if (isPowerModeOpen())
    {
        registerClient(&client);
        client.userChargeRequested = 1;
    }
    else
    {
        client.state = RCVR_STATE_DISCOVERY;
    }

    SaveClient(client);
}

/**
 * @brief Determines if the system is in 'Open Network' power mode.
 * @return true if we are in power mode, false if otherwise.
 */
static bool isPowerModeOpen(void)
{
    uint8_t val;
    POST_COTA_ERROR(CfgGetParam(CFG_POWER_MODE, &val, sizeof(val)));
    return (bool) val;
}

/**
 * @brief Updates the client in the database from a discover acknowledgment.
 * @param discAck - An acknowledgment message for a discovery
 */
static void updateExistingClientFromDiscovery(PrxHostDiscoveryMsg_t *discAck)
{
    Client_t client;

    if (GetClient(discAck->extAddr, &client))
    {
        // If a registered client resets, it will start responding to
        // discovery messages. It needs to be put in registered state
        // so it can re-join the transmitter.
        if (client.state != RCVR_STATE_DISCOVERY)
        {
            client.state = RCVR_STATE_REGISTERED;

            // If this client was previously in the charging state
            // there is a small window in time where this may get to
            // the charging state before the next TPS cycle.  If the
            // schedule doesn't change from the last TPS cycle, this
            // client will not get a TPS message, so to be safe we
            // increment the tps sequence number here.
            IncrementTpsSeqNum();
        }

        // These are the characteristics that can change after a
        // client reboots or between discovery responses.
        client.version                    = discAck->version;
        client.hwModel                    = discAck->clientHwModel;
        client.qStatus.header.linkQuality = discAck->linkQuality;
        client.qStatus.header.rssi        = discAck->rssiRxPower;
        client.lastCommTimeTicks          = xTaskGetTickCount();
        client.commFailCount            = 0;

        SaveClient(client);
    }
}

/**
 *  To begin the client's journey through all of the states,
 *   the client must decidedly become 'registered' with the device.
 *   Once this process begins, the process should auto magically
 *   get the client into the Ready state through a series of
 *   message exchanges between the client and the proxy.
 */
CotaError_t CmRegisterClient(ExtdAddr_t longId)
{
    Client_t client = {0};

    if (IsLongValid(longId))
    {
        // If the clients was in the discovered clients before
        // registering it, the client must first be already saved in
        // the CM with state RCVR_STATE_DISCOVERY
        if (GetClient(longId, &client))
        {
            if (client.state == RCVR_STATE_DISCOVERY)
            {
                registerClient(&client);
                SaveClient(client);
            }
            else
            {
                return COTA_ERROR_RCVR_ALREADY_REGISTERED;
            }
        }
        else
        {
            return COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
        }
    }

    return COTA_ERROR_NONE;
}

/**
 * @brief Walking function for #CmRegisterAllClients
 *
 * @param client The current client in the walk.
 */
static void RegisterClientWalk(Client_t* client)
{
    if (client->state == RCVR_STATE_DISCOVERY)
    {
        registerClient(client);
        client->qStatus.header.queryType = gQueryTypeForRegister;
    }
}

/**
 * @brief Registers all clients in the discovery state
 */
CotaError_t CmRegisterAllClients(CotaRcvrQuery_t queryType)
{
    gQueryTypeForRegister = queryType;
    Walk(RegisterClientWalk);
    return COTA_ERROR_NONE;
}

/*
  For now, determining the client that receives a join, tpc or query
  message we will simply 'pop' the head off of the tree. In cases of
  unstable communication, this is not a reliable method for send
  messages to each client and something smarter will need to be
  done. If the head doesn't respond, then no other clients within the
  tree will receive messages with this code.

  TODO be smarter, avoid deadlock!
 */

bool CmGetJoinClient(ExtdAddr_t *longId, ShrtAddr_t *shortId)
{
    Client_t client;
    if (longId != NULL && shortId != NULL)
    {
        if (FindClientState(RCVR_STATE_REGISTERED, &client))
        {
            memcpy((void *) longId, (void *)  &(client.longId), sizeof(ExtdAddr_t));
            memcpy((void *) shortId, (void *) &(client.shortId), sizeof(ShrtAddr_t));

            return true;
        }
    }
    return false;
}

/**
 * @brief A walk function designed to find stale unregistered clients
 * @param client A pointer to the client being considered.
 */
static void getOldDiscClientWalk(Client_t* client)
{
    uint16_t discTimeout;

    CfgGetParam(CFG_DISCOVERY_TIMEOUT, &discTimeout, sizeof(discTimeout));

    if ((client->state == RCVR_STATE_DISCOVERY) &&
        TICKS_TO_MS(TimeDiffTicks(xTaskGetTickCount(), client->lastCommTimeTicks)) > SEC_TO_MS(discTimeout))
    {
        gExpiredClient = *client;
    }

}

/**
 * @brief A function designed to find stale unregistered clients by walking the tree
 * @param client A pointer to a stale client.
 * @return true, if stale client found, false if not.
 */
static bool getOldDiscoveredClient(Client_t *client)
{
    bool ret = false;
    gExpiredClient.longId = INVALID_CLIENT_LONGID;

    Walk(getOldDiscClientWalk);

    if (IS_LONGID_VALID(gExpiredClient.longId))
    {
        *client = gExpiredClient;
        ret = true;
    }

    return ret;
}

/**
 * @brief Remove unresponsive discovered clients and designate unresponse
 *        registered clients as silent.
 */
void CmHandleUnresponsiveClients(void)
{
    Client_t client;

    //First remove all stale clients
    while(getOldDiscoveredClient(&client))
    {
        RemoveClient(client.longId);
    }

    //Walk the tree and mark all stale registered clients as silent.
    Walk(markSilentClientsWalk);
    Walk(updateSilentWalk);
}

/**
 * @brief Update clients that have recently responded in the system.
 *
 * @note This function works because clients will drop off the network
 *       i.e. ready to be joined if they haven't communicated with the
 *       transmitter in 2 minutes. This helps us deal with a lot of
 *       corner cases.
 */
static void updateSilentWalk(Client_t *client )
{
    if(client != NULL)
    {
        if (client->state == RCVR_STATE_SILENT)
        {
            if (client->commFailCount == 0)
            {
                LogInfo("Silent Rcvr 0x%016llx has become responsive. It will now be re-registered\r\n",
                        *(uint64_t*)&client->longId);
                client->state = RCVR_STATE_REGISTERED;
            }
        }
    }
}

#endif
