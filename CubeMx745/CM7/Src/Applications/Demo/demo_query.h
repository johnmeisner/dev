/**
 * @file       demo_query.h
 *
 * @brief      Header module for the demo_query.c module
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2021 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#ifndef _DEMO_QUERY_H_
#define _DEMO_QUERY_H_

#if defined(USE_DEMO_BUILD)
  void DoImplementationSpecificQueryCycle(void);

#endif // defined(USE_DEMO_BUILD)

#endif /* _DEMO_QUERY_H_ */