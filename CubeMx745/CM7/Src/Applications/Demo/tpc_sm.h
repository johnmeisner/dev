/**
 * @file       tpc_sm.h
 *
 * @brief      Header module for the TPC state machine modules
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_DEMO_TPC)

#ifndef _TPC_SM_H_
#define _TPC_SM_H_

bool StartTpcTimers(void);
void TpcStateInit(void);

#endif /* _TPC_SM_H_ */

#endif // defined(USE_DEMO_TPC)
