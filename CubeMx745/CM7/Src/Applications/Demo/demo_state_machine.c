/**
 * @file       demo_state_machine.c
 *
 * @brief      Contains the control task state machine skeleton
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 * @details    Below is a graphical representation of the state machine. For descriptions
 *             on each state, see the state declarations below.
 *             
 * @code{.c}
 *                              --------------------
 *                              |                   |
 *                   ---------->|       Init        |
 *                  /           |                   |
 *                 /            --------------------
 *                /                        |
 *               /                         |
 *              /                          |
 *             \/                         \|/
 *     --------------------       --------------------
 *    |                    |     |                   |
 *    |       Debug        |<----|       Ready       |<--> ...To States listed below
 *    |                    |     |                   |
 *     --------------------       --------------------
 *                                        /|\
 *                                         |
 *                                         |
 *                                        \|/
 *                               --------------------
 *                               |                   |
 *                               |       TPS         |
 *                               |                   |
 *                               --------------------
 *
 *
 *
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|       Disc        |
 *                               |                   |
 *                               --------------------  
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|       Join        |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|        TPC        |
 *                               |                   |
 *                               --------------------
 *                                --------------------
 *                               |                   |
 *     From Ready state... <---->|      Query        |
 *                               |                   |
 *                               --------------------
 *                                --------------------
 *                               |                   |
 *     From Ready state... <---->|    Calibration    |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|   Power Monitor   |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|       Fans        |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|  Client Command   |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|    Disconnect     |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|    Light Ring     |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|    App Command    |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|    Proxy Test     |
 *                               |                   |
 *                               --------------------
 * @endcode
 */

#include "orion_config.h"

#if defined (USE_DEMO_BUILD)

#include "state_machine.h"
#include "stddef.h"
#include "demo_state_machine.h"

/**
 * @brief State that indicates an error in the state machine has occurred
 * @todo  All the states should really be declared as "const"
 */
static StateMachineState_t gCtrlError;

/**
 * @brief Puts the transmitter in debug mode. In debug mode, the system disables
 *        all timers and yields more control to the user.
 */
static StateMachineState_t gCtrlDebug;

/**
 * @brief Starts system resources (timers) to prepare the system for the ready state
 */
static StateMachineState_t gCtrlInit;

/**
 * @brief The state of the system when it is ready to service any need based on
 *        flags set. This is the 'center' state that can reach all other states. 
 */
static StateMachineState_t gCtrlRdy;

/**
 * @brief On entry to this state, a discovery signal is sent. An
 *        acknowledgment timeout timer will cause this state to exit.
 */
static StateMachineState_t gCtrlDisc;

/**
 * @brief On entry to this state, a join signal is sent. An
 *        acknowledgment timeout timer will cause this state to exit.
 */
static StateMachineState_t gCtrlJoin;

/**
 * @brief On entry to this state, a TPC message is send. An
 *        acknowledgment timeout timer will cause this state to exit.
 */
static StateMachineState_t gCtrlTpc;

/**
 * @brief On entry to this state, a proxy info command will be sent to the proxy
 *        The state will wait for a response and reset the proxy if it doesn't get one
 */
static StateMachineState_t gCtrlProxyTest;

/**
 * @brief On entry to this state, a query message is sent. An
 *        acknowledgment timeout timer will cause this state to exit.
 */
static StateMachineState_t gCtrlQuery;

/**
 * @brief On entry to this state, commands are sent to power manager to refresh, update, or output
 *        power values
 */
static StateMachineState_t gCtrlPm;

/**
 * @brief On entry to this state, commands are sent to fans to check that they are operable
 */
static StateMachineState_t gCtrlFans;

/**
 * @brief State managing disconnecting client messages
 */
static StateMachineState_t gCtrlDisconnectClient;

/**
 * @brief State managinggclient commands
 */
static StateMachineState_t gCtrlAppCmd;

/**
 * @brief Ensures that the light ring is updated to the correct state
 */
static StateMachineState_t gCtrlLightRingUpdate;

/**
 * @brief This state determines if it's appropriate to calibrate the system. The
 *        system could require a calibration either based on the amount of time that
 *        has passed, or the measured temperature of the system.
 */
static StateMachineState_t gCtrlCalibration;

/**
 * @brief This is the TPS or Charging state of the transmitter. When charging, the
 *        types of messages (task-to-task) is reduced. This architecture prevents
 *        the transmitter from telling the proxy to send a message while the
 *        system is charging clients
 */
static StateMachineState_t gCtrlTps;

/*
 * Error State Definitions 
 */
static StateMachineTransition_t gCtrlErrorTxnOptions[];

static StateMachineState_t gCtrlError =
{
    .state             = CTRL_STATE_ERROR,
    .entryFcn          = ErrorEntry,
    .stateName         = "Control task error",
    .transitionOptions = gCtrlErrorTxnOptions
};

static StateMachineTransition_t gCtrlErrorTxnOptions[] =
{
    {
        .checkCondition = CtrlMachineError,
        .exitFcn        = NULL,
        .nextState      = &gCtrlError
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Init State Definitions
 */
static StateMachineTransition_t gCtrlInitTxnOptions[];

static StateMachineState_t gCtrlInit =
{
    .state             = CTRL_STATE_INIT,
    .entryFcn          = InitEntry,
    .stateName         = "Control task initialization",
    .transitionOptions = gCtrlInitTxnOptions
};

static StateMachineTransition_t gCtrlInitTxnOptions[] =
{
    {
        .checkCondition = InitToDebugCheck,
        .exitFcn        = InitToDebugExit,
        .nextState      = &gCtrlDebug
    },
    {
        .checkCondition = InitToRdyCheck,
        .exitFcn        = InitToRdyExit,
        .nextState      = &gCtrlRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Debug State Definitions
 */
static StateMachineTransition_t gCtrlDebugTxnOptions[];

static StateMachineState_t gCtrlDebug =
{
    .state             = CTRL_STATE_DEBUG,
    .entryFcn          = DebugEntry,
    .stateName         = "Control task debug mode",
    .transitionOptions = gCtrlDebugTxnOptions
};

static StateMachineTransition_t gCtrlDebugTxnOptions[] =
{
    {
        .checkCondition = DebugToInitCheck,
        .exitFcn        = DebugToInitExit,
        .nextState      = &gCtrlInit
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Ready State Definitions
 */
static StateMachineTransition_t gCtrlRdyTxnOptions[];

static StateMachineState_t gCtrlRdy =
{
    .state             = CTRL_STATE_RDY,
    .entryFcn          = ReadyEntry,
    .stateName         = "Control task ready mode",
    .transitionOptions = gCtrlRdyTxnOptions
};

static StateMachineTransition_t gCtrlRdyTxnOptions[] =
{
    {
        .checkCondition = RdyToErrorCheck,
        .exitFcn        = NULL,
        .nextState      = &gCtrlError
    },
    {
        .checkCondition = RdyToDebugCheck,
        .exitFcn        = RdyToDebugExit,
        .nextState      = &gCtrlDebug
    },
    {
        .checkCondition = RdyToCalCheck,
        .exitFcn        = RdyToCalExit,
        .nextState      = &gCtrlCalibration
    },
    {
        .checkCondition = RdyToDiscCheck,
        .exitFcn        = RdyToDiscExit,
        .nextState      = &gCtrlDisc
    },
    {
        .checkCondition = RdyToJoinCheck,
        .exitFcn        = RdyToJoinExit,
        .nextState      = &gCtrlJoin
    },
    {
        .checkCondition = RdyToTpcCheck,
        .exitFcn        = RdyToTpcExit,
        .nextState      = &gCtrlTpc
    },
    {
        .checkCondition = RdyToQueryCheck,
        .exitFcn        = RdyToQueryExit,
        .nextState      = &gCtrlQuery
    },
    {
        .checkCondition = RdyToProxyTestCheck,
        .exitFcn        = RdyToProxyTestExit,
        .nextState      = &gCtrlProxyTest
    },
    {
        .checkCondition = RdyToPmCheck,
        .exitFcn        = NULL,
        .nextState      = &gCtrlPm
    },
    {
        .checkCondition = RdyToFansCheck,
        .exitFcn        = NULL,
        .nextState      = &gCtrlFans
    },
    {
        .checkCondition = RdyToDisconnectClntCheck,     
        .exitFcn        = RdyToDisconnectClntExit,      
        .nextState      = &gCtrlDisconnectClient        
    },
    {
        .checkCondition = RdyToAppCmdCheck,
        .exitFcn        = RdyToAppCmdExit, 
        .nextState      = &gCtrlAppCmd     
    },
    {
        .checkCondition = RdyToLightRingUpdateCheck,
        .exitFcn        = NULL,
        .nextState      = &gCtrlLightRingUpdate
    },
    {
         .checkCondition = RdyToTpsCheck,
         .exitFcn        = RdyToTpsExit,
         .nextState      = &gCtrlTps
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Discovery State Definitions
 */
static StateMachineTransition_t gCtrlDiscTxnOptions[];

static StateMachineState_t gCtrlDisc =
{
    .state             = CTRL_STATE_SEND_DISC,
    .entryFcn          = DiscEntry,
    .stateName         = "Control task sending discovery messages",
    .transitionOptions = gCtrlDiscTxnOptions
};

static StateMachineTransition_t gCtrlDiscTxnOptions[] =
{
    {
        .checkCondition = DiscToRdyCheck,
        .exitFcn        = DiscToRdyExit,
        .nextState      = &gCtrlRdy
    },
    NULL_TRANSITION_STRUCT
};


/*
 * Join State Definitions
 */
static StateMachineTransition_t gCtrlJoinTxnOptions[];

static StateMachineState_t gCtrlJoin =
{
    .state             = CTRL_STATE_SEND_JOIN,
    .entryFcn          = JoinEntry,
    .stateName         = "Control task sending join messages",
    .transitionOptions = gCtrlJoinTxnOptions
};

static StateMachineTransition_t gCtrlJoinTxnOptions[] =
{
    {
        .checkCondition = JoinToRdyCheck,
        .exitFcn        = JoinToRdyExit,
        .nextState      = &gCtrlRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Tpc State Definitions
 */
static StateMachineTransition_t gCtrlTpcTxnOptions[];

static StateMachineState_t gCtrlTpc =
{
    .state             = CTRL_STATE_SEND_TPC,
    .entryFcn          = TpcEntry,
    .stateName         = "Control task sending TPC messages",
    .transitionOptions = gCtrlTpcTxnOptions
};

static StateMachineTransition_t gCtrlTpcTxnOptions[] =
{
    {
        .checkCondition = TpcToRdyCheck,
        .exitFcn        = TpcToRdyExit,
        .nextState      = &gCtrlRdy
    },

    NULL_TRANSITION_STRUCT
};

/*
 * Query State Definitions
 */
static StateMachineTransition_t gCtrlQueryTxnOptions[];

static StateMachineState_t gCtrlQuery =
{
    .state             = CTRL_STATE_SEND_QUERY,
    .entryFcn          = QueryEntry,
    .stateName         = "Control task sending query messages",
    .transitionOptions = gCtrlQueryTxnOptions
};

static StateMachineTransition_t gCtrlQueryTxnOptions[] =
{
    {
        .checkCondition = QueryToRdyCheck,
        .exitFcn        = QueryToRdyExit,
        .nextState      = &gCtrlRdy
    },
    NULL_TRANSITION_STRUCT
};


/*
 * Proxy test State definitions
 */
static StateMachineTransition_t gCtrlProxyTestTxnOptions[];

static StateMachineState_t gCtrlProxyTest =
{
    .state             = CTRL_STATE_PROXY_TEST,
    .entryFcn          = ProxyTestEntry,
    .stateName         = "Test the proxy to see if it's ok",
    .transitionOptions = gCtrlProxyTestTxnOptions
};

static StateMachineTransition_t gCtrlProxyTestTxnOptions[] =
{
    {
        .checkCondition = ProxyTestToRdyCheck,
        .exitFcn        = ProxyTestToRdyExit,
        .nextState      = &gCtrlRdy
    },

    NULL_TRANSITION_STRUCT
};



static StateMachineTransition_t gCtrlPmTxnOptions[];

/**
 * @brief  The following control task state manages power monitor readings
 */
static StateMachineState_t gCtrlPm =
{
    .state             = CTRL_STATE_PM,
    .entryFcn          = PmEntry,
    .stateName         = "Control task sending power manager messages",
    .transitionOptions = gCtrlPmTxnOptions
};

static StateMachineTransition_t gCtrlPmTxnOptions[] =
{
    {
        .checkCondition = PmToRdyCheck,
        .exitFcn        = NULL,
        .nextState      = &gCtrlRdy
    },
    NULL_TRANSITION_STRUCT
};

static StateMachineTransition_t gCtrlFansTxnOptions[];

/**
 * @brief  The following control state checks and manages the fans
 */
static StateMachineState_t gCtrlFans =
{
    .state             = CTRL_STATE_FAN_CTRL,
    .entryFcn          = FansEntry,
    .stateName         = "Control state checking fans",
    .transitionOptions = gCtrlFansTxnOptions
};

static StateMachineTransition_t gCtrlFansTxnOptions[] =
{
    {
        .checkCondition = FansToRdyCheck,
        .exitFcn        = NULL,
        .nextState      = &gCtrlRdy
    },
    NULL_TRANSITION_STRUCT
};

static StateMachineTransition_t gCtrlDisconnectClientOptions[];

/**
 * @brief  The following states manages the rate at which clients are disconnected
 */
static StateMachineState_t gCtrlDisconnectClient =
{
    .state             = CTRL_STATE_DISCONNECT_RCVR,         
    .entryFcn          = DisconnectClntEntry,                  
    .stateName         = "State managing clients disconnecting", 
    .transitionOptions = gCtrlDisconnectClientOptions            
};

static StateMachineTransition_t gCtrlDisconnectClientOptions[] =
{
    {
        .checkCondition = DisconnectClntToRdyCheck,          
        .exitFcn        = DisconnectClntToRdyExit,           
        .nextState      = &gCtrlRdy                     
    },
    NULL_TRANSITION_STRUCT
};

static StateMachineTransition_t gCtrlAppCmdOptions[];  

/**
 * @brief  The following states manages the rate at which send client commands
 */
static StateMachineState_t gCtrlAppCmd =
{
    .state             = CTRL_STATE_RCVR_CMD,        
    .entryFcn          = AppCmdEntry,                  
    .stateName         = "State managing client commands",
    .transitionOptions = gCtrlAppCmdOptions               
};

static StateMachineTransition_t gCtrlAppCmdOptions[] =
{
    {
        .checkCondition = AppCmdToRdyCheck,          
        .exitFcn        = AppCmdToRdyExit,           
        .nextState      = &gCtrlRdy                  
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Calibration State Definitions
 */
static StateMachineTransition_t gCtrlCalTxnOptions[];

static StateMachineState_t gCtrlCalibration =
{
    .state             = CTRL_STATE_CAL,
    .entryFcn          = CalEntry,
    .stateName         = "Control task calibrating system",
    .transitionOptions = gCtrlCalTxnOptions
};

static StateMachineTransition_t gCtrlCalTxnOptions[] =
{
    {
        .checkCondition = CalToRdyCheck,
        .exitFcn        = CalToRdyExit,
        .nextState      = &gCtrlRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * TPS (Charging) State Definitions
 */
static StateMachineTransition_t gCtrlTpsTxnOptions[];

static StateMachineState_t gCtrlTps =
{
    .state             = CTRL_STATE_TPS,
    .entryFcn          = TpsEntry,
    .stateName         = "Control task charging clients",
    .transitionOptions = gCtrlTpsTxnOptions
};

static StateMachineTransition_t gCtrlTpsTxnOptions[] =
{
    {
        .checkCondition = TpsDoneToRdyCheck,
        .exitFcn        = TpsDoneToRdyExit,
        .nextState      = &gCtrlRdy
    },
    {
        .checkCondition = TpsTimeoutToRdyCheck,
        .exitFcn        = TpsTimeoutToRdyExit,
        .nextState      = &gCtrlRdy
    },
    NULL_TRANSITION_STRUCT
};

static StateMachineTransition_t gCtrlLightRingUpdateOptions[];

static StateMachineState_t gCtrlLightRingUpdate =
{
    .state             = CTRL_STATE_LIGHT_RING_UPDATE,
    .entryFcn          = LightRingUpdateEntry,
    .stateName         = "Control state for updating the light ring",
    .transitionOptions = gCtrlLightRingUpdateOptions
};

static StateMachineTransition_t gCtrlLightRingUpdateOptions[] =
{
    {
        .checkCondition = LightRingUpdateToRdyCheck,
        .exitFcn        = NULL,
        .nextState      = &gCtrlRdy
    },
    NULL_TRANSITION_STRUCT
};

__weak void ErrorEntry           (void){ return; }
__weak void DebugEntry           (void){ return; }
__weak void InitEntry            (void){ return; }
__weak void ReadyEntry           (void){ return; }
__weak void TpsEntry             (void){ return; }
__weak void DiscEntry            (void){ return; }
__weak void TpcEntry             (void){ return; }
__weak void ProxyTestEntry       (void){ return; }
__weak void JoinEntry            (void){ return; }
__weak void QueryEntry           (void){ return; }
__weak void FansEntry            (void){ return; }
__weak void CalEntry             (void){ return; }
__weak void AppCmdEntry          (void){ return; }
__weak void DisconnectClntEntry  (void){ return; }
__weak void LightRingUpdateEntry (void){ return; }

// By having the error return true, the error state works a non-blocking while(1) loop
__weak bool CtrlMachineError            (void){ return true; }

__weak bool InitToRdyCheck              (void){ return false; }
__weak bool InitToDebugCheck            (void){ return false; }
__weak bool DebugToInitCheck            (void){ return false; }
__weak bool RdyToErrorCheck             (void){ return false; }
__weak bool RdyToDebugCheck             (void){ return false; }
__weak bool RdyToTpsCheck               (void){ return false; }
__weak bool RdyToDiscCheck              (void){ return false; }
__weak bool RdyToJoinCheck              (void){ return false; }
__weak bool RdyToTpcCheck               (void){ return false; }
__weak bool RdyToProxyTestCheck         (void){ return false; }
__weak bool RdyToQueryCheck             (void){ return false; }
__weak bool RdyToPmCheck                (void){ return false; }
__weak bool RdyToFansCheck              (void){ return false; }
__weak bool RdyToCalCheck               (void){ return false; }
__weak bool RdyToSchedCheck             (void){ return false; }
__weak bool RdyToLightRingUpdateCheck   (void){ return false; }
__weak bool TpsDoneToRdyCheck           (void){ return false; }
__weak bool TpsTimeoutToRdyCheck        (void){ return false; }
__weak bool DiscToRdyCheck              (void){ return false; }
__weak bool JoinToRdyCheck              (void){ return false; }
__weak bool TpcToRdyCheck               (void){ return false; }
__weak bool ProxyTestToRdyCheck         (void){ return false; }
__weak bool QueryToRdyCheck             (void){ return false; }
__weak bool FansToRdyCheck              (void){ return false; }
__weak bool CalToRdyCheck               (void){ return false; }
__weak bool SchedToRdyCheck             (void){ return false; }
__weak bool AppCmdToRdyCheck            (void){ return false; }
__weak bool RdyToDisconnectClntCheck    (void){ return false; }
__weak bool DisconnectClntToRdyCheck    (void){ return false; }
__weak bool LightRingUpdateToRdyCheck   (void){ return false; }

__weak void InitToRdyExit           (void){ return; }
__weak void InitToDebugExit         (void){ return; }
__weak void DebugToInitExit         (void){ return; }
__weak void RdyToDebugExit          (void){ return; }
__weak void RdyToTpsExit            (void){ return; }
__weak void RdyToDiscExit           (void){ return; }
__weak void RdyToJoinExit           (void){ return; }
__weak void RdyToTpcExit            (void){ return; }
__weak void RdyToProxyTestExit      (void){ return; }
__weak void RdyToQueryExit          (void){ return; }
__weak void RdyToCalExit            (void){ return; }
__weak void RdyToSchedExit          (void){ return; }
__weak void RdyToAppCmdExit         (void){ return; }
__weak void TpsDoneToRdyExit        (void){ return; }
__weak void TpsTimeoutToRdyExit     (void){ return; }
__weak void DiscToRdyExit           (void){ return; }
__weak void JoinToRdyExit           (void){ return; }
__weak void TpcToRdyExit            (void){ return; }
__weak void ProxyTestToRdyExit      (void){ return; }
__weak void QueryToRdyExit          (void){ return; }
__weak void CalToRdyExit            (void){ return; }
__weak void SchedToRdyExit          (void){ return; }
__weak void AppCmdToRdyExit         (void){ return; }
__weak void RdyToDisconnectClntExit (void){ return; }
__weak void DisconnectClntToRdyExit (void){ return; }

StateMachineState_t *CtrlSmInit(void)
{
    return &gCtrlInit;
}

#endif // defined(USE_DEMO_BUILD)
