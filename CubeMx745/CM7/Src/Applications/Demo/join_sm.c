/**
 * @file       join_sm.c
 *
 * @brief      Defines the Join state machine functions for the demo system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "demo_state_machine.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"

#if defined(USE_DEMO_JOIN)
#include "Demo/demo_client_interface.h"

TimerHandle_t  gJoinTimerHandle                      = NULL;     ///< FreeRTOS timer handle for the join timer
StaticTimer_t  gJoinTimerBuffer;                                 ///< FreeRTOS timer buffer for the join timer
TimerHandle_t  gJoinAckTimerHandle                   = NULL;     ///< FreeRTOS timer handle for the join acknowledgment timer
StaticTimer_t  gJoinAckTimerBuffer;                              ///< FreeRTOS timer buffer for the join acknowledgment timer

volatile bool gSendJoin         = false;       ///< true when a join message must be sent
volatile bool gJoinAckTimeout   = false;       ///< true when an join acknowledgment was not received before timeout

static void joinTimerCallback(TimerHandle_t timer);
static void ackJoinTimerCallback(TimerHandle_t timer);

void JoinStateInit(void)
{
    uint16_t timerVal;
    
    CfgGetParam(CFG_JOIN_PERIOD, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gJoinTimerHandle)
        {
            xTimerStop(gJoinTimerHandle, NO_WAIT);
            xTimerChangePeriod( gJoinTimerHandle, SEC_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gJoinTimerHandle = xTimerCreateStatic(
                "Join Timer",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                joinTimerCallback,
                &gJoinTimerBuffer);
        }
    
    }
    else
    {
        gJoinTimerHandle = NULL;
    }

    CfgGetParam(CFG_JOIN_WAIT, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gJoinAckTimerHandle)
        {
            xTimerStop(gJoinAckTimerHandle, NO_WAIT);
            xTimerChangePeriod(gJoinAckTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gJoinAckTimerHandle = xTimerCreateStatic(
                "Join Acknowledgment Timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                ackJoinTimerCallback,
                &gJoinAckTimerBuffer);
        }
    }
    else
    {
        gJoinAckTimerHandle = NULL;      
    }

    if ((!gJoinTimerHandle) ||
        (!gJoinAckTimerHandle))
    {
        LogFatal("Join timers failed to initialize\r\n");
    }
}

bool StartJoinTimers(void)
{
    return xTimerStart( gJoinTimerHandle, NO_WAIT ) == pdPASS;
}

void JoinEntry(void)
{
    ExtdAddr_t longId;
    ShrtAddr_t shortId;

    LogDebug("The Control task has entered the join state.\r\n");
    
    if (CmGetJoinClient(&longId, &shortId))
    {
        PrxSendJoin(longId, shortId);
        LogInfo("Sending Join Message to 0x%016llx to have shortId 0x%04x\r\n",
                *(uint64_t *) &longId,
                shortId);

        gJoinAckTimeout = false;
        TryToStartTimer(gJoinAckTimerHandle);
    }
    else
    {   
        //If we don't send a join message, no need to wait.
        gJoinAckTimeout = true;
    }
}

bool RdyToJoinCheck (void){ return gSendJoin && PrxIsReady(); }
bool JoinToRdyCheck (void){ return gJoinAckTimeout; }

void JoinToRdyExit   (void)
{
    gSendJoin = false;
    
    TryToStopTimer(gJoinAckTimerHandle);
    TryToStartTimer(gJoinTimerHandle);
}

/**
 * @brief This function is called when the join timer finishes. It will set the
 *        #gSendJoin flag and post to the ctrl semaphore that it is time to send
 *        a join message.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void joinTimerCallback(TimerHandle_t timer)
{
    gSendJoin = true;
}

/**
 * @brief The join acknowledgment timer is called after a join message is sent.
 *        If an acknowledgment message is not received before this timer expires, then
 *        the join message has timed out. 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void ackJoinTimerCallback(TimerHandle_t  timer)
{
    gJoinAckTimeout = true;
}

#endif // defined(USE_DEMO_JOIN)
