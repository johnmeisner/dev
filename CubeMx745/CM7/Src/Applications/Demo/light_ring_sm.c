/**
 * @file       light_ring_sm.c
 *
 * @brief      Defines the Light Ring state machine functions for the demo system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "demo_state_machine.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"
#include "light_ring_interface.h"


#if defined(USE_DEMO_LIGHT_RING)

TimerHandle_t  gLightRingUpdateTimerHandle           = NULL;     ///< FreeRTOS timer handle for the LightRingUpdate timeout timer
StaticTimer_t  gLightRingUpdateTimerBuffer;                      ///< FreeRTOS timer buffer for the LightRingUpdate timeout timer

volatile bool gLightRingUpdateNeeded = false;  ///< Indicates that the light ring needs to be checked for an update

static void lightRingUpdateTimerCallback(TimerHandle_t  timer);

bool StartLightRingTimers(void)
{
    return xTimerStart( gLightRingUpdateTimerHandle, NO_WAIT ) == pdPASS;
}

void LightRingStateInit(void)
{
    uint16_t timerVal;

    CfgGetParam(CFG_LIGHT_RING_UPDATE_PERIOD, &timerVal, sizeof(timerVal));

    if (gLightRingUpdateTimerHandle)
    {
        xTimerStop(gLightRingUpdateTimerHandle, NO_WAIT);
        xTimerChangePeriod(gLightRingUpdateTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
    }
    else
    {
        if (timerVal > 0)
        {
            gLightRingUpdateTimerHandle = xTimerCreateStatic(
                "Light Ring Update Timer",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                lightRingUpdateTimerCallback,
                &gLightRingUpdateTimerBuffer);
        }
        else
        {
            gLightRingUpdateTimerHandle = NULL;
        }
    }

    if ((!gLightRingUpdateTimerHandle))
    {
        LogFatal("Light ring timers failed to initialize\r\n");
    }
}

#if defined(USE_DEMO_BUILD)
void LightRingUpdateEntry(void)
{
    gLightRingUpdateNeeded = false;

    // The system behavior is largely driven by the states of the
    // client. These states will change over time as communication
    // occurs over time between the clients and the transmitter. As
    // this communication happens asynchronously, we monitor the
    // system status through the state of the clients (e.x. if a
    // client is in state RCVR_STATE_CHARGING, the transmitter will
    // see this and we will enter TPS if allowed). So, we can set the
    // light ring based on the states of the clients.

    // Also note this function uses if/else logic for implied priority.

    // There are following exceptions to this behavior pattern.
    //   1. Calibration (LR_STATE_CALIB, ctrl task state ctrlCalibration)
    //   2. Identification (LR_STATE_IDENT, ctrl api msgId CTRL_IDENTIFY_CHARGER)
    //   3. Error (LR_STATE_ERROR)
    //   4. Debug/Pause (LR_STATE_DEBUG, ctrl api msgId CTRL_PAUSE)
    //   5. Idle (LR_STATE_DEBUG, its the chosen status when exiting debug with CTRL_RUN)
    // These commands are event based. The light ring must update
    //   immediately to answer the event.

    // Transmitter Status: Wait for Clients
    // If we are not communicating with any clients, we update the
    //   light ring to this status. This occurs when theres no
    //   discovered clients, and all the clients are silent (failing to
    //   respond to queries). We check for discovery, ready, and
    //   charging clients because each of these clients have regular,
    //   periodic communication.

    if ( (!CmDoClientsWithThisStateExist(RCVR_STATE_DISCOVERY)) ||
         (!CmDoClientsWithThisStateExist(RCVR_STATE_READY)) ||
         (!CmDoClientsWithThisStateExist(RCVR_STATE_CHARGING)) )
    {
        // Tell the proxy that we do not see communication with clients.
        LRLightRingUpdate(LR_STATE_WAIT);
    }

    // Transmitter Status: Charging
    // If there is a client that exists that is 'charging', then we
    //   will update the light ring.
    else if (CmDoClientsWithThisStateExist(RCVR_STATE_CHARGING))
    {
        LRLightRingUpdate(LR_STATE_TX);
    }

    // Transmitter Status: Ready
    // This means we are ready to begin charging. In the case where
    //   there are no clients, status 'Wait for Client' should take
    //   precedence.
    else
    {
        // Tell the proxy that charging is possible.
        LRLightRingUpdate(LR_STATE_READY);
    }

    TryToStartTimer(gLightRingUpdateTimerHandle);
}
#elif defined(USE_FOREVER_TRACKER_BUILD)
void LightRingUpdateEntry(void)
{
    //NOT IMPLEMENTED
    //@todo could set the light ring to error condition
}
#else
    #error Undefined build target!
#endif

bool RdyToLightRingUpdateCheck(void) { return gLightRingUpdateNeeded && PrxIsReady(); }
bool LightRingUpdateToRdyCheck(void) { return true; }

static void lightRingUpdateTimerCallback(TimerHandle_t  timer)
{
    gLightRingUpdateNeeded = true;
}

#endif // defined(USE_DEMO_LIGHT_RING)
