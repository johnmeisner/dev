/**
 * @file       disc_sm.h
 *
 * @brief      Header module for the discovery state machine modules
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_DEMO_DISC)

#ifndef _DISC_SM_H_
#define _DISC_SM_H_

bool StartDiscoveryTimers(void);
void DiscStateInit(void);
void ScheduleDiscoveryOnChannel(uint8_t commCh);

#endif /* _DISC_SM_H_ */

#endif // defined(USE_DEMO_DISC)
