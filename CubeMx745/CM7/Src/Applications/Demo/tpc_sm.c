/**
 * @file       tpc_sm.c
 *
 * @brief      Defines the TPC state machine functions for the demo system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"

#if defined(USE_DEMO_TPC)
#include "Demo/demo_client_interface.h"

TimerHandle_t  gTpcTimerHandle                       = NULL;     ///< FreeRTOS timer handle for the tpc timer
StaticTimer_t  gTpcTimerBuffer;                                  ///< FreeRTOS timer buffer for the tpc timer
TimerHandle_t  gTpcAckTimerHandle                    = NULL;     ///< FreeRTOS timer handle for the tpc acknowledgment timer
StaticTimer_t  gTpcAckTimerBuffer;                               ///< FreeRTOS timer buffer for the tpc acknowledgment timer

volatile bool gSendTpc          = false;       ///< true when a tpc message must be sent
volatile bool gTpcAckTimeout    = false;       ///< true when an tpc acknowledgment was not received before timeout

static void tpcTimerCallback(TimerHandle_t timer);
static void ackTpcTimerCallback(TimerHandle_t  timer);

void TpcStateInit(void)
{
    uint16_t timerVal;
    
    CfgGetParam(CFG_TPC_PERIOD, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gTpcTimerHandle)
        {
            xTimerStop(gTpcTimerHandle, NO_WAIT);
            xTimerChangePeriod(gTpcTimerHandle, SEC_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gTpcTimerHandle = xTimerCreateStatic(
                "Tpc Timer",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                tpcTimerCallback,
                &gTpcTimerBuffer);
        }
    }
    else
    {
       gTpcTimerHandle = NULL;
    }

    CfgGetParam(CFG_TPC_WAIT, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gTpcAckTimerHandle)
        {
            xTimerStop(gTpcAckTimerHandle, NO_WAIT);
            xTimerChangePeriod(gTpcAckTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }      
        else
        {
            gTpcAckTimerHandle = xTimerCreateStatic(
                "Tpc Acknowledgment Timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                ackTpcTimerCallback,
                &gTpcAckTimerBuffer);
        }
    }
    else
    {
       gTpcAckTimerHandle = NULL;
    }

    if ((!gTpcTimerHandle) ||
        (!gTpcAckTimerHandle))
    {
        LogFatal("TPC timers failed to initialize\r\n");
    }
}

void TpcEntry(void)
{
    ShrtAddr_t shortId;

    LogDebug("The Control task has entered the TPC state.\r\n");
    
    if (CmGetTpcClient(&shortId))
    {
        PrxSendTpc(shortId);
        LogInfo("Sending TPC Message to client with shortId 0x%04x\r\n",
                shortId);

        // Begin the tpc timer
        gTpcAckTimeout = false;
        TryToStartTimer(gTpcAckTimerHandle);
    }
    else
    {
        //If we don't send a TPC message, no need to wait.
        gTpcAckTimeout = true;      
    }
}

bool StartTpcTimers(void)
{
    return xTimerStart( gTpcTimerHandle, NO_WAIT ) == pdPASS;
}

bool RdyToTpcCheck (void){ return gSendTpc && PrxIsReady(); }
bool TpcToRdyCheck (void){ return gTpcAckTimeout; }

void TpcToRdyExit   (void)
{
    gSendTpc = false;
    
    TryToStopTimer(gTpcAckTimerHandle);
    TryToStartTimer(gTpcTimerHandle);
}

/**
 * @brief This function is called when the tpc timer finishes. It will set the
 *        #gSendTpc flag and post to the ctrl semaphore that it is time to send
 *        a tpc message.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void tpcTimerCallback(TimerHandle_t timer)
{
    gSendTpc = true;
}

/**
 * @brief The tpc acknowledgment timer is called after a tpc message is sent.
 *        If an acknowledgment message is not received before this timer expires, then
 *        the tpc message has timed out. 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void ackTpcTimerCallback(TimerHandle_t  timer)
{
    gTpcAckTimeout = true;
}

#endif // defined(USE_DEMO_TPC)
