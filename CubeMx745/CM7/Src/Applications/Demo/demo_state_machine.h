/**
 * @file       demo_state_machine.h
 *
 * @brief      Header file for the state machine for the control task
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#if defined (USE_DEMO_BUILD)

#ifndef _DEMO_STATE_MACHINE_H_
#define _DEMO_STATE_MACHINE_H_

#include <stdbool.h>
#include "state_machine.h"
/**
 * @brief An enum that denotes what state is being defined in state definitions
 */
typedef enum _CtrlStates_t
{
    CTRL_STATE_ERROR = 0,           ///< Denotes a error state for the control task 
    CTRL_STATE_DEBUG,               ///< Give the user more intimate control of the system. We cannot charging clients in this state
    CTRL_STATE_INIT,                ///< The transmitter is initializing its hardware and software resources. 
    CTRL_STATE_RDY,                 ///< The transmitter is ready to begin TPS. All communication channels are handled in this task
    CTRL_STATE_SEND_DISC,           ///< Send discovery message to clients that are in range. 
    CTRL_STATE_SEND_TPC,            ///< Send TPC messages to all clients that are joined. 
    CTRL_STATE_SEND_JOIN,           ///< Send join messages to clients that have been registered to ensure they have properly joined the network
    CTRL_STATE_SEND_QUERY,          ///< Send queries to a select number of clients to update their information in the transmitter database.
    CTRL_STATE_CAL,                 ///< Check, based on software timers, if an adequate amount of time and temperature drift calibrate the transmitter. 
    CTRL_STATE_TPS,                 ///< The transmitter is charging clients. The functionality of the transmitter is reduced for operation critical tasks.
    CTRL_STATE_PM,                  ///< State used monitor power consumption
    CTRL_STATE_FAN_CTRL,            ///< State used to control fans and make sure they are working
    CTRL_STATE_DISCONNECT_RCVR,     ///< State managing Receiver disconnects
    CTRL_STATE_RCVR_CMD,            ///< State managing Receiver commands
    CTRL_STATE_LIGHT_RING_UPDATE,   ///< State for updating the light ring (if needed)
    CTRL_STATE_PROXY_TEST,          ///< State tests the proxy to see if it's alive.
} CtrlStates_t;


/*
 * State Machine Function Prototypes
 */
void ErrorEntry             (void);
void DebugEntry             (void);
void InitEntry              (void);
void ReadyEntry             (void);
void DiscEntry              (void);
void JoinEntry              (void);
void TpcEntry               (void);
void ProxyTestEntry         (void);
void QueryEntry             (void);
void PmEntry                (void);
void FansEntry              (void);
void CalEntry               (void);
void TpsEntry               (void);
void AppCmdEntry            (void);
void DisconnectClntEntry    (void);
void LightRingUpdateEntry   (void);

bool CtrlMachineError (void);

bool InitToRdyCheck             (void);
bool InitToDebugCheck           (void);
bool DebugToInitCheck           (void);
bool RdyToErrorCheck            (void);
bool RdyToDebugCheck            (void);
bool RdyToTpsCheck              (void);
bool RdyToDiscCheck             (void);
bool RdyToJoinCheck             (void);
bool RdyToTpcCheck              (void);
bool RdyToProxyTestCheck        (void);
bool RdyToQueryCheck            (void);
bool RdyToPmCheck               (void);
bool RdyToFansCheck             (void);
bool RdyToCalCheck              (void);
bool RdyToLightRingUpdateCheck  (void);
bool DiscToRdyCheck             (void);
bool JoinToRdyCheck             (void);
bool TpcToRdyCheck              (void);
bool ProxyTestToRdyCheck        (void);
bool QueryToRdyCheck            (void);
bool PmToRdyCheck               (void);
bool FansToRdyCheck             (void);
bool CalToRdyCheck              (void);
bool TpsDoneToRdyCheck          (void);
bool TpsTimeoutToRdyCheck       (void);
bool RdyToAppCmdCheck           (void);
bool AppCmdToRdyCheck           (void);
bool RdyToDisconnectClntCheck   (void);
bool DisconnectClntToRdyCheck   (void);
bool LightRingUpdateToRdyCheck  (void);

void InitToRdyExit              (void);
void InitToDebugExit            (void);
void DebugToInitExit            (void);
void RdyToDebugExit             (void);
void RdyToTpsExit               (void);
void RdyToDiscExit              (void);
void RdyToDebugExit             (void);
void RdyToTpsExit               (void);
void RdyToDiscExit              (void);
void RdyToJoinExit              (void);
void RdyToTpcExit               (void);
void RdyToProxyTestExit         (void);
void RdyToQueryExit             (void);
void RdyToCalExit               (void);
void TpsDoneToRdyExit           (void);
void TpsTimeoutToRdyExit        (void);
void DiscToRdyExit              (void);
void JoinToRdyExit              (void);
void TpcToRdyExit               (void);
void ProxyTestToRdyExit         (void);
void QueryToRdyExit             (void);
void CalToRdyExit               (void);
void RdyToAppCmdExit            (void);
void AppCmdToRdyExit            (void);
void RdyToDisconnectClntExit    (void);
void DisconnectClntToRdyExit    (void);

StateMachineState_t *CtrlSmInit(void);

#endif /* #ifndef _DEMO_STATE_MACHINE_H_ */

#endif // defined (USE_DEMO_BUILD)
