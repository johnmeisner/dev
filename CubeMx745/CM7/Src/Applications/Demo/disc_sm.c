/**
 * @file       disc_sm.c
 *
 * @brief      Defines the Discovery state machine functions for the demo system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "demo_state_machine.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"


#if defined(USE_DEMO_DISC)
 
TimerHandle_t  gDiscTimerHandle                      = NULL;     ///< FreeRTOS timer handle for the discover timer
StaticTimer_t  gDiscTimerBuffer;                                 ///< FreeRTOS timer buffer for the discover timer
TimerHandle_t  gDiscAckTimerHandle                   = NULL;     ///< FreeRTOS timer handle for the disc acknowledgment timer
StaticTimer_t  gDiscAckTimerBuffer;                              ///< FreeRTOS timer buffer for the disc acknowledgment timer

uint8_t  gDiscFreq              = 0;           ///< The discover frequency for discovery messages. 0 for config channel frequency
volatile bool gSendDisc         = false;       ///< true when a discovery message must be sent
volatile bool gDiscAckTimeout   = false;       ///< true when an disc acknowledgment was not received before timeout

static void discTimerCallback(TimerHandle_t timer);
static void ackDiscTimerCallback(TimerHandle_t timer);

/**
 * @brief Schedules the state machine to send a discovery message when
 *        it is able to.
 *
 *        Zero is a special value that indicates use of the current
 *        system communication.
 * @param commCh - the communication ch the discovery should be sent on. 0 indicates default ch.
 */
void ScheduleDiscoveryOnChannel(uint8_t commCh)
{
    gSendDisc = true;
                
    // If channel is non-zero, we'll send the discovery
    // message at a later point at that selected channel
    gDiscFreq = commCh;
}


void DiscStateInit(void)
{
    uint16_t timerVal;
    
    CfgGetParam(CFG_DISCOVERY_PERIOD, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gDiscTimerHandle)
        {
            xTimerStop(gDiscTimerHandle, NO_WAIT);
            xTimerChangePeriod( gDiscTimerHandle, SEC_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gDiscTimerHandle = xTimerCreateStatic(
                "Discovery Timer",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                discTimerCallback,
                &gDiscTimerBuffer);
        }
    }
    else
    {
        gDiscTimerHandle = NULL;
    }
          
    CfgGetParam(CFG_DISCOVERY_WAIT, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gDiscAckTimerHandle)
        {
            xTimerStop(gDiscAckTimerHandle, NO_WAIT);
            xTimerChangePeriod( gDiscAckTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gDiscAckTimerHandle = xTimerCreateStatic(
                "Disc Acknowledgment Timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                ackDiscTimerCallback,
                &gDiscAckTimerBuffer);
        }
    }
    else
    {
        gDiscAckTimerHandle = NULL;
    }

    if((!gDiscTimerHandle) ||
       (!gDiscAckTimerHandle))
    {
        LogFatal("Discovery timers failed to initialize\r\n");
    }
}

void DiscEntry(void)
{
    LogInfo("The Control task has entered the discovery state.\r\n");
    
    // If gOptionalDiscFreq is 0, then the discovery message will be
    //   sent on the same channels as is set via system configuration
    PrxSendDisc(gDiscFreq);

    // Set gDiscFreq to 0 so the next time we send a discovery
    //   message, we do so on the system channel frequency
    gDiscFreq = 0;

    // Begin the disc timer
    gDiscAckTimeout = false;

    TryToStartTimer(gDiscAckTimerHandle);
}


bool StartDiscoveryTimers(void)
{
    return xTimerStart( gDiscTimerHandle, NO_WAIT ) == pdPASS;
}

bool RdyToDiscCheck (void){ return gSendDisc && PrxIsReady(); }
bool DiscToRdyCheck (void){ return gDiscAckTimeout; }

void DiscToRdyExit   (void)
{
    gSendDisc = false;

    TryToStopTimer(gDiscAckTimerHandle);
    TryToStartTimer(gDiscTimerHandle);
}


/**
 * @brief This function is called when the discovery timer finishes. It will set the
 *        #gSendDisc flag and post to the ctrl semaphore that it is time to send
 *        a discovery.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void discTimerCallback(TimerHandle_t timer)
{
    gSendDisc = true;
}

/**
 * @brief The discovery acknowledgment timer is called after a discovery message is sent.
 *        If an acknowledgment message is not received before this timer expires, then
 *        the discovery message has timed out. 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void ackDiscTimerCallback(TimerHandle_t  timer)
{
    gDiscAckTimeout = true;
}

#endif // defined(USE_DEMO_DISC)
