/**
 * @file       join_sm.h
 *
 * @brief      Header module for the join state machine modules
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_DEMO_JOIN)

#ifndef _JOIN_SM_H_
#define _JOIN_SM_H_

void JoinStateInit(void);
bool StartJoinTimers(void);

#endif /* _JOIN_SM_H_ */

#endif // defined(USE_DEMO_JOIN)
