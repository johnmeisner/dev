/**
 * @file       disconnect_sm.c
 *
 * @brief      Defines the Disconnect state machine functions for the demo system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "demo_state_machine.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"

#if defined(USE_DEMO_DISCONNECT)
#include "Demo/demo_client_interface.h"

TimerHandle_t  gDisconnectClientTimeoutTimerHandle   = NULL;     ///< FreeRTOS timer handle for the disconnect client timeout timer
StaticTimer_t  gDisconnectClientTimeoutTimerBuffer;              ///< FreeRTOS timer buffer for the disconnect client timeout timer

bool gDisconnectClientTimeout   = false;       ///< Indicates the disconnect client timeout has occurred

static void disconnectClientTimeoutTimerCallback(TimerHandle_t  timer);

void DisconnectStateInit(void)
{
    uint16_t timerVal;
    
    CfgGetParam(CFG_DISC_CLIENT_TIMEOUT, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gDisconnectClientTimeoutTimerHandle)
        {
            xTimerStop(gDisconnectClientTimeoutTimerHandle, NO_WAIT);
            xTimerChangePeriod(gDisconnectClientTimeoutTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gDisconnectClientTimeoutTimerHandle = xTimerCreateStatic(
                "Disconnect client timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                disconnectClientTimeoutTimerCallback,
                &gDisconnectClientTimeoutTimerBuffer);
        }
    }
    else
    {
        gDisconnectClientTimeoutTimerHandle = NULL;
    }

    if (!gDisconnectClientTimeoutTimerHandle)
    {
        LogFatal("Disconnect timers failed to initialize\r\n");
    }
}

void DisconnectClntEntry(void)
{
    uint16_t maxClientToDisc = 0;

    LogDebug("The Control task has entered the disconnect state.\r\n");
    
    gDisconnectClientTimeout = false;
    TryToStartTimer(gDisconnectClientTimeoutTimerHandle);
    
    CfgGetParam(CFG_DISC_CLIENT_MAX_SEND, &maxClientToDisc, sizeof(maxClientToDisc));
    
    POST_COTA_ERROR(CmRemoveClients(maxClientToDisc));
}


bool DisconnectClntToRdyCheck (void){ return gDisconnectClientTimeout; }

bool RdyToDisconnectClntCheck(void)
{
    Client_t client;
    return CmGetDisconnectClient(&client) && PrxIsReady();
}

static void disconnectClientTimeoutTimerCallback(TimerHandle_t  timer)
{
    gDisconnectClientTimeout = true;
}

#endif // defined(USE_DEMO_DISCONNECT)
