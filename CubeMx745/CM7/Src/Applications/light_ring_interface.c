/**
 * @file       light_ring_interface.c 
 *
 * @brief      This interface for modifying the light ring display for
 *             the Orion system, thereby conveying system status to
 *             the user. The light ring is a LED ring that indicates
 *             basic system behavior to the user.
 *
 *             This module sends messages to the proxy MCU with system
 *             status information that will tell the proxy what
 *             pattern to display on the light ring.
 *            
 *             Because of connectivity limitations on the Orion MCU
 *             (We're using all 6 SPI ports for heaven's sake!), the
 *             Light ring (a load intensive spi-based peripheral) has
 *             been offloaded to the Sensor Controller on the proxy
 *             MCU. We will control the state of the light ring by
 *             sending proxy messages via the proxy message interface
 *             with information about the current status of the
 *             transmitter.
 *            
 *             For better or for worse, we are following the behavior
 *             of Venus in this module. That is to say, each status
 *             represented in this module represents a Daemon state in
 *             the Venus project.
 *            
 *             Prior to this module, the notion of 'state' already
 *             existed in Orion from the ctrl_task state
 *             machine. Venus's concept of state lives separately from
 *             its state machine (a global system state, and a state
 *             of the state machine, two different concepts that
 *             shared the terminology!). I want to take this as an
 *             opportunity to correct this naming discrepancy by
 *             calling the Venus 'system state' an Orion 'status', and
 *             try to refer to it as such. That is to say the Venus
 *             command 'get_system_state' and the Orion command
 *             'get_system_status' are equivalent in concept, though
 *             implemented differently).
 *            
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////
#include <stdbool.h>
#include "FreeRTOS.h"
#include "timers.h"
#include "CotaCommonTypes.h"
#include "proxy_msg_interface.h"
#include "orion_config.h"

/////////////////////////////////////////////////////////////////////////////////////////////
// Defines 
/////////////////////////////////////////////////////////////////////////////////////////////
     
#define LR_UPDATE_TIMER_SEC  5 ///< The amount of time that must pass before the light ring can be updated again.


/////////////////////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////
// Private Variable Declarations
/////////////////////////////////////////////////////////////////////////////////////////////
static LightRingState_t gCurrLightRingState = LR_STATE_ERROR;    ///< The current state of the light ring.

/**
 * When the light ring is updated, sometimes we can't update the light
 * ring again until some time has elapsed. The gLRUpdateTimer keeps
 * track of this. In addition to this, if we try to update the light
 * ring while this timer is running, then we need to remember that so
 * we can update the timer after the timer ends. 
 */
static TimerHandle_t    gLRUpdateTimerHandle;                    ///< Timer handle for the update timer
static StaticTimer_t    gLRUpdateTimerBuffer;                    ///< FreeRTOS timer buffer for the LR update timer
static bool             gTriedToUpdateDuringTimer = false;       ///< true if we tried to update the LR while the timer is going
static bool             gUpdateTimerRunning = false;             ///< True if the update timer is running
static LightRingState_t gUpdateDuringTimerState = LR_STATE_IDLE; ///< Holds the temporary state that we will update to later. 

/////////////////////////////////////////////////////////////////////////////////////////////
// Private Function Declarations 
/////////////////////////////////////////////////////////////////////////////////////////////
static void updateTimerCallback(TimerHandle_t timer);

/**
 * @brief Initialize the light ring module. 
 */
void LRLightRingInit(void)
{
    gLRUpdateTimerHandle = xTimerCreateStatic(
        "Light Ring Update Timer",
        SEC_TO_TICKS(LR_UPDATE_TIMER_SEC),
        pdFALSE,
        (void *) 0,
        updateTimerCallback,
        &gLRUpdateTimerBuffer);

    while (gLRUpdateTimerHandle == NULL);
}
/**
 * @brief Returns the current state of the light ring.
 * @return LightRingState_t The current light ring state
 */
LightRingState_t LRGetCurrLightRingState(void)
{
    return gCurrLightRingState;
}

/**
 * @brief Starts the light ring timer. The light ring state cannot be updated while
 *        the timer is running. After the timer finishes, the light ring will
 *        be updated with the last light ring state.
 * @return CotaError_t COTA_ERROR_NONE on success, COTA_ERROR_COMM_TIMER_START_FAIL
 */
CotaError_t LRStartUpdateTimer(void)
{
    gUpdateTimerRunning = true;

    return ((xTimerStart(gLRUpdateTimerHandle ,
                         pdMS_TO_TICKS(TIMER_BLOCK_MS) ) == pdPASS) ?
            COTA_ERROR_NONE :
            COTA_ERROR_COMM_TIMER_START_FAIL);
}

/**
 * @brief Update the light ring.
 * @param state The light ring state that we want to display.
 * @return CotaError_t COTA_ERROR_NONE if no error, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t LRLightRingUpdate(LightRingState_t state)
{
    PrxHostLightRingMsg_t msg;
    CotaError_t err = COTA_ERROR_NONE; 

    // If the light ring is updated while the update timer is running,
    // we'll save that state that attempted to update with and update
    // it once the timer has expired.
    if (gUpdateTimerRunning)
    {
        gTriedToUpdateDuringTimer = true;
        gUpdateDuringTimerState = state;
    }
    else
    {
        // At the time of implementing this, the debug state is not
        // supported at the proxy. So the debug light ring status will
        // become IDLE until DEBUG is implemented.
        ///< @todo remove this section when DEBUG is supported
        if (state == LR_STATE_DEBUG)
        {
            state = LR_STATE_IDLE;
        }
    
        // We only send an update if it needs an update. 
        if (gCurrLightRingState != state)
        {
            // Update the current state so GetCurrLightRingState stays correct
            gCurrLightRingState = state;
            msg.state = state;
            err = PrxSendLightRingMsg(&msg);
            LogDebug("The Light Ring has been updated!\r\n");
        }
    }
    
    return err;
}

/**
 * @brief A update timer that will prevent the light ring from
 *        updating too soon.
 *
 * @param timer Meta data on the timer.  
 */
static void updateTimerCallback(TimerHandle_t timer)
{
    uint32_t callbackCnt;
    BaseType_t wasHigherPriorityTaskWoken;

    wasHigherPriorityTaskWoken = pdFALSE;

    // Increment the timer count, which is the number of times the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);

    // Indicates that the update timer is not running and we can
    // update the light ring as we please.
    gUpdateTimerRunning = false;
    if (gTriedToUpdateDuringTimer)
    {
        gTriedToUpdateDuringTimer = false; 
        LRLightRingUpdate(gUpdateDuringTimerState);
    }
    
    portYIELD_FROM_ISR( wasHigherPriorityTaskWoken );   
}
