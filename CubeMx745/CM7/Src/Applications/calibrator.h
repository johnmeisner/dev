/**
 * @file      calibrator.h
 *
 * @brief     Header file containing prototypes of functions to calibrate the transmitter
 *
 * @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
 *            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
 *            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
 *            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
 *            ALL RIGHTS RESERVED.
 */
#ifndef _CALIBRATOR_H_
#define _CALIBRATOR_H_

#include "amb_control.h"
#include "uvp_driver.h"
#include "error.h"

void SetTpsActiveForAutoCal(void);
void SetManualCalibrationFlag(bool enable);
void SetChargingBeginForAutoCal(void);
bool CheckCalibrationNeed(void);

CotaError_t InitializeAutoCalibration(void);
CotaError_t Calibrate(AmbMask_t ambMask, UvpMask_t uvpMask);
CotaError_t StaticTune(AmbMask_t ambMask);

#endif //#ifndef _CALIBRATOR_H_
