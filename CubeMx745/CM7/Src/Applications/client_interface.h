/**
 * @file       client_interface.h
 *
 * @brief      Header for client interface
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CLIENT_INTERFACE_H_
#define _CLIENT_INTERFACE_H_

#include "error.h"
#include "client.h"
#include "CotaCommonTypes.h"
#include "client_manager.h"
#include "nvm_driver.h"

/*
 * Client Management operations
 */
void ResetShortIds(void);
ShrtAddr_t GetNextShortId (void);

CotaError_t CmGetClientDetail(ExtdAddr_t longId);
CotaError_t CmClientQueryConfirm(PrxHostCqRecCfn_t queryCfn);
CotaError_t CmClientQueryData(ClientQueryDataCstm_t* queryAck);
ExtdAddr_t  CmGetLongIdFromShortId(ShrtAddr_t shortId);
CotaError_t CmGetClientInfo(ExtdAddr_t longId, Client_t *client);
CotaError_t CmSetQueryType(ExtdAddr_t longId, CotaRcvrQuery_t queryType);
CotaError_t CmSetClientAppData(ShrtAddr_t shortId, CotaAppData_t *data);
CotaError_t CmGetClientAppData(ExtdAddr_t longId, CotaAppData_t *data);
CotaError_t CmSetAppCmdTriesLeft(ExtdAddr_t longId, uint8_t appCmdTriesLeft);
bool        CmGetAppCmdAllCandidate(ExtdAddr_t* longId, uint8_t* appCmdTriesLeft, uint16_t desiredNum);

bool CmDoClientsWithThisStateExist(ClientState_t state);
bool CmAnyChargeableClients(void);
void CmDeleteAllReceiversFromDatabase(void);
void CmSetCommMaxFailCount(uint8_t count);
uint8_t CmGetCommMaxFailCount(void);

static const ExtdAddr_t INVALID_CLIENT_LONGID = { {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }};   ///< Used to indicate an invalid long id
static const ExtdAddr_t ALL_CLIENT_LONGID = { {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }};       ///< Used to indicate all long id's have been specified
#define IS_LONGID_VALID(longId) (memcmp(&longId,&INVALID_CLIENT_LONGID, sizeof(ExtdAddr_t)) != 0)       ///< Used to compare a long id to the invalid id
#define IS_ALL_CLIENT(longId) (memcmp(&longId,&ALL_CLIENT_LONGID, sizeof(ExtdAddr_t)) == 0)             ///< Used to determine if the long id represents all clients

#endif // #endif _CLIENT_INTERFACE_H_
