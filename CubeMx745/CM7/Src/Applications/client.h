/**
 * @file       client.h
 *
 * @brief      Header for the client module 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <stdint.h>
#include <stdbool.h>
#include "orion_config.h"
#include "CotaCommonTypes.h"


#define NUM_RCVR_STATES             11        ///< The number of states that exist in #ClientState_t
#define BATTERY_LEVEL_MASK          0x7F      ///< A Battery level has a maximum of 100(%), the bit-field gets 7-bits
#define NUM_ANTENNA_PORTS           4         ///< Maximum number of antenna ports for a client
#define INVALID_POWER_VALUE         ((int8_t)(-1))   ///< Indicates that power values are not available
#define INVALID_SHORT_ID_0          0         ///< The lower bound for shortIds. This is considered invalid.
#define INVALID_SHORT_ID_FFFF       0xFFFF    ///< The upper bound for shortIds. This is considered invalid.

#if defined(USE_DEMO_BUILD) 
/**
 * Client states for the Demo build target 
 */
typedef enum _ClientState_t
{
    RCVR_STATE_STATE_UNKNOWN,       ///< The Receiver attached HW is in an unknown state
    RCVR_STATE_READY,               ///< The client responded to a TPC message
    RCVR_STATE_CHARGING,            ///< The client will be considered by the scheduler for charging.   
    RCVR_STATE_DISCOVERY,           ///< The client has responded to a discovery message
    RCVR_STATE_REGISTERED,          ///< The client has been registered, but needs to respond to a join message
    RCVR_STATE_JOINED,              ///< The client responded to a join message, but needs a TPC message
    RCVR_STATE_SILENT,              ///< The client was 'ready' but is not responding to queries.
    RCVR_STATE_DISCONNECT,          ///< The client is scheduled to be disconnected and removed.
    RCVR_MAX_STATES,
} ClientState_t;
#elif defined(USE_FOREVER_TRACKER_BUILD)
/**
 * Client states for the ForeverTracker build target 
 */
typedef enum _ClientState_t
{
    RCVR_STATE_STATE_UNKNOWN,       ///< The Receiver attached HW is in an unknown state
    RCVR_STATE_ANNOUNCED,           ///< The rcvr is known and ready for configuration. For ForeverTracker
    RCVR_STATE_NOT_READY_TO_CHARGE, ///< The Receiver is not ready to charge
    RCVR_STATE_READY_TO_CHARGE,     ///< The Receiver is ready to charge
    RCVR_STATE_REMOVED_FROM_DATABASE, ///< Indicates the receiver has been marked for removal.
    RCVR_STATE_SCHEDULED,           ///< Indicates the receiver is in the round-robin for charging
    RCVR_MAX_STATES,
} ClientState_t;
#else
    #error Undefined build target!
#endif

typedef PACKED struct _Client_t 
{
    ExtdAddr_t              longId;              ///< A client's MAC address assigned at manufacturing time
    ShrtAddr_t              shortId;             ///< A client short address assigned when it's joined.
    uint16_t                version;             ///< The version the software is running 
    uint32_t                hwModel;             ///< A hardware type identifier for the client.
    uint8_t                 isValid;             ///< Obsolete. Remove when able to. Requires work on MM
    ClientQueryDataCstm_t   qStatus;             ///< The query information from the client
    ClientState_t           state;               ///< The current state of the client.
    CotaAppData_t           appData;             ///< Client app data from Client Commands
    uint8_t                 priority;            ///< A priority used to determine which clients get charged.
    uint8_t                 userChargeRequested; ///< If 1 this client will immediately go to the charging after receiving the TPC message.
    uint8_t                 unschedQueryCount;   ///< The number of query periods (see CFG_QUERY_PERIOD) that have gone without a query scheduled.  The higher this number, the greater the chance of getting scheduled for a query
    uint8_t                 queryNeededFlag;     ///< if true, the client has not been sent a query during the query period. Once the client has been scheduled, the flag resets to true .
    uint32_t                lastCommTimeTicks;   ///< Time is ticks from the start of boot when the last message is received
    uint16_t                commFailCount;       ///< The # of times communication has failed w/ the rcvr
    uint8_t                 appCmdTriesLeft;     ///< When an app command is sent to all receivers, this tells the ctrl_task how many tries are left until it gives up.
    bool                    fatalLow;            ///< Indicates if the receiver should be charged. Forever Tracker only
} Client_t;

/**
 * @brief Contain the data in the custom query data area
 */ 
typedef PACKED struct
{
  int16_t  batteryVoltage;                      ///< Battery voltage in mV
  uint16_t avgRfPower[NUM_ANTENNA_PORTS];       ///< Average RF power based on ZCS_WIDTH_AVG_SAVED ASIC register values for each of the 4 antennas
  uint16_t avgRfPowerMdBm[NUM_ANTENNA_PORTS];   ///< Average RF power based on ZCS_WIDTH_AVG_SAVED ASIC register values for each of the 4 antennas, in milli-dBm
  uint16_t maxRfPower[NUM_ANTENNA_PORTS];       ///< Maximum RF power based on ZCS_WIDTH_AVG_SAVED ASIC register values for each of the 4 antennas
  uint16_t maxRfPowerMdBm[NUM_ANTENNA_PORTS];   ///< Maximum RF power based on ZCS_WIDTH_AVG_SAVED ASIC register values for each of the 4 antennas, in milli-dBm
  uint8_t  lastError;                           ///< Last error code
} CustomQueryData_t;   

int32_t LongCmp(ExtdAddr_t longId1, ExtdAddr_t longId2);
bool IsLongValid(ExtdAddr_t longId);
bool IsLongIdValidTiMALAddr(ExtdAddr_t longId);
bool IsShortValid(ShrtAddr_t shortId);
bool IsRcvrAbleToQuery(Client_t *client);
bool IsRcvrReadyForComm(Client_t *client);
bool IsRcvrRegistered(Client_t *rx);
bool IsRcvrReadyForCharging(Client_t *client);
bool DoesRcvrHaveCustomDataField(Client_t client);
CotaError_t DoBuildSpecificActionsAfterQueryResponse_CheckState(ClientQueryDataCstm_t* queryData, Client_t *client);
CotaError_t DoBuildSpecificActionsAfterQueryResponse_CheckRcvrPower(Client_t *client, uint32_t lastTick);
void MarkAllRemainingScheduledQueriesAsFailed(void);
    
#endif // #ifndef _CLIENT_H_
