/**
 * @file      cli_task.h
 *
 * @brief     A header for the CLI task module
 *
 * @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *            STRICTLY PROHIBITED.  COPYRIGHT 2019-2020 OSSIA INC. (SUBJECT TO LIMITED
 *            DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.

 */
#ifndef _CLI_TASK_H
#define _CLI_TASK_H
#include "FreeRTOS.h"
#include "error.h"
#include "client.h"

#define MAX_OUTPUT_LENGTH   MAX_UART_MSG_SIZE
static const char * SUCCESS_MSG  = "SUCCESS\r\n";
static const char * FAILURE_MSG  = "FAILURE\r\n";
static const char * ENABLED_MSG  = "enabled";
static const char * DISABLED_MSG = "disabled";
static const char * ON_MSG       = "ON";
static const char * OFF_MSG      = "OFF";
static const char * TX_MSG       = "TX";
static const char * RX_MSG       = "RX";
static const char * PU_ALL_MSG   = "PU_ALL";
static const char * ALL_MSG      = "ALL";
void CreateCliTask(void);
CotaError_t PostCliMsgForTransmit(uint8_t *pData, uint16_t size);
CotaError_t PostStringForTransmit(const char* str);
CotaError_t PostPrintf(const char* format, ...);
#endif
