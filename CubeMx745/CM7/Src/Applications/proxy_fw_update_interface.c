/**
 * @file       proxy_fw_update_interface.c
 *
 * @brief      Proxy firmware update interface module
 *
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////

#include "proxy_fw_update_interface.h"
#include "proxy_fw_update_driver.h"
#include "ctrl_task.h"
#include "cli_task.h"
#include "CotaCompiler.h"
#include "FreeRTOS.h"
#include <string.h>


/////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////////////////////

/** Commands supported by the proxy FW update interface */
typedef PACKED enum _PrxFwUpdMsg_t
{
    PRX_FW_UPD_CMD_INIT,   ///< Init command
    PRX_FW_UPD_CMD_PING,   ///< Ping command
    PRX_FW_UPD_CMD_READ,   ///< Read command
    PRX_FW_UPD_CMD_SETUP,  ///< Setup command
    PRX_FW_UPD_CMD_WRITE,  ///< Write command
    PRX_FW_UPD_CMD_ERASE,  ///< Erase command
} PrxFwUpdMsg_t;

/**
 * Data packet header definition for messages from the Message Manager
 * processed by the proxy_fw_update_interface module
 */
typedef PACKED struct _PrxFwUpdPacket_t
{
    uint8_t  msgId;  ///< The ID of the message in the payload; see #PrxFwUpdMsg_t for values

    PACKED union
    {
        /** Read command for proxy FW update interface */
        PACKED struct
        {
            uint32_t address;     ///< The starting address of the block to be read
            uint32_t size;        ///< The size of the block to be read in bytes
        } PrxFwUpdRead;
        
        /** Setup command for proxy FW update interface */
        PACKED struct
        {
            uint32_t address;     ///< The starting address of the block to be erased and programmed in bytes
            uint32_t size;        ///< The size of the block to be erased and programmed in bytes
        } PrxFwUpdSetup;
        
        /** Write command for proxy FW update interface */
        PACKED struct
        {
            uint16_t size;        ///< The size of the block to be programmed/written to in bytes
            uint8_t  firstByte;   ///< Placeholder for the first byte to be programmed
        } PrxFwUpdWrite;
        
        /** Erase command for proxy FW update interface */
        PACKED struct
        {
            uint32_t address;     ///< The starting address of the block to be erased
            uint32_t size;        ///< The size of the block to be erased in bytes
        } PrxFwUpdErase;
    };
} PrxFwUpdPacket_t;


/////////////////////////////////////////////////////////////////////////////////////////////
// Private Variable Declarations
/////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////
// Global Variables
/////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////
// Private Function Prototypes
/////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////
// Function implementation
/////////////////////////////////////////////////////////////////////////////////////////////


/**
 * @brief  Proxy FW update message dispatch logic based on input buffer data
 *
 * This function takes the first byte of the input data buffer and interprets
 * it as the proxy FW update message ID from #PrxFwUpdMsg_t.  It then parses the
 * rest of the buffer data as a specific data structure based on the message type.
 * For the read message type, data is read by the proxy driver and is returned via
 * the output buffer.
 *
 * @param  inBuf    Input buffer containing message type ID and command parameters
 * @param  inSize   Input buffer size in bytes
 * @param  outBuf   Output buffer to place returned data into (for the read operation)
 * @param  outSize  Output buffer size as an input param; output buffer data size as an output param
 *
 * @return COTA_ERROR_PRX_FW_UPDATE_CMD if a command was executed but returned an error;\n
 *         COTA_ERROR_NOT_IN_DEBUG_MODE if the system is not in debug mode;\n
 *         COTA_ERROR_NOT_IMPLEMENTED if an invalid command (message type) was passed in;\n
 *         COTA_ERROR_NONE if no errors were detected
 */
CotaError_t ProxyFwUpdateMsgDispatch(uint8_t * inBuf, uint16_t inSize, uint8_t * outBuf, uint16_t * outSize)
{
    CotaError_t err = COTA_ERROR_PRX_FW_UPDATE_CMD;
    PrxFwUpdPacket_t * packet = (PrxFwUpdPacket_t*) inBuf;
    PrxFwUpdMsg_t messageType = (PrxFwUpdMsg_t) packet->msgId;
    uint32_t size;
    uint8_t * dataBuf;
    uint16_t  outputBufSize = *outSize;
    static uint32_t address = 0;
    
    // Once we've read this parameter, we can set it to the new value.
    // The new value is the size of returned data, which is always 0
    // except in case the Read command succeeds and returns data.
    *outSize = 0;
    
    
    // Ensure we're in debug mode before issuing commands to the proxy bootloader
    if (!GetDebugMode())
    {
        PostPrintf("ERROR: Proxy bootloader communication was attempted "
                   "before debug mode was enabled.\r\n");
        err = COTA_ERROR_NOT_IN_DEBUG_MODE;
    }
    else
    {
        switch (messageType)
        {
            case PRX_FW_UPD_CMD_INIT:
            {
                // Send an initialization command to reboot the proxy MCU into
                // bootloader, then send a Ping command to confirm the bootloader
                // is running.
                
                // Note that the && operator guarantees left-to-right operand
                // evaluation and if the first expression evaluates to false (0),
                // the second operand of && is not evaluated, which is what we want.
                if (ProxyFwUpdateInit() && ProxyFwUpdatePingBL())
                {
                    PostPrintf("Successfully initialized proxy bootloader.\r\n");
                    err = COTA_ERROR_NONE;
                }
                else
                {
                    PostPrintf("Proxy bootloader initialization failed.\r\n");
                }
            }
            break;
            
            case PRX_FW_UPD_CMD_PING:
            {
                // Send a Ping command to confirm the bootloader is running
                if (ProxyFwUpdatePingBL())
                {
                    err = COTA_ERROR_NONE;
                }
            }
            break;
            
            case PRX_FW_UPD_CMD_READ:
            {
                address = packet->PrxFwUpdRead.address;
                size    = packet->PrxFwUpdRead.size;
                
                // Return the size of read data limited to the buffer size
                // or requested data size, whichever is smaller
                *outSize = MIN(size, outputBufSize);
                
                // Read data into outBuf buffer
                if (ProxyFwUpdateRead(address, outBuf, *outSize))
                {
                    PostPrintf("Successfully returned %d bytes at address 0x%05X.\r\n", size, address);
                    err = COTA_ERROR_NONE;
                }
                else
                {
                    PostPrintf("Proxy bootloader Read command failed.\r\n");
                }
            }
            break;
            
            case PRX_FW_UPD_CMD_SETUP:
            {
                address = packet->PrxFwUpdSetup.address;
                size    = packet->PrxFwUpdSetup.size;
                
                // Erase memory at the given address and prepare the bootloader
                // to accept future write operations
                if (ProxyFwUpdateSetup(address, size))
                {
                    PostPrintf("Successfully started update of 0x%X bytes at address 0x%05X.\r\n",
                               size, address);
                    err = COTA_ERROR_NONE;
                }
                else
                {
                    PostPrintf("Proxy bootloader Setup command failed.\r\n");
                }
            }
            break;
            
            case PRX_FW_UPD_CMD_WRITE:
            {
                size    =  packet->PrxFwUpdWrite.size;
                dataBuf = &packet->PrxFwUpdWrite.firstByte;
                
                // Write all the data in the buffer to the proxy flash memory
                if (ProxyFwUpdateWrite(size, dataBuf))
                {
                    PostPrintf("Successfully written %d bytes at address 0x%05X.\r\n", size, address);
                    address += size;
                    err = COTA_ERROR_NONE;
                }
                else
                {
                    PostPrintf("Proxy bootloader Write command failed.\r\n");
                }
            }
            break;
            
            case PRX_FW_UPD_CMD_ERASE:
            {
                address = packet->PrxFwUpdErase.address;
                size    = packet->PrxFwUpdErase.size;
                
                
                // Send an Erase command to erase all data on flash memory
                // secotors in the address range given by the starting
                // address and the size of address range in bytes
                if (ProxyFwUpdateErase(address, size))
                {
                    PostPrintf("Successfully erased 0x%X bytes at address 0x%05X.\r\n", size, address);
                    err = COTA_ERROR_NONE;
                }
                else
                {
                    PostPrintf("Proxy bootloader Erase command failed.\r\n");
                }
            }
            break;
            
            default:
            {
                err = COTA_ERROR_NOT_IMPLEMENTED;
            }
            break;
        }
    }
    
    return err;
}

