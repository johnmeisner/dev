/**
 * @file       client_interface.c
 *
 * @brief      Interface for managing clients
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include "FreeRTOS.h"
#include "projdefs.h"
#include "task.h"
#include "client.h"
#include "client_interface.h"
#include "client_manager.h"
#include "error.h"
#include "CotaCommonTypes.h"
#include "uart_driver.h"
#include "print_format.h"
#include "nvm_driver.h"
#include "proxyClientMsgInf.h"
#include "cqt_scheduler.h"
#include "tps_scheduler.h"
#include "rpi_msg_interface.h"
#include "ctrl_interface.h"
#include "proxy_msg_interface.h"
#include "rpi_msg_interface.h"
#include "orion_utilities.h"
#include "log.h"

#define SHORT_ID_INIT_VAL 1  ///< The initialization value for Forever Tracker short ids

///< These functions are provided by client_manager.c
PRIVATE bool GetLongId(ShrtAddr_t shortId, ExtdAddr_t *longId);
PRIVATE bool GetClient(ExtdAddr_t longId, Client_t *client);
PRIVATE bool SaveClient(Client_t client);
PRIVATE void Walk(void (*func)(Client_t* client));
PRIVATE bool FindClientState(ClientState_t state, Client_t *client);
PRIVATE void RemoveAllClients(void);

/**
 * @brief   Stores CmGetAppCmdAllCandidate parameter
 */
typedef struct _AppCmdGetParams_t
{
    ExtdAddr_t appCmdCandidateId;           ///< Used to find app cmd candidates when the 'all' argument is used. Holds the long id of the found candidate
    uint8_t    appCmdCandidateTriesLeft;    ///< Used to find app cmd candidates when the 'all' argument is used.  Holds the number of tries left of the found candidate.
    uint16_t   desiredFoundNum;             ///< The desired nth found candidate
    uint16_t   foundNum;                    ///< The actual number of the current found candidate
} AppCmdGetParams_t;

static AppCmdGetParams_t gAppCmdGetParams;        ///< Parameters use by CmGetAppCmdAllCandidate

static bool             gAnyChargeableClients; ///< A global bool indicating if any clients are chargeable
static uint8_t          gClientCommMaxFailCount = 0;
static uint16_t         gLastShortId = SHORT_ID_INIT_VAL;

void ResetShortIds(void)
{
    gLastShortId = SHORT_ID_INIT_VAL;
}

ShrtAddr_t GetNextShortId (void)
{
    // This allows us to use the first id as assigned at boot up.
    ShrtAddr_t shortId = gLastShortId;
    gLastShortId += 1;
    return shortId;
}

/**
 * @brief Retrieve the long id corresponding to a short id
 *
 * @return a long id if successful; otherwise INVALID_CLIENT_LONGID is returned.
 */
ExtdAddr_t  CmGetLongIdFromShortId(ShrtAddr_t shortId)
{
    ExtdAddr_t longId = INVALID_CLIENT_LONGID;

    if (!GetLongId(shortId, &longId))
    {
        longId = INVALID_CLIENT_LONGID;
    }
    return longId;
}

/**
 * @brief Handle the query confirm message
 *
 * @param queryCfn The query confirmation information
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t CmClientQueryConfirm(PrxHostCqRecCfn_t queryCfn)
{
    Client_t client;
    ExtdAddr_t longId;
    CotaError_t ret = COTA_ERROR_NONE;
    uint32_t currTime;
    GetLongId(queryCfn.shortAddr, &longId);

    if (queryCfn.status == PRX_HOST_STAT_SUCCESS) // Indicates successful query
    {
        if (GetClient(longId, &client))
        {
            client.qStatus.header.linkQuality = queryCfn.linkQuality;
            client.qStatus.header.rssi        = queryCfn.rssi;
        }
        else
        {
            ret = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
        }
    }
    else
    {
        currTime = xTaskGetTickCount();
        GetClient(longId, &client);
        LogWarning("Query confirmation failed for 0x%016llx shortId 0x%04x time = %llu.%03llu seconds lastComm = %llu ms ago\r\n",
                   *(uint64_t*)&longId, queryCfn.shortAddr,
                   TICKS_TO_MS(currTime)/MS_IN_SEC, TICKS_TO_MS(currTime) % MS_IN_SEC,
                   TICKS_TO_MS(TimeDiffTicks(currTime, client.lastCommTimeTicks)));
        ret = COTA_ERROR_QUERY_CONFIRM_FAILED;
        client.commFailCount++;
        SaveClient(client);
    }

    return ret;
}

/**
 * @brief Handle the query data responses
 * @note  There are two types of query responses.
 *
 * 1. When the ClientQueryTable is sent out, a confirmation message
 *     is sent from the proxy to the host (PrxHostCqRecCfn_t). This confirmation says
 *     that the client sent an acknowledgment for the QueryTable.
 * 2. Sometime later, the client will send out its data requested as part of
 *    the proxy. ClientQueryDataStd_t (got Query type 5) and ClientQueryDataCstm_t
 *    (for query type 6, which is type 5 but with a buffer full of custom data).
 *
 *    Battery SOC is a part of standard data (query type 5) whereas battery
 *    voltage is a part of custom data (query type 6).
 *    Power levels for peak and average power are a part of standard query data,
 *    whereas peak and average power per channel are a part of custom query data.
 *
 *    The second one is much more important to us for client management.
 *
 * @param queryData A pointer to the query data
 *
 * @return COTA_ERROR_NONE on success; otherwise COTA_ERROR_RCVR_COULD_NOT_BE_FOUND
 *         if client can't be found
 */
CotaError_t CmClientQueryData(ClientQueryDataCstm_t* queryData)
{
    Client_t client;
    ExtdAddr_t longId;
    uint32_t lastTick;
    CotaError_t ret = COTA_ERROR_NONE;

    GetLongId(queryData->header.clientShortAddr, &longId);

    if (GetClient(longId, &client) && ExpectingQuery(queryData->header.clientShortAddr))
    {
        // Populate the client query data on a specified client.
        client.qStatus.status.peakPower      = queryData->status.peakPower;
        client.qStatus.status.avgPower       = queryData->status.avgPower;

        client.qStatus.status.peakNetCurrent = queryData->status.peakNetCurrent;
        client.qStatus.status.batteryCharge  = queryData->status.batteryCharge;
        client.qStatus.status.statusFlags    = queryData->status.statusFlags;
        client.qStatus.header.rssi           = queryData->header.rssi;
        client.qStatus.header.linkQuality    = queryData->header.linkQuality;
        lastTick                             = client.lastCommTimeTicks;
        client.lastCommTimeTicks             = xTaskGetTickCount();
        client.commFailCount                 = 0;

        if (DoesRcvrHaveCustomDataField(client))
        {
             client.qStatus.size = MIN(queryData->size, MAX_CUSTOM_DATA_SIZE);
             memcpy(client.qStatus.data, queryData->data, client.qStatus.size);
        }

        //This removes the client from query scheduler so we know that
        //it was received.
        RemoveQueryClient(queryData->header.clientShortAddr);

        DoBuildSpecificActionsAfterQueryResponse_CheckState(queryData, &client);
        DoBuildSpecificActionsAfterQueryResponse_CheckRcvrPower(&client, lastTick);

        SaveClient(client);
    }
    else
    {
        ret = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
    }

    return ret;
}

/**
   @brief Retrieves client details
   @param longId - The ExtdAddr of the client to remove
   @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t CmGetClientDetail(ExtdAddr_t longId)
{
    Client_t client;

    if (IsLongValid(longId))
    {
        if (!GetClient(longId, &client))
        {
            return COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
        }

        PrintClientDetail(&client);

        return COTA_ERROR_NONE;
    }
    return COTA_ERROR_BAD_PARAMETER;
}

/**
 * @brief Walking function for #CmAnyChargeableClients
 *
 * @param client The current client in the walk.
 */
static void chargeableWalk(Client_t* client)
{
    if (IsRcvrReadyForCharging(client))
    {
        gAnyChargeableClients = true;
    }
}

/**
 * @brief Determines if there are any chargeable clients
 *        To be chargeable a client must in in charging state and power
 *        must be requested from the client.
 * @return true if yes, false if no.
 */
bool CmAnyChargeableClients(void)
{
    gAnyChargeableClients = false;
    Walk(chargeableWalk);
    return gAnyChargeableClients;
}

CotaError_t CmGetClientInfo(ExtdAddr_t longId, Client_t *client)
{
    CotaError_t err = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;

    if (IsLongValid(longId))
    {
        if (GetClient(longId, client))
        {
            err = COTA_ERROR_NONE;
        }
    }

    return err;
}

/**
 * @brief Sets the query type for a client
 *
 * @param longId The long id of the client
 * @param queryType   The query type
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t CmSetQueryType(ExtdAddr_t longId, CotaRcvrQuery_t queryType)
{
    Client_t client;
    CotaError_t err = COTA_ERROR_NONE;

    if (GetClient(longId, &client))
    {
        if (IsRcvrRegistered(&client))
        {
            client.qStatus.header.queryType = queryType;
            NvmUpdateClient(&client);
            SaveClient(client);
        }
        else
        {
            err = COTA_ERROR_RCVR_NOT_REGISTERED;
        }
    }
    else
    {
        err = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
    }

    return err;
}

/**
 * @brief Saves the client appdata in the client's data
 *
 * @param shortId The shortId of the client (returned in response to a client command)
 * @param data The raw data that resulted from a client command.
 *
 * @return COTA_ERROR_NONE on success else CORA_ERROR_RCVR_COULD_NOT_BE_FOUND
 */
CotaError_t CmSetClientAppData(ShrtAddr_t shortId, CotaAppData_t *data)
{
    Client_t    client;
    ExtdAddr_t  longId;
    CotaError_t err = COTA_ERROR_NONE;

    if (GetLongId(shortId, &longId) && GetClient(longId, &client))
    {
        memcpy((void *) client.appData.data, (void *) data->data, sizeof(CotaAppData_t));
        SaveClient(client);
    }
    else
    {
        err = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
    }

    return err;
}

/**
 * @brief Gets the client appdata in the client's data
 *
 * @param longId The longId of the client
 * @param data The output buffer the appdata will be copied to.
 *
 * @return COTA_ERROR_NONE on success else CORA_ERROR_RCVR_COULD_NOT_BE_FOUND
 */
CotaError_t CmGetClientAppData(ExtdAddr_t longId, CotaAppData_t *data)
{
    Client_t    client;
    CotaError_t err = COTA_ERROR_NONE;

    if (GetClient(longId, &client))
    {
        memcpy((void *) data, (void *) &client.appData, sizeof(CotaAppData_t));
    }
    else
    {
        err = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
    }

    return err;
}

/**
 * @brief Check if a client with a given state can exist.
 *
 * @param state The client state that we are checking for.
 *
 * @return true on success, false if a client with that state is not found.
 */
bool CmDoClientsWithThisStateExist(ClientState_t state)
{
    bool ret = false;

    if (FindClientState(state, NULL))
    {
        ret = true;
    }

    return ret;
}
/**
 * @brief Destroy all receiver data from the database
 *
 * @note Users beware, this function will delete all the client data
 *        from the database without any extra logic to handle how the
 *        clients will react. This function should only be called in
 *        special-cases.
 */
void CmDeleteAllReceiversFromDatabase(void) {
    RemoveAllClients();
}

/**
 * @brief Sets the app command try count for a single client
 *
 * @param longId          The long id of the client we are setting the appCmdId for.
 * @param appCmdTriesLeft The number of times left to attempt to send the app command
 *
 * @return COTA_ERROR_NONE on success else CORA_ERROR_RCVR_COULD_NOT_BE_FOUND
 */
CotaError_t CmSetAppCmdTriesLeft(ExtdAddr_t longId, uint8_t appCmdTriesLeft)
{
    Client_t client;
    CotaError_t err = COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;

    if (GetClient(longId, &client))
    {
        client.appCmdTriesLeft = appCmdTriesLeft;

        SaveClient(client);

        err = COTA_ERROR_NONE;
    }

    return err;
}

/**
 * @brief A walk function designed to find app command candidates.
 *        We don't just want the first, we want the nth candidate specified by gAppCmdGetParams.desireFoundNum.
 * @param client A pointer to the client being considered.
 */
static void findAppCmdClientWalk(Client_t* client)
{
    if (client->appCmdTriesLeft > 0 )
    {
        gAppCmdGetParams.foundNum++;  //We increment after every found candidate to keep track of which candidate we are on.
        if (gAppCmdGetParams.foundNum == gAppCmdGetParams.desiredFoundNum)
        {
            //This the current candidate we desire.
            gAppCmdGetParams.appCmdCandidateId = client->longId;
            gAppCmdGetParams.appCmdCandidateTriesLeft = client->appCmdTriesLeft;
        }
    }
}

/**
 * @brief Retrieves a receiver that is a canididate for sending a app command (used when all argument is passed to app command
 *
 * @param longId            receives the long id of the app command candidate
 * @param appCmdTriesLeft   receives how many tries are left of the app command canidate
 * @param desiredNum        Return this candidate. For example, if 3, return the third candidate encountered.
 *
 * @return true if an app command candidate is found.
 */
bool CmGetAppCmdAllCandidate(ExtdAddr_t* longId, uint8_t* appCmdTriesLeft, uint16_t desiredNum)
{
    gAppCmdGetParams.appCmdCandidateId = INVALID_CLIENT_LONGID;
    gAppCmdGetParams.desiredFoundNum = desiredNum;
    gAppCmdGetParams.foundNum = 0;

    Walk(findAppCmdClientWalk);

    *longId = gAppCmdGetParams.appCmdCandidateId;
    *appCmdTriesLeft = gAppCmdGetParams.appCmdCandidateTriesLeft;

    return IsLongValid(gAppCmdGetParams.appCmdCandidateId);
}

void CmSetCommMaxFailCount(uint8_t count)
{
    gClientCommMaxFailCount = count;
}

uint8_t CmGetCommMaxFailCount(void)
{
    return gClientCommMaxFailCount;
}
