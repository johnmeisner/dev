/** 
 * @file       rpi_msg_interface.h
 *
 * @brief      This is the header file for the Raspberry Pi message intefrace module
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#ifndef _RPI_MSG_INTERFACE_H_
#define _RPI_MSG_INTERFACE_H_

#include "stdint.h"
#include "stdbool.h"
#include "raspberry_pi.h"

#define RPI_MAX_MSG_SIZE MAX_FRAME_SIZE

void RpiFrameToMessages(uint8_t *framePayloadData, uint16_t frameSize);
bool RpiSendMessage(uint8_t * buf, uint16_t size);

#endif  // #ifndef _RPI_MSG_INTERFACE_H_
