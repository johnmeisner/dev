/**
 * @file       log.h
 *
 * @brief      Allows the system to log system behavior and allows users to
 *             decide what type of log messages they will receive.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _LOG_H_
#define _LOG_H_

#define MAX_LOG_STRING_SIZE 128    ///< The maximum length a log string can be. 

/**
 * @brief Defines the level in which log message 
 */
typedef enum _LogLevel_t
{
    LOG_NONE    = 0,  ///< Indicates no messages shall be logged. 
    LOG_FATAL   = 1,  ///< Indicates a case that will cause immediate termination. 
    LOG_ERROR   = 2,  ///< Indicates an error that must handled for proper system utilization 
    LOG_WARNING = 3,  ///< Indicates an issue that may result in erroneous results. 
    LOG_INFO    = 4,  ///< An informative message about events that have occurred. 
    LOG_DEBUG   = 5,  ///< A message that is used for debugging software operation
    LOG_LEVEL_COUNT   ///< This enum as an int is always equal to the number of log types
} LogLevel_t;

void LogInit(LogLevel_t systemLevel);
void SetSystemLevel(LogLevel_t level);
LogLevel_t GetSystemLevel(void);

void LogFatal(const char *, ...);
void LogError(const char *, ...);
void LogWarning(const char *, ...);
void LogInfo(const char *, ...);
void LogDebug(const char *, ...);

#endif // _LOG_H_
