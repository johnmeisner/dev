/****************************************************************************//**
* @file      calibrator.c
*
* @brief     Implements functions to calibrate the transmitter
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "main.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "power_level.h"
#include "calibrator.h"
#include "cli_task.h"
#include "ctrl_task.h"
#include <stdio.h>
#include "task.h"
#include "cli_task.h"
#include "timers.h"

#define MAX_TX_WAIT_COUNT        100        ///< The maximum times to poll the SEND_POWER_RDY register before throwing an error.
#define MAX_PU_WAIT_COUNT        100        ///< The maximum times to poll the PHASE_DETECT_DONE register before throwing an error.
#define MAX_PHASE_OFFSET         512        ///< The range of bits used to represent the phase
#define PHASE_0                  0          ///< The value for 0 phase
#define PHASE_180                0x100      ///< Phase value for 180 degrees
#define FULL_ARC_IN_CENTIDEGREES 36000      ///< 360 degress in centidegrees
#define UVP_CLIENT_1_MASK   0x1             ///< Mask of UVP client 1
#define UVP_CLIENT_2_MASK   0x2             ///< Mask of UVP client 2
#define UVP_CLIENT_3_MASK   0x4             ///< Mask of UVP client 3
#define UVP_CLIENT_4_MASK   0x8             ///< Mask of UVP client 4
#define UVP_CLIENT_5_MASK   0x10            ///< Mask of UVP client 5
#define UVP_CLIENT_6_MASK   0x20            ///< Mask of UVP client 6
#define UVP_CLIENT_7_MASK   0x40            ///< Mask of UVP client 7
#define UVP_CLIENT_8_MASK   0x80            ///< Mask of UVP client 8
#define UVP_ALL_CLIENT_MASK (UVP_CLIENT_1_MASK | UVP_CLIENT_2_MASK | UVP_CLIENT_3_MASK | UVP_CLIENT_4_MASK | UVP_CLIENT_5_MASK | UVP_CLIENT_6_MASK | UVP_CLIENT_7_MASK | UVP_CLIENT_8_MASK)  ///< Mask for all clients

#define UVP_RXCTRL_SET          1   ///< Value to turn RX on
#define UVP_RXCTRL_RESET        0   ///< Value to turn RX on
#define UVP_TXCTRL_SET          1   ///< Value to turn TX on
#define UVP_TXCTRL_RESET        0   ///< Value to turn TX on
#define UVP_PUCTRL_SET          1   ///< Value to turn PU on
#define UVP_PUCTRL_RESET        0   ///< Value to turn PU on
#define PHASE_CONJUGATE_ENABLE  1   ///< Value to enable phase conjugate mode
#define PHASE_CONJUGATE_DISABLE 0   ///< Value to disable phase conjugate mode

#define SIDE_OF_AMB_START(side)    ((side == 0) ? 0 : 8)        ///< For single AMB calibration, indicates the first UVP in a side
#define SIDE_OF_AMB_END(side)      ((side == 0) ? 8 : 16)       ///< For single AMB calibration, indicates the last UVP (+1) in a side

static AmbNum_t gCalMap[MAX_NUM_AMB] = { 3, 2, 1, 0 };  ///< Maps an amb to the amb used as a beacon.

static volatile bool  gShortCalReady                = false;   ///< Flag indicates a short time period calibration should be done
static volatile bool  gLongCalReady                 = false;   ///< Flag indicates a long timer period calibration should be done 
static volatile bool  gChargingBegins               = false;   ///< Indicates that charging has begun
static volatile bool  gTemperatureSample            = false;   ///< Indicates that it's time to sample UVP temperature
static volatile bool  gDoCalibration                = false;   ///< Indicates that a request from the user to calibrate has been received.
static volatile bool  gTpsActive                    = false;   ///< Indicates that TPS charging is active
static volatile bool  gObstacleDetected             = false;   ///< Indicates an obstacle has been detected.

static TimerHandle_t  gShortCalPeriodTimerHandle    = NULL;    ///< FreeRTOS timer handle for the short auto-cal timer
static StaticTimer_t  gShortCalPeriodTimerBuffer;              ///< FreeRTOS timer buffer for the short auto-cal timer
static TimerHandle_t  gChargeStartTimerHandle        = NULL;   ///< FreeRTOS timer handle for the charge starting timer
static StaticTimer_t  gChargeStartTimerBuffer;                 ///< FreeRTOS timer buffer for the charge starting timer
static TimerHandle_t  gLongCalPeriodTimerHandle     = NULL;    ///< FreeRTOS timer handle for the long auto-cal timer
static StaticTimer_t  gLongCalPeriodTimerBuffer;               ///< FreeRTOS timer buffer for the long auto-cal timer
static TimerHandle_t  gTemperatureSampleTimerHandle = NULL;    ///< FreeRTOS timer handle for the temperature auto-cal timer
static StaticTimer_t  gTemperatureSampleTimerBuffer;           ///< FreeRTOS timer buffer for the temperature auto-cal timer
static TimerHandle_t  gObstacleTimerHandle          = NULL;    ///< FreeRTOS timer handle for the obstacle auto-cal timer
static StaticTimer_t  gObstacleTimerBuffer;                    ///< FreeRTOS timer buffer for the obstacle auto-cal timer

static uint16_t gTempThresh;                                ///< Uvp temperature threshold
static uint16_t gObsThresh;                                 ///< The obstacle threshold read from the configuaration in degrees
static uint64_t gPhaseDeltaSquared                  = 0;    ///< Average square of delta from latest calibration phase in centidegrees^2
static uint16_t gLastCalPhase[TOTAL_UVPS];                  ///< The last set of calibration phases.    
static bool     gFirstCal                           = true; ///< Boolean needed to indicate first calibration

//Timer callbacks
static void temperatureSampleTimerCallback(TimerHandle_t  timer);
static void chargeStartTimerCallback(TimerHandle_t  timer);
static void shortTimerCallback(TimerHandle_t  timer);
static void longTimerCallback(TimerHandle_t  timer);
static void obstacleTimerCallback(TimerHandle_t  timer);


/**
 * @brief Check the reference AMB resides in the ambMask, if not it finds
 *        another AMB to use.
 * @param ambMask A mask indicating which AMB's are being calibrated
 *
 * @return The reference AMB number.
 */
static uint8_t checkAndGetReferenceAmb(AmbMask_t ambMask)
{
    uint8_t ambRefNum;
    uint8_t ii;

    CfgGetParam(CFG_REFERENCE_AMB, &ambRefNum, sizeof(ambRefNum));
    
    if (!BIT_IN_MASK(ambRefNum, ambMask))
    {
        for (ii = 0; ii < MAX_NUM_AMB; ii++)
        {
            if (BIT_IN_MASK(ii,ambMask))
            {
                ambRefNum = ii;
                break;
            }
        }    
    }
    return ambRefNum;
}

/**
 * @brief Select the beaconing AMB for an AMB being calibrated.
 * 
 * @todo Revisit this with Caner
 * 
 * @param ambMask   A mask indicating which AMB's are being calibrated.
 * @param ambSelNum The individual AMB being calibrated
 *
 * @return Return a recommended num for selecting a calibration AMB.
 */
static uint8_t getBeaconAmb(AmbMask_t ambMask, AmbNum_t ambSelNum)
{
    uint8_t num = ambSelNum;
    uint8_t ii;

    num = gCalMap[ambSelNum];
    
    if (BIT_IN_MASK(num,ambMask))
    {
        return num;
    }
    //Find an AMB different than the one being calibrated
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        num = ii;

        if (BIT_IN_MASK(num,ambMask) && (num != ambSelNum))
        {
            break;
        }
    }

    return num;
}

/**
 * @brief Wait for the send power ready register to indicate that the UVP is transmitting
 * 
 * @param ambNum The AMB number containing the UVP transmitting.
 * @param uvpNum The UVP number that is transmitting.
 * 
 * @return  COTA_ERROR_NONE on success, otherwise an error code of type #CotaError_t.
 */
static CotaError_t waitForTxReady(AmbNum_t ambNum, UvpNum_t uvpNum)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint16_t count = 0;
    uint32_t ready = 0;

    while ((ready == 0) && (count++ < MAX_TX_WAIT_COUNT) && (ret == COTA_ERROR_NONE))
    {
        ret = UvpRegisterRead(SEND_POWER_RDY, &ready, ambNum, uvpNum); 
    }

    if ((ready == 0) && (ret == COTA_ERROR_NONE))
    {
        ret = COTA_ERROR_UVP_WAIT_FOR_TRANSMIT_TIMEOUT;
    }
  
    return ret;
}

/**
 * @brief Wait for the phase detect register to indicate that the phases have been read or updated.
 * 
 * @param ambNum The AMB number containing the UVP undergoing phase detect.
 * @param uvpNum The UVP number that is undergoing phase detect.
 * 
 * @return  COTA_ERROR_NONE on success, otherwise an error code of type #CotaError_t.
 */
static CotaError_t waitForPhaseDetectDone(AmbNum_t ambNum, UvpNum_t uvpNum)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint16_t count = 0;
    uint32_t done = 0;

    while ((done == 0) && (count++ < MAX_TX_WAIT_COUNT) && (ret == COTA_ERROR_NONE))
    {
        ret = UvpRegisterRead(PHASE_DETECT_DONE, &done, ambNum, uvpNum);   
    }

    if ((done == 0) && (ret == COTA_ERROR_NONE))
    {
        ret = COTA_ERROR_UVP_WAIT_FOR_PHASE_DET_TIMEOUT;
    }

    return ret;
}

/**
 * @brief Write the phase offsets for a specific UVP. 
 *
 * @param offset    The phase offset to write
 * @param ambNum    The AMB number containing the UVP to set the phase offset for
 * @param uvpNum    The UVP number to set the phase offset for.
 * @param amuMask   Indicates which AMUs should have their phase offsets set.
 *
 * @return Return 0 on success and negative values for errors
 */
static CotaError_t WritePhaseOffset(uint32_t offset, AmbNum_t ambNum, UvpNum_t uvpNum, AmuNum_t amuMask)
{
    AmuNum_t amuNum;
    CotaError_t ret = COTA_ERROR_NONE;
    UvpReg_t phaseOffsetReg[AMUS_PER_UVP] = {PHASE_OFFSET_AMU1, PHASE_OFFSET_AMU2, PHASE_OFFSET_AMU3, PHASE_OFFSET_AMU4};

    for (amuNum = 0; (amuNum < AMUS_PER_UVP) && (ret == COTA_ERROR_NONE); amuNum++)
    {
        if (BIT_IN_MASK(amuNum, amuMask))
        {
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(phaseOffsetReg[amuNum], offset, ambNum, uvpNum) : ret;
        }
    }

    return ret;
}

/**
 * @brief Read detected phase for a specific AMU in a UVP
 * @param outPhase [out] - Phase read directly from the AMUs
 * @param ambNum Indicates which AMB to read from
 * @param uvpNum Indicate which UVP to read from
 * @param amuNum Indicate which AMU to read from
 * @return  COTA_ERROR_NONE on success, otherwise an error code of type #CotaError_t.
 */
static CotaError_t readAmuPhase(uint32_t *outPhase, AmbNum_t ambNum, UvpNum_t uvpNum, AmuNum_t amuNum)
{
    UvpReg_t amuArray[MAX_NUM_AMU] = {OUTPHASE_CLNT1_AMU1, OUTPHASE_CLNT1_AMU2, OUTPHASE_CLNT1_AMU3, OUTPHASE_CLNT1_AMU4};
    return UvpRegisterRead(amuArray[amuNum], outPhase, ambNum, uvpNum);
}

#ifdef DEBUG_CALIBRATION
/**
 * @brief Read detected rssi for a specific AMU in a UVP
 * @param outRssi [out] - Phase read directly from the AMUs
 * @param ambNum Indicates which AMB to read from
 * @param uvpNum Indicate which UVP to read from
 * @param amuNum Indicate which AMU to read from
 * @return  COTA_ERROR_NONE on success, otherwise an error code of type #CotaError_t.
 */
static CotaError_t readAmuRssi(uint32_t *outRssi, AmbNum_t ambNum, UvpNum_t uvpNum, amuNum_t amuNum)
{   
    UvpReg_t amuArray[MAX_NUM_AMU] = {RSSi_AMU1, RSSi_AMU2, RSSi_AMU3, RSSi_AMU4};
    return UvpRegisterRead(amuArray[amuNum], outRssi, ambNum, uvpNum);
}

uint32_t someRssi = 0;
#endif

/**
 * @brief This functions calibrates the antennas for just one AMB
 *        We do this by calibrating UVP's 0-7 separately
 *        from UVP's 8-15. UVP's 0-7 will use UVP 15 as a beacon
 *        and UVP's 8-15 will use UVP 0 as a beacon.
 * @param ambNum  Amb number of the AMB to calibrate
 * @param uvpMask A mask indicating which UVP's will be calibrated
 * @param deltaCount pointer to the number of counts creating the delta phase calc
 * 
 * @return COTA_ERROR_NONE on success, otherwise an error code of type #CotaError_t.
 */
CotaError_t SingleAmbCalibrate(AmbNum_t ambNum, UvpMask_t uvpMask,  uint16_t* deltaCount)
{
    CotaError_t ret = COTA_ERROR_NONE;
    UvpNum_t uvpRefNum;
    AmuNum_t amuRefNum;
    UvpNum_t uvpBeacon[2] = { 15, 0};
    AmuNum_t amuBeaconNum;
    UvpNum_t uvp;
    uint32_t phaseRef;
    uint32_t phaseAut;
    int32_t  phaseOffset;
    uint16_t maxPhase;
    uint16_t minPhase;
    int16_t del1;
    int16_t del2;
    int16_t delmin;
    uint8_t side;  //0 means we are calibrating UVP's 0-7 1 meams we are calibrating 8-15
    
    CfgGetParam(CFG_REFERENCE_UVP, &uvpRefNum, sizeof(uvpRefNum));
    
    CfgGetParam(CFG_REFERENCE_AMU, &amuRefNum, sizeof(amuRefNum));
        
    CfgGetParam(CFG_CALIBRATION_AMU, &amuBeaconNum, sizeof(amuBeaconNum));   
    
    if ((uvpRefNum == uvpBeacon[0]) || (uvpRefNum == uvpBeacon[1]))
    {
        PostPrintf("Warning: For single AMB cal reference UVP cannot be %d\r\n", uvpRefNum); 
        uvpRefNum = (uvpBeacon[0] + uvpBeacon[1])/2;
        PostPrintf("Warning: UVP reference %d\r\n", uvpRefNum); 
    }

    for (side = 0; side < 2; side++)
    {
        //Let's enable the selected reference AMU on all UVP's on this AMB
        ret = (ret == COTA_ERROR_NONE) ? UvpManyAmuEnable(NUM_TO_MASK(ambNum), uvpMask, NUM_TO_MASK(amuRefNum) ) : ret;
          
        //Enable the AMU that will serve as the beacon for calibrating AMU
        ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(ambNum, uvpBeacon[side], NUM_TO_MASK(amuBeaconNum)) : ret;   
        
        //This tells the UVP to let the phase be selected by the user for all 'clients' on the beaconing UVP
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARB_PHASE_SEL, UVP_ALL_CLIENT_MASK, ambNum, uvpBeacon[side]) : ret;

        //The following sets the phases for all AMU's for 'client' 1 on the beaconing UVP.
        //@todo confer with Caner to see if we still need these.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU1_CLNT1, PHASE_0, ambNum, uvpBeacon[side]) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU2_CLNT1, PHASE_180, ambNum, uvpBeacon[side]) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU3_CLNT1, PHASE_180, ambNum, uvpBeacon[side]) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU4_CLNT1, PHASE_0, ambNum, uvpBeacon[side]) : ret;
        
        //Put the first half of UVP's in receive mode
        for (uvp = SIDE_OF_AMB_START(side); uvp < SIDE_OF_AMB_END(side); uvp++)
        {
            if (BIT_IN_MASK(uvp, uvpMask))
            {
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_SET, ambNum, uvp) : ret;
            }     
        }
        
        //Make sure the reference is in receive mode
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_SET, ambNum, uvpRefNum) : ret;

        //Start transmitting out of the beacon
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_SET, ambNum, uvpBeacon[side]) : ret; 
        
        ret = (ret == COTA_ERROR_NONE) ? waitForTxReady( ambNum, uvpBeacon[side]): ret;    
        
        //Let's make sure the phases are updated for beacon transmit.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, ambNum, uvpBeacon[side]) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, ambNum, uvpBeacon[side]) : ret;    

        if ((uvpRefNum < SIDE_OF_AMB_START(side)) || (uvpRefNum >= SIDE_OF_AMB_END(side)))
        {
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, ambNum, uvpRefNum) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, ambNum, uvpRefNum) : ret;       
        }
        
        //Put the first half of UVP's in receive mode
        for (uvp = SIDE_OF_AMB_START(side); uvp < SIDE_OF_AMB_END(side); uvp++)
        {
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, ambNum, uvp) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, ambNum, uvp) : ret;
        }    
        
        //Wait for phase detects to be done.
        ret = (ret == COTA_ERROR_NONE) ? waitForPhaseDetectDone( ambNum, uvpRefNum) : ret; 

        //Put the first half of UVP's in receive mode
        for (uvp = SIDE_OF_AMB_START(side); uvp < SIDE_OF_AMB_END(side); uvp++)
        {
            if (BIT_IN_MASK(uvp, uvpMask))
            {
                ret = waitForPhaseDetectDone(ambNum, uvp);
            }
        }    

        //Put everything back in idle
        for (uvp = SIDE_OF_AMB_START(side); uvp < SIDE_OF_AMB_END(side); uvp++)
        {
            if (BIT_IN_MASK(uvp, uvpMask))
            {
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_RESET, ambNum, uvp) : ret;
            }     
        }
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_RESET, ambNum, uvpRefNum) : ret;    
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_RESET, ambNum, uvpBeacon[side]) : ret;
        
        //Let's put the beacon in receive mode.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_SET, ambNum, uvpBeacon[side]) : ret; 
        
        //Now put the reference UVP in TX mode.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_SET, ambNum, uvpRefNum) : ret;    
        ret = (ret == COTA_ERROR_NONE) ? waitForTxReady( ambNum, uvpRefNum): ret;    

        //Update the phase of the transmission phase to the conjugate of what was read.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, ambNum, uvpRefNum) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, ambNum, uvpRefNum) : ret;       
        
        //Configure the Cal UVP to select detected phase instead of phase conjugate
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(SEL_PHASE_CONJUGATE, PHASE_CONJUGATE_DISABLE,  ambNum, uvpBeacon[side]) : ret;
        
        //Read and update the phase for the beacon.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, ambNum, uvpBeacon[side]) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, ambNum, uvpBeacon[side]) : ret;  
        ret = (ret == COTA_ERROR_NONE) ? waitForPhaseDetectDone(ambNum, uvpBeacon[side]): ret;   

        //Read the phase it detected by the beacon when the refence AMU is transmitting.
        ret = (ret == COTA_ERROR_NONE) ? readAmuPhase(&phaseRef, ambNum, uvpBeacon[side], amuBeaconNum) : ret;

        //Stop the reference from transmitting.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_RESET, ambNum, uvpRefNum) : ret;    
         
        //Let's now iterate through the UVP's, ignoring ones that we don't want to calibrate.
        for (uvp = SIDE_OF_AMB_START(side); (uvp < SIDE_OF_AMB_END(side)) && (ret == COTA_ERROR_NONE); uvp++)
        {
            if (!BIT_IN_MASK(uvp, uvpMask))
            {
                continue;
            }

            //We skip the reference UVP if it's on this side.
            if (uvp == uvpRefNum)
            {
                continue;
            }   
        
            //Start the UVP transmitting, then wait till it's ready.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_SET, ambNum, uvp) : ret;
            ret = (ret == COTA_ERROR_NONE) ? waitForTxReady(ambNum, uvp): ret;         
     
            //Update the phase of the transmision signal to the conjugate of what was read earlier
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, ambNum, uvp) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, ambNum, uvp) : ret;       
        
            //Read and update the pase for the beacon.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, ambNum, uvpBeacon[side]) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, ambNum, uvpBeacon[side]) : ret;         

            ret = (ret == COTA_ERROR_NONE) ? waitForPhaseDetectDone(ambNum, uvpBeacon[side]): ret;
            ret = (ret == COTA_ERROR_NONE) ? readAmuPhase(&phaseAut, ambNum, uvpBeacon[side], amuBeaconNum) : ret;        

            //Stop transmitting on the UVP
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_RESET, ambNum, uvp) : ret;
        
            phaseOffset = phaseRef - phaseAut;
        
            if (phaseOffset < 0)
            {
                phaseOffset += MAX_PHASE_OFFSET;
            }
            
            //Write the offset phase (otherwise known as the calibration phase) to the UVP
            ret = (ret == COTA_ERROR_NONE) ? WritePhaseOffset(phaseOffset, ambNum, uvp, ALL_AMU_MASK) : ret;
        
             //Calculate the delta square since the last calibration
            maxPhase = (phaseOffset > gLastCalPhase[UVP_INDEX(ambNum, uvp)]) ? phaseOffset : gLastCalPhase[UVP_INDEX(ambNum, uvp)];
            minPhase = (phaseOffset <= gLastCalPhase[UVP_INDEX(ambNum, uvp)]) ? phaseOffset : gLastCalPhase[UVP_INDEX(ambNum, uvp)];
            del1 = (int16_t)maxPhase - (int16_t)minPhase;                         //Calculate counter clockwise arc
            del2 = ((int16_t)minPhase - ((int16_t)maxPhase - MAX_PHASE_OFFSET));  //Calculate clockwise arc
            delmin = MIN(del1, del2);                                             //The minimum of the two arc is the delta
            gPhaseDeltaSquared += (delmin*delmin);                                //Accumulate the delta squared
            *deltaCount++;
            gLastCalPhase[UVP_INDEX(ambNum, uvp)] = phaseOffset;   
        
        }
        
        //Take the beacon out of receive mode.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_RESET, ambNum, uvpBeacon[side]) : ret;

        //unselect user phase determination of the beacon.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARB_PHASE_SEL, 0, ambNum, uvpBeacon[side]) : ret;

        //restore the phase conjugate mode of the beacon
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(SEL_PHASE_CONJUGATE, PHASE_CONJUGATE_ENABLE, ambNum, uvpBeacon[side]) : ret;

        //Enable the reference AMU on the beacon since it was changed earlier.
        ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(ambNum, uvpBeacon[side], NUM_TO_MASK(amuRefNum)) : ret;     

    }

    return ret;
}

/**
 * @brief This functions calibrates the antennas on the system.
 *
 * @param ambMask A mask indicating which AMB's will be calibrated.
 * @param uvpMask A mask indicating which UVP's will be calibrated
 * 
 * @return COTA_ERROR_NONE on success, otherwise an error code of type #CotaError_t.
 */ 
CotaError_t Calibrate(AmbMask_t ambMask, UvpMask_t uvpMask)
{
    uint32_t minRssiThresh[TOTAL_UVPS];
    CotaError_t ret = COTA_ERROR_NONE;
    uint8_t refAmbNum;
    uint8_t beaconAmbNum;
    AmbNum_t ambNum;
    UvpNum_t uvpNum;
    uint32_t phaseRef;
    uint32_t phaseAut;
    int32_t  phaseOffset;
    uint16_t maxPhase;
    uint16_t minPhase;
    int16_t del1;
    int16_t del2;
    int16_t delmin;
    uint16_t deltacount = 0;
    UvpNum_t uvpRefNum;
    AmuNum_t amuRefNum;
    UvpNum_t uvpBeaconNum;
    AmuNum_t amuBeaconNum;
    PowerRegister_t startPower;
    
    //Let's make sure RX, TX, PU pins are disabled.  We will be controlling UVP's 
    //individually with their registers.
    AmbEnableRx(false);
    AmbEnableTx(false);
    AmbSetPuState(AMB_DISABLE_ALL);

    gPhaseDeltaSquared = 0;  //Zero out the phase delta used for obstacle determination
    
    CfgGetParam(CFG_REFERENCE_UVP, &uvpRefNum, sizeof(uvpRefNum));
    
    CfgGetParam(CFG_REFERENCE_AMU, &amuRefNum, sizeof(amuRefNum));
    
    CfgGetParam(CFG_CALIBRATION_UVP, &uvpBeaconNum, sizeof(uvpBeaconNum));  
        
    CfgGetParam(CFG_CALIBRATION_AMU, &amuBeaconNum, sizeof(amuBeaconNum));  
    
    //We are using lower power levels during calibration, so we need to set RSSI_LOWTHRESH to zero,
    //but we need to save their current values so we can restore them after calibration.
    ret = (ret == COTA_ERROR_NONE) ? ReadArrayUvps(RSSI_LOWTHRESH, minRssiThresh, TOTAL_UVPS, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(RSSI_LOWTHRESH, 0, ambMask, uvpMask) : ret; 

    //At the start we also need to ensure the RX, TX, and PU are turned off at the register level too.
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_RXCTRL, UVP_RXCTRL_RESET, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;   
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_TXCTRL, UVP_TXCTRL_RESET, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_PUCTRL, UVP_PUCTRL_RESET, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;

    //A UVP has many 'clients' that store phase information.  Let's use client 1.
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(CLIENT_SELECT_OUT, 0, ambMask, uvpMask) : ret;
    //This setting ensures that when we update the phase, we use client 1.
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(DETECT_PH_CLIENT_ID_1, 0, ambMask, uvpMask) : ret;   

    //Let's also make sure all the AMU's of all the UVP's are disabled.
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHASE_OFFSET_AMU1, 0, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;   
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHASE_OFFSET_AMU2, 0, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;   
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHASE_OFFSET_AMU3, 0, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;   
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHASE_OFFSET_AMU4, 0, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;

    //Disable all amu's
    ret = (ret == COTA_ERROR_NONE) ? UvpManyAmuEnable(ambMask, uvpMask, NO_AMU_MASK ) : ret;

    //All UVP's should be set to the same power levels.  Let's read one, so we can set them back after calibration.
    for ( ambNum = 0; (ambNum < MAX_NUM_AMB); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }
        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {
            if (!BIT_IN_MASK(ambNum, ambMask))
            {
              continue;
            }
            break;
        }
        break;
    }

    ret = (ret == COTA_ERROR_NONE) ? GetUvpPowerLevels(&startPower, ambNum, uvpNum) : ret;

    //We calibrate using the lowest power setting for the UVP's.
    ret = (ret == COTA_ERROR_NONE) ? SetUvpPowerLevels(AMU_POWER_13_DBM, ambMask, uvpMask) : ret;

    //Let's determine what AMB we are going to use for a reference to calibrate to.
    refAmbNum = checkAndGetReferenceAmb(ambMask);

    if (AmbValidCount() == 1)
    {
        //Calibrating a single AMB requires dividing an AMB in half.
        //so we create a separate routine
        ret = (ret == COTA_ERROR_NONE) ? SingleAmbCalibrate(MASK_TO_NUM(ambMask), uvpMask, &deltacount) : ret;
    }
    else
    {    
        //Alright it's time to calibrate!  Let's loop through the AMB's and ignore the ones not in the ambMask
        for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); ambNum++)
        {    
            if (!BIT_IN_MASK(ambNum, ambMask))
            {
                continue;
            }
    
            //Let's enable the selected reference AMU on all UVP's for the amb under test, and the reference amb
            ret = (ret == COTA_ERROR_NONE) ? UvpManyAmuEnable(NUM_TO_MASK(ambNum), uvpMask, NUM_TO_MASK(amuRefNum) ) : ret;
            if (refAmbNum != ambNum)
            {
                ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(refAmbNum, uvpRefNum, NUM_TO_MASK(amuRefNum) ) : ret;
            }
    
            //We need to determine what AMB will serve as the beacon.  This varies depending on which AMB we are calibrating
            beaconAmbNum = getBeaconAmb(ambMask, ambNum);
    
            //Enable the AMU that will serve as the beacon.
            ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(beaconAmbNum, uvpBeaconNum, NUM_TO_MASK(amuBeaconNum)) : ret;  
    
            //This tells the UVP to let the phase be selected by the user for all 'clients' on the beaconing UVP
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARB_PHASE_SEL, UVP_ALL_CLIENT_MASK, beaconAmbNum, uvpBeaconNum) : ret;
    
            //The following sets the phases for all AMU's for 'client' 1 on the beaconing UVP.
            //@todo confer with Caner to see if we still need these.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU1_CLNT1, PHASE_0, beaconAmbNum, uvpBeaconNum) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU2_CLNT1, PHASE_180, beaconAmbNum, uvpBeaconNum) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU3_CLNT1, PHASE_180, beaconAmbNum, uvpBeaconNum) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU4_CLNT1, PHASE_0, beaconAmbNum, uvpBeaconNum) : ret;
    
            // Set all UVP's on the AMB to receive mode.
            ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_RXCTRL, UVP_RXCTRL_SET, NUM_TO_MASK(ambNum), uvpMask) : ret;
    
            // Set the reference UVP to receive mode
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_SET, refAmbNum, uvpRefNum) : ret;
    
            //Start transmitting out of the beacon
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_SET, beaconAmbNum, uvpBeaconNum) : ret;
    
            ret = (ret == COTA_ERROR_NONE) ? waitForTxReady( beaconAmbNum, uvpBeaconNum): ret;
    
            //Let's make sure the phases are updated for beacon transmit.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, beaconAmbNum, uvpBeaconNum) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, beaconAmbNum, uvpBeaconNum) : ret;
    
            // If the reference AMB is different than the one we are currently calibrating, we need to sample
            // its phase separately.
            if (ambNum != refAmbNum)
            {
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, refAmbNum, uvpRefNum) : ret;
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, refAmbNum, uvpRefNum) : ret;
            }
    
            ///@todo consider setting the PU in hardware and waiting for phase detect pin.
            //We need to update the phase for all UVP's on the AMB we are calibrating.
            ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_PUCTRL, UVP_PUCTRL_SET, NUM_TO_MASK(ambNum), uvpMask) : ret;
            ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_PUCTRL, UVP_PUCTRL_RESET, NUM_TO_MASK(ambNum), uvpMask) : ret;
    
            //Wait for phase detects to be done.
            ret = (ret == COTA_ERROR_NONE) ? waitForPhaseDetectDone( refAmbNum, uvpRefNum) : ret; 
    
            for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
            {
                if (BIT_IN_MASK(uvpNum, uvpMask))
                {
                    ret = waitForPhaseDetectDone(ambNum, uvpNum);
                }
            }
    
            //Take the UVP's and the reference UVP out of receive mode, also stop the beacon from transmitting.
            ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_RXCTRL, UVP_RXCTRL_RESET, NUM_TO_MASK(ambNum), uvpMask) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_RESET, refAmbNum, uvpRefNum) : ret;    
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_RESET, beaconAmbNum, uvpBeaconNum) : ret;
    
            //Let's put the beacon in receive mode.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_SET, beaconAmbNum, uvpBeaconNum) : ret; 
    
            //Now put the reference UVP in TX mode.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_SET, refAmbNum, uvpRefNum) : ret;    
            ret = (ret == COTA_ERROR_NONE) ? waitForTxReady( refAmbNum, uvpRefNum): ret;
    
            //Update the phase of the transmission phase to the conjugate of what was read.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, refAmbNum, uvpRefNum) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, refAmbNum, uvpRefNum) : ret;   
                
            //Configure the Cal UVP to select detected phase instead of phase conjugate
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(SEL_PHASE_CONJUGATE, PHASE_CONJUGATE_DISABLE,  beaconAmbNum, uvpBeaconNum) : ret;
    
            //Read and update the phase for the beacon.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, beaconAmbNum, uvpBeaconNum) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, beaconAmbNum, uvpBeaconNum) : ret;  
            ret = (ret == COTA_ERROR_NONE) ? waitForPhaseDetectDone(beaconAmbNum, uvpBeaconNum): ret;
    
            //Read the phase it detected by the beacon when the refence AMU is transmitting.
            ret = (ret == COTA_ERROR_NONE) ? readAmuPhase(&phaseRef, beaconAmbNum, uvpBeaconNum, amuBeaconNum) : ret;
                
#ifdef DEBUG_CALIBRATION
            ret = (ret == COTA_ERROR_NONE) ? readAmuRssi(&someRssi, beaconAmbNum, uvpBeaconNum, amuBeaconNum) : ret;
#endif
            
            //Stop the reference from transmitting.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_RESET, refAmbNum, uvpRefNum) : ret;    
    
            //Let's now iterate through the UVP's, ignoring ones that we don't want to calibrate.
            for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
            {
                if (!BIT_IN_MASK(uvpNum, uvpMask))
                {
                    continue;
                }
    
                //We skip the reference UVP if it's on this AMB.
                if ((ambNum == refAmbNum) && (uvpNum == uvpRefNum))
                {
                    continue;
                }
    
                //Start the UVP transmitting, then wait till it's ready.
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_SET, ambNum, uvpNum) : ret;
                ret = (ret == COTA_ERROR_NONE) ? waitForTxReady(ambNum, uvpNum): ret;         
    
                //Update the phase of the transmision signal to the conjugate of what was read earlier
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, ambNum, uvpNum) : ret;
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, ambNum, uvpNum) : ret;       
    
                //Read and update the pase for the beacon.
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, beaconAmbNum, uvpBeaconNum) : ret;
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, beaconAmbNum, uvpBeaconNum) : ret; 
    
                ret = (ret == COTA_ERROR_NONE) ? waitForPhaseDetectDone(beaconAmbNum, uvpBeaconNum): ret;
                ret = (ret == COTA_ERROR_NONE) ? readAmuPhase(&phaseAut, beaconAmbNum, uvpBeaconNum, amuBeaconNum) : ret;
    
#ifdef DEBUG_CALIBRATION
                ret = (ret == COTA_ERROR_NONE) ? readAmuRssi(&someRssi, beaconAmbNum, uvpBeaconNum, amuBeaconNum) : ret;
#endif
    
                //Stop transmitting on the UVP
                ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_RESET, ambNum, uvpNum) : ret;
    
                phaseOffset = phaseRef - phaseAut;
            
                if (phaseOffset < 0)
                {
                    phaseOffset += MAX_PHASE_OFFSET;
                }
            
                //Write the offset phase (otherwise known as the calibration phase) to the UVP
                ret = (ret == COTA_ERROR_NONE) ? WritePhaseOffset(phaseOffset, ambNum, uvpNum, ALL_AMU_MASK) : ret;
    
                //Calculate the delta square since the last calibration
                maxPhase = (phaseOffset > gLastCalPhase[UVP_INDEX(ambNum, uvpNum)]) ? phaseOffset : gLastCalPhase[UVP_INDEX(ambNum, uvpNum)];
                minPhase = (phaseOffset <= gLastCalPhase[UVP_INDEX(ambNum, uvpNum)]) ? phaseOffset : gLastCalPhase[UVP_INDEX(ambNum, uvpNum)];
                del1 = (int16_t)maxPhase - (int16_t)minPhase;                         //Calculate counter clockwise arc
                del2 = ((int16_t)minPhase - ((int16_t)maxPhase - MAX_PHASE_OFFSET));  //Calculate clockwise arc
                delmin = MIN(del1, del2);                                             //The minimum of the two arc is the delta
                gPhaseDeltaSquared += (delmin*delmin);                                //Accumulate the delta squared
                deltacount++;
                gLastCalPhase[UVP_INDEX(ambNum, uvpNum)] = phaseOffset;
            }
    
            //Take the beacon out of receive mode.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_RESET, beaconAmbNum, uvpBeaconNum) : ret;
    
            //unselect user phase determination of the beacon.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARB_PHASE_SEL, 0, beaconAmbNum, uvpBeaconNum) : ret;
    
            //restore the phase conjugate mode of the beacon
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(SEL_PHASE_CONJUGATE, PHASE_CONJUGATE_ENABLE, beaconAmbNum, uvpBeaconNum) : ret;
    
            //Enable the reference AMU on the beacon since it was changed earlier.
            ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(beaconAmbNum, uvpBeaconNum, NUM_TO_MASK(amuRefNum)) : ret; 
        }  
    }
    
    if (deltacount > 0)
    {
        gPhaseDeltaSquared /= deltacount;  //Average the delta squares
    }
          
    //Convert to cenitidegrees squared
    gPhaseDeltaSquared = ((FULL_ARC_IN_CENTIDEGREES*FULL_ARC_IN_CENTIDEGREES)*gPhaseDeltaSquared)/(MAX_PHASE_OFFSET*MAX_PHASE_OFFSET);
    
    //If we are checking for obstacles (gObstacleTimerHandle != NULL), and this is not the first
    //calibration (!gFirstCal) and the average delta square phase exceeds the threshold, we need to start
    //the obstacle timer.
    if (gObstacleTimerHandle && (!gFirstCal) && (gPhaseDeltaSquared > (TO_CENT((uint64_t)gObsThresh)*TO_CENT((uint64_t)gObsThresh))))
    {
        xTimerStart( gObstacleTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS) );
    }
    
    //Restore the RSSI_LOWTHRESH values
    ret = (ret == COTA_ERROR_NONE) ? WriteArrayUvps(RSSI_LOWTHRESH, minRssiThresh, TOTAL_UVPS, ambMask, uvpMask) : ret;

    //Enable all AMU's on the UVP's  
    ret = (ret == COTA_ERROR_NONE) ? UvpManyAmuEnable(ambMask, uvpMask, ALL_AMU_MASK ) : ret;

    //Restore power levels to what they were before calibration
    ret = (ret == COTA_ERROR_NONE) ? SetUvpPowerLevels(startPower, ambMask, uvpMask) : ret;

    gFirstCal = false;
    return ret;
}


/**
 * @brief This functions does static tune with the help of a debugger
 *
 * @note This function is meant to be operated with a debugger, putting break point located where the comments indicate
 * @todo Change this so it can be run without a debugger.
 *
 * ambMask A mask indicating which AMB's will be used during static tune.
 * uvpMask A mask indicating which UVP's will be used during static tune.
 * 
 * @return COTA_ERROR_NONE on success, otherwise an error code of type #CotaError_t.
 */ 
CotaError_t StaticTune(AmbMask_t ambMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
    AmbNum_t ambNum;
    UvpNum_t uvpNum;
    uint32_t temp;
    uint32_t digtemp;
    int16_t ctemp;
    char buff[128];

    AmbSpiEnable(ambMask);

    ret = AmbSelectUvp(ALL_UVP_NUM);

    AmbEnableRx(true);

    vTaskDelay( pdMS_TO_TICKS(1));

    //Set the first break point here. The transmitter will be in the receive state.  Turn on the beacon.  Then hit go, in the debuger
    AmbEnablePuAll(true);
    AmbEnablePuAll(false);

    vTaskDelay( pdMS_TO_TICKS(1000));  //Wait a second to to allow phase detect to be done.

    AmbEnableRx(false);

    //Set the next break point here, turn of the beacon and hit go.   
    if (ambMask == ALL_AMB_MASK)
    {
        AmbEnableTx(true);
    }
    else
    {
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_TXCTRL, UVP_TXCTRL_SET, ambMask, ALL_UVP_MASK) : ret;
    }

    vTaskDelay( pdMS_TO_TICKS(1));

    AmbEnablePuAll(true);
    AmbEnablePuAll(false);   

    while (ret == COTA_ERROR_NONE)
    {  
        for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); ambNum++)
        {
            if (!BIT_IN_MASK(ambNum, ambMask))
            {
                continue;
            }
            for (uvpNum = 0; uvpNum < UVPS_PER_AMB; uvpNum++)
            {
                ret = UvpRegisterRead(TEMPADC_OUT, &temp, ambNum, uvpNum);
                digtemp = (int16_t)((temp & UVP_TEMP_SIGN_BIT) ? (temp | UVP_TEMP_SIGN_EXT) : temp);  //Convert to a signed 16 bit value
                ctemp = (int16_t)(((UVP_TEMP_SLOPE_NUM * ((int32_t)digtemp))/UVP_TEMP_SLOPE_DEN) + UVP_TEMP_OFFSET);        //Convert to a value in centidegrees C         
                snprintf(buff,sizeof(buff), "a = %d u=%d t=%i\r\n", ambNum, uvpNum, ctemp);
                PostStringForTransmit(buff);
                vTaskDelay( pdMS_TO_TICKS(100));
            }
        } 
        vTaskDelay( pdMS_TO_TICKS(5000));
    }

    //Set the last break point here
    if (ambMask == ALL_AMB_MASK)
    {
        AmbEnableTx(false);
    }
    else
    {
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_TXCTRL, UVP_TXCTRL_RESET, ambMask, ALL_UVP_MASK) : ret;
    }
    
    return ret;
}

/**
 * @brief Initializes the timers for auto calibration
 *
 * @return COTA_ERROR_NONE on success; an error code on failure
 */
CotaError_t InitializeAutoCalibration(void)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint16_t timerVal;
    
    /*
      Long timer
     */
    CfgGetParam(CFG_AUTO_CALIBRATION_PERIOD, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gLongCalPeriodTimerHandle)
        {
            xTimerStop(gLongCalPeriodTimerHandle, NO_WAIT);
            xTimerChangePeriod(gLongCalPeriodTimerHandle, MIN_TO_TICKS(timerVal), NO_WAIT);             
        }
        else
        {
            gLongCalPeriodTimerHandle = xTimerCreateStatic(
                "Long calibration timer",
                MIN_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                longTimerCallback,
                &gLongCalPeriodTimerBuffer);
            
            if (gLongCalPeriodTimerHandle)
            {
                xTimerStart( gLongCalPeriodTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS) );
            }        
            else
            {
                ret = COTA_ERROR_CTRL_TIMERS_INIT_FAILED;
            }
        }
    }
    else
    {
        gLongCalPeriodTimerHandle = NULL;
    }  

    /*
      short calibration period timer
     */
    CfgGetParam(CFG_SHORT_CALIBRATION_PERIOD, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gShortCalPeriodTimerHandle)
        {
            xTimerStop(gShortCalPeriodTimerHandle, NO_WAIT);
            xTimerChangePeriod(gShortCalPeriodTimerHandle, SEC_TO_TICKS(timerVal), NO_WAIT); 
        }
        else
        {
            gShortCalPeriodTimerHandle = xTimerCreateStatic(
                "Short calibration period",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                shortTimerCallback,
                &gShortCalPeriodTimerBuffer);

            if (!gShortCalPeriodTimerHandle)
            {
                ret = COTA_ERROR_CTRL_TIMERS_INIT_FAILED;
            }
        }
    }
    else
    {
        gShortCalPeriodTimerHandle = NULL;
    }

    /**
     * Obstacle timer.  Obstacle require short calibration to be enabled to work.
     */ 
    CfgGetParam(CFG_OBSTRUCTION_ANGLE_THRESHOLD, &gObsThresh, sizeof(gObsThresh));
    
    if ((gObsThresh > 0) && (timerVal > 0))
    {
        if (gObstacleTimerHandle)
        {
            xTimerStop(gObstacleTimerHandle, NO_WAIT);
            xTimerChangePeriod(gObstacleTimerHandle, SEC_TO_TICKS(timerVal), NO_WAIT); 
        }
        else
        {
            gObstacleTimerHandle = xTimerCreateStatic(
                "Obstacle timer",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                obstacleTimerCallback,
                &gObstacleTimerBuffer);
            
            if (!gObstacleTimerHandle)
            {
                ret = COTA_ERROR_CTRL_TIMERS_INIT_FAILED;
            }
        }
    }
    else
    {
        gObstacleTimerHandle = NULL;
    }
       
    /*
      short calibration duration timer
     */
    CfgGetParam(CFG_SHORT_CALIBRATION_DURATION, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gChargeStartTimerHandle)
        {
            xTimerStop(gChargeStartTimerHandle, NO_WAIT);
            xTimerChangePeriod(gChargeStartTimerHandle, SEC_TO_TICKS(timerVal), NO_WAIT); 
        }
        else
        {
            gChargeStartTimerHandle = xTimerCreateStatic(
                "Charge start period",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                chargeStartTimerCallback,
                &gChargeStartTimerBuffer);
            
            if (!gChargeStartTimerHandle)
            {
                ret = COTA_ERROR_CTRL_TIMERS_INIT_FAILED;
            }
        }
    }
    else
    {
        gChargeStartTimerHandle = NULL;
    }     
     
    /*
      temperature sampling duration timer
     */
    CfgGetParam(CFG_CAL_TEMP_SMPL_PERIOD, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gTemperatureSampleTimerHandle)
        {
            xTimerStop(gTemperatureSampleTimerHandle, NO_WAIT);
            xTimerChangePeriod(gTemperatureSampleTimerHandle, SEC_TO_TICKS(timerVal), NO_WAIT); 
        }
        else
        {
            gTemperatureSampleTimerHandle = xTimerCreateStatic(
                "Temperature sample period",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                temperatureSampleTimerCallback,
                &gTemperatureSampleTimerBuffer);
            
                if (gTemperatureSampleTimerHandle)
                {
                    xTimerStart( gTemperatureSampleTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS) );
                }
                else
                {
                    ret = COTA_ERROR_CTRL_TIMERS_INIT_FAILED;
                }
        }
    }
    else
    {
        gTemperatureSampleTimerHandle = NULL;
    }   

    CfgGetParam(CFG_CAL_TEMP_THRESHOLD, &gTempThresh, sizeof(gTempThresh));
        
    return ret;
}


/**
 * @brief The temperature sample period timer callback
 * 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void temperatureSampleTimerCallback(TimerHandle_t  timer)
{
    uint32_t callbackCnt;
    
    // Increment the timer count, which is the number of times the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);
    
    gTemperatureSample = true;
}

/**
 * @brief The charge start timer callback
 * 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void chargeStartTimerCallback(TimerHandle_t  timer)
{
    uint32_t callbackCnt;
    
    // Increment the timer count, which is the number of times the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);
    
    gChargingBegins = false;
}

/**
 * @brief The short period calibration timer callback
 * 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void shortTimerCallback(TimerHandle_t  timer)
{
    uint32_t callbackCnt;
    
    // Increment the timer count, which is the number of times the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);
    
    gShortCalReady = true;
}

/**
 * @brief The long period calibration timer callback
 * 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void longTimerCallback(TimerHandle_t  timer)
{
    uint32_t callbackCnt;
    
    // Increment the timer count, which is the number of times the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);
    
    gLongCalReady = true;
}

/**
 * @brief The obstacle period calibration timer callback
 * 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void obstacleTimerCallback(TimerHandle_t  timer)
{
    uint32_t callbackCnt;
    
    // Increment the timer count, which is the number of times the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);
    
    gObstacleDetected = true;
}

/**
 * @brief Sets a flag that will cause CheckCalibrationNeed to ctrl task
 *        to do a calibration on its call.
 */ 
void SetManualCalibrationFlag(bool enable)
{
    gDoCalibration = enable;
}

/**
 * @brief  Determines if a calibration is neeeded
 *
 * @return true; 
 */
bool CheckCalibrationNeed(void)
{
    bool ret = false;
    
    if (gDoCalibration)
    {
        ret = true;
    }

    if (gLongCalPeriodTimerHandle)  //Long and short period calibation is only supported if long period is greater than 0
    {
        if (gTpsActive && gChargingBegins && gShortCalPeriodTimerHandle)
        {
            if (gShortCalReady)
            {
                gShortCalReady = false;
                ret = true;
                xTimerStart( gShortCalPeriodTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS) );
            }
          
            gTpsActive = false;
        }
        else
        {
            if (gLongCalReady)
            {
                gLongCalReady = false;
                ret = true;
                xTimerStart( gLongCalPeriodTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS) );
            }
        }
        
        if (gObstacleTimerHandle && gObstacleDetected)
        {
            gObstacleDetected = false;
            ret = true;
        }
    }

    if (gTemperatureSample && gTemperatureSampleTimerHandle)
    {
        int16_t maxDelta;
        gTemperatureSample = false;
        xTimerStart( gTemperatureSampleTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS) ); 

        if ((UpdateUvpTemperatureCache(&maxDelta, true) == COTA_ERROR_NONE) && (maxDelta > TO_CENT(gTempThresh)))
        {
            ret = true;
        }
    }
    
    return ret;
}

/**
 * @brief Sets the TPS active flag, so auto calibration know if the charging is active
 */
void SetTpsActiveForAutoCal(void)
{
    gTpsActive = true;
}

/**
 * @brief Start the charging begin timer
 */
void SetChargingBeginForAutoCal(void)
{
    gChargingBegins = true;
    if (gShortCalPeriodTimerHandle && gChargeStartTimerHandle)
    {
        xTimerStop(gChargeStartTimerHandle, NO_WAIT);
        xTimerStart( gChargeStartTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS) );
        xTimerStop(gShortCalPeriodTimerHandle, NO_WAIT);    
        xTimerStart( gShortCalPeriodTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS) );
    }
}
