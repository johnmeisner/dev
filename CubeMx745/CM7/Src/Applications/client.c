/**
 * @file       client.c
 *
 * @brief      Defines helper functions for working with clients.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 * @note       A client has two methods of identification, a longId (#ExtdAddr_t) and
 *             a shortId (#ShrtAddr_t).
 *               - A longId is assigned when the client is manufactured, and is given
 *                   in accordance with the EUI-64 IP standard, and is assigned through
 *                   IEEE.
 *               - A shortId is assigned by the transmitter when the client is registered
 *                   with the system. Having a shortId is important because it reduces
 *                   network traffic, and is just overall easier to work with.
 *
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "orion_config.h"
#include "orion_utilities.h"
#include "CotaCommonTypes.h"
#include "client.h"
#include "cqt_scheduler.h"
#include "client_manager.h"
#include "debug_on.h"

const uint64_t TI_MAL_ADDR_BASE = (uint64_t)0x00124b;

///< These functions are provided by client_manager.c
PRIVATE bool GetClient(ExtdAddr_t longId, Client_t *client);
PRIVATE bool SaveClient(Client_t client);
PRIVATE bool GetLongId(ShrtAddr_t shortId, ExtdAddr_t *longId);

static bool isOkToIncrementFailCount(Client_t client);
static CqtSchedule_t gCqtSchedule;                           ///< The structure holding the query schedule

#if defined(USE_DEMO_BUILD)
static bool isOkToIncrementFailCount(Client_t client)
{
    return (!(client.state == RCVR_STATE_SILENT));
}
#elif defined(USE_FOREVER_TRACKER_BUILD)
static bool isOkToIncrementFailCount(Client_t client)
{
    return true;
}
#else
    #error Undefined build target!
#endif
/**
 * @brief Any Rxs left in the schedule after the query period have
 *        failed to respond with query data. Here is where we make
 *        note of that.
 */
void MarkAllRemainingScheduledQueriesAsFailed(void)
{
    ExtdAddr_t longId;
    Client_t client;
    uint32_t currTime = xTaskGetTickCount();

    for (uint8_t ii = 0; ii < gCqtSchedule.header.totalNumClients; ii++)
    {
        if (GetLongId(gCqtSchedule.clientQueryEntry[ii].shortAddr, &longId))
        {
            if (GetClient(longId, &client) && isOkToIncrementFailCount(client))
            {
                client.commFailCount++;
                LogWarning("Failed query response for 0x%016llx. Fail count: %d\r\n",
                           *(uint64_t*)&longId,
                           client.commFailCount);
                SaveClient(client);
            }
        }
    }
}

/**
 * @brief Determines if the longId is valid I.e non-zero
 * @param longId The longId that will be examined
 * @return true if the longId is valid, false if otherwise.
 */
bool IsLongValid(ExtdAddr_t longId)
{
    bool retVal;
    uint8_t i;

    retVal = false;

    for ( i = 0; i < sizeof(ExtdAddr_t); i++ )
    {
        // An invalid longId is all 0s
        if ( longId.bytes[i] != 0 )
        {
            retVal = true;
            break;
        }
    }

    return retVal;
}

/**
 * @brief Checks if the longId of a receiver is a valid TI MAL address
 *        specific to TI radios
 */
bool IsLongIdValidTiMALAddr(ExtdAddr_t longId)
{
    return (*(uint64_t *) &longId >> 40) == TI_MAL_ADDR_BASE;
}

/**
 * @brief compares two longIds and compares the values
 * @param longId1 The first longId for comparison
 * @param longId2 The second longId for comparison
 * @return 0 if equal, < 0 if longId1 < longId2, and 0 > if longId1 > longId2
 */
int32_t LongCmp(ExtdAddr_t longId1, ExtdAddr_t longId2)
{
    return memcmp((void * ) &longId1, (void * ) &longId2, sizeof(ExtdAddr_t));
}

/**
 * @brief determines if the Receiver has been registered.
 * @param rcvr a pointer to a receiver structure
 * @return true if the Receiver/Client_t shortId is valid
 */
bool IsRcvrRegistered(Client_t *rcvr)
{
    return  IsShortValid(rcvr->shortId);
}

/**
 * @brief determines if the shortId is valid
 * @param shortId The shortId we are checking for validity
 * @return true if the shortId is valid, not true if otherwise
 */
bool IsShortValid(ShrtAddr_t shortId)
{
    return ( shortId != INVALID_SHORT_ID_0 ) && ( shortId != INVALID_SHORT_ID_FFFF );
}

/**
 * @brief Returns boolean if the client is ready for queries.
 * @param client The client in question.
 * @return boolean if the client is ready for query
 */
#if defined(USE_DEMO_BUILD)
bool IsRcvrAbleToQuery(Client_t *client)
{
    return ( (client->state == RCVR_STATE_CHARGING) ||
             (client->state == RCVR_STATE_READY) ||
             (client->state == RCVR_STATE_SILENT) );
}
#elif defined(USE_FOREVER_TRACKER_BUILD)
bool IsRcvrAbleToQuery(Client_t *client)
{
    return ( (client->state == RCVR_STATE_NOT_READY_TO_CHARGE) ||
             (client->state == RCVR_STATE_READY_TO_CHARGE) ||
             (client->state == RCVR_STATE_SCHEDULED));
}
#else
    #error Undefined build target!
#endif

/**
 * @brief Returns boolean if the client is ready for communication
 * @param client The client in question.
 * @return boolean if the client is ready for query
 */
#if defined(USE_DEMO_BUILD)
bool IsRcvrReadyForComm(Client_t *client)
{
    return ( (client->state == RCVR_STATE_REGISTERED) ||
             (client->state == RCVR_STATE_JOINED) ||
             (client->state == RCVR_STATE_READY) ||
             (client->state == RCVR_STATE_CHARGING) );
}
#elif defined(USE_FOREVER_TRACKER_BUILD)
bool IsRcvrReadyForComm(Client_t *client)
{
    return ( (client->state == RCVR_STATE_NOT_READY_TO_CHARGE) ||
             (client->state == RCVR_STATE_READY_TO_CHARGE) ||
             (client->state == RCVR_STATE_SCHEDULED));
}
#else
    #error Undefined build target!
#endif

/**
 * @brief Returns boolean if the client is ready to be charged
 * @param client The client in question.
 * @return boolean if the client is ready for charging
 */
#if defined(USE_DEMO_BUILD)
bool IsRcvrReadyForCharging(Client_t *client)
{
    return (client->state == RCVR_STATE_CHARGING) && client->qStatus.status.statusFlags.powerRq;
}
#elif defined(USE_FOREVER_TRACKER_BUILD)
bool IsRcvrReadyForCharging(Client_t *client)
{
    return ( (client->state == RCVR_STATE_READY_TO_CHARGE) ||
             (client->state == RCVR_STATE_SCHEDULED));
}
#else
    #error Undefined build target!
#endif

/**
 * @brief Returns boolean if the client has custom query data
 * @param client The client in question.
 * @return boolean if the client has custom query data
 */
bool DoesRcvrHaveCustomDataField(Client_t client)
{
    return ( (client.qStatus.header.queryType == RCVR_QUERY_CUSTOM) ||
             (client.qStatus.header.queryType == RCVR_QUERY_OPTIMIZED));
}

#if defined(USE_DEMO_BUILD)

/**
 * @brief Function does nothing, provided for symmetry with other build targets
 * @param queryData Query response data for this receiver
 * @param client The receiver in question.
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t DoBuildSpecificActionsAfterQueryResponse_CheckState(ClientQueryDataCstm_t* queryData, Client_t *client)
{
    CotaError_t ret = COTA_ERROR_NONE;

    if (queryData == NULL)
    {
        ret = COTA_ERROR_NULL_VALUE_PASSED;
    }
    else if (client == NULL)
    {
        ret = COTA_ERROR_NULL_VALUE_PASSED;
    }
    else
    {
        // @todo build-specific state change logic, if any
    }

    return ret;
}

/**
 * @brief Checks receiver power and calls LogWarning() if power is invalid
 * @param client The receiver in question.
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t DoBuildSpecificActionsAfterQueryResponse_CheckRcvrPower(Client_t *client, uint32_t lastTick)
{
    CotaError_t ret = COTA_ERROR_NONE;

    if (client == NULL)
    {
        ret = COTA_ERROR_NULL_VALUE_PASSED;
    }
    else
    {
        if ( (client->state == RCVR_STATE_CHARGING) &&
             client->qStatus.status.statusFlags.powerRq &&
             ((client->qStatus.status.peakPower == INVALID_POWER_VALUE) ||
              (client->qStatus.status.avgPower == INVALID_POWER_VALUE)))
        {
          LogWarning("No power 0x%016llx time = %llu.%03llu seconds since last query = %llu ms\r\n",
                     *(uint64_t*)&client->longId,
                     TICKS_TO_MS(client->lastCommTimeTicks)/MS_IN_SEC,
                     TICKS_TO_MS(client->lastCommTimeTicks) % MS_IN_SEC,
                     TICKS_TO_MS(TimeDiffTicks(client->lastCommTimeTicks, lastTick)));
        }
    }

    return ret;
}

#elif defined(USE_FOREVER_TRACKER_BUILD)

/**
 * @brief Sets receiver state to RCVR_STATE_READY_TO_CHARGE or RCVR_STATE_NOT_READY_TO_CHARGE
 * @param queryData Query response data for this receiver
 * @param client The receiver in question.
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t DoBuildSpecificActionsAfterQueryResponse_CheckState(ClientQueryDataCstm_t* queryData, Client_t *client)
{
    CotaError_t ret = COTA_ERROR_NONE;

    if (queryData == NULL)
    {
        ret = COTA_ERROR_NULL_VALUE_PASSED;
    }
    else if (client == NULL)
    {
        ret = COTA_ERROR_NULL_VALUE_PASSED;
    }
    else if (queryData->status.trackerStatusFlags.sleepFlag)
    {
        // If a receiver is scheduled, that means its in a round robin
        // scheduler to charge. In this time, they can respond to
        // queries, but we don't want to over write the SCHEDULED
        // state, as set in ft_tps_scheduler.c
        if (client->state != RCVR_STATE_SCHEDULED)
        {
            client->state = RCVR_STATE_READY_TO_CHARGE;
        } else {} // nothing
    }
    else
    {
        client->state = RCVR_STATE_NOT_READY_TO_CHARGE;
    }

    return ret;
}

/**
 * @brief Function does nothing, provided for symmetry with other build targets
 * @param client The receiver in question.
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t DoBuildSpecificActionsAfterQueryResponse_CheckRcvrPower(Client_t *client, uint32_t lastTick)
{
    CotaError_t ret = COTA_ERROR_NONE;

    if (client == NULL)
    {
        ret = COTA_ERROR_NULL_VALUE_PASSED;
    }
    else
    {
        // @todo build-specific receiver power check logic, if any
    }

    return ret;
}

#else
    #error Undefined build target!
#endif
