/**
 * @file       gather_and_vote_task.h
 *
 * @brief      Gather and Vote task header file
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _GATHER_AND_VOTE_TASK_INCLUDE_H_
#define _GATHER_AND_VOTE_TASK_INCLUDE_H_

#include <stdint.h>
#include <stdbool.h>
#include "FreeRTOS.h"
#include "semphr.h"
#include "error.h"
#include "ctrl_interface.h"


bool VoteFailed(void);
bool VoteEnabled(void);
void ResetRssiVote(void);
extern SemaphoreHandle_t gGatherAndVoteSem;
BaseType_t GatherAndVoteInit(void);
CotaError_t SetUvpThreshEn(bool enable);
void GatherAndVoteTask(void *pvParameters);
void SetRssiVoteEn(bool enable);
void SetRssiThresh(MsgSetRssiThreshold_t* rssiThresh);
void StartGatherAndVoteTask(void);
void CreateGatherAndVoteTask(void);
void FreezeTpsTransmit(MsgStaticCharge_t* msg);
bool IsTpsFrozen(void);
void FreeTps(void);
void PrintRssiReport(void);
void PrintAmbDisabled(void);
#endif /*#ifndef _GATHER_AND_VOTE_TASK_INCLUDE_H_*/
