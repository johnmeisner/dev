/**
 * @file       generic_sm.c
 *
 * @brief      The generic initialization state machine for the control
 *             task. This contains the debug state, the init state,
 *             and the ready state. 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "stm32h7xx_it.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "error.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "sysclk_driver.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"
#include "amb_control.h"
#include "ccb_temperature.h"
#include "i2c_multiplex.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "fan_driver.h"
#include "calibrator.h"
#include "power_level.h"
#include "cqt_scheduler.h"
#include "tps_scheduler.h"
#include "rpi_msg_interface.h"
#include "power_monitor.h"
#include "light_ring_interface.h"
#include "gather_and_vote_task.h"
#include "door_driver.h"
#include "query_sm.h"          
#include "tps_sm.h"            
#include "proxy_test_sm.h"
#include "power_monitor_sm.h"  
#include "proxy_test_sm.h"
#include "fan_sm.h"
#include "app_cmd_sm.h"        
#include "generic_sm.h"

#include "Common/proxy_test_sm.h"
#include "Common/power_monitor_sm.h"  
#include "Common/proxy_test_sm.h"
#include "Common/fan_sm.h"
#include "Common/app_cmd_sm.h"
#include "generic_sm.h"

#include "reset_cause.h"

#if defined(USE_DEMO_BUILD)
#include "Demo/demo_state_machine.h"
#include "Demo/disc_sm.h"           
#include "Demo/join_sm.h"           
#include "Demo/tpc_sm.h"            
#include "Demo/light_ring_sm.h"
#include "Demo/disconnect_sm.h"

#elif defined(USE_FOREVER_TRACKER_BUILD)
#include "ForeverTracker/enumeration.h"
#include "ForeverTracker/wait_for_sleep.h"
#include "ForeverTracker/receiver_removal_sm.h"
#else
#error Build target not defined properly
#endif


#include "ForeverTracker/forever_tracker_state_machine.h"

static uint8_t gDebugVal   = 0;     ///< Debug mode value
static uint8_t gFatalError = 0;     ///< If true, send system to an error state
static bool    gSystemInit = false; ///< true when the control task has successfully initialized

bool InitToRdyCheck(void)
{
    gSystemInit = PrxIsReady();

#if defined(USE_DEMO_DISC)
    gSystemInit = (gSystemInit == true ) ? StartDiscoveryTimers() : false;
#endif

#if defined(USE_DEMO_JOIN)
    gSystemInit = (gSystemInit == true ) ? StartJoinTimers()      : false;
#endif

#if defined(USE_DEMO_TPC)
    gSystemInit = (gSystemInit == true ) ? StartTpcTimers()       : false;
#endif

#if defined(USE_COMMON_QUERY)
    gSystemInit = (gSystemInit == true ) ? StartQueryTimers()     : false;
#endif

#if defined(USE_COMMON_FAN)
    gSystemInit = (gSystemInit == true ) ? StartFanTimers()       : false;
#endif

#if defined(USE_DEMO_LIGHT_RING) && defined(USE_DEMO_BUILD)
    gSystemInit = (gSystemInit == true ) ? StartLightRingTimers() : false;
#endif

#if defined(USE_COMMON_PROXY_TEST)
    gSystemInit = (gSystemInit == true ) ? StartProxyTestTimers() : false;
#endif

#if defined(USE_FOREVER_TRACKER_RECEIVER_REMOVAL)
    gSystemInit = (gSystemInit == true ) ? StartReceiverRemovalTimers() : false;
#endif

    
    return gSystemInit;
}

void ErrorEntry(void)
{
    LogFatal("Control Task Error state. Transmitter will cease to function.\r\n");
    CompleteShutdownChecklist();
    HardFault_Handler();
}

void ReadyEntry(void)
{
    // Intentionally empty. 
}

void InitEntry(void)
{
    CotaError_t ret = COTA_ERROR_NONE;
    AmbMask_t validAmbs;
    uint8_t clkIdleDisable = 0;

    if (!WaitForPowerGood(POWER_SUPPLY_WAIT_TIMEOUT))
    {
        LogError("Power supplies failed to come up\r\n");
        ret = COTA_ERROR_POWER_FAILURE;
    }
    
    LogInfo("\r\n\r\nStart Initialization\r\n");
    PrxRestart(); //Start the proxy soon so that it can startup while we do other things

    if (NvmInit() == COTA_ERROR_NONE)
    {
        LogInfo("System configuration loaded successfully\r\n");
    }
    else
    {
        LogError("Failed to load system configuration\r\n");      
    }

    if (ret != COTA_ERROR_NONE)
    {
        LogError("Control task timer failed to init\r\n");
    }

    ret = (ret == COTA_ERROR_NONE) ? InitializeAutoCalibration() : ret;
    
    if (ret != COTA_ERROR_NONE)
    {
        LogError("Failed to initialize auto calibration\r\n");
    }  
    
    ret = (ret == COTA_ERROR_NONE) ? CfgGetParam(CFG_VALID_AMB_MASK, &validAmbs, sizeof(AmbMask_t)): ret;

    // For some builds (i.e. ForeverTracker) needs time for the
    // hardware to become ready before we can do work
    // initializing. This delay accomplishes that.
    vTaskDelay(pdMS_TO_TICKS(100));

    // Initialize AMB's
    AmbControlInit(validAmbs);
    
    AmbEnable(0);  //Disable all AMB's before initializing the clock
    vTaskDelay( pdMS_TO_TICKS(1000));
    ret = (ret == COTA_ERROR_NONE) ? AmbUvpEnable(AMB_DISABLE_ALL) : ret;  

    ResetI2CMultiplexer();
    
    ret = (ret == COTA_ERROR_NONE) ? InitFansAndTemps() : ret;  //Let's make sure the fans work before we turn on AMB's
    
    ret = (ret == COTA_ERROR_NONE) ? SystemClockInit() : ret;
    if (ret != COTA_ERROR_NONE)
    {
        LogError("System clock failed to init\r\n");
    }
      
    ret = (ret == COTA_ERROR_NONE) ? UpdateTransmitFrequency() : ret;

#ifdef USE_FOREVER_TRACKER_BUILD
    //We can't initialize the UVP's here because the door may be opened, and AMB's are disabled in hardware.
    ret = (ret == COTA_ERROR_NONE) ? InitDoorDriver(): ret;
    EnumerationInit();
    WaitForSleepInit();
#else
    ret = (ret == COTA_ERROR_NONE) ?  StartUvps(AmbGetValidAmbs()) : ret;
    SetManualCalibrationFlag(true);
#endif
    
    CfgGetParam(CFG_SYSCLK_IDLE_DISABLE, &clkIdleDisable, sizeof(uint8_t));
    if (clkIdleDisable)
    {
        EnableSystemClock(false);               
    }
   
    if (InitializeCcbPowerMonitors() != COTA_ERROR_NONE)
    {
        LogError("Power monitor failed to initialize\r\n");      
    }
    
    //  StartFanDriver(); //@todo address this when we visit the fan driver again.
    
#if defined(USE_DEMO_BUILD)
    StartGatherAndVoteTask(); 
#endif  
    
    POST_COTA_ERROR(ret);

    SetEmergencyFanOn(ret != COTA_ERROR_NONE);  //Turn fans on full if initialization fails.

    if (ret == COTA_ERROR_NONE)
    {
        //Let's update the fan states and uvp temperatures
        UpdateUvpTemperatureCache(NULL, true);  //Update the temperature in the cache        
        UpdateFansOkMaskAndSpeed();
    }

    LRLightRingInit();
    
    ForceLogIfWatchdogReset();    

    LogLevel_t systemLogLevel;
    CfgGetParam(CFG_SYS_LOG_LEVEL, &systemLogLevel, sizeof(systemLogLevel));
    LogInit(systemLogLevel);

    // Ask the proxy for init data.
    PrxSendReqInfo();
    
    uint8_t queryCount;
    CfgGetParam(CFG_MAX_QUERIES, &queryCount, sizeof(queryCount));  
    UpdateMaxQueryCount(queryCount);
    
    //Turn off the red LED and light up a green LED
    HAL_GPIO_WritePin(MCU_LED_7_GPIO_Port, MCU_LED_7_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(MCU_LED_4_GPIO_Port, MCU_LED_4_Pin, GPIO_PIN_SET);

    uint16_t commMaxFailCount;
    CfgGetParam(CFG_MAX_QUERY_FAIL_COUNT, &commMaxFailCount, sizeof(commMaxFailCount));  
    CmSetCommMaxFailCount(commMaxFailCount);

#if defined(USE_DEMO_DISC)
    DiscStateInit();
#endif
 
#if defined(USE_DEMO_JOIN)
    JoinStateInit();
#endif

#if defined(USE_DEMO_TPC)
    TpcStateInit();
#endif

#if defined(USE_COMMON_QUERY)
    QueryStateInit();
#endif

#if defined(USE_COMMON_TPS)
    TpsStateInit();
#endif

#if defined(USE_DEMO_LIGHT_RING) && defined(USE_DEMO_BUILD)
    LightRingStateInit();
#endif

#if defined(USE_DEMO_DISCONNECT)
    DisconnectStateInit();
#endif
    
#if defined(USE_COMMON_PROXY_TEST)
    ProxyTestStateInit();
#endif

#if defined(USE_COMMON_FAN)
    FansStateInit();
#endif

#if defined(USE_COMMON_APP_COMMAND)
    AppCmdStateInit();
#endif

#if defined(USE_FOREVER_TRACKER_RECEIVER_REMOVAL)
    ReceiverRemovalStateInit();
#endif
}

void DebugEntry(void)
{
    LogInfo("Debug state entered\r\n");
    LRLightRingUpdate(LR_STATE_DEBUG);
}

bool InitToDebugCheck (void)
{   
    uint8_t debugVal;
    
    CfgGetParam(CFG_DEBUG_MODE, (void*)&debugVal, sizeof(debugVal));
    
    return debugVal;
}

bool RdyToDebugCheck  (void)
{ 
    uint8_t debugVal;
    
    CfgGetParam(CFG_DEBUG_MODE, (void*)&debugVal, sizeof(debugVal));
    
    return debugVal;
}

bool DebugToInitCheck (void)
{   
    uint8_t debugVal;
    
    CfgGetParam(CFG_DEBUG_MODE, (void*)&debugVal, sizeof(debugVal));
    
    return !debugVal;
}

bool RdyToErrorCheck(void)
{
    return gFatalError; 
}

void DebugToInitExit(void)
{
    LogInfo("Leave debug mode\r\n");
}

void SetDebugMode(bool val)
{
    gDebugVal = (uint8_t)val;
    CfgSaveParam(CFG_DEBUG_MODE, (void*)&gDebugVal, sizeof(gDebugVal));
}

bool GetDebugMode(void)
{
    return (bool)gDebugVal;
}

void HaltSystem(void)
{
    gFatalError = true;
}

/**
 * @brief Complete a series of action before we can determine its safe
 *        to reset the Orion MCU. 
 */
void CompleteShutdownChecklist(void)
{
    // Make sure the EEPROM has finished all write operations
    NvmLock();
    __disable_irq();
    vTaskSuspendAll();
}

void RebootMCU(void)
{
    CompleteShutdownChecklist();
    NVIC_SystemReset();
}

void  ForceLogIfWatchdogReset(void)
{
    ResetCause_t reset_cause;   
    LogLevel_t level;
    
    level = GetSystemLevel();
    
    reset_cause = ResetCauseGetType();    
    SetSystemLevel(LOG_INFO);    
    if (reset_cause == RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET) LogInfo("Watchdog Reset\r\n");  

    SetSystemLevel(level);
}
