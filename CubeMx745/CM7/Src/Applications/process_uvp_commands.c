/**
* @file       process_uvp_commands.c
*
* @brief      Processes the UVP commands from the ctrl_interface
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*/

#include "stdlib.h"
#include "FreeRTOS.h"
#include "projdefs.h"
#include "task.h"
#include "semphr.h"
#include "error.h"
#include "string.h"
#include "stdio.h"
#include "uart_driver.h"
#include "orion_config.h"
#include "cli_task.h"
#include "client.h"
#include "client_manager.h"
#include "process_uvp_commands.h"
#include "cli_task.h"
#include "uvp_driver.h"
#include "rpi_msg_interface.h"
#include "ctrl_interface.h"

/**
 * @brief Process a request to write to one or more UVP registers
 *
 * @param uvpWriteInfo Information needed to write to UVP registers
 *
 * @return COTA_ERROR_NONE if successful, otherwise an error code is returned.
 */
CotaError_t ProcessUvpWriteCommand(UvpRegWrite_t* uvpWriteInfo)
{
    UvpReg_t uvpReg;

    CotaError_t ret;
    
    uvpReg.colAddr = uvpWriteInfo->regInfo.addr;
    uvpReg.offset  = uvpWriteInfo->regInfo.offset;
    uvpReg.pageIndex = uvpWriteInfo->regInfo.page;
    uvpReg.size = uvpWriteInfo->regInfo.size;
    
    ret =  WriteManyUvps(uvpReg, uvpWriteInfo->val, uvpWriteInfo->regInfo.id.ambMask,  uvpWriteInfo->regInfo.id.uvpMask);      

    if (ret == COTA_ERROR_NONE)
    {
        PostPrintf("Success\r\n");
    }
    
    return ret;
}

/**
 * @brief Process a request to read from one or more UVP registers
 *
 * @param uvpReadInfo Information needed to read from UVP registers
 *
 * @return COTA_ERROR_NONE if successful, otherwise an error code is returned.
 */
CotaError_t ProcessUvpReadCommand(UvpRegRead_t* uvpReadInfo)
{
    UvpReg_t    uvpReg;
    AmbMask_t   ambMask;
    UvpMask_t   uvpMask;
    CotaError_t ret;
    uint32_t    data[TOTAL_UVPS];
    UvpNum_t    uvp;
    AmbNum_t    amb;
    CtrlResp_t  resp;
    uint16_t    len;
    
    uvpReg.colAddr = uvpReadInfo->addr;
    uvpReg.offset  = uvpReadInfo->offset;
    uvpReg.pageIndex = uvpReadInfo->page;
    uvpReg.size = uvpReadInfo->size;
    
    ambMask = uvpReadInfo->id.ambMask;
    uvpMask = uvpReadInfo->id.uvpMask;

    resp.id  = CTRL_UVP_DATA;
    resp.err = COTA_ERROR_NONE;

    // The length is set to the values of the total message size. This
    //   size is the size of the id and err as shown, but also
    //   includes the "read" data later in this function
    len = sizeof(CTRL_UVP_DATA) +   
          sizeof(COTA_ERROR_NONE) +
          sizeof(amb) +
          sizeof(uvp) +
          sizeof(data[0]);
          
    ret = ReadArrayUvps(uvpReg, data, TOTAL_UVPS, ambMask, uvpMask);
    for (amb = 0; amb < MAX_NUM_AMB; amb++)
    {
        if (BIT_IN_MASK(amb,ambMask))
        {
            for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
            {
                if (BIT_IN_MASK(uvp, uvpMask))
                {
                    resp.uvpRead.uvpId.ambNum = amb;
                    resp.uvpRead.uvpId.uvpNum = uvp;
                    resp.uvpRead.val = data[UVP_INDEX(amb,uvp)];
                    RpiSendMessage((uint8_t*)&resp, len);

                    // To not overload the system with messages, we add a delay.
                    vTaskDelay(pdMS_TO_TICKS(5));
                    PostPrintf("AMB: %d UVP: %d VAL: %d\r\n", 
                               amb,uvp,data[UVP_INDEX(amb,uvp)]);
                }
            }
        }
    }     
    
    if (ret == COTA_ERROR_NONE)
    {
        PostPrintf("Success\r\n");
    }
    
    return ret;
}

/**
 * @brief Process a request to determine the lock detect one or more UVP registers
 *
 * @param uvpLockInfo Information needed to determine the lock detect from UVP registers
 *
 * @return COTA_ERROR_NONE if successful, otherwise an error code is returned.
 */
CotaError_t ProcessUvpLockCommand(AmbUvpMask_t* uvpLockInfo)
{
    AmbMask_t   ambMask;
    UvpMask_t   uvpMask;
    AmbNum_t    amb;
    UvpNum_t    uvp;
    CotaError_t err;
    CotaError_t ret = COTA_ERROR_NONE;
    CtrlResp_t  resp;
    uint16_t    len = 0;
    
    ambMask = uvpLockInfo->ambMask;
    uvpMask = uvpLockInfo->uvpMask;

    resp.id  = CTRL_LOCK_DETECT_DATA;
    // The length is set to the values of the total message size. This
    // size is the size of the id and err as shown, but also
    // includes the "read" data later in this function
    len = sizeof(CTRL_LOCK_DETECT_DATA) +   
          sizeof(COTA_ERROR_NONE) +
          sizeof(resp.lockDetect);
    
    for (amb = 0; amb < MAX_NUM_AMB; amb++)
    {
        if (BIT_IN_MASK(amb,ambMask))
        {
            for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
            {
                if (BIT_IN_MASK(uvp, uvpMask))
                {                       
                    err = LockDetect(amb, uvp, &resp.lockDetect.cfsVal);
                    resp.err = err;
                    resp.lockDetect.uvpId.ambNum = amb;
                    resp.lockDetect.uvpId.uvpNum = uvp;
                    resp.lockDetect.lock = (err == COTA_ERROR_NONE);
                    PostPrintf("AMB: %d UVP: %d %s at %d\r\n", 
                        amb, uvp, (err == COTA_ERROR_NONE) ? "LOCKED" : "NOT LOCKED", resp.lockDetect.cfsVal);
                    RpiSendMessage((uint8_t*)&resp, len);

                    // To not overload the system with messages, we add a delay.
                    vTaskDelay(pdMS_TO_TICKS(5));
                    if ((ret == COTA_ERROR_NONE) && (err != COTA_ERROR_NONE))
                    {
                        ret = err;  //Let's take first error
                    }
                }
            }
        }
    }

    if (ret == COTA_ERROR_NONE)
    {
        PostPrintf("Success\r\n");
    }
    
    return COTA_ERROR_NONE;
}
