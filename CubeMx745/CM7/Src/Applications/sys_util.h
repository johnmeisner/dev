/**
 * @file       sys_util.h
 *
 * @brief      System utility functions module header file
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 */
     

#ifndef _SYS_UTIL_H_
#define _SYS_UTIL_H_

#include <stdint.h>
#include "FreeRTOS.h"
#include "timers.h"

#define BOOT_SELECTION_BOOTLOADER  0xB00120AD  ///< Value indicating that at start-up, we need to run the bootloader
#define BOOT_SELECTION_APPLICATION 0xA1234567  ///< Value indicating that at start-up, we need to run the application FW

     
/**
 * @brief  Value to indicate bootloader vs application load selection at reset.
 *
 * The __no_init attribute allows us to set the value before reset and
 * read it after reset.
 *
 * Also, the linker doesn't mind multiple declarations of this variable because
 * of this file being included in multiple compilation units since it points
 * to the same memory location for all instances of the variables.  In fact,
 * if this variable is declared as extern, the linker won't be able to find it.
 */
__no_init uint32_t gBootSelectionFlag @ 0x30040000;


void JumpToBootloader(void);
void TryToStartTimer(TimerHandle_t timer);
void TryToStopTimer(TimerHandle_t timer);

#endif   // #ifndef _SYS_UTIL_H_
