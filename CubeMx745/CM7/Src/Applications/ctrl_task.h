/**
 * @file       ctrl_task.h
 *
 * @brief      This is the header file for the control task.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CONTROL_TASK_H_
#define _CONTROL_TASK_H_

#include "rpi_msg_interface.h"

#define CTRL_TASK_MAX_MSG_SIZE     200     ///< The maximum size a message for the control task can be.
#define CTRL_TASK_MAX_CMD_SIZE     64      ///< The maximum size of command sent from CLI
/** The maximum message size for a ctrl task response to the host */
#define CTRL_TASK_RESPONSE_SIZE    RPI_MAX_MSG_SIZE 

void CreateCtrlTask(void);
void SignalTpsDone(void);
void RandomSeed(uint16_t val);
bool IsTpsTimedOut(void);
bool GetDebugMode(void);
void CompleteShutdownChecklist(void);
void HaltSystem(void);
bool GetDoorState(void);
    
#endif /* _CONTROL_TASK_H_ */
