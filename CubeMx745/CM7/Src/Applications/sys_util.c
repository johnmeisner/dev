/**
 * @file       sys_util.c
 *
 * @brief      System utility functions module implementation
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "FreeRTOS.h"
#include "timers.h"
#include "ctrl_task.h"
#include "log.h"
#include "orion_config.h"

#include "sys_util.h"
#include "stm32h7xx_hal.h"

#define BOOTLOADER_START_ADDRESS  0x1FF09800  ///< Bootloader code starts at this address in STM32H745
#define INT_CLEAR_REG_COUNT       5           ///< 149 interrupts defined in IRQn_Type are controlled by bits in 5 32-bit registers

/**
 * @brief  This function is used to execute the STM32H745 bootloader
 *
 * This function prepares the MCU for jumping to the bootloader that's
 * built into the MCU in ROM and starts it up.  It adjusts the vector
 * table to use bootloader's own interrupt vectors.
 *
 * @note This code was borrowed from this page:
 * https://community.st.com/s/article/STM32H7-bootloader-jump-from-application
 *
 */
void JumpToBootloader(void)
{
    // Declare a function pointer used to jump to the bootloader entry point
    void (*SysMemBootJump)(void);

    // Set the address of the entry point to bootloader (the bootloader's vector table)
    volatile uint32_t bootloaderVectTable = BOOTLOADER_START_ADDRESS;

    // Disable all interrupts
    __disable_irq();
    
    // Disable Systick timer
    SysTick->CTRL = 0;

    // Set the clock to the default state
    HAL_RCC_DeInit();

    // Clear Interrupt Enable Register & Interrupt Pending Register
    for (int i = 0; i < INT_CLEAR_REG_COUNT; i++)
    {
        NVIC->ICER[i] = 0xFFFFFFFF;
        NVIC->ICPR[i] = 0xFFFFFFFF;
    }	

    // Set up the jump to the first instruction of the booloader (found
    // at bootloader vector table address + 4) as a function pointer
    SysMemBootJump = (void (*)(void)) (*((uint32_t *) ((bootloaderVectTable + 4))));

    // Remap interrupt vector table to bootloader's one
    SCB->VTOR = bootloaderVectTable;
    
    // Set the main stack pointer to the bootloader stack address
    __set_MSP(*(uint32_t *)bootloaderVectTable);

    // Re-enable all interrupts
    __enable_irq();
    
    // Call the function to jump to bootloader location
    SysMemBootJump(); 

    // Jump is done successfully
    while (1);
    
    // We should never get here
}


/**
 * @brief Try to start a FreeRTOS timer.
 *
 *        On failure, log the error, and send the control task to an
 *        error state.
 * 
 * @param timer - The FreeRTOS timer to be started 
 */
void TryToStartTimer(TimerHandle_t timer)
{
    BaseType_t ret;
    ret = xTimerStart(timer, pdMS_TO_TICKS(TIMER_BLOCK_MS));
    if (ret != pdPASS)
    {
        LogFatal("Unable to start timer %s. Stopping Transmitter\r\n",
                 pcTimerGetName(timer));

        HaltSystem();
    }
}

/**
 * @brief Try to stop a FreeRTOS timer.
 *
 *        On failure, log the error, and send the control task to an
 *        error state.
 *
 * @param timer - The FreeRTOS timer to be stopped 
 */
void TryToStopTimer(TimerHandle_t timer)
{
    BaseType_t ret;
    ret = xTimerStop(timer, pdMS_TO_TICKS(TIMER_BLOCK_MS));
    if (ret != pdPASS)
    {
        LogFatal("Unable to stop timer %s. Stopping Transmitter\r\n",
                 pcTimerGetName(timer));

        HaltSystem();
    }
}
