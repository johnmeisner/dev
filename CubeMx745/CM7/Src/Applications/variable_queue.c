/**
 * @file      variable_queue.c
 *
 * @brief     Source to create and use a variable length message queue.
 *            The variable queue is a circular buffer where the 16-bit length
 *            of the message is written first, and the message 
 *            second.   Messages are removed from the buffer by reading the length
 *            of the message, and then the message. 
 * 
 *            The variable queue can be posted to by multiple tasks, but only
 *            received by one task.  Do not call these API's from an interrupt.
 *          
 *            @note Storage is limited to the size of the buffer minus 1.  This 
 *                  circular buffer implementation does not allow the head to 
 *                  fill the buffer and sit on the tail; hence the -1 mentioned above.
 *
 * @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
 *            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
 *            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
 *            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
 *            ALL RIGHTS RESERVED.
 */

// Standard includes
#include <variable_queue.h>
#include <string.h>
#include <task.h>
#include <stdbool.h>


#define VAR_QUEUE_RESERVED_SPACE ((uint16_t) 1)
#define MAX_SEMAPHORE_COUNT  0xFFFFFFFF
     
     
/**
 * @brief Create a queue that allows multiple tasks to write variable length
 *        messages to a queue that will be processed by a single task.
 *
 * @param storageSizeBytes The size of the queue's storage area in bytes @note The actual buffer that is available is this value minus 1
 * @param storageArea The area in memory that will hold the messages
 * @param queueControl pointer to type #VariableQueueControl_t that is initialized and passed to other functions to control the queue
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t VariableQueueCreateStatic(uint16_t storageSizeBytes, uint8_t *storageArea, VariableQueueControl_t *queueControl )
{
    CotaError_t ret = COTA_ERROR_NONE;
    queueControl->tail = 0;
    queueControl->head = 0;
    queueControl->storageArea = storageArea;
    queueControl->length = storageSizeBytes;
    queueControl->mutex = xSemaphoreCreateMutexStatic(&queueControl->mutexBuff);
    //The count of this semaphore will reflect the number of messages in the queue.
    queueControl->signalSem = xSemaphoreCreateCountingStatic(MAX_SEMAPHORE_COUNT, 0, &queueControl->signalSemBuf);  //The count of this semaphore will reflect the number of messages in the queue.  Allow maximum count.
 
    if ((queueControl->mutex == NULL) || (queueControl->signalSem == NULL))
    {
        ret = COTA_ERROR_FAILED_TO_CREATE_VAR_QUEUE;
    }
    
    return ret;
}

/**
 *  @brief A local static task that retrieves the amount of bytes available in the queue
 *
 *  @param  queueControl  Pointer to structure that contains information to control the queue
 *
 *  @return  The number of bytes available in the queue.
 */
static uint16_t getSpaceAvailable(VariableQueueControl_t *queueControl)
{
      //We basically subtract (head-tail) from the length
    uint16_t space = queueControl->length - (queueControl->head - queueControl->tail) - VAR_QUEUE_RESERVED_SPACE;

    //adjust for the scenerio where the head has circled back but the tail hasn't
    if ( space >= queueControl->length )
    {
        space -= queueControl->length;
    }
    
    return space;
}

/**
 *  @brief Retrieves the amount of bytes available in the queue in a thread safe way.
 *
 *  @param queueControl  Pointer to structure that contains information to control the queue
 *
 *  @return  The number of bytes available in the queue.
 */
uint16_t VariableQueueSpacesAvailable(VariableQueueControl_t *queueControl)
{
    //We basically subtract (head-tail) from length
    uint16_t space;
      
    xSemaphoreTake(queueControl->mutex, portMAX_DELAY); 
    
    space =  getSpaceAvailable(queueControl);
    
    xSemaphoreGive(queueControl->mutex); 
    
    return space;
}

/**
 *  @brief Writes data to the queue in a way to account for buffer rotations.
 *
 *  @param queueControl  Pointer to structure that contains information to control the queue
 *  @param data          Pointer to the data to be written
 *  @param count         The number of bytes to write.  It must be smaller than the amount of available space - sizeof(uint32_t)
 *
 *  @return  The number of bytes written to the queue - sizeof(utin32_t)  (The size of the message)
 */
static uint16_t writeBytesToBuffer(VariableQueueControl_t *queueControl, const uint8_t *data, uint16_t count)
{
    uint16_t nextHead, firstLength;

    nextHead = queueControl->head;

    // Calculate the number of bytes that can be added in the first write -
    // which may be less than the total number of bytes that need to be added if
    // the buffer will wrap back to the beginning.
    firstLength = configMIN( queueControl->length - queueControl->head, count );

    memcpy(&queueControl->storageArea[nextHead], data, firstLength );

    // If the number of bytes written was less than the number that could be
    //    written in the first write...
    if ( count > firstLength )
    {
        // ...then write the remaining bytes to the start of the buffer.
        memcpy(queueControl->storageArea, &data[firstLength], count - firstLength );
    }

    nextHead += count;
    if (nextHead >= queueControl->length )
    {
        nextHead -=  queueControl->length;
    }

    queueControl->head = nextHead;

    return count;
}

/**
 *  @brief Sends a message to the queue in a thread safe way and notifies #VariableQueueReceive
 *
 *  @note The region a message takes up in the queue is larger than the actual message size by sizeof(uint16_t).
 *               Messages are written to the queue by first writing their length, then the data in the message.
 *  @note If a message cannot be written to a queue, zero bytes are returned.  No part of the message is
 *               written unless it all fits.
 *
 *  @param queueControl  Pointer to structure that contains information to control the queue
 *  @param txData        Pointer to the data to be written
 *  @param length         The number of bytes to write.
 *
 *  @return  The number of bytes written to the queue.
 */
uint16_t VariableQueueSend( VariableQueueControl_t *queueControl, uint8_t* txData, uint16_t length)
{
    uint16_t bytesWritten = 0;
    uint16_t requiredSpace = length + sizeof(uint16_t);
    uint16_t spaceAvail;
    if (length > 0)  //No need to post zero length data
    {
        xSemaphoreTake( queueControl->mutex, portMAX_DELAY);    
        
        spaceAvail  =  getSpaceAvailable(queueControl);

        if (requiredSpace <= spaceAvail)
        {
            //Let's first write the size of the message, then the message, then signal the VariableQueueReceive
            writeBytesToBuffer(queueControl, (uint8_t*)&length, sizeof(uint16_t));
            bytesWritten += writeBytesToBuffer(queueControl, txData, length);
            xSemaphoreGive( queueControl->signalSem);
            
        }

        xSemaphoreGive( queueControl->mutex); 
    }
    return bytesWritten;
}

/**
 * @brief Tests if the queue is empty
 *
 * @param queueControl  Pointer to structure that contains information to control the queue 
 *
 * @return  true if the queue is empty; otherwise false
 */
static bool isQueueEmpty(VariableQueueControl_t *queueControl)
{
    uint16_t spaceAvail = getSpaceAvailable(queueControl);
    
    return (spaceAvail == (queueControl->length-1));
}


/**
 *  @brief Removes data from the queue in a way to account for buffer rotations.
 *  
 *  @note Size should be checked to make sure it is valid before calling this function
 *
 *  @param queueControl  Pointer to structure that contains information to control the queue
 *  @param readData      Pointer to a buffer to receive the data
 *  @param size          The maximum size the buffer can receive
 *
 *  @return  The number of bytes written to the queue.
 */
static uint16_t readBytesFromBuffer( VariableQueueControl_t *queueControl, uint8_t *readData, uint16_t size)
{
    uint16_t nextTail;
    uint16_t firstLength;

    nextTail = queueControl->tail;

    // Calculate the number of bytes that can be read - which may be
    // less than the number wanted if the data wraps around to the start of
    // the buffer. */
    firstLength = configMIN( queueControl->length - nextTail, size );

    // Obtain the number of bytes it is possible to obtain in the first
    // read.  Asserts check bounds of read and write. */
    memcpy( readData, &queueControl->storageArea[nextTail] , firstLength );
    
    // If the total number of wanted bytes is greater than the number
    // that could be read in the first read... */
    if ( size > firstLength )
    {
        memcpy(&readData[firstLength], queueControl->storageArea, size - firstLength );
    }

    // Move the tail pointer to effectively remove the data read from
    // the buffer.
    nextTail += size;

    if (nextTail >= queueControl->length)
    {
        nextTail -= queueControl->length;
    }

    queueControl->tail = nextTail;

    return size;
}

/**
 *  @brief Recevies a message from the queue in a thread safe way.
 *
 *  @param queueControl  Pointer to structure that contains information to control the queue
 *  @param rxData        Pointer to the buffer to receive the data
 *  @param length        The maximum length of the buffer
 *  @param ticksToWait   Time to wait for a message to appear in the queue.
 *
 *  @return  The number of bytes removed from the queue.
 */
uint16_t VariableQueueReceive(VariableQueueControl_t *queueControl, uint8_t *rxData, uint16_t length, TickType_t ticksToWait )
{
    BaseType_t takeVal = pdPASS;
    uint16_t msgSize = 0;
    uint16_t secondRead = 0;
    uint16_t returnRead = 0;
    uint8_t dummy;

    takeVal = xSemaphoreTake(queueControl->signalSem, ticksToWait);
    
    if (takeVal == pdPASS)
    {
        xSemaphoreTake(queueControl->mutex, portMAX_DELAY);  
    
        if (!isQueueEmpty(queueControl))
        {
            //Read the size of the message, then the message
            readBytesFromBuffer(queueControl, (uint8_t*) &msgSize, sizeof(uint16_t));
            configASSERT(msgSize < queueControl->length);   //Something is majorly wrong here, let's break things
            secondRead = configMIN(msgSize, length);
            readBytesFromBuffer(queueControl, rxData, secondRead);
            returnRead = secondRead;
    
            while (secondRead < msgSize)
            {
                //In this case, we could not fit the message into the rxData buffer, so we need
                //to discard the excess message that we truncated so we can read the next message
                secondRead += readBytesFromBuffer(queueControl, &dummy, sizeof(dummy));
            }
            
        }
        
        xSemaphoreGive(queueControl->mutex); 
    }         

    return returnRead;
}

/**
 * @brief Determine if the variable queue is empty
 *
 * @param queueControl  Pointer to structure that contains information to control the queue
 * 
 * @return true if the queue is empty, false the queue is not empty
 */
bool IsVariableQueryEmpty(VariableQueueControl_t* queueControl)
{
    return isQueueEmpty(queueControl);
}