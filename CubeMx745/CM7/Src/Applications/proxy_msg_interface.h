/** 
 * @file       proxy_msg_interface.h
 *
 * @brief      This is the header file for proxy_msg_interface.c
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#ifndef _PROXY_MSG_INTERFACE_H_
#define _PROXY_MSG_INTERFACE_H_

#include <stdbool.h>
#include "error.h"
#include "CotaCommonTypes.h"
#include "proxyHostMsgInf.h"

#define APP_SET_COMM_CHANNEL_ID   15   ///< The app command used to set the comm channel.
#define APP_SET_COMM_CHANNEL_SIZE 2    ///< The payload size for set comm channel app command.

void PrxRestart(void);
bool PrxIsReady(void);
bool PrxMsgRdy(void);
ProxyHostMsgType_t PrxGetMsg(uint8_t *buf, uint8_t size);

CotaError_t PrxSendReqInfo(void);
void PrxUpdateProxyInfo(uint8_t *buf, uint8_t size);
void PrxUpdateProxyStatus(uint8_t *buf, uint8_t size);

void PrxExtractDiscoveryResp(uint8_t *buf, uint8_t size, PrxHostDiscoveryMsg_t *discResult);
void PrxGetProxyInfo(PrxHostProxyFwInfo_t *proxyInfo);
void PrxExtractJoinResp(uint8_t *buf, uint8_t size, PrxHostClientJoinRspMsg_t *joinResult);
void PrxExtractTpcResp(uint8_t *buf, uint8_t size, PrxHostTpcCfn_t *tpcResult);
void PrxExtractQueryCfnResp(uint8_t *buf, uint8_t size, PrxHostCqRecCfn_t *queryResult);
void PrxExtractQueryDataResp(uint8_t *buf, uint8_t size, ClientQueryDataCstm_t *queryData);
void PrxExtractDisconnectResp(uint8_t *buf, uint8_t size, PrxHostLeaveNetworkCfn_t *disconnectResult);
void PrxExtractAppCmdCfn(uint8_t *buf, uint8_t size);
void PrxExtractPrxCmdResp(uint8_t *buf, uint8_t size, PrxHostPrxCmdResp_t *proxyCmdData);
void PrxExtractAppDataResp(uint8_t *buf, uint8_t size, PrxHostAppData_t *clientAppData);
void PrxExtractAnnoucement(uint8_t *buf, uint16_t size, PrxHostAnnouncement_t *annc);
void PrxExtractConfigurationResp(uint8_t *buf, uint16_t size, PrxHostRcvrCfgRsp_t *cfgResp);
void PrxExtractImmediateShutoff(uint8_t *buf, uint16_t size, PrxHostImmediateShutoff_t *shutoff);
uint8_t PrxExtractCommChannelFromAppCommand(PrxHostClientCmdMsg_t* appCmd);

void InvalidateProxyReceivedFlags(void);
CotaError_t PrxSendDisc(uint8_t channel);
CotaError_t PrxSendJoin(ExtdAddr_t clientLongId, ShrtAddr_t clientShortId);
CotaError_t PrxSendTpc(ShrtAddr_t clientShortId);
CotaError_t PrxSendQuery(void);
CotaError_t PrxSendDisconnect(ShrtAddr_t clientShortId);
CotaError_t PrxSendTps(void);
CotaError_t PrxSendCommand(PrxHostProxyCmdMsg_t* proxyCmd);
CotaError_t PrxSendAppCommand(PrxHostClientCmdMsg_t* appCmd);
CotaError_t PrxSendLightRingMsg(PrxHostLightRingMsg_t *msg);
CotaError_t PrxSendProxySetup(void);
CotaError_t PrxSendGo(CotaRcvrGo_t *msg);
CotaError_t PrxSendReceiverConfig(ExtdAddr_t longId, ShrtAddr_t shortId);

#endif  // #ifndef _PROXY_MSG_INTERFACE_H_
