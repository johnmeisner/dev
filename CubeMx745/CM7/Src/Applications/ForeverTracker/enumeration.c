/**
 * @file       enumeration.c
 *
 * @brief      Defines the enumeration period for the ForeverTracker
 *             project. This is defines a period where transmission cannot occur
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "sys_util.h"
#include "debug_on.h"

#if defined(USE_FOREVER_TRACKER_BUILD)

TimerHandle_t  gEnumerationTimerHandle  = NULL; ///< FreeRTOS timer handle for the enumeration timer
StaticTimer_t  gEnumerationTimerBuffer;         ///< FreeRTOS timer buffer for the enumeration timer

static bool     gIsEnumerationPeriodActive = false;
static uint16_t gShortEnumerationTimerVal = 0;
static uint16_t gLongEnumerationTimerVal = 0;

static void enumerationTimerCallback(TimerHandle_t timer);
static void startTimerPeriod(uint16_t timer_val);

void EnumerationInit(void)
{
    /*
      Enumeration Timer
     */
    CfgGetParam(CFG_RCVR_LONG_CONFIG_RESP_TIMEOUT, &gLongEnumerationTimerVal, sizeof(gLongEnumerationTimerVal));
    CfgGetParam(CFG_RCVR_SHORT_CONFIG_RESP_TIMEOUT, &gShortEnumerationTimerVal, sizeof(gShortEnumerationTimerVal));

    if (gLongEnumerationTimerVal > 0)
    {
        if (gEnumerationTimerHandle)
        {
            xTimerStop(gEnumerationTimerHandle, NO_WAIT);
            xTimerChangePeriod(gEnumerationTimerHandle, pdMS_TO_TICKS(gLongEnumerationTimerVal), NO_WAIT);
        }
        else
        {
            gEnumerationTimerHandle = xTimerCreateStatic(
                "Enumeration Timer",
                pdMS_TO_TICKS(gLongEnumerationTimerVal),
                pdFALSE,
                (void *) 0,
                enumerationTimerCallback,
                &gEnumerationTimerBuffer);
        }
    }
    else
    {
        gEnumerationTimerHandle = NULL;
    }

    if(!gEnumerationTimerHandle)
    {
        LogFatal("Enumeration timer failed to initialize\r\n");
    }
}

bool CheckIfEnumerationPeriodActive(void)
{
    return gIsEnumerationPeriodActive;
}

void StartLongEnumerationPeriod(void)
{
    LogInfo("Long Enumeration period has begun\r\n");
    startTimerPeriod(gLongEnumerationTimerVal);
}

void StartShortEnumerationPeriod(void)
{
    LogInfo("Short Enumeration period has begun\r\n");
    startTimerPeriod(gShortEnumerationTimerVal);
}

static void startTimerPeriod(uint16_t timer_val_ms)
{
    if (gEnumerationTimerHandle)
    {
        xTimerStop(gEnumerationTimerHandle, NO_WAIT);
        xTimerChangePeriod(gEnumerationTimerHandle, pdMS_TO_TICKS(timer_val_ms), NO_WAIT);
        TryToStartTimer(gEnumerationTimerHandle);
DBG6_SET(); //trigger logic probe: "Enumeration period active"
    }

    gIsEnumerationPeriodActive = true;
}

void StopEnumerationPeriod(void)
{

    if (gIsEnumerationPeriodActive)
    {
        LogInfo("(Re)Enumeration period stopped before it finished.\r\n");
    }

    TryToStopTimer(gEnumerationTimerHandle);
    gIsEnumerationPeriodActive = false;
DBG6_RESET();
}

static void enumerationTimerCallback(TimerHandle_t timer)
{
    TryToStopTimer(gEnumerationTimerHandle);
    gIsEnumerationPeriodActive = false;
    LogInfo("Enumeration period is over. Charging will now begin.\r\n");
DBG6_RESET();
}

#endif
