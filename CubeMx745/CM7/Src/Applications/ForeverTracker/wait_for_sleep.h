/**
 * @file       wait_for_sleep.h
 *
 * @brief      Header file for the wait_for_sleep module supporting Forever Tracker build
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef WAIT_FOR_SLEEP_H
#define WAIT_FOR_SLEEP_H

#include <stdbool.h>

void WaitForSleepInit(void);
bool CheckIfWaitForSleepPeriodActive(void);
bool CheckIfSleepWaitPeriodExpiredNotification(void);
void StartWaitForSleepPeriod(void);
void StopWaitForSleepPeriod(void);

#endif /* WAIT_FOR_SLEEP_H */
