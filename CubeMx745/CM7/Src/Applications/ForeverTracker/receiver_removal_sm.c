/**
 * @file       receiver_removal_sm.c
 *
 * @brief      This state periodically removes receivers from the database
 *             that are marked with receiver RCVR_STATE_REMOVED_FROM_DATABASE
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "config_system.h"
#include "client.h"

#if defined(USE_FOREVER_TRACKER_RECEIVER_REMOVAL)
#include "ForeverTracker/receiver_removal_sm.h"
#include "ForeverTracker/ft_client_interface.h"
#include "ForeverTracker/enumeration.h"

TimerHandle_t  gReceiverRemovalTimerHandle                     = NULL;     ///< FreeRTOS timer handle for the Receiver Removal timer
StaticTimer_t  gReceiverRemovalTimerBuffer;                                ///< FreeRTOS timer buffer for the Receiver Removal timer

volatile bool gTryToRemoveReceivers = false;

static void receiverRemovalTimerCallback(TimerHandle_t timer);

bool StartReceiverRemovalTimers(void)
{
    return xTimerStart( gReceiverRemovalTimerHandle, NO_WAIT ) == pdPASS;
}

void ReceiverRemovalStateInit(void)
{
    uint16_t timerVal;

    /*
      ReceiverRemoval Timer
     */
    CfgGetParam(CFG_RECEIVER_REMOVAL_PERIOD, &timerVal, sizeof(timerVal));

    if (timerVal > 0)
    {
        if (gReceiverRemovalTimerHandle)
        {
            xTimerStop(gReceiverRemovalTimerHandle, NO_WAIT);
            xTimerChangePeriod(gReceiverRemovalTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gReceiverRemovalTimerHandle = xTimerCreateStatic(
                "Receiver Removal Timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                receiverRemovalTimerCallback,
                &gReceiverRemovalTimerBuffer);
        }
    }
    else
    {
        gReceiverRemovalTimerHandle = NULL;
    }

    if(!gReceiverRemovalTimerHandle)
    {
        LogFatal("Receiver Removal timer failed to initialize\r\n");
    }
}

/**
 * @brief Prompts the state machine to enter the Recevier Removal
 *        state, where we will try to remove receivers that aren't
 *        behaving properly.
 */
void TryToRemoveBadReceivers(void)
{
    gTryToRemoveReceivers = true;
}

void ReceiverRemovalEntry(void)
{
    Client_t client;

    // We set all announced receivers to be removed by the database in
    // the next step. This will clean out receivers that are not
    // properly configuring or communiticating.
    CmSetAllAnnouncedReceiversToBeRemoved();

    // When this state is ran, we want to remove every receiver from
    // the database that has been marked for removal. We do that by...
    //   1. Checking for marked receivers.
    //   2. If found, remove the receiver,
    //      then re-run the Receiver Removal state.
    //   3. If not found, set the timer and we'll try again later.
    //
    // This means if there are any receivers marked for removal, this
    // state will execute n + 1 times where n is the number of
    // receivers marked for removal.
    if (CmGetReceiverToRemove(&client))
    {
        if (CmRemoveReceiver(client.longId))
        {
            LogInfo("Receiver with Id: 0x%016llx has been removed from the database\r\n",
                    *(uint64_t*)&client.longId);
        }

        // By setting this boolean to true we trigger ctrl_task to
        // re-enter this state when possible.
        gTryToRemoveReceivers = true;
    }
    else
    {
        gTryToRemoveReceivers = false;
        TryToStartTimer(gReceiverRemovalTimerHandle);
    }
}

bool RdyToReceiverRemovalCheck (void){ return (gTryToRemoveReceivers && !CheckIfEnumerationPeriodActive()); }
bool ReceiverRemovalToRdyCheck (void){ return true; }

static void receiverRemovalTimerCallback(TimerHandle_t timer)
{
    gTryToRemoveReceivers = true;
}

#endif // defined(USE_FOREVER_TRACKER_RECEIVER_REMOVAL)
