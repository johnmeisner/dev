/**
 * @file       tps_charge_ready_check.c
 *
 * @brief      A utility module for tps charging logic specific to ForeverTracker
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>

#include "tps_sm_helper.h"
#include "client.h"

// These functions are provided by client_manager.c
PRIVATE void Walk(void (*func)(Client_t* client));

#if defined(USE_FOREVER_TRACKER_BUILD)

static void checkRxReadyToCharge(Client_t *client);

/**

 *   Indicates all receivers are ready to charge
 */
static bool gAllRxReadyToCharge;

/**
 * @brief Checks if all "user devices" (e.g. tracker board) attached to receivers
 *        are sleeping (ready to charge) in the system.
 */
bool AllRxReadyToCharge(void)
{
    gAllRxReadyToCharge = true; //assumption to be proven false during "walk" of clients
    Walk(checkRxReadyToCharge);
    return gAllRxReadyToCharge;
}

/**
 * @brief Checks if the "user device" (e.g. tracker board) attached to receiver
 *        is sleeping (ready to charge)
 * @note  This function is intended to be used with #Walk()
 */
static void checkRxReadyToCharge(Client_t *client)
{
    if (client != NULL)
    {
        if (!(IsRcvrReadyForCharging(client)))
        {
            gAllRxReadyToCharge = false;
        }
    }
}

#endif
