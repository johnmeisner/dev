/**
 * @file       forever_tracker_state_machine.c
 *
 * @brief      Contains the control task state machine skeleton for the forever tracker
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 * @details    Below is a graphical representation of the state machine. For descriptions
 *             on each state, see the state declarations below.
 *             
 * @code{.c}
 *                              --------------------
 *                              |                   |
 *                   ---------->|       Init        |
 *                  /           |                   |
 *                 /            --------------------
 *                /                        |
 *               /                         |
 *              /                          |
 *             \/                         \|/
 *     --------------------       --------------------
 *    |                    |     |                   |
 *    |       Debug        |<----|       Ready       |<--> ...To States listed below
 *    |                    |     |                   |
 *     --------------------       --------------------
 *                                        /|\
 *                                         |
 *                                         |
 *                                        \|/
 *                               --------------------
 *                               |                   |
 *                               |       TPS         |
 *                               |                   |
 *                               --------------------
 *
 *
 *
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|      Query        |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|    Calibration    |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|   Power Monitor   |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|       Fans        |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|    Light Ring     |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|    App Command    |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|    Proxy Test     |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->| Receiver Removal  |
 *                               |                   |
 *                               --------------------
 * @endcode
 */

#include "orion_config.h"

#if defined(USE_FOREVER_TRACKER_BUILD)

#include "state_machine.h"
#include "stddef.h"
#include "forever_tracker_state_machine.h"

/**
 * @brief State that indicates an error in the state machine has occurred
 * @todo  All the states should really be declared as "const"
 */
static StateMachineState_t gForeverTrackerError;

/**
 * @brief Puts the transmitter in debug mode. In debug mode, the system disables
 *        all timers and yields more control to the user.
 */
static StateMachineState_t gForeverTrackerDebug;

/**
 * @brief Starts system resources (timers) to prepare the system for the ready state
 */
static StateMachineState_t gForeverTrackerInit;

/**
 * @brief The state of the system when it is ready to service any need based on
 *        flags set. This is the 'center' state that can reach all other states. 
 */
static StateMachineState_t gForeverTrackerRdy;

/**
 * @brief On entry to this state, a proxy info command will be sent to the proxy
 *        The state will wait for a response and reset the proxy if it doesn't get one
 */
static StateMachineState_t gForeverTrackerProxyTest;

/**
 * @brief On entry to this state, a query message is sent. An
 *        acknowledgment timeout timer will cause this state to exit.
 */
static StateMachineState_t gForeverTrackerQuery;

/**
 * @brief On entry to this state, commands are sent to power manager to refresh, update, or output
 *        power values
 */
static StateMachineState_t gForeverTrackerPm;

/**
 * @brief On entry to this state, commands are sent to fans to check that they are operable
 */
static StateMachineState_t gForeverTrackerFans;

/**
 * @brief State for managing app commands
 */
static StateMachineState_t gForeverTrackerAppCmd;

/**
 * @brief Ensures that the light ring is updated to the correct state
 */
static StateMachineState_t gForeverTrackerLightRingUpdate;

/**
 * @brief This state determines if it's appropriate to calibrate the system. The
 *        system could require a calibration either based on the amount of time that
 *        has passed, or the measured temperature of the system.
 */
static StateMachineState_t gForeverTrackerCalibration;

/**
 * @brief This is the TPS or Charging state of the transmitter. When charging, the
 *        types of messages (task-to-task) is reduced. This architecture prevents
 *        the transmitter from telling the proxy to send a message while the
 *        system is charging clients
 */
static StateMachineState_t gForeverTrackerTps;

/**
 * @brief Removes receivers that are given the
 *        RCVR_STATE_REMOVED_FROM_DATABASE state on a periodic basis.
 */
static StateMachineState_t gForeverTrackerReceiverRemoval;

/*
 * Error State Definitions 
 */
static StateMachineTransition_t gForeverTrackerErrorTxnOptions[];

static StateMachineState_t gForeverTrackerError =
{
    .state             = CTRL_STATE_ERROR,
    .entryFcn          = ErrorEntry,
    .stateName         = "Control task error",
    .transitionOptions = gForeverTrackerErrorTxnOptions
};

static StateMachineTransition_t gForeverTrackerErrorTxnOptions[] =
{
    {
        .checkCondition = CtrlMachineError,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerError
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Init State Definitions
 */
static StateMachineTransition_t gForeverTrackerInitTxnOptions[];

static StateMachineState_t gForeverTrackerInit =
{
    .state             = CTRL_STATE_INIT,
    .entryFcn          = InitEntry,
    .stateName         = "Control task initialization",
    .transitionOptions = gForeverTrackerInitTxnOptions
};

static StateMachineTransition_t gForeverTrackerInitTxnOptions[] =
{
    {
        .checkCondition = InitToDebugCheck,
        .exitFcn        = InitToDebugExit,
        .nextState      = &gForeverTrackerDebug
    },
    {
        .checkCondition = InitToRdyCheck,
        .exitFcn        = InitToRdyExit,
        .nextState      = &gForeverTrackerRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Debug State Definitions
 */
static StateMachineTransition_t gForeverTrackerDebugTxnOptions[];

static StateMachineState_t gForeverTrackerDebug =
{
    .state             = CTRL_STATE_DEBUG,
    .entryFcn          = DebugEntry,
    .stateName         = "Control task debug mode",
    .transitionOptions = gForeverTrackerDebugTxnOptions
};

static StateMachineTransition_t gForeverTrackerDebugTxnOptions[] =
{
    {
        .checkCondition = DebugToInitCheck,
        .exitFcn        = DebugToInitExit,
        .nextState      = &gForeverTrackerInit
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Ready State Definitions
 */
static StateMachineTransition_t gForeverTrackerRdyTxnOptions[];

static StateMachineState_t gForeverTrackerRdy =
{
    .state             = CTRL_STATE_RDY,
    .entryFcn          = ReadyEntry,
    .stateName         = "Control task ready mode",
    .transitionOptions = gForeverTrackerRdyTxnOptions
};

static StateMachineTransition_t gForeverTrackerRdyTxnOptions[] =
{
    {
        .checkCondition = RdyToErrorCheck,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerError
    },
    {
        .checkCondition = RdyToDebugCheck,
        .exitFcn        = RdyToDebugExit,
        .nextState      = &gForeverTrackerDebug
    },
    {
        .checkCondition = RdyToCalCheck,
        .exitFcn        = RdyToCalExit,
        .nextState      = &gForeverTrackerCalibration
    },
    {
        .checkCondition = RdyToProxyTestCheck,
        .exitFcn        = RdyToProxyTestExit,
        .nextState      = &gForeverTrackerProxyTest
    },
    {
        .checkCondition = RdyToPmCheck,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerPm
    },
    {
        .checkCondition = RdyToFansCheck,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerFans
    },
    {
        .checkCondition = RdyToAppCmdCheck,
        .exitFcn        = RdyToAppCmdExit, 
        .nextState      = &gForeverTrackerAppCmd     
    },
    {
        .checkCondition = RdyToLightRingUpdateCheck,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerLightRingUpdate
    },
    {
        .checkCondition = RdyToQueryCheck,
        .exitFcn        = RdyToQueryExit,
        .nextState      = &gForeverTrackerQuery
    },
    {
        .checkCondition = RdyToReceiverRemovalCheck,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerReceiverRemoval
    },
    {
         .checkCondition = RdyToTpsCheck,
         .exitFcn        = RdyToTpsExit,
         .nextState      = &gForeverTrackerTps
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Query State Definitions
 */
static StateMachineTransition_t gForeverTrackerQueryTxnOptions[];

static StateMachineState_t gForeverTrackerQuery =
{
    .state             = CTRL_STATE_SEND_QUERY,
    .entryFcn          = QueryEntry,
    .stateName         = "Control task sending query messages",
    .transitionOptions = gForeverTrackerQueryTxnOptions
};

static StateMachineTransition_t gForeverTrackerQueryTxnOptions[] =
{
    {
        .checkCondition = QueryToRdyCheck,
        .exitFcn        = QueryToRdyExit,
        .nextState      = &gForeverTrackerRdy
    },
    NULL_TRANSITION_STRUCT
};


/*
 * Proxy test State definitions
 */
static StateMachineTransition_t gForeverTrackerProxyTestTxnOptions[];

static StateMachineState_t gForeverTrackerProxyTest =
{
    .state             = CTRL_STATE_PROXY_TEST,
    .entryFcn          = ProxyTestEntry,
    .stateName         = "Test the proxy to see if it's ok",
    .transitionOptions = gForeverTrackerProxyTestTxnOptions
};

static StateMachineTransition_t gForeverTrackerProxyTestTxnOptions[] =
{
    {
        .checkCondition = ProxyTestToRdyCheck,
        .exitFcn        = ProxyTestToRdyExit,
        .nextState      = &gForeverTrackerRdy
    },

    NULL_TRANSITION_STRUCT
};



static StateMachineTransition_t gForeverTrackerPmTxnOptions[];

/**
 * @brief  The following control task state manages power monitor readings
 */
static StateMachineState_t gForeverTrackerPm =
{
    .state             = CTRL_STATE_PM,
    .entryFcn          = PmEntry,
    .stateName         = "Control task sending power manager messages",
    .transitionOptions = gForeverTrackerPmTxnOptions
};

static StateMachineTransition_t gForeverTrackerPmTxnOptions[] =
{
    {
        .checkCondition = PmToRdyCheck,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerRdy
    },
    NULL_TRANSITION_STRUCT
};

static StateMachineTransition_t gForeverTrackerFansTxnOptions[];

/**
 * @brief  The following control state checks and manages the fans
 */
static StateMachineState_t gForeverTrackerFans =
{
    .state             = CTRL_STATE_FAN_CTRL,
    .entryFcn          = FansEntry,
    .stateName         = "Control state checking fans",
    .transitionOptions = gForeverTrackerFansTxnOptions
};

static StateMachineTransition_t gForeverTrackerFansTxnOptions[] =
{
    {
        .checkCondition = FansToRdyCheck,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerRdy
    },
    NULL_TRANSITION_STRUCT
};

static StateMachineTransition_t gForeverTrackerAppCmdOptions[];  

/**
 * @brief  The following states manages the rate at which send client commands
 */
static StateMachineState_t gForeverTrackerAppCmd =
{
    .state             = CTRL_STATE_CLIENT_CMD,        
    .entryFcn          = AppCmdEntry,                  
    .stateName         = "State managing client commands",
    .transitionOptions = gForeverTrackerAppCmdOptions               
};

static StateMachineTransition_t gForeverTrackerAppCmdOptions[] =
{
    {
        .checkCondition = AppCmdToRdyCheck,          
        .exitFcn        = AppCmdToRdyExit,           
        .nextState      = &gForeverTrackerRdy                  
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Calibration State Definitions
 */
static StateMachineTransition_t gForeverTrackerCalTxnOptions[];

static StateMachineState_t gForeverTrackerCalibration =
{
    .state             = CTRL_STATE_CAL,
    .entryFcn          = CalEntry,
    .stateName         = "Control task calibrating system",
    .transitionOptions = gForeverTrackerCalTxnOptions
};

static StateMachineTransition_t gForeverTrackerCalTxnOptions[] =
{
    {
        .checkCondition = CalToRdyCheck,
        .exitFcn        = CalToRdyExit,
        .nextState      = &gForeverTrackerRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * TPS (Charging) State Definitions
 */
static StateMachineTransition_t gForeverTrackerTpsTxnOptions[];

static StateMachineState_t gForeverTrackerTps =
{
    .state             = CTRL_STATE_TPS,
    .entryFcn          = TpsEntry,
    .stateName         = "Control task charging clients",
    .transitionOptions = gForeverTrackerTpsTxnOptions
};

static StateMachineTransition_t gForeverTrackerTpsTxnOptions[] =
{
    {
        .checkCondition = TpsDoneToRdyCheck,
        .exitFcn        = TpsDoneToRdyExit,
        .nextState      = &gForeverTrackerRdy
    },
    {
        .checkCondition = TpsTimeoutToRdyCheck,
        .exitFcn        = TpsTimeoutToRdyExit,
        .nextState      = &gForeverTrackerRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Light Ring state definitions 
 */
static StateMachineTransition_t gForeverTrackerLightRingUpdateOptions[];

static StateMachineState_t gForeverTrackerLightRingUpdate =
{
    .state             = CTRL_STATE_LIGHT_RING_UPDATE,
    .entryFcn          = LightRingUpdateEntry,
    .stateName         = "Control state for updating the light ring",
    .transitionOptions = gForeverTrackerLightRingUpdateOptions
};

static StateMachineTransition_t gForeverTrackerLightRingUpdateOptions[] =
{
    {
        .checkCondition = LightRingUpdateToRdyCheck,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Receiver Removal state definitions
 */
static StateMachineTransition_t gForeverTrackerReceiverRemovalTxnOptions[];

static StateMachineState_t  gForeverTrackerReceiverRemoval =
{
    .state             = CTRL_STATE_RECEIVER_REMOVAL,
    .entryFcn          = ReceiverRemovalEntry,
    .stateName         = "Receiver Removal state",
    .transitionOptions = gForeverTrackerReceiverRemovalTxnOptions
};

static StateMachineTransition_t gForeverTrackerReceiverRemovalTxnOptions[] =
{
    {
        .checkCondition = ReceiverRemovalToRdyCheck,
        .exitFcn        = NULL,
        .nextState      = &gForeverTrackerRdy
    },
    NULL_TRANSITION_STRUCT
};

__weak void ErrorEntry                  (void){ return; }
__weak void DebugEntry                  (void){ return; }
__weak void InitEntry                   (void){ return; }
__weak void ReadyEntry                  (void){ return; }
__weak void TpsEntry                    (void){ return; }
__weak void ProxyTestEntry              (void){ return; }
__weak void QueryEntry                  (void){ return; }
__weak void FansEntry                   (void){ return; }
__weak void CalEntry                    (void){ return; }
__weak void AppCmdEntry                 (void){ return; }
__weak void LightRingUpdateEntry        (void){ return; }
__weak void ReceiverRemovalEntry        (void){ return; }

// By having the error return true, the error state works a non-blocking while(1) loop
__weak bool CtrlMachineError  (void){ return true; }

__weak bool InitToRdyCheck              (void){ return false; }
__weak bool InitToDebugCheck            (void){ return false; }
__weak bool DebugToInitCheck            (void){ return false; }
__weak bool RdyToErrorCheck             (void){ return false; }
__weak bool RdyToDebugCheck             (void){ return false; }
__weak bool RdyToTpsCheck               (void){ return false; }
__weak bool RdyToProxyTestCheck         (void){ return false; }
__weak bool RdyToQueryCheck             (void){ return false; }
__weak bool RdyToPmCheck                (void){ return false; }
__weak bool RdyToFansCheck              (void){ return false; }
__weak bool RdyToCalCheck               (void){ return false; }
__weak bool RdyToSchedCheck             (void){ return false; }
__weak bool RdyToLightRingUpdateCheck   (void){ return false; }
__weak bool ReceiverRemovalToRdyCheck   (void){ return false; }
__weak bool TpsDoneToRdyCheck           (void){ return false; }
__weak bool TpsTimeoutToRdyCheck        (void){ return false; }
__weak bool ProxyTestToRdyCheck         (void){ return false; }
__weak bool QueryToRdyCheck             (void){ return false; }
__weak bool FansToRdyCheck              (void){ return false; }
__weak bool CalToRdyCheck               (void){ return false; }
__weak bool SchedToRdyCheck             (void){ return false; }
__weak bool AppCmdToRdyCheck            (void){ return false; }
__weak bool LightRingUpdateToRdyCheck   (void){ return false; }
__weak bool RdyToReceiverRemovalCheck   (void){ return false; }

__weak void InitToRdyExit           (void){ return; }
__weak void InitToDebugExit         (void){ return; }
__weak void DebugToInitExit         (void){ return; }
__weak void RdyToDebugExit          (void){ return; }
__weak void RdyToTpsExit            (void){ return; }
__weak void RdyToProxyTestExit      (void){ return; }
__weak void RdyToQueryExit          (void){ return; }
__weak void RdyToCalExit            (void){ return; }
__weak void RdyToSchedExit          (void){ return; }
__weak void RdyToAppCmdExit         (void){ return; }
__weak void TpsDoneToRdyExit        (void){ return; }
__weak void TpsTimeoutToRdyExit     (void){ return; }
__weak void ProxyTestToRdyExit      (void){ return; }
__weak void QueryToRdyExit          (void){ return; }
__weak void CalToRdyExit            (void){ return; }
__weak void SchedToRdyExit          (void){ return; }
__weak void AppCmdToRdyExit         (void){ return; }

StateMachineState_t *CtrlSmInit(void)
{
    return &gForeverTrackerInit;
}

#endif // defined(USE_FOREVER_TRACKER_BUILD)
