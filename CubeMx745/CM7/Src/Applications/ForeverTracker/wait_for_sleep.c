/**
 * @file       wait_for_sleep.c
 *
 * @brief      Defines an API for accessing a special timer used to allow TPS charging after a defined
 *             (CFG_WAIT_FOR_SLEEP_TIMEOUT) period of time. Specific to the ForeverTracker project.
 *
 *             Exact behavior is defined by the calling functions.
 *             Synopsis of intended use:
 *             A timer is started once the door is closed and stopped when the door is opened.
 *             If the timer times out, then any clients not sleeping are removed from the client list and
 *             TPS charging is allowed (other conditions may apply)
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "sys_util.h"
#include "debug_on.h"

#if defined(USE_FOREVER_TRACKER_BUILD)

TimerHandle_t  gWaitForSleepTimerHandle  = NULL; ///< FreeRTOS timer handle for the enumeration timer
StaticTimer_t  gWaitForSleepTimerBuffer;         ///< FreeRTOS timer buffer for the enumeration timer

static bool gIsWaitForSleepPeriodActive = false;
static bool gAllowTimerExpirationOneTimeNotification = false;

static void waitForSleepTimerCallback(TimerHandle_t timer);

void WaitForSleepInit(void)
{
    uint16_t timerVal;

    /*
      WaitForSleep Timer
     */
    CfgGetParam(CFG_WAIT_FOR_SLEEP_TIMEOUT, &timerVal, sizeof(timerVal));

    if (timerVal > 0)
    {
        if (gWaitForSleepTimerHandle)
        {
            xTimerStop(gWaitForSleepTimerHandle, NO_WAIT);
            xTimerChangePeriod(gWaitForSleepTimerHandle, SEC_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gWaitForSleepTimerHandle = xTimerCreateStatic(
                "Wait For Sleep Timer",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                waitForSleepTimerCallback,
                &gWaitForSleepTimerBuffer);
        }
    }
    else
    {
        gWaitForSleepTimerHandle = NULL;
    }

    if(!gWaitForSleepTimerHandle)
    {
        LogFatal("Wait For Sleep timer failed to initialize\r\n");
    }
}

bool CheckIfWaitForSleepPeriodActive(void)
{
    return gIsWaitForSleepPeriodActive;
}

/**
 * @brief This is a one-time per timer expiration notification mechanism
 */
bool CheckIfSleepWaitPeriodExpiredNotification(void)
{
    bool allowNotification = gAllowTimerExpirationOneTimeNotification;
    gAllowTimerExpirationOneTimeNotification = false;

    return allowNotification;
}

void StartWaitForSleepPeriod(void)
{
    LogInfo("Wait For Sleep period has begun\r\n");
    TryToStartTimer(gWaitForSleepTimerHandle);
    gIsWaitForSleepPeriodActive = true;
DBG7_SET(); //trigger logic probe: "Wait for sleep period active"
}

void StopWaitForSleepPeriod(void)
{
    if (gIsWaitForSleepPeriodActive)
    {
        LogInfo("Wait For Sleep period stopped before it finished.\r\n");
    }

    TryToStopTimer(gWaitForSleepTimerHandle);
    gIsWaitForSleepPeriodActive = false;
    gAllowTimerExpirationOneTimeNotification = false; //@todo remove for OMCU-714
DBG7_RESET();
}

static void waitForSleepTimerCallback(TimerHandle_t timer)
{
    TryToStopTimer(gWaitForSleepTimerHandle);
    gIsWaitForSleepPeriodActive = false;
    gAllowTimerExpirationOneTimeNotification = true;
    LogInfo("Wait For Sleep period is over. Charging will now begin.\r\n");
DBG7_RESET();
}

#endif
