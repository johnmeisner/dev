/**
 * @file       enumeration.h
 *
 * @brief      Header file for the enumeration period for the Forever Tracker.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef ENUMERATION_H
#define ENUMERATION_H

#include <stdbool.h>

void EnumerationInit(void);
bool CheckIfEnumerationPeriodActive(void);
void StartLongEnumerationPeriod(void);
void StartShortEnumerationPeriod(void);
void StopEnumerationPeriod(void);

#endif /* ENUMERATION_H */
