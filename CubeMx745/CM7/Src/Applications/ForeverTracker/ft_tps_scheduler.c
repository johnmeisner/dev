/**
 * @file       ft_tps_scheduler.c
 *
 * @brief      The TPS Scheudler for the Forever Tracker system
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdlib.h>
#include <string.h>
#include "orion_config.h"
#include "tps_scheduler.h"
#include "client.h"

#define FIRST_SPOT 0 ///< The first spot in a TPS schedule

#if defined(USE_FOREVER_TRACKER_BUILD)

// These functions are provided by client_manager.c
PRIVATE bool GetLongId(ShrtAddr_t shortId, ExtdAddr_t *longId);
PRIVATE bool GetClient(ExtdAddr_t longId, Client_t *client);
PRIVATE bool SaveClient(Client_t client);
PRIVATE bool RemoveClient(ExtdAddr_t longId);
PRIVATE void Walk(void (*func)(Client_t* client));
PRIVATE bool FindClientState(ClientState_t state, Client_t *client);

/**
 * This holds all of the receivers in list so that we can make a schedule at a later point.
 */
ReceiverList_t gScheduleCache;

// Configurable Parameters
static uint16_t gTpsMaxNumClients;   ///< The maximum allowed clients in the schedule, for fatally low and normal receivers
static uint16_t gTpsMaxFatalLowRecr; ///< The maximum allowed number of fatal low receivers
static uint16_t gChargingThreshold;  ///< The threshold value before charging ceases
uint16_t gTpsFatalLowHistMin;        ///< For fatal low hysteresis, This is the low-side value
uint16_t gTpsFatalLowHistMax;        ///< For fatal low hysteresis, This is the high-side value

static TonePowerSched_t gTonePowerSchedule;  ///< The tone power schedule sent to the proxy at the start of TPS
static uint8_t gTpsNumPowerSlotsPerClient;   ///< The number of power slots per client
static uint8_t gNumberOfFatallyLowReceivers; ///< The number of fatally low receivers in a schedule
static uint8_t gNumberOfTpsSchedulesBeforeRecalculation = 0; ///< The number of times we have used receivers from the cache before we must recalculate the cache
uint8_t gCurrentNumberOfReceiversInList;     ///< The current number of receivers scheduled during scheduling

void calculateCache(void);
void prepareForScheduling(void);
void populateListOfFatallyLowReceviers(void);
void populateListOfReceiversNeedingCharge(void);

static void populateListOfFatallyLowReceiversWalk(Client_t *client);
static void populateListOfReceiversNeedingChargeWalk(Client_t *client);
static void checkForFatallyLowWithHysteresis(Client_t *client);
static void scheduleFatalClient(Client_t *client);
static void replaceExistingReceiverIfLowerCharge(Client_t *client);
static void addReceiverToList(Client_t *client);
static bool cacheEntryIsValid(ReceiverCache_t cache);

static void findReceiversWithMaximumCharge(Client_t *client);
static void scheduleClientInRemainingScheduleSlots(Client_t *client);
static void replaceExistingReceiverIfHigherCharge(Client_t *client);
static void createScheduleBasedOffCacheAtEntry(uint8_t entry);
static bool doesReceiverHaveErrors(Client_t *client);

void IncrementTpsSeqNum(void)
{
    gTonePowerSchedule.seqNum++;
}

TonePowerSched_t* GetTpsSchedule(void)
{
    return &gTonePowerSchedule;
}

/**
 * @brief Calculate the TPS Schedule for the Forever Tracker project
 *
 *        This module is designed to slip in and replace an existing
 *        schedule located in `../Demo/tps_scheduler.c`.
 *
 *        For Forever Tracker, we will create a cache of receivers of
 *        no more than gTpsMaxNumClients receivers. Each of these
 *        receivers will get scheduled where they receive power for a
 *        4-second slot in a round robin fashion. Once
 *        gTpsMaxNumClients are charged in this round robin (or
 *        however many receivers are schedulable to charge, whichever
 *        is fewer), we will recalculate the cache and repeat the
 *        round robin process.
 *
 *        Fatally low receivers will get priority over all other
 *        receivers where the lowest batteries get scheduled up to
 *        CFG_MAX_NUM_OF_FATAL_LOW_RCVRS. If a receiver is lower than
 *        CFG_FATAL_LOW_HIST_MIN, it is considered fatally low and
 *        will be charged until CFG_FATAL_LOW_HIST_MAX. The rest of
 *        the receivers are all highest-charge first with a maximum
 *        charge set by CFG_CHG_PRIORITY_THRESHOLD.
 */
CotaError_t CalculateTpsSchedule(bool chargeVirtual)
{
    (void) chargeVirtual; // Virtual Client not supported

    if (gNumberOfTpsSchedulesBeforeRecalculation >= gCurrentNumberOfReceiversInList)
    {
        calculateCache();
        gNumberOfTpsSchedulesBeforeRecalculation = 0;
    }

    createScheduleBasedOffCacheAtEntry(gNumberOfTpsSchedulesBeforeRecalculation);
    gNumberOfTpsSchedulesBeforeRecalculation++;

    IncrementTpsSeqNum();

    return COTA_ERROR_NONE;
}

void ClearTpsSchedule(void)
{
    // These steps together will tell #CalculateTpsSchedule() to
    // recalculate the cache before creating a TPS schedule,
    // effectively clearing the TPS schedule.
    gNumberOfTpsSchedulesBeforeRecalculation = 0;
    gCurrentNumberOfReceiversInList = 0;
}

static void setEachCachedReceiverState(ClientState_t state)
{
    Client_t client;
    ExtdAddr_t longId;
    ReceiverCache_t *cache;

    for (uint8_t i = 0; i < gCurrentNumberOfReceiversInList; i++)
    {
        cache = &gScheduleCache.cache[i];

        if (cacheEntryIsValid(*cache) &&
            GetLongId(cache->shortId, &longId) &&
            GetClient(longId, &client))
        {
            client.state = state;
            SaveClient(client);
        }
    }
}

void calculateCache(void)
{
    prepareForScheduling();

    // The parameters resulting list (cache) are set by the
    // configuration parameters. These parameters are set on
    // initialization.
    populateListOfFatallyLowReceviers();

    // We don't know how many fatally low receivers, if any, where
    // scheduled. As a result, populateListOfReceiversNeedingCharge
    // must know how many receivers are scheduled prior to any more
    // scheduling. This means the ordering of calculateCache is
    // crucial.
    gNumberOfFatallyLowReceivers = gCurrentNumberOfReceiversInList;
    populateListOfReceiversNeedingCharge();
    setEachCachedReceiverState(RCVR_STATE_SCHEDULED);
}

void prepareForScheduling(void)
{
    // Makes it so the previous cache is set to
    // RCVR_STATE_READY_TO_CHARGE because they might not be scheduled
    // as part of this cache this time around.
    setEachCachedReceiverState(RCVR_STATE_READY_TO_CHARGE);

    memset((void*) &gScheduleCache, 0, sizeof(gScheduleCache));
    gCurrentNumberOfReceiversInList = 0;
    gNumberOfFatallyLowReceivers = 0;
}

/*****************************************************************
 *
 * Fatally Low Scheduling Functions
 *
 *****************************************************************/
void populateListOfFatallyLowReceviers(void)
{
    Walk(populateListOfFatallyLowReceiversWalk);
}

static void populateListOfFatallyLowReceiversWalk(Client_t *client)
{
    if (client != NULL)
    {
        checkForFatallyLowWithHysteresis(client);
    }
}

static void checkForFatallyLowWithHysteresis(Client_t *client)
{
    uint8_t charge = client->qStatus.status.batteryCharge;
    if (charge < gTpsFatalLowHistMin)
    {
        client->fatalLow = true;
    }

    if (IsRcvrReadyForCharging(client) &&
        (charge <= gTpsFatalLowHistMax) &&
        (client->fatalLow == true) &&
        !doesReceiverHaveErrors(client))
    {
        scheduleFatalClient(client);
    }

    if (charge > gTpsFatalLowHistMax)
    {
        client->fatalLow = false;
    }
}

static void scheduleFatalClient(Client_t *client)
{
    if (gCurrentNumberOfReceiversInList < gTpsMaxFatalLowRecr)
    {
        addReceiverToList(client);
    }
    else
    {
        replaceExistingReceiverIfLowerCharge(client);
    }
}

static void replaceExistingReceiverIfLowerCharge(Client_t *client)
{
    ReceiverCache_t *cache;
    uint8_t   listBattery, paramBattery;
    bool found = false;

    for (int i = 0; i < gCurrentNumberOfReceiversInList && !found; ++i)
    {
        // Parameterize the values so the code is a little easier to read
        cache = &gScheduleCache.cache[i];
        listBattery = cache->battery;
        paramBattery = client->qStatus.status.batteryCharge;

        if (cacheEntryIsValid(*cache) &&
            (paramBattery < listBattery) &&
            (client->shortId != cache->shortId))
        {
            cache->battery = paramBattery;
            cache->shortId = client->shortId;
            found = true;
        }
    }
}

static void addReceiverToList(Client_t *client)
{
    ReceiverCache_t *cache;
    bool found = false;

    for (int i = 0; i < MAX_NUM_CLIENTS_IN_TPS && !found; ++i)
    {
        cache = &gScheduleCache.cache[i];

        if (!cacheEntryIsValid(*cache))
        {
            cache->battery = client->qStatus.status.batteryCharge;
            cache->shortId = client->shortId;
            cache->isValid = true;
            gCurrentNumberOfReceiversInList++;

            found = true; // So we only add the receiver to 1 slot
        }
    }
}

static bool cacheEntryIsValid(ReceiverCache_t cache)
{
    return cache.isValid;
}

/*****************************************************************
 *
 * Highest battery charge priority Scheduling Functions
 *
 *****************************************************************/
void populateListOfReceiversNeedingCharge(void)
{
    Walk(populateListOfReceiversNeedingChargeWalk);

}

static void populateListOfReceiversNeedingChargeWalk(Client_t *client)
{
    if (client != NULL)
    {
        findReceiversWithMaximumCharge(client);
    }
}

static void findReceiversWithMaximumCharge(Client_t *client)
{
    uint8_t charge = client->qStatus.status.batteryCharge;

    // We don't charge fatally low receivers because they have already
    // been scheduled prior to this function.
    if (IsRcvrReadyForCharging(client) &&
        (client->fatalLow == false) &&
        (charge <= gChargingThreshold) &&
        !doesReceiverHaveErrors(client))
    {
        scheduleClientInRemainingScheduleSlots(client);
    }
}

static void scheduleClientInRemainingScheduleSlots(Client_t *client)
{
    if (gCurrentNumberOfReceiversInList < gTpsMaxNumClients)
    {
        addReceiverToList(client);
    }
    else
    {
        replaceExistingReceiverIfHigherCharge(client);
    }
}

static void replaceExistingReceiverIfHigherCharge(Client_t *client)
{
    ReceiverCache_t *cache;
    uint8_t   listBattery, paramBattery;
    bool found = false;

    // This function can't overwrite receivers that were already
    // scheduled when we scheduled the fatally low receivers. So, we
    // start from gNumberOfFatallyLowReceivers
    for (int i = gNumberOfFatallyLowReceivers; i < gCurrentNumberOfReceiversInList && !found; ++i)
    {
        // Parameterize the values so the code is a little easier to read
        cache = &gScheduleCache.cache[i];
        listBattery = cache->battery;
        paramBattery = client->qStatus.status.batteryCharge;

        if (cacheEntryIsValid(*cache) &&
            (paramBattery > listBattery) &&
            (client->shortId != cache->shortId))
        {
            cache->battery = paramBattery;
            cache->shortId = client->shortId;
            found = true;
        }
    }
}

static bool doesReceiverHaveErrors(Client_t *client)
{
    // We are seeing issues with receivers that fail to communicate
    // with the gas guages. In these cases, we see -32767 as the
    // current into the battery, and 0 for battery level. We cannot
    // schedule these receivers because we do not know their true
    // battery values, and due to a reported 0 battery level they will
    // have high priorities to the FT scheduler. This will cause them
    // to steal charge time away from other receivers that may need it
    // more. If we see this bad current value, those receivers cannot
    // be scheduled for charge.
    //
    // @todo This is a bandaid solution to this problem, and should be
    //       removed after it is solved on the receiver side.
    return (client->qStatus.status.peakNetCurrent == ((int16_t) -32767));
}

static void createScheduleBasedOffCacheAtEntry(uint8_t entry)
{
    TonePowerSched_t *schedule;
    schedule = &gTonePowerSchedule;
    schedule->clientTable[FIRST_SPOT].shortId = gScheduleCache.cache[entry].shortId;

    // For FT schedule, we will only be scheduling one receiver at a
    // time based on the contents of the schedule cache. Once each
    // receiver in the cache has been charged, then we recalculate the
    // cache and repeat the process.
    schedule->numClients = 1;
    schedule->prxCmd = HOST_PRX_TPS_TABLE;
    schedule->clientTable[FIRST_SPOT].toneSlot = 1;
    schedule->clientTable[FIRST_SPOT].tonePeriod = 1;
    schedule->clientTable[FIRST_SPOT].powerSlot = 1;
    schedule->clientTable[FIRST_SPOT].powerDuration = gTpsNumPowerSlotsPerClient;
    schedule->clientTable[FIRST_SPOT].powerPeriod = gTpsNumPowerSlotsPerClient * gTonePowerSchedule.numClients;
}

#if defined(UNIT_TESTS)
void InitializeScheduler(void)
{
    gTonePowerSchedule.seqNum = rand() & 0xff;

    gChargingThreshold = 82;
    gTpsMaxNumClients = 8;
    gTpsNumPowerSlotsPerClient = 1;
    gTpsMaxFatalLowRecr = 3;
    gTpsFatalLowHistMin = 18;
    gTpsFatalLowHistMax = 22;
}
#else
void InitializeScheduler(void)
{
    gTonePowerSchedule.seqNum = rand() & 0xff;

    CfgGetParam(CFG_RCVR_CHARGING_THRESHOLD,
                &gChargingThreshold,
                sizeof(gChargingThreshold));

    CfgGetParam(CFG_TPS_NUM_CLIENTS,
                &gTpsMaxNumClients,
                sizeof(gTpsMaxNumClients));

    CfgGetParam(CFG_TPS_NUM_POWER_SLOTS_PER_CLIENT,
                &gTpsNumPowerSlotsPerClient,
                sizeof(gTpsNumPowerSlotsPerClient));

    CfgGetParam(CFG_MAX_NUM_OF_FATAL_LOW_RCVRS,
                &gTpsMaxFatalLowRecr,
                sizeof(gTpsMaxFatalLowRecr));

    CfgGetParam(CFG_FATAL_LOW_HIST_MIN,
                &gTpsFatalLowHistMin,
                sizeof(gTpsFatalLowHistMin));

    CfgGetParam(CFG_FATAL_LOW_HIST_MAX,
                &gTpsFatalLowHistMax,
                sizeof(gTpsFatalLowHistMax));
}
#endif

#endif // defined(USE_FOREVER_TRACKER_BUILD)
