/**
 * @file       config_response_sm.c
 *
 * @brief      Configures the config response for the Forever tracker implementation.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#if defined(USE_FOREVER_TRACKER_CONFIG_RESP)

void ConfRespEntry(void)
{
    
}

bool RdyToConfRespCheck(void)
{

}

bool ConfRespToRdyCheck(void)
{
    
}

#endif // defined(USE_FOREVER_TRACKER_CONFIG_RESP)
