/**
 * @file       ft_client_interface.c
 *
 * @brief      Interface for managing clients for ForeverTracker project.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include "FreeRTOS.h"
#include "projdefs.h"
#include "task.h"
#include "client.h"
#include "client_manager.h"
#include "error.h"
#include "CotaCommonTypes.h"
#include "uart_driver.h"
#include "print_format.h"
#include "nvm_driver.h"
#include "proxyClientMsgInf.h"
#include "cqt_scheduler.h"
#include "tps_scheduler.h"
#include "rpi_msg_interface.h"
#include "proxy_msg_interface.h"
#include "rpi_msg_interface.h"
#include "orion_utilities.h"
#include "log.h"
#include "ctrl_interface.h"
#include "client_interface.h"

#if defined(USE_FOREVER_TRACKER_BUILD)
#include "ForeverTracker/ft_client_interface.h"

///< These functions are provided by client_manager.c
PRIVATE bool GetLongId(ShrtAddr_t shortId, ExtdAddr_t *longId);
PRIVATE bool GetClient(ExtdAddr_t longId, Client_t *client);
PRIVATE bool SaveClient(Client_t client);
PRIVATE void Walk(void (*func)(Client_t* client));
PRIVATE bool FindClientState(ClientState_t state, Client_t *client);
PRIVATE bool RemoveClient(ExtdAddr_t longId);

static void removeUnresponsiveRcvrsWalk(Client_t *client);
static void setAllAnnouncedReceiversToBeRemovedWalk(Client_t *client);
static void markSleepingReceiversForRemovalWalk(Client_t *client);

static bool            gClientListAll;        ///< A global allowing a walk to know if client_list is called with the 'all' argument
static uint8_t         gAppCmdTriesLeft;      ///< The value of app cmd try count to set all register clients to

/**
 * @brief Changes any client in the charing state to the ready state.
 *        All clients 'userChargeRequested' flag is set to 0.
 */
CotaError_t CmStopChargingAllClients(void)
{
    return COTA_ERROR_NOT_IMPLEMENTED;
}

/**
 * @brief After we get a command to start charging a client via the
 *        message manager (RPi), we also need to return data back for
 *        a returned JSON message. This message will do that by going
 *        through each client that is charging and returning some data
 *        about them.
 */
CotaError_t CmSendChargingClientData(void)
{
    return COTA_ERROR_NOT_IMPLEMENTED;
}

/**
 * @brief Changes any client in the ready  state to the charging state.
 *        The client 'userChargeRequested' flag is also set, but only in clients
 *        change to the charging state.
 */
CotaError_t CmStartChargingAllClients(void)
{
   return COTA_ERROR_NOT_IMPLEMENTED;
}

/**
 * @brief Changes a client's state to the ready state if it's in the charging state.
 *        The client 'userChargeRequested' flag is also set
 *
 * @param longId  The long id of the client
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t CmStopChargingClient(ExtdAddr_t longId)
{
    return  COTA_ERROR_NOT_IMPLEMENTED;
}

/**
 * @brief Changes a client's state to the charging state if it's in the ready state.
 *        The client 'userChargeRequested' flag is also set
 *
 * @param longId  The long id of the client
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t CmStartChargingClient(ExtdAddr_t longId)
{
    return  COTA_ERROR_NOT_IMPLEMENTED;
}

/**
 * @brief A walk function designed to sets the app command try count
 * @param client A pointer to the client being considered.
 */
static void setAppCmdTriesLeftClientWalk(Client_t* client)
{
    if ((client->state == RCVR_STATE_NOT_READY_TO_CHARGE) ||
        (client->state ==  RCVR_STATE_READY_TO_CHARGE)  ||
        (client->state == RCVR_STATE_SCHEDULED) )
        // (client->state ==  RCVR_STATE_REMOVE_FROM_DATABASE)
    {
        client->appCmdTriesLeft = gAppCmdTriesLeft;
    }
}

/**
 * @brief Sets the app command try count for all registered clients
 *
 * @param appCmdTriesLeft The number of times left to attempt to send the app command
 */
void CmSetAppCmdTriesLeftAll(uint8_t appCmdTriesLeft)
{
    gAppCmdTriesLeft = appCmdTriesLeft;
    Walk(setAppCmdTriesLeftClientWalk);
}

CotaError_t CmMarkClientAsDisconnect(ExtdAddr_t* longId)
{
    return  COTA_ERROR_NOT_IMPLEMENTED;
}

CotaError_t CmMarkAllClientsAsDisconnect(void)
{
     return COTA_ERROR_NOT_IMPLEMENTED;
}

/**
 * @brief This function is supposed to be used with Walk() from
 *          client_manager.h. Will print each client and their state
 *          to the CLI.
 * @param client - Client that will be printed
 */
static void printClient(Client_t* client)
{
    if (gClientListAll || (client->state != RCVR_STATE_STATE_UNKNOWN))
    {
        PostPrintf("0x%016llx  0x%04x   %-20s  %4d  %4d  %4i    %3s   %llu ms\r\n",
                   *(uint64_t*)&client->longId, client->shortId, GetClientStateString(client->state),
                   client->qStatus.status.batteryCharge, client->priority,
                   client->qStatus.status.peakNetCurrent,
                   client->qStatus.status.trackerStatusFlags.sleepFlag ? "yes" : "no ",
                   TICKS_TO_MS(client->lastCommTimeTicks));
    }
}

/**
 * @brief Send a basic client information to the RPi. Supposed to walk
 *          the client tree via function Walk()
 * @param client - Client data to be sent to RPi
 */
static void sendClientListToRpiWalk(Client_t *client)
{
    uint16_t    len = 0;
    CtrlResp_t  msg;

    if (gClientListAll || (client->state != RCVR_STATE_STATE_UNKNOWN))
    {
        msg.id                              = CTRL_REDUCED_RCVR_INFO;
        msg.err                             = COTA_ERROR_NONE;
        msg.reducedRcvrInfo.longId        = client->longId;
        msg.reducedRcvrInfo.shortId       = client->shortId;
        msg.reducedRcvrInfo.state         = client->state;
        msg.reducedRcvrInfo.batteryCharge = client->qStatus.status.batteryCharge;
        msg.reducedRcvrInfo.statusFlags   = client->qStatus.status.statusFlags;
        msg.reducedRcvrInfo.avgPower      = client->qStatus.status.avgPower;
        msg.reducedRcvrInfo.peakPower     = client->qStatus.status.peakPower;
        msg.reducedRcvrInfo.netCurrent    = client->qStatus.status.peakNetCurrent;
        msg.reducedRcvrInfo.rssi          = client->qStatus.header.rssi;
        msg.reducedRcvrInfo.linkQuality   = client->qStatus.header.linkQuality;

        len = sizeof(msg.id) +
              sizeof(msg.err) +
              sizeof(RespReducedRcvrInfo_t);

        // To not overload the system with messages, we add a delay.
        RpiSendMessage((uint8_t*) &msg, len);
        vTaskDelay(pdMS_TO_TICKS(5));
    }
}

/**
 * @brief This function will walk the clients in the tree do the following:
 *          1. Print out the list of clients via UART
 *          2. Send the list of clients to the MM via the rpi_msg_interface API.
 */
void CmClientList(bool all)
{
    gClientListAll = all;
    PostStringForTransmit("Client List:\r\n");

    // For the CLI
    PostPrintf("Long-Id-----------  Short-Id State---------------  Batt  Prio Current  Sleep Last-Comm-Recv\r\n");
    Walk(printClient);

    // For the Host, RPi, Message Manager
    Walk(sendClientListToRpiWalk);
}

/**
 * @brief Takes TPC confirmation results and updates the client
 *         manager.
 *
 * @param tpcAck - PrxHostTpcCfn_t message received from a client (from
 *          the proxy)
 *
 * @return COTA_ERROR_NONE on success,
 *          COTA_ERROR_RCVR_COULD_NOT_BE_FOUND if client could not be
 *          found. COTA_ERROR_BAD_PARAMETER is the message was not
 *          successful.
 */
CotaError_t CmClientTpcCfn(PrxHostTpcCfn_t tpcAck)
{
    Client_t client;
    ExtdAddr_t longId;

    GetLongId(tpcAck.shortAddr, &longId);

    if (tpcAck.status == PRX_HOST_STAT_SUCCESS) // Indicates successful tpc
    {
        if (GetClient(longId, &client))
        {
            if (client.qStatus.status.trackerStatusFlags.sleepFlag)
            {
                client.state = RCVR_STATE_READY_TO_CHARGE;
            }
            else
            {
                client.state  = RCVR_STATE_NOT_READY_TO_CHARGE;
            }
            client.queryNeededFlag      = true;
            client.lastCommTimeTicks    = xTaskGetTickCount();
            client.commFailCount        = 0;
            SaveClient(client);
            return COTA_ERROR_NONE;
        }
        return COTA_ERROR_RCVR_COULD_NOT_BE_FOUND;
    }
    else
    {
        LogError("TPC confirmation failed for 0x%016llx shortId 0x%04x\r\n",
                   *(uint64_t*)&longId, tpcAck.shortAddr);
        if (GetClient(longId, &client))
        {
            client.commFailCount++;
            SaveClient(client);
        }
    }

    return COTA_ERROR_BAD_PARAMETER;
}

/**
 * @brief This is a special function that is used by the NVM module to
 *        initialize the client manager based on the client data
 *        stored in the NVM module. This function should only be
 *        called from the nvm_driver.c module as part of
 *        initialization.
 *
 * @param nvmClient - A client slot data from the NVM module
 * @return COTA_ERROR_NONE w/ no error.
 */
CotaError_t CmAddClientFromNvm(ClientSlotData_t nvmClient)
{
    return COTA_ERROR_NOT_IMPLEMENTED;
}

bool CmGetTpcClient(ShrtAddr_t *shortId)
{
    //Not implemented in FT
    return false;
}

CotaError_t CmClientJoined(PrxHostClientJoinRspMsg_t joinAck)
{
    return COTA_ERROR_NOT_IMPLEMENTED;
}

CotaError_t CmRegisterClient(ExtdAddr_t longId)
{
    return COTA_ERROR_NOT_IMPLEMENTED;
}

CotaError_t CmRegisterAllClients(CotaRcvrQuery_t queryType)
{
    return COTA_ERROR_NOT_IMPLEMENTED;
}

void CmReportAnnouncement(PrxHostAnnouncement_t announcement)
{
    Client_t client;

    InitializeClientStruct(&client);

    client.longId = announcement.extAddr;
    client.state = RCVR_STATE_ANNOUNCED;

    // Check if a receiver with this Long ID already exists. This
    // handles the case where we had received two announcements from
    // the receiver before configuring it.
    if (!GetClient(announcement.extAddr, &client))
    {
        client.shortId = GetNextShortId();
    }

    SaveClient(client);
}

void CmReportConfigurationResponse(PrxHostRcvrCfgRsp_t configResponse)
{
    Client_t client;
    ExtdAddr_t longId;

    if (configResponse.status == PRX_HOST_STAT_SUCCESS)
    {
        if (GetLongId(configResponse.shortAddr, &longId) &&
            GetClient(longId, &client))
        {
            client.version                  = configResponse.configResp.fwVersion;
            client.hwModel                  = configResponse.configResp.hwModel;
            client.commFailCount            = 0;
            client.state                    = RCVR_STATE_NOT_READY_TO_CHARGE;
            client.qStatus.header.queryType = RCVR_QUERY_OPTIMIZED;
            client.queryNeededFlag          = true;

            SaveClient(client);
        }
    }
}

void CmHandleUnresponsiveClients(void)
{
    //Walk the tree and mark all stale registered clients as silent.
    Walk(removeUnresponsiveRcvrsWalk);
}

static void removeUnresponsiveRcvrsWalk(Client_t *client)
{
    if (client != NULL)
    {
        if (IsRcvrReadyForComm(client))
        {
            if (client->commFailCount > CmGetCommMaxFailCount())
            {
                LogInfo("Unresponsive client 0x%016llx after %d failed queries is marked for removal.\r\n",
                        *(uint64_t*)&client->longId,
                        client->commFailCount);
                client->state = RCVR_STATE_REMOVED_FROM_DATABASE;
            }
        }
    }
}

/**
 * @brief Finds a receiver with state RCVR_STATE_REMOVED_FROM_DATABASE
 *        and copies it to parameter client.
 */
bool CmGetReceiverToRemove(Client_t *client)
{
    if (client != NULL)
    {
        if (FindClientState(RCVR_STATE_REMOVED_FROM_DATABASE, client))
        {
            return true;
        }
    }
    return false;
}

bool CmRemoveReceiver(ExtdAddr_t longId)
{
    if (IsLongValid(longId))
    {
        return RemoveClient(longId);
    }
    return false;
}

void CmSetAllAnnouncedReceiversToBeRemoved(void)
{
    Walk(setAllAnnouncedReceiversToBeRemovedWalk);
}

static void setAllAnnouncedReceiversToBeRemovedWalk(Client_t *client)
{
    if (client != NULL)
    {
        if (client->state == RCVR_STATE_ANNOUNCED)
        {
            client->state = RCVR_STATE_REMOVED_FROM_DATABASE;
        }
    }
}

void MarkSleepingReceiversForRemoval(void)
{
    Walk(markSleepingReceiversForRemovalWalk);
}

static void markSleepingReceiversForRemovalWalk(Client_t *client)
{
    if (client != NULL)
    {
        if (!client->qStatus.status.trackerStatusFlags.sleepFlag)
        {
            client->state = RCVR_STATE_REMOVED_FROM_DATABASE;
        }
    }
}

#endif
