/**
 * @file       ft_client_interface.h
 *
 * @brief      Header file for the ft_client_interface.c module supporting Forever Tracker build
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CTRL_INTERFACE_FT_H_
#define _CTRL_INTERFACE_FT_H_

#include <stdbool.h>
#include "error.h"
#include "client.h"
#include "nvm_driver.h"

CotaError_t CmAddClientFromNvm(ClientSlotData_t nvmClient);
bool CmGetTpcClient(ShrtAddr_t *shortId);
CotaError_t CmRegisterAllClients(CotaRcvrQuery_t queryType);
CotaError_t CmClientJoined(PrxHostClientJoinRspMsg_t joinAck);
CotaError_t CmRemoveClients(uint16_t numberOfClients);
CotaError_t CmMarkClientAsDisconnect(ExtdAddr_t* longId);
CotaError_t CmMarkAllClientsAsDisconnect(void);
CotaError_t CmStartChargingClient(ExtdAddr_t longId);
CotaError_t CmStopChargingClient(ExtdAddr_t longId);
void        CmSetAppCmdTriesLeftAll(uint8_t appCmdTriesLeft);
CotaError_t CmStartChargingAllClients(void);
CotaError_t CmSendChargingClientData(void);
CotaError_t CmStopChargingAllClients(void);
void CmReportAnnouncement(PrxHostAnnouncement_t announcement);
void CmReportConfigurationResponse(PrxHostRcvrCfgRsp_t configResponse);
void CmClientList(bool all);
CotaError_t CmRegisterClient(ExtdAddr_t longId);
CotaError_t CmClientTpcCfn(PrxHostTpcCfn_t tpcAck);
void CmHandleUnresponsiveClients(void);
bool CmGetReceiverToRemove(Client_t *client);
bool CmRemoveReceiver(ExtdAddr_t longId);
void CmSetAllAnnouncedReceiversToBeRemoved(void);
void MarkSleepingReceiversForRemoval(void);

#endif /* _CTRL_INTERFACE_FT_H_ */
