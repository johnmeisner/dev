/**
 * @file       ft_query.h
 *
 * @brief      Header module for the ft_query.c module
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2021 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#ifndef _FT_QUERY_H_
#define _FT_QUERY_H_

#if defined(USE_FOREVER_TRACKER_BUILD)
  void SetFastQuery(bool state);
  void DoImplementationSpecificQueryCycle(void);
#endif // defined(USE_FOREVER_TRACKER_BUILD)

#endif /* _FT_QUERY_H_ */