/**
 * @file       tps_charge_ready_check.h
 *
 * @brief      A utility module for tps charging logic specific to ForeverTracker
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef TPS_CHARGE_READY_H
#define TPS_CHARGE_READY_H

#include <stdbool.h>

/**
 * @brief Checks if all "user devices" (e.g. tracker board) attached to receivers
 *        are sleeping (ready to charge) in the system.
 */
bool AllRxReadyToCharge(void);

#endif /* TPS_CHARGE_READY_H */
