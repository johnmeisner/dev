/**
 * @file       receiver_removal_sm.h
 *
 * @brief      Header for the receiver_removal_sm module
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef RECEIVER_REMOVAL_SM_H
#define RECEIVER_REMOVAL_SM_H

#include <stdbool.h>

void ReceiverRemovalStateInit(void);
bool StartReceiverRemovalTimers(void);
void TryToRemoveBadReceivers(void);

#endif /* RECEIVER_REMOVAL_SM_H */
