/**
 * @file       ft_query.c
 *
 * @brief      Implementation of query control for ForeverTracker project.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2021 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "cqt_scheduler.h"
#include "client_interface.h"
#include "query_sm.h"
#include "query_sm_helper.h"
#include "client.h"
#include "debug_on.h"

#if defined(USE_FOREVER_TRACKER_BUILD)
#include "ForeverTracker/enumeration.h"
#include "ForeverTracker/wait_for_sleep.h"

volatile bool gFastQueryEnabled = true;        ///< true to start a fast query cycle

/**
 *  @brief Called to enable/disable fast query
 *
 * @param state true to enable, false to disable fast query
 */
void SetFastQuery(bool state)
{
    gFastQueryEnabled = state;
}

/**
 *  @brief Called for each query cycle
 *
 *  @brief Sets up for what happens next. To be called from QueryToRdyExit()
 */
void DoImplementationSpecificQueryCycle(void)
{
DBG5_SET(); //trigger logic probe: "Query of receiver"
    if (CheckIfWaitForSleepPeriodActive())
    {
        SetFastQuery(true);
    }

    if (DoRxStillNeedQuery())
    {
        if(gFastQueryEnabled)
        {
            //FAST QUERY: query all receivers in schedule table
            EnableQueryCycle(true);
        }
        else
        {
            //SLOW QUERY: query and charge one receiver pair at a time
            StartQueryTimer();
        }
    }
    else
    {
        // Finished sending queries
        WrapUpClientQueryState();
        LogInfo("All clients have been sent queries as part of the query period\r\n");

        if(gFastQueryEnabled)
        {
            EnableQueryCycle(false);
            StartQueryTimer();
        }
        else
        {
            EnableQueryCycle(true);
            StopQueryTimer();
            StartShortEnumerationPeriod(); // Period for receivers that reset, failed to configure properly, or failed to query
        }
        gFastQueryEnabled = !gFastQueryEnabled;
    }
DBG5_RESET();
}



#endif // defined(USE_FOREVER_TRACKER_BUILD)
