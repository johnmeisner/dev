/**
 * @file       forever_tracker_config.c
 *
 * @brief      This defines the system config for the ForeverTracker build.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_FOREVER_TRACKER_BUILD)

BuildType_t gSystemBuildType = FOREVER_TRACKER;

/**
 * Default system configuration data for Forever Tracker. For the
 * meaning of each of these values, refer to /Common/orion_config.c/h
 */
const SysConfig_t gDefaultSystemConfig =
{
    .boardId                       = 0,
    .configFileVersion             = 0,
    .calibrationUvp                = 13,
    .calibrationAmb                = 0,
    .calibrationAmu                = 1,
    .referenceUvp                  = 0,
    .referenceAmb                  = 0,
    .referenceAmu                  = 2,
    .autoCalibrationPeriod         = 5,
    .calTempSmplPeriod             = 0,
    .calTempThreshold              = 0,
    .clientChargePriorityThreshold = 20,
    .commChannel                   = 25,
    .maxQueries                    = 1,
    .queryPeriod                   = 3500,
    .clientQueryTimeout            = 80,
    .clientResponseTimeout         = 120,
    .criticalTemperature           = 60,
    .debugMode                     = 0,
    .discoveryPeriod               = 10,
    .discoveryTimeout              = 60,
    .discoveryWait                 = 100,
    .joinPeriod                    = 10,
    .joinWait                      = 30,
    .tpcPeriod                     = 10,
    .tpcWait                       = 30,
    .uvpEncodeSize                 = 32,
    .goodAmbChannels               = 0xF,
    .lpmSlotCount                  = 0,
    .maxQueryFailCount             = 3,
    .networkPanId                  = 0x2345,
    .startingClientId              = 1,
    .proxyId                       = 0xBCDE,
    .obstructionAngleThreshold     = 0,
    .phaseAdjustmentPeriod         = 0,
    .powerLevel                    = 20000,
    .powerMode                     = 0,
    .shortCalibrationDuration      = 0,
    .shortCalibrationPeriod        = 0,
    .systemType                    = 0,
    .tpsNumBeaconBeats             = 1,
    .tpsNumClients                 = 5,
    .tpsNumPowerSlots              = 100,
    .tpsNumPowerSlotsPerClient     = 100,
    .tpsPaDelay                    = 25,
    .tpsRxDelay                    = 670,
    .tpsRxDuration                 = 1000,
    .tpsStartDelay                 = 1191,
    .tpsTxDuration                 = 39990,
    .tpsTimeout                    = 5000,
    .preTpsClientPause             = 3,
    .postTpsClientPause            = 5,
    .txFrequencyMhz                = 0,
    .systemClockConfig             = 0,
    .validFansMask                 = 0x3,
    .appCmdTimeout                 = 100,
    .appCmdMaxSend                 = 2,
    .discClientTimeout             = 200,
    .discClientMaxSend             = 2,
    .lightRingUpdatePeriod         = 1,
    .sysClkAmplitude               = 1,
    .sysClkCommonMode              = 0xd,
    .sysClkFormat                  = 1,
    .sysClkAmbInversion            = 0x0,
    .sysClkIdleDisable             = 0,
    .sysLogLevel                   = 2,
    .appAllCmdMaxTries             = 3,
    .proxyTestPeriod               = 30,
    .proxyTestWaitTimeout          = 100,
    .doorOpenMsgPeriod             = 1000,
    .rcvrLongConfigRespTimeout     = 30000,
    .rcvrSleepTime                 = 90,
    .rcvrChargingThreshold         = 82,
    .rcvrCommPower                 = 5,
    .ethernetClockState            = 0,
    .ftTpsFatalLowHistMin          = 10,
    .ftTpsFatalLowHistMax          = 12,
    .ftTpsMaxNumFatalLowRcvr       = 3,
    .ftWaitForSleepPeriodTimeout   = 300,
    .ftReceiverRemovalPeriod       = 10000,
    .ftShortConfigRespTimeout      = 5000,
    .ftProxyAnnouncementDelayMs    = 100,
    .ftProxyRssiFilterMinValue     = -60,
};

#endif // defined(USE_FOREVER_TRACKER_BUILD)
