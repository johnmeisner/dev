/**
 * @file       ctrl_interface.c
 *
 * @brief      Defines the messages that the control task is able to accept 
 *
 * The purpose of this module is to manage the message queue and define messages.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */



/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////
#include <stdint.h>
#include <string.h>
#include "ctrl_interface.h"
#include "process_uvp_commands.h"
#include "variable_queue.h"
#include "stm32h7xx_hal.h"
#include "error.h"
#include "orion_config.h"
#include "ctrl_task.h"     
     
/////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////
#define CTRL_INTF_SIZE  1024
#define MAX_MSG_SIZE    sizeof(CtrlMsg_t)

/////////////////////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////
// Private Variable Declarations
/////////////////////////////////////////////////////////////////////////////////////////////
static VariableQueueControl_t gCtrlQueue;                 ///< Handle the variable control queue for the control interface
static uint8_t gCtrlQueueBuf[CTRL_INTF_SIZE];             ///< The buffer for the variable queue for the control interface
static uint8_t gTempBuf[MAX_MSG_SIZE + sizeof(uint8_t)];  ///< Keeps the temporary buffer off the stack

/**
 * @brief Initialize the ctrl interface by creating the variable queue
 *
 * @return return COTA_ERROR_NONE on success; otherwise it returns an error code
 */ 
CotaError_t CtrlInterfaceInit(void)
{
    return VariableQueueCreateStatic(CTRL_INTF_SIZE, gCtrlQueueBuf, &gCtrlQueue);
}

/**
 * @brief Retrieves a message from a variable queue
 *
 * @param buf  Buffer to get the message
 * @param size Size of the buffer in bytes
 *
 * @return  The number of bytes removed from the queue.
 */
uint16_t CtrlGetMsg(uint8_t *buf, uint16_t size)
{
    if (buf == NULL)
    {
        return 0;
    }

    return VariableQueueReceive(&gCtrlQueue, buf, size, CTRL_NO_DELAY);
}
/**
 * @brief Adds a message to the Control Task processing queue
 *
 * @param msgId  Message ID
 * @param buf    Pointer to the message buffer
 * @param size   Message size in the buffer
 * 
 * @note  This function must be called from a task context (not from an ISR)
 * @note  This function truncates the CtrlMsgId_t to be a uint8_t
 */
uint16_t CtrlSendMsg(CtrlMsgId_t msgId, uint8_t *buf, uint16_t size)
{
    size = MIN(size, MAX_MSG_SIZE);

    // This sticks the message ID at the 0th element
    // And the message itself at the 1st element 
    gTempBuf[0] = (uint8_t) msgId;
    memcpy((void *)&gTempBuf[1], (void *)buf, size);

    VariableQueueSend(&gCtrlQueue, gTempBuf, (size + sizeof(uint8_t)));
    
    return 0;
}
