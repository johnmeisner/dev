/**
* @file       process_uvp_commands.h
*
* @brief      Process uvp commands sent from CLI or through the control interface
 *
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*/

#include "main.h"
#include "amb_control.h"

#ifndef _PROCESS_UVP_COMMANDS_
#define _PROCESS_UVP_COMMANDS_


//For CTRL_UVP_READ, CTRL_UVP_WRITE, and CTRL_LOCK_DETECT
/**
 * @brief A structure containing mask information to identify AMB's and UVP's
 */
typedef PACKED struct _AmbUvpMask_t
{
    AmbMask_t ambMask;    ///< AMB Mask containing the AMB's to read or write to
    UvpMask_t uvpMask;    ///< UVP Mask specifying which UVP's read or write to
} AmbUvpMask_t;

/**
 * @brief A structure containing information to identify a single UVP
 */
typedef PACKED struct _AmbUvpNum_t
{
    AmbNum_t ambNum;    ///< AMB number containing the UVP to read or write to
    UvpNum_t uvpNum;    ///< UVP number to read or write to
} AmbUvpNum_t;

/**
 * @brief Contains information needed to read (or write) to a UVP register
 */
typedef PACKED struct _UvpRegRead_t
{
    AmbUvpMask_t id; ///< A structure with the AMB and UVP numbers
    uint8_t page;    ///< The page in the UVP to read from
    uint8_t addr;    ///< The address in the UVP to read a 32 bit chunck of UVP memory from
    uint8_t size;    ///< Size in bits of the register
    uint8_t offset;  ///< The offset of the register in the 32 bit UVP memory region.
} UvpRegRead_t;

/**
 * @brief Contains information needed to write to a UVP register
 */
typedef PACKED struct _UvpRegWrite_t
{
    UvpRegRead_t regInfo;   ///< A structure with the register info
    uint32_t     val;       ///< The value to write to the register
} UvpRegWrite_t;

CotaError_t ProcessUvpWriteCommand(UvpRegWrite_t* uvpWriteInfo);
CotaError_t ProcessUvpReadCommand(UvpRegRead_t* uvpReadInfo);
CotaError_t ProcessUvpLockCommand(AmbUvpMask_t* uvpLockInfo);

#endif //_PROCESS_UVP_COMMANDS_
