/**
 * @file       gather_and_vote_task.c
 *
 * @brief      Gather and Vote task implementation
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 * @verbatim
 * ===============================================================================
 *  The Gather and Vote - Download Phase, RSSI, and complete RSSI Voting
 * ===============================================================================
 * -------------------------------------------------------------------------------
 *  Introduction 
 * -------------------------------------------------------------------------------
 *
 * This task will complete the following tasks in order:
 *   1) Gather all RSSI data from the UVPs. 
 *   2) Complete RSSI Voting. Based on the results of RSSI voting, either continue
 *        or terminate transmission.
 *   3) Gather all phase data from the UVPs.
 *
 * Information for how RSSI and Phase information gets downloaded can be found as 
 *   part of the SiWare documentation. For more information, those documents can
 *   be found on either ossianet, or SVN.
 *
 * -------------------------------------------------------------------------------
 *  RSSI Voting
 * -------------------------------------------------------------------------------
 *
 * To meet SAR (Specific Absorbtion Rate) requirements established by the FCC, we 
 *   do a procedure called "RSSI Voting". If a transmitter becomes blocked by a 
 *   body, we will disable transmission out of select areas of the transmitter, 
 *   or terminate transmission altogether. 
 *
 * We do this in two ways:
 *   1) Using the RSSI_LOWTHRESH register in the UVPs. When a client outputs a 
 *        beacon as part of TPS, each UVP will compare the RSSI of the recently 
 *        measured beacon, with the RSSI saved in the RSSI_LOWTHRESH register. 
 *        If the measured RSSI is less than the RSSI in RSSI_LOWTHRESH, then the
 *        AMU has 'violated' the low threshold. Violating AMUs will not transmit. 
 *        This entire operation occurs in hardware on the UVP.
 *
 *   2) Executing a process called "RSSI Voting". When a client outputs a beacon as 
 *        part of TPS, the MCU will download all the RSSI and Phase data from the 
 *        UVPs. Once it downloads the RSSI data, we do RSSI Voting. For RSSI 
 *        Voting, we will count the number of 'violating' vertical and hortizontal
 *        AMU's. If the number of 'violating' vertical AMU's becomes the majority of vertical 
 *        AMU's OR if the number of 'violating' horizontal AMU's becomes the majority of 
 *        horizontal AMU's transmission is terminated for tile.
 *        This entire operation occurs in software on the MCU.
 *
 * -------------------------------------------------------------------------------
 *  How to Complete the Operation
 * -------------------------------------------------------------------------------
 * In order to do RSSI Voting properly, the following steps must be taken.
 *
 * At manufacturing:
 *   1) At manufacture time, take an RSSI treshold baseline. Pair the transmitter 
 *        with a client, capture a beacon, then save all the RSSI values to an 
 *        array. Then subtract a constant value (in dB) from the RSSI, this will 
 *        become the RSSI threshold baseline. This baseline will need to be 
 *        saved and reloaded at boot-up.
 *
 * At boot-up:
 *   2) For each of these baseline values, write the value to the RSSI_LOWTHRESH 
 *        register on the corresponding AMUs for the UVPs.
 *
 * During operation:
 *   3) After the phase update pulse that occurs during the Rx phase of TPS, we 
 *        download all the RSSI data from the UVPs.
 *
 *   4) Compare all the measured RSSI with the threshold baseline and count the
 *        the number of AMUs that measured a Violating RSSI for horizontal
 *        and vertical antennas. If the eighter a majority of horizonal or vertical 
 *        AMU's violate the vote, we disable transmission entirely.
 *
 * @endverbatim 
*/

/*
* Includes
*/
#include "FreeRTOS.h"
#include "stdlib.h"
#include "gather_and_vote_task.h" 
#include "stm32h7xx_hal.h"
#include "cmsis_os.h"
#include "string.h"
#include "stdbool.h"
#include "stdint.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "orion_config.h"
#include "error.h"
#include "main.h"
#include "cli_task.h"
#include "projdefs.h"
#include "task.h"
#include "semphr.h"
#include "debug_off.h"
#include "tps_driver.h"
#include "nvm_driver.h"
#include "ctrl_interface.h"
#include "rpi_msg_interface.h"
#include "client_interface.h"
#include "proxy_msg_interface.h"
#include "orion_utilities.h"
#include "tps_scheduler.h"
#if defined(USE_DEMO_BUILD)
    #include "Demo/demo_client_interface.h"
#elif defined(USE_FOREVER_TRACKER_BUILD)
#pragma diag_suppress=Pe177 // variable declared but never referenced
#pragma diag_suppress=Pe550 // variable set but never used
#endif

/*
* Defines
*/
#define RSSI_EXTRACT_AMU0(lRssiData)    (lRssiData) //((lRssiData >> 6 )  & 0x1fff)  ///< Extracts a RSSI for AMU0 from a 64-bits worth of traffic from the UVP
#define RSSI_EXTRACT_AMU1(lRssiData)    (lRssiData) //((lRssiData >> 19)  & 0x1fff)  ///< Extracts a RSSI for AMU1 from a 64-bits worth of traffic from the UVP
#define RSSI_EXTRACT_AMU2(lRssiData)    (lRssiData) //((lRssiData >> 32)  & 0x1fff)  ///< Extracts a RSSI for AMU2 from a 64-bits worth of traffic from the UVP
#define RSSI_EXTRACT_AMU3(lRssiData)    (lRssiData) //((lRssiData >> 45)  & 0x1fff)  ///< Extracts a RSSI for AMU3 from a 64-bits worth of traffic from the UVP

#define DEFAULT_NUM_VIOLATING_ANTENNAS      32      ///< The default value for setting the number of Violating antennas for RSSI voting. 
#define APP_CMD_HARVEST                     58      ///< The app command number for harvesting
#define MAX_TIME_FOR_HARVEST                255     ///< Indicates maximum time for harvest (used for both hours and minutes)
#define APP_CMD_HARVEST_SIZE                3       ///< The size of the three byte app command for harvesting
#define APP_CMD_HARVEST_WAIT_MS             1000    ///< Time in milliseconds to wait for the harvest command to work.    
#define STATIC_CHARGE_ITERATION_DELAY       100     ///< Delay for each iteration of the static charge loop in milliseconds
#define STATIC_CHARGE_TIME_DISPLAY_PERIOD   10      ///< Time remaining display period in seconds

#define HARVEST_BUFF_SIZE  sizeof(ExtdAddr_t) + sizeof(uint8_t) + APP_CMD_HARVEST_SIZE

/*
* Task Architecture Variables
*/
SemaphoreHandle_t               gGatherAndVoteSem        = NULL;  ///< The Gather and Vote Task semaphore 
static StaticSemaphore_t        gGatherAndVoteSemBuff;            ///< The buffer holding the information for the #gGatherAndVoteTaskHandle semaphore
static volatile bool            gRssiVoteEnabled         = false; ///< Track whether rssi voting is enabled
static volatile bool            gTpsStarted              = false; ///< Indicates that a TPS cycle has just started.
static volatile bool            gTpsTxFrozen             = false; ///< Indicates that the TPS cycle is frozen
static volatile bool            gPrintRssiReport         = false; ///< Indicates an RSSI report has been requested
static volatile bool            gPrintAmbDisabled        = false; ///< Indicates that the gather and vote task should display disabled AMB's

#if defined(USE_DEMO_BUILD)
static MsgStaticCharge_t        gBeatNumFreeze           = {0};   ///< Beat number to freeze at.
static AmbMask_t                gAmbFailed               = 0x0;   ///< A mask indicating which AMB's are failing the vote
#endif
static MsgSetRssiThreshold_t    gSetThreshParams         = {0};   ///< Arguments to set the rssi threshold
static SemaphoreHandle_t        gSetThreshMutex;                  ///< Mutex used to guard set rssi thresh parameter accross tasks
static StaticSemaphore_t        gSetThreshMutexBuff;              ///< The buffer holding the information for the #gSetThreshMutex mutex
static TaskHandle_t             gGatherAndVoteTaskHandle = NULL;  ///< The task handle for the gather and vote task.
//#define MONITOR_MISSED_BEACONS
#ifdef MONITOR_MISSED_BEACONS
volatile uint16_t countLow;         ///< Counts the number of missed beacons
volatile uint16_t totalCount;       ///< Counts the total number of beacon slots
#endif

/*
 * Private (static) Variables
 */
static uint16_t gPhaseData             [MAX_NUM_AMB][UVPS_PER_AMB][MAX_NUM_AMU];  ///< A multi-dimensional array that holds the phase data for each beacon
static uint16_t gRssiData              [MAX_NUM_AMB][UVPS_PER_AMB][MAX_NUM_AMU];  ///< A multi-dimensional array that holds the RSSI data for each beacon
static uint32_t gUvpReadData           [TOTAL_UVPS];                              ///< Reads spi results, made global to take pressure off the stack.    
                             
static uint16_t gRssiThreshAcc     [MAX_NUM_AMB][UVPS_PER_AMB][MAX_NUM_AMU];    ///< This serves as the RSSI accumulators used for averaging during acquisition
static uint16_t (*gRssiLowThresh)  [MAX_NUM_AMB][UVPS_PER_AMB];                 ///< Pointer rssi low threhold values 
static AmuNum_t gAmuEnMask[TOTAL_UVPS];                                         ///< The AMU enable mask for each UVP
/*
* Private (static) functions
*/
#if defined(USE_DEMO_BUILD)
static CotaError_t gatherAllRssiData(void);
static CotaError_t gatherAllPhaseData(void);
static CotaError_t completeRssiVoting(void);
static bool AcquireRssiForThresholds(void);
static float rssiTodBm(uint16_t rssiVal);
static uint16_t dBmToRssi(float dBm);

/**
 *  @brief This will cause TPS to stop in transmit at the beat specified
 *  @param msg Structure of type #MsgStaticCharge_t contain the beat to freeze TPS at and the time to free
 */
void FreezeTpsTransmit(MsgStaticCharge_t* msg)
{
    //It's important to update the beat number and harvest times before setting
    //gTpsTxFrozen, otherwise the gather and vote task may not have the correct values
    //in gBeatNumFreeze when it starts.
    gBeatNumFreeze = *msg; 
    gTpsTxFrozen = true;
}

/**
* @brief  This function gathers all Phase data from the UVPs and stores it in a 
*           buffer.
* @return A CotaError_t that denotes if the function call was successful.
*/
static CotaError_t gatherAllPhaseData(void)
{
    CotaError_t retSuccess = COTA_ERROR_NONE;
    uint8_t amb, uvp, amu;
    
   /* Important note:
    *    All of the PCB/code names and indices are 0-indexed. SiWare's asic
    *    starts at 1. Therefore, SiWare registers such as OUTPHASE_CLNT1_AMU1 is 
    *    for AMU0 as so on. 
    */    
    UvpReg_t uvpReg[MAX_NUM_AMU] = {OUTPHASE_CLNT1_AMU1, OUTPHASE_CLNT1_AMU2, OUTPHASE_CLNT1_AMU3, OUTPHASE_CLNT1_AMU4};

    amb = 0;
    uvp = 0;
    amu = 0;
  
    /*
    * Read AMU Data for each AMB/UVP
    */
    for (amu = 0; (amu < MAX_NUM_AMU) && (retSuccess == COTA_ERROR_NONE); amu++)
    {
        //DBG5_SET();
        
        //This function reads in parallel from the amb's
        retSuccess = ReadArrayUvps32(gUvpReadData, TOTAL_UVPS, uvpReg[amu].pageIndex,  uvpReg[amu].colAddr,  AmbGetValidAmbs(), ALL_UVP_MASK);
        
        //DBG5_RESET(); 
        
        // gUvpReadData now contains a 32 bit block of memory from each UVP.
        // We must now extract the register values from these blocks for each UVP.
        for (amb = 0; amb < MAX_NUM_AMB; amb++)
        {
            if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
            {
                for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
                {
                    gPhaseData[amb][uvp][amu] = (gUvpReadData[UVP_INDEX(amb,uvp)] >>  uvpReg[amu].offset) & GEN_SOLID_MASK( uvpReg[amu].size);
                }
            }
        }
    }

    return retSuccess;
}

/**
* @brief  This function gathers all RSSI data from the UVPs and stores it in a 
*           buffer.
*
* @return A CotaError_t that denotes if the function call was successful.
*/
static CotaError_t gatherAllRssiData(void)
{
    return GetRssiValues(gRssiData, AmbGetValidAmbs(), ALL_UVP_MASK);
}

/**
* @brief This function completes RSSI Voting, which will terminate TPS during a 
*        TPS cycle if RSSI values do not check out.
*        The standard for rssi voting is that if half of the vertical or 
*        or horizontal fail to exceeed the threshold then the vote has failed.
*/
static CotaError_t completeRssiVoting(void)
{
    uint16_t totalVerts=0, totalHorz=0;
    uint16_t failedVerts=0, failedHorz=0;
    int32_t amb, uvp, amu;
    CotaError_t err = COTA_ERROR_NONE;
    AmuMask_t enMaskNew[TOTAL_UVPS] = {0};
    AmbMask_t ambFailed = 0x0;
    
    NvmLock();  
    
    //Begin voting by counting the number of AMUs whose RSSI values are below the thresholds for those AMUs and counting the total number of AMUs. 
    //The AMUs connected to horizontally-polarized and vertically-polarized antennas are counted separately.  
    for (amb = 0; amb < MAX_NUM_AMB; amb++)
    {
        if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
        {       
            totalVerts = 0;
            totalHorz = 0;
            failedVerts = 0;
            failedHorz = 0;

            for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
            {
                for (amu = 0; amu < MAX_NUM_AMU; amu++)
                {
                    if (gRssiData[amb][uvp][amu] >= (*gRssiLowThresh)  [amb][uvp])
                    {
                        enMaskNew[UVP_INDEX(amb,uvp)] |= NUM_TO_MASK(amu);
                        
                        //Add a pass vote to the tally
                        if ((amu == 0) || (amu == 3))
                        {
                            totalVerts++;
                        }
                        else
                        {
                            totalHorz++;
                        }
                    }
                    else
                    {
                        //Add a fail vote to the tally
                        if ((amu == 0) || (amu == 2))
                        {
                            failedVerts++;
                            totalVerts++;
                        }
                        else
                        {
                            failedHorz++;                              
                            totalHorz++;
                        }                  
                    }
                }
            }
            //Determine if horizontal antennas have failed to vote
            if ((totalHorz > 0) && (failedHorz > totalHorz/2))
            {
               ambFailed |= NUM_TO_MASK(amb);
            }
            
            //Determine if vertical antennas have failed to vote
            if ((totalVerts > 0) && (failedVerts > totalVerts/2))
            {
               ambFailed |= NUM_TO_MASK(amb);
            }
            
            if (ambFailed & NUM_TO_MASK(amb))
            {
                for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
                {
                    for (amu = 0; amu < MAX_NUM_AMU; amu++)
                    {
                        enMaskNew[UVP_INDEX(amb,uvp)]  = DISABLE_ALL_AMU_MASK;
                    }
                }  
            }
        }
    }
    
    NvmUnlock();
    if (ambFailed)
    {
        DBG5_SET();
        err = UvpDisableAmus(ambFailed);
        if (err != COTA_ERROR_NONE)
        {
            PostPrintf("Failed to disable AMB's after check\r\n");
        }
        DBG5_RESET();
    }
    
    if (gTpsStarted)
    {
        //If this is the first beat of the TPS cycle, we need to enable
        //AMB's that were previously disabled.
        DBG5_SET();
        err = (err == COTA_ERROR_NONE) ? UvpEnableAmus(~gAmbFailed) :err;  //On TPS start you have to enable some previously disabled AMB's if they pass RSSI voting.
        if (err != COTA_ERROR_NONE)
        {
            PostPrintf("Failed to enbable amus after first vote\r\n");
        }   
        DBG5_RESET();
    }
    
    //Disable any AMU's that fall below the UVP's low thresh value
    err = (err == COTA_ERROR_NONE) ? SetAmuEnableChanges(gAmuEnMask, enMaskNew, TOTAL_UVPS, ~ambFailed) : err;
    if (err != COTA_ERROR_NONE)
    {
        PostPrintf("Failed to set amus check\r\n");
    }
    
    memcpy(gAmuEnMask, enMaskNew, sizeof(gAmuEnMask));
    
    gAmbFailed = ambFailed;
    
    return err;
}

/**
 *  @brief Will acuire rssi value to create rssi threshold to vote against
 *         During normal voting, this function exits with false
 *         to inform the caller that rssi thresholds are not being created.
 *         However, if gSetThreshParams.nSamples is not zero,
 *         that means someone has issued set_rssi_thresh command.
 *         The function will engage in the threshold creating process.
 *         gSetThreshParams.nSamples is decremented each time the RSSI values
 *         are added to the accumulators.  When gSetThreshParams.nSamples 
 *         becomes zero, RSSI values are averaged, and the RSSI_LOWTHRESH
 *         is set.  
 *  @return true if Rssi acquisition happens, false otherwise.
 */
static bool AcquireRssiForThresholds(void)
{
    static uint32_t accCount = 0;
    bool ret = false;
    int32_t amb, uvp, amu;
    CotaError_t err = COTA_ERROR_NONE;
       
    xSemaphoreTake(gSetThreshMutex, portMAX_DELAY);
    
    NvmLock();
    //DBG2_SET(); 
    
    if (gSetThreshParams.nSamples > 0)
    {
        ret = true;
    
        if (accCount == 0) 
        {
            //Just starting threshold accquisition, 0 out accumulators
            memset(gRssiThreshAcc, 0, sizeof(gRssiThreshAcc));
        }
        
        //DBG2_RESET();   
        //Add latest rssi value to accummulators
        for (amb = 0; amb < MAX_NUM_AMB; amb++)
        {
            if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
            {
                for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
                {
                    for (amu = 0; amu < MAX_NUM_AMU; amu++)
                    {
                        gRssiThreshAcc[amb][uvp][amu] += gRssiData[amb][uvp][amu];
                    }
                }
            }
        }
        
        //DBG2_SET();
        accCount++;
        gSetThreshParams.nSamples--;
        
        if (gSetThreshParams.nSamples == 0)
        {
            //We have finsihed acquisition, time set the rssi threshold
          
            for (amb = 0; (amb < MAX_NUM_AMB) && (err == COTA_ERROR_NONE); amb++)
            {
                if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
                {
                    for (uvp = 0; (uvp < UVPS_PER_AMB) && (err == COTA_ERROR_NONE); uvp++)
                    {
                        float uvpLowThreshAvg;
                        uint16_t minVal;
                        uint8_t outlier;
                        //loop over the amu, divide accumulators by the accumulated count to averge
                        //Also average rssi over the amu's to set RSSI_LOWTHRESH
                        for (amu = 0; amu < MAX_NUM_AMU; amu++)
                        {
                            gRssiThreshAcc[amb][uvp][amu] /= accCount;
                        }
                        
                        //We need get the value for the register RSSI_LOWTHRESH
                        //So average over the three highest values and exclude
                        //the lowest one in case of problems.
                        
                        //Here we find the lowest rssi value.
                        outlier = 0;
                        minVal = GEN_SOLID_MASK(RSSi_AMU1.size);
                        for (amu = 0; amu < MAX_NUM_AMU; amu++)
                        {
                            if (gRssiThreshAcc[amb][uvp][amu] < minVal)
                            {
                                minVal = gRssiThreshAcc[amb][uvp][amu];
                                outlier = amu;
                            }
                        }                  
                        
                        //Here we average over the remaining three value in dBm   

                        uvpLowThreshAvg = 0.0f;
                        for (amu = 0; amu < MAX_NUM_AMU; amu++)
                        {
                            if (amu != outlier)
                            {
                                uvpLowThreshAvg +=  rssiTodBm(gRssiThreshAcc[amb][uvp][amu]);
                            }
                        }
                        uvpLowThreshAvg /= (MAX_NUM_AMU - 1);
                            
                        //Now we subtract from the low threshold average, and store it to NVM cache and UVP's    
                        uvpLowThreshAvg -= gSetThreshParams.nDevIndBm;
                       
                        (*gRssiLowThresh)[amb][uvp] = dBmToRssi(uvpLowThreshAvg);

                        err = UvpRegisterWrite(RSSI_LOWTHRESH, (*gRssiLowThresh)[amb][uvp], amb, uvp);
                        
                        if (err != COTA_ERROR_NONE)
                        {
                            POST_COTA_ERROR(err);
                            PostPrintf("Error writing RSSI_LOWTHRESH\r\n");
                        }
                    }
                }
            }
            err = (err == COTA_ERROR_NONE) ? NvmSaveRssiThreshToEeprom() : err;
            if (err != COTA_ERROR_NONE)
            {
                POST_COTA_ERROR(err);
                PostPrintf("Rssi threshhold creation failed\r\n");
                gRssiVoteEnabled = false;  //disable rssi voting if the threshold is bad.
            }
            else
            {
                PostPrintf("RSSI threshold created\r\n");
            }
            accCount = 0;
        }
    }
      
    xSemaphoreGive(gSetThreshMutex);
  
    //DBG2_RESET();
    NvmUnlock();
    return ret;
}

#endif //#if defined(USE_DEMO_BUILD)

/**
 *  @brief Determines if TPS is frozen
 *  @return true if frozen, false if not.
 */
bool IsTpsFrozen(void)
{
    return gTpsTxFrozen;
}

/**
 *  @brief Causes an RSSI report to be printed
 */
void PrintRssiReport(void)
{
    gPrintRssiReport = true;
}

/**
 *  @brief Causes disabled AMB's to be displayed
 */
void PrintAmbDisabled(void)
{
    gPrintAmbDisabled = true;
}

/**
 *  @brief Restarts TPS if frozen
 */
void FreeTps(void)
{
    //We need to send a new TPS message to the receiver, so increment the TPS seq num to force that.
    IncrementTpsSeqNum();  
    
    if (gTpsTxFrozen)
    {
        PostPrintf("Ending static charge\r\n");                   
    }
    
    gTpsTxFrozen = false;
    AmbEnableTx(false);
}

/**
 * @brief Enables or disables the UVP mininum RSSI threshholds
 *
 * @param enable   true to enable rssi voting, false to disable
 */
CotaError_t SetUvpThreshEn(bool enable)
{
    CotaError_t ret = COTA_ERROR_NONE;
    CotaError_t ret2;
    AmbNum_t amb;
    UvpNum_t uvp;   
    
    if (enable)
    {   
        if (gRssiLowThresh)
        {
            for (amb = 0; (amb < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); amb++)
            {
                if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
                {
                    for (uvp = 0; (uvp < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvp++)
                    {
                        ret = UvpRegisterWrite(RSSI_LOWTHRESH, (*gRssiLowThresh)[amb][uvp], amb, uvp);
                    }
                }
            }
        }
        else
        {
            ret = COTA_ERROR_BAD_PARAMETER;
        }
        
        POST_COTA_ERROR(ret);
    
        if (ret != COTA_ERROR_NONE)
        {
            PostPrintf("Failed to enable rssi voting\r\n");
        }
    }
    else
    {
        // Make sure all amus are enabled
        ret = UvpEnableAmus(AmbGetValidAmbs());   
         
        POST_COTA_ERROR(ret);
        
        //Restore rssi thresholds on the uvp
        ret2 = WriteManyUvps(RSSI_LOWTHRESH, 0, AmbGetValidAmbs(), ALL_UVP_MASK);
             
        POST_COTA_ERROR(ret2);
        
        if ((ret != COTA_ERROR_NONE) || (ret2 != COTA_ERROR_NONE))
        {
            PostPrintf("Failed to disable rssi voting\r\n");
            
            ret = (ret == COTA_ERROR_NONE) ? ret2 : ret;
        }
    }
    
    return ret;
}

/**
 * @brief Enables or disables RSSI voting
 *
 * @param enable   true to enable rssi voting, false to disable
 */
void SetRssiVoteEn(bool enable)
{
    CotaError_t ret;
    
    ret = SetUvpThreshEn(enable);

    if (ret == COTA_ERROR_NONE)
    {
        gRssiVoteEnabled = enable;      
    }
    else
    {
        gRssiVoteEnabled = false;  //Never enable RSSI voting if RSSI_LOWTHRESH registers are not set properly
    }
}

/**
 * @brief Starts acquitistion of rssi values to set rssi threshhold
 *
 * @param rssiThresh   pointer to type rssiThreshParams_t containing the rssi threshold parameters.
 */
void SetRssiThresh(MsgSetRssiThreshold_t* rssiThresh)
{
    xSemaphoreTake(gSetThreshMutex, portMAX_DELAY);
    
    gSetThreshParams = *rssiThresh;

    gRssiVoteEnabled = true;
    
    memset(gAmuEnMask, ALL_AMU_MASK , sizeof(gAmuEnMask));
    
    xSemaphoreGive(gSetThreshMutex);
}

#if defined(USE_DEMO_BUILD)
/**
 * @brief Displays which AMB's are enabled or disabled and sends info to RPI
 *
 * @param ambFailed A mask indicating which AMB's are disabled because they failed
 * @param resp Pointer to a message structure so we can work without expanding the stack
 */
static void PrintAmbState(AmbMask_t ambFailed, CtrlResp_t* resp)
{
    AmbNum_t amb;
    uint16_t len;
    
    for (amb = 0; amb < MAX_NUM_AMB; amb++)
    {
        if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
        {
            PostPrintf("AMB %d: %s\r\n", amb, (BIT_IN_MASK(amb, ambFailed) ? "Disabled" : "Enabled"));
        }
    }
    
    resp->id = CTRL_AMB_FILTER_DISABLED;
    resp->err = COTA_ERROR_NONE;
    len = sizeof(CTRL_AMB_FILTER_DISABLED) + 
      sizeof(resp->err) +
      sizeof(resp->ambDisabled);
    resp->ambDisabled.ambDisabled = ambFailed;
    RpiSendMessage((uint8_t*) resp, len);
    
    gPrintAmbDisabled = false;
}

/**
* @brief The GatherAndVote task. This task will download all RSSI and Phase 
*          data from the UVPs, and perform RSSI voting to disable transmission.
*
*
*         This can be confusing, so I will detail my reasoning here.
*         We will disable AMB's and individual AMU's
*         if their RSSI values fall below a threshold; however, you have 
*         to enable AMU's to read RSSI values.  So how do read RSSI values without
*         transmitting?  Here how's.
*
*         This is a typical RSSI vote cycle (not creating thresholds)
*         1) At TPS start, the control task will call  ResetRssiVote, which
*            sets a flag so the GatherAndVoteTask can tell it is the start
*            of a TPS cycle.  All AMU's are also enabled so that their RSSI can
*            can be sampled; the cached value of the AMU states in RAM are not changed
*         2) After the first phase update during the beacon phase of the first beat in
*            the TPS cycle, control is turned over to the GatherAndVoteTask using the 
*            gGatherAndVoteSem semaphore.
*         3) GatherAndVoteTask waits about 90us to ensure all RSSI reads are valid. 
*         4) Since this is the first beat of the TPS cycle, all AMU's that were disabled
*            in the previous TPS cycle (this is recorded in the cached values in RAM), 
*            are disabled once again to prevent them from transmitting when the TX line comes up
*         5) RSSI voting takes place.  AMU's will be enabled individually if their RSSI
*            is above the RSSI_LOWTHRESH value, and AMB's are once again assessed if
*            if they should be on or off.  Any AMB that was disabled in step 4 will be
*            enabled if the RSSI vote passes.
*         6) During the next beat, the GatherAndVoteTask will wait and then go straight to RSSI voting.  Any
*            AMU or AMB disabled in the subsequent beats stays disabled until the beginning of the
*            next TPS cycle.
 *
 * @param  pvParameters  Unused parameter
*/
void GatherAndVoteTask(void *pvParameters)
{ 
    uint16_t* ptr;
    CotaError_t err;
    AmbNum_t amb;
    UvpNum_t uvp;
    AmuNum_t amu;   
    uint8_t respBuf[sizeof(CtrlMsgId_t) + sizeof(CotaError_t) + sizeof(RespRssiFilterReportLine_t)];  ///< This buffer is set the maximum size of any RPI resposne from this task.
    CtrlResp_t* resp  = (CtrlResp_t*)respBuf;   ///Set the response stucture to point at gRespBuf to reduce stack usage.
    TickType_t startStaticChargeTicks;
    TickType_t staticChargeTicks;
    uint32_t timeRemainCount = SEC_TO_MS(STATIC_CHARGE_TIME_DISPLAY_PERIOD)/STATIC_CHARGE_ITERATION_DELAY;
    uint32_t count = 0;
#if  defined(TEST_GATHER_ALL_RSSI_DATA) || defined(TEST_SET_AMU_ENABLE)

    AmuMask_t oldAmuMask[TOTAL_UVPS];
#endif
    while (NvmGetRssiThreshFromEeprom() != COTA_ERROR_NONE);
    
    ptr = NvmGetRssiLowThreshArray();
    memcpy(&gRssiLowThresh, &ptr, sizeof(uint32_t));

    for (;;)
    {
        xSemaphoreTake(gGatherAndVoteSem, portMAX_DELAY);
        
        //We are buying some time with rssi reads.
        //The RX phase update has just been started, and we need about 50 microseconds until RSSI values are ready
        //We can't use the TX phase update to start this thread because we need able to turn off
        //disabled AMB's before the TX pin has risen.
        ReadArrayUvps32(gUvpReadData, TOTAL_UVPS, EN_AMU1.pageIndex,  EN_AMU1.colAddr,  AmbGetValidAmbs(), 0x1);
        
        err = COTA_ERROR_NONE;
        
        if (gTpsStarted)
        {
            //We have to disable previously disabed AMB's and AMU's at TPS start
            //because we need to measure RSSI on all the AMB's
            //but we can't tranmit on previosly disabled AMB's
            //until they are confirmed to pass RSSI voting.
            DBG5_SET();   
            err = UvpDisableAmus(gAmbFailed);
            if (err != COTA_ERROR_NONE)
            {
              PostPrintf("Failed to disable Amus at tps start\r\n");
            }
            DBG5_RESET();
            DBG5_SET();           
            err = (err == COTA_ERROR_NONE) ? SetAmuEnableChanges(NULL, gAmuEnMask, TOTAL_UVPS, ~gAmbFailed) : err;          
            if (err != COTA_ERROR_NONE)
            {
              PostPrintf("Failed to disable ind Amus at tps start\r\n");
            }
            DBG5_RESET();
        }
            
#ifdef TEST_SET_AMU_ENABLE
        //This a test created to test SetAmuEnableChanges, which employs some novel techniques.
        for (amb = 0; amb < TOTAL_UVPS; amb++)
        {
            oldAmuMask[amb] = 0x1;
        }
        SetAmuEnableChanges(NULL, oldAmuMask, TOTAL_UVPS, AmbGetValidAmbs());
        
        for (amb = 0; amb < TOTAL_UVPS; amb++)
        {
            gAmuEnMask[amb] = amb & 0xf;
        }
        SetAmuEnableChanges(oldAmuMask, gAmuEnMask, TOTAL_UVPS, AmbGetValidAmbs());
      
        for (amb = 0; amb < MAX_NUM_AMB; amb++)
        {
            for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
            {
                uint32_t val1, val2, val3, val4;
                POST_COTA_ERROR(UvpRegisterRead(EN_AMU1, &val1, amb, uvp));
                POST_COTA_ERROR(UvpRegisterRead(EN_AMU2, &val2, amb, uvp));
                POST_COTA_ERROR(UvpRegisterRead(EN_AMU3, &val3, amb, uvp));
                POST_COTA_ERROR(UvpRegisterRead(EN_AMU4, &val4, amb, uvp));
                PostPrintf("amb=%d uvp=%d 0x%x 0x%x\r\n", amb, uvp, gAmuEnMask[UVP_INDEX(amb,uvp)], 
                           (val4 << 3) | (val3 << 2) | (val2 << 1) | val1);
            }
            
        }
#endif
        
        DBG10_SET();      
        err = (err == COTA_ERROR_NONE) ? gatherAllRssiData() :err;
        if (err != COTA_ERROR_NONE)
        {
          PostPrintf("Failed to gather rssi\r\n");
        }
        DBG10_RESET();
     
#if TEST_GATHER_ALL_RSSI_DATA
        //Because gatherAllRssiData uses a special technigue of reading data,
        //we keep a test handy to make sure it's working.
        for (amb = 0; amb < MAX_NUM_AMB; amb++)
        {
            for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
            {
                uint32_t val;
                POST_COTA_ERROR(UvpRegisterRead(RSSi_AMU1, &val, amb, uvp));
                PostPrintf("rssi amb = %d uvp %d amu=0 0x%x 0x%x\r\n",
                           amb, uvp, gRssiData[amb][uvp][0],val);
                POST_COTA_ERROR(UvpRegisterRead(RSSi_AMU2, &val, amb, uvp));
                PostPrintf("rssi amb = %d uvp %d amu=0 0x%x 0x%x\r\n",
                           amb, uvp, gRssiData[amb][uvp][1],val);
                POST_COTA_ERROR(UvpRegisterRead(RSSi_AMU3, &val, amb, uvp));
                PostPrintf("rssi amb = %d uvp %d amu=0 0x%x 0x%x\r\n",
                           amb, uvp, gRssiData[amb][uvp][2],val);                
                POST_COTA_ERROR(UvpRegisterRead(RSSi_AMU4, &val, amb, uvp));
                PostPrintf("rssi amb = %d uvp %d amu=0 0x%x 0x%x\r\n",
                           amb, uvp, gRssiData[amb][uvp][3],val);              
              
            }
          
        }
#endif

        DBG10_SET();         

        if (err == COTA_ERROR_NONE)
        {
            //Check first to see if we are creating a threshold before
            //doing any voting.
            if (!AcquireRssiForThresholds())
            {
                err = completeRssiVoting();  //Do rssi voting.
            }
        }     
        
        if (err != COTA_ERROR_NONE)
        {
            //Something is wrong, we can't read rssi data
            gAmbFailed = AmbGetValidAmbs();
            UvpDisableAmus(gAmbFailed);
            StopTpsCycle();             //Stop the TPS cycle if there are any problems
            POST_COTA_ERROR(err);
            PostPrintf("Rssi voting failed\r\n");
        }
        
        gTpsStarted = false;
        DBG10_RESET();
        DBG10_SET();  
        err = (err == COTA_ERROR_NONE) ? gatherAllPhaseData() : err;
        DBG10_RESET();
        
        if (gPrintAmbDisabled)
        {
            PrintAmbState(gAmbFailed, resp);
        }
        
        //If a request to freeze the TPS cycle has been issued, and we are at the right beat,
        //we freeze the TPS cycle and print a report.
        if (IsTpsFrozen() && (gBeatNumFreeze.beat == (uint8_t)GetTpsBeat()) && (err == COTA_ERROR_NONE))
        {     
            UvpReg_t enAmu[MAX_NUM_AMU] = {EN_AMU1, EN_AMU2, EN_AMU3, EN_AMU4};
            UvpReg_t phaseOff[MAX_NUM_AMU] = {PHASE_OFFSET_AMU1, PHASE_OFFSET_AMU2, PHASE_OFFSET_AMU3, PHASE_OFFSET_AMU4};
            uint32_t val, minThresh;
            uint16_t len;
            ShrtAddr_t shortId;
            uint8_t harvestBuff[HARVEST_BUFF_SIZE];
            PrxHostClientCmdMsg_t* prxHarvest = (PrxHostClientCmdMsg_t*)harvestBuff;
              
            StopTpsCycle(); //Stop TPS
            PostPrintf("TPS stopped\r\n");              
            vTaskDelay( pdMS_TO_TICKS(10)); //A little time needs to transpire before we can turn TX on
            
            //If we are charging a client, we need to put it in harvesting mode                  
            if (CmGetChgClient(&shortId))
            {     
                PostPrintf("Wait for TPS timeout\r\n");  
                while(!IsTpsTimedOut())     //Wait for the TPS timeout to make sure the client is done with TPS
                {
                    vTaskDelay( pdMS_TO_TICKS(10));
                }
                //We cannot go through the client command queue 
                //because we are stuck in the TPS state.
                //We issue the app command directly.
                prxHarvest->extAddr = CmGetLongIdFromShortId(shortId);
                prxHarvest->payloadSize = APP_CMD_HARVEST_SIZE;
                prxHarvest->payload.bytes[0] = APP_CMD_HARVEST;
                prxHarvest->payload.bytes[1] = gBeatNumFreeze.hours;
                prxHarvest->payload.bytes[2] = gBeatNumFreeze.minutes;  
                PostPrintf("Send command to put client in harvest mode\r\n");
                PrxSendAppCommand(prxHarvest);
                vTaskDelay( pdMS_TO_TICKS(APP_CMD_HARVEST_WAIT_MS));
            }
            PostPrintf("Enable Tx\r\n");      
            AmbEnableTx(true);  //Enable TX
            PostPrintf("Frozen for beat %d\r\n", GetTpsBeat());  // Report the beat we stopped on
            startStaticChargeTicks = xTaskGetTickCount();
            staticChargeTicks = MIN_TO_SEC(HOURS_TO_MIN((uint32_t)gBeatNumFreeze.hours) + gBeatNumFreeze.minutes)*configTICK_RATE_HZ;
            count = 0;   //Tracks the number of times we through the wait loop so we know when to print time 

            while(IsTpsFrozen())  //Wait for TPS to become unfozen
            {
                if (gPrintRssiReport)
                {
                    gPrintRssiReport = false;
                    //Generate a report

                    PostPrintf("AMB UVP AMU RSSI(reg) Phase(reg) PhaseOffset(reg) RSSI_Threshold(reg) AMU_EN(reg)\r\n");

                    for (amb = 0; (amb < MAX_NUM_AMB) && (err == COTA_ERROR_NONE); amb++)
                    {
                        if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
                        {
                            for (uvp = 0; (uvp < UVPS_PER_AMB) && (err == COTA_ERROR_NONE); uvp++)
                            {  
                                err =  UvpRegisterRead(RSSI_LOWTHRESH, &minThresh, amb, uvp);
                                
                                for (amu = 0; (amu < MAX_NUM_AMU) && (err == COTA_ERROR_NONE); amu++)
                                {
                                    resp->id = CTRL_RSSI_REPORT_DATA;
                                    resp->err = COTA_ERROR_NONE;
                                    resp->rssiReportLine.ambNum = amb;
                                    resp->rssiReportLine.uvpNum = uvp;
                                    resp->rssiReportLine.amuNum = amu + 1;
                                    resp->rssiReportLine.rssiThresh = (uint16_t)minThresh;
                                    resp->rssiReportLine.phase = gPhaseData[amb][uvp][amu];
                                    resp->rssiReportLine.rssiVal = gRssiData[amb][uvp][amu];
                                    err = UvpRegisterRead(enAmu[amu], &val, amb, uvp);
                                    resp->rssiReportLine.amuEn = (AmuNum_t)val;
                                    err = (err == COTA_ERROR_NONE) ? UvpRegisterRead(phaseOff[amu], &val, amb, uvp) : err;
                                    resp->rssiReportLine.phaseOffset = (uint16_t)val; 
                                    len = sizeof(CTRL_RSSI_REPORT_DATA) + 
                                          sizeof(err) +
                                          sizeof(resp->rssiReportLine);
                                    RpiSendMessage((uint8_t*) resp, len);  //Send a report line to RPI
                                    PostPrintf(" %1d  %2d  %1d     %04d      %04d         %04d              %04d              %1d\r\n",
                                        amb, uvp, (amu + 1), resp->rssiReportLine.rssiVal, resp->rssiReportLine.phase,
                                        resp->rssiReportLine.phaseOffset, resp->rssiReportLine.rssiThresh, 
                                        resp->rssiReportLine.amuEn);
                                    vTaskDelay( pdMS_TO_TICKS(50));   //Wait a little so UART queue and RPI queue do not fill                                    
                                }
                            }
                        }
                    }
            
                    resp->id = CTRL_RSSI_FILTER_REPORT;
                    resp->err = COTA_ERROR_NONE;       
                    len = sizeof(CTRL_RSSI_FILTER_REPORT) + 
                          sizeof(resp->err);
                    RpiSendMessage((uint8_t*) resp, len);     //Send terminating message to RPI              
                    //wait for TPS to be freed
                }
                
                if (gPrintAmbDisabled)
                {
                    PrintAmbState(gAmbFailed, resp);
                }
                
                vTaskDelay( pdMS_TO_TICKS(STATIC_CHARGE_ITERATION_DELAY));
                
                if ((count % timeRemainCount) == 0)
                {                 
                    PostPrintf("%llu seconds left\r\n",
                           TICKS_TO_MS(staticChargeTicks -  TimeDiffTicks(xTaskGetTickCount(), startStaticChargeTicks))/MS_IN_SEC);
                }
                
                if (TimeDiffTicks(xTaskGetTickCount(), startStaticChargeTicks) >= staticChargeTicks)
                {
                    FreeTps();
                }
                count++;
            }
        }
        
#ifdef MONITOR_MISSED_BEACONS
        //This is a very simple test that will help us monitor missed beacons
        if (gRssiData[2][2][2] < 10)
        {
            //DBG5_SET(); 
            //DBG5_RESET(); 
            countLow++;
        }
        
        totalCount++;
        if (totalCount >= 100)
        {
            PostPrintf("rssi %d %d %d\r\n", countLow, totalCount, gRssiData[2][2][2]);  //Print a single rssi value. Choose 2,2,2 just for kicks
            countLow = 0;
            totalCount = 0;
        }
#endif       
        if (!VoteEnabled())
        {
            // Make sure all amus are enabled if rssi voting was disabled
            UvpEnableAmus(AmbGetValidAmbs());  
        }
    }
}

/**
 * @brief Convert a RSSI value to dBm
 * @param rssiVal  The RSSI value to convert
 * @return The value in dBm
 */
static float rssiTodBm(uint16_t rssiVal)
{
    return 20 * log10f((float)(rssiVal));  //convert rssi to dBm  
}

/**
 * @brief Convert a dBm value to RSSI
 * @param dBm The dBm value to convert
 * @return The RSSI value
 */
static uint16_t dBmToRssi(float dBm)
{
    return (uint16_t) powf(10, dBm/20);
}
#endif //#if defined(USE_DEMO_BUILD)

/**
* @brief  This function initializes all resources for the GatherAndVote task
* @return pdPASS or pdFAIL if the operation succeeds or fails
*/
BaseType_t GatherAndVoteInit(void)
{
    BaseType_t retSuccess;

    retSuccess = pdPASS;

    gGatherAndVoteSem = xSemaphoreCreateBinaryStatic(&gGatherAndVoteSemBuff);
    if (gGatherAndVoteSem == NULL)
    {
        retSuccess = pdFAIL;
    }

    gSetThreshMutex = xSemaphoreCreateMutexStatic(&gSetThreshMutexBuff);
    if (gSetThreshMutex == NULL)
    {
        retSuccess = pdFAIL;
    }

    return retSuccess;
}

/**
 * @brief Checks to see if rssi voting is enabled
 * @return Rssi voting is enabled if this function returns true; otherwise it returns false
 */ 
bool VoteEnabled(void)
{
   return gRssiVoteEnabled;
}

/**
 * @brief Reset a rssi voting so this task know that TPS has just started
 */ 
void ResetRssiVote(void)
{
    CotaError_t err;
    gTpsStarted = true;
    //We must ensure that all the UVP's are enabled in case RSSI voting has turned them off
    //Without this, the first beat of the TPS cycle cannot read the RSSI values.
    err = UvpEnableAmus(AmbGetValidAmbs());
    POST_COTA_ERROR(err);
    if (err != COTA_ERROR_NONE)
    {
        PostPrintf("Failed to reset rssi vote\r\n");
    }
}

#if defined(USE_DEMO_BUILD)
/**
 * @brief Creates gather and voting task
 */ 
void CreateGatherAndVoteTask(void)
{
    BaseType_t xReturned;
    
    while (GatherAndVoteInit() != pdPASS);
    /*
    * Gather and Vote Task Creation
    */
    xReturned = xTaskCreate(
                  GatherAndVoteTask,            // Function that implements the task.
                  "Gather and Vote Task",       // Text name for the task.
                  GATHER_AND_VOTE_STACK_SIZE,   // Stack size in 32-bit words.
                  NULL,                         // Parameter passed into the task.
                  GATHER_AND_VOTE_PRIORITY,     // Priority at which the task is created.
                  &gGatherAndVoteTaskHandle );  // Used to pass out the created task's handle.

    while (xReturned != pdPASS);
    
    vTaskSuspend(gGatherAndVoteTaskHandle);  //Suspend this task so that the control task can start it later.
}

/**
 * @brief Starts gather and voting task (called in the control task)
 */
void StartGatherAndVoteTask(void)
{
    vTaskResume(gGatherAndVoteTaskHandle);
}
#endif
