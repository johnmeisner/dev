/**
 * @file       client_avl_tree.h
 *
 * @brief      This is the header file for the AVL BST for organizing clients.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#ifndef _CLIENT_AVL_TREE_H_
#define _CLIENT_AVL_TREE_H_

/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////
#include <stdint.h>
#include "error.h"
#include "client.h"


/////////////////////////////////////////////////////////////////////////////////////////////
// Defines 
/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////////////////////

/*
   Forward declaration for ClientNode_t.
   Allows for the struct to hold ClientNode_t pointers
*/
#pragma diag_suppress=Pe301
typedef struct _ClientNode_t  ClientNode_t;

typedef struct _ClientNode_t
{
    Client_t client;
    ClientNode_t *left;
    ClientNode_t *right;
} ClientNode_t;

typedef struct _ClientTree_t
{
    ClientNode_t  nodeArray[MAX_NUM_CLIENTS];  ///< The array that contains all static nodes
    uint16_t      size;                        ///< The size of nodeArray in the client tree
    uint16_t      lastClient;                  ///< Counter that indexes the unused clients in nodeArray
    ClientNode_t *head;                        ///< The head nodes for each tree
} ClientTree_t;

CotaError_t InsertNode(Client_t key, ClientNode_t **node, ClientTree_t *tree);
CotaError_t DeleteNode(ExtdAddr_t key, ClientNode_t **node, Client_t *oclient);
bool        SearchTreeShortId(ShrtAddr_t key, ClientNode_t *node, Client_t *oclient);
bool        SearchTree(ExtdAddr_t key, ClientNode_t *node, Client_t *oclient);
void        WalkTree(ClientNode_t *node, void (*func)(Client_t* client));
bool        WalkToFindState(ClientState_t key, ClientNode_t *node, Client_t *oclient);
void        DestroyTree(ClientTree_t *tree);

#endif // _CLIENT_AVL_TREE_H_
