/**
 * @file       system_commands.c
 *
 * @brief      Contains all the CLI commands for the system. Later, when the RPi is online,
 *             this module will need to be adapted for an abstracted source. 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

// system_commands.c is the modern Orion style commands, and has a different command list

#ifndef VENUS_BW_COMPATIBILITY
/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////
#include "cli_task.h"
#include "main.h"
#include "string.h"
#include "cmsis_os.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stm32h7xx_hal.h"
#include "uart_driver.h"
#include "amb_control.h"
#include "system_commands.h"
#include "CotaCommonTypes.h"
#include "client.h"
#include "ctrl_interface.h"
#include "proxy.h"
#include "sysclk_driver.h"
#include "client_interface.h"
#include "gather_and_vote_task.h"
#include "process_uvp_commands.h"
#include "fan_driver.h"
#include "proxy_fw_update_driver.h"
   
/////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////
#define MAX_RSSI_THRESH_SAMPLES 255  ///< Maximum number of RSSI thresh samples
#define VARIABLE_PARAM_COUNT     -1   ///< Indicates that a command takes variable arguments

/////////////////////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////
// Private Function Declarations
/////////////////////////////////////////////////////////////////////////////////////////////

/*
  Function Callbacks
 */
static BaseType_t SetAmbOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetAmbOnCmdCalllback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetUvpOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetUvpOnCmdCalllback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetPuOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetPuOnCmdCalllback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetPuAllCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetPuAllCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SelectUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSelectedUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetPdDoneCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetSpiEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSpiEnCmdCalllback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t UvpInitCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClkInitCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t TxFreqCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t CalibrateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SendDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RegisterClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RemoveClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ProxyCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ProxyCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RcvrListCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetValidAmbMaskCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetValidAmbMaskCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetCommChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetCommChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RebootCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ResetProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RunCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t TxIdCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t PauseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StartStaticTuneCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SampleBeaconCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StaticChargeCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetSysClkCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSysClkCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StartChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StopChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ResetNvmCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetRssiThreshCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetRssiFilterEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetRssiFilterEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RssiFilterResetCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AppCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AppCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t UvpWriteCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t UvpReadCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t UvpLockCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetFansFullCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetFansFullCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetQueryTypeCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RcvrDetailCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ChargeVirtualCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetNvmConfigCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetNvmConfigCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t PrintFilterReportCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSystemTempCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ResetArrayCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t UvpMemDumpCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SysClkDumpCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetVersionCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSystemStateCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AmbFilterDisabledCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t BootloaderCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AmbFilterDisabledCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSystemStateCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClkI2CDisable(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClkRead(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClkWrite(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t CmdProxyInfo(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);

/*
  Helper functions
 */
static bool isArgAll(const char* pArg, BaseType_t argLen);
static bool isArgOff(const char* pArg, BaseType_t argLen);
static bool isArgOn(const char* pArg, BaseType_t argLen);
static uint8_t  getArgOnOff(const char* pArg, BaseType_t argLen);
static bool getNumArg(const char* pArg, BaseType_t argLen, int32_t* num);
static bool parseLongId(const char* pArg, ExtdAddr_t *longId);
static bool getLongNumArg(const char* pArg, BaseType_t argLen, int64_t* num);

static const CLI_Command_Definition_t gCliCmds[] = 
{
    { "set_amb_en",         "set_amb_en           <AMB_Number> <On 1, Off 0>  Turns on or off AMBs for the specified AMB Mask.\r\n",  SetAmbOnCmdCallback, 2},
    { "get_amb_en",         "get_amb_en           Retrieves a mask showing which AMB's are on.\r\n", GetAmbOnCmdCalllback, 0},
    { "set_uvp_en",         "set_uvp_en           <AMB_Mask>< On 1, Off 0>  Turns UVP's on or off for a specified AMB mask.\r\n",  SetUvpOnCmdCallback, 2},
    { "get_uvp_en",         "get_uvp_en           Retrieves a mask showing which AMBs' UVP's are on.\r\n", GetUvpOnCmdCalllback, 0},
    { "select_uvp",         "select_uvp           <UVP Number 0-15 | all> Selects UVP's to interact with.\r\n",  SelectUvpCmdCallback, 1},
    { "get_selected_uvp",   "get_selected_uvp     Retrieves the selected UVP number.\r\n",  GetSelectedUvpCmdCallback, 0},
    { "set_pu_en",          "set_pu_en            <AMB_Number> <on 1, off 0> Asserts or deasserts the specified AMB's phase update pin.\r\n",  SetPuOnCmdCallback, 2},
    { "get_pu_en",          "get_pu_en            Retrieves a mask showing which AMB's PU lines are asserted.\r\n", GetPuOnCmdCalllback, 0},
    { "get_pd_done",        "get_pd_done          Retrieves the state of the phase detect pins for every AMB.\r\n",  GetPdDoneCmdCallback, 0},
    { "set_tx_on",          "set_tx_on            <on 1, off 0> Asserts or deasserts the TX pin.\r\n",  SetTxCmdCallback, 1},
    { "get_tx_on",          "get_tx_on            Retrieves the TX pin state.\r\n",  GetTxCmdCallback, 0},
    { "set_rx_on",          "set_rx_on            <on 1, off 0> Asserts or deasserts the RX pin.\r\n",  SetRxCmdCallback, 1},
    { "get_rx_on",          "get_rx_on            Retrieves the RX pin state.\r\n",  GetRxCmdCallback, 0},
    { "set_pu_all",         "set_pu_all           <on 1, off 0> Sets the PU_ALL pin state by input parameter.\r\n",  SetPuAllCmdCallback, 1},    
    { "get_pu_all",         "get_pu_all           Retrieves the PU_ALL pin state.\r\n",  GetPuAllCmdCallback, 0},    
    { "set_spi_en",         "set_spi_en           <AMB_Mask> <on 1, off 0> Enables or disables  SPI transfers for the specified AMB Mask (0 - 0xF)\r\n",  SetSpiEnCmdCallback, 2},
    { "get_spi_en",         "get_spi_en           Retrieves a mask showing which AMB's have SPI ports enabled.\r\n", GetSpiEnCmdCalllback, 0}, 
    { "uvp_init",           "uvp_init             <AMB_Mask> <Uvp_Mask>  Initializes the UVP's on the specified AMB's.\r\n", UvpInitCmdCallback, 2},
    { "clk_init",           "clk_init             Initializes the system clock which is sent to all the AMB's.\r\n", ClkInitCmdCallback, 0},
    { "set_tx_freq",        "set_tx_freq          <Freq_in_Mhz> Sets the transmit frequency. If 0, the comm channel will be used to set the frequency.\r\n", TxFreqCmdCallback, 1},
    { "calibrate",          "calibrate            Causes the transmitter to go through its calibration process.\r\n", CalibrateCmdCallback, 0},
    { "send_disc",          "send_disc              [comm_ch] Send the discovery message on comm_ch. Unspecified means current ch. \r\n", SendDiscCmdCallback, VARIABLE_PARAM_COUNT},
    { "set_power_level",    "set_power_level      <PowerLevel>  Sets the UVP output power level in millidBm.\r\n",  SetPowerLevelCmdCallback, 1},
    { "get_power_level",    "get_power_level      Retrieves current UVP power level in millidBm.\r\n",  GetPowerLevelCmdCallback, 0},
    { "register_rx",        "register_rx          <Long_ID> [query_type] Registers a new discovered receiver with the system.\r\n",  RegisterClientCmdCallback, VARIABLE_PARAM_COUNT},
    { "remove_rx",          "remove_rx            <Long_ID> Removes a receiver from the list of registered receivers.\r\n",  RemoveClientCmdCallback, 1},
    { "proxy_command",      "proxy_command        <ID> [Data_Bytes] Sends a generic command to the proxy.\r\n",  ProxyCommandCmdCallback, VARIABLE_PARAM_COUNT},
    { "proxy_command_data", "proxy_command_data   Prints the data from returned from proxy_command.\r\n",  ProxyCommandDataCmdCallback, 0},
    { "rx_list",            "rx_list              [all] Displays each receiver that has been discovered.\r\n", RcvrListCmdCallback, VARIABLE_PARAM_COUNT},
    { "identify_tx",        "identify_tx          Identifies the Transmitter using the light ring.\r\n",  TxIdCmdCallback, 0},
    { "get_valid_amb_mask", "get_valid_amb_mask   Retrieves the mask of AMB's that are valid to enable.\r\n",  GetValidAmbMaskCmdCallback, 0},
    { "set_valid_amb_mask", "set_valid_amb_mask   <0x0-0xF> Sets the mask of AMB's that are valid to enable.\r\n",  SetValidAmbMaskCmdCallback, 1},
    { "get_comm_channel",   "get_comm_channel     Retrieves current comm channel.\r\n",  GetCommChannelCmdCallback, 0},
    { "set_comm_channel",   "set_comm_channel     <24-26> Sets comm channel for the system.\r\n",  SetCommChannelCmdCallback, VARIABLE_PARAM_COUNT},
    { "reboot",             "reboot               Reboots the system (gracefully shuts down AMB's and reboots).\r\n",  RebootCmdCallback, 0},
    { "reset_proxy",        "reset_proxy          Resets the proxy and resends the discovery message.\r\n", ResetProxyCmdCallback, 0},
    { "run",                "run                  Resumes normal operation after the pause command has been issued.\r\n",  RunCmdCallback, 0},
    { "pause",              "pause                Pauses the normal state machine operation and enters debug mode.\r\n", PauseCmdCallback, 0},
    { "start_static_tune",  "start_static_tune    Starts the static tune process.\r\n", StartStaticTuneCmdCallback, 0},
    { "sample_beacon",      "sample_beacon        Samples the beacon after a start_static_tune has been called.\r\n", SampleBeaconCmdCallback, 0},
    { "static_charge",      "static_charge        [beat] [hour] [min] Starts a static tune power transmission.\r\n", StaticChargeCmdCallback, VARIABLE_PARAM_COUNT},
    { "set_sys_clk_cfg",    "set_sys_clk_cfg      <mode> 0 = clock to AMB's only, 1 = add TAM, 2 = add Master, 3 = add TAM and Master  4 = Slave 5 = Slave and TAM.\r\n", SetSysClkCmdCallback, 1},
    { "get_sys_clk_cfg",    "get_sys_clk_cfg      Retrieves the system clock configuration.\r\n", GetSysClkCmdCallback, 0},
    { "start_charging",     "start_charging       <Long_ID | all> Changes a ready device to the charging state, making it eligible for TPS.\r\n", StartChargingCmdCallback, 1},
    { "stop_charging",      "stop_charging        <Long_ID | all> Changes a charging device to the ready state.\r\n", StopChargingCmdCallback, 1},
    { "reset_nvm",          "reset_nvm            Resets the non-volatile memory to its default values.\r\n", ResetNvmCmdCallback, 0},
    { "set_rssi_thresh",    "set_rssi_thresh      <nSamples> <deviation> Captures, averages, and sets the RSSI threshold for RSSI filtering.\r\n", SetRssiThreshCmdCallback, 2},
    { "set_rssi_filter_en", "set_rssi_filter_en   <on | off> Enables or disables RSSI filtering.\r\n", SetRssiFilterEnCmdCallback, 1},
    { "get_rssi_filter_en", "get_rssi_filter_en   Get the current  RSSI filtering state.\r\n", GetRssiFilterEnCmdCallback, 1},
    { "rssi_filter_reset",  "rssi_filter_reset    Resets the NVM storage for RSSI filtering.\r\n", RssiFilterResetCmdCallback, 0},
    { "amb_filter_disabled","amb_filter_disabled  Display which AMB's are disabled by RSSI voting.\r\n", AmbFilterDisabledCallback, 0},
    { "rssi_filter_report", "rssi_filter_report   If TPS is frozen, this will cause a report to be printed.\r\n", PrintFilterReportCmdCallback, 0},
    { "app_command",        "app_command          <Long_ID> <Data> Sends a generic command to the receiver.\r\n",  AppCommandCmdCallback, VARIABLE_PARAM_COUNT},
    { "app_command_data",   "app_command_data     <Long_ID> Prints the data received by last app_command.\r\n", AppCommandDataCmdCallback, 1},
    { "uvp_write_data",     "uvp_write_data       <AMB_Mask> <UVP_Mask> <page> <addr> <size> <offset> <value> Writes to a UVP register.\r\n", UvpWriteCallback, 7},
    { "uvp_read_data",      "uvp_read_data        <AMB_Mask> <UVP_Mask> <page> <addr> <size> <offset> Reads from a UVP register.\r\n", UvpReadCallback, 6},
    { "lock_detect" ,       "lock_detect          <AMB_Mask> <UVP_Mask> Determines if a UVP is locked to its clock.\r\n", UvpLockCallback, 2},
    { "set_fans_full",      "set_fans_full        <on | off> Forces the fans to full speed if on; if off, fans are controlled by the fan driver.\r\n", SetFansFullCallback, 1},
    { "get_fans_full",      "get_fans_full        Return the fans to full speed state flag.\r\n", GetFansFullCallback, 0},
    { "rx_config",          "rx_config            <Long_ID> <type> Sets the query type for a receiver (only 5 or 6 supported).\r\n", SetQueryTypeCallback, 2},
    { "rx_detail",          "rx_detail            <Long_ID> Retrieves detailed information for one receiver.\r\n", RcvrDetailCallback, 1},
    { "charge_virtual",     "charge_virtual       Starts TPS when an external beacon is present (test command).\r\n", ChargeVirtualCallback, 0},
    { "set_cfg_param",      "set_cfg_param        <parameter_id> <value>  Sets a parameter to a value in the NVM.\r\n", SetNvmConfigCallback, 2},
    { "get_cfg_param",      "get_cfg_param        <parameter_id>  Retrieves a parameter value from the NVM.\r\n", GetNvmConfigCallback, 1},
    { "get_system_temp",    "get_system_temp      Displays temperatures for the CCB and valid AMB's.\r\n", GetSystemTempCallback, 0},
    { "reset_array",        "reset_array          Resets the AMB Array for valid AMB's.\r\n", ResetArrayCallback, 0},
    { "uvp_mem_dump",       "uvp_mem_dump         <AMB_Number> <UVP_Number> Dumps the memory for the  specified  UVP (0-15) on the specified AMB (0-3).\r\n", UvpMemDumpCallback, 2},
    { "sys_clk_dump",       "sys_clk_dump         Dumps the register for the system clock.\r\n", SysClkDumpCallback, 0},
    { "versions",           "versions             Prints version number for this software.\r\n", GetVersionCallback, 0},
    { "get_system_state",   "get_system_state     Prints the state of the system.\r\n", GetSystemStateCallback, 0},
    { "clk_i2c_disable",    "clk_i2c_disable      Disables the i2c for the clock so it can programmed externally.\r\n", ClkI2CDisable, 0},
    { "clk_read",           "clk_read             <reg> reads a clk register\r\n", ClkRead, 1},
    { "clk_write",          "clk_write            <reg> <write> Writes a clock register.\r\n", ClkWrite, 2},
    { "bootloader",         "bootloader           Reboots the MCU into bootloader.\r\n",  BootloaderCallback, 0},
    { "proxy_info",         "proxy_info           Display proxy information.\r\n", CmdProxyInfo, 0},
};

const CLI_Command_Definition_t *GetCliCmds(void)
{
    return gCliCmds;
}

uint16_t GetCliCmdSize(void)
{
    return sizeof(gCliCmds)/sizeof(gCliCmds[0]);
}


/**
 * @brief Process the set_amb_en command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetAmbOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    int32_t num;
    MsgSetAmbOn_t setAmbOn;
    
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (getNumArg(pArg, argLen, &num))
    {
        setAmbOn.ambMask = (AmbMask_t)num;

        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);

        setAmbOn.enable = getArgOnOff(pArg, argLen);

        CtrlSendMsg(CTRL_SET_AMB_EN, (uint8_t*) &setAmbOn, sizeof(setAmbOn));
    }
    else
    {
        POST_COTA_ERROR(COTA_ERROR_BAD_PARAMETER);
    }

    
    return retVal;
}

/**
 * @brief Get a mask indicating which AMB's are on
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetAmbOnCmdCalllback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_GET_AMB_EN, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Implements amb_filter_disabled command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AmbFilterDisabledCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_AMB_FILTER_DISABLED, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process the set_uvp_en command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetUvpOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    int32_t num;
    MsgSetUvpOn_t setUvpOn;
     
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (getNumArg(pArg, argLen, &num) && (num <= ALL_AMB_MASK))
    {
        setUvpOn.ambMask = (AmbMask_t) num;
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
        setUvpOn.enable = getArgOnOff(pArg, argLen);
        CtrlSendMsg(CTRL_SET_UVP_EN, (uint8_t*) &setUvpOn, sizeof(setUvpOn));
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument 1.\r\n");
    }

    return retVal;
}

/**
 * @brief Get a mask indicating which AMB's UVP's are on
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetUvpOnCmdCalllback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_GET_UVP_EN, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process the set_pu_en command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetPuOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    int32_t num;
    MsgSetPuOn_t pu;
     
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (getNumArg(pArg, argLen, &num) && (num <= ALL_AMB_MASK))
    {
        pu.ambMask = (uint8_t)num;
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
        // returns a 0 if pArg is not 'on' or '1'
        pu.enable = getArgOnOff(pArg, argLen);
        CtrlSendMsg(CTRL_SET_PU_EN, (uint8_t*) &pu, sizeof(pu));
    }
    else
    {
        POST_COTA_ERROR(COTA_ERROR_BAD_PARAMETER);
    }
    
    return retVal;
}

static BaseType_t GetPuOnCmdCalllback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_GET_PU_EN, NULL, 0);
    return pdFALSE;
}
/**
 * @brief Process the set_tx_on command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    uint8_t arg;

    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    arg = getArgOnOff(pArg, argLen);
    CtrlSendMsg(CTRL_SET_TX, &arg, 1);
    
    return retVal;
}

/**
 * @brief Process the get_tx command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
   
    CtrlSendMsg(CTRL_GET_TX, NULL, 0);

    return retVal;
}

/**
 * @brief Process the rx_off command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    uint8_t arg;

    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    arg = getArgOnOff(pArg, argLen);
    CtrlSendMsg(CTRL_SET_RX, &arg, 1);
        
    return retVal;
}

/**
 * @brief Process the get_rx command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
   
    CtrlSendMsg(CTRL_GET_RX, NULL, 0);

    return retVal;
}


/**
 * @brief Process the set_pu_all command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetPuAllCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    uint8_t arg;
    
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    arg = getArgOnOff(pArg, argLen);
    CtrlSendMsg(CTRL_SET_PU_ALL, &arg, sizeof(uint8_t));
    
    return retVal;
}

/**
 * @brief Process the get_pu_all command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetPuAllCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
   
    CtrlSendMsg(CTRL_GET_PU_ALL, NULL, 0);

    return retVal;
}

/**
 * @brief Select the UVP number to interact with.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SelectUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    int32_t num;
    uint8_t arg;
    
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        arg = ALL_UVP_NUM;
    }
    else if (getNumArg(pArg, argLen, &num) && (num < UVPS_PER_AMB))
    {
        arg = (uint8_t)num;
    }

    CtrlSendMsg(CTRL_SELECT_UVP, (uint8_t*) &arg, sizeof(arg));
    
    return retVal;  
}

/**
 * @brief Retrieves the UVP number that is currently selected
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetSelectedUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_GET_SELECTED_UVP, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Retrieves the state of phase detect of each AMB
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetPdDoneCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_GET_PD_DONE, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process the set_spi_en command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetSpiEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;  

    int32_t value = 0;
    MsgSetSpiEnable_t spi;
    
    const char* pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (getNumArg(pArg, argLen, &value) && (value <= ALL_AMB_MASK))
    {
        spi.ambMask = (uint8_t)value;
        pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
        // returns a 0 if pArg is not 'on' or '1'
        spi.enable = getArgOnOff(pArg, argLen);
        CtrlSendMsg(CTRL_SET_SPI_EN, (uint8_t *)&spi, sizeof(spi));    
    }
    else
    {
        POST_COTA_ERROR(COTA_ERROR_BAD_PARAMETER);
    }

    return retVal;
}

/**
 * @brief Get a mask indicating which AMB's SPI's are enabled
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetSpiEnCmdCalllback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_GET_SPI_EN, NULL, 0);
    return pdFALSE;
}
/**
 * @brief In debug mode, this cause the uvp's to be initialized
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t UvpInitCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg1;
    BaseType_t argLen1;  
    char *pArg2;
    BaseType_t argLen2;
    int32_t num1, num2;
    MsgUvpInit_t uvpInit;
    
    // Contains a Long ID of the client
    pArg1 = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen1);
    pArg2 = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen2);    
    
    if (getNumArg(pArg1, argLen1, &num1) && getNumArg(pArg2, argLen2, &num2))
    {
        uvpInit.ambMask = (AmbMask_t)num1;
        uvpInit.uvpMask = (UvpMask_t)num2;
        CtrlSendMsg(CTRL_UVP_INIT, (uint8_t *)&uvpInit, sizeof(uvpInit));       
    }
    
    return pdFALSE;

}

/**
 * @brief Processes the system clock init command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t ClkInitCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    
    CtrlSendMsg(CTRL_SYS_CLK_INIT, NULL, 0);
    
    return retVal;
}


/**
 * @brief Processes the set tx frequency command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t TxFreqCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    int32_t num;
    uint32_t freq = 0;
    
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);   
 
    if (getNumArg(pArg, argLen, &num))
    {
        freq = (uint32_t) num;
    
        CtrlSendMsg(CTRL_TX_FREQ, (uint8_t*)&freq, sizeof(freq));
    }
      
    return retVal;
}

/**
 * @brief Processes the calibrate command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t CalibrateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    
    CtrlSendMsg(CTRL_CALIBRATION, NULL, 0);
    
    return retVal;
}

/**
 * @brief Processes the send_disc command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SendDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    MsgSendDisc_t disc;
    BaseType_t argLen;
    int32_t num;
    BaseType_t retVal = pdFALSE;

    // 0 indicates the current channel
    disc.channel = 0;
    
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (getNumArg(pArg, argLen, &num))
    {
        disc.channel = (uint8_t) num;
        CtrlSendMsg(CTRL_SEND_DISCOVERY_MESSAGE, (uint8_t *) &disc, sizeof(disc));
    }

    return retVal;
}

/**
 * @brief Processes the set power level command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{    
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    uint16_t arg;
    char * eptr;
    
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);    
    double powerlevel = strtod(pArg, &eptr);
    // now change to millidBm two bytes.
    // checking for valid powerlevels is done in SetAmbPowerLevels()
    arg = (uint16_t)(powerlevel * 1000.0);
    CtrlSendMsg(CTRL_SET_POWER_LEVEL, (uint8_t*)&arg, sizeof(arg));
    
    return retVal;
}

/**
 * @brief Processes the get power level command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    uint16_t powerLevel;
    
    CfgGetParam(CFG_POWER_LEVEL, &powerLevel, sizeof(powerLevel));
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "PL: %4.1f dBm\r\n", powerLevel/1000.0);

    return pdFALSE;  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}

/**
 * @brief Processes the register client command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t RegisterClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    MsgRegRcvr_t regData;
    int32_t num = 0;
    bool longIdOk = true;

    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);

    getNumArg(pArg, argLen, &num);

    regData.queryType = (CotaRcvrQuery_t)num;                  

    if ((regData.queryType != RCVR_QUERY_STANDARD) && (regData.queryType != RCVR_QUERY_CUSTOM))
    {
        regData.queryType = RCVR_QUERY_STANDARD;
    }
        
    // Contains a Long ID of the client
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        regData.longId = ALL_CLIENT_LONGID;
        
        snprintf(pcWriteBuffer, xWriteBufferLen, "Registering all discovered clients with query type %d\r\n",
            regData.queryType);
    }
    else if (parseLongId(pArg, &(regData.longId)))
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "Registering client w/Id: 0x%llx with query type %d\r\n",
            *(uint64_t *) &regData.longId, regData.queryType);
    }
    else
    {
        longIdOk = false;
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse longId: %s\r\n", pArg);
    }
   
    if (longIdOk)
    {      
        CtrlSendMsg(CTRL_REGISTER_RCVR, (uint8_t*) &regData, sizeof(regData));
    }

    return pdFALSE;
}

/**
 * @brief Processes remove client command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return
 */
static BaseType_t RemoveClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    MsgRegRcvr_t regData;

    // Contains a Long ID of the client
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    // Null terminate the parameter
    pArg[argLen] = 0x00;

    if (isArgAll(pArg,argLen))
    {
        regData.longId = ALL_CLIENT_LONGID;
        
        snprintf(pcWriteBuffer, xWriteBufferLen, "Removing all clients\r\n");
        
        CtrlSendMsg(CTRL_REMOVE_RCVR, (uint8_t*) &regData, sizeof(regData));
    }
    else if (parseLongId(pArg, &(regData.longId)))
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "Removing client w/ Id: 0x%llx\r\n",
                 *((uint64_t *) &regData.longId));
        
        CtrlSendMsg(CTRL_REMOVE_RCVR, (uint8_t*) &regData, sizeof(regData));
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse longId: %s\r\n", pArg);
    }
    
    return pdFALSE;
}

/**
 * @brief Processes the client_list command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return
 */
static BaseType_t RcvrListCmdCallback (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t argLen;
    MsgRcvrList_t all;
   
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        all.all = 1;
    }
    else
    {
        all.all = 0;
    }
    CtrlSendMsg(CTRL_RCVR_LIST, (uint8_t*)&all, sizeof(all));
    
    return pdFALSE;
}

/**
 * @brief Processes the reset nvm command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return
 */
static BaseType_t ResetNvmCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_RESET_NVM, NULL, 0);
    
    return pdFALSE;
}

/**
 * @brief Processes the start static tune command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return
 */
static BaseType_t StartStaticTuneCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_START_STATIC_TUNE, NULL, 0);
    return pdFALSE;    
}

/**
 * @brief Processes the sample becaon command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return
 */
static BaseType_t SampleBeaconCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_SAMPLE_BEACON, NULL, 0);
    return pdFALSE;    
}

/**
 * @brief Processes the static charge command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return
 */
static BaseType_t StaticChargeCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    int32_t num = 0;
    MsgStaticCharge_t msgStatic;

    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    getNumArg(pArg, argLen, &num);
    
    msgStatic.beat = num;
    
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);

    getNumArg(pArg, argLen, &num);
    
    msgStatic.hours = num;

    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 3, &argLen);

    getNumArg(pArg, argLen, &num);
    
    msgStatic.minutes = num;    
    
    CtrlSendMsg(CTRL_STATIC_CHARGE, (uint8_t*)&msgStatic, sizeof(msgStatic));
    return pdFALSE;    
}

/**
 * @brief Processes the start charging command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return
 */
static BaseType_t StartChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t argLen;
    ExtdAddr_t longId = INVALID_CLIENT_LONGID;
   
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        longId = ALL_CLIENT_LONGID;
    }
    else if (!parseLongId(pArg, &longId))
    {   
        longId = INVALID_CLIENT_LONGID;
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse longId: %s\r\n", pArg);
    }
    
    if (IsLongValid(longId))
    {
        CtrlSendMsg(CTRL_START_CHARGING_DEVICES, longId.bytes, sizeof(longId.bytes));      
    }
    
    return pdFALSE;   // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}


/**
 * @brief Processes the stop charging command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return
 */
static BaseType_t StopChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t argLen;
    ExtdAddr_t longId = INVALID_CLIENT_LONGID;

    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        longId = ALL_CLIENT_LONGID;
    }
    else if (!parseLongId(pArg, &longId))
    {   
        longId =INVALID_CLIENT_LONGID;
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse longId: %s\r\n", pArg);
    }
    
    if (IsLongValid(longId))
    {
        CtrlSendMsg(CTRL_STOP_CHARGING_DEVICES, (uint8_t*)&longId, sizeof(longId));      
    }
    
    return pdFALSE;  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}

/**
 * @brief Process get ValidAmbs command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetValidAmbMaskCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    AmbMask_t ambMask = 0;
    CotaError_t err = COTA_ERROR_NONE;

    err = CfgGetParam(CFG_VALID_AMB_MASK, &ambMask, sizeof(AmbMask_t));
    if (err == COTA_ERROR_NONE)
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "Valid AMB's: 0x%04x\r\n", ambMask);
    }
    else
    {
        POST_COTA_ERROR(err);
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);    
    }   
    
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}


/**
 * @brief Process the set ValidAmbs command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 *
 * @todo We may need to add code to reset the system
 *
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetValidAmbMaskCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen = 0;
    AmbMask_t ambMask = 0;
    int32_t num = 0;
    CotaError_t err = COTA_ERROR_NONE;
    
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        ambMask = ALL_AMB_MASK;
    }
    else if (getNumArg(pArg, argLen, &num) && (num <= ALL_AMB_MASK))
    {
        ambMask = (AmbMask_t)num;
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
        err = COTA_ERROR_BAD_PARAMETER;
    }
    
    if (err == COTA_ERROR_NONE)
    {
        err = CfgSaveParam(CFG_VALID_AMB_MASK, &ambMask, sizeof(AmbMask_t));
        if (err == COTA_ERROR_NONE)
        {
            snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, SUCCESS_MSG);
        }
        else
        {
            POST_COTA_ERROR(err);
            snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);    
        }
    }
    
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}


/**
 * @brief Process get comm channel command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetCommChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    uint8_t commChannel;
    
    CfgGetParam(CFG_COMM_CHANNEL, &commChannel, sizeof(commChannel));
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "Comm Channel: %d\r\n", commChannel);

    return pdFALSE;  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}

/**
 * @brief Process the set comm channel command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @todo We may need to add code to reset the system or change the clock frequency.  And change client comm channels.
 *
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetCommChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t argLen1 = 0;
    BaseType_t argLen2 = 0;    
    int32_t num = 0;
    CotaError_t err = COTA_ERROR_NONE;
    MsgSetCommChannel_t setCommCh;
    
    const char* pArg1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen1);
    const char* pArg2 = FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen2);
       
    setCommCh.longId = INVALID_CLIENT_LONGID;
    
    if (getNumArg(pArg1, argLen1, &num) && (num <= COMM_CHANNEL_HIGH) && (num >= COMM_CHANNEL_LOW))
    {
        setCommCh.channel = (uint8_t)num;
    }
    
    if (err == COTA_ERROR_NONE)
    {
        if (argLen2 > 0)
        {
            if (isArgAll(pArg2, argLen2))
            {
                setCommCh.longId = ALL_CLIENT_LONGID;
            }
            else if (!parseLongId(pArg2, &setCommCh.longId))
            {
                PostPrintf("Failed to parse long id argument\r\n");
                err = COTA_ERROR_BAD_PARAMETER;
            }
        }
    }
    
    if (err == COTA_ERROR_NONE)
    {
        CtrlSendMsg(CTRL_SET_COMM_CHANNEL, (uint8_t*)&setCommCh, sizeof(setCommCh));      
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
        err = COTA_ERROR_BAD_PARAMETER;
    }
    return pdFALSE;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}

/**
 * @brief Process the set system clock command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SetSysClkCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    int32_t num;
    uint8_t cfg = 0;
    CotaError_t err = COTA_ERROR_NONE;
    
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);   
 
    if (getNumArg(pArg, argLen, &num))
    {
        cfg = (uint8_t) num;
        if (cfg < SYS_CLK_MAX_CONFIG)
        {
            err = CfgSaveParam(CFG_SYSTEM_CLOCK, &cfg, sizeof(cfg));
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }

        POST_COTA_ERROR(err);
    }
    
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}

/**
 * @brief Process the get system clock config command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetSysClkCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    uint8_t sysClock;
    CfgGetParam(CFG_SYSTEM_CLOCK, &sysClock, sizeof(sysClock));
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "System Clock Cfg: %d\r\n", sysClock);

    return pdFALSE;  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}

/**
 * @brief Process the reset_proxy command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t ResetProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    // Issue the reset command
    CtrlSendMsg(CTRL_PROXY_RESET, NULL, 0);
    
    return pdFALSE;
}

/**
 * @brief Process the proxy command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t ProxyCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    uint8_t payloadByteIndex = 0;  // An index within payload field of the proxy command
    PrxHostProxyCmdMsg_t prxCmdData;
    const char* pArg;
    
    // Extract the first argument (message type) at offset 1
    pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    prxCmdData.msgType = (uint8_t) strtoul(pArg, NULL, 0); 
    
    // Make sure to parse MAX_PROXY_CMD_PAYLOAD_SIZE payload bytes
    while (pArg && (payloadByteIndex < MAX_PROXY_CMD_PAYLOAD_SIZE))
    {
        // Read the next parameter starting at offset 2
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, payloadByteIndex + 2, &argLen);
        if (pArg)
        {
            prxCmdData.payload.bytes[payloadByteIndex] = (uint8_t) strtoul(pArg, NULL, 0); 
            payloadByteIndex++;
        }        
    }

    // Payload size includes the message type and the payload field
    prxCmdData.payloadSize = payloadByteIndex + 1;
    
    // Send the message including the payload size field (hence the extra 1 byte)
    CtrlSendMsg(CTRL_PROXY_COMMAND, (uint8_t*)&prxCmdData, prxCmdData.payloadSize + 1);
    
    return retVal;                      // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}

/**
 * @brief Process the client command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AppCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    uint8_t ii = 0;
    PrxHostClientCmdMsg_t clientData;
    
    clientData.extAddr = INVALID_CLIENT_LONGID;

    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg, argLen))
    {
        clientData.extAddr = ALL_CLIENT_LONGID;
    }
    else if (!parseLongId(pArg, &clientData.extAddr))
    {   
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse longId: %s\r\n", pArg);
    }

    if (IsLongValid(clientData.extAddr))
    {
        while (pArg && (ii < MAX_PROXY_CMD_PAYLOAD_SIZE))
        {
            pArg = FreeRTOS_CLIGetParameter(pcCommandString, ii + 2 , &argLen);  //arguments start at position 2
            if (pArg)
            {
                clientData.payload.bytes[ii] = (uint8_t) strtol(pArg, NULL, 0); 
                ii++;
            }        
        }

        clientData.payloadSize = ii;
     
        CtrlSendMsg(CTRL_APP_COMMAND, (uint8_t*)&clientData, clientData.payloadSize + (uint16_t)((uint8_t*)&clientData.payload - (uint8_t*)&clientData));      
    }
    
    return retVal;                      // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise    
}


/**
 * @brief Process run command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t RunCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_RUN, NULL, 0);
    return pdFALSE;
}
/**
 * @brief Process the identify_tx command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t TxIdCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_IDENTIFY_TX, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process the pause command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t PauseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_PAUSE, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process the reboot command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t RebootCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_REBOOT, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process the proxy_command_data command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t ProxyCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_PROXY_COMMAND_DATA, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process the sys_clk_dump command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t SysClkDumpCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_SYS_CLK_DUMP, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process the set_rssi_thresh command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t SetRssiThreshCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlMsg_t msg;
    uint16_t  len = 0;
    int32_t num1, num2;
    BaseType_t argLen1, argLen2;

    const char* pArg1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen1); 
    const char* pArg2 = FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen2);
     
    if (getNumArg(pArg1, argLen1, &num1) && getNumArg(pArg2, argLen2, &num2))
    {
        msg.id                                       = CTRL_SET_RSSI_THRESH;
        if (num1 <= MAX_RSSI_THRESH_SAMPLES)
        {
            msg.rssiThresh.nSamples  = num1;
            msg.rssiThresh.nDevIndBm = num2;
        len = sizeof(msg.id) +
                  sizeof(msg.rssiThresh);
        CtrlSendMsg(CTRL_SET_RSSI_THRESH, (((uint8_t*)&msg)+1), len);
    }
    else
    {
            snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR: max samples is 255\r\n");
        }
    }
    else
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR: Bad arguments\r\n");
    }
    
    return pdFALSE;
}

/**
 * @brief Process the set_rssi_filter_en command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t SetRssiFilterEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t argLen;
    uint8_t arg = 0;
     
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    arg = getArgOnOff(pArg, argLen);
    CtrlSendMsg(CTRL_SET_RSSI_FILTER_EN, &arg, sizeof(arg));
    return pdFALSE;
}

/**
 * @brief Process the get_rssi_filter_en command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t GetRssiFilterEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_GET_RSSI_FILTER_EN, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process rssi_filter_reset command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t RssiFilterResetCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_RSSI_FILTER_RESET, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process uvp write command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t UvpWriteCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    UvpRegWrite_t uvpRegWrite;
    const char* pArg;
    int32_t num;
    CotaError_t err = COTA_ERROR_NONE;
  
    pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (getNumArg(pArg, argLen, &num))
    {
        uvpRegWrite.regInfo.id.ambMask = (AmbMask_t)num;
    }
    else
    {
        err = COTA_ERROR_BAD_PARAMETER;
    }
    
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegWrite.regInfo.id.uvpMask = (UvpMask_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    }
    
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 3, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegWrite.regInfo.page = (uint8_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    }
 
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 4, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegWrite.regInfo.addr = (uint8_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    }
    
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 5, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegWrite.regInfo.size = (uint8_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    } 
    
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 6, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegWrite.regInfo.offset = (uint8_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    } 

    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 7, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegWrite.val = (uint32_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    }
    if (err == COTA_ERROR_NONE)
    {
        CtrlSendMsg(CTRL_UVP_WRITE_DATA, (uint8_t*)&uvpRegWrite, sizeof(uvpRegWrite));
    }
    
    POST_COTA_ERROR(err);
    
    return retVal;
}

/**
 * @brief Process uvp read command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t UvpReadCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    UvpRegRead_t uvpRegRead;
    const char* pArg;
    int32_t num;
    CotaError_t err = COTA_ERROR_NONE;
  
    pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (getNumArg(pArg, argLen, &num))
    {
        uvpRegRead.id.ambMask = (AmbMask_t)num;
    }
    else
    {
        err = COTA_ERROR_BAD_PARAMETER;
    }
    
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegRead.id.uvpMask = (UvpMask_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    }
    
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 3, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegRead.page = (uint8_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    }
 
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 4, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegRead.addr = (uint8_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    }
    
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 5, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegRead.size = (uint8_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    } 
    
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 6, &argLen);
        if (getNumArg(pArg, argLen, &num))
        {
            uvpRegRead.offset = (uint8_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    } 

    if (err == COTA_ERROR_NONE)
    { 
        CtrlSendMsg(CTRL_UVP_READ_DATA, (uint8_t*)&uvpRegRead, sizeof(uvpRegRead));
    }
    
    POST_COTA_ERROR(err);
    
    return retVal;  
}

/**
 * @brief Process uvp lock command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t UvpLockCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    AmbUvpMask_t uvpId;
    const char* pArg;
    int32_t num;
    CotaError_t err = COTA_ERROR_NONE;
  
    pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    if (isArgAll(pArg, argLen))
    {
        uvpId.ambMask = (AmbMask_t) AmbGetValidAmbs();
    }
    else 
    {
        if (getNumArg(pArg, argLen, &num))
        {
            uvpId.ambMask = (AmbMask_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    }
    
    if (err == COTA_ERROR_NONE)
    {  
        pArg = FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
        if (isArgAll(pArg, argLen))
        {
            uvpId.uvpMask = (UvpMask_t) ALL_UVP_MASK;
        }
        else if (getNumArg(pArg, argLen, &num))
        {
            uvpId.uvpMask = (UvpMask_t)num;
        }
        else
        {
            err = COTA_ERROR_BAD_PARAMETER;
        }
    }
 
    if (err == COTA_ERROR_NONE)
    {
        CtrlSendMsg(CTRL_LOCK_DETECT, (uint8_t*)&uvpId, sizeof(uvpId));
    }
    
    POST_COTA_ERROR(err);
    
    return retVal;   
}

/**
 * @brief Process Get fans_full command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t GetFansFullCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    uint8_t arg;

    GetFansFull(&arg);
     
    CtrlSendMsg(CTRL_GET_FANS_FULL, &arg, sizeof(arg));

    return pdFALSE;
}

/**
 * @brief Process Set fans_full command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t SetFansFullCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t argLen;
    uint8_t arg = 0;
     
    const char* pArg = FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);
    
    if (isArgOn(pArg,argLen))
    {
        arg = FAN_SPEED_FULL;
        CtrlSendMsg(CTRL_SET_FANS_FULL, &arg, sizeof(arg));
    }
    else if (isArgOff(pArg,argLen))
    {
        arg = FAN_SPEED_AUTO;
        CtrlSendMsg(CTRL_SET_FANS_FULL, &arg, sizeof(arg));
    }
    else
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR: Bad argument\r\n");
    }
    return pdFALSE;
}

/**
 * @brief Process set_query_type command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t SetQueryTypeCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    int32_t num;
    MsgSetQueryType_t params;
    bool argsCorrect = true;
   

    // Contains a Long ID of the client
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    if (!parseLongId(pArg, &(params.longId)))
    {
        argsCorrect = false;
    }
  
    // Contains a query typ
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
   
    if (getNumArg(pArg, argLen, &num))
    {
        params.type = (CotaRcvrQuery_t)num;
    }
    else
    {
        argsCorrect = false;     
    }
    
    if (argsCorrect)
    {
        CtrlSendMsg(CTRL_SET_RCVR_CONFIG, (uint8_t*) &params, sizeof(params));    
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse arguments\r\n");
    }
    return pdFALSE;
}

/**
 * @brief Process client_detail command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t RcvrDetailCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    MsgRegRcvr_t regData;

    // Contains a Long ID of the client
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    // Null terminate the parameter
    pArg[argLen] = 0x00;

    if (parseLongId(pArg, &(regData.longId)))
    {        
        CtrlSendMsg(CTRL_GET_RCVR_DETAIL, (uint8_t*) &regData, sizeof(regData));
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse long Id: %s\r\n", pArg);
    }
    
    return pdFALSE; 
}

/**
 * @brief Process app_command_data command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t AppCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    MsgRegRcvr_t regData;

    // Contains a Long ID of the client
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    // Null terminate the parameter
    pArg[argLen] = 0x00;

    if (parseLongId(pArg, &(regData.longId)))
    {        
        CtrlSendMsg(CTRL_APP_COMMAND_DATA, (uint8_t*) &regData, sizeof(regData));
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse long Id: %s\r\n", pArg);
    }
    
    return pdFALSE; 
}

/**
 * @brief Process charge_virtual command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t ChargeVirtualCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_CHARGE_VIRTUAL, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Process reset_array
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t ResetArrayCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{

    CtrlSendMsg(CTRL_RESET_ARRAY, NULL, 0);

    return pdFALSE;
}

/**
 * @brief Process get_system_temp
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t GetSystemTempCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{

    CtrlSendMsg(CTRL_GET_SYSTEM_TEMP, NULL, 0);

    return pdFALSE;
}

/**
 * @brief Process set_nvm_config
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t SetNvmConfigCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    int32_t num;
    int64_t longNum;
    bool argsCorrect = true;
    MsgSetConfigParam_t setNvmConfig;

    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    if (getNumArg(pArg, argLen, &num))
    {
        setNvmConfig.paramId = (ConfigParamId_t)num;       
    }
    else
    {
        argsCorrect = false;
    }
  
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
   
    if (getLongNumArg(pArg, argLen, &longNum))
    {
        setNvmConfig.val = (uint64_t)longNum;
    }
    else
    {
        argsCorrect = false;     
    }
    
    if (argsCorrect)
    {
        CtrlSendMsg(CTRL_SET_CFG_PARAM, (uint8_t*) &setNvmConfig, sizeof(setNvmConfig));    
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse arguments\r\n");
    }
    return pdFALSE;
  
}

/**
 * @brief Process uvp_mem_dump
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t UvpMemDumpCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    int32_t num;
    int64_t longNum;
    bool argsCorrect = true;
    MsgUvpMemDumpCallback_t uvpDump;

    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    if (getNumArg(pArg, argLen, &num))
    {
        uvpDump.ambNum = (AmbNum_t)num;       
    }
    else
    {
        argsCorrect = false;
    }
  
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
   
    if (getLongNumArg(pArg, argLen, &longNum))
    {
        uvpDump.uvpNum = (AmbNum_t)num;     
    }
    else
    {
        argsCorrect = false;     
    }
    
    if (argsCorrect)
    {
        CtrlSendMsg(CTRL_UVP_MEM_DUMP, (uint8_t*) &uvpDump, sizeof(uvpDump));    
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse arguments\r\n");
    }
    return pdFALSE;
  
}
/**
 * @brief Process get_nvm_config
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t GetNvmConfigCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    int32_t num;
    ConfigParamId_t configParam;

    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    if (getNumArg(pArg, argLen, &num))
    {
        configParam = (ConfigParamId_t)num;
        CtrlSendMsg(CTRL_GET_CFG_PARAM, (uint8_t*) &configParam, sizeof(configParam));  
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse arguments\r\n");
    }

    return pdFALSE;  
}
/**
 * @brief Print versions information about the current software. 
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t GetVersionCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_GET_FIRMWARE_VERSION, NULL, 0);
    return pdFALSE;
}
/**
 * @brief Print versions information about the current software. 
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t GetSystemStateCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_GET_SYSTEM_STATE, NULL, 0);
    return pdFALSE;
}


/**
 * @brief Reboots the MCU into the bootloader
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t BootloaderCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_BOOTLOADER, NULL, 0);
    return pdFALSE;
}


/**
 * @brief Process print_filter_reset
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t PrintFilterReportCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_RSSI_FILTER_REPORT, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Puts the i2c bus in tri-state so it can be programed independly. 
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t ClkI2CDisable(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_SYS_CLK_I2C_DISABLE, NULL, 0);
    return pdFALSE;
}

/**
 * @brief Handles the clk_read command.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t ClkRead(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    int32_t num;
    MsgSysClkRead_t msgRead;
          
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    if (getNumArg(pArg, argLen, &num))
    {
        msgRead.addr = (uint16_t)num;
        CtrlSendMsg(CTRL_SYS_CLK_READ, (uint8_t*)&msgRead, sizeof(msgRead));
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse arguments\r\n");
    }
    return pdFALSE;
}

/**
 * @brief Handles the clk_write command.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return  pdFALSE to indicate the command has been processed
 */
static BaseType_t ClkWrite(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    bool argsCorrect = true;
    int32_t reg;
    int32_t regVal;
    MsgSysClkWrite_t msgWrite;

    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 1, &argLen);

    if (!getNumArg(pArg, argLen, &reg))
    {
        argsCorrect = false;
    }
  
    pArg = (char *)FreeRTOS_CLIGetParameter(pcCommandString, 2, &argLen);
   
    if (!getNumArg(pArg, argLen, &regVal))
    {
        argsCorrect = false;     
    }
    
    if (argsCorrect)
    { 
        msgWrite.addr = (uint16_t)reg;
        msgWrite.val = regVal;
        CtrlSendMsg(CTRL_SYS_CLK_WRITE, (uint8_t*)&msgWrite, sizeof(msgWrite));
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse arguments\r\n");
    }
    return pdFALSE;
}

static BaseType_t CmdProxyInfo(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_PROXY_INFO, NULL, 0);
    return pdFALSE;
}

/********************************************************************************
  Parsing helper functions
 ********************************************************************************/
/**
 * @brief Tests if an argument is 'on' or 'off' in a case insensitive manner
 *
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 *
 * @return 1 if 'on' or '1' else 0 
 */
static uint8_t getArgOnOff(const char* pArg, BaseType_t argLen)
{
    uint8_t num = 0;
    if ((argLen == strlen("on")) && (strncasecmp(pArg, "on", argLen) == 0) ||
        (argLen == strlen("1")) && (strncasecmp(pArg, "1", argLen) == 0))
    {
        num = 1;
    }
    return num;
}

/**
 * @brief Tests if an argument is 'all' in a case insensitive manner
 *
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 *
 * @return true if 'all', otherwise false 
 */
static bool isArgAll(const char* pArg, BaseType_t argLen)
{
    return ((argLen == strlen("all")) && (strncasecmp(pArg, "all", argLen) == 0));
}

/**
 * @brief Tests if an argument is 'off' in a case insensitive manner
 *
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 *
 * @return true if 'off', otherwise false 
 */
static bool isArgOff(const char* pArg, BaseType_t argLen)
{
    return (strncasecmp(pArg, "off", argLen) == 0);
}

/**
 * @brief Tests if an argument is 'on' in a case insensitive manner
 *
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 *
 * @return true if 'on', otherwise false 
 */
static bool isArgOn(const char* pArg, BaseType_t argLen)
{
    return (strncasecmp(pArg, "on", argLen) == 0);
}

/**
 * @brief Retrieves a number argument from a string
 * 
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 * @param num    Pointer to type long to receive the value of the string.
 *
 * @return true if the conversion is successful, false otherwise
 */
static bool getNumArg(const char* pArg, BaseType_t argLen, int32_t* num)
{
    char* endPtr;
    *num = strtol(pArg, &endPtr,  0);
    return ((pArg + argLen) == endPtr);   //This tests there was no stray characters in the arg
}

/**
 * @brief Retrieves a long number argument from a string
 * 
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 * @param num    Pointer to type long long to receive the value of the string.
 *
 * @return true if the conversion is successful, false otherwise
 */
static bool getLongNumArg(const char* pArg, BaseType_t argLen, int64_t* num)
{
    char* endPtr;
    *num = strtoll(pArg, &endPtr,  0);
    return ((pArg + argLen) == endPtr);   //This tests there was no stray characters in the arg
}

/**
 * @brief This function parses a long id from a command string
 * 
 * @param pArg    Pointer to a string containing the long id
 * @param longId  structure to fill with the long id
 *  
 * @return true if successful; false if not
 */
static bool parseLongId(const char* pArg, ExtdAddr_t *longId)
{
    char *endptr = NULL;
    uint64_t temp;
    
    temp = strtoll(pArg, &endptr, 16);

    // error parsing temp if endptr is NULL
    if ((temp == 0) || (longId == NULL))
    {
        return false;
    }
    
    memcpy((void *) longId, (void *) &temp, sizeof(temp));

    return IsLongValid(*longId);
}
#endif // #ifndef VENUS_BW_COMPATIBILITY 
