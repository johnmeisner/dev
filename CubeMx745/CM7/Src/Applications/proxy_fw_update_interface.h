/**
 * @file       proxy_fw_update_interface.h
 *
 * @brief      Header file for the proxy firmware update interface module
 *
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _PROXY_FW_UPDATE_INTERFACE_H_
#define _PROXY_FW_UPDATE_INTERFACE_H_

#include "error.h"
#include <stdbool.h>
#include <stdint.h>

CotaError_t ProxyFwUpdateMsgDispatch(uint8_t * inBuf, uint16_t inSize, uint8_t * outBuf, uint16_t * outSize);

#endif  // #ifndef _PROXY_FW_UPDATE_INTERFACE_H_
