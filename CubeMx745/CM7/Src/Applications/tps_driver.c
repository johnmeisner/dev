/**
 * @file       tps_driver.c
 *
 * @brief      TPS driver implementation
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "FreeRTOS.h"
#include "tps_driver.h" 
#include "stm32h7xx_hal.h"
#include "cmsis_os.h"
#include "stdbool.h"
#include "stdint.h"

#include "projdefs.h"
#include "task.h"
#include "semphr.h"
#include "gather_and_vote_task.h"

#include "main.h"
#include "amb_control.h"
#include "uvp_driver.h"
//jm #include "debug_off.h"
#include "debug_on.h"   //jm
#include "sysclk_driver.h"
#include "orion_config.h"
#include "ctrl_task.h"
#include "cli_task.h"

#define US_TO_TIMER_TICKS(us)           (us)               ///< Microseconds to ticks conversion for HW timers
#define US_TO_TIM1_PERIOD_REG_VAL(us)   ((us) - 1)         ///< Convert timer period in microseconds to ARR register value for timer 1 running at 1MHz after prescaler
#define US_TO_TIM3_PERIOD_REG_VAL(us)   ((us) - 1)         ///< Convert timer period in microseconds to ARR register value for timer 3 running at 1MHz after prescaler
#define US_TO_TIM8_PERIOD_REG_VAL(us)   (((us) * 10) - 1)  ///< Convert timer period in microseconds to ARR register value for timer 8 running at 10MHz after prescaler
#define PU_PULSE_DURATION_TICKS         1                  ///< PU pulse duration in timer ticks
#define MAX_TIMER_COUNT                 0xFFFF             ///< Maximum count value for 16-bit counters

static volatile int32_t gNumBbCount = 0;    ///< The current beat number in a TPS cycle
static uint32_t gBbPerTpsCycle;             ///< The number of beacon beats in a TPS cycle
static uint16_t gTpsRxDuration = 0;         ///< TPS RX pulse (tone slot) duration in units of #gClockTickUs1 microseconds
static uint16_t gTpsTxDuration = 0;         ///< TPS TX pulse (power slot) duration in units of #gClockTickUs1 microseconds
static uint16_t gClockTickUs1 = 1;          ///< Time of a clock tick for timer1 in microseconds
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim8;

static void TriggerPuPulse(void);

/**
 * @brief Sets the TPS delay time
 * @param timeUs Time to set it to in microseconds
 */ 
void SetTpsDelay(uint16_t timeUs)
{
    __HAL_TIM_SET_AUTORELOAD(&htim3, US_TO_TIM3_PERIOD_REG_VAL(timeUs));
}


/**
 * @brief Retrieve the beat number in the TPS cycle
 * @return The beat count
 */
int GetTpsBeat(void)
{
    return gNumBbCount;
}

/**
 * @brief Initializes the TPS timers
 * @return pdPass
 */ 
BaseType_t TpsDrvInit(void)
{
    BaseType_t retSuccess  = pdPASS;
    uint16_t tpsStartDelay = 0;     // TPS start delay in microseconds
    uint16_t tpsPaDelay    = 0;     // TPS PA delay in microseconds
    uint16_t tpsPaDelayRaw = 0;     // TPS PA delay in microseconds, but not modified for large beacon beats.
    uint16_t tpsRxDelay    = 0;     // TPS RX delay (the time between the start of the Rx pulse and the start of the PU pulse) in microseconds
    uint32_t beaconBeatPeriod = 0;  // TPS BB period in microseconds
    uint16_t tpsNumPowerSlots = 0;  // TPS power slot count in Tx pulse
    uint16_t prescaler;             // The adjusted prescaler for timer1 if large beacon beats are used

    // Load the number of beacon beats per TPS cycle from NVM
    CfgGetParam(CFG_TPS_NUM_BEACON_BEATS, &gBbPerTpsCycle, sizeof(gBbPerTpsCycle));
    
    // Load TPS delay time from NVM into the auto-reload register
    CfgGetParam(CFG_TPS_START_DELAY, &tpsStartDelay, sizeof(tpsStartDelay));
    __HAL_TIM_SET_AUTORELOAD(&htim3, US_TO_TIM3_PERIOD_REG_VAL(tpsStartDelay));
    
    
    // Load the power slot duration from NVM and calculate the BB period
    CfgGetParam(CFG_TPS_TX_DURATION, &gTpsTxDuration, sizeof(gTpsTxDuration));
    
    // Load the number of power slots in a TX pulse from NVM
    CfgGetParam(CFG_TPS_NUM_POWER_SLOTS, &tpsNumPowerSlots, sizeof(tpsNumPowerSlots));
    
    // Load the tone slot duration from NVM into the channel 1 capture compare register
    CfgGetParam(CFG_TPS_RX_DURATION, &gTpsRxDuration, sizeof(gTpsRxDuration));
    
    // Load the PA Delay from NVM into the auto-reload register
    CfgGetParam(CFG_TPS_PA_DELAY, &tpsPaDelayRaw, sizeof(tpsPaDelay));
    tpsPaDelay = tpsPaDelayRaw;
    
    // Load the the delay between the start of the RX pulse and the PU pulse from NVM
    CfgGetParam(CFG_TPS_RX_DELAY, &tpsRxDelay, sizeof(tpsRxDelay));
    
    // Calculate the BB period
    beaconBeatPeriod = (uint32_t)gTpsRxDuration + (((uint32_t)gTpsTxDuration) * ((uint32_t)tpsNumPowerSlots));

    if (beaconBeatPeriod >= MAX_TIMER_COUNT)
    {
        //In this case we going to have to increase the perscaler of tim1 in order to ensure the span of time1 counter is greater than the beat period
        prescaler = htim1.Init.Prescaler + 1;  //The prescaler is stored in the register as the prescaler -1, so we have to add a 1
        gClockTickUs1 = (beaconBeatPeriod/MAX_TIMER_COUNT+1);
        prescaler *= gClockTickUs1;
        htim1.Init.Prescaler = prescaler - 1;
        if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
        {
            Error_Handler();
        }
        //We must now update variables used to set up timer 1.
        gTpsRxDuration = (gTpsRxDuration + (gClockTickUs1/2))/gClockTickUs1;
        gTpsTxDuration = (gTpsTxDuration + (gClockTickUs1/2))/gClockTickUs1;
        tpsRxDelay = (tpsRxDelay + (gClockTickUs1/2))/gClockTickUs1;
        tpsPaDelay = (tpsPaDelay + (gClockTickUs1/2))/gClockTickUs1;
        beaconBeatPeriod = (uint32_t)gTpsRxDuration + (((uint32_t)gTpsTxDuration) * ((uint32_t)tpsNumPowerSlots));
    }

    PostPrintf("TPS Cycle duration  = %d us\r\n", beaconBeatPeriod * gClockTickUs1 * gBbPerTpsCycle);    
#ifdef TPS_PRINT_INFO_ON_STARTUP    
    PostPrintf("beacon beat period  = %d us\r\n", beaconBeatPeriod * gClockTickUs1);
    PostPrintf("tpsRxDuration       = %d us\r\n", gTpsRxDuration * gClockTickUs1);
    PostPrintf("tpsTxDuration       = %d us\r\n", gTpsTxDuration * gClockTickUs1);    
    PostPrintf("tpsRxDelay          = %d us\r\n", tpsRxDelay * gClockTickUs1);
    PostPrintf("tpsPaDelay          = %d us\r\n", tpsPaDelay * gClockTickUs1);
    PostPrintf("tpsStartDelay       = %d us\r\n", tpsStartDelay); 
    PostPrintf("time tick rate      = %d us\r\n", gClockTickUs1);    
    PostPrintf("tpsNumPowerSlots    = %d\r\n",    tpsNumPowerSlots); 
    PostPrintf("gBbPerTpsCycle      = %d\r\n",    gBbPerTpsCycle); 
#endif
    
    TIM1->CCR1 = US_TO_TIMER_TICKS(gTpsRxDuration);
    
    __HAL_TIM_SET_AUTORELOAD(&htim8, US_TO_TIM8_PERIOD_REG_VAL(tpsPaDelayRaw) + PU_PULSE_DURATION_TICKS);
    
    // Update the phase update timing to generate the PU pulse during the RX pulse
    // by updating the channel 2 capture compare register.  Note that the PU pulse starts
    // after a PA_Delay period after TIM8 timer is started, so we need to start the PU timer
    // PA_Delay before the time when the PU pulse needs to start.
    TIM1->CCR2 = US_TO_TIMER_TICKS(tpsRxDelay - tpsPaDelay);

    // Set up the initial counter value for the first PU pulse
    __HAL_TIM_SET_COUNTER(&htim8, MAX_TIMER_COUNT - htim8.Init.Period);
        
    // Load the BB period into the auto-reload register
    __HAL_TIM_SET_AUTORELOAD(&htim1, US_TO_TIM1_PERIOD_REG_VAL(beaconBeatPeriod));

    return retSuccess;
}

/**
 * @brief Starts the trigger pulse timer
 */ 
static void TriggerPuPulse(void)
{
    // Start phase update (PU) pulse generation after a delay
    //__HAL_TIM_SET_COUNTER(&htim8, htim8.Init.Period);
    DBG2_SET();
    HAL_TIM_PWM_Start_IT(&htim8, TIM_CHANNEL_3);
}

/**
  * @brief  This function is called from HAL when a PWM pulse ends
  *
  * This function is called by the MCU to start PWM generation of
  * the PU pulses and at the end of each PU pulse to end PU pulse
  * generation.
  *
  * @note   This function is meant to be called from ISR context.
  *
  */
void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim)
{
    static uint8_t isRxPulse;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
 
    if (htim->Instance == TIM1)
    {
        if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
        {
            // Indicates the end of the AMB_RX pulse          
            // PU pulse 2 (during Tx phase)
            TriggerPuPulse();

            // Tx active
            isRxPulse = 0;
        }
        else if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
        {
            // Indicates the middle of the Rx pulse on AMB_RX line
            //DBG10_SET();
            //DBG10_RESET();

            // PU pulse 1 (during Rx phase)
            TriggerPuPulse();
            isRxPulse = 1;
        }
        else
        {
            // Not reachable
        }
    }
    else if (htim->Instance == TIM8)
    {
        // Indicates the end of the PU pulse on PU_ALL line
        // Stop phase update (PU) pulse generation after the PU pulse
        HAL_TIM_PWM_Stop_IT(&htim8, TIM_CHANNEL_3);
        DBG2_RESET();
        if (isRxPulse && VoteEnabled())
        {
            
            //DBG6_SET();
            //It's important that we wait until pu pulse for the TX block
            //so that we are assured phases and RSSI values from the RX block
            //are updated and correct.
            xSemaphoreGiveFromISR(gGatherAndVoteSem, &xHigherPriorityTaskWoken);
            //DBG6_RESET();
            portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
        }
    }
}

/**
  * @brief  This function is called when a timer period elapses
  *
  * This function is called by the MCU when at the end of the TPS delay
  * timeout period and at the end of the Tx pulse on the AMB_TX line.
  *
  * @note   This function is meant to be called from ISR context.
  *
  */
void TpsTimerPeriodElapsedEvent(TIM_HandleTypeDef *htim)
{
    // Check if the Tx pulse just ended
    if (htim->Instance == TIM1)
    {
        // Indicate the end of the TPS Tx pulse
        DBG6_SET();
        DBG6_RESET();

        // @todo  Stop TPS here - this is executed after a Tx pulse is finished and before an Rx pulse starts

        gNumBbCount++;
        if (gNumBbCount == gBbPerTpsCycle)
        {
            // Reset our beacon beat count back to 0
            gNumBbCount = 0;
            
            // Show that TPS on TIM1 is about to stop
            //DBG1_RESET();
            
            // Stop a TPS cycle after the last TX pulse
            
            // Turn timer outputs for AMB_TX and AMB_RX signals off to set them both low
            __HAL_TIM_MOE_DISABLE_UNCONDITIONALLY(&htim1);
            
            // Stop the timer channels
            HAL_TIM_PWM_Stop_IT(&htim1, TIM_CHANNEL_1);
            HAL_TIMEx_PWMN_Stop_IT(&htim1, TIM_CHANNEL_1);
            HAL_TIM_PWM_Stop_IT(&htim1, TIM_CHANNEL_2);
            
            // Re-initialize the timer 1 counter - otherwise the next Rx pulse will be shorter than expected
            __HAL_TIM_SET_COUNTER(&htim1, 0);

            // Show that TPS on TIM1 has stopped
            DBG9_SET();
            DBG9_RESET();
            SignalTpsDone();
        }
    }
  
    else if (htim->Instance == TIM3)
    {
        // Indicate the start of the TPS cycle after the TPS delay
        DBG9_SET();
      
        // Start PWM on TIM1 to start controlling Tx, Rx, PU signals
        HAL_TIM_PWM_Start_IT(&htim1, TIM_CHANNEL_1);
        HAL_TIMEx_PWMN_Start_IT(&htim1, TIM_CHANNEL_1);
        HAL_TIM_PWM_Start_IT(&htim1, TIM_CHANNEL_2);

        // Indicate that the TPS timers have been started
        DBG9_RESET();
        DBG9_SET();
    }
    
    else
    {
        // Not used
    }
}

/**
 * @brief Stops the TPS cycle
 */
void StopTpsCycle(void)
{
    // Turn timer outputs for AMB_TX and AMB_RX signals off to set them both low
    __HAL_TIM_MOE_DISABLE_UNCONDITIONALLY(&htim1);

    // Stop the timer channels
    HAL_TIM_PWM_Stop_IT(&htim1, TIM_CHANNEL_1);
    HAL_TIMEx_PWMN_Stop_IT(&htim1, TIM_CHANNEL_1);
    HAL_TIM_PWM_Stop_IT(&htim1, TIM_CHANNEL_2);
    gNumBbCount = 0;  //Reset BB count 
    // Re-initialize the timer 1 counter - otherwise the next Rx pulse will be shorter than expected
    __HAL_TIM_SET_COUNTER(&htim1, 0);
}
