/**
 * @file       debug_off.h
 *
 * @brief      Debug pin control macros for modules with debug pins disabled
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _DEBUG_OFF_H_
#define _DEBUG_OFF_H_
#define DBG1_SET()
#define DBG2_SET()
#define DBG3_SET()
#define DBG4_SET()
#define DBG5_SET()
#define DBG6_SET()
#define DBG7_SET()
#define DBG8_SET()
#define DBG9_SET()
#define DBG10_SET()
#define DBG11_SET()
#define DBG12_SET()
#define DBG13_SET()

#define DBG1_RESET()
#define DBG2_RESET()
#define DBG3_RESET()
#define DBG4_RESET()
#define DBG5_RESET()
#define DBG6_RESET()
#define DBG7_RESET()
#define DBG8_RESET()
#define DBG9_RESET()
#define DBG10_RESET()
#define DBG11_RESET()
#define DBG12_RESET()
#define DBG13_RESET()

#define DBG1_TOGGLE()
#define DBG2_TOGGLE()
#define DBG3_TOGGLE()
#define DBG4_TOGGLE()
#define DBG5_TOGGLE()
#define DBG6_TOGGLE()
#define DBG7_TOGGLE()
#define DBG8_TOGGLE()
#define DBG9_TOGGLE()
#define DBG10_TOGGLE()
#define DBG11_TOGGLE()
#define DBG12_TOGGLE()
#define DBG13_TOGGLE()
#endif // #ifndef _DEBUG_OFF_H_
