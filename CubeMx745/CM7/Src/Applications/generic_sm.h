/**
 * @file       generic_sm.h
 *
 * @brief      Header for generic states among all Orion implementations
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _GENERIC_SM_H_
#define _GENERIC_SM_H_

void SetDebugMode(bool val);
bool GetDebugMode(void);
void HaltSystem(void);
void CompleteShutdownChecklist(void);
void RebootMCU(void);
void ForceLogIfWatchdogReset(void);

#endif /* _GENERIC_SM_H_ */
