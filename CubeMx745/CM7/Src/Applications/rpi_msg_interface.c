/** 
 * @file       rpi_msg_interface.C
 *
 * @brief      This module implements the Raspberry Pi intefrace functions
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/* Includes ---------------------------------------------------------*/

#include "rpi_msg_interface.h"
#include "ctrl_interface.h"
#include "raspberry_pi.h"
#include "crc.h"
#include <string.h>

/* Private defines -----------------------------------------------------------*/
#define CHECKSUM_SIZE sizeof(uint32_t) ///< The size of the CRC-32 checksum value

/* Private variables ---------------------------------------------------------*/
static uint8_t gTxFrameBuf[MAX_FRAME_SIZE];  ///< Tx frame buffer

/* Functions -----------------------------------------------------------------*/

/**
 * @brief  This function takes a received RPi SPI frame, breaks it down
 *         into individual messages and passes it on to the control task
 *
 * @param  framePayloadData  Pointer to the SPI frame data buffer
 *                           not including CRC offset or CRC
 * @param  frameSize         The number of bytes in the frame data buffer
 */
void RpiFrameToMessages(uint8_t *framePayloadData, uint16_t frameSize)
{
    if (framePayloadData != NULL)
    {
    
        uint8_t *   msgStartAddr   = framePayloadData;
        uint16_t    msgLen         = *((uint16_t*) (msgStartAddr));
        CtrlMsgId_t msgId          = *((CtrlMsgId_t*)(msgStartAddr + MSG_LEN_FIELD_SIZE));
        uint8_t *   msgPayloadAddr = msgStartAddr + MSG_LEN_FIELD_SIZE + MSG_ID_FIELD_SIZE;

        while (msgStartAddr < framePayloadData + frameSize)
        {
            // Subtract the message ID from the length because the id
            //   is separated from the payload.
            CtrlSendMsg(msgId, msgPayloadAddr, (msgLen - MSG_ID_FIELD_SIZE));
        
            msgStartAddr   += (msgLen + MSG_LEN_FIELD_SIZE);
            msgPayloadAddr  = msgStartAddr + MSG_LEN_FIELD_SIZE + MSG_ID_FIELD_SIZE;
            msgLen          = *((uint16_t*) (msgStartAddr));
            msgId           = *((CtrlMsgId_t*)(msgStartAddr + MSG_LEN_FIELD_SIZE));
        }
    }
}

/**
 * @brief   Function to send a message to RPi
 *
 * @details This function takes a message and wraps it in the SPI
 *          frame format for sending it to the RPi.
 *
 * @note    Do not call this function from ISR context
 *
 * @param   buf      Pointer to a buffer with data to transmit to the RPi
 * @param   size     Number of bytes in the buffer pointed to by buf
 *
 * @return  true if successful; false if the transmit queue is full
 */
bool RpiSendMessage(uint8_t * buf, uint16_t size)
{
    bool retVal = true;
    
    // Calculate the offset of the CRC field, which follows the
    // CRC offset field, message size field and message payload field
    uint16_t crcOffset = (uint16_t) (CRC_OFST_FIELD_SIZE + sizeof(size) + size);
    
    // Update the CRC offset field
    *(uint16_t*)gTxFrameBuf = crcOffset;

    // Add the size of the message within the frame
    *(uint16_t*)&gTxFrameBuf[CRC_OFST_FIELD_SIZE] = size;

    // Copy message data in to the frame buffer. We add the
    // sizeof(size) because this will be the message length within the
    // frame
    memcpy(&gTxFrameBuf[CRC_OFST_FIELD_SIZE + sizeof(size)], buf, size);
    
    // Update the CRC field based on all the other fields
    *(uint32_t*)&gTxFrameBuf[crcOffset] = Crc32(gTxFrameBuf, crcOffset);

    // Send the frame buffer to the RPi comm module
    retVal = RpiSendFrame(gTxFrameBuf, crcOffset + CHECKSUM_SIZE);

    return retVal;
}
