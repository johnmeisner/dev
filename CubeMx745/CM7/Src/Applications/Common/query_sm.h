/**
 * @file       query_sm.h
 *
 * @brief      Header module for the query state machine module
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#ifndef _QUERY_SM_H_
#define _QUERY_SM_H_

#if defined(USE_COMMON_QUERY)
  void QueryStateInit(void);
  bool StartQueryTimers(void);
  void EnableQueryCycle(bool state);
  void StartQueryTimer(void);
  void StopQueryTimer(void);
#endif // defined(USE_COMMON_QUERY)

#if defined(USE_FOREVER_TRACKER_BUILD)
  void SetFastQuery(bool state);
#endif // defined(USE_FOREVER_TRACKER_BUILD)

#endif /* _QUERY_SM_H_ */