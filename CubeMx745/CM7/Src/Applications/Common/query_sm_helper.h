/**
 * @file       query_sm_helper.h
 *
 * @brief      A helper module for the query_sm module. This helps us eliminate
 *             duplicate code between multiple definitions in query_sm.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef QUERY_SM_HELPER_H
#define QUERY_SM_HELPER_H

#include <stdbool.h>

/**
 * @brief Checks all potential cases that would disallow queries from.
 *
 *        This function can be linked to a handful of different calls
 *        based on the configuration of the system. Each
 *        implementation will have different parameters for what is
 *        considered an 'Error' condition.
 */
bool CheckIfQueryAllowed(void);
void WrapUpClientQueryState(void);

#endif /* QUERY_SM_HELPER_H */
