/**
 * @file       tps_scheduler.h
 *
 * @brief      The TPS Scheudler for the Forever Tracker system
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/**
 * Forever Tracker Scheduling
 *
 * 
 */
#ifndef TPS_SCHEDULER_H
#define TPS_SCHEDULER_H

#include "orion_config.h"
#include "error.h"
#include <stdbool.h>

#define  MAX_NUM_CLIENTS_IN_TPS     8   ///< Maximum number of client that can be in a TPS cycle with this firmware

/**
 * @brief A structure containing TPS info for a single client
 */ 
typedef PACKED struct _TpsClientData_t
{
    uint16_t shortId;          ///< Client's short ID ((0x0000-0x3fff)
    uint8_t  toneSlot;         ///< The slot that the client starts to tone
    uint8_t  tonePeriod;       ///< The number of tone slots (usually set to the number of clients)
    uint8_t  powerSlot;        ///< The power slot that client will be charge in.
    uint8_t  powerPeriod;      ///< Number of power slots aloted to the client
    uint8_t  powerDuration;    ///< Total number of power slots
} TpsClientData_t;

/**
 * @brief A structure contain TPS info for all the clients
 */ 
typedef PACKED struct _TonePowerSched_t
{
    uint8_t  prxCmd;                       ///< The value of the proxy command
    uint8_t  seqNum;                       ///< A number allowing the client to determine if TPS and GO message match
    uint8_t  numClients;                   ///< Total number of clients in this TPS cycle 
    uint8_t  repeatPeriod;                 ///< Total number of times to repeat a single tone power slot.  (usually set to number of clients) 
    TpsClientData_t clientTable[MAX_NUM_CLIENTS_IN_TPS]; ///< List of client see #TpsClientData_t
} TonePowerSched_t;

/**
 * Intended to hold cached ids and their battery values. This cache
 * will be used to create the actual TPS schedule at the end of the
 * process.
 */
typedef struct _ReceiverCache_t
{
    bool    isValid;  ///< Determines if the slot contains valid data
    uint8_t battery;  ///< battery value cache
    uint8_t shortId;  ///< short id of the receiver
} ReceiverCache_t;

/**
 * Arrays of receiver listings that intermediately holds all those
 * able to be scheduled and their batteries. Using this list, a TPS
 * Schedule will be constructed.
*/
typedef struct _ReceiverList_t
{
    ReceiverCache_t cache[MAX_NUM_CLIENTS_IN_TPS]; ///< A cache of all the receivers 
} ReceiverList_t;

void InitializeScheduler(void);
void IncrementTpsSeqNum(void);
TonePowerSched_t* GetTpsSchedule(void);
CotaError_t CalculateTpsSchedule(bool chargeVirtual);
void ClearTpsSchedule(void);
    
#endif /* TPS_SCHEDULER_H */
