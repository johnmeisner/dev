/**
 * @file       fan_sm.h
 *
 * @brief      Header module for the fan control state machine modules
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_COMMON_FAN)

#ifndef _FAN_SM_H_
#define _FAN_SM_H_

bool StartFanTimers(void);
void FansStateInit(void);
void CheckFans(bool);

#endif /* _FAN_SM_H_ */

#endif // defined(USE_COMMON_FAN)
