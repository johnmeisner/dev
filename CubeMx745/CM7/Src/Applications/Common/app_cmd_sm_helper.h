/**
 * @file       app_cmd_sm_helper.h
 *
 * @brief      A helper module for the app_cmd_sm module. This helps us eliminate
 *             duplicate code between multiple definitions in app_cmd_sm.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef APP_CMD_SM_HELPER_H
#define APP_CMD_SM_HELPER_H

/**
 * @brief Checks all potential cases that would prevent app commands from sending.
 *
 *        This function can be linked to a handful of different calls
 *        based on the configuration of the system. Each
 *        implementation will have different parameters for what is
 *        considered an 'Error' condition.
 */
bool CheckIfAppCmdAllowed();

#endif /* APP_CMD_SM_HELPER_H */

