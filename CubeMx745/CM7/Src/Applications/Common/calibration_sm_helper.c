/**
 * @file       calibration_sm_helper.c
 *
 * @brief      A helper module for the calibration_sm module. This helps us eliminate
 *             duplicate code between multiple definitions in calibration_sm.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include "orion_config.h"
#include "door_driver.h"

#if defined(USE_DEMO_BUILD)

bool CheckIfCalibrationAllowed(void)
{
    return true; // We have no conditions that wouldn't allow 
}

#elif defined(USE_FOREVER_TRACKER_BUILD)
#include "ForeverTracker/enumeration.h"

static bool checkDoorClosed(void);

bool CheckIfCalibrationAllowed(void)
{
    bool isDoorClosed = checkDoorClosed();
    
    return isDoorClosed && !CheckIfEnumerationPeriodActive(); 
}

/**
 * @brief Returns the state of the door once the door state has changed.
 */
static bool checkDoorClosed(void)
{
    static bool doorStateClosedCache = false;

    if (doorStateClosedCache != IsDoorClosed())
    {
        doorStateClosedCache = IsDoorClosed();
    }

    return doorStateClosedCache;
}

#endif
