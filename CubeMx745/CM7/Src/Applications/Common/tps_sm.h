/**
 * @file       tps_sm.h
 *
 * @brief      Header module for the tps state machine module
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_COMMON_TPS)

#ifndef _TPS_SM_H_
#define _TPS_SM_H_

bool IsTpsTimedOut(void);
void SignalTpsDone(void);
void TpsStateInit(void);
void ChargeVirtualClient(bool);

#endif /* _TPS_SM_H_ */

#endif // defined(USE_COMMON_TPS)
