/**
 * @file       tps_sm.c
 *
 * @brief      Defines the TPS state machine functions for the demo system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "error.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "log.h"
#include "tps_scheduler.h"
#include "tps_driver.h"
#include "calibrator.h"
#include "gather_and_vote_task.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"
#include "fan_driver.h"
#include "sysclk_driver.h"
#include "door_driver.h"
#include "tps_sm_helper.h"
#include "debug_on.h"
#include "door_driver.h"

#if defined(USE_FOREVER_TRACKER_BUILD)
  #include "ForeverTracker/receiver_removal_sm.h"
  #include "ForeverTracker/ft_client_interface.h"
  #include "ForeverTracker/wait_for_sleep.h"
#endif

#if defined(USE_COMMON_TPS)

static uint16_t     gTpsStartDelay;                 ///<
static uint8_t      gPreTpsClientPause;             ///< Time in milliseconds to wait before starting to allow client to clear pending commands.
static uint8_t      gPostTpsClientPause;            ///<
volatile bool       gChargeVirtual      = false;    ///< true when the system is charging virtually.
volatile bool       gTpsTimeout         = false;    ///< true when a tps timeout has occurred
volatile bool       gTpsDone            = false;    ///< true when the system has finished TPS
static bool         gRandSeed           = false;    ///< A flag indicating whether the random number generator has been seeded

TimerHandle_t  gTpsTimeoutTimerHandle   = NULL;     ///< FreeRTOS timer handle for the tps timeout timer
StaticTimer_t  gTpsTimeoutTimerBuffer;              ///< FreeRTOS timer buffer for the tps timeout timer

static void tpsTimeoutTimerCallback(TimerHandle_t  timer);

void ChargeVirtualClient(bool doChargeVirtual)
{
    gChargeVirtual = doChargeVirtual;
}

void TpsStateInit()
{
    uint16_t timerVal;
    uint8_t ethernetClkState = 0;

    gRandSeed = false;

    CfgGetParam(CFG_TPS_TIMEOUT, &timerVal, sizeof(timerVal));

    if (timerVal > 0)
    {
        if (gTpsTimeoutTimerHandle)
        {
            xTimerStop(gTpsTimeoutTimerHandle, NO_WAIT);
            xTimerChangePeriod(gTpsTimeoutTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gTpsTimeoutTimerHandle = xTimerCreateStatic(
                "Tps Timeout Timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                tpsTimeoutTimerCallback,
                &gTpsTimeoutTimerBuffer);
        }
    }
    else
    {
        gTpsTimeoutTimerHandle = NULL;
    }

    InitializeScheduler();

    // set the state of the Ethernet clock control GPIO
    CfgGetParam(CFG_ETHERNET_CLOCK_STATE , &ethernetClkState , sizeof(uint8_t));
    SetResetEthernetClock(ethernetClkState);

    TpsDrvInit();

    // Load timing parameters
    CfgGetParam(CFG_TPS_START_DELAY, &gTpsStartDelay, sizeof(gTpsStartDelay));
    CfgGetParam(CFG_PRE_TPS_CLIENT_PAUSE, &gPreTpsClientPause, sizeof(gPreTpsClientPause));
    CfgGetParam(CFG_POST_TPS_CLIENT_PAUSE, &gPostTpsClientPause, sizeof(gPostTpsClientPause));

    if (!gTpsTimeoutTimerHandle)
    {
        LogFatal("TPS Timeout timer failed to initialize\r\n");
    }
}

void TpsEntry(void)
{
    CotaError_t     ret;

    LogDebug("The Control task has entered the tps state.\r\n");

    vTaskDelay( pdMS_TO_TICKS(gPreTpsClientPause));

    if (VoteEnabled())
    {
        ResetRssiVote();
    }

    if (!gRandSeed)
    {
        // Tps sequence number are generated incrementally based on a random
        // number.  We have to generate a random number based on a seed
        // set by the tick counter
        srand(xTaskGetTickCount());
        gRandSeed = true;
        InitializeScheduler();
    }

    SetTpsActiveForAutoCal();

    ret = CalculateTpsSchedule(gChargeVirtual);
    gTpsTimeout = false;
    gTpsDone = false;
    SetTpsDelay(gTpsStartDelay);

    TryToStartTimer(gTpsTimeoutTimerHandle);
    if (ret == COTA_ERROR_NONE)
    {
        ret = DoBuildSpecificTpsEntryProxyMessageSend();
    }

    POST_COTA_ERROR(ret);
}

/**
 * @brief Checks if clients should be scheduled.
 *
 * @return true if clients need to be scheduled.
 */
bool RdyToTpsCheck  (void)
{
    bool tpsAllowed = false;
    bool chargingNeeded = CmAnyChargeableClients();

    chargingNeeded = (chargingNeeded || gChargeVirtual);

    if (chargingNeeded)
    {
        SetChargingBeginForAutoCal();
        EnableSystemClock(true);

        //Because we are just starting to charge, we should increment
        //the tps seq num to force a TPS message to all the receivers.
        //If we don't do this, and we charge the exact same clients,
        //we won't send a TPS and some of the clients, (which may have
        //rebooted for any number of reasons) would not know the TPS
        //schedule.
        IncrementTpsSeqNum();
    }
    else
    {
        DisableSystemClockIfClockIdleIsSet();
    }

    // This function changes depending on config_system.h
    tpsAllowed = CheckImplementationSpecificTpsPrerequisites(chargingNeeded);

    return (chargingNeeded && tpsAllowed);
}

/**
 * @brief Checks to see if the TPS cycle is done, so the state machine transitions back to ready.
 */
bool TpsDoneToRdyCheck(void)
{
    if (gTpsDone)
    {
        vTaskDelay( pdMS_TO_TICKS(gPostTpsClientPause));
    }

    return (gTpsDone && (!IsTpsFrozen()));
}

/**
 * @brief Checks to see if the TPS cycle has timed out, so the state machine transitions back to ready.
 */
bool TpsTimeoutToRdyCheck(void)
{
    return (gTpsTimeout && (!IsTpsFrozen()));
}

/**
 * @brief Warns a tps timeout has occurred.
 */
void TpsTimeoutToRdyExit(void)
{
    LogWarning("TPS Timeout \r\n");
}

/**
 * @brief The query acknowledgment timer is called after a query message is sent.
 *        If an acknowledgment message is not received before this timer expires, then
 *        the query message has timed out.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void tpsTimeoutTimerCallback(TimerHandle_t  timer)
{
    gTpsTimeout = true;
}

/**
 * @brief the tps interrupts need to call this to signal that tps is done
 */
void SignalTpsDone(void)
{
    gTpsDone = true;
}

/**
 * @brief Determines if TPS has timed out
 * @return true if TPS has timeout, false if not.
 */
bool IsTpsTimedOut(void)
{
    return gTpsTimeout;
}

#endif // defined(USE_COMMON_TPS)
