/**
 * @file       orion_config.c 
 *
 * @brief      This is a mutex protected configuration file for the Orion system. Any
 *             data saved within this file will be saved to the EEProm at shutdown.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */



/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////
#include "FreeRTOS.h"
#include "semphr.h"
#include "main.h"
#include "nvm_driver.h"
#include "proxyHostMsgInf.h"
#include "orion_config.h"
#include "error.h"
#include <string.h>


/////////////////////////////////////////////////////////////////////////////////////////////
// Private Defines 
/////////////////////////////////////////////////////////////////////////////////////////////

/**
 *  The number of power slots between two adjacent tone slots.
 *  Total power section is gPowerSlotDur * gPowerSlotCount
 **/
#define DEFAULT_POWER_SLOT_COUNT     1           

#define CONFIG_PARAMS_COUNT          (sizeof(CONFIG_INFO_DEFINITION) / sizeof(CONFIG_INFO_DEFINITION[0]))  ///< The size of the structure containing information about individual configuration fields


/////////////////////////////////////////////////////////////////////////////////////////////
// Private Type Definitions
/////////////////////////////////////////////////////////////////////////////////////////////

/** Structure defining the layout of the configration definition record */
typedef struct _ConfigDef_t
{
    ConfigParamId_t id;    ///< Item ID
    uint8_t*        addr;  ///< Item address
    uint8_t         size;  ///< Item size
} ConfigDef_t;


/////////////////////////////////////////////////////////////////////////////////////////////
// Private Variable Declarations
/////////////////////////////////////////////////////////////////////////////////////////////
static SemaphoreHandle_t gOrionConfigMutex = NULL;   ///< Mutex for limiting access to configuration values
static SysConfig_t       gSysConfig     = {0};       ///< Local copy of configuration data


/////////////////////////////////////////////////////////////////////////////////////////////
// Private Constant Definitions
/////////////////////////////////////////////////////////////////////////////////////////////

/** Array defining the layout of the configration structure fields */
static const ConfigDef_t CONFIG_INFO_DEFINITION[] =
{
    /**
     * Unique board ID
     */
    {
        .id = CFG_BOARD_ID,
        .addr = (uint8_t*)&gSysConfig.boardId,
        .size = sizeof(gSysConfig.boardId)
    },
    
    /**
     * Current version of this configuration file
     */
    {
        .id = CFG_CONFIG_FILE_VERSION,
        .addr = (uint8_t*)&gSysConfig.configFileVersion,
        .size = sizeof(gSysConfig.configFileVersion)
    },
    
    /**
     * ACL to be used for calibration
     */
    {
        .id = CFG_CALIBRATION_UVP,
        .addr = (uint8_t*)&gSysConfig.calibrationUvp,
        .size = sizeof(gSysConfig.calibrationUvp)
    },
    
    /**
     * AMB to be used for calibration
     */
    {
        .id = CFG_CALIBRATION_AMB,
        .addr = (uint8_t*)&gSysConfig.calibrationAmb,
        .size = sizeof(gSysConfig.calibrationAmb)
    },
    
    /**
     * AMU to be used for calibration
     */
    {
        .id = CFG_CALIBRATION_AMU,
        .addr = (uint8_t*)&gSysConfig.calibrationAmu,
        .size = sizeof(gSysConfig.calibrationAmu)
    },
    
    /**
     * ACL to be used as reference during calibration
     */
    {
        .id = CFG_REFERENCE_UVP,
        .addr = (uint8_t*)&gSysConfig.referenceUvp,
        .size = sizeof(gSysConfig.referenceUvp)
    },
    
    /**
     * AMB to be used as reference during calibration
     */
    {
        .id = CFG_REFERENCE_AMB,
        .addr = (uint8_t*)&gSysConfig.referenceAmb,
        .size = sizeof(gSysConfig.referenceAmb)
    },
    
    /**
     * AMU to be used as reference during calibration
     */
    {
        .id = CFG_REFERENCE_AMU,
        .addr = (uint8_t*)&gSysConfig.referenceAmu,
        .size = sizeof(gSysConfig.referenceAmu)
    },
    
    /**
     * Indicates how often should auto calibration run (0 ->
     * auto-calibration is disabled)
     */
    {
        .id = CFG_AUTO_CALIBRATION_PERIOD,
        .addr = (uint8_t*)&gSysConfig.autoCalibrationPeriod,
        .size = sizeof(gSysConfig.autoCalibrationPeriod)
    },
    
    /**
     * The temperatures are sampled with this period in seconds
     */
    {
        .id = CFG_CAL_TEMP_SMPL_PERIOD,
        .addr = (uint8_t*)&gSysConfig.calTempSmplPeriod,
        .size = sizeof(gSysConfig.calTempSmplPeriod)
    },
    
    /**
     * A calibration will be initiated if any thermometer on the transmitter
     * exceeds this value in degrees C
     */
    {
        .id = CFG_CAL_TEMP_THRESHOLD,
        .addr = (uint8_t*)&gSysConfig.calTempThreshold,
        .size = sizeof(gSysConfig.calTempThreshold)
    },
    
    /**
     * If a client's battery charge falls below this level, it will be
     * given greater priority during charging
     */
    {
        .id = CFG_CHG_PRIORITY_THRESHOLD,
        .addr = (uint8_t*)&gSysConfig.clientChargePriorityThreshold,
        .size = sizeof(gSysConfig.clientChargePriorityThreshold)
    },
    
    /**
     * IEEE 802.15.4 communication channel (11 - 26) to be used to
     * communicate with clients
     */
    {
        .id = CFG_COMM_CHANNEL,
        .addr = (uint8_t*)&gSysConfig.commChannel,
        .size = sizeof(gSysConfig.commChannel)
    },
    
    /**
     *  The maximum number of queries to send at one time
     */
    {
        .id = CFG_MAX_QUERIES,
        .addr = (uint8_t*)&gSysConfig.maxQueries,
        .size = sizeof(gSysConfig.maxQueries)
    },
    
    /**
     * The period in milliseconds for scheduling client queries for each
     * client
     */
    {
        .id = CFG_QUERY_PERIOD,
        .addr = (uint8_t*)&gSysConfig.queryPeriod,
        .size = sizeof(gSysConfig.queryPeriod)
    },
    
    /**
     * The time period in milliseconds after which the system stop
     * waiting for a query response from a client and move on to the
     * next TPS cycle
     */
    {
        .id = CFG_CLIENT_QUERY_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.clientQueryTimeout,
        .size = sizeof(gSysConfig.clientQueryTimeout)
    },
    
    /**
     * A registered client will be marked as "UNKNOWN" if it does
     * respond to any message after this many seconds
     */
    {
        .id = CFG_CLIENT_RESPONSE_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.clientResponseTimeout,
        .size = sizeof(gSysConfig.clientResponseTimeout)
    },
    
    /**
     * Critical system temperature in C used to stop power delivery
     * when the system is overheated
     */
    {
        .id = CFG_CRITICAL_TEMPERATURE,
        .addr = (uint8_t*)&gSysConfig.criticalTemperature,
        .size = sizeof(gSysConfig.criticalTemperature)
    },
    
    /**
     * 0 -> the system is running normally; non-zero for debug mode
     */
    {
        .id = CFG_DEBUG_MODE,
        .addr = (uint8_t*)&gSysConfig.debugMode,
        .size = sizeof(gSysConfig.debugMode)
    },
    
    /**
     * Client discovery period in seconds; a value of 0 indicates a
     * one-time client discovery
     */
    {
        .id = CFG_DISCOVERY_PERIOD,
        .addr = (uint8_t*)&gSysConfig.discoveryPeriod,
        .size = sizeof(gSysConfig.discoveryPeriod)
    },
    
    /**
     * An unregistered client will be removed from the active list if
     * it does not respond to discovery messages after this many
     * seconds
     */
    {
        .id = CFG_DISCOVERY_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.discoveryTimeout,
        .size = sizeof(gSysConfig.discoveryTimeout)
    },
    
    /**
     * The time slot in milliseconds to wait for discovery message
     * responses from clients
     */
    {
        .id = CFG_DISCOVERY_WAIT,
        .addr = (uint8_t*)&gSysConfig.discoveryWait,
        .size = sizeof(gSysConfig.discoveryWait)
    },
    
    /**
     *  The period of time in seconds to check to see if join messages should be sent.
     */
    {
        .id = CFG_JOIN_PERIOD,
        .addr = (uint8_t*)&gSysConfig.joinPeriod,
        .size = sizeof(gSysConfig.joinPeriod)
    },

    /**
     *  The period of time in milliseconds to wait for a join message response
     */
    {
        .id = CFG_JOIN_WAIT,
        .addr = (uint8_t*)&gSysConfig.joinWait,
        .size = sizeof(gSysConfig.joinWait)
    },
    
    /**
     *  The period of time in seconds to check to see if tpc messages should be sent.
     */
    {
        .id = CFG_TPC_PERIOD,
        .addr = (uint8_t*)&gSysConfig.tpcPeriod,
        .size = sizeof(gSysConfig.tpcPeriod)
    },

    /**
     *  The period of time in milliseconds to wait for a tpc message response
     */
    {
        .id = CFG_TPC_WAIT,
        .addr = (uint8_t*)&gSysConfig.tpcWait,
        .size = sizeof(gSysConfig.tpcWait)
    },
    
    /**
     * The value of the ENCODE_SIZE register, which controls the Rx
     * frequency bandwidth in the UVP
     */
    {
        .id = CFG_UVP_ENCODE_SIZE,
        .addr = (uint8_t*)&gSysConfig.uvpEncodeSize,
        .size = sizeof(gSysConfig.uvpEncodeSize)
    },
    
    /**
     * Bitmask indicating which AMBs are marked as being used in the
     * system
     */
    {
        .id = CFG_VALID_AMB_MASK,
        .addr = (uint8_t*)&gSysConfig.goodAmbChannels,
        .size = sizeof(gSysConfig.goodAmbChannels)
    },

    /**
     * The number of ESL slots (0 -> LPM disabled)
     */
    {
        .id = CFG_LPM_SLOT_COUNT,
        .addr = (uint8_t*)&gSysConfig.lpmSlotCount,
        .size = sizeof(gSysConfig.lpmSlotCount)
    },

    /**
     * The number of consecutive failed client queries after which the
     * client is put into NOT_ACTIVE state, where it will need to go
     * through the join process
     */
    {
        .id = CFG_MAX_QUERY_FAIL_COUNT,
        .addr = (uint8_t*)&gSysConfig.maxQueryFailCount,
        .size = sizeof(gSysConfig.maxQueryFailCount)
    },
    
    /**
     * The unique PAN ID of the proxy network used for communication
     * with the clients
     */
    {
        .id = CFG_NETWORK_PAN_ID,
        .addr = (uint8_t*)&gSysConfig.networkPanId,
        .size = sizeof(gSysConfig.networkPanId)
    },
    
    /**
     * The first client short ID to be assigned to clients; this value
     * is incremented for each successive client that gets a short ID
     * assigned
     */
    {
        .id = CFG_STARTING_CLIENT_ID,
        .addr = (uint8_t*)&gSysConfig.startingClientId,
        .size = sizeof(gSysConfig.startingClientId)
    },
    
    /**
     * Short ID to be assigned to the proxy for communication with
     * clients
     */
    {
        .id = CFG_PROXY_ID,
        .addr = (uint8_t*)&gSysConfig.proxyId,
        .size = sizeof(gSysConfig.proxyId)
    },
    
    /**
     * Minimum phase change during recalibration that causes another
     * recalibration
     */
    {
        .id = CFG_OBSTRUCTION_ANGLE_THRESHOLD,
        .addr = (uint8_t*)&gSysConfig.obstructionAngleThreshold,
        .size = sizeof(gSysConfig.obstructionAngleThreshold)
    },
    
    /**
     * Period in seconds to adjust phases based on temperature
     */
    {
        .id = CFG_PHASE_ADJUSTMENT_PERIOD,
        .addr = (uint8_t*)&gSysConfig.phaseAdjustmentPeriod,
        .size = sizeof(gSysConfig.phaseAdjustmentPeriod)
    },
    
    /**
     * System power level in milli-dBm
     */
    {
        .id = CFG_POWER_LEVEL,
        .addr = (uint8_t*)&gSysConfig.powerLevel,
        .size = sizeof(gSysConfig.powerLevel)
    },
    
    /**
     * 0 = closed; 1 = open (anyone can join)
     */
    {
        .id = CFG_POWER_MODE,
        .addr = (uint8_t*)&gSysConfig.powerMode,
        .size = sizeof(gSysConfig.powerMode)
    },
    
    /**
     * Short calibration starts when charging starts and is performed
     * periodically for this amount of time in seconds (0 disables
     * recalibration)
     */
    {
        .id = CFG_SHORT_CALIBRATION_DURATION,
        .addr = (uint8_t*)&gSysConfig.shortCalibrationDuration,
        .size = sizeof(gSysConfig.shortCalibrationDuration)
    },
    
    /**
     * How often short calibration is performed, in seconds
     */
    {
        .id = CFG_SHORT_CALIBRATION_PERIOD,
        .addr = (uint8_t*)&gSysConfig.shortCalibrationPeriod,
        .size = sizeof(gSysConfig.shortCalibrationPeriod)
    },
    
    /**
     * Byte 0: Revision (variant); byte 1: Version (Transmitter/PAC); byte 2:
     * System type (Venus/Orion); byte 3: Reserved
     */
    {
        .id = CFG_SYSTEM_TYPE,
        .addr = (uint8_t*)&gSysConfig.systemType,
        .size = sizeof(gSysConfig.systemType)
    },
    
    /**
     * Number of beacon beats in a TPS
     */
    {
        .id = CFG_TPS_NUM_BEACON_BEATS,
        .addr = (uint8_t*)&gSysConfig.tpsNumBeaconBeats,
        .size = sizeof(gSysConfig.tpsNumBeaconBeats)
    },
    
    /**
     * Number of clients participating in a TPS cycle
     */
    {
        .id = CFG_TPS_NUM_CLIENTS,
        .addr = (uint8_t*)&gSysConfig.tpsNumClients,
        .size = sizeof(gSysConfig.tpsNumClients)
    },
    
    /**
     * Number of power slots between tone slots
     */
    {
        .id = CFG_TPS_NUM_POWER_SLOTS,
        .addr = (uint8_t*)&gSysConfig.tpsNumPowerSlots,
        .size = sizeof(gSysConfig.tpsNumPowerSlots)
    },
    
    /**
     * Number of power slots between tone slots per client
     */
    {
        .id = CFG_TPS_NUM_POWER_SLOTS_PER_CLIENT,
        .addr = (uint8_t*)&gSysConfig.tpsNumPowerSlotsPerClient,
        .size = sizeof(gSysConfig.tpsNumPowerSlotsPerClient)
    },
    
    /**
     * Power amplifier start-up time in microseconds (this is used at
     * the end of the tone slot to warm up the PAs before transmitting
     * power during the power slot)
     */
    {
        .id = CFG_TPS_PA_DELAY,
        .addr = (uint8_t*)&gSysConfig.tpsPaDelay,
        .size = sizeof(gSysConfig.tpsPaDelay)
    },
    
    /**
     * Delay between the start of the tone slot and the Rx phase
     * update pulse, in microseconds
     */
    {
        .id = CFG_TPS_RX_DELAY,
        .addr = (uint8_t*)&gSysConfig.tpsRxDelay,
        .size = sizeof(gSysConfig.tpsRxDelay)
    },
    
    /**
     * Tone slot duration in microseconds
     */
    {
        .id = CFG_TPS_RX_DURATION,
        .addr = (uint8_t*)&gSysConfig.tpsRxDuration,
        .size = sizeof(gSysConfig.tpsRxDuration)
    },
    
    /**
     * The delay between the TPS Go signal and the start of the first
     * tone slot in microseconds
     */
    {
        .id = CFG_TPS_START_DELAY,
        .addr = (uint8_t*)&gSysConfig.tpsStartDelay,
        .size = sizeof(gSysConfig.tpsStartDelay)
    },
    
    /**
     * Duration of a single power slot in microseconds
     */
    {
        .id = CFG_TPS_TX_DURATION,
        .addr = (uint8_t*)&gSysConfig.tpsTxDuration,
        .size = sizeof(gSysConfig.tpsTxDuration)
    },

    /**
     *  Timeout waiting for TPS cycle to complete    
     */
    {
        .id = CFG_TPS_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.tpsTimeout,
        .size = sizeof(gSysConfig.tpsTimeout)
    },
        
    /**
     * Power transmission frequency in MHz; 0 causes Tx frequency to
     * be set based on the comm channel
    */
    {
        .id = CFG_TX_FREQUENCY_MHZ,
        .addr = (uint8_t*)&gSysConfig.txFrequencyMhz,
        .size = sizeof(gSysConfig.txFrequencyMhz)
    },
    
    /**
     *  0 = clock to AMB's only, 1 = add TAM, 2 = add Master, 3 = add TAM and Master  4 = Slave 5 = Slave and Tam 
     */
    {
        .id = CFG_SYSTEM_CLOCK,
        .addr = (uint8_t*)&gSysConfig.systemClockConfig,
        .size = sizeof(gSysConfig.systemClockConfig)
    },
    
    /**
     *  A four bit mask indicating which fans are plugged in.
     */
    {
        .id = CFG_VALID_FANS,
        .addr = (uint8_t*)&gSysConfig.validFansMask,
        .size = sizeof(gSysConfig.validFansMask)
    },
    
    /**
     *  Time in milliseconds to wait before starting to allow client to clear pending commands.
     */
    {
        .id = CFG_PRE_TPS_CLIENT_PAUSE,
        .addr = (uint8_t*)&gSysConfig.preTpsClientPause,
        .size = sizeof(gSysConfig.preTpsClientPause)           
    },
    
    /**
     *  Time in milliseconds to wait after a TPS cycle to allow the client to start its radio
     */
    {
        .id = CFG_POST_TPS_CLIENT_PAUSE,
        .addr = (uint8_t*)&gSysConfig.postTpsClientPause,
        .size = sizeof(gSysConfig.postTpsClientPause)           
    },   
    
    /**
     *  After issuing a app command, the time to wait for a response in milliseconds
     */
    {
        .id = CFG_APP_COMMAND_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.appCmdTimeout,
        .size = sizeof(gSysConfig.appCmdTimeout)         
    },
    
    /**
     * The maximum app commands to send in a given app command slot.
     */
    {
        .id = CFG_APP_COMMAND_MAX_SEND,
        .addr = (uint8_t*)&gSysConfig.appCmdMaxSend,
        .size = sizeof(gSysConfig.appCmdMaxSend)         
    },
    
    /**
     *  The time to wait for client disconnect commands to respond in milliseconds.
     */
    {
        .id = CFG_DISC_CLIENT_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.discClientTimeout,
        .size = sizeof(gSysConfig.discClientTimeout)         
    },
    
    /**
     * The maximum number of client disconnect commands to send in a given client command slot.
     */
    {
        .id = CFG_DISC_CLIENT_MAX_SEND,
        .addr = (uint8_t*)&gSysConfig.discClientMaxSend,
        .size = sizeof(gSysConfig.discClientMaxSend)         
    },

    /**
     * The period in which we ensure that the light ring is set properly.
     */
    {
        .id = CFG_LIGHT_RING_UPDATE_PERIOD,
        .addr = (uint8_t*)&gSysConfig.lightRingUpdatePeriod,
        .size = sizeof(gSysConfig.lightRingUpdatePeriod)         
    },
    
    /**
     * The amplitude setting for the system clocks.
     */
    {
        .id = CFG_SYSCLK_AMPLITUDE,
        .addr = (uint8_t*)&gSysConfig.sysClkAmplitude,
        .size = sizeof(gSysConfig.sysClkAmplitude)         
    },
    
    /**
     * The common mode setting for the system clocks.
     */
    {
        .id = CFG_SYSCLK_COMMON_MODE,
        .addr = (uint8_t*)&gSysConfig.sysClkCommonMode,
        .size = sizeof(gSysConfig.sysClkCommonMode)         
    },
    
    /**
     * The format setting for the system clocks.
     */
    {
        .id = CFG_SYSCLK_FORMAT,
        .addr = (uint8_t*)&gSysConfig.sysClkFormat,
        .size = sizeof(gSysConfig.sysClkFormat)         
    },

    /**
     * The inversion settings for the AMB system clocks.
     */
    {
        .id = CFG_SYSCLK_AMB_INVERSION,
        .addr = (uint8_t*)&gSysConfig.sysClkAmbInversion,
        .size = sizeof(gSysConfig.sysClkAmbInversion)         
    },
    
    /**
     * If non-zero, the system clock will be disabled while the
     * transmitter in not charging.
     */
    {
        .id = CFG_SYSCLK_IDLE_DISABLE,
        .addr = (uint8_t*)&gSysConfig.sysClkIdleDisable,
        .size = sizeof(gSysConfig.sysClkIdleDisable)         
    },

    /**
     * The log level of the system. 
     */
    {
        .id = CFG_SYS_LOG_LEVEL,
        .addr = (uint8_t*)&gSysConfig.sysLogLevel,
        .size = sizeof(gSysConfig.sysLogLevel)         
    },
    
    /**
     * The maximum number of attempts to send an app command to a
     * client when the all argument is used.
     */
    {
        .id = CFG_APP_CMD_MAX_TRIES,
        .addr = (uint8_t*)&gSysConfig.appAllCmdMaxTries,
        .size = sizeof(gSysConfig.appAllCmdMaxTries)         
    },

    /**
     * The period in seconds to poll the proxy with the proxy info
     * command.  If the proxy doesn't respond, the proxy is reset.
     */
    {
        .id = CFG_PROXY_TEST_PERIOD,
        .addr = (uint8_t*)&gSysConfig.proxyTestPeriod,
        .size = sizeof(gSysConfig.proxyTestPeriod)         
    },

    /**
     *  The maximum time in milliseconds to wait for a proxy info
     *  response.  If a proxy response does not arrive we assume the
     *  proxy has crashed and reset it.
     */
    {
        .id = CFG_PROXY_TEST_WAIT_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.proxyTestWaitTimeout,
        .size = sizeof(gSysConfig.proxyTestWaitTimeout)         
    },

    /**
     *  The period in milliseconds in which a door open message should
     *  be sent to the host.
     */
    {
        .id = CFG_DOOR_OPEN_MSG_PERIOD,
        .addr = (uint8_t*)&gSysConfig.doorOpenMsgPeriod,
        .size = sizeof(gSysConfig.doorOpenMsgPeriod)         
    },    

    /**
     *  The amount of time in milliseconds that the transmitter expects
     *  announcement messages.
     */
    {
        .id = CFG_RCVR_LONG_CONFIG_RESP_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.rcvrLongConfigRespTimeout,
        .size = sizeof(gSysConfig.rcvrLongConfigRespTimeout)         
    },

    /**
     *  The amount of time in seconds that the Xirgo boards will
     *  wait before sleeping
     */
    {
        .id = CFG_XIRGO_SLEEP_TIME,
        .addr = (uint8_t*)&gSysConfig.rcvrSleepTime,
        .size = sizeof(gSysConfig.rcvrSleepTime)         
    },
    
    /**
     *  The charging threshold for a receiver
     */
    {
        .id = CFG_RCVR_CHARGING_THRESHOLD,
        .addr = (uint8_t*)&gSysConfig.rcvrChargingThreshold,
        .size = sizeof(gSysConfig.rcvrChargingThreshold)         
    },

    /**
     *  The communication power for the proxy-receiver messages
     */
    {
        .id = CFG_RCVR_COMM_PWR,
        .addr = (uint8_t*)&gSysConfig.rcvrCommPower,
        .size = sizeof(gSysConfig.rcvrCommPower)         
    },

    /**
     *  The state that the Ethernet clock control GPIO shold be set to.
     */
    {
        .id = CFG_ETHERNET_CLOCK_STATE,
        .addr = (uint8_t*)&gSysConfig.ethernetClockState,
        .size = sizeof(gSysConfig.ethernetClockState)         
    },

    /**
     *  The minimum hysteresis charge value for fatally low receivers
     */
    {
        .id = CFG_FATAL_LOW_HIST_MIN,
        .addr = (uint8_t*)&gSysConfig.ftTpsFatalLowHistMin,
        .size = sizeof(gSysConfig.ftTpsFatalLowHistMin)         
    },

    /**
     *  The maximum hysteresis charge value for fatally low receivers
     */
    {
        .id = CFG_FATAL_LOW_HIST_MAX,
        .addr = (uint8_t*)&gSysConfig.ftTpsFatalLowHistMax,
        .size = sizeof(gSysConfig.ftTpsFatalLowHistMax)         
    },

    /**
     *  The number of fatally low receivers allowed to receive charge
     */
    {
        .id = CFG_MAX_NUM_OF_FATAL_LOW_RCVRS,
        .addr = (uint8_t*)&gSysConfig.ftTpsMaxNumFatalLowRcvr,
        .size = sizeof(gSysConfig.ftTpsMaxNumFatalLowRcvr)         
    },

    /**
     *  The maximum time in seconds to wait for receivers to indicate
     *  sleep before beginning to charge
     */
    {
        .id = CFG_WAIT_FOR_SLEEP_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.ftWaitForSleepPeriodTimeout,
        .size = sizeof(gSysConfig.ftWaitForSleepPeriodTimeout)         
    },

    /**
     * The period in milliseconds where we will remove receivers from the database
     */
    {
        .id = CFG_RECEIVER_REMOVAL_PERIOD,
        .addr = (uint8_t*)&gSysConfig.ftReceiverRemovalPeriod,
        .size = sizeof(gSysConfig.ftReceiverRemovalPeriod)
    },

    /**
     *  The amount of time in milliseconds that the transmitter expects
     *  announcement messages during "re-enumeration" (after each short query cycle).
     */
    {
        .id = CFG_RCVR_SHORT_CONFIG_RESP_TIMEOUT,
        .addr = (uint8_t*)&gSysConfig.ftShortConfigRespTimeout,
        .size = sizeof(gSysConfig.ftShortConfigRespTimeout)         
    },

    /**
     *  The amount of time in milliseconds that the proxy will delay before
     *  acknowledging and forwarding another announcement message.
     */
    {
        .id = CFG_PROXY_ANNOUNCEMENT_DELAY,
        .addr = (uint8_t*)&gSysConfig.ftProxyAnnouncementDelayMs,
        .size = sizeof(gSysConfig.ftProxyAnnouncementDelayMs)         
    },

    /**
     *  The minimum value of RSSI (in dBm) that will allow an
     *  announcement message to be acted upon.
     */
    {
        .id = CFG_PROXY_RSSI_FILTER_MIN_VAL,
        .addr = (uint8_t*)&gSysConfig.ftProxyRssiFilterMinValue,
        .size = sizeof(gSysConfig.ftProxyRssiFilterMinValue)         
    },
};

/////////////////////////////////////////////////////////////////////////////////////////////
// Functions 
/////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Initializes all resources for the orion config module
 */
void CfgInit(void)
{
    // Create a mutex for limiting access to configuration to one user at a time
    gOrionConfigMutex = xSemaphoreCreateMutex();
    
    while (gOrionConfigMutex == NULL);
}

// This is defined in one of the build-specific config files. See
// `/Demo/demo_config.h` for an implementation example.
extern SysConfig_t gDefaultSystemConfig;

/**
 * @brief Returns the default system configuration for your current build.
 *
 *        Note, the configuration variable gDefaultSystemConfig will
 *        change depending on the build of the compile. 
 */
SysConfig_t GetDefaultSystemConfig(void)
{
    return gDefaultSystemConfig;
}

/**
 * @brief  Reads a given parameter from NVM
 *
 * @param  paramId   ID of the parameter to read
 * @param  paramVal  Pointer to the buffer of sufficient size to hold the value of the parameter
 * @param  size      The size of the parameter passed in by the caller
 *
 * @return COTA_ERROR_NONE if the parameter was found, COTA_ERROR_CFG_PARAM_NOT_FOUND otherwise
 */
CotaError_t CfgGetParam(ConfigParamId_t paramId, void * paramVal, uint8_t size)
{
    CotaError_t retVal = COTA_ERROR_CFG_PARAM_NOT_FOUND;

    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);
    
    // Get data from NVM
    (void)NvmGetConfigData(&gSysConfig);

    // Find the parameter we're looking for and copy it to the caller's buffer
    for (uint8_t i = 0; i < CONFIG_PARAMS_COUNT; i++)
    {
        if (CONFIG_INFO_DEFINITION[i].id == paramId)
        {
            memcpy(paramVal, CONFIG_INFO_DEFINITION[i].addr, MIN(size, CONFIG_INFO_DEFINITION[i].size));
            retVal = COTA_ERROR_NONE;
            break;
        }
    }

    xSemaphoreGive(gOrionConfigMutex);
    
    return retVal;
}


/**
 * @brief  Saves a given parameter to NVM
 *
 * @param  paramId   ID of the parameter to read
 * @param  paramVal  Pointer to the buffer of sufficient size to hold the value of the parameter
 * @param  size      The size of the parameter passed in by the caller
 *
 * @return COTA_ERROR_NONE if the parameter was found, COTA_ERROR_CFG_PARAM_NOT_FOUND otherwise
 */
CotaError_t CfgSaveParam(ConfigParamId_t paramId, void * paramVal, uint8_t size)
{
    CotaError_t retVal = COTA_ERROR_CFG_PARAM_NOT_FOUND;
    uint8_t idx;   // Index of the parameter in the parameter definition array

    // Find the index of the parameter we're looking for
    for (idx = 0; idx < CONFIG_PARAMS_COUNT; idx++)
    {
        if (CONFIG_INFO_DEFINITION[idx].id == paramId)
        {
            retVal = COTA_ERROR_NONE;
            break;
        }
    }
    
    if (retVal == COTA_ERROR_NONE)
    {
        xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);
        
        // Get configuration data from NVM
        (void)NvmGetConfigData(&gSysConfig);
     
        memcpy(CONFIG_INFO_DEFINITION[idx].addr, paramVal, MIN(size, CONFIG_INFO_DEFINITION[idx].size));
        
        // Write configuration data back to NVM
        retVal = NvmSaveConfigData(&gSysConfig);
    }

    xSemaphoreGive(gOrionConfigMutex);
    
    return retVal;
}


// This is defined in one of the build-specific config files. See
// `/Demo/demo_config.h` for an implementation example.
extern BuildType_t gSystemBuildType;

/**
 * @brief Returns the build type for your current build.
 *
 *        Note, the build type variable gSystemBuildType will
 *        change depending on the build of the compile. 
 */
 
BuildType_t GetBuildType(void) 
{
  return gSystemBuildType;
}

