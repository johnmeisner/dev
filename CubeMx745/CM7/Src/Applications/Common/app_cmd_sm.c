/**
 * @file       app_cmd_sm.c
 *
 * @brief      Defines the Application Command state machine functions for common use.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "app_cmd_sm_helper.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"
#include "proxyHostMsgInf.h"
#include "variable_queue.h"
#include "sysclk_driver.h"
#include "calibrator.h"

#if defined(USE_DEMO_BUILD)
    #include "Demo/demo_client_interface.h"
#elif defined(USE_FOREVER_TRACKER_BUILD)
    #include "ForeverTracker/ft_client_interface.h"
#endif

#if defined(USE_COMMON_APP_COMMAND)

#define FIRST_APP_CMD_ALL_CANDIDATE  1                                                                           ///< Used to indicate that we all looking for the first app command all candidate
#define CLIENT_COMMAND_HEADER_SIZE   ((uint16_t)((uint8_t*)&gAppCmdWorking.payload - (uint8_t*)&gAppCmdWorking)) ///< The size of a client command minus its payload
/**
 *  @brief A full sized client commands can be as large 264 bytes, but most commands
 *         are much smaller (as small as 9 bytes).  Let's 
 *         make sure the client command buffer can support at least one huge commands,
 *         and a few small commands
 */
#define CLIENT_COMMAND_QUEUE_SIZE    (sizeof(PrxHostClientCmdMsg_t) + 50)   ///< The size of the buffer used by the client buffer

static PrxHostClientCmdMsg_t gAppCmdWorking;                                                                     ///< A global used to hold a client command and keep pressure off the stack.
TimerHandle_t  gAppCmdTimeoutTimerHandle             = NULL;                                                     ///< FreeRTOS timer handle for the client commands timeout timer
StaticTimer_t  gAppCmdTimeoutTimerBuffer;                                                                        ///< FreeRTOS timer buffer for the client commands timeout timer

static uint8_t gAppCmdQueueStorageArea[CLIENT_COMMAND_QUEUE_SIZE];                                               ///< Storage area used by the variable message queue storing client command
bool gAppCmdAllActive           = false;                                                                         ///< A flag saying when we are sending app commands to all receivers.
bool gAppCmdTimeout             = false;                                                                         ///< Indicates the client command timeout has occurred
PrxHostClientCmdMsg_t gAppCmdAll;                                                                                 ///< When sending a app command to all receivers we store the command here;
VariableQueueControl_t gAppCmdQueue;                                                                             ///< Handle to queue storing client commands
bool gCommChanUpdateInProgress  = false;       ///< When using the set comm channel commmand, this indicates we are in the process of changing a comm channels.


static void clientCmdTimeoutTimerCallback(TimerHandle_t  timer);

bool IsAppCmdActive(void)
{
    return gAppCmdAllActive;
}

CotaError_t SendAppCmd(ExtdAddr_t longId, PrxHostClientCmdMsg_t command)
{
    CotaError_t err = COTA_ERROR_NONE;
    uint8_t maxTries = 0;
    uint8_t appCmdSize = sizeof(ExtdAddr_t) + sizeof(uint8_t);
    
    if (IS_ALL_CLIENT(longId))
    {
        //Here we receive an app command with the
        //'all' argument.  In this secrenerio, we save
        //off the command to a global and mark all
        //registered receivers in the client manager
        //with a number of tries of 3.  The state
        //machine will then dribble command out to all
        //the clients that are marked.  All other app
        //command will return until this command has
        //been full processed
                      
        gAppCmdAllActive = true;  //locks out all app commands until this command is fully processed
        gAppCmdAll =  command;  //copies the command to a globals
        CfgGetParam(CFG_APP_CMD_MAX_TRIES, &maxTries, sizeof(maxTries));
        CmSetAppCmdTriesLeftAll(maxTries);  //Marks all registered receivers with a try number which will be decremented after each try.
    }
    else
    {
        //Here we are sending a app command to just
        //one receiver.  Because sending a client
        //command enables communication transmission,
        //we want to send client commands between TPS
        //cycles, so we queue them up to wait for an
        //opportune time.  This will NOT lock out
        //additional app commands.
        appCmdSize += command.payloadSize;
        if (VariableQueueSend( &gAppCmdQueue,
                               (uint8_t*)&command,
                               appCmdSize) != appCmdSize)
        {
            LogError("Failed to queue client command\r\n");
            err = COTA_ERROR_FAILED_TO_SEND_TO_VAR_QUEUE;
        }
        else
        {
            LogInfo("Queued\r\n");
        }
    }

    return err;
}


CotaError_t SetCommCh(ExtdAddr_t longId, uint8_t channel)
{
    CotaError_t err      = COTA_ERROR_NONE;
    uint8_t     maxTries = 0;
    //If the user has specified a long id or all 
    //with the set_comm_channel, we need to
    //change the com channel of the receiver(s)
    //before we change the comm channel of the transmitter
                      
    // To change receiver comm channels we need issue
    // app commands; hence the logic here is very
    // similar to the APP_COMMAND logic below
    gCommChanUpdateInProgress = true;
                        
    if (!gAppCmdAllActive)
    {
        gAppCmdAllActive = true;  //Lock out all other commands
        gAppCmdAll.extAddr = ALL_CLIENT_LONGID;   
        gAppCmdAll.payloadSize = APP_SET_COMM_CHANNEL_SIZE;
        gAppCmdAll.payload.bytes[0] = APP_SET_COMM_CHANNEL_ID;
        gAppCmdAll.payload.bytes[1] = channel;
        CfgGetParam(CFG_APP_CMD_MAX_TRIES, &maxTries, sizeof(maxTries));
        if (IS_ALL_CLIENT(longId))
        {
            CmSetAppCmdTriesLeftAll(maxTries);
                              
        }
        else
        {
            err = CmSetAppCmdTriesLeft(longId,  maxTries);                                         
        }
    }
    else
    {
        //Since we have to issue app commands, we cannot call set_comm_channel 
        //with long id's when another app_command 'all' is beeing processed
        err= COTA_ERROR_APP_CMD_BUSY;  
    }

    return err;
}

void AppCmdStateInit(void)
{
    CotaError_t err;
    uint16_t timerVal;
    
    err = VariableQueueCreateStatic(sizeof(gAppCmdQueueStorageArea),
                                    gAppCmdQueueStorageArea,
                                    &gAppCmdQueue );

    while (err != COTA_ERROR_NONE);

    CfgGetParam(CFG_APP_COMMAND_TIMEOUT, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gAppCmdTimeoutTimerHandle)
        {
            xTimerStop(gAppCmdTimeoutTimerHandle, NO_WAIT);
            xTimerChangePeriod(gAppCmdTimeoutTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gAppCmdTimeoutTimerHandle = xTimerCreateStatic(
                "Client command timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                clientCmdTimeoutTimerCallback,
                &gAppCmdTimeoutTimerBuffer);
        }
    }
    else
    {
        gAppCmdTimeoutTimerHandle = NULL;
    }

    if (!gAppCmdTimeoutTimerHandle)
    {
        LogFatal("App Command timers failed to initialize\r\n");
    }
}

void AppCmdEntry(void)
{
    uint8_t maxAppCmds = 0;                       // The mamximum number of app commands to send at one time, as read from the nvm configuration   
    uint16_t appCmdSize = sizeof(gAppCmdWorking); // Indicates the size of an app command received from the variable queue
    CotaError_t err = COTA_ERROR_NONE;            // err value
    ExtdAddr_t longId;                            // long id of a particular of a receiver
    uint8_t appCmdTriesLeft = 0;                  // Indicates the number of attempts to send an app command left for a particular receiver
    uint16_t desiredNum = 1;                      // The desired receiver to use when process 'all' argument app commands.
    
    gAppCmdTimeout = false;                       // Set timer flag to false

    LogDebug("The Control task has entered the App. command state.\r\n");
    
    TryToStartTimer(gAppCmdTimeoutTimerHandle); 
    
    CfgGetParam(CFG_APP_COMMAND_MAX_SEND, &maxAppCmds, sizeof(maxAppCmds));
 
    if (gAppCmdAllActive)
    {
        // This is where the 'app_command all args' command is handled.
        // In this secenerio, all receivers in the client manager
        // are assigned a try count.  The command is stored globably in gAppCmdAll.
        // The goal is to dribble out commands until the try count for all the receivers
        // is zero.  After every send, we descrement the number of tries left. 
        // If a receiver confirms it received the app command, then number of tries left is set to zero.
        gAppCmdWorking = gAppCmdAll;
        
        // This section is called after all app_commands are processed.
        if (!CmGetAppCmdAllCandidate(&longId, &appCmdTriesLeft, FIRST_APP_CMD_ALL_CANDIDATE))
        {            
            gAppCmdAllActive = false;  //This allows other app_commands to be issued by the user
            
            if (gCommChanUpdateInProgress) //This indicates that we were sending a change to the com channel, we now need to change the transmitters com channel
            {
                uint8_t commChannel;
                commChannel = PrxExtractCommChannelFromAppCommand(&gAppCmdWorking);
                if ( CfgSaveParam(CFG_COMM_CHANNEL,
                   &commChannel,
                   sizeof(commChannel)) != COTA_ERROR_NONE)
                {
                    LogError("Failed to set transmitter comm channel\r\n");
                }
                else
                {
                    LogInfo("Finished sending to receivers. Set transmitter comm channel to %d\r\n",
                            commChannel);
                    UpdateTransmitFrequency();
                    SetManualCalibrationFlag(true);
            
                }
                gCommChanUpdateInProgress = false;   //This is set to zero to indicate that we are not longer changing com channels.
            }        
        }
        else
        {
            //This code sends maxAppCmds of qualified canidates that are marked with try counts.
            while ((maxAppCmds > 0) && CmGetAppCmdAllCandidate(&longId, &appCmdTriesLeft, desiredNum))
            {
                maxAppCmds--; 
                gAppCmdWorking.extAddr = longId;
                err = PrxSendAppCommand(&gAppCmdWorking);
                appCmdTriesLeft--;
                CmSetAppCmdTriesLeft(longId, appCmdTriesLeft);
                if (appCmdTriesLeft != 0)
                {
                    //We only need to increment the dersiredNum if the try count of the current candidate is above 1.
                    //If the try count is 0, the CmGetAppCmdAllCandidate will return a different candidate on the next call regardles.
                    desiredNum++;  
                }
                LogDebug("Sending command to 0x%llx size=%d first byte 0x%02x\r\n", 
                        *(uint64_t*)gAppCmdWorking.extAddr.bytes,
                        gAppCmdWorking.payloadSize,
                        gAppCmdWorking.payload.bytes[0]);
            }
        }
    }
    else
    {   
        //This section processes app commands that are sent to individual receivers.  It plucks the command out of a variable queue
        //and then sends it.  But it only sends maxAppCmds at any single time.
        while ((maxAppCmds > 0) && (appCmdSize > 0))
        {
            appCmdSize = VariableQueueReceive(&gAppCmdQueue, (uint8_t*)&gAppCmdWorking, sizeof(gAppCmdWorking), CTRL_NO_DELAY );
            maxAppCmds--;
            if (appCmdSize > CLIENT_COMMAND_HEADER_SIZE)
            {            
                err = PrxSendAppCommand(&gAppCmdWorking);                
            /// @todo We may want to send a response to the RPI here
            }   
        }
    }
    
    if (err != COTA_ERROR_NONE)
    {
        LogError("Failed to send command to 0x%llx size=%d first byte 0x%02x\r\n", 
                 *(uint64_t*)&gAppCmdWorking.extAddr,
                 gAppCmdWorking.payloadSize,
                 gAppCmdWorking.payload.bytes[0]);
    }
}

bool RdyToAppCmdCheck(void)
{
    return ( gAppCmdAllActive || (!IsVariableQueryEmpty(&gAppCmdQueue)) ) && PrxIsReady() && CheckIfAppCmdAllowed();
}
bool AppCmdToRdyCheck(void){ return gAppCmdTimeout; }

static void clientCmdTimeoutTimerCallback(TimerHandle_t  timer)
{
    gAppCmdTimeout = true;
}

#endif // defined(USE_COMMON_APP_COMMAND)
