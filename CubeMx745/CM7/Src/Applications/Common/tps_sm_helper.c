/**
 * @file       tps_sm_helper.c
 *
 * @brief      A helper module for the tps_sm module. This helps us eliminate
 *             duplicate code between multiple definitions in tps_sm.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "error.h"
#include "door_driver.h"
#include "fan_driver.h"
#include "proxy_msg_interface.h"
#include "client.h"
#include "cli_task.h"
#include "debug_on.h"

#if defined(USE_FOREVER_TRACKER_BUILD)
    #include "tps_scheduler.h"
    #include "ForeverTracker/enumeration.h"
    #include "ForeverTracker/wait_for_sleep.h"
    #include "ForeverTracker/tps_charge_ready_check.h"
    #include "ForeverTracker/ft_client_interface.h"
    #include "ForeverTracker/receiver_removal_sm.h"
    #include "query_sm.h"
#endif

// These functions are provided by client_manager.c
PRIVATE void Walk(void (*func)(Client_t* client));

static bool checkFanEmergency(bool chargingNeeded);

#if defined(USE_DEMO_BUILD)

CotaError_t DoBuildSpecificTpsEntryProxyMessageSend(void)
{
    return PrxSendTps();
}

bool CheckImplementationSpecificTpsPrerequisites(bool chargingNeeded)
{
    bool fansOk = checkFanEmergency(chargingNeeded);

    return (chargingNeeded && fansOk && PrxIsReady());
}

#elif defined(USE_FOREVER_TRACKER_BUILD)

CotaError_t DoBuildSpecificTpsEntryProxyMessageSend(void)
{
    CotaError_t     err = COTA_ERROR_TPS_SCHEDULER_FAILED;
    CotaRcvrGo_t    msg;

    TonePowerSched_t* tps = GetTpsSchedule();
    if (tps->numClients > 0)
    {
        // @todo: check: is the first element in the table always the one about to receive charge?
        msg.shortAddr = tps->clientTable[0].shortId;
        err = PrxSendGo(&msg);
    }
    return err;
}

static bool checkDoorClosed(void);

bool CheckImplementationSpecificTpsPrerequisites(bool chargingNeeded)
{
    bool fansOk = checkFanEmergency(chargingNeeded);
    bool isDoorClosed = checkDoorClosed();
    bool chargingAllowed = AllRxReadyToCharge();

    //FT Only:
    //@todo this code can be moved, since it does not affect the return value
    //note that AllRxReadyToCharge() walks the entire tree
    if (chargingAllowed &&
        !CheckIfEnumerationPeriodActive())
    {
        StopWaitForSleepPeriod();
DBG8_SET(); //trigger logic probe: "All receivers are ready for charging"
    }
    else
    {
        // When we are not ready to charge, we will not send out any
        // GO messages. As a result, we will have to enable fast
        // querying or else the receiver's watchdog timer will
        // fire and the receiver will wake up.
        SetFastQuery(true);
DBG8_RESET();
    }

    //FT Only: If the sleep period has expired, check for non-sleeping receivers and
    //remove them to unblock charging
    //@todo this code can be relocated to a system monitor function
    if (IsDoorClosed() && !CheckIfWaitForSleepPeriodActive())
    {
        if (CheckIfSleepWaitPeriodExpiredNotification())
        {
            //@todo re-evaluate for periodic checking
            MarkSleepingReceiversForRemoval();
            TryToRemoveBadReceivers();
        }
    }


    return (chargingAllowed &&
            chargingNeeded &&
            fansOk &&
            PrxIsReady() &&
            isDoorClosed &&
            !CheckIfEnumerationPeriodActive());
}

/**
 * @brief Returns the state of the door once the door state has changed.
 */
static bool checkDoorClosed(void)
{
    static bool doorStateClosedCache = false;

    if (doorStateClosedCache != IsDoorClosed())
    {
        doorStateClosedCache = IsDoorClosed();
    }

    return doorStateClosedCache;
}
#endif

static bool checkFanEmergency(bool chargingNeeded)
{
    static bool detectFanEmergency = false;
    bool fansOk = FansOk();

    if (chargingNeeded && (!fansOk))
    {
        if (detectFanEmergency == false)
        {
            LogWarning("TPS Stopped because of fan emergency\r\n");
            detectFanEmergency = true;
        }
    }
    else if (fansOk)
    {
        if (detectFanEmergency == true)
        {
            LogInfo("TPS Resumed: Fan emergency over.\r\n");
            detectFanEmergency = false;
        }
    }

    return fansOk;
}
