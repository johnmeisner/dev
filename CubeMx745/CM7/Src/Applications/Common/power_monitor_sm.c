/**
 * @file       power_monitor_sm.c
 *
 * @brief      Defines the Power Monitor state machine functions for the common system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "client_interface.h"
#include "power_monitor.h"


#if defined(USE_COMMON_POWER_MONITOR)

#define PM_PERIOD_MS 5000 ///< time in milliseconds before the next power monitor query is sent

TimerHandle_t  gPmTimeoutTimerHandle = NULL;  ///< FreeRTOS timer handle for the pm timeout timer
StaticTimer_t  gPmTimeoutTimerBuffer;         ///< FreeRTOS timer buffer for the pm timeout timer

volatile bool gPmUpdate         = false;       ///< if true, the power manager timeout was occurred
uint8_t gPmControl              = 0;           ///< Bit field that controls pm state.

static void pmTimeoutTimerCallback(TimerHandle_t  timer);

void PowerMonitorStateInit(void)
{
    if (gPmTimeoutTimerHandle)
    {
        xTimerStop(gPmTimeoutTimerHandle, NO_WAIT);
        xTimerChangePeriod(gPmTimeoutTimerHandle, pdMS_TO_TICKS(PM_PERIOD_MS), NO_WAIT);        
    }
    else
    {
        gPmTimeoutTimerHandle = xTimerCreateStatic(
            "Pm Timer",
            pdMS_TO_TICKS(PM_PERIOD_MS),
            pdFALSE,
            (void *) 0,
            pmTimeoutTimerCallback,
            &gPmTimeoutTimerBuffer);
    }

    if (!gPmTimeoutTimerHandle)
    {
        LogFatal("Power Monitor timers failed to initialize\r\n");
    }
}

void SetPmControl(uint8_t pmControl)
{
    gPmControl = pmControl;
}
uint8_t GetPmControl(void)
{
    return gPmControl;
}

void PmEntry(void)
{
    LogInfo("The Control task has entered the power monitor task\r\n");

    //Check the update flag first, so we don't update power values immediately after a refresh.
    if (gPmUpdate)  
    {

        TryToStartTimer(gPmTimeoutTimerHandle);

        gPmUpdate = false;
    }
    
    if (gPmControl & PM_REFRESH)
    {
        TryToStopTimer(gPmTimeoutTimerHandle);

        RefreshPowerMonitors();

        TryToStartTimer(gPmTimeoutTimerHandle);

        gPmControl &= (~PM_REFRESH);
    }
}

bool RdyToPmCheck(void)
{ 
    return (gPmControl & PM_ENABLED) && ((gPmControl & PM_REFRESH) || gPmUpdate); 
}
bool PmToRdyCheck (void)
{
    return true;
}

/**
 * @brief The power monitor update timer callback 
 * 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void pmTimeoutTimerCallback(TimerHandle_t  timer)
{
    gPmUpdate = true;
}

#endif // defined(USE_COMMON_POWER_MONITOR)
