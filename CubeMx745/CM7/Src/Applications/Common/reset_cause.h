/**
 * @file       reset_cause.c
 *
 * @brief      Header file for the reset cause module
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _RESET_CAUSE_H
#define _RESET_CAUSE_H

/**
 * @brief  Possible STM32 system reset causes 
 */
typedef enum _ResetCause_t
{
    RESET_CAUSE_UNKNOWN = 0,
    RESET_CAUSE_LOW_POWER_RESET,
    RESET_CAUSE_WINDOW_WATCHDOG_RESET,
    RESET_CAUSE_INDEPENDENT_WATCHDOG_RESET,
    RESET_CAUSE_SOFTWARE_RESET,
    RESET_CAUSE_POWER_ON_POWER_DOWN_RESET,
    RESET_CAUSE_EXTERNAL_RESET_PIN_RESET,
    RESET_CAUSE_BROWNOUT_RESET,
} ResetCause_t;

ResetCause_t ResetCauseGetType(void);
void ResetCauseGetFromSystem(void);
const char * ResetCauseGetName(ResetCause_t causeType);

#endif /* _RESET_CAUSE_H */
