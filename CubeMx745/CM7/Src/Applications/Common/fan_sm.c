/**
 * @file       fan_sm.c
 *
 * @brief      Defines the Fan control state machine functions for the common system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"
#include "fan_driver.h"
#include "uvp_driver.h"


#if defined(USE_COMMON_FAN)

TimerHandle_t  gFansTimeoutTimerHandle               = NULL;     ///< FreeRTOS timer handle for the fans check timeout timer
StaticTimer_t  gFansTimeoutTimerBuffer;                          ///< FreeRTOS timer buffer for the fans check timeout timer

volatile bool gFansCheck        = false;       ///< if true, the fan check timer has timed out.

static void fansTimeoutTimerCallback(TimerHandle_t  timer);

void CheckFans(bool check)
{
    gFansCheck = check;
}

void FansStateInit(void)
{
    if (gFansTimeoutTimerHandle)
    {
        xTimerStop(gFansTimeoutTimerHandle, NO_WAIT);
        xTimerChangePeriod(gFansTimeoutTimerHandle, pdMS_TO_TICKS(FAN_CHECK_PERIOD_MS), NO_WAIT);   
    }
    else
    {
        gFansTimeoutTimerHandle = xTimerCreateStatic(
            "Fan Timer",
            pdMS_TO_TICKS(FAN_CHECK_PERIOD_MS),
            pdFALSE,
            (void *) 0,
            fansTimeoutTimerCallback,
            &gFansTimeoutTimerBuffer);    
    }
    
    if (!gFansTimeoutTimerHandle)
    {
        LogFatal("Fan timers failed to initialize\r\n");
    }
}

void FansEntry(void)
{
    LogDebug("The Control task has entered the fan control state.\r\n");
   
    UpdateUvpTemperatureCache(NULL, false);  //Update the temperature in the cache
    UpdateFansOkMaskAndSpeed();
    gFansCheck = false;
        
    TryToStartTimer(gFansTimeoutTimerHandle);
}

bool StartFanTimers(void)
{
    return xTimerStart( gFansTimeoutTimerHandle, NO_WAIT ) == pdPASS;
}

bool RdyToFansCheck(void) { return gFansCheck; }
bool FansToRdyCheck(void) { return true; }

static void fansTimeoutTimerCallback(TimerHandle_t  timer)
{
    gFansCheck = true;
}

#endif // defined(USE_COMMON_FAN)
