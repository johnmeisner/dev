/**
 * @file       app_cmd_sm.h
 *
 * @brief      Header module for the application command state machine modules
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_COMMON_APP_COMMAND)

#ifndef _APP_CMD_SM_H_
#define _APP_CMD_SM_H_

void AppCmdStateInit(void);
bool IsAppCmdActive(void);
CotaError_t SendAppCmd(ExtdAddr_t longId, PrxHostClientCmdMsg_t command);
CotaError_t SetCommCh(ExtdAddr_t longId, uint8_t channel);

#endif /* _APP_CMD_SM_H_ */

#endif // defined(USE_COMMON_APP_COMMAND)
