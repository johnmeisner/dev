/****************************************************************************//**
* @file      orion_config.h
*
* @brief     Header file contain general configuration settings for the application
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.)
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _ORION_CONFIG_H_
#define _ORION_CONFIG_H_

#include "proxyHostMsgInf.h"
#include "error.h"
#include "CotaCompiler.h"
#include "log.h"
#include "main.h"
#include "config_system.h"


extern IWDG_HandleTypeDef hiwdg1;

/*
 * Cota Transmitter Task Definitions
 */

// The lower the priority number, the lower the priority of the task. The IDLE Task has priority 0

#define ERROR_HANDLER_STACK_SIZE                512         ///< Stack size for the error task in 32-bit words
#define ERROR_HANDLER_TASK_PRIORITY             1           ///< Priority of the error task

// Software Timer Task is Priority 2

#define CLI_TASK_STACK_SIZE                     512         ///< Stack size for the CLI task in 32-bit words
#define CLI_TASK_TASK_PRIORITY                  4           ///< Priority of the CLI task

#define CLI_SEND_TASK_STACK_SIZE                1024        ///< Uart driver task stack size in 32-bit words
#define CLI_SEND_TASK_PRIORITY                  5           ///< Uart driver task priority

#define FAN_DRIVER_TASK_STACK_SIZE              256         ///< Fan driver task stack size in 32-bit words
#define FAN_DRIVER_TASK_PRIORITY                6           ///< Fan driver task priority

#define RPI_TASK_STACK_SIZE                     256         ///< Raspberry Pi task stack size in 32-bit words
#define RPI_TASK_PRIORITY                       7           ///< Raspberry Pi task priority

#define PROXY_TASK_STACK_SIZE                   256         ///< Proxy task stack size in 32-bit words
#define PROXY_TASK_PRIORITY                     8           ///< Proxy task priority

#define GATHER_AND_VOTE_STACK_SIZE              500         ///< Stack size for the gather and vote task in 32-bit words
#define GATHER_AND_VOTE_PRIORITY                9           ///< Priority of the gather and vote Task

#define CTRL_TASK_STACK_SIZE                    1024        ///< Stack size for the control task in 32-bit words
#define CTRL_TASK_PRIORITY                      10          ///< Priority of the control task

#define PROXY_SPI_HANDLE                        hspi3       ///< The SPI handle for the proxy communication port
#define RPI_SPI_HANDLE                          hspi2       ///< The SPI handle for the Raspberry Pi communication port
#define AMB0_SPI_HANDLE                         hspi4       ///< The SPI handle for AMB0
#define AMB1_SPI_HANDLE                         hspi5       ///< The SPI handle for AMB1
#define AMB2_SPI_HANDLE                         hspi1       ///< The SPI handle for AMB2
#define AMB3_SPI_HANDLE                         hspi6       ///< The SPI handle for AMB3

#define IWDG1_HANDLE                            hiwdg1      ///< The Independent watchdog handle

#define MASTER_AMB_SPI_PORT                     1           ///< The spi port designated as the master SPI port for AMB data transfers
//jm #define MASTER_AMB_SPI_PORT                     3           ///< The spi port designated as the master SPI port for AMB data transfers
#define MAX_NUM_AMB                             4           ///< The number of AMB's available on the transmitter.
#define MAX_NUM_CLIENTS                         1024        ///< The maximum number of client storable in the client tree.

#define EEPROM_I2C_HANDLE                       hi2c1       ///< The I2C handle going to the EEPROM chip
#define SYSCLK_I2C_HANDLE                       hi2c2       ///< The I2C handle going to the system clock
#define MULIPLEXER_I2C_HANDLE                   hi2c3       ///< The I2C handle going to the I2C multiplexers.

#define COMM_CHANNEL_LOW                        24          ///< Lowest allowed comm channel
#define COMM_CHANNEL_HIGH                       26          ///< Highest allowed comm channel

#define FIRST_CLIENT_ID                         1           ///< 0 and 0xFFFF are invalid, but 1 - 0xFFFE are valid starting client IDs (short IDs)

// The ForeverTracker receiver supports new Optimized Query messages, which contain the battery voltage data
// and don't carry any unnecessary information that the standard query messages do.  ForeverTracker receivers
// still support Standard Query message, but these can be considered obsolete and are only used for backward
// compatibility with Venus receivers (when USE_DEMO_BUILD symbol is defined).
#if defined USE_DEMO_BUILD
    #define DEFAULT_QUERY_TYPE   RCVR_QUERY_STANDARD        ///< Default query type for Venus Demo system
#elif defined USE_FOREVER_TRACKER_BUILD
    #define DEFAULT_QUERY_TYPE   RCVR_QUERY_OPTIMIZED       ///< Default query type for ForeverTracker/Cota_Box system
#endif

#define MIN(a, b)              (((a) <  (b)) ? (a) : (b))   ///< Returns the smallest of a and b
#define MAX(a, b)              (((a) >= (b)) ? (a) : (b))   ///< Returns the maximum between the two passed in parameters
#define SEC_TO_TICKS(a)        (pdMS_TO_TICKS((a) * 1000))  ///< Converts seconds to ticks
#define MIN_TO_TICKS(a)        (pdMS_TO_TICKS((a) * 60000)) ///< Converts minutes to ticks
#define SEC_TO_MS(a)           ((a)*1000)                   ///< Converts seconds to milliseconds
#define TIMER_BLOCK_MS         1000                         ///< time in milliseconds that the timer will block to start
#define NO_WAIT                0                            ///< Use to indicate we want a timer to stop immediately.
#define MS_IN_SEC              1000                         ///< Number of milliseconds in a second
#define MS_TO_SECONDS(ms)      ((ms) / 1000)                ///< Converts milliseconds to seconds
#define SEC_TO_MIN(s)          ((s) / 60)                   ///< Converts seconds to minutes
#define MIN_TO_HOURS(h)        ((h) / 60)                   ///< Converts minutes to hours
#define HOURS_TO_MIN(h)        ((h) * 60)                   ///< Converts hours to minutes
#define MIN_TO_SEC(m)          ((m) * 60)                   ///< Converts minutes to seconds
#define TICKS_TO_MS(a)         (((uint64_t)(a) * (MS_IN_SEC)) / (configTICK_RATE_HZ))  ///< Converts ticks to milliseconds

#if defined( __ICCARM__ )
  #define DMA_BUFFER \
      _Pragma("location=\".dma_buffer\"")
#elif defined(__GNUC__)
  // Only used for Unity testing where this is not used.
  #define DMA_BUFFER
#else
  #error Need to define DMA_BUFFER for this compiler
#endif

/**
 * Functions with this define indicate the function is defined in one
 * .c file, but not declared in the the corresponding .h file. Inorder
 * for a module to leverage that definition, it must copy its
 * declaration into the .c module that wants to use it. This keeps the
 * function private and helps enforce encapsulation.
 */
#define PRIVATE

/**
 * @brief System configuration structure
 *
 * This data is stored on the Identification Page of the EEPROM chip.
 * The fields must be packed to ensure all the data fits on a single
 * 256-byte memory page to avoid issues with reading/writing across
 * multiple pages and also to free up the general memory space for
 * client data storage.
 */
typedef PACKED struct _SysConfig_t
{
    uint64_t boardId;                        ///< Unique board ID
    uint32_t configFileVersion;              ///< Current version of this configuration file
    uint8_t  calibrationUvp;                 ///< ACL to be used for calibration
    uint8_t  calibrationAmb;                 ///< AMB to be used for calibration
    uint8_t  calibrationAmu;                 ///< AMU to be used for calibration
    uint8_t  referenceUvp;                   ///< ACL to be used as reference during calibration
    uint8_t  referenceAmb;                   ///< AMB to be used as reference during calibration
    uint8_t  referenceAmu;                   ///< AMU to be used as reference during calibration
    uint16_t autoCalibrationPeriod;          ///< Indicates how often should auto calibration run in minutes(0 -> auto-calibration is disabled)
    uint16_t calTempSmplPeriod;              ///< The temperatures are sampled with this period in seconds
    uint16_t calTempThreshold;               ///< A calibration will be initiated if any thermometer on the transmitter exceeds this value in degrees C
    uint8_t  clientChargePriorityThreshold;  ///< If a client's battery charge falls below this level, it will be given greater priority during charging
    uint8_t  commChannel;                    ///< IEEE 802.15.4 communication channel (11 - 26) to be used to communicate with clients
    uint16_t maxQueries;                     ///< The maximum number of queries to send at one time
    uint16_t queryPeriod;                    ///< The period in milliseconds for scheduling client queries for each client
    uint16_t clientQueryTimeout;             ///< The time period in milliseconds after which the system stop waiting for a query response from a client and move on to the next TPS cycle
    uint16_t clientResponseTimeout;          ///< A registered client will be marked as "UNKNOWN" if it does respond to any message after this many seconds
    uint16_t criticalTemperature;            ///< Critical system temperature used to stop power delivery when the system is overheated
    uint8_t  debugMode;                      ///< 0 -> the system is running normally; non-zero for debug mode
    uint16_t discoveryPeriod;                ///< Client discovery period in seconds; a value of 0 indicates a one-time client discovery
    uint16_t discoveryTimeout;               ///< An unregistered client will be removed from the active list if it does not respond to discovery messages after this many seconds
    uint16_t discoveryWait;                  ///< The time slot in milliseconds to wait for discovery message responses from clients
    uint16_t joinPeriod;                     ///< Client join period in seconds
    uint16_t joinWait;                       ///< The period of time in milliseconds to wait for a join message response
    uint16_t tpcPeriod;                      ///< Client TPC period in seconds
    uint16_t tpcWait;                        ///< The period of time in milliseconds to wait for a TPS message response
    uint8_t  uvpEncodeSize;                  ///< The value of the ENCODE_SIZE register, which controls the Rx frequency bandwidth in the UVP
    uint16_t goodAmbChannels;                ///< Bitmask indicating which AMBs are marked as being used in the system
    uint16_t lpmSlotCount;                   ///< The number of ESL slots
    uint16_t maxQueryFailCount;              ///< The number of consecutive failed client queries after which the client is put into NOT_ACTIVE state, where it will need to go through the join process
    uint16_t networkPanId;                   ///< The unique PAN ID of the proxy network used for communication with the clients
    uint16_t startingClientId;               ///< The first client short ID to be assigned to clients; this value is incremented for each successive client that gets a short ID assigned
    uint16_t proxyId;                        ///< Short ID to be assigned to the proxy for communication with clients
    uint16_t obstructionAngleThreshold;      ///< Minimum phase change during recalibration that causes another recalibration in degrees
    uint16_t phaseAdjustmentPeriod;          ///< Period in seconds to adjust phases based on temperature
    uint16_t powerLevel;                     ///< System power level in milli-dBm
    uint8_t  powerMode;                      ///< 0 = closed; 1 = open
    uint16_t shortCalibrationDuration;       ///< Short calibration starts when charging starts and is performed periodically for this amount of time in seconds
    uint16_t shortCalibrationPeriod;         ///< How often short calibration is performed, in seconds
    uint32_t systemType;                     ///< Byte 0:  Revision (variant); byte 1:  Version (Transmitter/PAC); byte 2:  System type (Venus/Orion); byte 3:  Reserved
    uint8_t  tpsNumBeaconBeats;              ///< Number of beacon beats in a TPS
    uint16_t tpsNumClients;                  ///< Number of clients participating in a TPS cycle
    uint8_t  tpsNumPowerSlots;               ///< Number of power slots between tone slots
    uint8_t  tpsNumPowerSlotsPerClient;      ///< Number of power slots between tone slots per client
    uint16_t tpsPaDelay;                     ///< Power amplifier start-up time in microseconds (this is used at the end of the tone slot to warm up the PAs before transmitting power during the power slot)
    uint16_t tpsRxDelay;                     ///< Delay between the start of the tone slot and the Rx phase update pulse, in microseconds
    uint16_t tpsRxDuration;                  ///< Tone slot duration in microseconds
    uint16_t tpsStartDelay;                  ///< The delay between the TPS Go signal and the start of the first tone slot in microseconds
    uint16_t tpsTxDuration;                  ///< Duration of a single power slot in microseconds
    uint16_t tpsTimeout;                     ///< Timeout waiting for TPS cycle to complete in milliseconds
    uint8_t  preTpsClientPause;              ///< Time in milliseconds to wait before starting to allow client to clear pending commands.
    uint8_t  postTpsClientPause;             ///< Time in milliseconds to wait after a TPS cycle to allow the client to start its radio
    uint16_t txFrequencyMhz;                 ///< Power transmission frequency in MHz
    uint8_t  systemClockConfig;              ///< 0 = clock to AMB's only, 1 = add TAM, 2 = add Master, 3 = add TAM and Master  4 = Slave 5 = Slave and Tam
    uint8_t  validFansMask;                  ///< A four bit mask indicating which fans are plugged in.
    uint16_t appCmdTimeout;                  ///< After issuing a app command, the time to wait for a response in milliseconds
    uint8_t  appCmdMaxSend;                  ///< The maximum app commands to send in a given client command slot.
    uint16_t discClientTimeout;              ///< The time to wait for client disconnect commands to respond in milliseconds.
    uint8_t  discClientMaxSend;              ///< The maximum number of client disconnect commands to send in a given client command slot.
    uint8_t  lightRingUpdatePeriod;          ///< The period in seconds in which we update the light ring.
    uint8_t  sysClkAmplitude;                ///< The amplitude setting for the system clocks.
    uint8_t  sysClkCommonMode;               ///< The common mode setting for the system clocks.
    uint8_t  sysClkFormat;                   ///< The format setting for the system clocks.
    uint8_t  sysClkAmbInversion;             ///<  4 2 bit settings for the inversion settings for amb clocks
    uint8_t  sysClkIdleDisable;              ///< If non-zero, the system clock will be disabled while the transmitter in not charging.
    uint8_t  sysLogLevel;                    ///< The level that defines which logs will be sent to the RPi or to the UART terminal.
    uint8_t  appAllCmdMaxTries;              ///< The maximum number of attempts to send an app command to a client when the all argument is used.
    uint16_t proxyTestPeriod;                ///< The period in seconds to poll the proxy with the proxy info command.  If the proxy doesn't respond, the proxy is reset.
    uint16_t proxyTestWaitTimeout;           ///< The maximum time in milliseconds to wait for a proxy info response.  If a proxy response does not arrive we assume the proxy has crashed and reset it.
    uint16_t doorOpenMsgPeriod;              ///< The period in milliseconds that the door open message will be sent to the host.
    uint16_t rcvrLongConfigRespTimeout;      ///< The amount of time in milliseconds that the transmitter expects configuration responses for Forever Tracker.
    uint8_t  rcvrSleepTime;                  ///< The amount of time in seconds that the Xirgo boards will wait before sleeping
    uint8_t   rcvrChargingThreshold;          ///< The charging threshold for a receiver
    int8_t    rcvrCommPower;                  ///< The communication power for the proxy-receiver messages
    uint8_t   ethernetClockState;             ///< The state that the Ethernet clock will be set to.
    uint16_t  ftTpsFatalLowHistMin;           ///< The minimum hysteresis charge value for fatally low receivers
    uint16_t  ftTpsFatalLowHistMax;           ///< The maximum hysteresis charge value for fatally low receivers
    uint16_t  ftTpsMaxNumFatalLowRcvr;        ///< The number of fatally low receivers allowed to receive charge
    uint16_t  ftWaitForSleepPeriodTimeout;    ///< The maximum time in seconds to wait for receivers to indicate sleep before beginning to charge
    uint16_t  ftReceiverRemovalPeriod;        ///< The period in milliseconds where we will remove receivers from the database
    uint16_t  ftShortConfigRespTimeout;       ///< The amount of time in milliseconds that the transmitter expects configuration responses ("re-enumeration" period) for Forever Tracker.
    uint16_t  ftProxyAnnouncementDelayMs;     ///< The amount of time in milliseconds that the proxy will delay before acknowledging and forwarding another announcement message.
    int8_t    ftProxyRssiFilterMinValue;      ///< The minimum value of RSSI (in dBm) that will allow an announcement message to be acted upon.
} SysConfig_t;

/** Enumeration of system configuration data fields */
typedef enum _ConfigParamId_t
{
    CFG_BOARD_ID,                       ///< Unique board ID
    CFG_CONFIG_FILE_VERSION,            ///< Current version of this configuration file
    CFG_CALIBRATION_UVP,                ///< UVP to be used for calibration
    CFG_CALIBRATION_AMB,                ///< AMB to be used for calibration
    CFG_CALIBRATION_AMU,                ///< AMU to be used for calibration
    CFG_REFERENCE_UVP,                  ///< UVP to be used as reference during calibration
    CFG_REFERENCE_AMB,                  ///< AMB to be used as reference during calibration
    CFG_REFERENCE_AMU,                  ///< AMU to be used as reference during calibration
    CFG_AUTO_CALIBRATION_PERIOD,        ///< Indicates how often should auto calibration run (0 -> auto-calibration is disabled, except for temperature based)
    CFG_CAL_TEMP_SMPL_PERIOD,           ///< The temperatures are sampled with this period in seconds (0 -> disable temperature based calibration)
    CFG_CAL_TEMP_THRESHOLD,             ///< A calibration will be initiated if any thermometer on the transmitter exceeds this value in degrees C
    CFG_CHG_PRIORITY_THRESHOLD,         ///< If a client's battery charge falls below this level, it will be given greater priority during charging
    CFG_COMM_CHANNEL,                   ///< IEEE 802.15.4 communication channel (11 - 26) to be used to communicate with clients
    CFG_MAX_QUERIES,                    ///< The maximum number of queries to send at one time
    CFG_QUERY_PERIOD,                   ///< The period in milliseconds for scheduling client queries for each client
    CFG_CLIENT_QUERY_TIMEOUT,           ///< The time period in milliseconds after which the system stop waiting for a query response from a client and move on to the next TPS cycle
    CFG_CLIENT_RESPONSE_TIMEOUT,        ///< A registered client will be marked as "UNKNOWN" if it does respond to any message after this many seconds
    CFG_CRITICAL_TEMPERATURE,           ///< Critical system temperature in C used to stop power delivery when the system is overheated
    CFG_DEBUG_MODE,                     ///< 0 -> the system is running normally; non-zero for debug mode
    CFG_DISCOVERY_PERIOD,               ///< Client discovery period in seconds; a value of 0 indicates a one-time client discovery
    CFG_DISCOVERY_TIMEOUT,              ///< An unregistered client will be removed from the active list if it does not respond to discovery messages after this many seconds
    CFG_DISCOVERY_WAIT,                 ///< The time slot in milliseconds to wait for discovery message responses from clients
    CFG_JOIN_PERIOD,                    ///< The period of time in seconds to check to see if join messages should be sent.
    CFG_JOIN_WAIT,                      ///< The period of time in milliseconds to wait for a join message response
    CFG_TPC_PERIOD,                     ///< The period of time in seconds to check to see if TPC messages should be sent.
    CFG_TPC_WAIT,                       ///< The period of time in milliseconds to wait for a TPS message response
    CFG_UVP_ENCODE_SIZE,                ///< The value of the ENCODE_SIZE register, which controls the Rx frequency bandwidth in the UVP
    CFG_VALID_AMB_MASK,                 ///< Bitmask indicating which AMBs are marked as being used in the system
    CFG_LPM_SLOT_COUNT,                 ///< The number of ESL slots (0 -> LPM disabled)
    CFG_MAX_QUERY_FAIL_COUNT,           ///< The number of consecutive failed client queries after which the client is put into NOT_ACTIVE state, where it will need to go through the join process
    CFG_NETWORK_PAN_ID,                 ///< The unique PAN ID of the proxy network used for communication with the clients
    CFG_STARTING_CLIENT_ID,             ///< The first client short ID to be assigned to clients; this value is incremented for each successive client that gets a short ID assigned
    CFG_PROXY_ID,                       ///< Short ID to be assigned to the proxy for communication with clients
    CFG_OBSTRUCTION_ANGLE_THRESHOLD,    ///< Minimum phase change during recalibration that causes another recalibration
    CFG_PHASE_ADJUSTMENT_PERIOD,        ///< Period in seconds to adjust phases based on temperature
    CFG_POWER_LEVEL,                    ///< System power level in milli-dBm
    CFG_POWER_MODE,                     ///< 0 = closed; 1 = open (anyone can join)
    CFG_SHORT_CALIBRATION_DURATION,     ///< Short calibration starts when charging starts and is performed periodically for this amount of time in seconds (0 disables recalibration)
    CFG_SHORT_CALIBRATION_PERIOD,       ///< How often short calibration is performed, in seconds
    CFG_SYSTEM_TYPE,                    ///< Byte 0:  Revision (variant); byte 1:  Version (Transmitter/PAC); byte 2:  System type (Venus/Orion); byte 3:  Reserved
    CFG_TPS_NUM_BEACON_BEATS,           ///< Number of beacon beats in a TPS
    CFG_TPS_NUM_CLIENTS,                ///< Number of clients participating in a TPS cycle
    CFG_TPS_NUM_POWER_SLOTS,            ///< Number of power slots between tone slots
    CFG_TPS_NUM_POWER_SLOTS_PER_CLIENT, ///< Number of power slots between tone slots per client
    CFG_TPS_PA_DELAY,                   ///< Power amplifier start-up time in microseconds (this is used at the end of the tone slot to warm up the PAs before transmitting power during the power slot)
    CFG_TPS_RX_DELAY,                   ///< Delay between the start of the tone slot and the Rx phase update pulse, in microseconds
    CFG_TPS_RX_DURATION,                ///< Tone slot duration in microseconds
    CFG_TPS_START_DELAY,                ///< The delay between the TPS Go signal and the start of the first tone slot in microseconds
    CFG_PRE_TPS_CLIENT_PAUSE,           ///< Time in milliseconds to wait before starting to allow client to clear pending commands.
    CFG_POST_TPS_CLIENT_PAUSE,          ///< Time in milliseconds to wait after a TPS cycle to allow the client to start its radio
    CFG_TPS_TX_DURATION,                ///< Duration of a single power slot in microseconds
    CFG_TPS_TIMEOUT,                    ///< Timeout waiting for TPS cycle to complete in milliseconds
    CFG_TX_FREQUENCY_MHZ,               ///< Power transmission frequency in MHz; 0 causes Tx frequency to be set based on the comm channel
    CFG_SYSTEM_CLOCK,                   ///< 0 = clock to AMB's only, 1 = add TAM, 2 = add Master, 3 = add TAM and Master  4 = Slave 5 = Slave and Tam
    CFG_VALID_FANS,                     ///< A four bit mask indicating which fans are plugged in.
    CFG_APP_COMMAND_TIMEOUT,            ///< The time to wait for app command to respond in milliseconds.
    CFG_APP_COMMAND_MAX_SEND,           ///< The maximum app commands to send in a given client command slot.
    CFG_DISC_CLIENT_TIMEOUT,            ///< The time to wait for client disconnect commands to respond in milliseconds.
    CFG_DISC_CLIENT_MAX_SEND,           ///< The maximum number of client disconnect commands to send in a given client command slot.
    CFG_LIGHT_RING_UPDATE_PERIOD,       ///< The period where we will ensure that the light ring is the correct state
    CFG_SYSCLK_AMPLITUDE,               ///< The amplitude setting for the system clocks.
    CFG_SYSCLK_COMMON_MODE,             ///< The common mode setting for the system clocks.
    CFG_SYSCLK_FORMAT,                  ///< The format setting for the system clocks.
    CFG_SYSCLK_AMB_INVERSION,           ///< The inversion settings for the system AMB clocks.
    CFG_SYSCLK_IDLE_DISABLE,            ///< If non-zero, the system clock will be disabled while the transmitter in not charging.
    CFG_SYS_LOG_LEVEL,                  ///< The system log level that determines which errors are logged.
    CFG_APP_CMD_MAX_TRIES,              ///< The maximum number of attempts to send an app command to a client when the all argument is used.
    CFG_PROXY_TEST_PERIOD,              ///< The period in seconds to poll the proxy with the proxy info command.  If the proxy doesn't respond, the proxy is reset.
    CFG_PROXY_TEST_WAIT_TIMEOUT,        ///< The maximum time in milliseconds to wait for a proxy info response.  If a proxy response does not arrive we assume the proxy has crashed and reset it.
    CFG_DOOR_OPEN_MSG_PERIOD,           ///< The period in milliseconds that the door open message will be sent to the host.
    CFG_RCVR_LONG_CONFIG_RESP_TIMEOUT,  ///< The amount of time in milliseconds that the transmitter expects announcement messages.
    CFG_XIRGO_SLEEP_TIME,               ///< The amount of time in seconds that the Xirgo boards will wait before sleeping
    CFG_RCVR_CHARGING_THRESHOLD,        ///< Battery SOC threshold at which receivers will stop requesting power
    CFG_RCVR_COMM_PWR,                  ///< Data communication power level for receivers in dBm
    CFG_ETHERNET_CLOCK_STATE,           ///< Holds the state of the Ethernet clock control gpio.
    CFG_FATAL_LOW_HIST_MIN,             ///< The minimum hysteresis charge value for fatally low receivers
    CFG_FATAL_LOW_HIST_MAX,             ///< The maximum hysteresis charge value for fatally low receivers
    CFG_MAX_NUM_OF_FATAL_LOW_RCVRS,     ///< The number of fatally low receivers allowed to receive charge
    CFG_WAIT_FOR_SLEEP_TIMEOUT,         ///< The maximum time in seconds to wait for receivers to indicate sleep before beginning to charge
    CFG_RECEIVER_REMOVAL_PERIOD,        ///< The period in milliseconds where we will remove receivers from the database
    CFG_RCVR_SHORT_CONFIG_RESP_TIMEOUT, ///< The amount of time in milliseconds that the transmitter expects announcement messages ("re-enumeration").
    CFG_PROXY_ANNOUNCEMENT_DELAY,       ///< The amount of time in milliseconds that the proxy will delay before acknowledging and forwarding another announcement message.
    CFG_PROXY_RSSI_FILTER_MIN_VAL,      ///< The minimum value of RSSI (in dBm) that will allow an announcement message to be acted upon.
} ConfigParamId_t;

///** Build Types */
typedef enum _BuildType_t
{
    DEMO,                               ///< Demo build used for general power delivery
    FOREVER_TRACKER,                    ///< Forever Tracker build used for general power delivery
} BuildType_t;


void CfgInit(void);
CotaError_t CfgGetParam(ConfigParamId_t paramId, void * paramVal, uint8_t size);
CotaError_t CfgSaveParam(ConfigParamId_t paramId, void * paramVal, uint8_t size);
SysConfig_t GetDefaultSystemConfig(void);
BuildType_t GetBuildType(void);

#endif //#ifndef _ORION_CONFIG_H_
