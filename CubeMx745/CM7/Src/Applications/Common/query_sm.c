/**
 * @file       query_sm.c
 *
 * @brief      The query state machine definitions for the demo system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "cqt_scheduler.h"
#include "client_interface.h"
#include "query_sm.h"
#include "query_sm_helper.h"
#include "client.h"
#include "debug_on.h"

#if defined(USE_FOREVER_TRACKER_BUILD)
    #include "ForeverTracker/ft_query.h"
#elif defined(USE_DEMO_BUILD)
    #include "Demo/demo_query.h"
#endif

#if defined(USE_COMMON_QUERY)

TimerHandle_t  gQueryTimerHandle                     = NULL;     ///< FreeRTOS timer handle for the query timer
StaticTimer_t  gQueryTimerBuffer;                                ///< FreeRTOS timer buffer for the query timer
TimerHandle_t  gQueryAckTimerHandle                  = NULL;     ///< FreeRTOS timer handle for the query acknowledgment timer
StaticTimer_t  gQueryAckTimerBuffer;                             ///< FreeRTOS timer buffer for the query acknowledgment timer

volatile bool gSendQuery        = false;       ///< true when a query message must be sent
volatile bool gQueryAckTimeout  = false;       ///< true when an query acknowledgment was not received before timeout

static void queryTimerCallback(TimerHandle_t timer);
static void ackQueryTimerCallback(TimerHandle_t  timer);

bool StartQueryTimers(void)
{
    return xTimerStart( gQueryTimerHandle, NO_WAIT ) == pdPASS;
}

void QueryStateInit(void)
{
    uint16_t timerVal;

    /*
      Query Timer
     */
    CfgGetParam(CFG_QUERY_PERIOD, &timerVal, sizeof(timerVal));

    if (timerVal > 0)
    {
        if (gQueryTimerHandle)
        {
            xTimerStop(gQueryTimerHandle, NO_WAIT);
            xTimerChangePeriod(gQueryTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gQueryTimerHandle = xTimerCreateStatic(
                "Query Timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                queryTimerCallback,
                &gQueryTimerBuffer);
        }
    }
    else
    {
        gQueryTimerHandle = NULL;
    }

    CfgGetParam(CFG_CLIENT_QUERY_TIMEOUT, &timerVal, sizeof(timerVal));

    if (timerVal > 0)
    {
        if (gQueryAckTimerHandle)
        {
            xTimerStop(gQueryAckTimerHandle, NO_WAIT);
            xTimerChangePeriod(gQueryAckTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gQueryAckTimerHandle = xTimerCreateStatic(
                "Query Acknowledgment Timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                ackQueryTimerCallback,
                &gQueryAckTimerBuffer);
        }
    }
    else
    {
        gQueryAckTimerHandle = NULL;
    }

    if((!gQueryTimerHandle) ||
       (!gQueryAckTimerHandle))
    {
        LogFatal("Query timers failed to initialize\r\n");
    }
}

/**
 * @brief Called when the query state is entered
 */
void QueryEntry(void)
{
    CalculateQuerySchedule();
    LogInfo("The Control task has entered the Query state.\r\n");

    // Begin the query timer
    gQueryAckTimeout = false;
    TryToStartTimer(gQueryAckTimerHandle);

    PrxSendQuery();
}

/**
 *  @brief Called to enable/disable a query cycle
 *
 * @param state true to enable another round of queries to execute
 */
void EnableQueryCycle(bool state)
{
    gSendQuery = state;
}

/**
 *  @brief Called to start query timer
 *
 */
void StartQueryTimer(void)
{
    TryToStartTimer(gQueryTimerHandle);
}

/**
 *  @brief Called to stop query timer
 *
 */
void StopQueryTimer(void)
{
    TryToStopTimer(gQueryTimerHandle);
}

///< @todo query should be moved to common for clarity
bool RdyToQueryCheck (void){ return gSendQuery && PrxIsReady() && CheckIfQueryAllowed(); }
bool QueryToRdyCheck (void){ return (gQueryAckTimeout || (!GetRemainingQueries())); }

/**
 *  @brief Called when the query state returns to the ready state.
 *         This restarts the query timers, and checks for query failures
 */
void QueryToRdyExit (void)
{
    gSendQuery = false;
    TryToStopTimer(gQueryAckTimerHandle);
    MarkAllRemainingScheduledQueriesAsFailed();
    DoImplementationSpecificQueryCycle();
}

/**
 * @brief This function is called when the query timer finishes. It will set the
 *        #gSendQuery flag and post to the ctrl semaphore that it is time to send
 *        a query.
 *
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void queryTimerCallback(TimerHandle_t timer)
{
    gSendQuery = true;
}

/**
 * @brief The query acknowledgment timer is called after a query message is sent.
 *        If an acknowledgment message is not received before this timer expires, then
 *        the query message has timed out.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void ackQueryTimerCallback(TimerHandle_t  timer)
{
    gQueryAckTimeout = true;
}

#endif // defined(USE_COMMON_QUERY)
