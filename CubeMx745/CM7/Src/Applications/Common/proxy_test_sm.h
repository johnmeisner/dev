/**
 * @file       proxy_test_sm.h
 *
 * @brief      
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_COMMON_PROXY_TEST)

#ifndef _PROXY_TEST_SM_H_
#define _PROXY_TEST_SM_H_

bool StartProxyTestTimers(void);
void ProxyTestStateInit(void);

#endif /* _PROXY_TEST_SM_H_ */

#endif // defined(USE_COMMON_PROXY_TEST)
