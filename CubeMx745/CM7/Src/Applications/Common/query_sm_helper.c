/**
 * @file       query_sm_helper.c
 *
 * @brief      A helper module for the query_sm module. This helps us eliminate
 *             duplicate code between multiple definitions in query_sm.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"
#include "cqt_scheduler.h"
#include "client_interface.h"
#include "query_sm_helper.h"

#if defined(USE_FOREVER_TRACKER_BUILD)
#include "ForeverTracker/enumeration.h"
#include "ForeverTracker/ft_client_interface.h"
#include "door_driver.h"
#endif

#if defined(USE_DEMO_BUILD)
#include "Demo/demo_client_interface.h"

void WrapUpClientQueryState(void)
{
    ClearClientQueryFlag();
    CmHandleUnresponsiveClients();
}

bool CheckIfQueryAllowed(void)
{
    return true; // This parameter is not used for the Demo machine
}

#elif defined(USE_FOREVER_TRACKER_BUILD)

static bool checkDoorClosed(void);

void WrapUpClientQueryState(void)
{
    ClearClientQueryFlag();
    CmHandleUnresponsiveClients();
}

bool CheckIfQueryAllowed(void)
{
    bool isDoorClosed = checkDoorClosed();

    return (!CheckIfEnumerationPeriodActive() && isDoorClosed);
}

/**
 * @brief Returns the state of the door once the door state has changed.
 */
static bool checkDoorClosed(void)
{
    static bool doorStateClosedCache = false;

    if (doorStateClosedCache != IsDoorClosed())
    {
        doorStateClosedCache = IsDoorClosed();
    }

    return doorStateClosedCache;
}

#endif
