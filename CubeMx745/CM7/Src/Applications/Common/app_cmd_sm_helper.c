/**
 * @file       app_cmd_sm_helper.c
 *
 * @brief      A helper module for the app_cmd_sm module. This helps us eliminate
 *             duplicate code between multiple definitions in app_cmd_sm.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include "orion_config.h"

#if defined(USE_DEMO_BUILD)

bool CheckIfAppCmdAllowed()
{
    return true;
}

#elif defined(USE_FOREVER_TRACKER_BUILD)

#include "ForeverTracker/enumeration.h"

bool CheckIfAppCmdAllowed()
{
    return !CheckIfEnumerationPeriodActive();
}

#endif 
