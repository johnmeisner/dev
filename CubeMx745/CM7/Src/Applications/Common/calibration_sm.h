/**
 * @file       calibration_sm.h
 *
 * @brief      The header file for the calibration state machine state.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_COMMON_CALIBRATION)

#ifndef _CALIBRATION_SM_H_
#define _CALIBRATION_SM_H_



#endif /* _CALIBRATION_SM_H_ */

#endif // defined(USE_COMMON_CALIBRATION)
