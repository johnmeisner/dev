/**
 * @file       power_monitor_sm.h
 *
 * @brief      
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "orion_config.h"

#if defined(USE_COMMON_POWER_MONITOR)

#ifndef _POWER_MONITOR_SM_H_
#define _POWER_MONITOR_SM_H_

void SetPmControl(uint8_t);
uint8_t GetPmControl(void);
void PowerMonitorStateInit(void);

#endif /* _POWER_MONITOR_SM_H_ */

#endif // defined(USE_COMMON_POWER_MONITOR)
