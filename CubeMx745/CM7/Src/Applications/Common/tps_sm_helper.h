/**
 * @file       tps_sm_helper.h
 *
 * @brief      A helper module for the tps_sm module. This helps us eliminate
 *             duplicate code between multiple definitions in tps_sm.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */
#include "error.h"

#ifndef TPS_SM_HELPER_H
#define TPS_SM_HELPER_H

/**
 * @brief Checks all potential cases that would prevent TPS/Charging from starting.
 *
 *        This function can be linked to a handful of different calls
 *        based on the configuration of the system. Each
 *        implementation will have different parameters for what is
 *        considered an 'Error' condition.
 */
bool CheckImplementationSpecificTpsPrerequisites(bool chargingNeeded);

/**
 * @brief Build-specific actions for comm to Proxy entering the TPS state.
 *
 *        This function is typically called from the TPS state entry code 
 *        and differentiates based upon the build configuration
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code
 */
CotaError_t DoBuildSpecificTpsEntryProxyMessageSend(void);

#endif /* TPS_SM_HELPER_H */
