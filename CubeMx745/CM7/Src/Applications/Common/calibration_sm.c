/**
 * @file       calibration_sm.c
 *
 * @brief      The common state machine definition for the calibration state.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "calibration_sm_helper.h"
#include "calibrator.h"
#include "sysclk_driver.h"
#include "log.h"
#include "orion_config.h"
#include "amb_control.h"
#include "light_ring_interface.h"
#include "FreeRTOS.h"
#include "task.h"

#if defined(USE_COMMON_CALIBRATION)

/**
 * @brief Entry function for the calibration state of the control task.  The goal
 *        is to set the power level and calibrate the system.
 */  
void CalEntry(void)
{
    CotaError_t err = COTA_ERROR_NONE;
    uint32_t currTime;
    
    LogInfo("The Control task has entered the Calibration State.\r\n");
    
    LRLightRingUpdate(LR_STATE_CALIB);
    
    EnableSystemClock(true);
    
    if (AmbValidCount() > 0)
    {
        err = Calibrate(AmbGetValidAmbs(), ALL_UVP_MASK);
    }
    else
    {
        err = COTA_ERROR_CALIBRATION_NEEDS_AMBS;
    }
    
    DisableSystemClockIfClockIdleIsSet();
    
    currTime = xTaskGetTickCount();
    
    if ( err != COTA_ERROR_NONE)
    {
        POST_COTA_ERROR(err);
        LogError("Calibration failed at %llu.%03llu seconds\r\n",
                   TICKS_TO_MS(currTime)/MS_IN_SEC,
                   TICKS_TO_MS(currTime) % MS_IN_SEC);
    }
    else
    {
        LogInfo("Calibration succeeded at %llu.%03llu seconds\r\n",
                   TICKS_TO_MS(currTime)/MS_IN_SEC,
                   TICKS_TO_MS(currTime) % MS_IN_SEC);

        
#ifdef COTA_STATIC_TUNE
        err = (err == COTA_ERROR_NONE) ? StaticTune(AmbGetValidAmbs()) : err;
        if ( err != COTA_ERROR_NONE)
        {
            POST_COTA_ERROR(err);
            PRINT_DEBUG("Static tune failed\r\n");
        }
        else
        {
            PRINT_DEBUG("Static tune success\r\n");
        }
#endif        
    }

    SetManualCalibrationFlag(false);
}

bool RdyToCalCheck (void){ return (CheckCalibrationNeed() && CheckIfCalibrationAllowed()); }
bool CalToRdyCheck (void){ return true; }

#endif // defined(USE_COMMON_CALIBRATION)
