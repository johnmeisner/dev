/**
 * @file       proxy_test_sm.c
 *
 * @brief      Defines the Proxy Test state machine functions for the common system.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdbool.h>
#include <stdlib.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "orion_config.h"
#include "log.h"
#include "sys_util.h"
#include "proxy_msg_interface.h"
#include "client_interface.h"


#if defined(USE_COMMON_PROXY_TEST)

TimerHandle_t  gProxyTestPeriodTimerHandle           = NULL;     ///< FreeRTOS timer handle for the proxy test period timer
StaticTimer_t  gProxyTestPeriodTimerBuffer;                      ///< FreeRTOS timer buffer for the proxy test period timer
TimerHandle_t  gProxyTestWaitTimerHandle             = NULL;     ///< FreeRTOS timer handle for the proxy test timeout timer
StaticTimer_t  gProxyTestWaitTimerBuffer;                        ///< FreeRTOS timer buffer for the proxy test timeout timer

volatile bool gSendProxyTest    = false;       ///< true if it's time to send a proxy info message to see if the proxy is alive.
volatile bool gProxyTestTimeout = false;       ///< true if the proxy wait timeout was expired.

static void proxyTestPeriodTimerCallback(TimerHandle_t timer);
static void proxyTestTimeoutTimerCallback(TimerHandle_t timer);

void ProxyTestStateInit(void)
{
    uint16_t timerVal;
    
    CfgGetParam(CFG_PROXY_TEST_PERIOD, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gProxyTestPeriodTimerHandle)
        {
            xTimerStop(gProxyTestPeriodTimerHandle, NO_WAIT);
            xTimerChangePeriod(gProxyTestPeriodTimerHandle, SEC_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gProxyTestPeriodTimerHandle = xTimerCreateStatic(
                "Proxy Test Period Timer",
                SEC_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,
                proxyTestPeriodTimerCallback,
                &gProxyTestPeriodTimerBuffer);
        }
    }
    else
    {
       gProxyTestPeriodTimerHandle = NULL;
    }

    CfgGetParam(CFG_PROXY_TEST_WAIT_TIMEOUT, &timerVal, sizeof(timerVal));
    
    if (timerVal > 0)
    {
        if (gProxyTestWaitTimerHandle)
        {
            xTimerStop(gProxyTestWaitTimerHandle, NO_WAIT);
            xTimerChangePeriod(gProxyTestWaitTimerHandle, pdMS_TO_TICKS(timerVal), NO_WAIT);
        }
        else
        {
            gProxyTestWaitTimerHandle = xTimerCreateStatic(
                "Proxy Test Wait Timer",
                pdMS_TO_TICKS(timerVal),
                pdFALSE,
                (void *) 0,  //no timer id needed since we only have one callback.
                proxyTestTimeoutTimerCallback,
                &gProxyTestWaitTimerBuffer);
        }
    }
    else
    {
       gProxyTestWaitTimerHandle = NULL;
    }

    if ((!gProxyTestPeriodTimerHandle) ||
        (!gProxyTestWaitTimerHandle))
    {
        LogFatal("Proxy Test timers failed to initialize\r\n");
    }
}

bool StartProxyTestTimers(void)
{
    return xTimerStart( gProxyTestPeriodTimerHandle, NO_WAIT ) == pdPASS;
}


void ProxyTestEntry(void)
{
    gProxyTestTimeout = false;
    gSendProxyTest = false;

    TryToStartTimer(gProxyTestWaitTimerHandle);
    
    InvalidateProxyReceivedFlags();
    PrxSendReqInfo();
}

bool RdyToProxyTestCheck (void) 
{
   return gSendProxyTest && PrxIsReady();
}

bool ProxyTestToRdyCheck(void)
{
    bool ret = PrxIsReady();
  
    if (!ret)
    {
        if (gProxyTestTimeout)
        {
            //The proxy test has timed out, time to reset the proxy
            LogWarning("PROXY RESET\r\n");            
            PrxRestart();
            ret = true;  //exit the state
        }      
    }
    
    return ret;
}

void ProxyTestToRdyExit (void)
{
    TryToStopTimer(gProxyTestWaitTimerHandle);
    TryToStartTimer(gProxyTestPeriodTimerHandle);
}

/**
 * @brief This function is called when the proxy test period timer finishes. It will set the
 *        #gSendProxyTest flag causing a proxy info message to be sent to the proxy.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void proxyTestPeriodTimerCallback(TimerHandle_t timer)
{
    gSendProxyTest = true;
}

/**
 * @brief This function is called when the proxy test wait timer finishes. It will set the
 *        #gProxyTestTimeout flag causing the proxy to be reset.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void proxyTestTimeoutTimerCallback(TimerHandle_t timer)
{
    gProxyTestTimeout = true;

}

#endif // defined(USE_COMMON_PROXY_TEST)
