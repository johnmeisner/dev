/**
 * @file       ctrl_interface.h
 *
 * @brief      Defines the messages that the control task is able to accept and act upon.
 *
 *             The purpose of this module is to manage the message queue and define messages.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CTRL_INTERFACE_H_
#define _CTRL_INTERFACE_H_


/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////
#include <stdint.h>
#include "CotaCommonTypes.h"
#include "proxyHostMsgInf.h"
#include "error.h"
#include "amb_control.h"
#include "client.h"
#include "process_uvp_commands.h"
#include "raspberry_pi.h"
#include "ccb_temperature.h"   

/////////////////////////////////////////////////////////////////////////////////////////////
// Defines 
/////////////////////////////////////////////////////////////////////////////////////////////
#define CTRL_NO_DELAY        0       ///< no delay in starting a timer
#define MAX_TIMESTAMP_SIZE   30      ///< Maximum size of the string containing the FW build timestamp

/** Maximum size of the host-to-RPi response message payload data */
#define MAX_RESP_RPI_HOST_DATA_MSG_SIZE  MAX_FRAME_SIZE - CRC_FIELD_SIZE \
                                                        - CRC_OFST_FIELD_SIZE \
                                                        - MSG_LEN_FIELD_SIZE \
                                                        - sizeof(CtrlMsgId_t) \
                                                        - sizeof(CotaError_t)

/** Maximum size of the RPi-to-host message payload data */
#define MAX_RPI_HOST_DATA_MSG_SIZE       MAX_FRAME_SIZE - CRC_FIELD_SIZE \
                                                        - CRC_OFST_FIELD_SIZE \
                                                        - MSG_LEN_FIELD_SIZE \
                                                        - sizeof(CtrlMsgId_t)

// Typedefs

/** Control task messages */
typedef enum _CtrlMsgId_t
{
    CTRL_MSG_INVALID  = 0,           ///< Invalid message type
    CTRL_REGISTER_RCVR,              ///< Register a receiver that has been discovered
    CTRL_REMOVE_RCVR,                ///< Remove a Receiver from the registration list.
    CTRL_RCVR_LIST,                  ///< Gives a list of clients
    CTRL_PROXY_RESET,                ///< Toggles the reset line of the proxy. 
    CTRL_RUN,                        ///< Take the charger out of debug mode
    CTRL_PAUSE,                      ///< Put the charger in debug mode.
    CTRL_REBOOT,                     ///< Causes the charger to reboot
    CTRL_SET_AMB_EN,                 ///< Turn on or off the AMB's in the specified mask 
    CTRL_GET_AMB_EN,                 ///< Retrieves a mask indicating which AMB's are on
    CTRL_SET_UVP_EN,                 ///< Turn UVP's on or off for the AMBs in the specified mask
    CTRL_GET_UVP_EN,                 ///< Retrieves a mask indicating which uvp's are enabled
    CTRL_SELECT_UVP,                 ///< Selects UVP's to interact with
    CTRL_GET_SELECTED_UVP,           ///< Get the selected UVP number
    CTRL_SET_PU_EN,                  ///< Assert or Deassert the  specified AMB's phase update pin
    CTRL_GET_PU_EN,                  ///< Retrieves a mask indicating which AMB's PU are asserted
    CTRL_SET_TX,                     ///< Set or reset the TX control pin
    CTRL_GET_TX,                     ///< Gets the TX control pin's state
    CTRL_SET_RX,                     ///< Set or reset the RX control pin
    CTRL_GET_RX,                     ///< Gets the RX control pin state
    CTRL_SET_PU_ALL,                 ///< Turn on the PU_ALL pin defined by a false(0) or true(1) input value
    CTRL_GET_PU_ALL,                 ///< Get the state of the PU_ALL pin
    CTRL_GET_PD_DONE,                ///< Get the state of the phase detect pins for every AMB
    CTRL_SET_SPI_EN,                 ///< Enable/Disable  SPI transfers for the specified AMB mask 
    CTRL_GET_SPI_EN,                 ///< Retrieves a mask indicating which AMB's spi's are enabled
    CTRL_UVP_INIT,                   ///< Re-initialize the select uvp's
    CTRL_SYS_CLK_INIT,               ///< Initializes the system clock
    CTRL_TX_FREQ,                    ///< Sets the transmit frequency
    CTRL_CALIBRATION,                ///< Causes calibration to occur. 
    CTRL_SET_POWER_LEVEL,            ///< Sets the power level for the transmitter
    CTRL_GET_POWER_LEVEL,            ///< Gets the power level for the transmitter.
    CTRL_START_STATIC_TUNE,          ///< Prepares the system to static tune.
    CTRL_SAMPLE_BEACON,              ///< Sample the beacon
    CTRL_STATIC_CHARGE,              ///< Start the static tune charge
    CTRL_GET_RCVR_DETAIL,            ///< return a list of receiver  detail data, net current, received power, received average power etc.
    CTRL_SHUTDOWN_CCB,               ///< halts the RPi and sends a signal to the M7 to shutdown
    CTRL_START_CHARGING_DEVICES,     ///< start the transmitter charging a specified Receiver, list of clients or key word all,
    CTRL_STOP_CHARGING_DEVICES,      ///< stop charging clients
    CTRL_LPM_ASSIGN,                 ///< Assign Receiver to a slot number
    CTRL_LPM_FREE,                   ///< Free a slot number
    CTRL_LPM_LIST,                   ///< Requests the list of clients and info about them in slot number order
    CTRL_RCVR_LPM,                   ///< Set the Receiver LPM state.
    CTRL_LPM_SLOTS,                  ///< set the number of LPM slots
    CTRL_SET_COMM_CHANNEL,           ///< set the current tx to rx communications channel
    CTRL_APP_COMMAND,                ///< send the specified command number to a Receiver
    CTRL_APP_COMMAND_DATA,           ///< Get the returned data from the Receiver after a app_command message
    CTRL_SEND_DISCOVERY_MESSAGE,     ///< send a discovery message using an optional com channel.
    CTRL_RESET_NVM,                  ///< Resets the non-volatile data to its defaults
    CTRL_SET_RCVR_CONFIG,            ///< Set the Receiver  Config QueryType 5=Std or 6=Ext
    CTRL_RESET_ARRAY,                ///< Reset the Antenna Array
    CTRL_SET_RSSI_THRESH,            ///< Captures, averages, and sets the rssi threshold for filtering
    CTRL_SET_RSSI_FILTER_EN,         ///< Enables or disables rssi filtering feature
    CTRL_GET_RSSI_FILTER_EN,         ///< Returns the state of the rssi filtering feature
    CTRL_RSSI_FILTER_RESET,          ///< Resets the EEPROM storage for rssi filtering
    CTRL_RSSI_FILTER_REPORT,         ///< Report the RSSI values from the UVPs
    CTRL_AMB_FILTER_DISABLED,        ///< Informs which AMB's are disabled by rssi filtering
    CTRL_PROXY_COMMAND,              ///< Sends a proxy command
    CTRL_PROXY_COMMAND_DATA,         ///< Requests the proxy command data
    CTRL_PROXY_INFO,                 ///< Requests proxy info. 
    CTRL_GET_FIRMWARE_VERSION,       ///< Request System Firmware Version information
    CTRL_GET_SYSTEM_STATE,           ///< Get System State Running, pause, error, etc. 
    CTRL_GET_SYSTEM_TEMP,            ///< Retrieve the system Temperature
    CTRL_IDENTIFY_TX,                ///< Causes the Light ring to Signal
    CTRL_LOCK_DETECT,                ///< Causes the system to perform the lock detect process on the selected UVP(s)
    CTRL_UVP_READ_DATA,              ///< Read data from the selected UVP Register
    CTRL_UVP_WRITE_DATA,             ///< Write data to the selected UVP Register
    CTRL_SET_FANS_FULL,              ///< Used to override fans driver and put fans at full.
    CTRL_GET_FANS_FULL,              ///< Returns the override fans driver setting.
    CTRL_CHARGE_VIRTUAL,             ///< Causes the transmitter to enter TPS with no Receiver
    CTRL_SET_CFG_PARAM,              ///< Sets a register in the NVM config
    CTRL_GET_CFG_PARAM,              ///< Gets an nvm register value
    CTRL_UVP_MEM_DUMP,               ///< Starts a memory dump of single UVP
    CTRL_SYS_CLK_DUMP,               ///< Dumps system clock registers
    CTRL_RPI_HOST_DATA,              ///< Send data from RPI to Host MCU
    CTRL_SYS_CLK_WRITE,              ///< Sets a system clock register
    CTRL_SYS_CLK_READ,               ///< Reads a system clock register
    CTRL_SYS_CLK_I2C_DISABLE,        ///< Disable the I2C port to system clock chip so it can be programmed externally
    CTRL_BOOTLOADER,                 ///< Reboots the MCU into the bootloader
    CTRL_SYS_DOOR_OPEN,              ///< Indicates a door has opened
    CTRL_SYS_DOOR_CLOSED,            ///< Indicates a door has closed

    /*
      Below are special messages that are designated for
      responses. These values will grow from 255 down. 
     */
    CTRL_LOG_INFO            = 243,  ///< Log message from M7 containing info level data
    CTRL_LOG_WARN            = 244,  ///< Log message from M7 containing warning level data
    CTRL_LOG_ERROR           = 245,  ///< Log message from M7 containing error level data
    CTRL_LOG_DEBUG           = 246,  ///< Log message from M7 containing debug level data
    CTRL_LOG_FAULT           = 247,  ///< Log message from M7 containing fault level data
    CTRL_SYS_CLK_DUMP_DATA   = 248,  ///< Identifies the system clock registers data
    CTRL_UVP_MEM_DUMP_DATA   = 249,  ///< Identifies the memory dump data of a single UVP
    CTRL_RSSI_REPORT_DATA    = 250,  ///< RSSI Report data for each valid AMB and all UVPs
    CTRL_LOCK_DETECT_DATA    = 251,  ///< UVP lock detect data. 
    CTRL_UVP_DATA            = 252,  ///< UVP read data
    CTRL_REDUCED_RCVR_INFO   = 253,  ///< Exists only as a return message. Send reduced Receiver info to the MM
    CTRL_ERROR_ENUM          = 254   ///< Returns just an enum value that represents an error. 
} CtrlMsgId_t;

CotaError_t CtrlInterfaceInit(void);
uint16_t CtrlGetMsg(uint8_t *buf, uint16_t size);

/*
 This function truncates the CtrlMsgId_t to be a uint8_t. This is because
  I am unable to figure out how to force enums to be 1-byte. For right now,
  this will do.
*/
uint16_t CtrlSendMsg(CtrlMsgId_t msgId, uint8_t *buf, uint16_t size);

/*
  Typedefs
 */

/** @brief MSG data for MsgId CTRL_REGISTER_RCVR */
typedef struct _MsgRegRcvr_t
{
    ExtdAddr_t longId;          ///< The LongId of the Receiver we are registering with the system.
    CotaRcvrQuery_t  queryType; ///< The query type for the Receiver we are registering. 0 means make default.
} MsgRegRcvr_t;

/** @brief MSG data for MsgId CTRL_REMOVE_RCVR */
typedef struct _MsgRmRcvr_t
{
    ExtdAddr_t longId; ///< the LongId of the Receiver we are removing from the system.
} MsgRmRcvr_t;

/** @brief Msg data for MsgId CTRL_GET_RCVR_DETAIL */
typedef struct _MsgRcvrDetail_t 
{
    ExtdAddr_t longId; ///< the LongId of the Receiver we are requesting Receiver data on. 
} MsgRcvrDetail_t;

/** @brief Msg data for CTRL_SET_POWER_LEVEL */
typedef struct _MsgSetPowerLevel_t
{
    uint16_t powerLevel; ///< The power level that the transmitter will be set to.
} MsgSetPowerLevel_t;

/** @brief Msg data for CTRL_START_CHARGING_DEVICES */
typedef struct _MsgStartCharging_t
{
    ExtdAddr_t longId; ///< The long Id that we want to stop charging. 0xFFFFFFFFFFFFFFFF means 'all clients'
} MsgStartCharging_t;

/** @brief Msg data for CTRL_STOP_CHARGING_DEVICES */
typedef struct _MsgStopCharging_t
{
    ExtdAddr_t longId; ///< The long Id that we want to stop charging. 0xFFFFFFFFFFFFFFFF means 'all clients'
} MsgStopCharging_t;


/** @brief Msg data for CTRL_TX_FREQ */
typedef struct _MsgTxFreq_t
{
    uint16_t frequency; ///< The target frequency for the transmitter. 0 will make txFreq = commChFreq
} MsgTxFreq_t;

/** @brief For MSG with MsgId CTRL_SET_CLK_CFG */
typedef PACKED struct _MsgSetClockConfig_t
{
    uint8_t mode;  ///< Set the clock configuration between several possible clock modes 
} MsgSetClockConfig_t;

/** @brief For MSG with MsgId CTRL_SET_COMM_CHANNEL */
typedef PACKED struct _MsgSetCommChannel_t 
{
    uint8_t channel;   ///< The communication channel that the proxy will be set to.
    ExtdAddr_t longId; ///< The long Id that we want to stop charging. 0xFFFFFFFFFFFFFFFF means 'all clients'
} MsgSetCommChannel_t;

/** @brief For MSG with MsgId CTRL_PM_EN */
typedef PACKED struct _MsgPowerMonitorEnable_t
{
    bool value; ///< True to enable the power monitor, false to disable it. 
} MsgPowerMonitorEnable_t;

/** @brief For MSG with MsgId CTRL_FANS_FULL */
typedef PACKED struct _MsgFansFull_t
{
    uint8_t fullFans; ///< 1 - fans on full, 0 - Let fan driver control fans
} MsgFansFull_t;

/** @brief For MSG with MsgId CTRL_SET_PU_EN */
typedef PACKED struct _MsgSetPuOn_t
{
    AmbMask_t ambMask;  ///< A mask that defines the AMBs that will be affected by enable. 
    uint8_t   enable;   ///< 1 - enables the AMBs in the mask, 0 - disable them
} MsgSetPuOn_t;

/** @brief For MSG with MsgId CTRL_SET_SPI_ENABLE */
typedef PACKED struct _MsgSetSpiEnable_t
{
    AmbMask_t ambMask;  ///< A mask that defines the AMBs that will be affected by enable. 
    uint8_t   enable;   ///< 1 - enables the AMBs in the mask, 0 - disable them
} MsgSetSpiEnable_t;

/** @brief For MSG with MsgId CTRL_SET_TX */
typedef PACKED struct _MsgSetTx_t
{
    uint8_t enable; ///< 1 - enables the Tx pin, 0 - disables it
} MsgSetTx_t;

/** @brief For MSG with MsgId CTRL_SET_RX */
typedef PACKED struct _MsgSetRx_t
{
    uint8_t enable; ///< 1 - enables the Rx pin, 0 - disables it
} MsgSetRx_t;

/** @brief For MSG with MsgId CTRL_SET_PU_ALL */
typedef PACKED struct _MsgSetPuAll_t
{
    uint8_t enable; ///< 1 - enables the PU ALL on pin, 0 - disables it
} MsgSetPuAll_t;

/** @brief For MSG with MsgId CTRL_RSSI_FILTER_EN */
typedef PACKED struct _MsgRssiFilterEn_t
{
    uint8_t enable; ///< 1 - enables RSSI voting, 0 - disable RSSI voting
} MsgRssiFilterEn_t;

/**
 * @brief Parameters to the #CTRL_SET_RCVR_CONFIG command
 */
typedef PACKED struct _MsgSetQueryTypeParams_t
{  
    ExtdAddr_t      longId;     ///< The long id of the Receiver to set query type for
    CotaRcvrQuery_t type;       ///< The query type (currently 5 or 6)
} MsgSetQueryType_t;

/** @brief message for MsgId CTRL_UVP_READ_DATA */
typedef PACKED struct _MsgUvpRead_t
{
    UvpRegRead_t uvpData; ///< UVP read data
} MsgUvpRead_t;

/** @brief message for MsgId CTRL_UVP_WRITE_DATA */
typedef PACKED struct _MsgUvpWrite_t
{
    UvpRegWrite_t uvpData; ///<  UVP Write data
} MsgUvpWrite_t;

/** @brief message for MsgId CTRL_LOCK_DETECT */
typedef PACKED struct _MsgLockDetect_t
{ 
    AmbUvpMask_t  uvpData;  ///< Amb and Uvp information for reading data. 
} MsgLockDetect_t;

/** @brief Msg data for CTRL_PROXY_COMMAND */
typedef PACKED struct _MsgProxyCommand_t
{
    PrxHostProxyCmdMsg_t command; ///< Proxy command data
} MsgProxyCommand_t;

/** @brief Msg data for CTRL_APP_COMMAND */
typedef PACKED struct _MsgAppCommand_t
{
    PrxHostClientCmdMsg_t command; ///< Client command data
} MsgAppCommand_t;

/** @brief Msg data for CTRL_SET_RCVR_CONFIG */
typedef PACKED struct _MsgSetRcvrConfig_t
{
    ExtdAddr_t longId;          ///< Long Id of the Receiver in question
    CotaRcvrQuery_t  queryType; ///< The query type for the Receiver we are registering. 0 means make default.
} MsgSetRcvrConfig_t;

/**
 * @brief Msg data for CTRL_RCVR_LIST
 */
typedef PACKED struct _MsgRcvrList_t
{
    uint8_t all; ///< 1 request to send all clients, 0 omits silent clients
} MsgRcvrList_t;

/** @brief Msg data for CTRL_GET_CONFIG_PARAM */
typedef PACKED struct _MsgGetConfigParam_t
{
    ConfigParamId_t paramId; ///< Config parameter id
} MsgGetConfigParam_t;

/** @brief Msg data for CTRL_SET_CONFIG_PARAM */
typedef PACKED struct _MsgSetConfigParam_t
{
    ConfigParamId_t paramId; ///< Config parameter id
    uint64_t        val;     ///< Config value
} MsgSetConfigParam_t;

/** @brief Msg data for CTRL_SEND_DISCOVERY_MESSAGE */
typedef PACKED struct _MsgSendDisc_t
{
    uint8_t channel; ///< 0 for system channel, else for specific channel
} MsgSendDisc_t;


/**
 * @brief Msg data for CTRL_SET_RSSI_THRESH
 *        This struct includes the number of samples to average over
 *        and a deviation, in dBm, to subtract from the samples so the
 *        rssi threshold is at a value lower than mean rssi value.
 *        That way during rssi voting, a standard signal will pass but
 *        a low signal will fail.
 */
typedef PACKED struct _MsgSetRssiThreshold_t
{
    uint8_t nSamples;   ///< Number of samplde forming the rssi thresh average
    uint8_t nDevIndBm;  ///< The deviation is dBm to subratct from each threshold
} MsgSetRssiThreshold_t;

/** @brief Msg data for CTRL_SET_RSSI_THRESH */
typedef PACKED struct _MsgAppCommandData_t
{
    ExtdAddr_t longId; ///< The long Id of the target Receiver data. 
} MsgAppCommandData_t;

/**
 * @brief Msg data for CTRL_UVP_MEM_DUMP
 */
typedef PACKED struct _MsgUvpMemDumpCallback_t
{
    AmbNum_t ambNum;        ///< The AMB where the UVP is located 
    UvpNum_t uvpNum;        ///< Number of UVP to dump the memory of
} MsgUvpMemDumpCallback_t;

/**
 * @brief  Static charge arguments
 */
typedef PACKED struct _MsgStaticCharge_t
{
    uint8_t beat;     ///< Beat to freeze the TPS cycle on
    uint8_t hours;    ///< The number of hours the Receiver will stay in harvest mode
    uint8_t minutes;  ///< The number of minutes the Receiver will stay in harvest mode
} MsgStaticCharge_t;

/** @brief Msg data for CTRL_SET_UVP_EN */
typedef PACKED struct _MsgSetUvpOn_t
{
    AmbMask_t ambMask; ///< The AMB mask whose UVPs will be enabled.
    uint8_t enable;    ///< Enable 0 off 1 on
} MsgSetUvpOn_t;

/** @brief Msg data for CTRL_SET_AMB_EN */
typedef PACKED struct _MsgSetAmbOn_t
{
    uint8_t ambMask; ///< Mask that defines which AMBs will be affected by enable. 
    uint8_t enable;  ///< Enable 0 off 1 on
} MsgSetAmbOn_t;

/**
 * @brief  RPi to Host data payload for CTRL_RPI_HOST_DATA
 */
typedef PACKED struct _MsgRpiHostData_t
{
    uint8_t data[MAX_RPI_HOST_DATA_MSG_SIZE]; ///< Generic message poyload received from the RPi
} MsgRpiHostData_t;

/**
 * @brief system clock write arguments
 */
typedef PACKED struct _MsgSysClkWrite_t
{
    uint16_t addr;      ///< The system clock register to write to
    uint8_t  val;       ///< The value to write to.
} MsgSysClkWrite_t;

/**
 * @brief system clock read arguments
 */
typedef PACKED struct _MsgSysClkRead_t
{
    uint16_t addr;      ///< The system clock register to write to
} MsgSysClkRead_t;

/**
 * @brief Uvp select param
 */
typedef PACKED struct _MsgSetSelectUvp_t
{
    uint8_t uvpNum;      ///< The uvp number to select 0-15, 16 for all.
} MsgSetSelectUvp_t;

/**
 * @brief Uvp Init params
 */
typedef PACKED struct _MsgUvpInit_t
{
    AmbMask_t  ambMask;      ///< The AMB mask to init
    UvpMask_t  uvpMask;      ///< The UVP mask to init
} MsgUvpInit_t;

/**
 * @brief  A ctrl message includes all input possibilities for all
 *         incoming messages to the ctrl task. It defines the format,
 *         and also list all the possible payloads. 
 */
typedef PACKED struct _CtrlMsg_t
{
    CtrlMsgId_t id; 
    
    PACKED union
    {
        MsgRegRcvr_t            registerRcvr; 
        MsgRmRcvr_t             removeRcvr;
        MsgSetPowerLevel_t      setPowerLevel;
        MsgRcvrDetail_t         rcvrDetail;
        MsgStartCharging_t      startCharging;
        MsgStopCharging_t       stopCharging;
        MsgTxFreq_t             txFreq;
        MsgSetClockConfig_t     setClockConfig;
        MsgSetCommChannel_t     setCommChannel;
        MsgPowerMonitorEnable_t powerMonitorEnable;
        MsgUvpRead_t            uvpRead;
        MsgUvpWrite_t           uvpWrite; 
        MsgLockDetect_t         lockDetect;
        MsgProxyCommand_t       proxyCommand;
        MsgAppCommand_t         appCommand;
        MsgSetRcvrConfig_t      setRcvrConfig;
        MsgGetConfigParam_t     getConfigParam;
        MsgSetConfigParam_t     setConfigParam;
        MsgSendDisc_t           sendDisc;
        MsgSetRssiThreshold_t   setRssiThreshold;
        MsgAppCommandData_t     appCommandData;
        MsgUvpMemDumpCallback_t uvpMemDump;
        MsgStaticCharge_t       staticCharge;
        MsgSetRssiThreshold_t   rssiThresh;
        MsgRcvrList_t           rcvrListAll;
        MsgFansFull_t           fansFull;
        MsgRssiFilterEn_t       rssiFilterEn;
        MsgRpiHostData_t        rpiHostData;
        MsgSysClkWrite_t        sysClkWriteArgs;
        MsgSysClkRead_t         sysClkReadArg;        

        // Here begins debug message definitions.
        MsgSetSelectUvp_t       setUvpNum;
        MsgSetSpiEnable_t       setSpiEnable;
        MsgSetPuOn_t            setPuOn;
        MsgSetRx_t              setRx;
        MsgSetTx_t              setTx;
        MsgSetPuAll_t           setPuAll;
        MsgSetUvpOn_t           setUvpOn;
        MsgSetAmbOn_t           setAmbOn;
        MsgUvpInit_t            setUvpInit;
    };
} CtrlMsg_t;

/** @brief For Response  CTRL_GET_FANS_FULL */
typedef PACKED struct _RespFansFull_t
{
    uint8_t fullFans; ///< 1 - fans on full, 0 - Let fan driver control fans
} RespFansFull_t;

/** @brief Response data for CTRL_GET_POWER_LEVEL */
typedef PACKED struct _RespGetPowerLevel_t
{
    uint16_t powerLevel; ///< The power level of the transmitter. 
} RespGetPowerLevel_t;

/** @brief Response data for CTRL_SET_POWER_LEVEL */
typedef PACKED struct _RespSetPowerLevel_t
{
    uint16_t powerLevel; ///< The actual power level. We respond with the actual power level give user input. 
} RespSetPowerLevel_t;

/** @brief Response data for CTRL_GET_RCVR_DETAIL */
typedef PACKED struct _RespRcvrDetail_t
{
    Client_t receiver; ///< Rcvr data
} RespRcvrDetail_t;

/** @brief Response data for CTRL_AMB_EN */
typedef PACKED struct _RespGetAmbOn_t
{
    uint8_t ambMask; ///< Mask that describes the which AMBs are enabled. 
} RespGetAmbOn_t;

/** @brief Response data for CTRL_GET_PU_ALL */
typedef PACKED struct _RespGetPuAll_t
{
    uint8_t puOn; ///< 1 represents on, 0 represents off. 
} RespGetPuAll_t;

/** @brief Response data for CTRL_GET_UVP_EN */
typedef PACKED struct _RespGetUvpOn_t
{
    uint8_t ambMask; ///< Amb mask which has enabled UVPs 
} RespGetUvpOn_t;

/** @brief Response data for CTRL_GET_SELECTED_UVP */
typedef PACKED struct _RespGetSelectedUvp_t
{
    uint8_t uvpNum; ///< Number which represents the uvp that are selected for commands. 
} RespGetSelectedUvp_t;

/** @brief Response data for CTRL_GET_PU_EN */
typedef PACKED struct _RespGetPuOn_t
{
    uint8_t puOn; ///< 1 represents on, 0 represents off
} RespGetPuOn_t;

/** @brief Response data for CTRL_GET_TX */
typedef PACKED struct _RespGetTxOn_t
{
    uint8_t txOn; ///< 1 for on, 0 for off
} RespGetTxOn_t;

/** @brief Response data for CTRL_GET_RX */
typedef PACKED struct _RespGetRxOn_t
{
    uint8_t rxOn; ///< 1 for on, 0 for off
} RespGetRxOn_t;

/** @brief Response data for CTRL_GET_PD_DONE */
typedef PACKED struct _RespGetPdDone_t
{
    uint8_t ambMask; ///< ambMask that represents state of pd done for each AMB as a mask. 
} RespGetPdDone_t;

/** @brief Response data for CTRL_GET_SPI_EN */
typedef PACKED struct _RespGetSpiEn_t
{
    AmbMask_t ambMask; ///< ambMask that represents which ambs have the spi enabled
} RespGetSpiEn_t;

/** @brief Response data for CTRL_GET_SYSTEM_STATE */
typedef PACKED struct _RespGetSystemState_t
{
    uint8_t state; ///< Represents the system state.
} RespGetSystemState_t;

/**
 * @brief Response data for CTRL_GET_SYSTEM_TEMP
 */
typedef PACKED struct _RespGetSystemTemp_t
{
    int16_t temp[MAX_NUM_AMB + TEMP_CCB_NUM_OF_THERM]; ///< Temperature values
} RespGetSystemTemp_t;

/** @brief Msg data for CTRL_GET_CONFIG_PARAM */
typedef PACKED struct _RespGetConfigParam_t
{
    uint64_t val; ///< The value of the requested config param
} RespGetConfigParam_t;

/** @brief Resp data for CTRL_APP_COMMAND_DATA */
typedef PACKED struct _RespAppCommandData_t
{
    CotaAppData_t data;   ///< Rx command data response 
} RespAppCommandData_t;

/** @brief Resp data for CTRL_PROXY_COMMAND_DATA */
typedef PACKED struct _RespProxyCommandData_t
{
    PrxHostPrxCmdResp_t data;   ///< Proxy command
} RespProxyCommandData_t;

/** @brief Response data for CTRL_PROXY_INFO */
typedef PACKED struct _RespProxyInfo_t
{
    PrxHostProxyFwInfo_t info; ///< Proxy info struct
} RespProxyInfo_t;

/** @brief for Resp for MsgId CTRL_UVP_READ_DATA */
typedef PACKED struct _RespUvpRead_t
{
    AmbUvpNum_t uvpId;  ///< Amb and uvp number information for denoting which UVP is in question.
    uint32_t    val;    ///< The value of the register that we read from. 
} RespUvpRead_t;

/** @brief for Resp for MsgId CTRL_LOCK_DETECT */
typedef PACKED struct _RespLockDetect_t
{
    AmbUvpNum_t uvpId;   ///< Amb and uvp number information for denoting which UVP is in question.
    uint8_t     lock;    ///< 1 = locked, 0 = unlocked 
    uint8_t     cfsVal;  ///< the value written to the VCOCFS register that caused the lock to be 1
} RespLockDetect_t; 


/** @brief for Resp for MsgId CTRL_SYS_CLK_READ */
typedef PACKED struct _RespSysClkRead_t
{
    uint8_t val;       ///< The value of the system clock register
} RespSysClkRead_t; 

/**
 * @brief This is the a reduced set of Receiver info that is returned
 *          during select operations. For now, this is when the
 *          clients start charging, and when a ClientList is
 *          requested. For MsgId CTRL_REDUCED_RCVR_INFO
 */
typedef PACKED struct _RespReducedRcvrInfo_t
{
    ExtdAddr_t         longId;        ///< Long id of a Receiver 
    ShrtAddr_t         shortId;       ///< Short id of a Receiver
    ClientState_t      state;         ///< The state of the Receiver
    uint8_t            batteryCharge; ///< The battery change level of the Receiver
    CotaClientStatus_t statusFlags;   ///< Flags that describe the behavior of the Receiver
    uint8_t            avgPower;      ///< The average power the Receiver is receiving 
    uint8_t            peakPower;     ///< The peak power the Receiver has received
    int8_t             rssi;          ///< The RSSI value that the Receiver is receiving.
    uint8_t            linkQuality;   ///< The quality of the Receiver's link to the proxy.
    int16_t            netCurrent;    ///< The net current of the Receiver (- for discharging, + for charging)
} RespReducedRcvrInfo_t;

/**
 *  @brief Holds information for one line of a RSSI voting report
 */
typedef PACKED struct _respRssiFilterReportLine_t
{
    AmbNum_t    ambNum;         ///< The AMB number for this AMU
    UvpNum_t    uvpNum;         ///< The UVP number for this AMU
    AmuNum_t    amuNum;         ///< The number of this AMU
    uint8_t     amuEn;          ///< 1 - The AMU is enabled, 0 - The AMU is not enabled
    uint16_t    rssiVal;        ///< The rssi value for this AMU
    uint16_t    phase;          ///< The phases measured for this AMU
    uint16_t    phaseOffset;    ///< Phase offset from calibration for this AMU
    uint16_t    rssiThresh;     ///< The minimum RSSI threshold for the UVP for this AMU
} RespRssiFilterReportLine_t;

/**
 * @brief for Resp for MsgId CTRL_UVP_MEM_DUMP
 */
typedef PACKED struct _RespUvpMemDump_t
{
    uint32_t    data;       ///< 32 bit chunk of data
    uint8_t     colAddr;    ///< Column address of the data
    uint8_t     pageIndex;  ///< Page index of the data
} RespUvpMemDump_t;

/**
 * @brief for Resp for MsgId CTRL_AMB_FILTER_DISABLED
 */
typedef PACKED struct _RespAmbDisableDump_t
{
    AmbMask_t ambDisabled;  //Mask indicating which AMB's are disabled
} RespAmbDisableDump_t;

/**
 * @brief for Resp for MsgId CTRL_SYS_CLK_DUMP
 */
typedef PACKED struct _RespSysClkDump_t
{
    uint16_t addr;  ///< Address of the system clock register
    uint8_t  val;   ///< Value of the system clock register
} RespSysClkDump_t;  

/** @brief Msg data for CTRL_GET_FIRMWARE_VERSION */
typedef PACKED struct _RespVersion_t
{
    uint16_t    major;                         ///< Software major release version
    uint16_t    minor;                         ///< Software minor release version
    uint32_t    commit;                        ///< Git commit hash
    uint32_t    proxyRevision;                 ///< The proxy revision data. Higher 16-bits is Major, lower 16 is minor revision
    BuildType_t bldType;                       ///< Software build type
    uint8_t     timestamp[MAX_TIMESTAMP_SIZE]; ///< Timestamp when current image was compiled.                      
} RespVersion_t;

/**
 * @brief for Resp for MsgId CTRL_RPI_HOST_DATA
 */
typedef PACKED struct _RespRpiHostData_t
{
    uint8_t data[MAX_RESP_RPI_HOST_DATA_MSG_SIZE]; ///< Generic message poyload returned from the host to RPi
} RespRpiHostData_t;

/** @brief Response data for CTRL_ERROR_STRING */
typedef PACKED struct _RespLog_t
{
    char buff[MAX_LOG_STRING_SIZE]; ///< Char buffer that is null terminated 
} RespLog_t; 

/** @brief Response data for CTRL_GET_RSSI_FILTER_EN */
typedef PACKED struct _RespGetRssiFilterEn_t
{
    uint8_t enable; ///< enable return value 0 or 1
} RespGetRssiFilterEn_t; 

/**
 * @brief A ctrl message response include all output possibilities in
 *          response to an incoming messages.
 */
typedef PACKED struct _CtrlResp_t
{
    CtrlMsgId_t id; 
    CotaError_t err;
    
    PACKED union 
    {
        RespRcvrDetail_t            rcvrDetail;
        RespSetPowerLevel_t         setPowerLevel;
        RespGetPowerLevel_t         getPowerLevel;
        RespGetRssiFilterEn_t       getRssiFilterEn;
        RespGetSystemState_t        getSystemState;
        RespGetSystemTemp_t         getSystemTemp;
        RespGetConfigParam_t        getConfigParam;
        RespProxyInfo_t             proxyInfo;
        RespAppCommandData_t        appCommandData;
        RespProxyCommandData_t      proxyCommandData;
        RespRssiFilterReportLine_t  rssiReportLine;
        RespUvpMemDump_t            uvpMemDump;
        RespSysClkDump_t            sysClkDump;
        RespVersion_t               version; 
        RespFansFull_t              fansFull;
        
        // Here begins debug message definitions
        RespGetAmbOn_t              getAmbOn;
        RespGetPuAll_t              getPuAll;
        RespGetUvpOn_t              getUvpOn;
        RespGetSelectedUvp_t        getSelectedUvp;
        RespGetPuOn_t               getPu;
        RespGetTxOn_t               getTx;
        RespGetRxOn_t               getRx;
        RespGetPdDone_t             getPd;
        RespGetSpiEn_t              getSpiEn;
        
        // These are special commands that are not associated with any
        // specific command, but are used to communicate meta-data
        // back to the MM.
        RespLog_t                   logMessage; 
        RespUvpRead_t               uvpRead;           // Used for UVP read data
        RespLockDetect_t            lockDetect; 
        RespReducedRcvrInfo_t       reducedRcvrInfo; // Used for Start Charging and Rcvr List
        RespAmbDisableDump_t        ambDisabled;
        RespRpiHostData_t           rpiHostData;
        RespSysClkRead_t            sysClkRead;
    };
} CtrlResp_t;

#endif // #ifndef _CTRL_INTERFACE_H_

