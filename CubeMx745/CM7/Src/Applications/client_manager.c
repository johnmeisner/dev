/**
 * @file       client_manager.c
 *
 * @brief      Client data structure manager
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 *
 * @brief The client manager project is intended to be a wrapper
 *          abstraction layer between the client_interface (Which
 *          holds all the interface functions, i.e. the highest level
 *          of abstraction) and a data structure for organizing
 *          clients. At this time, there is only one data structure,
 *          and that structure is client_avl_tree.c/.h
 *
 *        The client_manager API follows a read/modify/write
 *          mindset. That is, a bulk of this abstraction layer is
 *          built off of:
 *            - GetClient()
 *            - SaveClient()
 *
 *
 *          where GetClient obtains the client information (read), the
 *          user modifies the data, then saves it back to the client
 *          manager with SaveClient (write).
 *
 *        In addition to this, there is RemoveClient, which as the
 *          name entails will remove the data from the structure. In
 *          order to accomplish this, the biggest consideration is
 *          with SaveClient.
 *
 *        If SaveClient() receives a longId from a client that was
 *          previous saved in the client manager, then that client's
 *          data will be overwritten. This means that all SaveClient()
 *          calls should be paired with a GetClient() so that the
 *          caller only updates pertinent info for a client.
 *
 *
 * As far as the transmitter is concerned, the clients can be put into the following
 * states. Each state holds not only the state description, but also points to the
 * storage for those clients. For detailed descriptions on each state, see the state
 * declaration sections. Note, this is the transmitter's knowledge of the state of
 * the client in question and may or may not concur with the state the client believes
 * it should be in.
 * @verbatim
 *
 *
 *                              --------------------
 *                              |                   |
 * Remove Via timer timeout <---|    Discovered     |
 *                              |                   |
 *                              --------------------
 *                                        |
 *                                        | Register the client
 *                                        |
 *                                       \|/
 *                              --------------------
 *                              |                   |  _______________
 *                              |       Join        | /                 V
 *    Remove device command <---|                   ||    Send Joins    |
 *                              |   Confirmation    |^_________________/
 *                              |                   |
 *                              --------------------
 *                                        |
 *                                        | Send Join msg, receive acknowledgment
 *                                        |
 *                                       \|/
 *                              --------------------
 *                              |                   |  _______________
 *                              |       TPC         | /                 V
 * Remove Via timer timeout <---|                   ||    Send TPCs     |
 *                              |   Confirmation    |^_________________/
 *                              |                   |
 *                              --------------------
 *                                       |
 *                                       | Send TPC msg, receive acknowledgment
 *                                       |
 *                                      \|/
 *                              --------------------   _______________
 *                              |                   | /                 V
 *    Remove device command <---|      Ready        ||   Send Queries   |
 *                              |                   |^_________________/
 *                              --------------------
 *
 * @endverbatim
 * The job of the client manager is to manage the database of clients on the transmitter, and determine
 * which clients need to receive messages (which is done implicitly given the state
 * that the client resides in). The control task (ctrl_task.c) determines WHEN a client gets these
 * messages, and passes the acknowledgments to the client via client manager. This will also cause
 * the client to move states.
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "client.h"
#include "client_manager.h"
#include "client_avl_tree.h"
#include "CotaCommonTypes.h"
#include "orion_config.h"
#include "client_interface.h"
#include "client_avl_tree.h"

#define USE_AVL_TREE

#ifdef USE_AVL_TREE
ClientTree_t tree;
#endif

/*
 * Helper functions
 */

void InitializeClientStruct(Client_t *client)
{
    //Client_t client
    memset(client->longId.bytes, 0, EXTADDR_LEN);
    client->shortId = 0;
    client->version = 0;
    client->hwModel = 0;
    client->isValid = 0;
    //ClientQueryDataCstm_t qStatus
    //CotaClientQueryHeader_t header
    client->qStatus.header.clientShortAddr  = 0;
    client->qStatus.header.linkQuality      = 0;
    client->qStatus.header.rssi             = 0;
    client->qStatus.header.queryType        = RCVR_QUERY_RESERVED_1;
    //CotaStatusStd_t status
    client->qStatus.status.peakPower        = 0;
    client->qStatus.status.avgPower         = 0;
    client->qStatus.status.peakNetCurrent   = 0;
    client->qStatus.status.peakPower        = 0;
    client->qStatus.status.batteryCharge    = 0;
    //CotaTrackerStatus_t trackerStatusFlags
    client->qStatus.status.trackerStatusFlags.powerRq           = 0;
    client->qStatus.status.trackerStatusFlags.clientRxCharge    = 0;
    client->qStatus.status.trackerStatusFlags.targetRxCharge    = 0;
    client->qStatus.status.trackerStatusFlags.errorStatus       = 0;
    client->qStatus.status.trackerStatusFlags.missedTpsCycles   = 0;
    client->qStatus.status.trackerStatusFlags.sleepFlag         = 0;
    //
    client->qStatus.size = 0;
    memset(client->qStatus.data, 0, MAX_CUSTOM_DATA_SIZE);
    //ClientState_t state
    client->state = RCVR_STATE_STATE_UNKNOWN;
    //CotaAppData_t
    memset(client->appData.data, 0, MAX_APP_MESSAGE_SIZE);
    //
    client->priority = 0;
    client->userChargeRequested = 0;
    client->unschedQueryCount = 0;
    client->queryNeededFlag = false;
    client->lastCommTimeTicks = 0;
    client->commFailCount = 0;
    client->appCmdTriesLeft = 0;
    client->fatalLow = 0;
}

/**
 * @brief Initializes every component the clientManager relies on for initialization
 * @return COTA_ERROR_NONE on success, COTA_ERROR_RCVR_MANAGER_INIT_FAILED on failure
 */
CotaError_t InitClientManager(void)
{
    // Initialize AVL Tree Data Structures
    #ifdef USE_AVL_TREE
    memset(&tree.nodeArray, 0, sizeof(ClientNode_t)*MAX_NUM_CLIENTS);
    tree.size = MAX_NUM_CLIENTS;
    tree.lastClient = 0;
    tree.head = NULL;
    #endif

    return COTA_ERROR_NONE;
}

#ifdef USE_AVL_TREE
/**
 * @brief Gets a longId from a shortId.
 * @param shortId - The shortId that is being searched for.
 * @param longId - An output pointer to a longId
 * @return bool - If the longId was found successfully.
 */
bool GetLongId(ShrtAddr_t shortId, ExtdAddr_t *longId)
{
    Client_t client;

    SearchTreeShortId(shortId, tree.head, &client);
    memcpy((void*)longId, (void*)&client.longId, sizeof(ExtdAddr_t));

    return IsLongValid(*longId);
}

/**
 * @brief Read a client structure given a longId
 * @param longId - A client's longId
 * @param client - Output pointer that the client data is copied to
 * @return true if found correctly, false if otherwise.
 */
bool GetClient(ExtdAddr_t longId, Client_t *client)
{
    return SearchTree(longId, tree.head, client);
}

/**
 * @brief Saves a client in the AVL tree. If the ID was already
 *          logged, then the data is overwritten.
 * @note  Do not call this function from within a walk function
 * @param client - Client data to save
 * @return true on success, false if otherwise.
 */
bool SaveClient(Client_t client)
{
    // Insert node has a special characteristic to overwrite the data
    // in the node if the id was already registered. This is an
    // intentional feature. This helps the Read, Modify, Write
    // direction of this module.
    CotaError_t err = InsertNode(client, &(tree.head), &tree);
    return ((err == COTA_ERROR_NONE) || (err == COTA_ERROR_RCVR_ALREADY_REGISTERED));
}

/**
 * @brief Removes a client from the AVL Tree.
 * @param longId - longId of a Client
 * @return returns true if successful, false if otherwise
 */
bool RemoveClient(ExtdAddr_t longId)
{
    return (DeleteNode(longId, &(tree.head), NULL) == COTA_ERROR_NONE);
}

/**
 * @brief This function will walk the AVL tree and apply function func
 *         to each client in the tree. The idea is to use this
 *         function for all interactions with the clients as is
 *         possible.
 * @param func - A function that gets executed on type client.
 */
void Walk(void (*func)(Client_t* client))
{
    WalkTree(tree.head, func);
}

/**
 * @brief Memcpy's a client with state 'state' to address at 'client'
 * @param state - The state of the client we are targeting.
 * @param client Output client pointer to copy out client data.
 * @return true if the client with 'state' is found.
 */
bool FindClientState(ClientState_t state, Client_t *client)
{
    return WalkToFindState(state, tree.head, client);
}

/**
 * @brief Deletes all client data from the AVL tree
 */
void RemoveAllClients(void) {
    DestroyTree(&tree);
}
#endif



