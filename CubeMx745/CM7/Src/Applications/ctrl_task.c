/**
 * @file       ctrl_task.c
 *
 * @brief      The control task is the master control of the system. It
 *             processes the main state machine that controls the
 *             operation of the transmitter.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "FreeRTOS.h"
#include "projdefs.h"
#include "timers.h"
#include "semphr.h"
#include "task.h"
#include "ctrl_task.h"
#include "state_machine.h"
#include "orion_config.h"
#include "uart_driver.h"
#include "error.h"
#include "proxy_msg_interface.h"
#include "client.h"
#include "client_interface.h"
#include "ctrl_interface.h"
#include "proxyHostMsgInf.h"
#include "cli_task.h"
#include "proxy.h"
#include "sysclk_driver.h"
#include "nvm_driver.h"
#include "main.h"
#include "ccb_temperature.h"
#include "i2c_multiplex.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "fan_driver.h"
#include "calibrator.h"
#include "power_level.h"
#include "cqt_scheduler.h"
#include "tps_scheduler.h"
#include "rpi_msg_interface.h"
#include "power_monitor.h"
#include "gather_and_vote_task.h"
#include "process_uvp_commands.h"
#include "variable_queue.h"
#include "light_ring_interface.h"
#include "orion_utilities.h"
#include "version_from_source_control.h"
#include "print_format.h"
#include "proxy_fw_update_interface.h"
#include "log.h"
#include "sys_util.h"
#include "door_driver.h"
#include "gpio_interrupt.h"
#include "generic_sm.h"
#include "power_monitor_sm.h"
#include "proxy_test_sm.h"
#include "fan_sm.h"
#include "app_cmd_sm.h"
#include "query_sm.h"
#include "tps_sm.h"

#if defined(USE_DEMO_BUILD)
#include "Demo/demo_state_machine.h"
#include "Demo/disc_sm.h"
#include "Demo/light_ring_sm.h"
#include "Demo/demo_client_interface.h"

#elif defined(USE_FOREVER_TRACKER_BUILD)
#include "ForeverTracker/forever_tracker_state_machine.h"
#include "ForeverTracker/enumeration.h"
#include "ForeverTracker/wait_for_sleep.h"
#include "ForeverTracker/ft_client_interface.h"
#else
#error Build target not defined properly
#endif

// Semaphores
#define CTRL_CNT_SEM_MAX     10      ///< The maximum count that the gCtrlSem can iterate to.
#define CTRL_CNT_SEM_MIN     0       ///< The minimum count that the gCtrlSem can decrement to.

// Timers 
#define PROXY_MESSAGE_DATA   2       ///< The byte number for a proxy message where the data begins.

static BaseType_t CtrlTaskInit(void);
static void CtrlTask(void *pvParameters);
static void processProxyMessages(CtrlStates_t currState);
static void processCtrlCommands(CtrlStates_t currState);

static TaskHandle_t   gCtrlTaskHandle                = NULL;     ///< FreeRTOS task handle for the control task

static StateMachineState_t* gCtrlSmState  = NULL;     ///< Keeps track of the current state of the SM
static uint8_t      gRespBuf[sizeof(CtrlResp_t)];     ///< Response message buffer
/*
 * System Control Flags and State Machines
 */
#if defined(USE_FOREVER_TRACKER_BUILD)
static bool         gDoorClosed       = false;              ///< If true, a door closed message has been received, false, if an open door message is received.   Assume the door is open, the driver will send a message if it's closed.
#endif
static PrxHostPrxCmdResp_t  gProxyCmdData = {0};            ///< A global buffer for holding all of the command data

static uint8_t gIncomingMsgBuf[sizeof(CtrlMsg_t)];                      ///< Buffer for incoming message to be processed
static CotaError_t ProcessDebugCommand(uint8_t* buf, uint16_t size);

/**
 * @brief Runs the system-level state machine for controlling what the transmitter is 
 *          doing at any given moment.
 * @param pvParameters  All FreeRTOS tasks must have a pvParameters pointer parameter 
 */
static void CtrlTask(void *pvParameters)
{
    while (CtrlTaskInit() != pdPASS);

    // We must run the first entry function now b/c RunStateMachine() will not.
    gCtrlSmState->entryFcn();
    LogDebug("Ctrl task entry done\r\n");
    gCtrlSmState = RunStateMachine(gCtrlSmState);
    for (;;)
    {
        /*
          This task has two major processes to it.
            1) Process for all inter-task communication
            2) Run through the state machine and run whatever
               processes that entails.
          Here, we process queue messages before running the state machine
          as the state machine's actions might change depending on what
          we get from a message.
        */
        /**
         * @todo we may want to revisit this and using a signal systems using semaphores
         */
        vTaskDelay( pdMS_TO_TICKS(10));

        ProcessInterruptFlags();
        processProxyMessages((CtrlStates_t) gCtrlSmState->state);
        processCtrlCommands((CtrlStates_t) gCtrlSmState->state);
        gCtrlSmState = RunStateMachine(gCtrlSmState);
    }
}

/**
 * @brief Creates the instance of the CtrlTask in FreeRTOS
 */
void CreateCtrlTask(void)
{
    BaseType_t xReturned;
  
    xReturned = xTaskCreate(
                  CtrlTask,               // Function that implements the task. 
                  "Control Task",         // Text name for the task. 
                  CTRL_TASK_STACK_SIZE,   // Stack size in 32-bit words. 
                  NULL,                   // Parameter passed into the task. 
                  CTRL_TASK_PRIORITY,     // Priority at which the task is created. 
                  &gCtrlTaskHandle );     // Used to pass out the created task's handle. 
  
    while (xReturned != pdPASS);
}


/**
 * @brief Initializes all of the resources that are used by the CtrlTask
 * @return Returns pdPASS on success, and pdFAIL on failure. 
 */
static BaseType_t CtrlTaskInit(void)
{
    // Depending on how the project is configured, CtrlSmInit() can be
    // supplied by any of the x_state_machine.h definitions. See the
    // state machine definitions in the sub-directories of
    // Applications/
    gCtrlSmState = CtrlSmInit();

    if ( gCtrlSmState == NULL )
        return pdFAIL;

    return pdPASS;
}


/**
 * @brief This function will process all queue messages (AKA inter-task communication).
 *        The types of messages we process will differ depending on the current state
 *        of the control state machine.
 * @param currState The current state of the control task state machine #CtrlStates_t
 */
static void processProxyMessages(CtrlStates_t currState)
{
    uint8_t buf[CTRL_TASK_MAX_MSG_SIZE];
    ProxyHostMsgType_t msgType;
    
    if (PrxMsgRdy())
    {
        LogDebug("Message from Proxy Received\r\n");
        
        msgType = PrxGetMsg(buf, CTRL_TASK_MAX_MSG_SIZE);
        switch (msgType)
        {
#if defined(USE_DEMO_BUILD)
            case(PRX_HOST_CLIENT_DISCOVERED):
            {
                /// @todo remove Start/Stop discovery behavior in the proxy. Change to just SendDisc and timeout
                PrxHostDiscoveryMsg_t discResult;
                PrxExtractDiscoveryResp(&buf[PROXY_MESSAGE_DATA], CTRL_TASK_MAX_MSG_SIZE, &discResult);
                CmClientDiscovered(discResult);
                LogDebug("Client 0x%016llx responded to discovery\r\n");
            }
            break;
            
            case(PRX_HOST_CLIENT_JOINED):
            {
                PrxHostClientJoinRspMsg_t joinResult;
                PrxExtractJoinResp(&buf[PROXY_MESSAGE_DATA], CTRL_TASK_MAX_MSG_SIZE, &joinResult);
                CmClientJoined(joinResult);
                LogInfo("Client 0x%016llx has joined with short id 0x%04x\r\n",
                        *(uint64_t *) &joinResult.extAddr,
                        joinResult.shortAddr);
            }
            break;
            
            case(PRX_HOST_TPC_CFN):
            {
                PrxHostTpcCfn_t tpcResult;
                PrxExtractTpcResp(&buf[PROXY_MESSAGE_DATA], CTRL_TASK_MAX_MSG_SIZE, &tpcResult);
                CmClientTpcCfn(tpcResult);
                LogInfo("Client with short id 0x%04x has confirmed TPC\r\n",
                        tpcResult.shortAddr);
            }
            break;
#endif            
            case(PRX_HOST_CLIENT_QUERY_CFN):
            {
                PrxHostCqRecCfn_t queryCfn;
                PrxExtractQueryCfnResp(&buf[PROXY_MESSAGE_DATA], CTRL_TASK_MAX_MSG_SIZE, &queryCfn);
                CmClientQueryConfirm(queryCfn);
                LogInfo("Query successfully sent to receiver 0x%04x\r\n",
                        queryCfn.shortAddr);
            }
            break;
            
            case(PRX_HOST_CLIENT_QUERY_DATA):
            {
                ClientQueryDataCstm_t queryData;
                PrxExtractQueryDataResp(&buf[PROXY_MESSAGE_DATA], CTRL_TASK_MAX_MSG_SIZE, &queryData);
                // ClientQueryDataCstm_t has a buffer in it which is
                // memcpy'd. For this reason, a pointer is required.
                CmClientQueryData(&queryData);
                LogInfo("Client 0x%04x responded with query data\r\n",
                        queryData.header.clientShortAddr);
            } 
            break;
            
            case(PRX_HOST_TPS_GO):
            {
                if (!buf[PROXY_MESSAGE_DATA])
                {
                    LogError("Proxy failed to send TPS\r\n");
                }
            } 
            break;       
            
            case(PRX_HOST_DISCONNECT_CFN):
            {
                PrxHostLeaveNetworkCfn_t disconnectResult;
                PrxExtractDisconnectResp(&buf[PROXY_MESSAGE_DATA], CTRL_TASK_MAX_MSG_SIZE, &disconnectResult);
                LogInfo("Client 0x%04x has disconnected\r\n",
                        disconnectResult.shortAddr);
            }
            break;
            
            case(PRX_HOST_PROXY_DATA):
            {
                PrxExtractPrxCmdResp(&buf[PROXY_MESSAGE_DATA], sizeof(buf), &gProxyCmdData);
            }
            break;

            case(PRX_HOST_CLIENT_CMD_CFN):
            {
                PrxExtractAppCmdCfn(&buf[PROXY_MESSAGE_DATA], sizeof(buf));
            }
            break;
     
            // This is the response to a client command. 
            case(PRX_HOST_APP_DATA):
            {
                PrxHostAppData_t appCmdData;
                PrxExtractAppDataResp(&buf[PROXY_MESSAGE_DATA], sizeof(buf), &appCmdData);
                CmSetClientAppData(appCmdData.shortAddr, &appCmdData.data);
                LogDebug("A client's app command data has been received\r\n");
            }
            break;
                    
            case(PRX_HOST_PROXY_INFO):
            {
                PrxUpdateProxyInfo(&buf[PROXY_MESSAGE_DATA],sizeof(buf));
                LogInfo("Proxy info has been received\r\n");
            }
            break;
            
            case(PRX_HOST_PROXY_INIT_REQUEST):
            {
                PrxSendReqInfo();
                LogInfo("Requesting proxy initialization request\r\n");
            }
            break;

            case(PRX_HOST_PROXY_STATUS):
            {
                POST_COTA_ERROR(PrxSendProxySetup());
                PrxUpdateProxyStatus(&buf[PROXY_MESSAGE_DATA],sizeof(buf));
            }
            break;
            
#if defined(USE_FOREVER_TRACKER_BUILD)

            case(PRX_HOST_ANNOUNCEMENT):   
            {
                PrxHostAnnouncement_t announcement = {0};
                Client_t client = {0};
                
                PrxExtractAnnoucement(&buf[PROXY_MESSAGE_DATA], sizeof(buf), &announcement);

                if (IsDoorClosed() &&
                    IsLongIdValidTiMALAddr(announcement.extAddr))
                {   
                    LogInfo("Announcement received for 0x%016llx\r\n",
                         *(uint64_t *) &announcement.extAddr);
                    CmReportAnnouncement(announcement);
                    CmGetClientInfo(announcement.extAddr, &client);
                    PrxSendReceiverConfig(client.longId, client.shortId);
                }
            }
            break;           

            case(PRX_HOST_RCVR_CONFIG_RSP):
            {
                PrxHostRcvrCfgRsp_t configResponse;
                PrxExtractConfigurationResp(&buf[PROXY_MESSAGE_DATA], sizeof(buf), &configResponse);
                LogInfo("Configuration response received for 0x%04x\r\n",
                         configResponse.shortAddr);
                CmReportConfigurationResponse(configResponse);
            }
            break;

            case(PRX_HOST_DOOR_SW_STATE):
            {
                PrxHostImmediateShutoff_t door;
                PrxExtractImmediateShutoff(&buf[PROXY_MESSAGE_DATA], sizeof(buf), &door);
                LogInfo("Received proxy message. Door state is %s\r\n",
                        (door.state == 1) ? "closed" : "open");
            }
            break;
#endif
            
            default:
            {
                LogError("Invalid proxy message received. msgType = %d not supported \r\n", msgType);
            }
            break;
        }
    }
}

/**
 * @brief Puts the system in "virtual charge" mode, where the transmitter enters TPS
 *        with a "virtual client".  Usually, this is done in presence of an external beacon
 *        so power will focus on that beacon.
 */
void ChargeVirtual(void)
{
    ChargeVirtualClient(true);
}

/**
 * @brief Process commands that can only be processed in debug mode
 */
static CotaError_t ProcessDebugCommand(uint8_t* buf, uint16_t size)
{
    uint8_t     *msgBuf = buf;
    CtrlMsg_t   *msg    = (CtrlMsg_t *) msgBuf;
    CtrlResp_t  *resp   = (CtrlResp_t *) gRespBuf;
    uint16_t    len = 0;
    
    resp->id  = msg->id;
    resp->err = COTA_ERROR_NONE;
    len = sizeof(resp->id) + sizeof(resp->err); 

    // These commands are meant for testing only

    switch (buf[0])
    {
        case CTRL_SET_AMB_EN: 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {
                MsgSetAmbOn_t setAmb = msg->setAmbOn;
                AmbMask_t ambMask = AmbGetEnabled();
                if (setAmb.enable > 0)
                {
                    ambMask |= setAmb.ambMask;
                }
                else
                {
                    ambMask &= ~(setAmb.ambMask);
                }
                AmbEnable(ambMask);
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;
        
        case CTRL_GET_AMB_EN:
        {
            resp->getAmbOn.ambMask = AmbGetEnabled();

            len += sizeof(resp->getAmbOn);

            PostPrintf("AMB ON: 0x%02x\r\n", resp->getAmbOn.ambMask);
        }
        break;
        
        case CTRL_SET_UVP_EN: 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {

                // message input values
                MsgSetUvpOn_t  setUvp    = msg->setUvpOn;

                AmbMask_t   currentAmbMask = AmbGetUvpEnabled();
                AmbMask_t   newAmbMask     = 0;

                if (setUvp.enable != 0)
                {
                    newAmbMask = currentAmbMask | setUvp.ambMask;
                }
                else
                {
                    // Clear the currentAmbMask of the pertinent bits. 
                    newAmbMask = currentAmbMask & (~setUvp.ambMask);
                }

                // Update the UVPs that are enabled with the new mask. 
                resp->err = AmbUvpEnable(newAmbMask);
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;

        case CTRL_GET_UVP_EN:
        {
            resp->getUvpOn.ambMask = AmbGetUvpEnabled();
            PostPrintf("UVP ON: 0x%02x\r\n", resp->getUvpOn.ambMask);

            len += sizeof(resp->getUvpOn);
        }
        break;
        
        case CTRL_SELECT_UVP: 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {
                // This function enables a uvp number (0-15); to enable all the uvp's set the value to 16.

                resp->err = AmbSelectUvp(msg->setUvpNum.uvpNum);
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;

        case CTRL_GET_SELECTED_UVP: 
        {
            resp->getSelectedUvp.uvpNum = AmbGetSelectedUvp();
             
            if (resp->getSelectedUvp.uvpNum == ALL_UVP_NUM)
            {
                PostPrintf("SEL_UVP: %s\r\n", ALL_MSG);
            }
            else
            {
                PostPrintf("SEL_UVP: %d\r\n", AmbGetSelectedUvp());
            }
            
            len += sizeof(resp->getSelectedUvp);
        }
        break;

        case CTRL_SET_PU_EN: 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {                    
                AmbMask_t ambMaskToSet = msg->setPuOn.ambMask; // user in mask 0 to 0xF
                uint8_t   onoff        = msg->setPuOn.enable;  // 0 on 1 or more off

                AmbMask_t ambMaskOnNow = AmbGetPuState();
                AmbMask_t tempMask;

                if (onoff > 0 ) // Check for on or off
                {
                    tempMask = (ambMaskOnNow |  ambMaskToSet) & ALL_AMB_MASK;
                }
                else // off path
                {
                    tempMask = (ambMaskOnNow & ~ambMaskToSet) & ALL_AMB_MASK;
                }

                AmbSetPuState(tempMask);
                vTaskDelay( pdMS_TO_TICKS(1));  //Wait a millisecond to to allow process to get done.

                resp->getPu.puOn = AmbGetPuState(); // get and return the enabled mask
                len += sizeof(resp->getPu); 
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            } 
        }
        break;

        case CTRL_GET_PU_EN:
        {
            resp->getPu.puOn = AmbGetPuState();
            PostPrintf("PU_EN: 0x%02x\r\n", resp->getPu.puOn);

            len += sizeof(resp->getPu);
        }
        break;
        
        case CTRL_SET_TX: 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {
                AmbEnableTx((msg->setTx.enable > 0));

                vTaskDelay( pdMS_TO_TICKS(1));  //Wait a millisecond to to allow process to get done.

                resp->getTx.txOn = AmbInTxMode();
                len += sizeof(resp->getTx);

                PostPrintf("%s: %s\r\n", TX_MSG, resp->getTx.txOn ? ON_MSG : OFF_MSG);
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;

        case CTRL_GET_TX: 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {
                resp->getTx.txOn = AmbInTxMode();
                PostPrintf("%s: %s\r\n", TX_MSG, resp->getTx.txOn ? ON_MSG : OFF_MSG);
                
                len += sizeof(resp->getTx);
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;
          
        case CTRL_SET_RX: 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {
                AmbEnableRx((msg->setRx.enable > 0));

                vTaskDelay( pdMS_TO_TICKS(1));  //Wait a millisecond to to allow process to get done.

                resp->getRx.rxOn = AmbInRxMode();
                len += sizeof(resp->getRx);

                PostPrintf("%s: %s\r\n", RX_MSG, resp->getRx.rxOn ? ON_MSG : OFF_MSG);                
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            } 
        }
        break;
        
        case CTRL_GET_RX: 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {
                resp->getRx.rxOn = AmbInRxMode();
                PostPrintf("%s: %s\r\n", RX_MSG, resp->getRx.rxOn ? ON_MSG : OFF_MSG);                
                
                len += sizeof(resp->getRx);
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;

        case CTRL_SET_PU_ALL : 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {                
                uint8_t  onoff = msg->setPuAll.enable;
                AmbEnablePuAll((onoff > 0));  // 0 off 1 or more on. Convert uint8 to bool

                vTaskDelay( pdMS_TO_TICKS(1));  //Wait a millisecond to to allow process to get done.

                resp->getPuAll.puOn = AmbInPuAllMode();

                PostPrintf("%s: %s\r\n", PU_ALL_MSG, resp->getPuAll.puOn ? ON_MSG : OFF_MSG);
                
                len += sizeof(resp->getPuAll);
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;
        
        case CTRL_GET_PU_ALL:
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {
                resp->getPuAll.puOn = AmbInPuAllMode();
                PostPrintf("%s: %s\r\n", PU_ALL_MSG, resp->getPuAll.puOn ? ON_MSG : OFF_MSG);
                
                len += sizeof(resp->getPuAll);
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;
        
        case CTRL_GET_PD_DONE: 
        {
            resp->getPd.ambMask = AmbGetPhaseDetect();
            
            PostPrintf("PD_DONE: 0x%02x\r\n", resp->getPd.ambMask);    
            
            len += sizeof(resp->getPd); 
        }
        break;
        
        case CTRL_SET_SPI_EN : 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {                
                AmbMask_t ambMaskToSet = msg->setSpiEnable.ambMask; // user in mask 0 to 0xF
                uint8_t   onoff        = msg->setSpiEnable.enable;  // 0 off 1 or more on

                AmbMask_t ambMaskOnNow = AmbGetSpiEnabled();
                AmbMask_t tempMask;

                if (onoff > 0 ) // Check for on or off
                {
                    tempMask = (ambMaskOnNow |  ambMaskToSet) & ALL_AMB_MASK;
                }
                else // off path
                {
                    tempMask = (ambMaskOnNow & ~ambMaskToSet) & ALL_AMB_MASK;
                }

                AmbSpiEnable(tempMask); // enable this mask
                vTaskDelay( pdMS_TO_TICKS(1));  //Wait a millisecond to to allow process to get done.

                resp->getSpiEn.ambMask = AmbGetSpiEnabled(); // get and return the enabled mask
                len += sizeof(resp->getSpiEn); 
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }   
        }
        break;

        case CTRL_GET_SPI_EN:
        {
            resp->getSpiEn.ambMask = AmbGetSpiEnabled();
            
            PostPrintf("SPI ON: 0x%02x\r\n", resp->getSpiEn.ambMask);
            
            len += sizeof(resp->getSpiEn); 
        }
        break;
          
        case CTRL_UVP_INIT: 
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {                
                AmbMask_t ambMask = msg->setUvpInit.ambMask;
                UvpMask_t uvpMask = msg->setUvpInit.uvpMask;
                resp->err = UvpInit(ambMask, uvpMask);
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;
        
        case CTRL_SYS_CLK_INIT:
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {
                resp->err = SystemClockInit();
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            } 
        }
        break;

        case CTRL_START_STATIC_TUNE:
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {
                AmbSpiEnable(AmbGetValidAmbs());

                AmbSelectUvp(ALL_UVP_NUM);

                AmbEnableRx(true);

                vTaskDelay( pdMS_TO_TICKS(1));
                
                PostPrintf("Turn on the receiver beacon, app_command <id> 29 <min> <ch> <pwr>, then type 'sample_beacon'\r\n");
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }   
        }
        break; 
      
        case CTRL_SAMPLE_BEACON:
        {
            if (gCtrlSmState->state == CTRL_STATE_DEBUG)
            {                
                AmbEnablePuAll(true);
                AmbEnablePuAll(false);

                vTaskDelay( pdMS_TO_TICKS(1));  //Wait a milli-second to to allow phase detect to be done.

                AmbEnableRx(false);
                
                PostPrintf("Wait for the beacon to turn off then type 'static_charge'\r\n");
            }
            else
            {
                resp->err = COTA_ERROR_NOT_IN_DEBUG_MODE;
            }
        }
        break;
        
        default:
        {
            // Response with the message id of the failed command
            resp->err = COTA_ERROR_NOT_IMPLEMENTED;
        }
        break;
    }
   
    RpiSendMessage(gRespBuf, len);
    
    if (resp->err == COTA_ERROR_NONE)
    {
        PostPrintf(SUCCESS_MSG);
    }
    else
    {
        PostPrintf(FAILURE_MSG);
        POST_COTA_ERROR(resp->err);
    }

    return resp->err;
}


/**
 * @brief This function will process all client queue messages from CLI (AKA inter-task communication).
 *        The types of messages we process will differ depending on the current state
 *        of the control state machine.
 *
 *        This function currently has a bug in it and it could not be resolved before merge.
 *        This is because I am working from home and broke me development board (oops)
 *
 * @param currState The current state of the control task state machine #CtrlStates_t
 */
static void processCtrlCommands(CtrlStates_t currState)
{
    // Much of the modules in the Orion system are built off of
    //   passing uint8_t buffers. This behavior is preferred for
    //   flexibility of the modules, and declaring the data as follows
    //   is useful for clean code within this function call. 

    // For handling our communication between the MCU and external
    // processor (such as a Raspberry Pi) we define
    //   - Messages: Incoming messages that must be answered.
    //   - Response: Outgoing messages that immediately answer incoming messages.
    CtrlMsg_t  *msg  = (CtrlMsg_t *) gIncomingMsgBuf;
    CtrlResp_t *resp = (CtrlResp_t *) gRespBuf;
    uint16_t    size;  // Received message size, which includes the message type field of type CtrlMsgId_t
    uint16_t    respLen;  // The length of the response message


    // There are two places that we can expect a command, from the
    //   raspberry pi, and from the CLI. The raspberry pi follows the
    //   protocol as listed above while the CLI is address by calls to
    //   PostPrintf(). Currently we don't keep track of where the message
    //   came from. So, we will just send these messages to both! That is
    //   to say, that when we get an incoming message of type CtrlMsg_t,
    //   we will address it by sending message of type CtrlResp_t, and
    //   print the same information to the CLI. 
    // 

    // Zero out the buffer so CtrlGetMsg gets clean input.
    memset((void *) gIncomingMsgBuf, 0, sizeof(gIncomingMsgBuf));
    size = CtrlGetMsg(gIncomingMsgBuf, sizeof(gIncomingMsgBuf));
    if (size > 0)
    {
        memset((void *) gRespBuf, 0, sizeof(gRespBuf));
        switch (msg->id)
        {
            case CTRL_MSG_INVALID:
            {
                uint16_t len;
                
                resp->id  = CTRL_MSG_INVALID;
                resp->err = COTA_ERROR_INVALID_CTRL_MSG_RECEIVED;

                len = sizeof(resp->id) + 
                      sizeof(resp->err);
                
                RpiSendMessage(gRespBuf, len);
            }
            break;
            
            case CTRL_REGISTER_RCVR:
            {
                MsgRegRcvr_t reg = msg->registerRcvr;
                CotaError_t    err = COTA_ERROR_NONE;

                resp->id  = CTRL_REGISTER_RCVR;
                resp->err = err;
                
                respLen = sizeof(CTRL_REGISTER_RCVR) + 
                      sizeof(err);
                
                if (IS_ALL_CLIENT(reg.longId))
                {
                    err = CmRegisterAllClients(reg.queryType);
                    RpiSendMessage(gRespBuf, respLen);
                    POST_COTA_ERROR(err);
                    PostPrintf("Finished registering all discovered clients\r\n");
                }  
                else 
                {
                    err = (err == COTA_ERROR_NONE) ?
                        CmRegisterClient(reg.longId) : err;
                    err = (err == COTA_ERROR_NONE) ?
                        CmSetQueryType(reg.longId, reg.queryType) : err;
                    
                    resp->err = err;

                    RpiSendMessage(gRespBuf, respLen);
                    POST_COTA_ERROR(err);
                    if (err == COTA_ERROR_NONE)
                    {
                        ExtdAddr_t longId;  // reg->longId is unaligned, so let's copy it to a aligned structure so I can cast to uint64_t.
                        memcpy(&longId, &reg.longId, sizeof(longId));
                        PostPrintf("Finished registering 0x%llx\r\n", *(uint64_t*)&longId);
                    }
                }
            }
            break;
            
            case CTRL_REMOVE_RCVR:
            {
                MsgRmRcvr_t rm  = msg->removeRcvr;
                CotaError_t   err = COTA_ERROR_NONE;

                if (IS_ALL_CLIENT(rm.longId))
                {
                    err = CmMarkAllClientsAsDisconnect();
                }
                else if ((err = CmMarkClientAsDisconnect(&rm.longId)) == COTA_ERROR_NONE)
                {
                    PostPrintf("Success!\r\n");
                }
                else
                {
                    POST_COTA_ERROR(err);
                }
                
                resp->id  = CTRL_REMOVE_RCVR;
                resp->err = err;

                respLen = sizeof(CTRL_REMOVE_RCVR) +
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);
            }
            break;
                
            case CTRL_RCVR_LIST:
            {
                CotaError_t err = COTA_ERROR_NONE;

                resp->id  = CTRL_RCVR_LIST;
                resp->err = err;
                
                respLen = sizeof(CTRL_RCVR_LIST) + 
                      sizeof(err);

                // The first message indicates to the MM that we have
                //   successfully received the request for a client list
                RpiSendMessage(gRespBuf, respLen);
                vTaskDelay(pdMS_TO_TICKS(3)); // Delay cushions timing for next call

                // This function will send multiple messages to the MM
                // containing client information. It will only print
                // receiver information to the serial console
                CmClientList(msg->rcvrListAll.all);

                // This second message indicates to the MM that we
                // have have finished sending clients
                RpiSendMessage(gRespBuf, respLen);
            }
            break;

            case CTRL_IDENTIFY_TX:
            {
                CotaError_t err = COTA_ERROR_NONE;

                resp->id  = CTRL_IDENTIFY_TX;
                resp->err = err;
                
                respLen = sizeof(CTRL_IDENTIFY_TX) + 
                      sizeof(err);

                LRLightRingUpdate(LR_STATE_IDENT);
                LRStartUpdateTimer();
                
                RpiSendMessage(gRespBuf, respLen);
                POST_COTA_ERROR(err);
                if (err == COTA_ERROR_NONE)
                {
                    PostPrintf("Done\r\n");
                }
            }
            break;

            case CTRL_GET_RCVR_DETAIL:
            {
                CotaError_t   err = COTA_ERROR_NONE;
                MsgRcvrDetail_t detail = msg->rcvrDetail;
                Client_t      receiver;

                CmGetClientInfo(detail.longId, &receiver);
                
                // This is just a raw memory dump of all the receivers
                // data we have on the system
                resp->id   = CTRL_GET_RCVR_DETAIL;
                resp->err  = err;
                resp->rcvrDetail.receiver = receiver;
                
                respLen = sizeof(CTRL_GET_RCVR_DETAIL) + 
                          sizeof(err) +
                          sizeof(receiver);

                RpiSendMessage(gRespBuf, respLen);
                PrintClientDetail(&receiver);
                POST_COTA_ERROR(err);
            }
            break;
            
            case CTRL_PROXY_RESET:
            {
                CotaError_t err = COTA_ERROR_NONE;
                
                PrxRestart();
                                    
                resp->id  = CTRL_PROXY_RESET;
                resp->err = err;
                
                respLen = sizeof(CTRL_PROXY_RESET) + 
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);
                POST_COTA_ERROR(err);
                if (err == COTA_ERROR_NONE)
                {
                    PostPrintf("Proxy Reset Complete\r\n");
                }
            }
            break;
                
            case CTRL_RUN:
            {
                CotaError_t err = COTA_ERROR_NONE;
                
                if ((gCtrlSmState->state != CTRL_STATE_DEBUG) && (!IsTpsFrozen()))
                {
                    PostPrintf("Already running.\r\n");
                }
                SetDebugMode(false);
                LRLightRingUpdate(LR_STATE_IDLE);

                resp->id  = CTRL_RUN;
                resp->err = err;
                
                respLen = sizeof(CTRL_RUN) + 
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);
            
                FreeTps();
            }
            break;
                    
            case CTRL_PAUSE:
            {
                CotaError_t err = COTA_ERROR_NONE;
                
                if (gCtrlSmState->state == CTRL_STATE_DEBUG)
                {
                    PostStringForTransmit("In Debug Mode\r\n");
                }
                SetDebugMode(true);

                resp->id  = CTRL_PAUSE;
                resp->err = err;
                
                respLen = sizeof(CTRL_PAUSE) + 
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);  
            }
            break;

            case CTRL_TX_FREQ:
            {
                CotaError_t err = COTA_ERROR_NONE;
                MsgTxFreq_t txFreq = msg->txFreq;

                CfgSaveParam(CFG_TX_FREQUENCY_MHZ, &txFreq.frequency, sizeof(txFreq.frequency));
                err = UpdateTransmitFrequency();
                POST_COTA_ERROR(err);
                
                if (err == COTA_ERROR_NONE)
                {
                    PostPrintf("Done\r\n");
                }

                resp->id  = CTRL_TX_FREQ;
                resp->err = err;
                
                respLen = sizeof(CTRL_TX_FREQ) + 
                          sizeof(err);

                RpiSendMessage(gRespBuf, respLen);  

                // Give the rpi comm time to complete.
                vTaskDelay(pdMS_TO_TICKS(3));
                SetManualCalibrationFlag(true);  ///Causes calibration to be done by the state machine.
            }
            break;
                
            case CTRL_CALIBRATION:
            {
                CotaError_t err = COTA_ERROR_NONE;
                bool calFlag = false;

                if (gCtrlSmState->state == CTRL_STATE_DEBUG)
                {
                    err = Calibrate(AmbGetValidAmbs(), ALL_UVP_MASK);
                }
                else
                {
                    calFlag = true;
                }
                                    
                resp->id  = CTRL_CALIBRATION; 
                resp->err = err;
                        
                respLen = sizeof(CTRL_CALIBRATION) +
                          sizeof(err);
                    
                RpiSendMessage(gRespBuf, respLen);
                // Give the rpi comm time to complete.
                vTaskDelay(pdMS_TO_TICKS(3));

                SetManualCalibrationFlag(calFlag);  ///Causes calibration to be done by the state machine.

                POST_COTA_ERROR(err);
            }
            break;
                
            case CTRL_SET_POWER_LEVEL:
            {
                MsgSetPowerLevel_t requestedPowerLevel = msg->setPowerLevel;
                uint16_t           actualPowerLevel    = 0;
                CotaError_t        err                 = COTA_ERROR_NONE;
                    
                err = SetAmbPowerLevels( AmbGetValidAmbs(),
                                         requestedPowerLevel.powerLevel,
                                         &actualPowerLevel);
                    
                if (err == COTA_ERROR_NONE)
                {
                    CfgSaveParam(CFG_POWER_LEVEL,
                                 &actualPowerLevel,
                                 sizeof(actualPowerLevel));
                }

                resp->id   = CTRL_SET_POWER_LEVEL;
                resp->err  = err;
                resp->setPowerLevel.powerLevel = actualPowerLevel;

                respLen = sizeof(CTRL_SET_POWER_LEVEL) + 
                          sizeof(err) + 
                          sizeof(actualPowerLevel);

                PostPrintf("PL: %4.1f dBm\r\n", (double)(actualPowerLevel/1000.0));
                RpiSendMessage(gRespBuf, respLen);
                POST_COTA_ERROR(err);
            }
            break;
                
            case CTRL_START_CHARGING_DEVICES:
            {
                CotaError_t err = COTA_ERROR_NONE;
                MsgStartCharging_t startCharging = msg->startCharging;
                        
                if (IS_ALL_CLIENT(startCharging.longId))
                {
                    err = CmStartChargingAllClients();
                }
                else
                {
                    err = CmStartChargingClient(startCharging.longId);
                }
                POST_COTA_ERROR(err);
                PostPrintf("Done\r\n");

                resp->id  = CTRL_START_CHARGING_DEVICES; 
                resp->err = err;

                respLen = sizeof(CTRL_START_CHARGING_DEVICES) +
                      sizeof(err);
                
                RpiSendMessage(gRespBuf, respLen);
                vTaskDelay(pdMS_TO_TICKS(3));
                
                err = CmSendChargingClientData();
                POST_COTA_ERROR(err);

                vTaskDelay(pdMS_TO_TICKS(3));
                RpiSendMessage(gRespBuf, respLen);
            }
            break;
            
            case CTRL_STOP_CHARGING_DEVICES:
            {
                CotaError_t       err          = COTA_ERROR_NONE;
                MsgStopCharging_t stopCharging = msg->stopCharging;
                        
                if (IS_ALL_CLIENT(stopCharging.longId))
                {
                    err = CmStopChargingAllClients();
                }
                else
                {
                    err = CmStopChargingClient(stopCharging.longId);
                }
                POST_COTA_ERROR(err);
                ChargeVirtualClient(false);
                
                PostPrintf("Done\r\n");
                
                resp->id  = CTRL_STOP_CHARGING_DEVICES;
                resp->err = err;

                respLen = sizeof(CTRL_STOP_CHARGING_DEVICES) +
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);
            }
            break;
                
            case CTRL_RESET_NVM:
            {
                CotaError_t err = COTA_ERROR_NONE;
                
                NvmReset();
                CmDeleteAllReceiversFromDatabase();

                resp->id  = CTRL_RESET_NVM;
                resp->err = err;
                
                respLen = sizeof(CTRL_RESET_NVM) + 
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);
            }
            break;

            case CTRL_SET_COMM_CHANNEL:
            {
                CotaError_t         err       = COTA_ERROR_NONE;
                MsgSetCommChannel_t setCommCh = msg->setCommChannel;
                bool calFlag = false;
                
                if (IsValidCommCh(setCommCh.channel))
                {
                    // setting TX only can be done in the debug state invalid longid says just tx.
                    if (IS_LONGID_VALID(setCommCh.longId) && (gCtrlSmState->state == CTRL_STATE_DEBUG))
                    {
                        PostPrintf("Cannot set receiver comm channel in debug\r\n");
                        err = COTA_ERROR_RUN_MODE_REQUIRED;
                    }
                    else
                    {
                        // no error so lets check again for valid long id to set the receiver
                        // comm channel.
                        if (IS_LONGID_VALID(setCommCh.longId))
                        {
                            /*We set the comm channel of the receiver by sending an app command to the receiver.
                              All app commands are sent through an app command queue, which is a queue that gets
                              serviced by the app command state.
                              While in debug mode, this state never executes, so the queue never gets serviced.
                              In order to handle this case, that code will need to be updated.
                            */

                            err = SetCommCh(setCommCh.longId, setCommCh.channel);
                        }
                        //The discovery message sent to the proxy controls
                        //the comm channel for the transmitter. The discovery 
                        //channel always reads the comm channel value from NVM
                        //so we only need to change it in NVM.

                        // if an error in setting the receivers then don't set TX
                        if (err == COTA_ERROR_NONE)
                        {
                            err = CfgSaveParam(CFG_COMM_CHANNEL,
                                               &setCommCh.channel,
                                               sizeof(setCommCh.channel));
                            if (err == COTA_ERROR_NONE)
                            {
                                PostPrintf("Set transmitter comm channel to %d\r\n", setCommCh.channel);
                                UpdateTransmitFrequency();
                                calFlag = true;
                            }
                            else
                            {
                                PostPrintf("Error Setting transmitter comm channel to %d \r\n", setCommCh.channel);
                                POST_COTA_ERROR(err);
                            }
                        }
                    }
                }
                else
                {
                    err = COTA_ERROR_BAD_PARAMETER;
                }
                
                resp->id  = CTRL_SET_COMM_CHANNEL;   
                resp->err = err;

                respLen = sizeof(CTRL_SET_COMM_CHANNEL) +
                       sizeof(err);
                
                RpiSendMessage(gRespBuf, respLen);

                // Give the rpi comm time to complete.
                vTaskDelay(pdMS_TO_TICKS(3));

                SetManualCalibrationFlag(calFlag);  ///Causes calibration to be done by the state machine.

                POST_COTA_ERROR(err);
            }
            break;
                
            case CTRL_SET_RSSI_THRESH:
            {
                CotaError_t err = COTA_ERROR_NONE;

                SetRssiThresh(&msg->rssiThresh);

                resp->id  = CTRL_SET_RSSI_THRESH;
                resp->err = err;
                respLen = sizeof(err) + 
                      sizeof(CTRL_SET_RSSI_THRESH);
                
                RpiSendMessage(gRespBuf, respLen);  
            }
            break;
        
            case CTRL_SET_RSSI_FILTER_EN:
            {
                CotaError_t err = COTA_ERROR_NONE;
                
                SetRssiVoteEn(msg->rssiFilterEn.enable);
                PostPrintf("RSSI filtering %s\r\n", msg->rssiFilterEn.enable ? "enabled" : "disabled");
                resp->id  = CTRL_SET_RSSI_FILTER_EN;
                resp->err = err;
                // return the value it was set to
                resp->getRssiFilterEn.enable = (uint8_t)VoteEnabled();
                respLen = sizeof(err) + 
                          sizeof(CTRL_SET_RSSI_FILTER_EN) +
                          sizeof(resp->getRssiFilterEn.enable);
                
                RpiSendMessage(gRespBuf, respLen);  
            }
            break;
            
            case CTRL_GET_RSSI_FILTER_EN:
            {
                CotaError_t err = COTA_ERROR_NONE;
                
                bool enable = VoteEnabled();
                PostPrintf("RSSI filtering is currently %s\r\n", enable ? "enabled" : "disabled");
                // return the current value of the enable variable
                resp->getRssiFilterEn.enable = (uint8_t)enable;
                resp->id  = CTRL_GET_RSSI_FILTER_EN;
                resp->err = err;
                respLen = sizeof(err) + 
                          sizeof(CTRL_GET_RSSI_FILTER_EN) +
                          sizeof(resp->getRssiFilterEn.enable);
                
                RpiSendMessage(gRespBuf, respLen);  
            }
            break;
            
            case CTRL_RSSI_FILTER_RESET:
            {
                CotaError_t err = COTA_ERROR_NONE;
                
                NvmRssiThreshReset();

                PostPrintf("Done\r\n");
                resp->id  = CTRL_RSSI_FILTER_RESET;
                resp->err = err;
                respLen = sizeof(err) + 
                      sizeof(CTRL_RSSI_FILTER_RESET);

                RpiSendMessage(gRespBuf, respLen);
            }
            break;

            case CTRL_AMB_FILTER_DISABLED:
            {
                resp->err = COTA_ERROR_NONE;
                
                if (VoteEnabled())
                {                      
                    // enabling the print will send data to the RPI in GatherAndVoteTask
                    PrintAmbDisabled();
                }
                else
                {
                    // error path send it back to RPI
                    resp->err = COTA_ERROR_RSSI_FILTER_NOT_ENABLED;
                
                    resp->id = CTRL_AMB_FILTER_DISABLED;
                    respLen = sizeof(CTRL_AMB_FILTER_DISABLED) +
                              sizeof(resp->err);
                    RpiSendMessage(gRespBuf, respLen);
                }
            }
            break;
            
            case CTRL_RSSI_FILTER_REPORT:
            {     
                //This function is only setting a bool,
                //all the responses are control in the gather and vote task. 
                //see #GatherAndVoteTask                  
                resp->id = CTRL_RSSI_FILTER_REPORT;
                if (VoteEnabled())
                {                                
                    resp->err = COTA_ERROR_NONE;   
                    //This must be done before we call PrintRssiReport, 
                    //because this function will cause RPI data to be issued.
                    respLen = sizeof(CTRL_RSSI_FILTER_REPORT) +
                              sizeof(resp->err);
                    RpiSendMessage(gRespBuf, respLen);   
                    PrintRssiReport();
                    // the data and end messages are sent in  GatherAndVoteTask                  
                }
                else
                {
                    resp->err = COTA_ERROR_RSSI_FILTER_NOT_ENABLED;
                    respLen = sizeof(CTRL_RSSI_FILTER_REPORT) +
                              sizeof(resp->err);
                    RpiSendMessage(gRespBuf, respLen);
                    POST_COTA_ERROR(resp->err);
                }
            }
            break;
            
            case CTRL_PROXY_COMMAND:
            {
                CotaError_t       err     = COTA_ERROR_NONE;
                MsgProxyCommand_t prxCmd = msg->proxyCommand;

                err = PrxSendCommand(&prxCmd.command);

                POST_COTA_ERROR(err);
                if (err == COTA_ERROR_NONE)
                {
                    PostPrintf("Done\r\n");
                }
                resp->id  = CTRL_PROXY_COMMAND;
                resp->err = err;
                respLen = sizeof(err) + 
                      sizeof(CTRL_PROXY_COMMAND);

                RpiSendMessage(gRespBuf, respLen);  
            }
            break;

            case CTRL_PROXY_COMMAND_DATA:
            {
                CotaError_t err = COTA_ERROR_NONE;
                
                resp->id  = CTRL_PROXY_COMMAND_DATA;
                resp->err = err;
                resp->proxyCommandData.data = gProxyCmdData;

                respLen = sizeof(err) + 
                          sizeof(CTRL_PROXY_COMMAND_DATA) +
                          sizeof(gProxyCmdData.payloadSize) +
                          gProxyCmdData.payloadSize;

                RpiSendMessage(gRespBuf, respLen);
                PostPrintf("Proxy Command Data\r\n");
                if (gProxyCmdData.payloadSize > 0)
                {
                    PostPrintf("Message Type: 0x%02X (%d)\r\n", gProxyCmdData.msgType, gProxyCmdData.msgType);
                    PostPrintf("Hex: ");
                    for (int32_t i = 0; i < gProxyCmdData.payloadSize - 1; i++)
                    {
                        PostPrintf("0x%02X ",gProxyCmdData.payload.bytes[i]);
                    }
                    PostPrintf("\r\n" "Dec: ");
                    for (int32_t i = 0; i < gProxyCmdData.payloadSize - 1; i++)
                    {
                        PostPrintf("%4u ",gProxyCmdData.payload.bytes[i]);
                    }
                    PostPrintf("\r\n");
                }
                else
                {
                    PostPrintf("No data has been returned from the proxy.\r\n");
                }
            }
            break;
            
            case CTRL_PROXY_INFO:
            {
                CotaError_t err = COTA_ERROR_NONE;
                uint16_t*   wordBuff;
                PrxHostProxyFwInfo_t proxyInfo;

                PrxGetProxyInfo(&proxyInfo);

                resp->id  = CTRL_PROXY_INFO;
                resp->err = err;
                resp->proxyInfo.info = proxyInfo;
#if defined(USE_FOREVER_TRACKER_BUILD)
                resp->proxyInfo.info.txNotAllowed = !IsDoorClosed();
#else
                resp->proxyInfo.info.txNotAllowed = 0; //Transmission always allowed if door is not used.
#endif
                respLen = sizeof(err) + 
                          sizeof(CTRL_PROXY_INFO) +
                          sizeof(proxyInfo);
                RpiSendMessage(gRespBuf, respLen);
                wordBuff = (uint16_t*) &proxyInfo.revision;
                PostPrintf("FW Version: %d.%d \r\n", wordBuff[1], wordBuff[0]);
#if defined(USE_FOREVER_TRACKER_BUILD)
                PostPrintf("Door is %s\r\n", IsDoorClosed() ? "closed" : "open");
#else
                PostPrintf("Door state not supported\r\n");
#endif
                PostPrintf("CRC error is %d\r\n", proxyInfo.crcError);
            }
            break;
            
            case CTRL_APP_COMMAND:
            {
                CotaError_t     err    = COTA_ERROR_NONE;
                MsgAppCommand_t appCmd = msg->appCommand; 

                if (gCtrlSmState->state == CTRL_STATE_DEBUG)
                {
                    if (IS_ALL_CLIENT(appCmd.command.extAddr))
                    {
                        // app_commands cannot be used with all arguments in debug mode
                        err = COTA_ERROR_BAD_PARAMETER;
                        PostPrintf("all argument not supported in debug\r\n");
                    }
                    else
                    {
                        err = PrxSendAppCommand(&appCmd.command);
                    }
                }
                else if (!IsAppCmdActive())
                {
                    err = SendAppCmd(appCmd.command.extAddr, appCmd.command);
                }
                else
                {
                    err = COTA_ERROR_APP_CMD_BUSY;
                }
                resp->id  = CTRL_APP_COMMAND;
                resp->err = err;
                respLen = sizeof(err) + 
                      sizeof(CTRL_APP_COMMAND);

                RpiSendMessage(gRespBuf, respLen);
                POST_COTA_ERROR(err);
            }
            break;
      
            case CTRL_APP_COMMAND_DATA:
            {
                CotaError_t         err     = COTA_ERROR_NONE;
                MsgAppCommandData_t cmdData = msg->appCommandData;
                CotaAppData_t       appData;

                CmGetClientAppData(cmdData.longId, &appData);

                resp->id  = CTRL_APP_COMMAND_DATA;
                resp->err = err;
                resp->appCommandData.data = appData;

                respLen = sizeof(err) + 
                          sizeof(CTRL_APP_COMMAND_DATA) +
                          sizeof(appData);



                RpiSendMessage(gRespBuf, respLen);
                PrintAppCommandData(cmdData.longId, appData);
            }
            break;
          
            case CTRL_UVP_READ_DATA:
            {
                CotaError_t  err = COTA_ERROR_NONE;
                MsgUvpRead_t read = msg->uvpRead;

                resp->id  = CTRL_UVP_READ_DATA;
                resp->err = err;

                respLen = sizeof (CTRL_UVP_READ_DATA) +
                       sizeof (err);

                RpiSendMessage(gRespBuf, respLen); //Start messages to send
                vTaskDelay(pdMS_TO_TICKS(3));

                // This function will send a flurry of message to the
                //   RPi, and print to the CLI via UART
                err = ProcessUvpReadCommand(&read.uvpData);
                resp->err = err;

                vTaskDelay(pdMS_TO_TICKS(3));

                RpiSendMessage(gRespBuf, respLen); //End messages to send
                POST_COTA_ERROR(err);
            }
            break;

            case CTRL_UVP_WRITE_DATA:
            {
                CotaError_t   err = COTA_ERROR_NONE;
                MsgUvpWrite_t write = msg->uvpWrite;

                err = ProcessUvpWriteCommand(&write.uvpData);

                resp->id  = CTRL_UVP_WRITE_DATA;
                resp->err = err;

                respLen = sizeof(CTRL_UVP_WRITE_DATA) +
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);
                POST_COTA_ERROR(err);
            }
            break;
            
            case CTRL_LOCK_DETECT:
            {
                CotaError_t     err = COTA_ERROR_NONE;
                MsgLockDetect_t lockDetect = msg->lockDetect;

                resp->id  = CTRL_LOCK_DETECT;
                resp->err = err;
                respLen = sizeof (err) + 
                      sizeof (CTRL_LOCK_DETECT);

                RpiSendMessage(gRespBuf, respLen); //Start messages to send
                vTaskDelay(pdMS_TO_TICKS(3));

                err = ProcessUvpLockCommand(&lockDetect.uvpData);
                resp->err = err;

                vTaskDelay(pdMS_TO_TICKS(3));
                RpiSendMessage(gRespBuf, respLen); //End messages to send
                POST_COTA_ERROR(err);
            }
            break;  
        
            case CTRL_SET_FANS_FULL:
            {
                CotaError_t err = COTA_ERROR_NONE;
                MsgFansFull_t msgFull = msg->fansFull;
#if defined(USE_FOREVER_TRACKER_BUILD)
                PostPrintf("Forever tacker requires fans to be full\r\n");
                msgFull.fullFans = true;
#endif
                SetFansFull(msgFull.fullFans);
                CheckFans(true);
                PostPrintf("%s\r\n", msgFull.fullFans ? "Fans full enabled\r\n" : "Fans on auto\r\n");

                resp->id  = CTRL_SET_FANS_FULL;
                resp->err = err;
                respLen = sizeof(err) + 
                          sizeof(CTRL_SET_FANS_FULL);

                RpiSendMessage(gRespBuf, respLen);
            }
            break;

            case CTRL_GET_FANS_FULL:
            {
                CotaError_t err = COTA_ERROR_NONE;

                GetFansFull(&(resp->fansFull.fullFans));
                PostPrintf("%s\r\n", resp->fansFull.fullFans ? "Fans full enabled\r\n" : "Fans on auto\r\n");

                resp->id  = CTRL_GET_FANS_FULL;
                resp->err = err;
                respLen = sizeof(err) + 
                          sizeof(CTRL_GET_FANS_FULL) +
                          sizeof(resp->fansFull.fullFans);

                RpiSendMessage(gRespBuf, respLen);
            }
            break;
        
            case CTRL_SET_RCVR_CONFIG:
            {
                CotaError_t          err    = COTA_ERROR_NONE;
                MsgSetRcvrConfig_t config = msg->setRcvrConfig;

                err = CmSetQueryType(config.longId, config.queryType);

                resp->id  = CTRL_SET_RCVR_CONFIG;
                resp->err = err;
                respLen = sizeof(err) + 
                          sizeof(CTRL_SET_RCVR_CONFIG);

                RpiSendMessage(gRespBuf, respLen);
                POST_COTA_ERROR(err);
                if (err == COTA_ERROR_NONE)
                {
                    PostPrintf("Success\r\n");
                }
            }
            break;

            case CTRL_CHARGE_VIRTUAL:
            {
                CotaError_t err = COTA_ERROR_NONE;

                ChargeVirtual();

                resp->id  = CTRL_CHARGE_VIRTUAL;
                resp->err = err;
                
                respLen = sizeof(CTRL_CHARGE_VIRTUAL) + 
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);
                
                PostPrintf("Virtual charging started\r\n");                               
            }
            break;
        
            case CTRL_SET_CFG_PARAM:
            {
                CotaError_t err = COTA_ERROR_NONE;
                MsgSetConfigParam_t set = msg->setConfigParam;
                
                // Attention PRers, doesn't this corrupt other data?
                //   val is uint64_t.
                err = CfgSaveParam(set.paramId, &set.val, sizeof(set.val));
                if (err == COTA_ERROR_NONE)
                {
                    PostPrintf("Success\r\n");
                }
                else
                {                  
                    POST_COTA_ERROR( CfgSaveParam(set.paramId, &set.val, sizeof(set.val)));
                }

                resp->id  = CTRL_SET_CFG_PARAM;
                resp->err = err;
                
                respLen = sizeof(CTRL_SET_CFG_PARAM) + 
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);  
            }
            break;

            case CTRL_GET_CFG_PARAM:
            {
                CotaError_t err = COTA_ERROR_NONE;
                uint64_t val = 0;
                MsgGetConfigParam_t get = msg->getConfigParam; 
                
                err = CfgGetParam(get.paramId, &val, sizeof(val));

                resp->id                 = CTRL_GET_CFG_PARAM;
                resp->err                = err;
                resp->getConfigParam.val = val;
                
                respLen = sizeof(CTRL_GET_CFG_PARAM) +
                      sizeof(err) +
                      sizeof(val);

                RpiSendMessage(gRespBuf, respLen);  
                PostPrintf("Cfg value for id = %d: %lld (0x%llx)\r\n", get.paramId,val,val);
            }
            break;
            
            case CTRL_RESET_ARRAY:
            {
                CotaError_t err = COTA_ERROR_NONE;
                AmbMask_t   validAmbs;

                validAmbs = AmbGetValidAmbs();
                
                err = AmbUvpEnable(0); // Disable all UVPs
                AmbEnable(0); // Disables all AMBs

                // Give the power circuitry some time to approach steady state. 
                vTaskDelay(pdMS_TO_TICKS(10));
                // Enable the ambs that the system is configured to.
                AmbEnable(validAmbs);
                vTaskDelay(pdMS_TO_TICKS(10));

                err = (err == COTA_ERROR_NONE) ?
                    AmbUvpEnable(validAmbs) : err;
                vTaskDelay(pdMS_TO_TICKS(10));

                // Reinitialize all the UVPs
                err = (err == COTA_ERROR_NONE) ?
                    UvpInit(validAmbs, ALL_UVP_MASK) : err;

                resp->id  = CTRL_RESET_ARRAY;
                resp->err = err;

                respLen = sizeof(CTRL_RESET_ARRAY) +
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);  

                // Give the rpi comm time to complete.
                vTaskDelay(pdMS_TO_TICKS(3));

                SetManualCalibrationFlag(true); // Now we need to calibrate again
                
                PostPrintf("AMBs Reset\r\n");
            }
            break;
            
            case CTRL_GET_SYSTEM_STATE:
            {
                CotaError_t err = COTA_ERROR_NONE;
                LightRingState_t state = LRGetCurrLightRingState();

                // The Orion's concept of 'system state' is not
                //   well defined. As a piggy-back from Venus, we
                //   will associate the state of the light ring
                //   (LED on the transmitter that indicates basic information
                //   to the user) as 'state'. The light ring state
                //   is updated periodically as the system
                //   operates. See #LightRingUpdateEntry() for
                //   more information.
                resp->id  = CTRL_GET_SYSTEM_STATE;
                resp->err = err;
                resp->getSystemState.state = state;

                respLen = sizeof(CTRL_GET_SYSTEM_STATE) + 
                      sizeof(err) +
                      sizeof(RespGetSystemState_t);

                PostPrintf("System State: %s\r\n", GetSystemStateString(state));

                RpiSendMessage(gRespBuf, respLen);
                POST_COTA_ERROR(err);
            }
            break;

            case CTRL_GET_SYSTEM_TEMP:
            {
                CotaError_t err = COTA_ERROR_NONE;
                uint8_t ii = 0;
                uint8_t jj = 0;
                int16_t temp[MAX_NUM_AMB] = {0}; 
                
                // CCB temperatures
                for (ii = 0; (ii < TEMP_CCB_NUM_OF_THERM) && (err == COTA_ERROR_NONE); ii++)
                {
                    err =  ReadCCBTemperature(ii, &temp[0]); // reuse the temp variable. Just the first location
                    resp->getSystemTemp.temp[jj++] = temp[0];
                    if (err == COTA_ERROR_NONE)
                    {
                        PostPrintf("CCB Thermometer %d is %.2f degrees C\r\n", ii, CENTICELSIUS_TO_CELSIUS(resp->getSystemTemp.temp[ii]));
                    }
                }
                // UVP temperatures
                if (err == COTA_ERROR_NONE)
                {
                    temp[0] = 0; // reset the temp variable
                    err = GetAverageUvpTempsFromCache(temp, AmbGetValidAmbs());
                
                    if  (err == COTA_ERROR_NONE)
                    {
                        for (ii=0; ii < MAX_NUM_AMB; ii++)
                        {
                            resp->getSystemTemp.temp[jj++] = temp[ii];
                            if (BIT_IN_MASK(ii, AmbGetValidAmbs()))
                            {
//jm                                PostPrintf("AMB %d T = %.2f degrees C\r\n", ii, CENTICELSIUS_TO_CELSIUS(resp->getSystemTemp.temp[ii]));
//jm                                PostPrintf("AMB %d T = %.2f degrees C\r\n", ii, CENTICELSIUS_TO_CELSIUS(resp->getSystemTemp.temp[5]));
                                PostPrintf("AMB %d T = %.2f degrees C\r\n", ii, CENTICELSIUS_TO_CELSIUS(resp->getSystemTemp.temp[TEMP_CCB_NUM_OF_THERM + ii]));                              
               
                            }                  
                        }
                    }
                }

              
                resp->id  = CTRL_GET_SYSTEM_TEMP;
                resp->err = err;
                
                respLen = sizeof(CTRL_GET_SYSTEM_TEMP) + 
                          sizeof(err) +
                          sizeof(resp->getSystemTemp.temp); // the data length from the structure 
                
                RpiSendMessage(gRespBuf, respLen);  
                    
                POST_COTA_ERROR(err);
            }
            break;
        
            case CTRL_STATIC_CHARGE:
            {                 
#if defined(USE_DEMO_BUILD)
                resp->err = COTA_ERROR_NONE;
                
                if (gCtrlSmState->state == CTRL_STATE_DEBUG)
                {                        
                    AmbEnableTx(true);
                    vTaskDelay( pdMS_TO_TICKS(1));

                    AmbEnablePuAll(true);
                    AmbEnablePuAll(false); 
                    
                    PostPrintf("Turn off charging by typing 'set_tx_on off'\r\n");   
                }
                else
                {
                    if ( VoteEnabled() )
                    {
                        FreezeTpsTransmit(&msg->staticCharge);
                    }
                    else
                    {
                        resp->err = COTA_ERROR_RSSI_FILTER_NOT_ENABLED;
                    }
                }
#elif defined(USE_FOREVER_TRACKER_BUILD)
                resp->err = COTA_ERROR_NOT_IMPLEMENTED;
#else
    #error Undefined build target!
#endif
                POST_COTA_ERROR(resp->err);
                resp->id = CTRL_STATIC_CHARGE;
                respLen = sizeof(CTRL_STATIC_CHARGE) +
                          sizeof(resp->err);
                RpiSendMessage(gRespBuf, respLen);  
            }
            break;  
            
            case CTRL_UVP_MEM_DUMP:
            {
                MsgUvpMemDumpCallback_t info = msg->uvpMemDump;  
                
                resp->id    = CTRL_UVP_MEM_DUMP;
                resp->err   = COTA_ERROR_NONE;
                respLen     = sizeof(CTRL_UVP_MEM_DUMP) + sizeof(resp->err);

                RpiSendMessage(gRespBuf, respLen);   //Send start message to RPI

                // add delay to next RpiSendMessage in UvpDump
                vTaskDelay(pdMS_TO_TICKS(3));

                UvpDump(info.ambNum, info.uvpNum, resp);

                // We do it twice because UvpDump alters the response data. 
                resp->id    = CTRL_UVP_MEM_DUMP;
                resp->err   = COTA_ERROR_NONE;

                RpiSendMessage(gRespBuf, respLen);   //Send end message to RPI
            }
            break;
            
            case CTRL_SYS_CLK_DUMP:
            {
                resp->id    = CTRL_SYS_CLK_DUMP;
                resp->err   = COTA_ERROR_NONE;      
                respLen     = sizeof(CTRL_SYS_CLK_DUMP) + sizeof(resp->err);
                
                RpiSendMessage(gRespBuf, respLen);   //Send start message to RPI
                SysClkDump(resp);
                
                resp->id    = CTRL_SYS_CLK_DUMP;
                resp->err   = COTA_ERROR_NONE;              
                respLen     = sizeof(CTRL_SYS_CLK_DUMP) + 
                                sizeof(resp->err);
                RpiSendMessage(gRespBuf, respLen);   //Send end message to RPI
            }
            break;
 
            case CTRL_GET_FIRMWARE_VERSION:
            {
                CotaError_t err = COTA_ERROR_NONE;
                PrxHostProxyFwInfo_t proxyInfo;
            
                // Obtains multiple pieces of information, including
                //   revision number.
                PrxGetProxyInfo(&proxyInfo);             
                resp->id  = CTRL_GET_FIRMWARE_VERSION;
                resp->err = err;
                
                resp->version.commit        = SOURCE_CONTROL_VERSION;
                resp->version.major         = RELEASE_VERSION_MAJOR;
                resp->version.minor         = RELEASE_VERSION_MINOR;
                resp->version.proxyRevision = proxyInfo.revision;
                resp->version.bldType       = GetBuildType();
            
                // Time stamp is a human readable string
                memcpy((void*)resp->version.timestamp,(void*) TIMESTAMP, sizeof(TIMESTAMP));
                
                respLen = sizeof(CTRL_GET_FIRMWARE_VERSION) +
                      sizeof(RespVersion_t) +
                      sizeof(err);
            
                RpiSendMessage(gRespBuf, respLen);
                PrintVersionsCommand(proxyInfo);
                
            }
            break;
                
            case CTRL_RPI_HOST_DATA:
            {
                CotaError_t err          = COTA_ERROR_NONE;
                uint16_t payloadSize     = size - sizeof(CtrlMsgId_t);
                uint16_t respDataSize    = sizeof(resp->rpiHostData);  // Initial value is max response message buffer size
                
                // Pass the data to the proxy FW update interface module for parsing
                err = ProxyFwUpdateMsgDispatch(msg->rpiHostData.data, payloadSize, resp->rpiHostData.data, &respDataSize);
                
                // Send the response back to RPi
                resp->id = CTRL_RPI_HOST_DATA;
                resp->err = err;
                respLen = sizeof(CTRL_RPI_HOST_DATA) + 
                      sizeof(resp->err) +
                      respDataSize;
                RpiSendMessage(gRespBuf, respLen);
            }
            break;
                
            case CTRL_REBOOT:
            {
                RebootMCU();
            }
            break;
            
            case CTRL_SHUTDOWN_CCB:
            {
                PostPrintf("Shutdown Message Received\r\n");
                CompleteShutdownChecklist();

            // Disable the current task from
            // continuing. We must be power cycled
            // to return to normal operation. 
                // resetting automatically.
                while (1)
                {
                    HAL_IWDG_Refresh(&IWDG1_HANDLE);
                }
            }
            break;
            
            case CTRL_SYS_CLK_WRITE:
            {
                resp->id = CTRL_SYS_CLK_WRITE;
                resp->err = SafeSysClkWriteReg(msg->sysClkWriteArgs.addr, msg->sysClkWriteArgs.val);
                respLen = sizeof(CTRL_SYS_CLK_WRITE) + 
                          sizeof(resp->err);
                RpiSendMessage(gRespBuf, respLen);
                POST_COTA_ERROR(resp->err);
                if (resp->err == COTA_ERROR_NONE)
                {
                    PostPrintf("Done\r\n");
                }
            }
            break;
            
            case CTRL_SYS_CLK_READ:
            {
                resp->id = CTRL_SYS_CLK_READ;  
                resp->err = SysClkReadReg(msg->sysClkReadArg.addr, &resp->sysClkRead.val);
                PostPrintf("Value = 0x%0x\r\n", resp->sysClkRead.val);
                respLen = sizeof(CTRL_SYS_CLK_READ) + 
                          sizeof(resp->err) +
                          sizeof(resp->sysClkRead);
                RpiSendMessage(gRespBuf, respLen);
            }
            break;
            
            case CTRL_SYS_CLK_I2C_DISABLE:
            {
                resp->id = CTRL_SYS_CLK_I2C_DISABLE;
                resp->err = COTA_ERROR_NONE;
                SysClkI2CDisable();
                PostPrintf("Done\r\n");
                respLen = sizeof(CTRL_SYS_CLK_I2C_DISABLE) + 
                          sizeof(resp->err);
                RpiSendMessage(gRespBuf, respLen);
            }
            break;
         
            case CTRL_BOOTLOADER:
            {
                PostPrintf("Bootloader command received.\r\n");
                
                resp->id = CTRL_BOOTLOADER;
                resp->err = COTA_ERROR_NONE;
                respLen = sizeof(CTRL_BOOTLOADER) + 
                          sizeof(resp->err);
                RpiSendMessage(gRespBuf, respLen);
                
                // Allow a few milliseconds for the response to RPi
                // to go through, set the bootloader flag, then reboot
                vTaskDelay(pdMS_TO_TICKS(100));
                SCB_DisableDCache();
                gBootSelectionFlag = BOOT_SELECTION_BOOTLOADER;
                RebootMCU();
            }
            break;
            
            case CTRL_SYS_DOOR_OPEN:
            {
                resp->id = CTRL_SYS_DOOR_OPEN;
#if defined(USE_FOREVER_TRACKER_BUILD)
                resp->err = COTA_ERROR_NONE;
                PostPrintf("Door opened\r\n");
                //The door mechanism has already disabled the AMB's, but we need to align our pins to that.
                AmbEnable(0);  
                AmbUvpEnable(AMB_DISABLE_ALL);
                gDoorClosed = false;
                CmDeleteAllReceiversFromDatabase();
                ResetShortIds();
                StopEnumerationPeriod();
                StopWaitForSleepPeriod();
                ClearTpsSchedule();
                SetFastQuery(true);
#else
                resp->err = COTA_ERROR_DOOR_NOT_SUPPORTED;
#endif
                RpiSendMessage((uint8_t*) resp, sizeof(CTRL_SYS_DOOR_OPEN) + 
                  sizeof(resp->err));
            }
            break;

            case CTRL_SYS_DOOR_CLOSED:
            {
#if defined(USE_FOREVER_TRACKER_BUILD)
                PostPrintf("Door closed\r\n"); 
                //We must re-initialize the AMB's since they were disabled 
                gDoorClosed = true;
                resp->err = StartUvps(AmbGetValidAmbs());
                SetManualCalibrationFlag(true);
                StartLongEnumerationPeriod();
                StartWaitForSleepPeriod();
                SetFastQuery(true);
#else
                resp->err = COTA_ERROR_DOOR_NOT_SUPPORTED;
#endif
                resp->id = CTRL_SYS_DOOR_CLOSED;
                RpiSendMessage((uint8_t*) resp, sizeof(CTRL_SYS_DOOR_CLOSED) + 
                  sizeof(resp->err));
            }
            break;

#if defined(USE_DEMO_DISC)
            case CTRL_SEND_DISCOVERY_MESSAGE:
            {
                CotaError_t   err      = COTA_ERROR_NONE;
                MsgSendDisc_t sendDisc = msg->sendDisc; 

                ScheduleDiscoveryOnChannel(sendDisc.channel);
                
                resp->id  = CTRL_SEND_DISCOVERY_MESSAGE;
                resp->err = err;
                
                respLen = sizeof(CTRL_SEND_DISCOVERY_MESSAGE) + 
                      sizeof(err);

                RpiSendMessage(gRespBuf, respLen);  
            }
            break;
#endif //defined(USE_DEMO_DISC)

            default:
            {
                ProcessDebugCommand(gIncomingMsgBuf, size);
            }
            break;
        }
    }
}

#if defined(USE_FOREVER_TRACKER_BUILD)
bool GetDoorState(void)
{
    return gDoorClosed; 
}
#endif 
