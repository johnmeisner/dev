/**
 * @file       client_manager.h
 *
 * @brief      Header file for the client manager.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CLIENT_MANAGER_H_
#define _CLIENT_MANAGER_H_


#include "error.h"
#include "CotaCommonTypes.h"
#include "client.h"
#include "client_avl_tree.h"

// Several assumptions are made in the code based on
//   the fact that all of these states have an
//   equal number of clients. This reduced complexity
//   and programming time.
#define NUM_DISC_CLIENTS   MAX_NUM_CLIENTS       
#define NUM_JOIN_CLIENTS   MAX_NUM_CLIENTS      
#define NUM_TPC_CLIENTS    MAX_NUM_CLIENTS      
#define NUM_RDY_CLIENTS    MAX_NUM_CLIENTS

CotaError_t       InitClientManager(void);
ShrtAddr_t        GetNewShortId(void);
void              InitializeClientStruct(Client_t *client);



#endif // #ifndef _CLIENT_MANAGER_H_
