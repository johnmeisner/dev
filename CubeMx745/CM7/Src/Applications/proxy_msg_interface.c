/**
 * @file       proxy_msg_interface.c
 *
 * @brief      This module defines all messages types that can be sent to the proxy.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <string.h>
#include "proxyHostMsgInf.h"
#include "CotaCommonTypes.h"
#include "proxyClientMsgInf.h"
#include "error.h"
#include "orion_config.h"
#include "proxy.h"
#include "proxy_msg_interface.h"
#include "ctrl_task.h"
#include "cli_task.h"
#include "tps_driver.h"
#include "cqt_scheduler.h"
#include "tps_scheduler.h"
#include "client_interface.h"

/////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////
#define MSG_SIZE_DISCONNECT     (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostDisconnectClientMsg_t))
#define MSG_SIZE_QUERY          (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostCqTablePartialMsg_t))
#define MSG_SIZE_TPC            (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostTonePowerConfig_t))
#define MSG_SIZE_JOIN           (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostJoinClientMsg_t))
#define MSG_SIZE_DISC           (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostStartDiscMsg_t))
#define MSG_SIZE_PRX_CMD        (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostProxyCmdMsg_t))
#define MSG_SIZE_CLIENT_CMD     (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostClientCmdMsg_t))
#define MSG_SIZE_LIGHT_RING_MSG (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostLightRingMsg_t))
#define MSG_SIZE_RCVR_CONFIG    (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostRcvrConfig_t))
#define MSG_SIZE_PROXY_SETUP    (sizeof(ProxyHostMsgType_t) + sizeof(PrxHostProxySetup_t))
#define MSG_SIZE_GO_MSG         (sizeof(ProxyHostMsgType_t) + sizeof(CotaRcvrGo_t))

#define ONE_TIME_DISCOVERY_MESSAGE_PERIOD  0  ///< A special value used by the Discovery message logic to only issue a single Discovery message

/////////////////////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////
// Private Variable Declarations
/////////////////////////////////////////////////////////////////////////////////////////////
static PrxHostProxyFwInfo_t gProxyFwInfo;           ///< Proxy Initialization information including version
static PrxHostProxyStatus_t gProxyStatus;           ///< Proxy initialization information for forever tracker

static bool gProxyMsgReceived = false;              ///< Set to true when a message is received
static bool gSilenceCrcCheckPrint = false;          ///< If true, the next PostPrintf of the CRC check will not be displayed.  Used when we test the proxy.
static uint8_t gProxyCmdMsgBuf[MSG_SIZE_PRX_CMD];   ///< Keep proxy command buffer global to save stack memory
static uint8_t gAppCmdMsgBuf[MSG_SIZE_CLIENT_CMD];  ///< Keep app command buffer global to save stack memory

/*******************************************************************************************
 * Private Function Declarations
 *******************************************************************************************/

/**
 * @brief The proxy is ready once it has receive a response to a message.
 * @return true if the proxy is ready.
 */
bool PrxIsReady(void)
{
    return gProxyMsgReceived;
}

/**
 * @brief Resets the proxy chip and reinitializes module variables
 */
void PrxRestart(void)
{
    ProxyReset();
    gProxyFwInfo.revision = 0;
    gProxyFwInfo.status = 0;
    gProxyFwInfo.crcError = 1;
    gProxyFwInfo.txNotAllowed = 1;

    gProxyStatus.fwVersion = 0;
    gProxyStatus.status = 0;
    gProxyStatus.crcError = 1;
    gProxyStatus.initNeeded = 1;
    gProxyMsgReceived = false;
}

/**
 *  @brief Invalidates the proxy received flag and silences the next CRC check message.  Done while testing the proxy.
 */
void InvalidateProxyReceivedFlags(void)
{
    gProxyMsgReceived = false;
    gSilenceCrcCheckPrint = true;
}

/**
 * @brief Determines if a Proxy message is ready to be processed by the control task
 * @return true if a message is ready to be processed, false otherwise.
 */
bool PrxMsgRdy(void)
{
    return !ProxyIsRxQueueEmpty();
}

/**
 * @brief Gets the message from the proxy driver and returns the type the message is.
 *
 * @param buf  An output buffer for the proxy data to be copied to.
 * @param size The maximum size of the output buffer
 *
 * @return If successful, The type of message obtained from the proxy driver, else PRX_HOST_ERROR
 */
ProxyHostMsgType_t PrxGetMsg(uint8_t *buf, uint8_t size)
{
    ProxyHostMsgType_t  msg_type = PRX_HOST_ERROR;

    if (buf != NULL)
    {
        if (ProxyGetMessage(buf, size) == COTA_ERROR_NONE)
        {
            gProxyMsgReceived = true;
            // The 1st element is the message type.
            msg_type = (ProxyHostMsgType_t) buf[1];
        }
    }

    return msg_type;
}

/**
 * @brief Extracts a firmware version and status caches it into a global to be retrieved later.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 */
void PrxUpdateProxyInfo(uint8_t *buf, uint8_t size)
{
    if (buf != NULL)
    {
        size = MIN(size, sizeof(PrxHostProxyFwInfo_t));
        memcpy((void*)&gProxyFwInfo, (void*)buf, size); //type: PrxHostProxyFwInfo_t
        //We want the user to know the CRC check
        //passed or failed on reboot, but we don't
        //need to print this message during the period Proxy test,
        //so we use a flag to skip it in the latter case.
        if (!gSilenceCrcCheckPrint)
        {
            PostPrintf("Proxy CRC check: %s\r\n", gProxyFwInfo.crcError ? "FAILED" : "SUCCESS");
        }
        else
        {
            gSilenceCrcCheckPrint = false;
        }
    }
}

/**
 * @brief Extracts a firmware version and status caches it into a global to be reqrieved later.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 */
void PrxUpdateProxyStatus(uint8_t *buf, uint8_t size)
{
    if (buf != NULL)
    {
        size = MIN(size, sizeof(PrxHostProxyStatus_t));
        memcpy((void*)&gProxyStatus, (void*)buf, size); //type:PrxHostProxyStatus_t
        //We want the user to know the CRC check
        //passed or failed on reboot, but we don't
        //need to print this message during the period Proxy test,
        //so we use a flag to skip it in the latter case.
        if (!gSilenceCrcCheckPrint)
        {
            PostPrintf("Proxy CRC check: %s\r\n", gProxyStatus.crcError ? "FAILED" : "SUCCESS");
        }
        else
        {
            gSilenceCrcCheckPrint = false;
        }
    }
}

/**
 * @brief Extracts a discovery type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param discResult An output buffer that will get populated with discovered client information
 */
void PrxExtractDiscoveryResp(uint8_t *buf, uint8_t size, PrxHostDiscoveryMsg_t *discResult)
{
    if ((buf != NULL) && (discResult != NULL))
    {
        size = MIN(size, sizeof(PrxHostDiscoveryMsg_t));
        memcpy((void*)discResult, (void*)buf, size);
    }
}

/**
 * @brief memcpys proxy fw info to a local struct.
 * @param proxyInfo pointer to proxy info struct that will be memcpy'd to
 */
void PrxGetProxyInfo(PrxHostProxyFwInfo_t *proxyInfo)
{
    if (proxyInfo != NULL)
    {
        memcpy((void *)proxyInfo, (void*)&gProxyFwInfo, sizeof(PrxHostProxyFwInfo_t));
    }
}


/**
 * @brief Extracts a discovery type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param joinResult An output buffer that will get populated with join response information
 */
void PrxExtractJoinResp(uint8_t *buf, uint8_t size, PrxHostClientJoinRspMsg_t *joinResult)
{
    if ((buf != NULL) && (joinResult != NULL))
    {
        size = MIN(size, sizeof(PrxHostClientJoinRspMsg_t));
        memcpy((void*)joinResult, (void*)buf, size);
    }
}

/**
 * @brief Extracts a 'Tone Power Config' (TPC) type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param tpcResult An output field that will get populated with the tpc response information
 */
void PrxExtractTpcResp(uint8_t *buf, uint8_t size, PrxHostTpcCfn_t *tpcResult)
{
    if ((buf != NULL) && (tpcResult != NULL))
    {
        size = MIN(size, sizeof(PrxHostTpcCfn_t));
        memcpy((void*)tpcResult, (void*)buf, size);
    }
}

/**
 * @brief Extracts a Query response type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param queryResult An output field that will get populated with the query cfn response information
 */
void PrxExtractQueryCfnResp(uint8_t *buf, uint8_t size, PrxHostCqRecCfn_t *queryResult)
{
    if ((buf != NULL) && (queryResult != NULL))
    {
        size = MIN(size, sizeof(PrxHostCqRecCfn_t));
        memcpy((void*)queryResult, (void*)buf, size);
    }
}


/**
 * @brief Extracts a Query data type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param queryResult An output field that will get populated with the query data response information
 */
void PrxExtractQueryDataResp(uint8_t *buf, uint8_t size, ClientQueryDataCstm_t *queryResult)
{
    if ((buf != NULL) && (queryResult != NULL))
    {
        size = MIN(size, sizeof(ClientQueryDataCstm_t));
        memcpy((void*)queryResult, (void*)buf, size);
    }
}

/**
 * @brief Extracts a Disconnect response type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param disconnectResult An output field that will get populated with the query response information
 */
void PrxExtractDisconnectResp(uint8_t *buf, uint8_t size, PrxHostLeaveNetworkCfn_t *disconnectResult)
{
    if ((buf != NULL) && (disconnectResult != NULL))
    {
        size = MIN(size, sizeof(PrxHostLeaveNetworkCfn_t));
        memcpy((void*)disconnectResult, (void*)buf, size);
    }
}

/**
 * @brief Extracts a Disconnect response type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param disconnectResult An output field that will get populated with the query response information
 */
void PrxExtractAppCommandResp(uint8_t *buf, uint8_t size, PrxHostLeaveNetworkCfn_t *disconnectResult)
{
    if ((buf != NULL) && (disconnectResult != NULL))
    {
        size = MIN(size, sizeof(PrxHostLeaveNetworkCfn_t));
        memcpy((void*)disconnectResult, (void*)buf, size);
    }
}

/**
 * @brief Extracts the response from a proxy command and saves it to the appropriate struct.
 * @todo This function will later need to be modified for frequency agility, since
 *       it will be needed to process rssi values of different channels.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer (used to gaurd against buffer overruns)
 * @param proxyCmdData The stucture that receieves the extracted data
 */
void PrxExtractPrxCmdResp(uint8_t *buf, uint8_t size, PrxHostPrxCmdResp_t *proxyCmdData)
{
    if ((buf != NULL) && (proxyCmdData != NULL))
    {
        size = MIN(size, sizeof(PrxHostPrxCmdResp_t));
        memcpy((void*)proxyCmdData, (void*)buf, size);
    }
}

/**
 * @brief Extracts data from the response of a proxy command data and saves it to the appropriate struct
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer (used to gaurd against buffer overruns)
 * @param clientCmdData The structure to fill with the extracted data
 */
void PrxExtractAppDataResp(uint8_t *buf, uint8_t size, PrxHostAppData_t *clientCmdData)
{
    if ((buf != NULL) && (clientCmdData != NULL))
    {
        size = MIN(size, sizeof(PrxHostAppData_t));
        memcpy((void*)clientCmdData, (void*)buf, size);
    }
}

/**
 * @brief Extracts the status from an app command confirmation message
 *
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer (used to gaurd against buffer overruns)
 */
void PrxExtractAppCmdCfn(uint8_t *buf, uint8_t size)
{
    PrxHostClientCmdCfn_t cmdCfn;
    if (buf != NULL)
    {
        size = MIN(size, sizeof(PrxHostClientCmdCfn_t));
        if (size < sizeof(PrxHostClientCmdCfn_t))
        {
            PostPrintf("Insufficient allocation for buffer!\r\n");
            memset((void *)buf, 0, size); //clear rather than possibly leave random junk
        }
        else
        {
            memcpy((void*)&cmdCfn, (void*)buf, size);
            if (cmdCfn.status)
            {
                CmSetAppCmdTriesLeft(cmdCfn.extAddr, 0);
            }
            PostPrintf("App Command to 0x%llx %s.\r\n",
                       *(uint64_t*)cmdCfn.extAddr.bytes,
                       (cmdCfn.status == PRX_HOST_STAT_SUCCESS) ? "succeeded": "failed");
        }
    }
}

/**
 * @brief Retrieves the comm channel number from an app command
 * @param appCmd Pointer to the app command
 * @return The comm channel
 */
uint8_t PrxExtractCommChannelFromAppCommand(PrxHostClientCmdMsg_t* appCmd)
{
    uint8_t ret = 0;
    if (appCmd != NULL)
    {
        ret = appCmd->payload.bytes[1];
    }
    return ret;
}

/**
 * @brief Extracts data from a proxy msg and saves it to PrxHostAnnouncement_t struct
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer (used to gaurd against buffer overruns)
 * @param clientCmdData The structure to fill with the extracted data
 */
void PrxExtractAnnoucement(uint8_t *buf, uint16_t size, PrxHostAnnouncement_t *annc)
{
    if ((buf != NULL) && (annc != NULL))
    {
        size = MIN(size, sizeof(PrxHostAnnouncement_t));
        memcpy((void*)annc, (void*)buf, size);
    }
}

/**
 * @brief Extracts data from the Receiver Configuration Response message from the proxy
 * @param buf      An input buffer that contains the data that will be parsed
 * @param size     The size of the input buffer (used to gaurd against buffer overruns)
 * @param cfgResp  Pointer to the structure to place extracted data into
 */
void PrxExtractConfigurationResp(uint8_t *buf, uint16_t size, PrxHostRcvrCfgRsp_t *cfgResp)
{
    if ((buf != NULL) && (cfgResp != NULL))
    {
        size = MIN(size, sizeof(PrxHostRcvrCfgRsp_t));
        memcpy((void*)cfgResp, (void*)buf, size);
    }
}

void PrxExtractImmediateShutoff(uint8_t *buf, uint16_t size, PrxHostImmediateShutoff_t *shutoff)
{
    if ((buf != NULL) && (shutoff != NULL))
    {
        size = MIN(size, sizeof(PrxHostImmediateShutoff_t));
        memcpy((void*)shutoff, (void*)buf, size);
    }
}

/**
 * @brief Send message to the proxy that it needs to transmit a discovery message
 *        and respond with the clients that it has found.
 *
 *        The PrxHostStartDiscMsg_t message tells the proxy to do one of two things.
 *          1. It will send a discovery message to the clients when the PrxHostStartDiscMsg_t
 *             is received at the proxy.
 *          2. It will send a discovery message again at a time in accordance to the time
 *             as set in the 'period' field in PrxHostStartDiscMsg_t.
 *        Orion has a finer scope of control over timing than its predecessors and this periodic
 *        behavior becomes less desirable. For this reason, this function will send the special
 *        period value to the proxy so the proxy just sends messages when the host sends a
 *        discovery message via this function call.
 *
 * @todo  The PrxHostStartDiscMsg_t periodic behavior should  be removed in the proxy for
 *        simplicity's sake and give full control of timing to the host.
 *
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendDisc(uint8_t channel)
{
    PrxHostStartDiscMsg_t*  discMsg;
    //uint16_t                discPeriod;
    uint8_t                 buf[MSG_SIZE_DISC];

    // Offset by one to make space in the buff for the message ID
    discMsg = (PrxHostStartDiscMsg_t *) &buf[1];

    // Read configuration data
    CfgGetParam(CFG_NETWORK_PAN_ID,     &discMsg->panId,        sizeof(&discMsg->panId));
    CfgGetParam(CFG_PROXY_ID,           &discMsg->shortAddr,    sizeof(discMsg->shortAddr));
    //CfgGetParam(CFG_DISCOVERY_PERIOD,   &discMsg->period,       sizeof(discMsg->period));

    // If the channel is 0, then we use channel set in the config file.

    // This function is stupid, meaning it does the one action of
    //   sending a discovery message. It does not check for
    //   correctness and it is expected for that to be handled higher
    //   in abstraction.
    if (channel)
    {
        discMsg->commChannel = channel;
    }
    else
    {
        CfgGetParam(CFG_COMM_CHANNEL, &discMsg->commChannel, sizeof(discMsg->commChannel));
    }

    buf[0] = HOST_PRX_START_DISCOVERY;

    // Use the special value for one-time Discovery messages
    discMsg->period = ONE_TIME_DISCOVERY_MESSAGE_PERIOD;

    return ((ProxySendMessage(buf, MSG_SIZE_DISC) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

/**
 * @brief Send TPS table so the proxy will start TPS
 *
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendTps(void)
{
    CotaError_t         ret = COTA_ERROR_NONE;
    TonePowerSched_t*   tps = GetTpsSchedule();
    uint8_t             size;

    if (tps->numClients > 0)
    {
        //We should only send the portion of the structure that is valid, so we must substract the unsused slots from the size
        size = sizeof(TonePowerSched_t);
        size -= (sizeof(TpsClientData_t) * (MAX_NUM_CLIENTS_IN_TPS - tps->numClients));
        ret = (ProxySendMessage((uint8_t*)tps, size) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG;
    }

    return ret;
}

/**
 * @brief Send a message to proxy requesting its version info
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendReqInfo(void)
{
    ProxyHostMsgType_t regInfo = HOST_PRX_GET_PROXY_INFO;

    return ((ProxySendMessage((uint8_t*)&regInfo, sizeof(regInfo)) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

/**
 * @brief Send message to proxy that it needs to transmit a join message to a
 *        particular client and get an acknowledgment from the client that
 *        it has properly joined the network.
 * @param clientLongId  The long id associated with the target client
 * @param clientShortId The short id associated with the target client
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendJoin(ExtdAddr_t clientLongId, ShrtAddr_t clientShortId)
{
    PrxHostJoinClientMsg_t* joinMsg;
    uint8_t                 buf[MSG_SIZE_JOIN];

    // Offset by one to make space in the buff for the message ID
    joinMsg = (PrxHostJoinClientMsg_t *) &buf[1];

    // Message ID
    buf[0]  = HOST_PRX_JOIN_CLIENT;

    // Memcpy the extended ID
    memcpy((void*) &joinMsg->extAddr, (void *) &clientLongId, sizeof(joinMsg->extAddr));
    joinMsg->shortAddr = clientShortId;

    return ((ProxySendMessage(buf, MSG_SIZE_JOIN) == true) ?
            COTA_ERROR_NONE :
            COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

/**
 * @brief Tell the proxy that it needs to transmit a Tpc message to a specified client.
 *        An acknowledgment is expected from the proxy after sending this message.
 *
 *        This is step where we would send a beacon encoding to a client. But, because this
 *        has never been properly implemented or planned, the encoding is set to 0.
 *
 * @param clientShortId The short id associated with the target client.
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendTpc(ShrtAddr_t clientShortId)
{
    PrxHostTonePowerConfig_t*   tpcMsg;
    CotaTonePowerConfig_t*      tpcPay;
    uint8_t                     buf[MSG_SIZE_TPC];

    buf[0] = HOST_PRX_TONE_POWER_CONFIG;
    // Init pointers, Offset by one to make space in the buff for the message ID
    tpcMsg = (PrxHostTonePowerConfig_t *) &buf[1];
    tpcMsg->shortAddress = clientShortId;

    tpcPay = (CotaTonePowerConfig_t *)    &tpcMsg->data;
    // Read configuration data
    CfgGetParam(CFG_TPS_RX_DURATION     , &tpcPay->toneSlotDuration,  sizeof(tpcPay->toneSlotDuration));
    CfgGetParam(CFG_TPS_TX_DURATION     , &tpcPay->powerSlotDuration, sizeof(tpcPay->powerSlotDuration));
    CfgGetParam(CFG_TPS_NUM_POWER_SLOTS , &tpcPay->powerSlotCount,    sizeof(tpcPay->powerSlotCount));
    CfgGetParam(CFG_TPS_NUM_BEACON_BEATS, &tpcPay->totalTones,        sizeof(tpcPay->totalTones));

    // Since the beacon encoding has not been designed, set the array to 0
    memset((void *)tpcPay->beaconEncoding, 0, sizeof(tpcPay->beaconEncoding));

    return ((ProxySendMessage(buf, MSG_SIZE_TPC) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

/**
 * @brief Tell the proxy that it needs to transmit a Query message to a specific client.
 *        An acknowledgment is expected from the proxy after sending this message.
 *
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendQuery(void)
{
    CotaError_t     ret = COTA_ERROR_NONE;
    CqtSchedule_t*  cqt = GetCqtSchedule();
    uint8_t         size;

    if (cqt->header.totalNumClients > 0)
    {
        // We only send the valid parts of the structure, so we subtract the unsused slots from the size
        size = sizeof(CqtSchedule_t);
        size -= (sizeof(PrxHostCqTableEntry_t) * (MAX_NUM_CLIENTS_TO_QUERY-cqt->header.totalNumClients));
        ret = (ProxySendMessage((uint8_t*)cqt, size) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG;
    }

    return ret;
}

/**
 * @brief Tell the proxy that it needs to transmit a disconnect message to a specific client.
 *        An acknowledgment is expected from the proxy after sending this message.
 * @param clientShortId The shortId of the client that we are removing from the network.
 * @result returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendDisconnect(ShrtAddr_t clientShortId)
{
    PrxHostDisconnectClientMsg_t*   disconnectMsg;
    uint8_t                         buf[MSG_SIZE_DISCONNECT];

    // Message ID
    buf[0] = HOST_PRX_DISCONNECT_CLIENT;

    disconnectMsg = (PrxHostDisconnectClientMsg_t *) &buf[1];
    disconnectMsg->shortAddr = clientShortId;

    return ((ProxySendMessage(buf, MSG_SIZE_DISCONNECT) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

/**
 * @brief Send a command to the proxy.
 * @param proxyCmd Pointer to type #PrxHostProxyCmdMsg_t containing proxy command info
 * @result returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendCommand(PrxHostProxyCmdMsg_t* proxyCmd)
{
    uint8_t     size = 0;  // Proxy message size (including message ID, payload size, message type and the payload field)
    CotaError_t err = COTA_ERROR_FAILED_TO_SEND_PROXY_MSG;

    if (proxyCmd != NULL)
    {
        // Set the first byte of gProxyCmdMsgBuf to proxy message type
        gProxyCmdMsgBuf[0] = HOST_PRX_PROXY_CMD;
        size++;

        // Add the size of payload size field, the msgType field and the payload field
        // Note:  The proxyCmd->payloadSize field includes the size of msgType and payload fields
        size += sizeof(proxyCmd->payloadSize);
        size += MIN(proxyCmd->payloadSize, sizeof(proxyCmd->payload));

        // Copy everything from proxyCmd to the bytes following the 1st byte of gProxyCmdMsgBuf
        memcpy((void*) &gProxyCmdMsgBuf[1], (void*) proxyCmd, size - 1);

        err = ((ProxySendMessage(gProxyCmdMsgBuf, size) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
    }

    return err;
}

/**
 * @brief Send a client command to the proxy.
 * @param appCmd Pointer to type #PrxHostClientCmdMsg_t containing client command info
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendAppCommand(PrxHostClientCmdMsg_t* appCmd)
{
    uint8_t     size = 0;
    CotaError_t err = COTA_ERROR_FAILED_TO_SEND_PROXY_MSG;

    if (appCmd != NULL)
    {
        gAppCmdMsgBuf[0] = HOST_PRX_CLIENT_CMD;
        size += sizeof(appCmd->extAddr);
        size += sizeof(appCmd->payloadSize);
        appCmd->payloadSize = MIN(appCmd->payloadSize, sizeof(appCmd->payload));
        size += appCmd->payloadSize;

        memcpy((void*) &gAppCmdMsgBuf[1], (void*) appCmd, size);

        err = ((ProxySendMessage(gAppCmdMsgBuf, size) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
    }

    return err;
}

/**
 * @brief Send a light ring message to the proxy
 * @param msg Pointer to type #PrxHostLightRingMsg_t containing client command info
 * @result returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendLightRingMsg(PrxHostLightRingMsg_t *msg)
{
    uint8_t     buf[MSG_SIZE_LIGHT_RING_MSG];
    CotaError_t err = COTA_ERROR_FAILED_TO_SEND_PROXY_MSG;

    if (msg != NULL)
    {
        buf[0] = HOST_PRX_SET_LR_STATE;
        memcpy((void*) &buf[1], (void*) msg, sizeof(PrxHostLightRingMsg_t));
        err = ((ProxySendMessage(buf, MSG_SIZE_LIGHT_RING_MSG) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
    }

    return err;
}

/**
 * @brief Send the proxy a setup message
 * @param msg The message of type PrxHostProxySetup_t
 * @result returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendProxySetup(void)
{
    PrxHostProxySetup_t setup;
    uint8_t             buf[MSG_SIZE_PROXY_SETUP];

    CfgGetParam(CFG_COMM_CHANNEL,               &setup.commChannel,             sizeof(setup.commChannel));
    CfgGetParam(CFG_NETWORK_PAN_ID,             &setup.proxyPanId,              sizeof(setup.proxyPanId));
    CfgGetParam(CFG_PROXY_ID,                   &setup.proxyShortAddr,          sizeof(setup.proxyShortAddr));
    CfgGetParam(CFG_DOOR_OPEN_MSG_PERIOD,       &setup.doorOpenMsgPeriodMs,     sizeof(setup.doorOpenMsgPeriodMs));
    CfgGetParam(CFG_PROXY_ANNOUNCEMENT_DELAY,   &setup.announcementDelayMs,     sizeof(setup.announcementDelayMs));
    CfgGetParam(CFG_PROXY_RSSI_FILTER_MIN_VAL,  &setup.rssiFilterMinValueDbm,   sizeof(setup.rssiFilterMinValueDbm));

    buf[0] = HOST_PRX_PROXY_SETUP;
    memcpy((void*) &buf[1], (void*) &setup, sizeof(PrxHostProxySetup_t));

    return ((ProxySendMessage(buf, MSG_SIZE_PROXY_SETUP) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

/**
 * @brief Send the proxy a Go message
 * @param msg the message of type CotaRcvrGo_t
 * @result returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendGo(CotaRcvrGo_t *msg)
{
    uint8_t     buf[MSG_SIZE_GO_MSG];
    CotaError_t err = COTA_ERROR_FAILED_TO_SEND_PROXY_MSG;

    if (msg != NULL)
    {
        buf[0] = HOST_PRX_RCVR_GO;
        memcpy((void*) &buf[1], (void*) msg, sizeof(CotaRcvrGo_t));

        err = ((ProxySendMessage(buf, MSG_SIZE_GO_MSG) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
    }

    return err;
}


/**
 * @brief  Send a Receiver Configuration message to the proxy
 * @param  longId   Receiver's long ID/MAC address
 * @param  shortId  Short address to assign to the receiver
 * @result returns COTA_ERROR_NONE on success; COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
CotaError_t PrxSendReceiverConfig(ExtdAddr_t longId, ShrtAddr_t shortId)
{
    PrxHostNpiPayload_t prxMsg;
    uint8_t*            buf = (uint8_t*) &prxMsg;

    prxMsg.msgType                          = HOST_PRX_RCVR_CONFIG;
    prxMsg.rcvrConfigMsg.extAddr            = longId;
    prxMsg.rcvrConfigMsg.config.shortAddr   = shortId;

    CfgGetParam(CFG_TPS_RX_DURATION,        &prxMsg.rcvrConfigMsg.config.toneSlotDuration,      sizeof(prxMsg.rcvrConfigMsg.config.toneSlotDuration));
    CfgGetParam(CFG_TPS_TX_DURATION,        &prxMsg.rcvrConfigMsg.config.powerSlotDuration,     sizeof(prxMsg.rcvrConfigMsg.config.powerSlotDuration));
    CfgGetParam(CFG_TPS_NUM_POWER_SLOTS,    &prxMsg.rcvrConfigMsg.config.powerSlotCount,        sizeof(prxMsg.rcvrConfigMsg.config.powerSlotCount));
    CfgGetParam(CFG_TPS_NUM_BEACON_BEATS,   &prxMsg.rcvrConfigMsg.config.totalTones,            sizeof(prxMsg.rcvrConfigMsg.config.totalTones));
    CfgGetParam(CFG_RCVR_COMM_PWR,          &prxMsg.rcvrConfigMsg.config.commPwrLevel,          sizeof(prxMsg.rcvrConfigMsg.config.commPwrLevel));
    CfgGetParam(CFG_XIRGO_SLEEP_TIME,       &prxMsg.rcvrConfigMsg.config.trackerSleepDelay,     sizeof(prxMsg.rcvrConfigMsg.config.trackerSleepDelay));
    CfgGetParam(CFG_RCVR_CHARGING_THRESHOLD,&prxMsg.rcvrConfigMsg.config.stopChargingSocLevel,  sizeof(prxMsg.rcvrConfigMsg.config.stopChargingSocLevel));

    return (ProxySendMessage(buf, MSG_SIZE_RCVR_CONFIG) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}


