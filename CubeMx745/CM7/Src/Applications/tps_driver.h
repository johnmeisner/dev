/**
 * @file       tps_driver.h
 *
 * @brief      TPS driver header file
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _TPS_DRIVER_H_
#define _TPS_DRIVER_H_

#include "FreeRTOS.h"
#include "semphr.h"
#include "stm32h7xx_hal.h"

BaseType_t TpsDrvInit(void);
void TpsTimerPeriodElapsedEvent(TIM_HandleTypeDef *htim);
void SetTpsDelay(uint16_t time);
void StopTpsCycle(void);
int GetTpsBeat(void);
#endif /*#ifndef _TPS_DRIVER_H_*/
