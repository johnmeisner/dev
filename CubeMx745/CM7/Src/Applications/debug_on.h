/**
 * @file       debug_on.h
 *
 * @brief      Debug pin control macros for modules with debug pins enabled
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 * @details    Including this file enables debug pins and frees the LEDs from automated 
 *             status updates for debugging use.  
 *
 */

#ifndef _DEBUG_ON_H_
#define _DEBUG_ON_H_
#define DBG1_SET()     HAL_GPIO_WritePin(DEBUG_1_GPIO_Port,  DEBUG_1_Pin, GPIO_PIN_SET)     ///< Set debug pin 1 high
#define DBG2_SET()     HAL_GPIO_WritePin(DEBUG_2_GPIO_Port,  DEBUG_2_Pin, GPIO_PIN_SET)     ///< Set debug pin 2 high
#define DBG3_SET()     HAL_GPIO_WritePin(DEBUG_3_GPIO_Port,  DEBUG_3_Pin, GPIO_PIN_SET)     ///< Set debug pin 3 high
#define DBG4_SET()     HAL_GPIO_WritePin(DEBUG_4_GPIO_Port,  DEBUG_4_Pin, GPIO_PIN_SET)     ///< Set debug pin 4 high
#define DBG5_SET()     HAL_GPIO_WritePin(DEBUG_5_GPIO_Port,  DEBUG_5_Pin, GPIO_PIN_SET)     ///< Set debug pin 5 high
#define DBG6_SET()     HAL_GPIO_WritePin(DEBUG_6_GPIO_Port,  DEBUG_6_Pin, GPIO_PIN_SET)     ///< Set debug pin 6 high
#define DBG7_SET()     HAL_GPIO_WritePin(DEBUG_7_GPIO_Port,  DEBUG_7_Pin, GPIO_PIN_SET)     ///< Set debug pin 7 high
#define DBG8_SET()     HAL_GPIO_WritePin(DEBUG_8_GPIO_Port,  DEBUG_8_Pin, GPIO_PIN_SET)     ///< Set debug pin 8 high
#define DBG9_SET()     HAL_GPIO_WritePin(DEBUG_9_GPIO_Port,  DEBUG_9_Pin, GPIO_PIN_SET)     ///< Set debug pin 9 high
#define DBG10_SET()    HAL_GPIO_WritePin(DEBUG_10_GPIO_Port, DEBUG_10_Pin, GPIO_PIN_SET)    ///< Set debug pin 10 high
#define DBG11_SET()    HAL_GPIO_WritePin(DEBUG_11_GPIO_Port, DEBUG_11_Pin, GPIO_PIN_SET)    ///< Set debug pin 11 high
#define DBG12_SET()    HAL_GPIO_WritePin(DEBUG_12_GPIO_Port, DEBUG_12_Pin, GPIO_PIN_SET)    ///< Set debug pin 12 high
#define DBG13_SET()    HAL_GPIO_WritePin(DEBUG_13_GPIO_Port, DEBUG_13_Pin, GPIO_PIN_SET)    ///< Set debug pin 13 high

#define DBG1_RESET()   HAL_GPIO_WritePin(DEBUG_1_GPIO_Port,  DEBUG_1_Pin, GPIO_PIN_RESET)   ///< Set debug pin 1 low
#define DBG2_RESET()   HAL_GPIO_WritePin(DEBUG_2_GPIO_Port,  DEBUG_2_Pin, GPIO_PIN_RESET)   ///< Set debug pin 2 low
#define DBG3_RESET()   HAL_GPIO_WritePin(DEBUG_3_GPIO_Port,  DEBUG_3_Pin, GPIO_PIN_RESET)   ///< Set debug pin 3 low
#define DBG4_RESET()   HAL_GPIO_WritePin(DEBUG_4_GPIO_Port,  DEBUG_4_Pin, GPIO_PIN_RESET)   ///< Set debug pin 4 low
#define DBG5_RESET()   HAL_GPIO_WritePin(DEBUG_5_GPIO_Port,  DEBUG_5_Pin, GPIO_PIN_RESET)   ///< Set debug pin 5 low
#define DBG6_RESET()   HAL_GPIO_WritePin(DEBUG_6_GPIO_Port,  DEBUG_6_Pin, GPIO_PIN_RESET)   ///< Set debug pin 6 low
#define DBG7_RESET()   HAL_GPIO_WritePin(DEBUG_7_GPIO_Port,  DEBUG_7_Pin, GPIO_PIN_RESET)   ///< Set debug pin 7 low
#define DBG8_RESET()   HAL_GPIO_WritePin(DEBUG_8_GPIO_Port,  DEBUG_8_Pin, GPIO_PIN_RESET)   ///< Set debug pin 8 low
#define DBG9_RESET()   HAL_GPIO_WritePin(DEBUG_9_GPIO_Port,  DEBUG_9_Pin, GPIO_PIN_RESET)   ///< Set debug pin 9 low
#define DBG10_RESET()  HAL_GPIO_WritePin(DEBUG_10_GPIO_Port, DEBUG_10_Pin, GPIO_PIN_RESET)  ///< Set debug pin 10 low
#define DBG11_RESET()  HAL_GPIO_WritePin(DEBUG_11_GPIO_Port, DEBUG_11_Pin, GPIO_PIN_RESET)  ///< Set debug pin 11 low
#define DBG12_RESET()  HAL_GPIO_WritePin(DEBUG_12_GPIO_Port, DEBUG_12_Pin, GPIO_PIN_RESET)  ///< Set debug pin 12 low
#define DBG13_RESET()  HAL_GPIO_WritePin(DEBUG_13_GPIO_Port, DEBUG_13_Pin, GPIO_PIN_RESET)  ///< Set debug pin 13 low

#define DBG1_TOGGLE()  HAL_GPIO_TogglePin(DEBUG_1_GPIO_Port,  DEBUG_1_Pin)                  ///< Toggle the debug pin 1
#define DBG2_TOGGLE()  HAL_GPIO_TogglePin(DEBUG_2_GPIO_Port,  DEBUG_2_Pin)                  ///< Toggle the debug pin 2
#define DBG3_TOGGLE()  HAL_GPIO_TogglePin(DEBUG_3_GPIO_Port,  DEBUG_3_Pin)                  ///< Toggle the debug pin 3
#define DBG4_TOGGLE()  HAL_GPIO_TogglePin(DEBUG_4_GPIO_Port,  DEBUG_4_Pin)                  ///< Toggle the debug pin 4
#define DBG5_TOGGLE()  HAL_GPIO_TogglePin(DEBUG_5_GPIO_Port,  DEBUG_5_Pin)                  ///< Toggle the debug pin 5
#define DBG6_TOGGLE()  HAL_GPIO_TogglePin(DEBUG_6_GPIO_Port,  DEBUG_6_Pin)                  ///< Toggle the debug pin 6
#define DBG7_TOGGLE()  HAL_GPIO_TogglePin(DEBUG_7_GPIO_Port,  DEBUG_7_Pin)                  ///< Toggle the debug pin 7
#define DBG8_TOGGLE()  HAL_GPIO_TogglePin(DEBUG_8_GPIO_Port,  DEBUG_8_Pin)                  ///< Toggle the debug pin 8
#define DBG9_TOGGLE()  HAL_GPIO_TogglePin(DEBUG_9_GPIO_Port,  DEBUG_9_Pin)                  ///< Toggle the debug pin 9
#define DBG10_TOGGLE() HAL_GPIO_TogglePin(DEBUG_10_GPIO_Port, DEBUG_10_Pin)                 ///< Toggle the debug pin 10
#define DBG11_TOGGLE() HAL_GPIO_TogglePin(DEBUG_11_GPIO_Port, DEBUG_11_Pin)                 ///< Toggle the debug pin 11
#define DBG12_TOGGLE() HAL_GPIO_TogglePin(DEBUG_12_GPIO_Port, DEBUG_12_Pin)                 ///< Toggle the debug pin 12
#define DBG13_TOGGLE() HAL_GPIO_TogglePin(DEBUG_13_GPIO_Port, DEBUG_13_Pin)                 ///< Toggle the debug pin 13
#endif //_DEBUG_ON_H_