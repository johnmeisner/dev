/**
* @file       error.h
*
* @brief      Cota error logging header file.
*
* @note       Do not use prototypes defined in this header.  Use the MACROS.
*             We want to able to save memory by removing error handling.  
*             By using the MACRO's, we can easily remove error handling by not 
*             including COTA_ERROR_ENABLED in the compile line.
*
* @todo This is just a simple shell of what should be a larger debug message handler that includes
        a separate task to queue up messages, and masks to prioritize messages.
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*/

#ifndef _COTA_ERROR_INCLUDE_H_
#define _COTA_ERROR_INCLUDE_H_

/**
 * @brief  Error value definitions
 *
 * @note   Numeric values should not be assigned to enumeration constants in order
 *         for the compiler to be able to use the COTA_ERROR_COUNT enum value as
 *         the number of defined error values.
 */
typedef enum _CotaError_t
{
    COTA_ERROR_NONE                           ,  ///< 0  A Non-Error from a function call
    COTA_ERROR_HAL                            ,  ///< 1  An error from a HAL driver
    COTA_ERROR_QUEUE_FAILED                   ,  ///< 2  The error handler failed to create its queue
    COTA_ERROR_TASK_FAILED                    ,  ///< 3  The error handler failed to create its task.
    COTA_ERROR_SPI_TRANSFER_FAILED            ,  ///< 4  A SPI transfer has failed
    COTA_ERROR_SPI_SIMUL_TRANS_FAILED         ,  ///< 5  A simultaneous SPI transfer has failed
    COTA_ERROR_UVP_SIMUL_READ_TOO_LARGE       ,  ///< 6  A simultaneous read of several UVP's has a buffer that is too large.
    COTA_ERROR_UART_TX_FAILED                 ,  ///< 7  A UART transmit failed
    COTA_ERROR_LOCK_DETECT_FAILED             ,  ///< 8  An attempt to lock a UVP clock failed 
    COTA_ERROR_UVP_TEST_FAILED                ,  ///< 9  The UVP test failed.
    COTA_ERROR_UVP_REG_VALUE_NOT_IN_MASK      ,  ///< 10 A UVP register value did not fit in its mask.
    COTA_ERROR_AMB_NOT_ENABLED                ,  ///< 11 An attempt was made to enable a UVP on an AMB not enabled.
    COTA_ERROR_UVP_UNKNOWN                    ,  ///< 12 An attempt was made to select an unknown UVP number
    COTA_ERROR_UVP_WAIT_FOR_TRANSMIT_TIMEOUT  ,  ///< 13 An error occurred while waiting for a UVP to transmit
    COTA_ERROR_UVP_WAIT_FOR_PHASE_DET_TIMEOUT ,  ///< 14 An error occurred while wait for a phase detect from a UVP
    COTA_ERROR_UVP_ENABLE                     ,  ///< 15 An error occurred while enabling UVP's
    COTA_ERROR_SYSCLK_FAILED_TO_WRITE_PAGE    ,  ///< 16 An error occurred while selecting the page for the system clock
    COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG     ,  ///< 17 An error occurred while writing a register to the system clock
    COTA_ERROR_SYSCLK_FAILED_TO_READ_REG      ,  ///< 18 An error occurred while reading a register from the system clock
    COTA_ERROR_I2C_INVALID_MULTIPLEXER        ,  ///< 19 An error occurred because an invalid i2c multiplexer was initialized
    COTA_ERROR_I2C_INVLAID_MULTI_CHANNEL      ,  ///< 20 An error occurred because an invalid i2c multiplexer channel was initialized
    COTA_ERROR_I2C_FAILED_TO_WRITE_MULTI_CHAN ,  ///< 21 An error occurred while selecting the channel on an i2c multiplexer
    COTA_ERROR_I2C_FAILED_TO_WRITE            ,  ///< 22 A failure occurred while writing to an i2c device.
    COTA_ERROR_I2C_FAILED_TO_READ             ,  ///< 23 A failure occurred while reading from an i2c device
    COTA_ERROR_NOT_IMPLEMENTED                ,  ///< 24 A function was called that is not implemented.
    COTA_ERROR_INVALID_CCB_THERMOMETER        ,  ///< 25 An attempt was made to read a temperature from a thermometer that doesn't exist. 
    COTA_ERROR_BAD_PARAMETER                  ,  ///< 26 A bad parameter was passed to a function.
    COTA_ERROR_OVER_CRITICAL_TEMPERATURE      ,  ///< 27 A critical temperature has been exceeded
    COTA_ERROR_RCVR_MANAGER_INIT_FAILED       ,  ///< 28 The Client Manager failed to properly load clients an initialization
    COTA_ERROR_RCVR_STORAGE_FULL              ,  ///< 29 The maximum number of clients available has been reached.
    COTA_ERROR_RCVR_COULD_NOT_BE_FOUND        ,  ///< 30 The client could not be found in the database
    COTA_ERROR_RCVR_ALREADY_REGISTERED        ,  ///< 31 The client has already been registered in the database
    COTA_ERROR_NULL_VALUE_PASSED              ,  ///< 32 The function was passed a null value
    COTA_ERROR_FAILED_TO_REMOVE_NODE          ,  ///< 33 The Client Manager failed to remove the node
    COTA_ERROR_COMM_TIMER_START_FAIL          ,  ///< 34 A timer failed to start 
    COTA_ERROR_FAILED_TO_CREATE_VAR_QUEUE     ,  ///< 35 A variable length queue could not be created
    COTA_ERROR_FAILED_TO_SEND_TO_VAR_QUEUE    ,  ///< 36 A message could not be sent to a variable queue
    COTA_ERROR_CLI_TX_FAILED                  ,  ///< 37 A message to the CLI could not be sent.
    COTA_ERROR_CONFIG_MUTEX_FAILED_TAKE       ,  ///< 38 The mutex in the orion_config.c file failed to take
    COTA_ERROR_CONFIG_MUTEX_FAILED_GIVE       ,  ///< 39 The mutex in the orion_config.c file failed to give
    COTA_ERROR_FAILED_TO_SEND_PROXY_MSG       ,  ///< 40 Failed to send a message to the proxy
    COTA_ERROR_FAILED_TO_PARSE_PROXY_MSG      ,  ///< 41 The Ctrl Task failed to parse a proxy message
    COTA_ERROR_NVM_MEMORY_FULL                ,  ///< 42 Saving client data to NVM failed because memory is full
    COTA_ERROR_NVM_INVALID_DELETE             ,  ///< 43 Attempting to delete a client that doesn't exist in NVM
    COTA_ERROR_EEPROM_INVALID_ADDRESS         ,  ///< 44 Attempting to write to an invalid EEPROM address
    COTA_ERROR_CFG_PARAM_NOT_FOUND            ,  ///< 45 Attempting to read a non-existent configuration parameter
    COTA_ERROR_FAILED_TO_LOAD_RCVR_FROM_NVM   ,  ///< 46 Failed to load a client from the NVM
    COTA_ERROR_TPS_SCHEDULER_FAILED           ,  ///< 47 The scheduler failed to create a schedule for tps
    COTA_ERROR_CQT_SCHEDULER_FAILED           ,  ///< 48 The scheduler failed to create a schedule for queries
    COTA_ERROR_QUERY_CONFIRM_FAILED           ,  ///< 49 A query confirmation failed.
    COTA_ERROR_CTRL_TIMERS_INIT_FAILED        ,  ///< 50 The control task timer failed to initialize.
    COTA_ERROR_SEMAPHORE_CREATION_FAILED      ,  ///< 51 A semaphore or mutex failed to create
    COTA_ERROR_POWER_FAILURE                  ,  ///< 52 The power supplies failed to come up.
    COTA_ERROR_NOT_IN_DEBUG_MODE              ,  ///< 53 A 'debug mode only' message was received when we're not in debug mode
    COTA_ERROR_PRX_FW_UPDATE_CMD              ,  ///< 54 A FW Update command error
    COTA_ERROR_RSSI_FILTER_NOT_ENABLED        ,  ///< 55 An RSSI Filter command was issued without RSSI voting enabled
    COTA_ERROR_CALIBRATION_NEEDS_AMBS         ,  ///< 56 Calibration requires at least one AMB to be valid
    COTA_ERROR_DOOR_INIT_FAILED               ,  ///< 57 Door initialization failed.
    COTA_ERROR_DOOR_NOT_SUPPORTED             ,  ///< 58 Door open/close detection not supported.
    COTA_ERROR_INVALID_CTRL_MSG_RECEIVED      ,  ///< 59 Invalid ctrl message received
    COTA_ERROR_APP_CMD_BUSY                   ,  ///< 60 An app command cannot be issued because the control task is busy sending other app commands
    COTA_ERROR_RCVR_NVM_NOT_FOUND             ,  ///< 61 A NVM client of the Id selected could not be found 
    COTA_ERROR_RCVR_NOT_REGISTERED            ,  ///< 62 The client is Discovered but not Registered.
    COTA_ERROR_RUN_MODE_REQUIRED              ,  ///< 63 A RUN mode required command was received while in DEBUG mode.
    COTA_ERROR_FAILED_TO_RECEIVE_MSG_QUEUE    ,  ///< 64 Failed to return data from message queue
    COTA_ERROR_COUNT                             ///< The total count of error values defined above
} CotaError_t;

#ifdef COTA_ERROR_ENABLED
CotaError_t PostCotaError(CotaError_t cotaError);
#define POST_COTA_ERROR(num) (PostCotaError(num))
#else
#define POST_COTA_ERROR(num) (num)
#endif

#endif
