/****************************************************************************//**
 * @file      variable_queue.h
 *
 * @brief     Variable length queue header
 *
 * @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
 *            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
 *            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
 *            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
 *            ALL RIGHTS RESERVED.
 ********************************************************************************/
#ifndef _VARIABLE_QUEUE_H_
#define _VARIABLE_QUEUE_H_
#include "error.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "stdbool.h"

/**
 * @brief  Structure that holds state information on the variable queue buffer. 
 *         Note: Do not touch this outside of the API below.
 */ 
typedef struct _VariableQueueControl_t 
{
    uint16_t tail;                        ///< Index to the next place to read within the buffer. */
    uint16_t head;                        ///< Index to the next place to write within the buffer. */
    uint16_t length;                      ///< The length of the buffer passed to #VariableQueueCreateStatic
    SemaphoreHandle_t mutex;            ///< Mutex protecting the variable queue calls
    StaticSemaphore_t mutexBuff;        ///< Buffer needed to statically allocate the above mutex
    SemaphoreHandle_t signalSem;        ///< Signal a receive that a message has been sent to the buffer
    StaticSemaphore_t signalSemBuf;     ///< Buffer needed to statically allocate the above semaphore
    uint8_t *storageArea;               ///< Points to the storage itself - that is - the RAM that stores the data passed through the buffer. */
} VariableQueueControl_t;

CotaError_t VariableQueueCreateStatic(uint16_t bufferSizeBytes, uint8_t *storageArea, VariableQueueControl_t *queueControl);
uint16_t VariableQueueReceive(VariableQueueControl_t *queueControl, uint8_t *rxData, uint16_t length, TickType_t ticksToWait );
uint16_t VariableQueueSend( VariableQueueControl_t *queueControl, uint8_t* txData, uint16_t length);
uint16_t VariableQueueSpacesAvailable(VariableQueueControl_t *queueControl);
bool IsVariableQueryEmpty(VariableQueueControl_t* queueControl);

#endif // #ifndef _VARIABLE_QUEUE_H_
