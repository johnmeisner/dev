/**
 * @file       proxy_fw_update_driver.c
 *
 * @brief      Proxy firmware update driver
 *
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/////////////////////////////////////////////////////////////////////////////////////////////
// Includes
/////////////////////////////////////////////////////////////////////////////////////////////

#include "proxy_fw_update_driver.h"
#include "ctrl_task.h"
#include "cli_task.h"
#include "main.h"
#include "CotaCompiler.h"
#include "FreeRTOS.h"
#include "task.h"
#include <string.h>


/////////////////////////////////////////////////////////////////////////////////////////////
// Defines
/////////////////////////////////////////////////////////////////////////////////////////////

#define MR_LINE_ASSERTED             GPIO_PIN_RESET            ///< The state of an asserted PROXY_MASTER_READY (MR) pin
#define MR_LINE_DEASSERTED           GPIO_PIN_SET              ///< The state of a deasserted PROXY_MASTER_READY (MR) pin

#define CS_ASSERT()                  HAL_GPIO_WritePin(PROXY_MASTER_READY_GPIO_Port, PROXY_MASTER_READY_Pin, MR_LINE_ASSERTED)          ///< Assert chip select line
#define CS_DEASSERT()                HAL_GPIO_WritePin(PROXY_MASTER_READY_GPIO_Port, PROXY_MASTER_READY_Pin, MR_LINE_DEASSERTED)        ///< Deassert chip select line

#define PROXY_RESET_DELAY_MS         100      ///< Proxy reset state period in milliseconds
#define XFER_TIMEOUT_MS              1000     ///< SPI transfer timeout in milliseconds for communication with the proxy bootloader
#define ACK_DATA_SIZE                1        ///< Acknowledgement data packet size (1 byte)
#define MAX_BYTES_TO_ACK             1000     ///< Maximum number of bytes to read waiting for an ACK byte before giving up
#define ACK_BYTE_VALUE               0xCC     ///< ACK byte sent/received after sending or receiving a data packet over the SPI bus
#define NACK_BYTE_VALUE              0x33     ///< NACK byte received after sending a data packet over the SPI bus
//#define MAX_READ_PAYLOAD_SIZE      253      ///< Payload data size in a SPI packet when reading - this value results in missing response from bootloader
#define MAX_READ_PAYLOAD_SIZE        250      ///< Payload data size in a SPI packet when reading
#define MAX_WRITE_PAYLOAD_SIZE       252      ///< Payload data size in a SPI packet when writing
#define READ_WRITE_BUF_SIZE          255      ///< Size of the buffer gPrxFwSpiBuf used to store read/write data packets for SPI transactions
#define PROXY_FLASH_PAGE_SIZE        0x1000   ///< Proxy flash memory page size (flash memory gets erased in 1 page increments)
#define WRITABLE_PROXY_FLASH_SIZE    0x1F000  ///< We should only write to the first 0x1F000 bytes of flash memory
#define READ_ACCESS_TYPE_8_BIT       0        ///< Access Type field value for the COMMAND_MEMORY_READ command indicating read operation requesting data in 8-bit increments
#define MAX_TRIES                    20       ///< The maximum number of command retransmission attempts
#define RX_PACKET_HEADER_SIZE        2        ///< SPI packet header size in bytes for received data packets; the header is the bytes preceding the payload bytes (packet size and checksum)

#define CMD_PING                     0x20     ///< Bootloader command value for COMMAND_PING
#define CMD_GET_STATUS               0x23     ///< Bootloader command value for the COMMAND_GET_STATUS command
#define CMD_DOWNLOAD                 0x21     ///< Bootloader command value for the COMMAND_DOWNLOAD command
#define CMD_SEND_DATA                0x24     ///< Bootloader command value for the COMMAND_SEND_DATA command
#define CMD_SECTOR_ERASE             0x26     ///< Bootloader command value for the COMMAND_SECTOR_ERASE command
#define CMD_MEMORY_READ              0x2A     ///< Bootloader command value for the COMMAND_MEMORY_READ command

#define CMD_PACKET_SIZE_ERASE        7        ///< The data packet size for the erase command
#define CMD_PACKET_SIZE_STATUS       3        ///< The data packet size for the status command
#define CMD_PACKET_SIZE_READ         9        ///< The data packet size for the read command
#define CMD_PACKET_SIZE_DOWNLOAD     11       ///< The data packet size for the download command
#define CMD_PACKET_SIZE_STATUS_RESP  3        ///< The size for the GET_STATUS_COMMAND response data packet
#define CMD_PACKET_SIZE_PING         3        ///< The data packet size for the ping command
#define CMD_PACKET_HEADER_SIZE       3        ///< SPI packet header size in bytes; the header is the bytes preceding the payload bytes (packet size, checksum, command)


/////////////////////////////////////////////////////////////////////////////////////////////
// Typedefs
/////////////////////////////////////////////////////////////////////////////////////////////

/** MEMORY_READ command packet definition */
typedef PACKED struct _PrxFwMemReadCmd_t
{
    uint8_t  packetSize;     ///< The total size of COMMAND_MEMORY_READ data packet, including this field
    uint8_t  checksum;       ///< The checksum for the bytes of this data packet that follow this field
    uint8_t  command;        ///< COMMAND_MEMORY_READ command value
    uint32_t address;        ///< The address to start reading from
    uint8_t  accessType;     ///< Access type (read bytes if accessType == 0 and 32-bit words if accessType == 1)
    uint8_t  numValues;      ///< Number of values to read back (bytes if accessType == 0 and 32-bit words if accessType == 1)
} PrxFwMemReadCmd_t;

/** DOWNLOAD command packet definition */
typedef PACKED struct _PrxFwDownloadCmd_t
{
    uint8_t  packetSize;     ///< The total size of COMMAND_DOWNLOAD data packet, including this field
    uint8_t  checksum;       ///< The checksum for the bytes of this data packet that follow this field
    uint8_t  command;        ///< COMMAND_DOWNLOAD command value
    uint32_t address;        ///< The starting address in proxy flash memory for the data to be sent in COMMAND_SEND_DATA command packets
    uint32_t dataSize;       ///< The size of data to be sent in the subsequent COMMAND_SEND_DATA command packets
} PrxFwDownloadCmd_t;

/** SEND_DATA command packet definition */
typedef PACKED struct _PrxFwSendDataCmd_t
{
    uint8_t packetSize;      ///< The total size of COMMAND_SEND_DATA data packet, including this field
    uint8_t checksum;        ///< The checksum for the bytes of this data packet that follow this field
    uint8_t command;         ///< COMMAND_SEND_DATA command value
    uint8_t dataFirstByte;   ///< The size of data to be sent in COMMAND_SEND_DATA command packets
} PrxFwSendDataCmd_t;

/** GET_STATUS command packet definition */
typedef PACKED struct _PrxFwGetStatusCmd_t
{
    uint8_t packetSize;      ///< The total size of COMMAND_GET_STATUS data packet, including this field
    uint8_t checksum;        ///< The checksum for the bytes of this data packet that follow this field
    uint8_t command;         ///< COMMAND_GET_STATUS command value
} PrxFwGetStatusCmd_t;

/** GET_STATUS command response packet definition */
typedef PACKED struct _PrxFwGetStatusRsp_t
{
    uint8_t packetSize;      ///< The total size of the data packet received in response to the COMMAND_GET_STATUS command, including this field
    uint8_t checksum;        ///< The checksum for the bytes of this data packet that follow this field
    uint8_t status;          ///< The status value (see #PrxCmdStatus_t)
} PrxFwGetStatusRsp_t;

/** GET_STATUS command response status values */
typedef enum _PrxCmdStatus_t
{
    CMD_STATUS_SUCCESS     = 0x40,     ///< Status command return value indicating successful completion of the previous command
    CMD_STATUS_UNKNOWN     = 0x41,     ///< Status command return value indicating an unknown command was previously sent
    CMD_STATUS_INVALID_CMD = 0x42,     ///< Status command return value indicating an invalid command or invalid packet size was previously sent
    CMD_STATUS_INVALID_ADR = 0x43,     ///< Status command return value indicating an invalid address was previously sent
    CMD_STATUS_FLASH_FAIL  = 0x44,     ///< Status command return value indicating a previous flash erase or program operation has failed
} PrxCmdStatus_t;

/** PING command packet definition */
typedef PACKED struct _PrxFwPingCmd_t
{
    uint8_t  packetSize;     ///< The total size of COMMAND_PING data packet, including this field
    uint8_t  checksum;       ///< The checksum for the bytes of this data packet that follow this field
    uint8_t  command;        ///< COMMAND_PING command value
} PrxFwPingCmd_t;

/** SECTOR_ERASE command packet definition */
typedef PACKED struct _PrxFwEraseCmd_t
{
    uint8_t  packetSize;     ///< The total size of COMMAND_SECTOR_ERASE data packet, including this field
    uint8_t  checksum;       ///< The checksum for the bytes of this data packet that follow this field
    uint8_t  command;        ///< COMMAND_SECTOR_ERASE command value
    uint32_t address;        ///< The starting address of the flash memory sector to erase (each sector is 4KB)
} PrxFwEraseCmd_t;

/////////////////////////////////////////////////////////////////////////////////////////////
// Private Variable Declarations
/////////////////////////////////////////////////////////////////////////////////////////////

static bool gProxyBootloaderInitialized = false;            ///< Flag indicating that proxy bootloader has been initialized and is ready for communication
static uint8_t gPrxFwSpiBuf[READ_WRITE_BUF_SIZE] = { 0 };   ///< General-purpose buffer for SPI communication
static uint32_t gStartingWriteAddress = 0;                  ///< The starting flash memory write address used to keep track of the current write address
static bool gProxyWriteAllowed = false;                     ///< Flag that allows writing to flash memory once the erase and download commands have succeeded
static uint32_t gTotalBytesToWrite = 0;                     ///< The number of bytes to be written to flash memory as indicated in the setup/download command
static uint32_t gTotalBytesWritten = 0;                     ///< The total number of bytes written via the SEND_DATA commands since the last DOWNLOAD command was sent


/////////////////////////////////////////////////////////////////////////////////////////////
// Global Variables
/////////////////////////////////////////////////////////////////////////////////////////////

extern SPI_HandleTypeDef PROXY_SPI_HANDLE;


/////////////////////////////////////////////////////////////////////////////////////////////
// Private Function Prototypes
/////////////////////////////////////////////////////////////////////////////////////////////

#ifdef  PRINT_DEBUG_OUTPUT_FOR_PROXY_FW_UPDATE
    static void PrintAckStatus(int32_t status);
#endif
static uint32_t BlEraseFlashMemory(uint32_t address, uint32_t size);
static uint32_t BlReadDataPacket(uint32_t address, uint32_t size, uint8_t * buf);
static uint32_t BlSendDataCmd(uint8_t * buf, uint32_t size);
static bool BlWaitForAck(void);
static bool BlDownloadCmd(uint32_t address, uint32_t size);
static bool BlGetStatus(void);
static void BlApplyPacketChecksum(uint8_t * buf);


/////////////////////////////////////////////////////////////////////////////////////////////
// Function implementation
/////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief  This function activates the proxy bootloader
 *
 * If the system hasn't been put in debug state prior to this
 * function call, this function will fail.
 *
 * @return  true if the function is successful; false otherwise
 *
 */
bool ProxyFwUpdateInit(void)
{
    // Assert proxy reset
    HAL_GPIO_WritePin(PROXY_RESET_GPIO_Port, PROXY_RESET_Pin, GPIO_PIN_RESET);
    vTaskDelay(pdMS_TO_TICKS(PROXY_RESET_DELAY_MS));

    // Assert bootloader enable
    HAL_GPIO_WritePin(PROXY_BL_ENABLE_GPIO_Port, PROXY_BL_ENABLE_Pin, GPIO_PIN_SET);
    vTaskDelay(pdMS_TO_TICKS(PROXY_RESET_DELAY_MS));

    // Deassert proxy reset
    HAL_GPIO_WritePin(PROXY_RESET_GPIO_Port, PROXY_RESET_Pin, GPIO_PIN_SET);
    vTaskDelay(pdMS_TO_TICKS(PROXY_RESET_DELAY_MS));

    // Deassert bootloader enable
    HAL_GPIO_WritePin(PROXY_BL_ENABLE_GPIO_Port, PROXY_BL_ENABLE_Pin, GPIO_PIN_RESET);
    vTaskDelay(pdMS_TO_TICKS(PROXY_RESET_DELAY_MS));

    gProxyBootloaderInitialized = true;

    return gProxyBootloaderInitialized;
}


/**
 * @brief  This function reads from the proxy flash memory
 *
 * If the system hasn't been put in debug state prior to this
 * function call, this function will fail.
 * If the proxy bootloader wasn't active prior to this function
 * call, this function will activate the bootloader.
 *
 * @param  address  The starting address of flash memory to read from
 * @param  buf      The buffer to place data into
 * @param  size     The number of bytes to read (size of the buffer)
 *
 * @return  true if all the requested bytes have been received; false otherwise
 *
 */
bool ProxyFwUpdateRead(uint32_t address, uint8_t * buf, uint32_t size)
{
    uint32_t numBytesToRead = size;
    uint32_t numBytesReceived;

    if (!gProxyBootloaderInitialized)
    {
        return false;
    }

    while (numBytesToRead > 0)
    {
        // Read as much data from the proxy flash memory as the bootloader allows at a time
        numBytesReceived = BlReadDataPacket(address, numBytesToRead, buf);

        if (numBytesReceived == 0)
        {
            break;
        }

        numBytesToRead -= numBytesReceived;
        address += numBytesReceived;
        buf += numBytesReceived;
    }

    return (numBytesToRead == 0);
}


/**
 * @brief  Sets up a write operation to the proxy flash memory
 *
 * If the system hasn't been put in debug state prior to this
 * function call, this function will fail.
 * If the proxy bootloader wasn't active prior to this function
 * call, this function will activate the bootloader.
 * The actual write to the flash memory is accomplished via one
 * or more WriteProxyFwData() function calls.
 *
 * @return  true if the operation is successful; false otherwise
 *
 */
bool ProxyFwUpdateSetup(uint32_t address, uint32_t size)
{
    int32_t  success = true;
    uint32_t erasedBytes;

    gProxyWriteAllowed = false;

    if (!gProxyBootloaderInitialized)
    {
        return false;
    }

    erasedBytes = BlEraseFlashMemory(address, size);
    if (erasedBytes < size)
    {
        PostPrintf("Error erasing page at address 0x%05X.\r\n", address + size - erasedBytes);
        PostPrintf("Erase range: 0x%05X; bytes erased: 0x%05X.\r\n", size, erasedBytes);
        return false;
    }

    // Send the Download command to prepare to write size bytes at address 'address'
    if (!BlDownloadCmd(address, size))
    {
        PostPrintf("Bootloader DOWNLOAD command failed.\r\n");
        return false;
    }
    else
    {
        //PostPrintf("Starting Proxy FW update...\r\n");
        gProxyWriteAllowed = true;
        gStartingWriteAddress = address;
        gTotalBytesToWrite = size;
        gTotalBytesWritten = 0;
    }

    return success;
}


/**
 * @brief  Writes data to the proxy flash memory
 *
 * This function can be called to transfer firmware image data
 * or any other data to the proxy flash memory.
 * This function must be called after calling WriteProxyFwSetup().
 * It can be called multiple times until all the bytes specified
 * in the size parameter of the WriteProxyFwSetup() call have been
 * transferred.
 *
 * @return  true if the write operation is successful; false otherwise
 *
 */
bool ProxyFwUpdateWrite(uint32_t size, uint8_t * buf)
{
    uint32_t remainingBytesToSend = size;
    uint32_t numBytesSent;
    bool     success = true;

    // Ensure a DOWNLOAD command has been issued prior to writing to flash memory
    if (gProxyWriteAllowed)
    {
        while (remainingBytesToSend)
        {
            // How many bytes we want to send in this data packet
            numBytesSent = BlSendDataCmd(buf, remainingBytesToSend);
            remainingBytesToSend -= numBytesSent;
            buf += numBytesSent;
            gTotalBytesWritten += numBytesSent;

            if (numBytesSent == 0)
            {
                PostPrintf("SEND_DATA command failed to write to address 0x%05X.\r\n", gStartingWriteAddress + gTotalBytesWritten);
                success = false;
                break;
            }
        }
    }
    else
    {
        PostPrintf("Proxy FW update setup command not received prior to data write command.\r\n");
    }

    // If all data has been written, reset the gProxyWriteAllowed flag so that
    // another DOWNLOAD command would be required before writing data to flash memory
    if (gTotalBytesWritten == gTotalBytesToWrite)
    {
        gProxyWriteAllowed = false;
    }

    return success;
}


/**
 * @brief  Sends the COMMAND_PING command to the proxy bootloader
 *
 * This function can be used to verify that communication with the
 * proxy bootloader has been established.
 *
 * @return  true if the bootloader acknowledged the ping; false otherwise
 *
 */
bool ProxyFwUpdatePingBL(void)
{
    uint8_t buffer[CMD_PACKET_SIZE_PING] = {0};
    bool success = false;
    uint32_t numReads;
    HAL_StatusTypeDef spiStatus;
    PrxFwPingCmd_t * pingCmdData = (PrxFwPingCmd_t*)buffer;

    if (!gProxyBootloaderInitialized)
    {
        return false;
    }

    for (int i = 0; i < 5; i++)
    {
        // Update data packet fields
        pingCmdData->packetSize = CMD_PACKET_SIZE_PING;
        pingCmdData->command = CMD_PING;
        pingCmdData->checksum = CMD_PING;

        // Assert chip select line
        CS_ASSERT();

        // Write the ping command
        HAL_SPI_Transmit(&PROXY_SPI_HANDLE, buffer, sizeof(buffer), XFER_TIMEOUT_MS);

        memset(buffer, 0, sizeof(buffer));
        numReads = 0;

        // Read the response
        do
        {
            spiStatus = HAL_SPI_Receive(&PROXY_SPI_HANDLE, buffer, ACK_DATA_SIZE, XFER_TIMEOUT_MS);
            numReads++;
        } while ((numReads < MAX_BYTES_TO_ACK) && (buffer[0] == 0) && (spiStatus == HAL_OK));  // Non-zero value is our response

        // Deassert chip select line
        CS_DEASSERT();

        if (spiStatus != HAL_OK)
        {
            PostPrintf("Failed to read SPI bus after %d attempts.\r\n", numReads);
        }
        else if (buffer[0] == ACK_BYTE_VALUE)
        {
            //PostPrintf("Bootloader responded with an ACK after %d attempts.\r\n", numReads);
            success = true;
        }
        else
        {
            PostPrintf("ACK byte 0x%02X after %d attempts.\r\n", buffer[0], numReads);
        }
    }

    return success;
}


/**
 * @brief  Sends the COMMAND_MEMORY_READ message to the proxy bootloader
 *         to read data from proxy flash memory.
 *
 * This function will only read at most MAX_READ_PAYLOAD_SIZE bytes,
 * which is the maximum amount that can be read in a single SPI transaction
 * transfer.  The number of bytes received is returned from this function
 * so that more data can be read later on with another call to this function.
 *
 * @param address  Starting address of flash memory to read from
 * @param size     The number of bytes to read (space available in the buffer)
 * @param buf      Buffer to place data from flash memory into
 *
 * @return  The number of bytes placed in the buffer
 *
 */
static uint32_t BlReadDataPacket(uint32_t address, uint32_t size, uint8_t * buf)
{
    uint8_t   i;
    uint8_t   computedRxChecksum = 0;
    bool      ackSuccess;
    uint8_t   rxMsgSize = 0;
    PrxFwMemReadCmd_t * readCmdData = (PrxFwMemReadCmd_t*)gPrxFwSpiBuf;
    HAL_StatusTypeDef spiStatus;

    // Update data packet fields
    size = (size > MAX_READ_PAYLOAD_SIZE) ? MAX_READ_PAYLOAD_SIZE : size;
    readCmdData->packetSize = CMD_PACKET_SIZE_READ;
    readCmdData->command = CMD_MEMORY_READ;
    readCmdData->address = __REV(address);            // switch endianness
    readCmdData->accessType = READ_ACCESS_TYPE_8_BIT;
    readCmdData->numValues = size;

    // Compute the checksum
    BlApplyPacketChecksum(gPrxFwSpiBuf);

    // Assert chip select line
    CS_ASSERT();

    for (i = 0; i < MAX_TRIES; i++)
    {
        // Write the COMMAND_MEMORY_READ command
        HAL_SPI_Transmit(&PROXY_SPI_HANDLE, gPrxFwSpiBuf, CMD_PACKET_SIZE_READ, XFER_TIMEOUT_MS);
        //PostPrintf("Read %u bytes @ 0x%05X\r\n", size, address);

        ackSuccess = BlWaitForAck();

        if (ackSuccess)
        {
            // Attempt to read the message size
            for (i = 0; i < MAX_TRIES; i++)
            {
                spiStatus = HAL_SPI_Receive(&PROXY_SPI_HANDLE, &rxMsgSize, sizeof(rxMsgSize), XFER_TIMEOUT_MS);
                if (spiStatus != HAL_OK)
                {
                    // Deassert chip select line
                    CS_DEASSERT();
                    PostPrintf("Error reading message size.\r\n");
                    return 0;
                }
                
                // Stop trying to read message size when a non-zero value is received
                if (rxMsgSize)
                {
                    break;
                }
            }
            
            if (rxMsgSize - RX_PACKET_HEADER_SIZE == size)
            {
                break;
            }
            else
            {
                PostPrintf("ERROR: Requested payload size is 0x%02X and received payload size is 0x%02X.\r\n", size, rxMsgSize - RX_PACKET_HEADER_SIZE);
            }
        }
        vTaskDelay(2);
    }

    if ((!ackSuccess) || (rxMsgSize - RX_PACKET_HEADER_SIZE != size))
    {
        // Deassert chip select line
        CS_DEASSERT();
        PostPrintf("Error waiting for an ACK.\r\n");
        return 0;
    }

    if (rxMsgSize <= RX_PACKET_HEADER_SIZE)
    {
        // Deassert chip select line
        CS_DEASSERT();
        PostPrintf("Message size is %d, and therefore has no payload.\r\n", rxMsgSize);
        return 0;
    }

    // Read the remaining (rxMsgSize - 1) bytes (-1 for the message size byte)
    spiStatus = HAL_SPI_Receive(&PROXY_SPI_HANDLE, gPrxFwSpiBuf, rxMsgSize - 1, XFER_TIMEOUT_MS);

    // Compute the checksum for all the bytes following the checksum byte at index 0
    for (i = 1; i < rxMsgSize - 1; i++)
        computedRxChecksum += gPrxFwSpiBuf[i];

    // Check if the computed checksum matches the received checksum
    if (computedRxChecksum != gPrxFwSpiBuf[0])
    {
        // Deassert chip select line
        CS_DEASSERT();
        PostPrintf("Received checksum 0x%02X doesn't match the computed checksum 0x%02X.\r\n", gPrxFwSpiBuf[0], computedRxChecksum);
        return 0;
    }

    // Copy all the bytes to the caller excluding the checksum byte, which is the first byte in the buffer
    memcpy(buf, &gPrxFwSpiBuf[1], rxMsgSize - RX_PACKET_HEADER_SIZE);

    // Send an ACK to the bootloader
    gPrxFwSpiBuf[0] = ACK_BYTE_VALUE;
    HAL_SPI_Transmit(&PROXY_SPI_HANDLE, gPrxFwSpiBuf, ACK_DATA_SIZE, XFER_TIMEOUT_MS);

    // Deassert chip select line
    CS_DEASSERT();

    // Return the number of payload bytes returned to the caller
    return rxMsgSize - RX_PACKET_HEADER_SIZE;
}


/**
 * @brief  This function erases proxy flash memory in the given address range
 *
 * The memory will be erased in increments of 1 flash memory page, which is 4KB
 *
 * @param address  Starting address of flash memory to erase
 * @param size     The number of bytes to erase
 *
 * @return  true if successful; false otherwise
 *
 */
bool ProxyFwUpdateErase(uint32_t address, uint32_t size)
{
    int32_t  success = true;
    uint32_t erasedBytes;

    
    if (!gProxyBootloaderInitialized)
    {
        return false;
    }
    
    erasedBytes = BlEraseFlashMemory(address, size);
    if (erasedBytes != size)
    {
        PostPrintf("Error erasing page at address 0x%05X.\r\n", address - (address % PROXY_FLASH_PAGE_SIZE) + erasedBytes);
        PostPrintf("Erase range: 0x%05X; bytes erased: 0x%05X.\r\n", size, erasedBytes);
        success = false;
    }

    return success;
}


/**
 * @brief  This function waits for the first non-zero resposne
 *         from the proxy bootloader and returns true if it's an ACK
 *
 * This function can be used after any proxy bootloader command to
 * get an acknowledgement of a command received by the bootloader.
 * 
 * @return  true if an ACK is received within MAX_BYTES_TO_ACK bytes;
 *          false otherwise.
 *
 */
static bool BlWaitForAck(void)
{
    HAL_StatusTypeDef spiStatus;
    uint32_t numReads = 0;
    uint8_t  ackDataBuf[ACK_DATA_SIZE] = {0};

    // Read the response
    do
    {
        spiStatus = HAL_SPI_Receive(&PROXY_SPI_HANDLE, ackDataBuf, ACK_DATA_SIZE, XFER_TIMEOUT_MS);
        numReads++;
    } while ((numReads < MAX_BYTES_TO_ACK) && (ackDataBuf[0] == 0) && (spiStatus == HAL_OK));  // Non-zero value is our response

    //PrintAckStatus(ackDataBuf[0]);

    return (ackDataBuf[0] == ACK_BYTE_VALUE);
}


#ifdef  PRINT_DEBUG_OUTPUT_FOR_PROXY_FW_UPDATE
/**
 * @brief  Prints the type of ACK received after a
 *         command execution by the proxy bootloader.
 *
 * This function can be used to print the ACK/NACK byte
 * value returned after executing any bootloader command.
 * 
 * @param  status  The value read from the proxy bootloader after any command execution
 *
 */
static void PrintAckStatus(int32_t ackByte)
{
    switch (ackByte)
    {
        case ACK_BYTE_VALUE:
            PostPrintf("ACK received\r\n");
            break;
        case NACK_BYTE_VALUE:
            PostPrintf("NACK received\r\n");
            break;
        default:
            PostPrintf("Unknown ACK value\r\n");
            break;
    }
}
#endif  // #ifdef  PRINT_DEBUG_OUTPUT_FOR_PROXY_FW_UPDATE


/**
 * @brief  Sends COMMAND_GET_STATUS command to the proxy bootloader
 *         to get the status of the last executed command.
 *
 * @return true if the last executed command was executed successfully; false otherwise
 *
 */
static bool BlGetStatus(void)
{
    bool ackSuccess = false;
    bool statusSuccess = false;
    HAL_StatusTypeDef spiStatus;
    uint8_t cmdPacketBuf[sizeof(PrxFwGetStatusCmd_t)];
    PrxFwGetStatusCmd_t * statusCmdData = (PrxFwGetStatusCmd_t*)cmdPacketBuf;
    PrxFwGetStatusRsp_t * statusRspData = (PrxFwGetStatusRsp_t*)cmdPacketBuf;
    
    
    // Set checksum to command byte since that's the only payload byte
    // used to calculate the checksum
    statusCmdData->packetSize = CMD_PACKET_SIZE_STATUS;
    statusCmdData->checksum   = CMD_GET_STATUS;
    statusCmdData->command    = CMD_GET_STATUS;

    for (int i = 0; i < 10; i++)
    {
        // Write the COMMAND_GET_STATUS command
        HAL_SPI_Transmit(&PROXY_SPI_HANDLE, cmdPacketBuf, CMD_PACKET_SIZE_STATUS, XFER_TIMEOUT_MS);

        ackSuccess = BlWaitForAck();
        if (ackSuccess)
            break;
    }

    if (ackSuccess)
    {
        // We'll reuse cmdPacketBuf buffer for received data
        memset(statusCmdData, 0, sizeof(statusCmdData));
        spiStatus = HAL_SPI_Receive(&PROXY_SPI_HANDLE, cmdPacketBuf, sizeof(statusRspData), XFER_TIMEOUT_MS);
        if (spiStatus != HAL_OK)
        {
            PostPrintf("Failed to read SPI bus.\r\n");
        }
        else
        {
            if ((statusRspData->packetSize == CMD_PACKET_SIZE_STATUS_RESP) && (statusRspData->checksum == statusRspData->status))
            {
                // We're here because we received a valid number of bytes and a valid checksum
                statusSuccess = (statusRspData->status == CMD_STATUS_SUCCESS);
                if (!statusSuccess)
                {
                    PostPrintf("ERROR: Command status returned: 0x%02X.\r\n", statusRspData->status);
                }
            }

            // ACK received data
            cmdPacketBuf[0] = ACK_BYTE_VALUE;
            HAL_SPI_Transmit(&PROXY_SPI_HANDLE, cmdPacketBuf, ACK_DATA_SIZE, XFER_TIMEOUT_MS);
        }

    }
    else
    {
        PostPrintf("ERROR: ACK not received after sending GET_STATUS command.\r\n");
    }

    return (ackSuccess && statusSuccess);
}


/**
 * @brief  Sends COMMAND_SECTOR_ERASE commands to the proxy bootloader
 *         to erase flash memory pages in a given address range.
 *
 * This command erases flash memory pages in the memory range specified
 * by the parameters.  Each flash memory page is 4KB.
 *
 * @param address  Starting address to program flash memory at
 * @param size     The number of bytes to be written
 *
 */
static uint32_t BlEraseFlashMemory(uint32_t address, uint32_t size)
{
    uint32_t erasedBytes = 0;
    uint32_t eraseAddress;
    bool     success;
    PrxFwEraseCmd_t * eraseCmdData = (PrxFwEraseCmd_t*) gPrxFwSpiBuf;


    // Update data packet fields
    eraseCmdData->packetSize = CMD_PACKET_SIZE_ERASE;
    eraseCmdData->command    = CMD_SECTOR_ERASE;

    // Assert chip select line
    CS_ASSERT();

    // Erase all the flash memory pages affected by the new data before writing
    for (eraseAddress = address - (address % PROXY_FLASH_PAGE_SIZE); eraseAddress < address + size; eraseAddress += PROXY_FLASH_PAGE_SIZE)
    {
        // Update the data packet
        eraseCmdData->address = __REV(eraseAddress);

        // Compute the data packet checksum
        BlApplyPacketChecksum(gPrxFwSpiBuf);

        for (uint8_t i = 0; i < MAX_TRIES; i++)
        {
            // Write the COMMAND_SECTOR_ERASE command
            HAL_SPI_Transmit(&PROXY_SPI_HANDLE, gPrxFwSpiBuf, CMD_PACKET_SIZE_ERASE, XFER_TIMEOUT_MS);
            //PostPrintf("Erasing flash memory sector @ 0x%05X\r\n", eraseAddress);

            success = BlWaitForAck();
            if (success)
            {
                success &= BlGetStatus();
                if (success)
                {
                    break;
                }
            }
        }

        if (!success)
        {
            PostPrintf("Error waiting for an ACK and successful command completion status.\r\n");
            break;
        }

        // Count another flash memory page as erased
        erasedBytes += PROXY_FLASH_PAGE_SIZE;
    }

    // Deassert chip select line
    CS_DEASSERT();

    return erasedBytes;
}


/**
 * @brief  Sends COMMAND_DOWNLOAD command to initiate programming of
 *         proxy flash memory by specifying the starting address and
 *         the size of memory to be programmed.
 *
 * This command doesn't erase flash memory, so it needs to be erased
 * first by sending COMMAND_SECTOR_ERASE command via the
 * BlEraseFlashMemory() function call for each sector occupied by
 * the data to be written to the flash memory.
 *
 * @param address  Starting address to program flash memory at
 * @param size     The number of bytes to be written
 *
 */
static bool BlDownloadCmd(uint32_t address, uint32_t size)
{
    uint8_t success;
    PrxFwDownloadCmd_t * downloadCmdData = (PrxFwDownloadCmd_t*)gPrxFwSpiBuf;

    // Validate address and size
    if (address + size > WRITABLE_PROXY_FLASH_SIZE)
    {
        return false;
    }

    // Set command value and packet size
    downloadCmdData->command = CMD_DOWNLOAD;
    downloadCmdData->packetSize = CMD_PACKET_SIZE_DOWNLOAD;

    // Set the address and size flipping the endianness
    downloadCmdData->address = __REV(address);
    downloadCmdData->dataSize = __REV(size);

    // Compute the checksum
    BlApplyPacketChecksum(gPrxFwSpiBuf);

    // Assert chip select line
    CS_ASSERT();


    for (uint8_t i = 0; i < MAX_TRIES; i++)
    {
        // Write the COMMAND_DOWNLOAD data packet
        HAL_SPI_Transmit(&PROXY_SPI_HANDLE, gPrxFwSpiBuf, CMD_PACKET_SIZE_DOWNLOAD, XFER_TIMEOUT_MS);
        //PostPrintf("Download command to program %u bytes @ 0x%05X.\r\n", size, address);

        success = BlWaitForAck();
        if (success)
        {
            success &= BlGetStatus();
            if (success)
            {
                break;
            }
        }
    }

    // Deassert chip select line
    CS_DEASSERT();

    return success;
}


/**
 * @brief  Sends COMMAND_SEND_DATA command to proxy bootloader, containing
 *         data to be programmed at the next location in proxy flash memory.
 *
 * Before sending this command, the COMMAND_DOWNLOAD command needs to
 * be sent by calling the BlDownloadCmd() function. This command is
 * sent multiple times until all the payload data has been transmitted.
 *
 * @param buf   Buffer containing the paylod data to program
 * @param size  Size of payload data
 *
 */
static uint32_t BlSendDataCmd(uint8_t * buf, uint32_t size)
{
    uint8_t payloadSize;
    int32_t bytesWritten = 0;
    uint8_t success = false;
    PrxFwSendDataCmd_t * sendDataCmdPacket = (PrxFwSendDataCmd_t*)gPrxFwSpiBuf;

    // Check if there's data to write
    if ((size == 0) || (buf ==  NULL))
    {
        PostPrintf("No data to write to proxy flash memory.\r\n");
        return 0;
    }


    // Fill in the packet length to be transmitted over SPI
    payloadSize = (size > MAX_WRITE_PAYLOAD_SIZE) ? MAX_WRITE_PAYLOAD_SIZE : size;
    sendDataCmdPacket->packetSize = CMD_PACKET_HEADER_SIZE + payloadSize;

    // Fill in the command ID
    sendDataCmdPacket->command = CMD_SEND_DATA;

    // Copy the data to the SPI transfer buffer
    memcpy(gPrxFwSpiBuf + CMD_PACKET_HEADER_SIZE, buf, payloadSize);

    // Compute the checksum
    BlApplyPacketChecksum(gPrxFwSpiBuf);

    // Assert chip select line
    CS_ASSERT();


    for (uint8_t i = 0; i < MAX_TRIES; i++)
    {
        // Write the COMMAND_SEND_DATA command packet data
        HAL_SPI_Transmit(&PROXY_SPI_HANDLE, gPrxFwSpiBuf, sendDataCmdPacket->packetSize, XFER_TIMEOUT_MS);

        success = BlWaitForAck();
        if (success)
        {
            success &= BlGetStatus();
            if (success)
            {
                break;
            }
        }
    }

    // Deassert chip select line
    CS_DEASSERT();

    bytesWritten = success ? payloadSize : 0;

    return bytesWritten;
}


/**
 * @brief  Computes and applies the checksum for the given data packet
 *         to be transmitted
 *
 * The checksum is computed for the bytes following the second byte
 * in the data packet.  The first byte is assumed to be the data
 * packet size byte.  The second byte in the packet is the checksum.
 *
 * @param buf  Buffer containing the data packet
 *
 */
static void BlApplyPacketChecksum(uint8_t * buf)
{
    buf[1] = 0;

    for (uint8_t i = 2; i < buf[0]; i++)
        buf[1] += buf[i];
}
