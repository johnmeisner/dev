/****************************************************************************//**
* @file      power_level.h
*
* @brief     Header file containing prototypes to set the power level of the transmitter
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#ifndef _POWER_LEVEL_H_
#define _POWER_LEVEL_H_
#include "error.h"
#include "amb_control.h"

typedef enum _PowerRegister_t
{
    AMU_POWER_13_DBM     = 0, ///< The AMUiPAPOWERMODE register value for 13dBm
    AMU_POWER_16_DBM     = 1, ///< The AMUiPAPOWERMODE register value for 16dBm
    AMU_POWER_19_DBM     = 2, ///< The AMUiPAPOWERMODE register value for 19dBm
    AMU_POWER_19_5_DBM   = 3, ///< The AMUiPAPOWERMODE register value for 19.5dBm
    AMU_POWER_HIGH       = 4  ///< Indicates that 20dBm has been requested. Set the AMU1PAHIGHPOWER register to 1
} PowerRegister_t;


CotaError_t SetUvpPowerLevels(PowerRegister_t pow, AmbMask_t ambMask, UvpMask_t uvpMask);
CotaError_t SetAmbPowerLevels(AmbMask_t ambMask, uint16_t requestedPowerLevel, uint16_t* actualPowerLevel);
CotaError_t GetUvpPowerLevels(PowerRegister_t* pow, AmbNum_t ambNum, UvpNum_t uvpNum);
#endif // #ifndef _POWER_LEVEL_H_

