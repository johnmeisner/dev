/****************************************************************************//**
 * @file       eeprom_driver.h
 *
 * @brief      Header file for the EEPROM chip device driver
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _EEPROM_DRIVER_H_
#define _EEPROM_DRIVER_H_


#include "error.h"
#include "stm32h7xx_hal.h"


#define EEPROM_MEM_ADDRESS_SIZE     18                                 ///< EEPROM memory address size in bits
#define EEPROM_MEM_SIZE             (1 << (EEPROM_MEM_ADDRESS_SIZE))   ///< EEPROM memory size in bytes
#define EEPROM_PAGE_SIZE            0x100                              ///< EEPROM page size in bytes - one page is 256 bytes
#define EERPOM_ERASED_BYTE          0xFF                               ///< EERPROM byte value with the byte is erase or has not been written to

CotaError_t EepromInit(void);
CotaError_t EepromRead(uint32_t address, uint8_t* pData, uint32_t size);
CotaError_t EepromWrite(uint32_t address, uint8_t* pData, uint32_t size);
CotaError_t EepromReadConfigPage(uint8_t address, uint8_t* pData, uint16_t size);
CotaError_t EepromWriteConfigPage(uint8_t address, uint8_t* pData, uint16_t size);
void Eeprom_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c);
void Eeprom_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c);
#endif // #ifndef _EEPROM_DRIVER_H_
