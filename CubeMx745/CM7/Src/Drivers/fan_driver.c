/****************************************************************************//**
* @file      fan_driver.c
*
* @brief     The fan driver is meant to expose access to the MAX31790 which
*            controls four fans, one for each AMB.
*
*            The fan driver has a task that does most of the work of monitoring
*            the fans and adjusting the speed for higher temperature AMB's
*            but the control task must periodically call the functoin 
*            AmbMask_t FansOk(void).  If the returned mask
*            has a zero bit for a valid AMB, it must stop charging that AMB.
*
*            @todo Need to map fan numbers to amb numbers
*            @todo Need to map average uvp temperatures to fan speeds
* 
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019-2020 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/


#include "FreeRTOS.h"
#include "main.h"
#include "semphr.h"
#include "orion_config.h"
#include "i2c_multiplex.h"
#include "ccb_temperature.h"
#include "amb_control.h"
#include "fan_driver.h"
#include "uvp_driver.h"
#include "task.h"
#include "debug_off.h"
#include "power_monitor.h"
#include "cli_task.h"

#define FAN_I2C_ADDRESS         0x20                ///< The 7 bit fan i2c slave address.
#define FAN_MULTIPLEX_NUM       0                   ///< The number of i2c multiplexer that the fan chip is connected to
#define FAN_MULTIPLEX_CHAN      0                   ///< The channel on the multiplexer that the fan chip is connected to.
#define FAN_READ_WRITE_TIMEOUT  10                  ///< The maximum allowed time to wait for a FAN read or write i2c transaction.
#define FAN_REG_SET_BIT         1                   ///< For a bit value, set to 1
#define FAN_REG_RESET_BIT       0                   ///< For a bit value, set to 0
#define FAN_MAX_NUM             4                   ///< The number of fans
#define FAN_MAX_PWM             511                 ///< The maximum target duty cycle.
#define FAN_LOW_TARGET_PWM      (FAN_MAX_PWM/5+1)   ///< The lowest allowed setting is a 20% duty cycle.
#define FAN_MAX_RPM             2200                ///< The maximum RPM of the fan
#define FAN_SECS_IN_MIN         60                  ///< Seconds in a minute
#define FAN_TACH_CLOCK_HZ       8192                ///< The clock frequency used to create a tachometer count between tachometer ticks.
#define FAN_MIN_RPM_PERCENT     20                  ///< Tne minimum operational RPM percentage for the FAN.
#define FAN_FULL_SPEED_ON       GPIO_PIN_RESET      ///< State of the gpio to enable full fans on
#define FAN_FULL_SPEED_OFF      GPIO_PIN_SET        ///< State of the gpio to disable full fans on
#define FAN_CRITICAL_TEMP_SPEED 0xff                ///< An imaginary speed that maps to the critical temperature.

/** 
 * The maximum allowed tachometer count before a failure is generated.
 * It's calculated the following way using the 20% of the the maximum 2200 rpm for the fan
 * (fan rots per second) = (20/100)*(2200rpm)/60 = 7.33 rots/sec
 * (tachs per second) = (fan rots per second) * 2 = 14.666 tachs/sec
 * (max count) = 8192 Hz * (2 tach meas periods)/ 14.6666 = 1117
 */
#define FAN_MAX_TACH_COUNT      1117        
#define FAN_TACH_FAIL_COUNT     (FAN_MAX_TACH_COUNT + FAN_MAX_TACH_COUNT/20)  //If we are trying to use FAN_MAX_TACH_COUNT to detect failures, let's add five percent

#define  FAN_GEN_CONFIG_ADDRESS           0x0                                               ///< Address of the general configuration register
#define  GC_WATCHDOG_STATUS               (FanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  0}      ///< Used to enable watchdog status
#define  GC_WATCHDOG_PERIOD               (FanReg_t){ FAN_GEN_CONFIG_ADDRESS,   2,  1}      ///< Sets the watchdog period
#define  GC_OSC_SELECTION                 (FanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  3}      ///< Sets the use of an internal or external oscilator
#define  GC_I2C_BUS_TIMEOUT_DIS           (FanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  5}      ///< Enables a i2c bus timeout
#define  GC_RESET_REGISTERS               (FanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  6}      ///< Resets the registers
#define  GC_STANDY_ENABLE                 (FanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  7}      ///< Enables standby

#define  FAN_PWM_FREQUENCY_ADDRESS        0x1                                               ///< Address of PWM frequency address register
#define  FAN_PWM_FREQ_25KHZ               0xB                                               ///< Value to set the PWM frequency to 25 kHz
#define  PWM_FREQUENCY_1_THRU_3           (FanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   4,  0}   ///< PWM frequencies for fan's 1-3
#define  PWM_FREQUENCY_4_THRU_6           (FanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   4,  4}   ///< PWM frequencies for fan's 4-6


//The following register is repeated 6 times, one for each possible fan
#define  FAN_CONFIG_ADDRESSS              0x2                                               ///< Fan configuration address  (first of 6 consecutive registers)
#define  FAN_CONF_PWM_TACH_IN_EN          (FanReg_t){ FAN_CONFIG_ADDRESSS,   1,  0}         ///< Bit changes the PWM pin for fan to a tach input.
#define  FAN_CONF_LOCKED_ROTOR_POL        (FanReg_t){ FAN_CONFIG_ADDRESSS,   1,  1}         ///< Bit sets the expected polarity of the tach when the fan is locked and can't rotate
#define  FAN_CONF_TACH_IN_LOCK_ROT_EN     (FanReg_t){ FAN_CONFIG_ADDRESSS,   1,  2}         ///< Bit puts the tach in locked rotor mode
#define  FAN_CONF_TACH_INPUT_EN           (FanReg_t){ FAN_CONFIG_ADDRESSS,   1,  3}         ///< Enables the tach input
#define  FAN_CONF_MONITOR_ONLY_EN         (FanReg_t){ FAN_CONFIG_ADDRESSS,   1,  4}         ///< Puts the fan in monitor only mode
#define  FAN_CONF_SPINUP_TIME             (FanReg_t){ FAN_CONFIG_ADDRESSS,   2,  5}         ///< Options for fan spin up
#define  FAN_CONF_RPM_MODE_EN             (FanReg_t){ FAN_CONFIG_ADDRESSS,   1,  7}         ///< Selects RPM mode

//The following register is repeated 6 times, one for each possible fan
#define  FAN_DYNAMICS_ADDRESSS            0x8                                               ///< The fan dynamics register  (first of 6 consecutive registers)
#define  FAN_DYN_ASYM_RATE_OF_CHG_EN      (FanReg_t){ FAN_DYNAMICS_ADDRESSS,   1,  1}       ///< Enables asymetric speed changes
#define  FAN_DYN_PWM_RATE_OF_CHANGE       (FanReg_t){ FAN_DYNAMICS_ADDRESSS,   3,  2}       ///< Sets the rate of change for fan speed changes
#define  FAN_DYN_SPEED_RANGE              (FanReg_t){ FAN_DYNAMICS_ADDRESSS,   3,  5}       ///< Determines the speed range of the tach

#define  FAN_FAULT_ADDRESS                0x11                                              ///< The FAN fault register
#define  FAN_FAULT_MASK(num)              (1 << num)                                        ///< Generates the bit returned from #checkFanFailures that would indicate this fan has failed.

#define  FAN_FAILED_ADDRESS               0x14                                              ///< The fan failure options address
#define  FAN_FAILED_QUEUE                 (FanReg_t){ FAN_FAILED_ADDRESS,  2,  0}           ///< The number of failures to detect before setting a fan fail
#define  FAN_FAILED_OPTIONS               (FanReg_t){ FAN_FAILED_ADDRESS,  2,  2}           ///< Options on what to do during a fan faulure
#define  FAN_FAILED_START_DELAY           (FanReg_t){ FAN_FAILED_ADDRESS,  2,  5}           ///< Sets a delay between sequential fan starts


//The folowing pair of registers is repeated 12 times because we can have up to 12 tachometers
#define  FAN_TACH_COUNT_HIGH_ADDRESS       0x18                                             ///< Register contains the upper 8 bits of the tach count (first pair of 6 consecutive register pairs)
#define  FAN_TACH_COUNT_LOW_ADDRESS        0x19                                             ///< Register contains the lower 3 bits of the tach count

//The following pair of registers is repeated 6 times, one for each possible fan
#define  FAN_PWM_DUTY_CYCLE_HIGH_ADDRESS  0x30                                              ///< Register contains the upper 8 bits of the pwm duty cycle (first pair of 6 consecutive register pairs)
#define  FAN_PWM_DUTY_CYCLE_LOW_ADDRESS   0x31                                              ///< Register contains the lower 1 bit of the pwm duty cycle 

//The following pair of registers is repeated 6 times, one for each possible fan
#define  FAN_TARGET_DUTY_CYCLE_HIGH_ADDR  0x40                                              ///< Register contains the upper 8 bits of the target duty cycle (first pair of 6 consecutive register pairs)
#define  FAN_TARGET_DUTY_CYCLE_LOW_ADDR   0x41                                              ///< Register contains the lower 1 bit of the target duty cycle 
#define  FAN_TARGET_DUTY_CYCLE_HIGH(val)  ((((val) << 7)  & 0xff00) >> 8)                   ///< Gets the high bits for the target duty cycle
#define  FAN_TARGET_DUTY_CYCLE_LOW(val)   (((val) << 7)  & 0xff)                            ///< Gets the low bit for the target dut cycle

//The following pair of registers is repeated 6 times, one for each possible fan
#define  FAN_TARGET_COUNT_HIGH_ADDRESS    0x50                                              ///< Register contains the upper 8 bits of the target count (first pair of 6 consecutive register pairs)
#define  FAN_TARGET_COUNT_LOW_ADDRESS     0x51                                              ///< Register contains the lower 3 bits of the target count
#define  FAN_TARGET_COUNT_HIGH(val)       ((((val) << 5)  & 0xff00) >> 8)                   ///< Gets the high bits for the target count
#define  FAN_TARGET_COUNT_LOW(val)        (((val) << 5)  & 0xff)                            ///< Gets the low bits for the target count
#define  FAN_MAX_SPEED                    100                                               ///< 100 percent is maximum fan speed
/**
 * @brief Contains all the information necessary to update a fan register.
 */
typedef struct _FanReg_t
{
    uint8_t addr;           ///< The address of the register
    uint8_t size;           ///< The size of the field in the register to update in bits
    uint8_t offset;         ///< The offset of the field in the register to update in bit
} FanReg_t;

/**
 * @brief enum defines the allowed watchdog periods.
 */ 
typedef enum _FanWatchDogPeriods_t
{
    NO_WATCHDOG         = 0,   ///< No watchdog is enabled 
    WATCHDOG_5_SECONDS  = 1,   ///< An i2c message must be received within 5 seconds or the fans will turn on full
    WATCHDOG_10_SECONDS = 2,   ///< An i2c message must be received within 10 seconds or the fans will turn on full
    WATCHDOG_30_SECONDS = 3,   ///< An i2c message must be received within 30 seconds or the fans will turn on full
} FanWatchDogPeriods_t;
   
/**
 * @brief The spead range is the number tachometer pulses where the 8192Hz is counted.
 */
typedef enum _FanSpeedRange_t
{
    COUNT_BETWEEN_1_TACH_PERIOD     = 0,   ///< Count 8192 Hz ticks over 1 tach period
    COUNT_BETWEEN_2_TACH_PERIODS    = 1,   ///< Count 8192 Hz ticks over 2 tach periods
    COUNT_BETWEEN_4_TACH_PERIODS    = 2,   ///< Count 8192 Hz ticks over 4 tach periods
    COUNT_BETWEEN_8_TACH_PERIODS    = 3,   ///< Count 8192 Hz ticks over 8 tach periods
    COUNT_BETWEEN_16_TACH_PERIODS   = 4,   ///< Count 8192 Hz ticks over 16 tach periods
    COUNT_BETWEEN_32_TACH_PERIODS   = 5,   ///< Count 8192 Hz ticks over 32 tach periods
} FanSpeedRange_t;

/**
 * @brief Transition timing settings
 */
typedef enum _FanPwrRateOfChange_t
{
    PWM_RATE_OF_CHANGE_0_33   = 0,  ///< Cause changes from 33% to 100% Max RPM in 0.33s
    PWM_RATE_OF_CHANGE_0_67   = 1,  ///< Cause changes from 33% to 100% Max RPM in 0.67s
    PWM_RATE_OF_CHANGE_1_34   = 2,  ///< Cause changes from 33% to 100% Max RPM in 1.34s
    PWM_RATE_OF_CHANGE_2_70   = 3,  ///< Cause changes from 33% to 100% Max RPM in 2.70s 
    PWM_RATE_OF_CHANGE_5_30   = 4,  ///< Cause changes from 33% to 100% Max RPM in 5.30s
    PWM_RATE_OF_CHANGE_10_7   = 5,  ///< Cause changes from 33% to 100% Max RPM in 10.7s
    PWM_RATE_OF_CHANGE_21_7   = 6,  ///< Cause changes from 33% to 100% Max RPM in 21.7s
    PWM_RATE_OF_CHANGE_42_8   = 7   ///< Cause changes from 33% to 100% Max RPM in 42.8s
} FanPwrRateOfChange_t;

/**
 * @brief FAN sequential start up delay
 */
typedef enum _FanSeqStartDelay_t
{
    SEQ_START_DELAY_0_s     = 0,    ///< Sets sequential startup delay to 0 seconds
    SEQ_START_DELAY_250_ms  = 1,    ///< Sets sequential startup delay to 250 milliseconds    
    SEQ_START_DELAY_500_ms  = 2,    ///< Sets sequential startup delay to 500 milliseconds (default)
    SEQ_START_DELAY_1_s     = 3,    ///< Sets sequential startup delay to 1 seconds
    SEQ_START_DELAY_2_s     = 4,    ///< Sets sequential startup delay to 2 seconds
    SEQ_START_DELAY_4a_s    = 5,    ///< Sets sequential startup delay to 4 seconds
} FanSeqStartDelay_t;

/**
 * @brief Sets the number of failures until a fault is thrown
 */
typedef enum _FanFailQueueCount_t
{
    FAN_FAIL_FAULT_1    = 0,    ///< Throw a fault after 1 detections
    FAN_FAIL_FAULT_2    = 1,    ///< Throw a fault after 2 detections
    FAN_FAIL_FAULT_4    = 2,    ///< Throw a fault after 4 detections
    FAN_FAIL_FAULT_6    = 3,    ///< Throw a fault after 6 detections
} FanFailQueueCount_t;
 
/**
 * @brief Sets the behavior when a fan fails;
 */
typedef enum _FanFailBehavior_t
{
    FAN_FAIL_SET_TO_0           = 0,    ///< Set fan speed to 0%
    FAN_FAIL_CONTINUE_OPERATION = 1,    ///< Continue normal PWM operation (default)
    FAN_FAIL_SET_TO_100         = 2,    ///< Set fan speed to 100%
    FAN_FAIL_SET_FAN_FULL       = 3,    ///< Set all fans to 100%
} FanFailBehavior_t;


/**
 * @brief Spin up time settings
 */
typedef enum _FanSpinUp_t 
{
    SPIN_UP_OFF  = 0,    ///< Disable spin up
    SPIN_UP_0_5  = 1,    ///< Max spin up time is 0.5s
    SPIN_UP_1_0  = 2,    ///< Max spin up time is 1.0s
    SPIN_UP_2_0  = 3     ///< Max spin up time is 2.0s
} FanSpinUp_t;

/**
 * @brief A structure used to map UVP temperature to fan speed.
 */    
typedef struct _FanMapTempToSpeed_t
{
    float temp;             ///< The average UVP temperature for an AMB
    uint8_t speed;          ///< The speed (in percent) to set the fans to for temps exceeding the temperature in this structure (If set to 0xff, temp represent a critical temperature)
} FanMapTempToSpeed_t;

typedef uint8_t  FanMask_t;
typedef uint8_t  FanNum_t;

static I2CMultiplexerInfo_t gCcbFanInfo;                    ///< The i2c multiplexer structure for the fan driver chip
static FanMask_t gFansOkMask;                               ///< A mask indication which fans are ok
static FanMask_t gValidFans;                                ///< A mask indicating which fans are valid
static uint16_t gCritTemp;                                  ///< Temperatures over this value should cause fans to go high (and tps to stop)
static volatile bool gFullFans;                             ///< If true, fans speed are always set at full.
#define ENABLE_UVP_TEMP_MONITOR 1
#define ENABLE_CCB_TEMP_MONITOR 1
#ifdef ENABLE_UVP_TEMP_MONITOR   
/**
 * @brief This array maps fan speed to UVP temperature
 * 
 * @todo  This structure needs to be refined experimentally with the UVP's and AMB's
 */
static FanMapTempToSpeed_t gMapTempToSpeed[] = {
    {-273.0,                            20 },
    {  20.0,                            40},
    {  30.0,                            80},
    {  50.0,                            FAN_MAX_SPEED},
    { 500.0,                            FAN_CRITICAL_TEMP_SPEED}   //500 will be swapped out on initialization with the critical temperature.
};

const uint8_t gTempSpeedMapSize = sizeof(gMapTempToSpeed)/sizeof(gMapTempToSpeed[0]);  ///< Number of entries in the temperature speed map
#endif

/**
 * @brief Retrieves a mask indicating which AMB's fans are ok.  If 
 *        an AMB's fan is not ok, charging should not be done with that AMB.
 *        The mask will be updated by the driver every 25 seconds.
 *
 * @return true if the fans are ok, false otherwise
 */
bool FansOk(void)
{
    return ((gFansOkMask & gValidFans) == gValidFans);
}

/**
 *  @brief Used to create a field in a register that can be OR'd in
 *  
 *  @param fanReg Contains the information about the register and its field
 *  @param val    The value to place in the field
 * 
 *  @return The value stuck in the appropiate field of the register, ready to be OR'd
 */
static uint8_t createFanRegField(FanReg_t fanReg, uint8_t val)
{
    uint8_t mask = GEN_SOLID_MASK(fanReg.size);

    return (mask & val) << fanReg.offset;
}

/**
 * @brief  Writes an 8 bit register to the fan driver
 *         To write a register, we simply write the register addresss and follow it with the value.
 * @param  regAddr  The register address to write to
 * @param  val      The value to write to the register
 * 
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
static CotaError_t writeFanRegister(uint8_t regAddr, uint8_t val)
{
    uint8_t sendData[2];
    sendData[0] = regAddr;
    sendData[1] = val;
    
    return I2CMultiplexWrite(&gCcbFanInfo, sendData, sizeof(sendData), FAN_READ_WRITE_TIMEOUT);  
}

/**
 * @brief  Read an 8 bit register to the fan driver
 *         To read a register, we first write the register address,
 *         then follow it with a read.

 * @param  regAddr  The register address to read from
 * @param  val      Receives value read from the register
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
static CotaError_t readFanRegister(uint8_t regAddr, uint8_t* val)
{
    CotaError_t ret;
    
    ret = I2CMultiplexWrite(&gCcbFanInfo, &regAddr, sizeof(regAddr), FAN_READ_WRITE_TIMEOUT);
    
    ret = (ret == COTA_ERROR_NONE) ? I2CMultiplexRead(&gCcbFanInfo, val, sizeof(uint8_t), FAN_READ_WRITE_TIMEOUT) : ret; 
    
    return ret;
}

/**
 * @brief The target count register controls the fan in RPM mode, or 
 *        detects faults in PWM mode.  This function will set it for a fan
 *
 * @param fanNum      The number of the fan to set this for. Select 0-3
 * @param targetCount The 11 bit target count value 
 * 
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
static CotaError_t setFanTachTargetCount(uint8_t fanNum, uint16_t targetCount)
{
    CotaError_t ret = COTA_ERROR_NONE;

    ret = writeFanRegister(FAN_TARGET_COUNT_HIGH_ADDRESS+2*fanNum, FAN_TARGET_COUNT_HIGH(targetCount));
    ret = (ret == COTA_ERROR_NONE) ? writeFanRegister(FAN_TARGET_COUNT_LOW_ADDRESS+2*fanNum, FAN_TARGET_COUNT_LOW(targetCount)) : ret;

    return ret;
}
 
/**
 *  @brief Sets the PWM to select the fan speed
 *
 * @param fanNum   The number of the fan to set this for.  Select 0-3
 * @param percent  0-20% is minimum speed #FAN_MIN_RPM_PERCENT, 100% is max speed
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t 
 */
static CotaError_t setFanSpeed(FanNum_t fanNum, uint8_t percent)
{
    uint16_t count;
    CotaError_t ret = COTA_ERROR_NONE;
  
    if (percent < FAN_MIN_RPM_PERCENT)
    {
        percent = FAN_MIN_RPM_PERCENT;
    }
    
    count = (percent)*FAN_MAX_PWM/FAN_MAX_SPEED;   
    ret = writeFanRegister(FAN_TARGET_DUTY_CYCLE_HIGH_ADDR + 2*fanNum, FAN_TARGET_DUTY_CYCLE_HIGH(count));
    ret = (ret == COTA_ERROR_NONE) ? writeFanRegister(FAN_TARGET_DUTY_CYCLE_LOW_ADDR + 2*fanNum, FAN_TARGET_DUTY_CYCLE_HIGH(count)) : ret;    

    return ret;
}

/**
 * @brief Sets the PWM for all valid fans to the selected the fan speed
 *
 * @param percent  0-20% is minimum speed #FAN_MIN_RPM_PERCENT, 100% is max speed
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t 
 */
static CotaError_t setAllFansSpeed(uint8_t percent)
{
    CotaError_t err = COTA_ERROR_NONE;
    uint8_t ii;
    
    for (ii = 0; ii < FAN_MAX_NUM; ii++)
    {     
        if (BIT_IN_MASK(ii, gValidFans))
        {
            err |= setFanSpeed(ii, percent);  
        }
    }
  
    return err;
}
/**
 * @brief Fans can be set to full on by setting a GPIO in case there is an issue with I2C
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t 
 */
void SetEmergencyFanOn(bool fullFanOn)
{ 
    HAL_GPIO_WritePin(FAN_FULL_GPIO_Port, FAN_FULL_Pin, fullFanOn ? FAN_FULL_SPEED_ON : FAN_FULL_SPEED_OFF); 

    if (fullFanOn)
    {
        gFansOkMask = 0;
        PostPrintf("Fan emergency: All fans full on\r\n");
    }
}


/**
 * @brief Initializes the fan controller for operation.
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t 
 */
CotaError_t InitFansAndTemps(void)
{
    uint8_t regVal;
    uint16_t regVal16;
    uint8_t ii;
    CotaError_t ret = COTA_ERROR_NONE;
    
    CfgGetParam(CFG_VALID_FANS, &gValidFans, sizeof(gValidFans));
    CfgGetParam(CFG_CRITICAL_TEMPERATURE, &gCritTemp, sizeof(gCritTemp)); 

    //Plug the critical temperatue into the fan speed map.
    for (ii = 0; (ii < gTempSpeedMapSize); ii++)
    { 
        //Every map entry at or above the critical temperature must be set to FAN_CRITICAL_TEMP_SPEED
        //so an emergency will be declared if this temperature is reached.
        if (gMapTempToSpeed[ii].temp >= gCritTemp)
        {
            gMapTempToSpeed[ii].speed = FAN_CRITICAL_TEMP_SPEED;
            gMapTempToSpeed[ii].temp = gCritTemp;
        }
    }

    
#ifdef ENABLE_CCB_TEMP_MONITOR 
    ///< @todo Enable for version B of the CCB
    ret = InitializeCCBTemperatures();
    ret = (ret == COTA_ERROR_NONE) ? StartCCBTemperatureAcquisition(TEMP_CONV_RATE_0_25SEC) : ret;
#endif    
    ret = (ret == COTA_ERROR_NONE) ? InitI2CMultiplexer(&gCcbFanInfo, FAN_MULTIPLEX_NUM, FAN_MULTIPLEX_CHAN, FAN_I2C_ADDRESS) : ret;
    
    //Make sure the 'full fan' gpio, which forces fans to full speed, is off.
    if (ret == COTA_ERROR_NONE)
    {
        SetEmergencyFanOn(false);
    }
    
    //Let's set the general configuration register
    regVal = createFanRegField(GC_WATCHDOG_PERIOD, WATCHDOG_30_SECONDS) |      ///< If no I2C traffic detected in 30 seconds, the watchdog turns on all fans to the max.
             createFanRegField(GC_I2C_BUS_TIMEOUT_DIS, FAN_REG_SET_BIT);       ///< Disable I2C reset
    
    ret = (ret == COTA_ERROR_NONE) ? writeFanRegister(FAN_GEN_CONFIG_ADDRESS, regVal) : ret;
    
    //Set the PWM's to 25kHz
    regVal = createFanRegField(PWM_FREQUENCY_1_THRU_3, FAN_PWM_FREQ_25KHZ) |
             createFanRegField(PWM_FREQUENCY_4_THRU_6, FAN_PWM_FREQ_25KHZ);
    
    ret = (ret == COTA_ERROR_NONE) ? writeFanRegister(FAN_PWM_FREQUENCY_ADDRESS, regVal) : ret;
    
    //We choose to use 2 tach periods (1 rotations) to count because we want to make the best use of 
    //the 11 bit tach count regiseter.   The maximum value occurs at 20% fan speed 
    //which is the lowest supported fan speed.  At 2200 rpm, the maximum fan speed,
    //the tach count should be 223 counts.  At 20% rpm, it should 1117. 
    regVal = createFanRegField(FAN_DYN_SPEED_RANGE, COUNT_BETWEEN_2_TACH_PERIODS) |
             createFanRegField(FAN_DYN_PWM_RATE_OF_CHANGE, PWM_RATE_OF_CHANGE_2_70);   //Set rate of change from 33% to 100% to 2.7s
    
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = writeFanRegister(FAN_DYNAMICS_ADDRESSS+ii, regVal);   
    }

    //We to throw a fan fault after the first fault detection, we continue normal operation 
    //after a fault, and we delay startup by 500 ms.
    regVal = createFanRegField(FAN_FAILED_QUEUE, FAN_FAIL_FAULT_1) |
             createFanRegField(FAN_FAILED_OPTIONS, FAN_FAIL_CONTINUE_OPERATION) |  
             createFanRegField(FAN_FAILED_START_DELAY, SEQ_START_DELAY_500_ms);  
    
    ret = writeFanRegister(FAN_FAILED_ADDRESS, regVal);   
       
    //We now need to set the the maximum tach count.  If the count is exceeded, fan faults will be generated.
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = setFanTachTargetCount(ii, FAN_TACH_FAIL_COUNT);
    }    
    
    //Now we set the fan configuration registers for each fan. Most settings are set to default
    //expept for spin up time and tach input enable
    regVal = createFanRegField(FAN_CONF_RPM_MODE_EN, FAN_REG_RESET_BIT) |           //Let's stay in PWM mode for now, we will switch to RPM mode later in initialization
             createFanRegField(FAN_CONF_SPINUP_TIME, SPIN_UP_0_5) |                 //Choose a maximum of 0.5 second spin up time.
             createFanRegField(FAN_CONF_MONITOR_ONLY_EN, FAN_REG_RESET_BIT) |       //Let's leave the fan in control mode
             createFanRegField(FAN_CONF_TACH_INPUT_EN, FAN_REG_SET_BIT) |           //Let's enable the tachometer input so we can control the fan
             createFanRegField(FAN_CONF_TACH_IN_LOCK_ROT_EN, FAN_REG_RESET_BIT) |   //Let's not lock fan rotation
             createFanRegField(FAN_CONF_LOCKED_ROTOR_POL, FAN_REG_RESET_BIT) |      //Fan tach will go low when stopped.
             createFanRegField(FAN_CONF_PWM_TACH_IN_EN, FAN_REG_RESET_BIT);         //Do not configure the fan output as tach input.
    
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = writeFanRegister(FAN_CONFIG_ADDRESSS+ii, regVal);   
    }
    
    //Hopefully, the fans are already going, but's let's start them at the lowest setting of 20% to keep the sound under control
    regVal16 = FAN_LOW_TARGET_PWM;
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = writeFanRegister(FAN_TARGET_DUTY_CYCLE_HIGH_ADDR + 2*ii, FAN_TARGET_DUTY_CYCLE_HIGH(regVal16));
        ret = (ret == COTA_ERROR_NONE) ? writeFanRegister(FAN_TARGET_DUTY_CYCLE_LOW_ADDR + 2*ii, FAN_TARGET_DUTY_CYCLE_HIGH(regVal16)) : ret;
        ret = (ret == COTA_ERROR_NONE) ? setFanTachTargetCount(ii,FAN_TACH_FAIL_COUNT) : ret;
    }  
    
    if (ret != COTA_ERROR_NONE)
    {
        SetEmergencyFanOn(true);  //Turn on fans full if there is an error
    }
    
    return ret;
}

/**
 * @brief Retrieves the fan fault register so fan faults can be detected.
 *        
 * @note  Fan fault bits are cleared by setting the fan's speed
 * 
 * @param reg pointer to a uint8_t to retrieve the fan fault register 
 */
static CotaError_t checkFanFailures(uint8_t* reg)
{
    return readFanRegister(FAN_FAULT_ADDRESS, reg);
}

/**
 * @brief Updates the gFansOkMask which is read by the main thread. Also updates
 *        fan speed based on UVP temperature.
 * 
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t 
 */
CotaError_t UpdateFansOkMaskAndSpeed(void)
{
    CotaError_t err = COTA_ERROR_NONE;
    uint8_t fanFaultMask = 0;
    uint8_t ii;
    AmbMask_t workMask = 0;
#ifdef ENABLE_UVP_TEMP_MONITOR 
    uint8_t speed;
    int16_t uvpAvgTemps[MAX_NUM_AMB];
    int16_t uvpLargestAvg;
#endif
  
    err = checkFanFailures(&fanFaultMask);

    if (err == COTA_ERROR_NONE)
    {
        //loops through fans and check for failures
        for (ii = 0; ii < FAN_MAX_NUM; ii++)
        {     
            if (BIT_IN_MASK(ii, gValidFans))
            {
                if (!(fanFaultMask & FAN_FAULT_MASK(ii)))
                {
                    workMask |= NUM_TO_MASK(ii);
                }
            }
        }
    }
    
    if (gFullFans)
    {
        setAllFansSpeed(FAN_MAX_SPEED);
    }
    else
    {
#ifdef ENABLE_CCB_TEMP_MONITOR   
        //Loops through ccb temps and checks if the critical temperature has been exceeded
        for (ii = 0; (ii < TEMP_CCB_NUM_OF_THERM) && (err == COTA_ERROR_NONE); ii++)
        {
            int16_t temp = 0;
            err = ReadCCBTemperature(ii, &temp);
            if ((err == COTA_ERROR_NONE) && (temp > CELSIUS_TO_CENTICELSIUS(gCritTemp)))
            {
                err = COTA_ERROR_OVER_CRITICAL_TEMPERATURE;
            }
        }  
#endif
#ifdef ENABLE_UVP_TEMP_MONITOR
        if (err == COTA_ERROR_NONE)
        {
            //Let's get the UVP temperatures
            err =  GetAverageUvpTempsFromCache(uvpAvgTemps, MAX_NUM_AMB);

            if (err == COTA_ERROR_NONE)
            {
                uvpLargestAvg = 0;
                //Let's loop over the UVP and find the largest UVP temperature.

                for (ii = 0; ii < MAX_NUM_AMB; ii++)
                {     
                    if (!BIT_IN_MASK(ii, AmbGetValidAmbs()))
                    {
                        continue;
                    }
                    
                    if (uvpAvgTemps[ii] > uvpLargestAvg)
                    {
                        uvpLargestAvg = uvpAvgTemps[ii];
                    }
                }
                
                //adjust the fan speed according to largest UVP temperature             
                for (ii = 0; (ii < gTempSpeedMapSize) && (CELSIUS_TO_CENTICELSIUS(gMapTempToSpeed[ii].temp) < uvpLargestAvg); ii++)
                { 
                    speed = gMapTempToSpeed[ii].speed;
                }
                
                if (speed == FAN_CRITICAL_TEMP_SPEED)
                {
                    err = COTA_ERROR_OVER_CRITICAL_TEMPERATURE;
                }
                else
                {    
                    err = setAllFansSpeed(speed);
                }
            } 
        }
#endif
    }
    // If there is an error, shit has hit the fans. Set gFansOkMask to zero
    // so the control task knows to stop charging.  Also turn the fans on full
    // with a gpio.
    if (err != COTA_ERROR_NONE)
    {
        workMask = 0;
        POST_COTA_ERROR(err);
        SetEmergencyFanOn(true);  //Turn on fans full if there is an error
    }
    
    gFansOkMask = workMask;
    
    if (!FansOk())
    { 
        setAllFansSpeed(FAN_MAX_SPEED);

        SetEmergencyFanOn(true);  //Turn on fans full if there is an fan error
    }
    else
    {
        SetEmergencyFanOn(false); 
    }
 
    return POST_COTA_ERROR(err);
}

/**
 * @brief Return the value of the global fans full variable
 */
void GetFansFull(uint8_t * enable)
{
    *enable = (uint8_t) gFullFans;
}
/**
 * @brief If enabled, this will force the fans to run at full speed
 *        but this is NOT considered an emergency, so TPS will still work.
 * enable  If true, fans run at full speed; otherwise the fan driver controls the speed.
 */
void SetFansFull(bool enable)
{
    gFullFans = enable;
}
