/**
 * @file       proxy_fw_update_driver.c
 *
 * @brief      Header file for the proxy firmware update driver
 *
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _PROXY_FW_UPDATE_DRIVER_H_
#define _PROXY_FW_UPDATE_DRIVER_H_

#include <stdbool.h>
#include <stdint.h>

bool ProxyFwUpdateInit(void);
bool ProxyFwUpdatePingBL(void);
bool ProxyFwUpdateRead(uint32_t address, uint8_t * buf, uint32_t size);
bool ProxyFwUpdateSetup(uint32_t address, uint32_t size);
bool ProxyFwUpdateWrite(uint32_t size, uint8_t * buf);
bool ProxyFwUpdateErase(uint32_t address, uint32_t size);

#endif  // #ifndef _PROXY_FW_UPDATE_DRIVER_H_
