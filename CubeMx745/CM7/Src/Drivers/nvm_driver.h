/****************************************************************************//**
 * @file       nvm_driver.h
 *
 * @brief      Header file for the NVM storage driver
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _NVM_DRIVER_H_
#define _NVM_DRIVER_H_


#include "error.h"
#include "eeprom_driver.h"
#include "orion_config.h"
#include "CotaCommonTypes.h"
#include "stm32h7xx_hal.h"
#include "client.h"

#define CRC32_CHECKSUM_SIZE_BYTES  4  ///< Number of bytes used by the 32-bit CRC checksum

/** Struct defining the field layout of the client memory slot */
typedef struct _ClientSlotData_t
{
    uint16_t        shortId;    ///< Short ID assigned to the client when it was registered
    ExtdAddr_t      longId;     ///< Long ID (IEEE 802.15.4 MAC address)
    CotaRcvrQuery_t queryType;  ///< Client query type
    uint8_t         priority;   ///< Charging priority
    uint16_t        version;    ///< Version (stored on discovery, reported for ESLs, which never get discovered again, via the client_detail cmd)
    uint32_t        hwModel;    ///< HW model (stored on discovery, reported for ESLs, which never get discovered again, via the client_detail cmd)
    uint8_t         motionEnabled; ///< Motion enabled flag
    uint16_t        nvmSlot;    ///< Memory slot number used for storing the client data
} ClientSlotData_t;

CotaError_t NvmInit(void);
void NvmClientDataInit(ClientSlotData_t * nvmClientData);
CotaError_t NvmGetConfigData(SysConfig_t * configData);
CotaError_t NvmSaveConfigData(SysConfig_t * configData);
CotaError_t NvmAddClient(ClientSlotData_t * nvmClientData);
CotaError_t NvmDeleteClient(uint16_t slotNum);
CotaError_t NvmUpdateClient(Client_t *client);
CotaError_t NvmSaveRssiThreshToEeprom(void);
CotaError_t NvmGetRssiThreshFromEeprom(void);
uint16_t*   NvmGetRssiLowThreshArray(void);
void        NvmRssiThreshReset(void);
void        NvmLock(void);
void        NvmUnlock(void);

void NvmReset(void);

#endif // #ifndef _NVM_DRIVER_H_
