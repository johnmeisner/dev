/**
 * @file      eeprom_driver.c
 *
 * @brief     Device driver to manage I2C communication with the EEPROM chip.
 *
 * Note that the 7-bit address of the chip depends on which memory block is being addressed:
 * 1st 64KB:  0x54
 * 2nd 64KB:  0x55
 * 3rd 64KB:  0x56
 * 4th 64KB:  0x57
 *
 * To address the 256-byte Identification Page, use the address 0x5C.
 *
 *
 * @note       The first HW build had the 1-Mbit version of the chip installed, so the number
 *             of pages is limited to 512 and the memory address size is 17 bits.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "eeprom_driver.h"
#include "main.h"
#include "orion_config.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "string.h"
#include "stdbool.h"
#include "debug_off.h"
#include "cmsis_os.h"


#define EEPROM_I2C_ADDRESS          0x54                                                   ///< The EEPROM 7-bit address on I2C bus, which includes 2 bits to select the first of the four 64KB memory blocks
#define EEPROM_I2C_ID_ADDR          0x5C                                                   ///< The EEPROM 7-bit address on I2C bus used to address the Identification Page memory
#define PAGE_NUM_MSB_MASK           0x03                                                   ///< Most significant page number bits (A16 and A17)
#define PAGE_NUM_MSB(addr)          (((addr) >> 16) & (PAGE_NUM_MSB_MASK))                 ///< Bits 16 and 17 of the address are a part of the I2C device address used to address the chip
#define ADDR_TO_BYTE(addr)          (((EEPROM_I2C_ADDRESS) | (PAGE_NUM_MSB(addr))) << 1)   ///< Adjust the I2C device address based on the targeted memory address and move the 7-bit I2C address to the top seven bits in the byte
#define ADDR_TO_BYTE_ID             ((EEPROM_I2C_ID_ADDR) << 1)                            ///< 7-bit I2C device address for the Identification Page shifted to the left to form a byte
#define MAX_EEPROM_TIMEOUT_MS       1000                                                   ///< Maximum EEPROM read/write timeout in milliseconds
#define EEPROM_WRITE_TIME_MS        10                                                     ///< Write time in milliseconds per datasheet
#define BOARD_ID_SIZE               sizeof(gBoardId)                                       ///< The size of the board ID in bytes
#define EEPROM_NUM_PAGES            ((EEPROM_MEM_SIZE) / (EEPROM_PAGE_SIZE))               ///< Number of EEPROM memory pages


static SemaphoreHandle_t gEepromSem = NULL;      ///< Semaphore signaling a complete i2c transaction
static StaticSemaphore_t gEepromSemBuff;         ///< The buffer holding the information for the #gEepromSem semaphore

static SemaphoreHandle_t gEepromMutex;           ///< Semaphore used to make this driver thread safe
static StaticSemaphore_t gEepromMutexBuff;       ///< The buffer holding the information for the #gEepromMutex mutex


extern I2C_HandleTypeDef EEPROM_I2C_HANDLE;      ///< The handle to the I2C hal driver
static bool EepromInited = false;

/**
 * @brief  Initializes the EEPROM driver.
 *
 * This function creates a semaphore and a mutex used by this driver
 *
 * @note   Since the function uses semaphores, it must be called from a task context.
 *  
 * @return COTA_ERROR_NONE
 */
CotaError_t EepromInit(void)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    
    if (!EepromInited)
    {
        // Create a semaphore for waiting for EEPROM operation completion
        gEepromSem = xSemaphoreCreateBinaryStatic(&gEepromSemBuff);
        while (gEepromSem == NULL);

        // Create a mutex for limiting concurrent EEPROM operations to one
        gEepromMutex = xSemaphoreCreateMutexStatic(&gEepromMutexBuff); 
        while (gEepromMutex == NULL);
        xSemaphoreGive(gEepromMutex);
        EepromInited = true;
    }
    return POST_COTA_ERROR(retVal);
}


/**
 * @brief I2C memory write completed event handler
 *
 * This function is meant to be called when I2C memory write
 * transaction has completed from the callback function registered
 * with the HAL I2C driver.
 *
 * @param  hi2c  Pointer to the I2C HAL driver handle
 *
 * @note   This function is called from an interrupt context
 *
 */ 
void Eeprom_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    xSemaphoreGiveFromISR(gEepromSem, &xHigherPriorityTaskWoken); 
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}


/**
 * @brief  I2C memory read completed event handler
 *
 * This function is meant to be called when I2C memory read
 * transaction has completed from the callback function registered
 * with the HAL I2C driver.
 *
 * @param  hi2c  Pointer to the I2C HAL driver handle
 *
 * @note   This function is called from an interrupt context
 *
 */ 
void Eeprom_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
      
    xSemaphoreGiveFromISR(gEepromSem, &xHigherPriorityTaskWoken);       
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}


/**
 * @brief Writes data to general memory of the EERPOM chip
 * 
 * @param address  Starting memory address
 * @param pData    Pointer to a buffer to write to EEPROM
 * @param size     The number of bytes in the buffer to write to EEPROM
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 *
 * @note   This function will only write to a single 256-byte page of EEPROM
 *
 */
CotaError_t EepromWrite(uint32_t address, uint8_t* pData, uint32_t size)
{
    CotaError_t retVal = COTA_ERROR_I2C_FAILED_TO_WRITE;
    HAL_StatusTypeDef retHal = HAL_OK;

    // Ensure we're writing to a single EEPROM page
    if ((address / EEPROM_PAGE_SIZE) == ((address + size - 1) / EEPROM_PAGE_SIZE))
    {
        if (xSemaphoreTake(gEepromMutex, pdMS_TO_TICKS(MAX_EEPROM_TIMEOUT_MS)) == pdPASS)
        {
            retHal = HAL_I2C_Mem_Write_IT(&EEPROM_I2C_HANDLE, ADDR_TO_BYTE(address), (uint16_t)address, I2C_MEMADD_SIZE_16BIT, pData, size);
            
            if (retHal == HAL_OK)
            {
                if (xSemaphoreTake(gEepromSem, pdMS_TO_TICKS(MAX_EEPROM_TIMEOUT_MS)) == pdPASS)
                {
                    retVal = COTA_ERROR_NONE;
                }
            }

            xSemaphoreGive(gEepromMutex);
            
            // Ensure nothing happens while the EEPROM is being written to
            osDelay(pdMS_TO_TICKS(EEPROM_WRITE_TIME_MS));
        }
    }
    else
    {
        retVal = COTA_ERROR_EEPROM_INVALID_ADDRESS;
    }

    return retVal;
}


/**
 * @brief Reads data from general memory of the EERPOM chip
 * 
 * @param address  Starting memory address (0 - 255)
 * @param pData    Pointer to an array to receive the data
 * @param size     The mamximum number of bytes to receive
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 *
 * @note   This function will only read from a single 256-byte page of EEPROM
 *
 */
CotaError_t EepromRead(uint32_t address, uint8_t* pData, uint32_t size)
{
    CotaError_t retVal = COTA_ERROR_I2C_FAILED_TO_READ;
    HAL_StatusTypeDef retHal = HAL_OK;
   
    // Ensure we're reading from a single EEPROM page
    if ((address / EEPROM_PAGE_SIZE) == ((address + size - 1) / EEPROM_PAGE_SIZE))
    {
        if (xSemaphoreTake(gEepromMutex, pdMS_TO_TICKS(MAX_EEPROM_TIMEOUT_MS)) == pdPASS)
        {
            retHal = HAL_I2C_Mem_Read_IT(&EEPROM_I2C_HANDLE, ADDR_TO_BYTE(address), (uint16_t)address, I2C_MEMADD_SIZE_16BIT, pData, size);

            if (retHal == HAL_OK)
            {
                if (xSemaphoreTake(gEepromSem, pdMS_TO_TICKS(MAX_EEPROM_TIMEOUT_MS)) == pdPASS)
                {
                    retVal = COTA_ERROR_NONE;
                }
            }

            xSemaphoreGive(gEepromMutex);
            
            // Ensure nothing happens while the EEPROM is being written to
            osDelay(pdMS_TO_TICKS(EEPROM_WRITE_TIME_MS));
        }
    }
    else
    {
        retVal = COTA_ERROR_EEPROM_INVALID_ADDRESS;
    }
    
    return retVal;
}


/**
 * @brief Writes data to the EEPROM Identification Page
 * 
 * @param address  Starting memory address (0 - 255)
 * @param pData    Pointer to a buffer to write to EEPROM
 * @param size     The number of bytes in the buffer to write to EEPROM
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 *
 * @note   This function will only write to a single 256-byte page of EEPROM
 *
 */
CotaError_t EepromWriteConfigPage(uint8_t address, uint8_t* pData, uint16_t size)
{
    CotaError_t retVal = COTA_ERROR_I2C_FAILED_TO_WRITE;
    HAL_StatusTypeDef retHal = HAL_OK;

    // Ensure we're writing to a single EEPROM page
    if ((address / EEPROM_PAGE_SIZE) == ((address + size - 1) / EEPROM_PAGE_SIZE))
    {
        if (xSemaphoreTake(gEepromMutex, pdMS_TO_TICKS(MAX_EEPROM_TIMEOUT_MS)) == pdPASS)
        {
            retHal = HAL_I2C_Mem_Write_IT(&EEPROM_I2C_HANDLE, ADDR_TO_BYTE_ID, address, I2C_MEMADD_SIZE_16BIT, pData, size);
            
            if (retHal == HAL_OK)
            {
                if (xSemaphoreTake(gEepromSem, pdMS_TO_TICKS(MAX_EEPROM_TIMEOUT_MS)) == pdPASS)
                {
                    retVal = COTA_ERROR_NONE;
                }
            }

            xSemaphoreGive(gEepromMutex);
            
            // Ensure nothing happens while the EEPROM is being written to
            osDelay(pdMS_TO_TICKS(EEPROM_WRITE_TIME_MS));
        }
    }
    else
    {
        retVal = COTA_ERROR_EEPROM_INVALID_ADDRESS;
    }

    return retVal;
}


/**
 * @brief Reads data from the EEPROM Identification Page
 * 
 * @param address  Starting memory address (0 - 255)
 * @param pData    Pointer to an array to receive the data
 * @param size     The mamximum number of bytes to receive
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 *
 * @note   This function will only read from a single 256-byte page of EEPROM
 *
 */
CotaError_t EepromReadConfigPage(uint8_t address, uint8_t* pData, uint16_t size)
{
    CotaError_t retVal = COTA_ERROR_I2C_FAILED_TO_READ;
    HAL_StatusTypeDef retHal = HAL_OK;

    // Ensure we're reading from a single EEPROM page
    if ((address / EEPROM_PAGE_SIZE) == ((address + size - 1) / EEPROM_PAGE_SIZE))
    {
        if (xSemaphoreTake(gEepromMutex, pdMS_TO_TICKS(MAX_EEPROM_TIMEOUT_MS)) == pdPASS)
        {
            retHal = HAL_I2C_Mem_Read_IT(&EEPROM_I2C_HANDLE, ADDR_TO_BYTE_ID, address, I2C_MEMADD_SIZE_16BIT, pData, size);

            if (retHal == HAL_OK)
            {
                if (xSemaphoreTake(gEepromSem, pdMS_TO_TICKS(MAX_EEPROM_TIMEOUT_MS)) == pdPASS)
                {
                    retVal = COTA_ERROR_NONE;
                }
            }

            xSemaphoreGive(gEepromMutex);
            
            // Ensure nothing happens while the EEPROM is being written to
            osDelay(pdMS_TO_TICKS(EEPROM_WRITE_TIME_MS));
        }
    }
    else
    {
        retVal = COTA_ERROR_EEPROM_INVALID_ADDRESS;
    }
    
    return retVal;
}
