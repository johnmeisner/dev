/**
 * @file       raspberry_pi.h
 *
 * @brief      Raspberry Pi communication driver header file
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#ifndef _RPI_DRIVER_HEADER_
#define _RPI_DRIVER_HEADER_

#include "stdint.h"
#include "stdbool.h"
#include "ctrl_task.h"
#include "FreeRTOS.h"

#define MAX_FRAME_SIZE        1024   ///< The size of the SPI frame of type #RpiMsg_t to/from Raspberry Pi
#define CRC_OFST_FIELD_SIZE      2   ///< The size of the CRC offset value field
#define CRC_FIELD_SIZE           4   ///< The size of the CRC field in the message frame
#define MSG_LEN_FIELD_SIZE       2   ///< The size of the message length field
#define MSG_ID_FIELD_SIZE        1   ///< The size of the message id field 

void     RpiDriverInit(void);
bool     RpiIsRxQueueEmpty(void);
bool     RpiSendFrame(uint8_t * buf, uint16_t size);
uint32_t RpiGetMessage(uint8_t * buf, uint16_t size);
void     RpiMasterReadyIrq(BaseType_t * const higherPriorityTaskWoken);

#endif   // ifndef _RPI_DRIVER_HEADER_
