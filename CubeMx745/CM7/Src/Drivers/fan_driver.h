/****************************************************************************//**
* @file      fan_driver.h
*
* @brief     Header file to read and write to the fan driver chip
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019-2020 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _FAN_DRIVER_H_
#define _FAN_DRIVER_H_

#include <stdbool.h>
#include "error.h"

/**
 * @note Since the fan watchdog is set at 30 seconds, let's poll the fan driver every 25 seconds to give us an extra 5 seconds of buffer
 */
#define FAN_CHECK_PERIOD_MS 25000 ///< The wait interval for the fan task in milliseconds.  
#define FAN_SPEED_FULL      1     ///< The argument to SetFansFull when full speed is desired
#define FAN_SPEED_AUTO      0     ///< The argument to SetFansFull when driver control is desired

CotaError_t InitFansAndTemps(void);
CotaError_t UpdateFansOkMaskAndSpeed(void);
void SetEmergencyFanOn(bool fullFanOn);
bool FansOk(void);
void SetFansFull(bool enable);
void GetFansFull(uint8_t * enable);
#endif  //#ifndef _FAN_DRIVER_H_

