/****************************************************************************//**
* @file      power_monitor.h
*
* @brief     Header file to enable reading of power monitors
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2020 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _POWER_MONITOR_H_
#define _POWER_MONITOR_H_
#include "error.h"
#include "ctrl_interface.h"

#define POWER_SUPPLY_WAIT_TIMEOUT   2000   ///< Time to wait for power supplies to come up in milliseconds

#define PGPAB_BIT       0   ///< The position in stateMask for the PGPAB gpio
#define PGCCBB_BIT      1   ///< The position in stateMask for the PGCCBB gpio
#define PGPAA_BIT       2   ///< The position in stateMask for the PGPAA gpio
#define PGCCBA_BIT      3   ///< The position in stateMask for the PGCCBA gpio
#define PG5P0_BIT       4   ///< The position in stateMask for the PG5P0 gpio
#define PG3P3_BIT       5   ///< The position in stateMask for the PG3P3 gpio
#define PG1P8_BIT       6   ///< The position in stateMask for the PG1P8 gpio
#define POE_1_ON_BIT    7   ///< The position in stateMask for the POE_1_ON gpio
#define POE_2_ON_BIT    8   ///< The position in stateMask for the POE_2_ON gpio

#define PM_REFRESH (0x2)   ///< Indicates power monitors need refreshing
#define PM_ENABLED (0x1)   ///< Bit indicates that power monitor is enabled


CotaError_t InitializeCcbPowerMonitors(void);
CotaError_t RefreshPowerMonitors(void);
CotaError_t UpdatePowerValues(void);
bool IsPowerGoodForCCB(void);
bool WaitForPowerGood(uint16_t timeoutMs);
#endif // #ifndef _POWER_MONITOR_H_

