/**
 * @file       proxy.c
 *
 * @brief      Proxy communication driver implementation
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 *
 * Proxy Communication Protocol
 * ============================
 *
 * Outgoing Messages
 * -----------------
 *
 * Since this driver is controlling the SPI master device, we know exactly how many bytes we
 * want to transmit before the SPI clock starts.  This allows us to limit message length to
 * the minimum necessary number of bytes to transmit.
 *
 *
 * @verbatim
 *
 * Outgoing message format:
 * ------------------------------------------------------------
 * | Payload | Payload | Payload |       | Payload | Checksum |
 * |  Size   |  Byte   |  Byte   |  ---  |  Byte   |          |
 * |   N     |   0     |   1     |       |   N-1   |          |
 * ------------------------------------------------------------
 *            \_ _ _ _ _ _N payload bytes_ _ _ _ _/
 *
 * @endverbatim
 *
 * The driver has to allocate and transmit N+2 bytes to send a message of up to N bytes in
 * the payload.  The two extra bytes are described below.
 *
 * The 1st byte that's transmitted to the proxy contains the number of bytes in the message
 * payload (payload size).  This number doesn't include the payload size field or the checksum.
 *
 * The last byte transmitted is the checksum of all the preceding bytes, including the payload
 * size byte and the payload data bytes.  The bytes are simply XOR'ed together to obtain the
 * checksum value.
 *
 * Incoming Messages
 * -----------------
 *
 * Since this driver is controlling the SPI master device, it has no prior knowledge of how
 * many bytes the proxy is trying to transmit in the message payload.  To speed up processing
 * and delegate SPI transfer to DMA, it's easier to attempt to transmit a block of data of
 * predetermined size and sort out what bytes the proxy was transmitting at a later time
 * (when the number of bytes in the transfer becomes known).  The payload size for all incoming
 * SPI transfers is going to be 128 bytes, which is the maximum number of bytes supported by the
 * IEEE 802.15.4 messages (this means the proxy doesn't need to send us larger messages anyway).
 *
 *
 * @verbatim
 *
 * Incoming message format:
 *  ----------------------------------------------------------------------------
 *  | Payload | Payload | Payload |       | Payload |       | Payload | Checksum |
 *  |  Size   |  Byte   |  Byte   |  ---  |  Byte   |  ---  |  Byte   |          |
 *  |   N     |   0     |   1     |       |   N-1   |       |   127   |          |
 *  ----------------------------------------------------------------------------
 *             \_ _ _ _ _ _N payload bytes_ _ _ _ _/
 *
 * @endverbatim
 *
 * The driver has to allocate and transmit 130 bytes to receive a message from the proxy. 128 bytes
 * are allocated for the payload data and two extra bytes are described below.
 *
 * The 1st byte that's transmitted by the proxy contains the number of bytes in the message
 * payload (payload size).  This number doesn't include the payload size field or the checksum.
 *
 * The last byte transmitted is the checksum of the payload size byte and the N payload data bytes.
 * It doesn't include the unused payload data bytes beyond the first N payload data bytes.  The bytes
 * are simply XOR'ed together to obtain the checksum value.
 *
 */


#include "proxy.h"
#include "semphr.h"
#include "task.h"
#include "main.h"
#include "orion_config.h"
#include "stdbool.h"
#include "string.h"
#include "stdio.h"
#include "debug_off.h"
#include "cli_task.h"
#include "error.h"


#define PROXY_MSG_SIZE            128                       ///< Proxy message maximum payload data size
#define PROXY_QUEUE_LEN           10                        ///< Allow up to 10 outgoing messages in the queue
#define PROXY_TX_QUEUE_ITEM_SIZE  sizeof(ProxyTxMsg_t)      ///< The size of a message buffer required to store a proxy Tx message
#define PROXY_RX_QUEUE_ITEM_SIZE  (sizeof(ProxyRxMsg_t)-1)  ///< The size of a message buffer required to store a proxy Rx message (-1 because we don't need the checksum)
#define CHECKSUM_SIZE             1                         ///< The size of the checksum at end of a SPI message (Rx/Tx)
#define SR_LINE_ASSERTED          GPIO_PIN_RESET            ///< The state of an asserted slave ready (SR) pin
#define MR_LINE_ASSERTED          GPIO_PIN_RESET            ///< The state of an asserted master ready (MR) pin
#define SR_LINE_DEASSERTED        GPIO_PIN_SET              ///< The state of a de-asserted slave ready (SR) pin
#define MR_LINE_DEASSERTED        GPIO_PIN_SET              ///< The state of a de-asserted master ready (MR) pin
#define NO_WAIT                   0                         ///< The timeout parameter for the queue and semaphore API to never block on their function calls
#define MAX_TOTAL_MSG_COUNT       ((PROXY_QUEUE_LEN) + 1)   ///< The maximum count value for the message counting semaphore (PROXY_QUEUE_LEN is for outgoing messages and another 1 is for received messages)
#define INITIAL_MSG_COUNT         0                         ///< Initial count value for the message counting semaphore
#define SIZEOF_CHECKSUM           sizeof(uint8_t)           ///< the computed checksum in the Proxy_Msg_t
#define SIZEOF_MSG_LEN            sizeof(uint8_t)           ///< length of the message in the Proxy_Msg_t

#define MR_ASSERT()            HAL_GPIO_WritePin(PROXY_MASTER_READY_GPIO_Port, PROXY_MASTER_READY_Pin, MR_LINE_ASSERTED)          ///< Assert master ready line
#define MR_DEASSERT()          HAL_GPIO_WritePin(PROXY_MASTER_READY_GPIO_Port, PROXY_MASTER_READY_Pin, MR_LINE_DEASSERTED)        ///< De-assert master ready line
#define READ_SR_STATE()        HAL_GPIO_ReadPin(PROXY_SLAVE_READY_GPIO_Port, PROXY_SLAVE_READY_Pin)                             ///< Read slave ready line

#define PROXY_CPU_RESET_TIME   100   ///< time in mS to hold proxy CPU in reset


/**
 * @brief  Enumeration of the proxy state machine states
 */
typedef enum _ProxySm_t
{
    PROXY_SM_IDLE,       ///< Idle state
    PROXY_SM_TX_REQ,     ///< Transmit request state (MR asserted; waiting for SR to be asserted)
    PROXY_SM_TX_RDY,     ///< Ready to transmit (SPI transaction started; waiting for completion)
    PROXY_SM_TX_DONE,    ///< SPI transfer for message transmission has ended
    PROXY_SM_RX_RDY,     ///< Ready to transfer data from proxy (SPI transaction started; waiting for completion)
    PROXY_SM_RX_DONE,    ///< SPI transfer for the SPI transaction has ended; waiting for acknowledgement from the proxy
    PROXY_SM_FINISHED    ///< SPI transfer has ended and has been acknowledged by the proxy
} ProxySm_t;

/**
 * @brief  Structure for a Tx proxy message
 *
 *         Because the Tx message is of variable size, but the buffer in #ProxyTxMsg_t
 *         is of a fixed length. For this reason, the checksum must be in the data
 *         buffer as well.
 */
typedef struct _ProxyTxMsg_t
{
    uint8_t messageSize;          ///< Payload data and checksum byte total size, must be kept up-to-date with #SIZEOF_MSG_LEN
    uint8_t data[PROXY_MSG_SIZE]; ///< Buffer for the message payload data, terminated with checksum
    uint8_t reserved[32-((PROXY_MSG_SIZE+sizeof(messageSize)) % 32)];  ///reserved area to keep structure 32 byte aligned.
} ProxyTxMsg_t;

/**
 * @brief  Structure for a proxy message
 *
 *         All messages between the host and proxy include a checksum.
 *         Because the slave message from proxy to host is of fixed length,
 *         the checksum is always in the last position of the packet
 *         (unlike #ProxyTxMsg_t)
 */
typedef struct _ProxyRxMsg_t
{
    uint8_t dummy;
    uint8_t messageSize;           ///< Payload size, must be kept up-to-date with #SIZEOF_MSG_LEN
    uint8_t data[PROXY_MSG_SIZE];  ///< Buffer for the message payload data
    uint8_t checksum;              ///< Checksum byte, must be kept up-to-date with #SIZEOF_CHECKSUM
    uint8_t reserved[32-((PROXY_MSG_SIZE+sizeof(dummy)+sizeof(messageSize)+sizeof(checksum)) % 32)];  ///reserved area to keep structure 32 byte aligned.
} ProxyRxMsg_t;


/**
 * Semaphore to count pending messages to transmit/receive and block
 * on if no incoming/outgoing messages are available (message semaphore)
 */
static SemaphoreHandle_t gProxyMessageSem = NULL;
static SemaphoreHandle_t gProxyStateSem = NULL;          ///< Semaphore to signal a state machine change (state machine semaphore)
static QueueHandle_t     gProxyTxQueue = NULL;           ///< Proxy queue handle for messages to be transmitted
static QueueHandle_t     gProxyRxQueue = NULL;           ///< Proxy queue handle for messages that have been received
static TaskHandle_t      gProxyTaskHandle = NULL;        ///< Proxy task handle
#pragma data_alignment=32
DMA_BUFFER static ProxyRxMsg_t      gProxyRxBuf;         ///< Holds a message that is being sent to the host from the proxy
#pragma data_alignment=32
DMA_BUFFER static ProxyTxMsg_t      gProxyTxMsg;         ///< Used to transmit data to the proxy
static ProxySm_t         gProxySmState = PROXY_SM_IDLE;  ///< Proxy state machine state
static volatile bool     gSrLineAsserted   = false;      ///< True if Slave Ready (SR) line is asserted (logic low)
static volatile bool     gSpiXferCompleted = false;      ///< True if a transfer over the SPI bus has been completed
static volatile bool     gIsRxTest         = true;       ///< If true, run Rx test when the user button is pressed.  If false, run Tx test.
static StaticSemaphore_t gProxySemBuff;                  ///< The buffer holding the information for the #gEepromSem semaphore


extern SPI_HandleTypeDef PROXY_SPI_HANDLE;

static void ProxyTask(void *pvParameters);
void TxCpltCallback(void);
static uint8_t ProxyCalculateChecksum(uint8_t * buf, uint8_t size);


/**
 * @brief  Resets the proxy CPU by toggling the PROXY_RESET line.  It also resets the proxy task.
 *
 * @note   This function uses a vTaskDelay() call, and therefore must be called from a task context
*/
void ProxyReset(void)
{
    UBaseType_t uGetCount = 0;
    uint8_t ii;

    vTaskSuspend(gProxyTaskHandle);   //Suspend the proxy task
    HAL_GPIO_WritePin(GPIOI, PROXY_RESET_Pin, GPIO_PIN_RESET);  //toggle the reset line and hold until we have resumed the proxy task
    MR_DEASSERT();          //Make sure master ready is deasserted.

    //Wait a minimum time to ensure the proxy resets.
    vTaskDelay(pdMS_TO_TICKS(PROXY_CPU_RESET_TIME));

    //Reset both queues to and from the proxy task.
    xQueueReset(gProxyTxQueue);
    xQueueReset(gProxyRxQueue);

    //If the state is not idle, we need to pretend that a message was just completed.
    //Change the state to PROXY_SM_FINISHED and give on the gProxyStateSem
    if (gProxySmState != PROXY_SM_IDLE)
    {
        gProxySmState = PROXY_SM_FINISHED;
        xSemaphoreGive(gProxyStateSem);
    }

    //We reset the gProxyMessageSem counting semaphore by insuring it's count is zero
    uGetCount = uxSemaphoreGetCount(gProxyMessageSem);

    if (uGetCount != 0)
    {
        for (ii = 0; ii < uGetCount; ii++)
        {
           xSemaphoreTake(gProxyMessageSem, NO_WAIT);
        }
    }


    vTaskResume(gProxyTaskHandle);  //Resume the proxy task
    HAL_GPIO_WritePin(GPIOI, PROXY_RESET_Pin, GPIO_PIN_SET);  //Let the proxy start
}


/**
* @brief  Creates the proxy communication task and initializes the proxy comm module
*/
void ProxyDriverInit(void)
{
    BaseType_t xReturned;

    // Create semaphore to wait for state machine state transitions on
    gProxyStateSem = xSemaphoreCreateBinaryStatic(&gProxySemBuff);

    // Halt on error
    while (gProxyStateSem == NULL);


    // Create semaphore to wait for incoming/outgoing messages on
    gProxyMessageSem = xSemaphoreCreateCounting(MAX_TOTAL_MSG_COUNT, INITIAL_MSG_COUNT);  /// @todo  Use xSemaphoreCreateCountingStatic instead

    // Halt on error
    while (gProxyMessageSem == NULL);

    // Create a Tx and Rx message queues
    gProxyTxQueue = xQueueCreate(PROXY_QUEUE_LEN, PROXY_TX_QUEUE_ITEM_SIZE);  /// @todo  Use xQueueCreateStatic() instead
    gProxyRxQueue = xQueueCreate(PROXY_QUEUE_LEN, PROXY_RX_QUEUE_ITEM_SIZE);  /// @todo  Use xQueueCreateStatic() instead

    // Halt on error
    while (gProxyTxQueue == NULL);
    while (gProxyRxQueue == NULL);

    /*
    * TPS Task Creation
    */
    xReturned = xTaskCreate(ProxyTask,              // Function that implements the task
                            "Proxy task",           // Text name for the task
                            PROXY_TASK_STACK_SIZE,  // Stack size in 32-bit words
                            NULL,                   // Parameter passed into the task
                            PROXY_TASK_PRIORITY,    // Priority at which the task is created
                            &gProxyTaskHandle);     // Used to pass out the created task's handle
    /// @todo  Use xTaskCreateStatic instead

    while (xReturned != pdPASS);
}


/**
 * @brief  The proxy task responsible for sending messages to the proxy
 *         and receiving messages from the proxy.
 *
 * @param  pvParameters  Parameters to pass to the task (not used)
 */
static void ProxyTask(void *pvParameters)
{
    while (true)
    {
        // Wait for the proxy to make a request (assert the SR line) or for the application to queue up a message for transmission
        DBG3_RESET();
        xSemaphoreTake(gProxyMessageSem, portMAX_DELAY);
        DBG3_SET();

        // Run the state machine loop until the Finished state is reached
        while (gProxySmState != PROXY_SM_FINISHED)
        {
            // Wait for a state machine state transition event
            DBG4_RESET();
            if (gProxySmState != PROXY_SM_IDLE)
            {
                xSemaphoreTake(gProxyStateSem, portMAX_DELAY);
                /// @todo  Time out after a period of inactivity
            }
            DBG4_SET();

            // Read the SR line (note that the race condition involving the SR line can only occur in the Idle state, so we deal with that separately below)
            gSrLineAsserted = (READ_SR_STATE() == SR_LINE_ASSERTED);

            switch (gProxySmState)
            {
                case PROXY_SM_IDLE:
                {
                    gSpiXferCompleted = false;

                    // Check if the proxy wants to send us a message
                    if (gSrLineAsserted)
                    {
                        // We're waiting to receive a message from the proxy and SR just got asserted
                        // Assert the MR line and receive a message into the Rx queue
                        MR_ASSERT();

                        // We are here because the ISR processing SR line assertion from the proxy has already read the state of gProxySmState
                        // and let us get past the state machine semaphore, so there's no race condition when updating gProxySmState
                        gProxySmState = PROXY_SM_RX_RDY;

                        HAL_SPI_Receive_DMA(&PROXY_SPI_HANDLE, (uint8_t *) &gProxyRxBuf,  PROXY_MSG_SIZE+sizeof(gProxyRxBuf.dummy)+sizeof(gProxyRxBuf.messageSize)+sizeof(gProxyRxBuf.checksum));
                    }
                    // Check if we have any messages we need to transmit
                    else if (uxQueueMessagesWaiting(gProxyTxQueue))
                    {
                        // If SR is asserted at this point, we're going to check for it below

                        // This needs to happen quickly (preferably under 1us), so we'll just disable interrupts to prevent task switching
                        taskENTER_CRITICAL();

                        // If we're transmitting, we need to wait for SR to be asserted first by the proxy,
                        // so set the next state to PROXY_SM_TX_REQ to wait for the proxy to assert SR.
                        // Also, we don't want the ISR processing the SR line assertion from the proxy to read
                        // the state of gProxySmState while we're modifying it, so we want to do it in the critical section.
                        // And, finally, we want the ISR processing the SR line assertion to read the current state machine
                        // state after it's been set, so we have to set our state machine state before asserting the MR line.
                        gProxySmState = PROXY_SM_TX_REQ;

                        // Read the SR line
                        gSrLineAsserted = (READ_SR_STATE() == SR_LINE_ASSERTED);

                        // Assert the MR line and wait for the slave to acknowledge
                        MR_ASSERT();

                        // Read the SR line again - we need to confirm that the proxy didn't initiate a transaction while we were in the middle of initiating a transaction
                        gSrLineAsserted = gSrLineAsserted || (READ_SR_STATE() == SR_LINE_ASSERTED);

                        taskEXIT_CRITICAL();


                        // If SR was not asserted while MR was being asserted, it means the proxy didn't initiate
                        // a SPI transaction and we're safe to transmit in the next state machine state.
                        // If SR is asserted immediately after MR is asserted, it most likely means that SR got
                        // asserted by the proxy before the proxy has detected MR line being asserted, and we need to
                        // take care of the incoming message first by initiating a SPI transfer immediately in this state.
                        /// @todo  As a future improvement, consider transmitting and receiving at the same time
                        if (gSrLineAsserted)
                        {
                            // If an SR got asserted, the state machine semaphore count might have been incremented by one
                            // by the callback function on SR state change, so we need to decrement the semaphore counter
                            // back to 0 to block the inner state machine loop until the SPI transaction is completed.
                            xSemaphoreTake(gProxyStateSem, NO_WAIT);

                            gProxySmState = PROXY_SM_RX_RDY;

                            // Unblock the message semaphore since the received message may not have done it
                            xSemaphoreGive(gProxyMessageSem);
                        }
                    }
                    else
                    {
                        // Nothing
                    }
                }
                break;


                case PROXY_SM_TX_REQ:
                {
                    // We should not return to line unless the SR line was properly asserted (by interrupt)
                    // Check if SR got asserted
                    if (gSrLineAsserted)
                    {
                        gSpiXferCompleted = false;

                        // We've already made sure there's something in the Tx queue in the previous state
                        xQueueReceive(gProxyTxQueue, &gProxyTxMsg, NO_WAIT);

                        // Start a SPI transfer
                        SCB_CleanDCache_by_Addr((uint32_t*)&gProxyTxMsg, gProxyTxMsg.messageSize + SIZEOF_CHECKSUM + SIZEOF_MSG_LEN);
                        HAL_SPI_Transmit_DMA(&PROXY_SPI_HANDLE, (uint8_t *)(&gProxyTxMsg),
                                            gProxyTxMsg.messageSize + SIZEOF_CHECKSUM + SIZEOF_MSG_LEN);
                    }

                    gProxySmState = PROXY_SM_TX_RDY;
                }
                break;


                case PROXY_SM_TX_RDY:
                {
                    // Check if the SPI transfer is completed
                    if (gSpiXferCompleted)
                    {
                        // De-assert the MR line
                        gProxySmState = PROXY_SM_TX_DONE;
                        MR_DEASSERT();
                    }
                }
                break;


                case PROXY_SM_TX_DONE:
                {
                    // SR line got de-asserted - return to Idle state
                    if (!gSrLineAsserted)
                    {
                        gProxySmState = PROXY_SM_FINISHED;
                    }
                }
                break;


                case PROXY_SM_RX_RDY:
                {
                    // Check if the SPI transfer is completed
                    if (gSpiXferCompleted)
                    {
                        // Validate the received message
                        uint8_t checksum = ProxyCalculateChecksum(&gProxyRxBuf.messageSize, gProxyRxBuf.messageSize+1);
                        if ((gProxyRxBuf.checksum == checksum) && (gProxyRxBuf.messageSize > 0))
                        {
                            BaseType_t xReturned = pdTRUE;

                            /*
                             * Put the received message in the Rx queue without waiting for an empty slot
                             * Note that the checksum field will be stripped out by this function call
                             * since it won't fit in the queue element storage block
                             */

                            if (gProxyRxBuf.data[0] != PRX_HOST_NULL)
                            {
                                xReturned = xQueueSend(gProxyRxQueue, &gProxyRxBuf, NO_WAIT);
                            }

                            // If we failed to add the new message to the queue, flag a queue full error
                            if (xReturned != pdTRUE)
                            {
                                /// @todo  Possibly flag a queue full error
                            }
                            else
                            {
                                /// @todo  Alert the main control task that we have a message waiting in the queue by posting a global counting semaphore
                            }
                        }
                        else
                        {
                            PostPrintf("Bad SPI read buf 0x%02x 0x%02x 0x%02x\r\n", gProxyRxBuf.messageSize, gProxyRxBuf.data[0], gProxyRxBuf.data[1]);
                        }

                        // De-assert the MR line
                        gProxySmState = PROXY_SM_RX_DONE;
                        MR_DEASSERT();
                    }
                }
                break;


                case PROXY_SM_RX_DONE:
                {
                    // SR line got de-asserted - return to Idle state
                    if (!gSrLineAsserted)
                    {
                        gProxySmState = PROXY_SM_FINISHED;
                    }
                }
                break;
            }  // switch (gProxySmState)
        }  // while (gProxySmState != PROXY_SM_FINISHED)

        // The state machine has reached the finished state causing the state machine loop to end, so reset the state machine to the Idle state
        gProxySmState = PROXY_SM_IDLE;

        // Indicate we're at the end of the state machine loop back to the Idle state
        DBG4_RESET();
    }
}


/**
 * @brief  Callback on proxy SR line change event
 *
 * @param  higherPriorityTaskWoken  Determines if context switch is needed after the interrupt
 *
 */
void ProxySlaveReadyIrq(BaseType_t * const higherPriorityTaskWoken)
{
    if ((gProxySmState == PROXY_SM_IDLE) &&
        (READ_SR_STATE() == SR_LINE_ASSERTED))
    {
        // Indicate to the message processing loop that the proxy is ready to send us a message
        DBG1_SET();
        if (gProxyMessageSem)
        {
            xSemaphoreGiveFromISR(gProxyMessageSem, higherPriorityTaskWoken);
        }
        DBG1_RESET();
    }
    else
    {
        // Indicate to the state machine that SR line was asserted/deasserted and the proxy is ready for a SPI transaction
        DBG2_SET();
        if (gProxyStateSem)
        {
            xSemaphoreGiveFromISR(gProxyStateSem, higherPriorityTaskWoken);
        }
        DBG2_RESET();
    }
}

/**
 * @brief   Function to send a message to the proxy
 *
 * @details This function will put a message on the queue and signal the proxy
 *          message processing task to start processing the new message.
 *
 * @note    Do not call this function from ISR context
 *
 * @param   buf      Pointer to a buffer with data to transmit to the proxy
 * @param   size     Number of bytes in the buffer pointed to by buf
 *
 * @return  true if successful; false if the transmit queue is full
 */
bool ProxySendMessage(uint8_t * buf, uint8_t size)
{
    BaseType_t retVal;
    ProxyTxMsg_t proxyTxMsg;
    uint8_t framePayloadSize; // This is the size of the payload, plus the message type size

    /*
     * Update size by truncating the message by CHECKSUM_SIZE.
     * There MUST be room for the checksum at the end of the message.
     */
    size = MIN(size, (PROXY_MSG_SIZE - CHECKSUM_SIZE));

    proxyTxMsg.messageSize = size;
    memcpy(proxyTxMsg.data, buf, size);

    // Put the checksum in the last valid position in the data buffer.
    framePayloadSize = size + CHECKSUM_SIZE;
    proxyTxMsg.data[size] = ProxyCalculateChecksum((uint8_t*) &proxyTxMsg, framePayloadSize);

    // Put the new message on the queue
    retVal = xQueueSend(gProxyTxQueue, &proxyTxMsg, NO_WAIT);

    // Only unblock the message processing loop if we successfully added the new message to the queue
    if (retVal == pdTRUE)
    {
        // Unblock the proxy message processing loop
        DBG1_SET();
        xSemaphoreGive(gProxyMessageSem);
        DBG1_RESET();
    }

    return (retVal == pdTRUE);
}


/**
 * @brief  The function that calculates the checksum of all the bytes in a buffer
 *
 * @param  buf      Pointer to a buffer
 * @param  size     Number of bytes in the buffer pointed to by buf
 *
 * @return  0 if the buffer size is 0; otherwise, the checksum of the bytes in the buffer
 */
static uint8_t ProxyCalculateChecksum(uint8_t * buf, uint8_t size)
{
    uint8_t checksum = 0;

    // Return 0 in the special case when the buffer size is 0;
    // otherwise, initialize the checksum value to the first byte in the buffer
    if (size)
    {
        checksum = buf[0];
    }

    // Calculate the checksum by XOR'ing all the bytes in buf
    for (uint8_t i = 1; i < size; i++)
    {
        checksum ^= buf[i];
    }

    return checksum;
}

/**
 * @brief Determines if the Rx Queue is empty
 * @return true if the Rx Queue is empty, false if queue is not empty
 */
bool ProxyIsRxQueueEmpty(void)
{
    return !uxQueueMessagesWaiting(gProxyRxQueue);
}

/**
 * @brief  Retrieves the first available message received from the proxy
 *
 * @param  buf   Pointer to a buffer to put received message into
 * @param  size  Number of bytes in the buffer pointed to by buf
 *
 * @return returns the amount of bytes that was copied to buf
 */
CotaError_t ProxyGetMessage(uint8_t * buf, uint8_t size)
{
    BaseType_t      xReturned;
    ProxyRxMsg_t    proxyRxMsgBuf;
    CotaError_t     err = COTA_ERROR_FAILED_TO_RECEIVE_MSG_QUEUE;

    if ((buf != NULL) && (size >= (sizeof(ProxyRxMsg_t) - 1)))
    {
        // Get the next received message from the Rx queue without blocking on an empty queue
        xReturned = xQueueReceive(gProxyRxQueue, &proxyRxMsgBuf, NO_WAIT);

        if (xReturned == pdPASS)
        {
            //Note that the proxy sends an extra leading 0x00 byte with every
            //SPI transmission as a result of a buggy SPI driver/hardware
            //(not sure if this preceeding statement is accurate anymore)

            //offset the buffer by one
            uint8_t *temp = &(((uint8_t *) &proxyRxMsgBuf)[1]); //this skips "dummy" (the first variable) in ProxyRxMsg_t

            // Copy the received message into the caller's buffer
            memcpy((void*)buf, temp, sizeof(ProxyRxMsg_t)-1); //subtract size by 1 to compensate for the offset
            err = COTA_ERROR_NONE;
        }
    }

    return err;
}

/**
 * @brief  This function is called by the SPI driver after the SPI Tx transfer is completed
 *
 */
void ProxySpiTxCpltIrq(BaseType_t * const higherPriorityTaskWoken)
{
    if (gSrLineAsserted)
    {
        // Unblock the proxy state machine loop
        gSpiXferCompleted = true;
        DBG2_SET();
        xSemaphoreGiveFromISR(gProxyStateSem, higherPriorityTaskWoken);
        DBG2_RESET();
    }
}

/**
 * @brief  This function is called by the SPI driver after the SPI Rx transfer is completed
 *
 */
void ProxySpiRxCpltIrq(BaseType_t * const higherPriorityTaskWoken)
{
    if (gSrLineAsserted)
    {
        // Unblock the proxy state machine loop
        gSpiXferCompleted = true;
        SCB_InvalidateDCache_by_Addr((uint32_t*)&gProxyRxBuf, sizeof(gProxyRxBuf));
        DBG2_SET();
        xSemaphoreGiveFromISR(gProxyStateSem, higherPriorityTaskWoken);
        DBG2_RESET();
    }
}
