/****************************************************************************//**
* @file      uvp_driver.c
*
* @brief     Driver to read and write to UVP's (The asic that controls receive and transmission)
*            The UvpInit function is geared toward SWS1411 UVPv2 Variant 1.
*
*            @todo Uvp temperatures are needed for fan speed updates and
*                  to initiate calibration.    We do not want to get UVP temperatures during TPS,
*                  so we need the top level state machine to call UpdateUvpTemperatureCache(int16_t* maxDelta) 
periodically. (At least once every 25 seconds)
*                  This will cause the temperatures to be read and cached so the fan driver
*                  can use them.  Also, the maxDelta can be used to initiate a calibration if
*                  necessary.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "FreeRTOS.h"
#include "task.h"
#include "string.h"
#include "main.h"
#include "semphr.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "spi_driver.h"
#include "amb_control.h"
#include "uart_driver.h"
#include "stdio.h"
#include "stm32h7xx_hal_uart.h"
#include "error.h"
#include "stdlib.h"
#include "cli_task.h"
#include "fan_driver.h"
#include "ctrl_interface.h"
#include "rpi_msg_interface.h"
#include "debug_off.h"
#include "power_level.h"

#define UVP_PAGE_BUF_SIZE               2       ///< The size of the message we need to send to set the page index
#define UVP_32BIT_CHUNK_BUF_SIZE        5       ///< The size of the data in bytes to write or receive a 32 bit chunk of UVP memory.  5 bytes are needed because the first byte specfies the column address in UVP memory to read and write from.
#define UVP_64BIT_CHUNK_BUF_SIZE        9       ///< The size of the data in bytes to write or receive a 64 bit chunk of UVP memory.  9 bytes are needed because the first byte specfies the column address in UVP memory to read and write from.
#define UVP_READ_MASK                   0x80    ///< The mask on the first byte that indicates a read operation is initiated
#define UVP_WRITE_PAGE_INDEX            0x7F    ///< The column address of the page index.
#define VCOCFS_VALUE_START              32      ///< Low value for the voltage control oscilator capcitor frequency selection bits
#define VCOCFS_VALUE_END                48      ///< High value for the voltage control oscilator capcitor frequency selection bits
#define AMU_ENABLED                     1       ///< Value of the AMU enable register indicating the AMU is enabled.
#define AMU_DISABLED                    0       ///< Value of the AMU enable register indicating the AMU is disabled.

#define EN_AMU_PAGE                     1       ///< The page in a UVP where the EN_AMU bits are stored
#define EN_AMU1_COL_ADDR                107     ///< The column address where the EN_AMU1 bit is stored         
#define EN_AMU1_MASK                    0x80    ///< The mask indicating the bit for EN_AMU1
#define EN_AMU1_OFFSET                  7       ///< The mask indicating the location for EN_AMU1 within the byte
#define EN_AMU1_BYTE_OFFSET             24      ///< The offset to shift the highest byte in the 32 bit section of memory for EN_AMU1
#define EN_AMU234_COL_ADDR              108     ///< The column address where EN_AMU2, EN_AMU3, and EN_AMU4 are stored
#define EN_AMU234_MASK                  0x07    ///< The mask indicating the bits for EN_AMU2, EN_AMU3, and EN_AMU4
#define EN_AMU234_BYTE_MASK             0xff    ///< The mask for lowest in the 32 bit section of memory for EN_AMU2, EN_AMU3, and EN_AMU4
#define AMU234_MASK                     0xE     ///< The mask for AMU 2,3, and 4 and in #AmuMask_t
#define AMU1_MASK                       0x1     ///< The mask for AMU 1 in #AmuMask_t

#define TEMPADCEN_ON                    1       ///< Indicates the temperature ADC is on.
#define TEMPADCEN_OFF                   0       ///< Indicates the temperature ADC is off.
extern UART_HandleTypeDef huart3;

static SemaphoreHandle_t gUvpRecursiveMutex;      ///< A recursive mutex used to make this driver thread safe
static StaticSemaphore_t gUvpRecursiveMutexBuff;  ///< A buffer holding the information about #gUvpRecursiveMutex
static int16_t uvpTempCache[TOTAL_UVPS];          ///< A cache to hold the last read temperatures from the UVP
static uint8_t gAmu1Byte;                          ///< The full byte value of the byte containing the AMU1 enable bit
static uint8_t gAmu234Byte;                        ///< The full byte value of the byte containing the AMU2, AMU3, and AMU4 enable bits.
static CotaError_t uvpInitReadBackTest(AmbNum_t ambNum, UvpNum_t uvpNum);
static CotaError_t uvpTestReg(UvpReg_t reg, uint32_t data, AmbNum_t ambNum, UvpNum_t uvpNum);
static CotaError_t TestParallelRead();
/**
 * @brief Intializes the mutex used to keep the UVP driver thread safe
 */
void InitUvpDriver(void)
{
    gUvpRecursiveMutex = xSemaphoreCreateRecursiveMutexStatic( &gUvpRecursiveMutexBuff);
    while (gUvpRecursiveMutex == NULL);  
}


/**
 * @brief Writes the page index number to UVP's on a particular AMB.
 * 
 * @param pageIndex   The number of the page that subsequent UVP reads and writes will be referring to.
 * @param ambNum      The zero based number of the AMB containing the UVP's to write to.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
static CotaError_t uvpWritePage(uint8_t pageIndex, AmbNum_t ambNum)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    uint8_t inBuff[UVP_PAGE_BUF_SIZE];
    uint8_t outBuff[UVP_PAGE_BUF_SIZE];
    uint32_t ambSelMask = NUM_TO_MASK(ambNum);
      
    AmbSpiEnable(ambSelMask);
    
    outBuff[0] = UVP_WRITE_PAGE_INDEX;
    outBuff[1] = pageIndex;

    retVal = SpiTransfer(AmbGetSpiHandle(ambNum), outBuff, inBuff, UVP_PAGE_BUF_SIZE);
    
    AmbSpiEnable(AMB_DISABLE_ALL);
    
    return retVal;
}

/**
 * @brief Reads a 32 bit chunck of UVP memory
 *
 * @param pageIndex   The number of the page that subsequent uvp read and writes will be referring to.
 * @param colAddr     The column address of to read the 32 bit chunck from
 * @param pData       Point to 32 bit memory to receive the chunk
 * @param ambNum      The number of the AMB's containing the UVP's to read from.
 * @param uvpNum      The number of the UVP to read the data from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
static CotaError_t uvpRegisterRead32(int32_t pageIndex, uint16_t colAddr, uint32_t* pData, AmbNum_t ambNum, UvpNum_t uvpNum)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    uint8_t inBuff[UVP_32BIT_CHUNK_BUF_SIZE];
    uint8_t outBuff[UVP_32BIT_CHUNK_BUF_SIZE];
    uint32_t ambSelMask = NUM_TO_MASK(ambNum);

    retVal = AmbSelectUvp(uvpNum);

    retVal = (retVal == COTA_ERROR_NONE) ? uvpWritePage(pageIndex, ambNum) : retVal;

    if (retVal == COTA_ERROR_NONE)
    {
        memset(inBuff, 0, UVP_32BIT_CHUNK_BUF_SIZE);
        memset(outBuff, 0, UVP_32BIT_CHUNK_BUF_SIZE);
        outBuff[0] = (colAddr | UVP_READ_MASK);

        AmbSpiEnable(ambSelMask);

        retVal = SpiTransfer(AmbGetSpiHandle(ambNum), outBuff, inBuff, UVP_32BIT_CHUNK_BUF_SIZE);

        if (retVal == COTA_ERROR_NONE)
        {
            *pData = inBuff[1] | (inBuff[2] << 8) | (inBuff[3] << 16) | (inBuff[4] << 24);
        }
    }

    AmbSpiEnable(AMB_DISABLE_ALL);
    
    return retVal;
}

/**
 * @brief Reads a register from a UVP
 *
 * @param reg           A structure of type #UvpReg_t containing the location of the register in UVP memory.
 * @param pData         Pointer to 32 bit memory to receive the register value
 * @param ambNum        The number of the AMB containing the UVP's to read from.
 * @param uvpNum        The number of the UVP to read the data from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t UvpRegisterRead(UvpReg_t reg, uint32_t* pData, AmbNum_t ambNum, UvpNum_t uvpNum)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    uint32_t regMask = 0;
    
    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);

    regMask = GEN_SOLID_MASK(reg.size);
    
    retVal = uvpRegisterRead32(reg.pageIndex, reg.colAddr, pData, ambNum, uvpNum);
    
    *pData = (*pData >> reg.offset) & regMask;
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);    

    return POST_COTA_ERROR(retVal);
}

/**
 * @brief Writes a byte to a column address for a given UVP on a given AMB
 *
 * @param data      The byte to write
 * @param colAddr   The column address to write to
 * @param ambNum    The number of the AMB containing the UVP to write to.
 * @param uvpNum    The number of the UVP to write to
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */    
static CotaError_t uvpRegisterWrite8(uint8_t data, uint16_t colAddr, AmbNum_t ambNum, UvpNum_t uvpNum)
{
    CotaError_t err = COTA_ERROR_NONE;
    uint8_t outBuff[2];
    uint8_t inBuff[2];
    
    err = AmbSelectUvp(uvpNum);
    
    if (err == COTA_ERROR_NONE)
    {
        AmbSpiEnable(NUM_TO_MASK(ambNum));   
        outBuff[0] = colAddr;
        outBuff[1] = data;
        err = SpiTransfer(AmbGetSpiHandle(ambNum), outBuff, inBuff, sizeof(outBuff));  
    }
    
    return err;
}
  
/**
 * @brief Reads a register from a UVP
 *
 * @param reg         A structure of type #UvpReg_t containing the location of the register in UVP memory.
 * @param data        The value to write to the register
 * @param ambNum      The zero based number of the AMB containing the UVP's to read from.
 * @param uvpNum      The zero based number of the UVP to read the data from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t UvpRegisterWrite(UvpReg_t reg, uint32_t data, AmbNum_t ambNum, UvpNum_t uvpNum)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    uint32_t readData = 0;
    uint32_t mask = 0;
    uint8_t inBuff[UVP_32BIT_CHUNK_BUF_SIZE];
    uint8_t outBuff[UVP_32BIT_CHUNK_BUF_SIZE];
    uint32_t ambSelMask = NUM_TO_MASK(ambNum);

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);

    mask = GEN_SOLID_MASK(reg.size);
    
    if (IS_NUMBER_IN_MASK(data, mask))
    {
        data = data << reg.offset;
        mask = mask << reg.offset;

        retVal = uvpRegisterRead32(reg.pageIndex, reg.colAddr, &readData, ambNum, uvpNum);

        if (retVal == COTA_ERROR_NONE)
        {
            readData = readData & (~mask);
            data = (data & mask) | readData;

            retVal = uvpWritePage(reg.pageIndex, ambNum);  

            if (retVal == COTA_ERROR_NONE)
            {
                memset(outBuff, 0, UVP_32BIT_CHUNK_BUF_SIZE);
                outBuff[0] = reg.colAddr;
                outBuff[1] = (data & 0xff);
                outBuff[2] = ((data >> 8) & 0xff);
                outBuff[3] = ((data >> 16) & 0xff);
                outBuff[4] = ((data >> 24) & 0xff);

                AmbSpiEnable(ambSelMask);

                retVal = SpiTransfer(AmbGetSpiHandle(ambNum), outBuff, inBuff, UVP_32BIT_CHUNK_BUF_SIZE);
            }
        }
        AmbSpiEnable(AMB_DISABLE_ALL);
    }
    else
    {
        retVal = COTA_ERROR_UVP_REG_VALUE_NOT_IN_MASK;
    }
    
    AmbSpiEnable(AMB_DISABLE_ALL);
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);   
    
    return POST_COTA_ERROR(retVal);
}

/**
 * @brief Reads a register from many UVP's and puts them in an array.
 *
 * @param pReg        A structure of type #UvpReg_t containing the location of the register in UVP memory (Pass a define from uvpDriver.h)
 * @param pData       An array of uint32_t to receive the values. The location of values in the array are determined by the #UVP_INDEX macro.
 * @param numValues   The number of uint32_t's in the array.
 * @param ambMask     A mask indicating the AMB's where the targer UVP's reside
 * @param uvpMask     A mask indicating which UVP's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t ReadArrayUvps(UvpReg_t pReg, uint32_t* pData, uint16_t numValues, AmbMask_t ambMask, UvpMask_t uvpMask)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    uint16_t ambNum;
    uint16_t uvpNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
    {    
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
        {    
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            if (UVP_INDEX(ambNum, uvpNum) > numValues)
            {
                continue;
            }

            retVal = UvpRegisterRead(pReg, &pData[UVP_INDEX(ambNum, uvpNum)], ambNum, uvpNum);
        }
    }
  
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return POST_COTA_ERROR(retVal);
}

/**
 * @brief This function will retrieve chunks of UVP data from one UVP on multiple AMB's.  
 *        The routine has been optimized to read UVP data four at time, one from each AMB
 * 
 * @param pData     A byte array to receive the memory from the UVP.
 *                  Its size should be MAX_AMB_NUM times the value of the size parameter.
 *                  Data from each amb is stored as follows:
 *                  AMB0 starts at pData[0],  AMB1 start at pData[size], AMB2 start at pData[2*size], etc.
 * @param size      The size of the data to receive from each UVP. Maximum 8.
 * @param colAddr   The column address where memory to read is located in the UVP
 * @param ambMask   A mask indicating which AMB's to read from
 * @param uvpNum    The number of the UVP to read from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t readSimulUvps(uint8_t* pData, uint8_t size, uint16_t colAddr, AmbMask_t ambMask, UvpNum_t uvpNum)
{
    CotaError_t retVal = COTA_ERROR_UVP_SIMUL_READ_TOO_LARGE;
    uint8_t inBuff[UVP_64BIT_CHUNK_BUF_SIZE*MAX_NUM_AMB];
    uint8_t outBuff[UVP_64BIT_CHUNK_BUF_SIZE];
    uint8_t ii;
 
    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    if (size < UVP_64BIT_CHUNK_BUF_SIZE)
    {       
        //Set the address pins so we read from the correct IVP
        AmbSelectUvp(uvpNum);

        //Clear local buffers
        memset(inBuff, 0, UVP_64BIT_CHUNK_BUF_SIZE*MAX_NUM_AMB);
        memset(outBuff, 0, UVP_64BIT_CHUNK_BUF_SIZE);
        outBuff[0] = (colAddr | UVP_READ_MASK);

        //Select the SPI port chip we want to send to.
        AmbSpiEnable(ambMask);
          
        //Initiate a simultaneous tranfer over all buses selected in ambMask, we must use size + 1 to make room for the address byte.
        retVal = SpiSimulTransfer(outBuff, inBuff, size + 1, ambMask);

        if (retVal == COTA_ERROR_NONE)
        {
            //Since the results are spaced out in the outBuff at size +1 length,
            //we need to copy them individually into pData which is space at size length.
            for (ii = 0; ii < MAX_NUM_AMB; ii++)
            {
                memcpy(&pData[ii*size], &inBuff[ii*(size+1)+1], size);
            }
        }

        //Make sure the SPI port are no longer selected.
        AmbSpiEnable(AMB_DISABLE_ALL);
    }
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return retVal;
}

/**
 * @brief This function will broadcast a byte to all UVP's on selected AMB's
 * 
 * @param colAddr   The column address where the memory to read is located in the UVP
 * @param val       The byte value to broadcast
 * @param ambMask   A mask indicating which AMB's to broadcast to
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
static CotaError_t broadcastByte(uint8_t colAddr, uint8_t val, AmbMask_t ambMask)
{
    uint8_t outBuff[UVP_PAGE_BUF_SIZE];
    CotaError_t retVal = COTA_ERROR_NONE;  

    AmbSelectUvp(ALL_UVP_NUM);
    AmbSpiEnable(ambMask);

    outBuff[0] = colAddr;
    outBuff[1] = val;  

    retVal = SpiSimulTransfer(outBuff, NULL, UVP_PAGE_BUF_SIZE, ambMask);

    AmbSpiEnable(AMB_DISABLE_ALL);

    return retVal;
}

/**
 * @brief This function will broadcast a page index to all UVP's on selected AMB's
 * 
 * @param pageIndex The page index where the memory to read is located in the UVP
 * @param ambMask   A mask indicating which AMB's to broadcast to
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
static CotaError_t broadcastPageIndex(uint8_t pageIndex, AmbMask_t ambMask)
{
    return  broadcastByte(UVP_WRITE_PAGE_INDEX, pageIndex, ambMask);
}


/**
 * @brief This function will retrieve one 8 bit chunk of UVP data from multiple UVP's on multiple AMB's.  
 *        The routine has been optimized to read UVP data four at time, one for each AMB
 * 
 *        @note This function does set the page index.
 * 
 * @param pData     An array of 8 bit unsigned integers to receive the data
 * @param numValues The number of values in the array.  Recommend MAX_NUM_AMB*UVP_PER_AMB
 * @param colAddr   The column address where memory to read is located in the UVP
 * @param ambMask   A mask indicating which AMB's to read from
 * @param uvpMask   A mask indicating which UVP's to read from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t ReadArrayUvps8(uint8_t* pData, uint16_t numValues, uint16_t colAddr, AmbMask_t ambMask, UvpMask_t uvpMask) 
{
    CotaError_t retVal = COTA_ERROR_NONE;  
    uint8_t readBuff[sizeof(uint8_t)*MAX_NUM_AMB];
    UvpNum_t uvpNum;
    AmbNum_t ambNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);

    // Loop through every UVP
    for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
    {      
        //Ignore UVP's not selected.
        if (!BIT_IN_MASK(uvpNum, uvpMask))
        {
            continue;
        }

        //Read the selected UVP on the selected AMB's
        retVal = readSimulUvps(readBuff, sizeof(uint8_t), colAddr, ambMask, uvpNum);

        //We need to grab the data from each AMB and put it in the appropriate locaation.
        for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
        {
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }
            if (UVP_INDEX(ambNum, uvpNum) > numValues)
            {
                continue;
            }
            pData[UVP_INDEX(ambNum, uvpNum)] = readBuff[ambNum];
        }
    }

    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return POST_COTA_ERROR(retVal);  
}


/**
 * @brief This function will retrieve 32 bit chunks of UVP data from multiple UVP's on multiple AMB's.  
 *        The routine has been optimized to read UVP data four at time, one for each AMB
 * 
 * @param pData     An array of 32 bit unsigned integers to receive the data
 * @param numValues The number of values in the array.  Recommend MAX_NUM_AMB*UVP_PER_AMB
 * @param pageIndex The page index where the memory to read is located in the UVP
 * @param colAddr   The column address where memory to read is located in the UVP
 * @param ambMask   A mask indicating which AMB's to read from
 * @param uvpMask   A mask indicating which UVP's to read from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t ReadArrayUvps32(uint32_t* pData, uint16_t numValues, uint8_t pageIndex, uint16_t colAddr, AmbMask_t ambMask, UvpMask_t uvpMask) 
{
    CotaError_t retVal = COTA_ERROR_NONE;  
    uint8_t readBuff[sizeof(uint32_t)*MAX_NUM_AMB];
    UvpNum_t uvpNum;
    AmbNum_t ambNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    // Broadcast page index to all UVP's
    retVal = broadcastPageIndex(pageIndex, ambMask);

    // Loop through every UVP
    for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
    {      
        //Ignore UVP's not selected.
        if (!BIT_IN_MASK(uvpNum, uvpMask))
        {
            continue;
        }

        //Read the selected UVP on the selected AMB's
        retVal = readSimulUvps(readBuff, sizeof(uint32_t), colAddr, ambMask, uvpNum);

        //We need to grab the data from each AMB and put it in the appropriate locaation.
        for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
        {
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }
            if (UVP_INDEX(ambNum, uvpNum) > numValues)
            {
                continue;
            }
            pData[UVP_INDEX(ambNum, uvpNum)] = *(uint32_t*)&readBuff[ambNum*sizeof(uint32_t)];
        }
    }

    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return POST_COTA_ERROR(retVal);  
}

/**
 * @brief This function will retrieve 64 bit chunks of UVP data from multiple UVP's on multiple AMB's.  
 *        The routine has been optimized to read UVP data four at time, one for each AMB
 * 
 * @param pData     An array of 64 bit unsigned integers to receive the data
 * @param numValues The number of values in the array.  Recommend MAX_NUM_AMB*MAX_NUM_UVP
 * @param pageIndex The page index where the memory to read is located in the UVP
 * @param colAddr   The column address where memory to read is located in the UVP
 * @param ambMask   A mask indicating which AMB's to read from
 * @param uvpMask   A mask indicating which UVP's to read from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t ReadArrayUvps64(uint64_t* pData, uint16_t numValues, uint8_t pageIndex, uint16_t colAddr, AmbMask_t ambMask, UvpMask_t uvpMask) 
{
    CotaError_t retVal =COTA_ERROR_NONE;  
    uint8_t readBuff[sizeof(uint64_t)*MAX_NUM_AMB];
    UvpNum_t uvpNum;
    AmbNum_t ambNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    // Broadcast page index to all UVP's
    retVal = broadcastPageIndex(pageIndex, ambMask);

    // Loop through every UVP  
    for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
    {     
        //Ignore UVP's not selected.
        if (!BIT_IN_MASK(uvpNum, uvpMask))
        {
            continue;
        }

        //Read the selected UVP on the selected AMB's    
        retVal = readSimulUvps(readBuff, sizeof(uint64_t), colAddr, ambMask, uvpNum);

        //We need to grab the data from each AMB and put it in the appropiate locatation.    
        for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
        {
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }
            if (UVP_INDEX(ambNum, uvpNum) > numValues)
            {
                continue;
            }
            pData[UVP_INDEX(ambNum, uvpNum)] = *(uint64_t*)&readBuff[ambNum*sizeof(uint64_t)];
        }
    }
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return POST_COTA_ERROR(retVal);  
}

/**
 * @brief Write a register to all UVP's
 *       
 * @note This is not a broadcast operation, it writes to each UVP one at a time.
 *
 * @param pReg        A structure of type #UvpReg_t containing the location of the register in UVP memory (Pass a define from uvpDriver.h)
 * @param data        The value to write to the register
 * @param ambMask     A mask indicating the AMB's where the target UVP's reside
 * @param uvpMask     A mask indicating which UVP's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t WriteManyUvps(UvpReg_t pReg, uint32_t data, AmbMask_t ambMask, UvpMask_t uvpMask)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    AmbNum_t ambNum;
    UvpNum_t uvpNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
    {    
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
        {      
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            retVal = UvpRegisterWrite(pReg, data, ambNum, uvpNum);
        }
    }

    vTaskDelay(pdMS_TO_TICKS(10));
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return POST_COTA_ERROR(retVal);
}

/**
 * @brief Write an array of register values to many UVP's.
 *
 * @param pReg        A structure of type #UvpReg_t containing the location of the register in UVP memory (Pass a define from uvpDriver.h)
 * @param pData       An array of uint32_t of register values. The location of values in the array are determined by the #UVP_INDEX macro.
 * @param numValues   The number of uint32_t's in the array.
 * @param ambMask     A mask indicating the AMB's where the target UVP's reside
 * @param uvpMask     A mask indicating which UVP's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t WriteArrayUvps(UvpReg_t pReg, uint32_t* pData, uint32_t numValues, AmbMask_t ambMask, UvpMask_t uvpMask)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    uint16_t ambNum;
    uint16_t uvpNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
        {      
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            if (UVP_INDEX(ambNum, uvpNum) > numValues)
            {
                continue;
            }
            retVal = UvpRegisterWrite(pReg, pData[UVP_INDEX(ambNum, uvpNum)], ambNum, uvpNum);
        }
    }
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);    
  
    return POST_COTA_ERROR(retVal);
}

/**
 * @brief Enable or disable AMUs based on their mask.
 *
 * @param ambNum  Number indicating which AMB the UVP  is on.
 * @param uvpNum  Number of UVP to enable/disable AMU's on.
 * @param amuMask Indicates which AMUs should be enabled and which should be disabled.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t UvpAmuEnable(AmbNum_t ambNum, UvpNum_t uvpNum, AmuMask_t amuMask)
{
    int32_t amuNum;
    CotaError_t ret = COTA_ERROR_NONE;
    UvpReg_t enAmu[MAX_NUM_AMU] = {EN_AMU1, EN_AMU2, EN_AMU3, EN_AMU4};

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (amuNum = 0; (amuNum < MAX_NUM_AMU) && (ret == COTA_ERROR_NONE); amuNum++)
    {
        if (BIT_IN_MASK(amuNum, amuMask))
        {
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(enAmu[amuNum], AMU_ENABLED, ambNum, uvpNum) : ret;
        }
        else
        {
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(enAmu[amuNum], AMU_DISABLED, ambNum, uvpNum) : ret;
        }
    }
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);   
    
    return ret;
}

/**
 * @brief Enable or disable AMUs based on their mask for many UVPS'
 *
 * @param ambMask A mask indicating which AMB's to enable AMU's on
 * @param uvpMask A mask indicating which UVP's to enable AMU's on
 * @param amuMask Indicates which AMUs should be enabled and which should be disabled.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t UvpManyAmuEnable(AmbMask_t ambMask, UvpMask_t uvpMask, AmuMask_t amuMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
    AmbNum_t ambNum; 
    UvpNum_t uvpNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {      
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            ret = UvpAmuEnable(ambNum, uvpNum, amuMask);
        }
    }

    xSemaphoreGiveRecursive(gUvpRecursiveMutex);   
    
    return ret;
}

/**
 * @brief Reads the UVP temperature in units of centiCelsius into an array of int16_t
 * 
 * @param pTemp     The int16_t temperature array that receives the values
 * @param numValues Number of floating point values in the array
 * @param ambMask   A mask indicating which AMB's to read the UVP temperatures from
 * @param uvpMask   A mask indicating which UVP's to read temperatures from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t  
 */
CotaError_t UvpGetTemperatureArray(int16_t* pTemp, uint16_t numValues, AmbMask_t ambMask, UvpMask_t uvpMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint32_t digTemps[TOTAL_UVPS];
    AmbNum_t ambNum;
    UvpNum_t uvpNum;
    int16_t digTemp;
    uint32_t temp;
    uint32_t mask = GEN_SOLID_MASK(TEMPADC_OUT.size);
    
    //Enable the temperature ADC in the UVP's
    ret =  WriteManyUvps(TEMPADCEN, TEMPADCEN_ON, ambMask, uvpMask);
    
    //Let's read all the 32 bit chunks containing the temperature into an array
    ret = (ret == COTA_ERROR_NONE) ? ReadArrayUvps32(digTemps, TOTAL_UVPS, TEMPADC_OUT.pageIndex, TEMPADC_OUT.colAddr, ambMask, uvpMask) : ret; 

    if (ret == COTA_ERROR_NONE)
    {
        //Now we loop through the array and convert the 32 bit chunks into actual temperatures in degrees
        for (ambNum = 0; ambNum < MAX_NUM_AMB; ambNum++)
        {
            if (!BIT_IN_MASK(ambNum, ambMask))
            {
                continue;
            }
            for (uvpNum = 0; uvpNum < UVPS_PER_AMB; uvpNum++)
            {
                if (!BIT_IN_MASK(uvpNum, uvpMask))
                {
                    continue;
                }
              
                if (UVP_INDEX(ambNum, uvpNum) < numValues)
                {
                    temp =    ((digTemps[UVP_INDEX(ambNum, uvpNum)] >> TEMPADC_OUT.offset) & mask);      //Extract the temperature bits from the 32 bit chunk
                    digTemp = (int16_t)((temp & UVP_TEMP_SIGN_BIT) ? (temp | UVP_TEMP_SIGN_EXT) : temp);  //Convert to a signed 16 bit value
                    pTemp[UVP_INDEX(ambNum, uvpNum)] = (int16_t)(((UVP_TEMP_SLOPE_NUM * ((int32_t)digTemp))/UVP_TEMP_SLOPE_DEN) + UVP_TEMP_OFFSET);        //Convert to a value in centidegrees C
                }
            }
        }
    }
    //always attempt to disable the ADC so that tranmission spectrum is not affected
    WriteManyUvps(TEMPADCEN, TEMPADCEN_OFF, ambMask, uvpMask); 
    
    return ret;
}

/**
 *  @brief This will cause all the UVP temperatures to read and cached in memory.
 *         It will also calculate that maximum delta since the last cache
 *         if an argument is specified
 *
 * @param  maxDelta  A pointer to a int16_t to receive the maximum absolute temperature change since the last cache
 *                   If NULL, this calculation is not done.
 *
 * @param  updateAlways  If true, the update will be done, if false, the update will only be done if the last update was done more than  #FAN_CHECK_PERIOD_MS ago
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t  
 */
CotaError_t UpdateUvpTemperatureCache(int16_t* maxDelta, bool updateAlways)
{
    CotaError_t ret = COTA_ERROR_NONE;
    int16_t uvpTemps[TOTAL_UVPS];
    AmbNum_t ambNum;
    UvpNum_t uvpNum;
    static TickType_t lastTickCount = 0;
    int16_t del;
    
    if (updateAlways || ((xTaskGetTickCount() - lastTickCount) > pdMS_TO_TICKS(FAN_CHECK_PERIOD_MS)))
    {
        ret = UvpGetTemperatureArray(uvpTemps, TOTAL_UVPS, AmbGetValidAmbs(), ALL_UVP_MASK);

        if (ret == COTA_ERROR_NONE)
        {
//jm          *maxDelta = 2;    //jm  
          if (maxDelta)
            {
                *maxDelta = 0;
                //Let's determine that maximum absolute change since the last reading
                for (ambNum = 0; ambNum < MAX_NUM_AMB; ambNum++)
                {
                    if (!BIT_IN_MASK(ambNum, AmbGetValidAmbs()))
                    {
                        continue;
                    }

                    for (uvpNum = 0; uvpNum < UVPS_PER_AMB; uvpNum++)
                    {
                        del = abs(uvpTemps[UVP_INDEX(ambNum, uvpNum)] - uvpTempCache[UVP_INDEX(ambNum, uvpNum)]);
                        if (del > *maxDelta) 
                        {
                            *maxDelta = del;
                        }
                    }
                }
            }
          
            //Let's update the cache
            memcpy(uvpTempCache, uvpTemps, sizeof(uvpTempCache));      
        }
        
        lastTickCount = xTaskGetTickCount();
    }

    return ret;
}


/**
 * @brief Gets the average UVP temperature on each AMB using the last read uvp temperature cache
 * 
 * @param uvpAvgTemps A int16_t point array that receives the average temperatures in centidegree C
 * @param numAvgTemps The number of floating point values in the array.
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t 
 */
CotaError_t GetAverageUvpTempsFromCache(int16_t* uvpAvgTemps, uint8_t numAvgTemps)
{
    CotaError_t ret = COTA_ERROR_NONE;
    int32_t tempAcc;
    AmbNum_t ambNum;
    UvpNum_t uvpNum;
    
    if (ret == COTA_ERROR_NONE)
    {
        for (ambNum = 0; ambNum < MAX_NUM_AMB; ambNum++)
        {
            if (!BIT_IN_MASK(ambNum, AmbGetValidAmbs()))
            {
                continue;
            }
            
            tempAcc = 0;
            //Average UVP temps for this AMB
            for (uvpNum = 0; uvpNum < UVPS_PER_AMB; uvpNum++)
            {
                tempAcc += uvpTempCache[UVP_INDEX(ambNum, uvpNum)];
            }
            uvpAvgTemps[ambNum] = (int16_t)(tempAcc/UVPS_PER_AMB);
        }
    }
    return ret;
}

/**
 *  @brief Retrieves the RSSI values from UVP's on the transmitter
 *  
 *  @param rssiData A three-dimenional array holding the rssiData values.
 *  @param ambMask A mask determining which AMB's to read from
 *  @param uvpMask A mask determining whic UVP's to read from
 *
 *  @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t 
 */
CotaError_t GetRssiValues(uint16_t rssiData[MAX_NUM_AMB][UVPS_PER_AMB][MAX_NUM_AMU], AmbMask_t ambMask, UvpMask_t uvpMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint64_t data[MAX_NUM_AMB];
    uint32_t regMask;    
    int32_t amb, uvp;
  
    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    memset(data, 0, sizeof(data));
    //The RSSI register all live in the same page, so let's make sure all the UVP's are set to that page.
    ret = broadcastPageIndex(RSSi_AMU1.pageIndex, ambMask);
    
    //Iterate throught the UVP's.
    for (uvp = 0; uvp < UVPS_PER_AMB && (ret == COTA_ERROR_NONE); uvp++)
    {
        if (!BIT_IN_MASK(uvp, uvpMask))
        {
            continue;
        }
        
        //All four RSSI registers reside in a 64 bit chunk of UVP memory.
        //Here we read all four AMU's for the designated UVP on all AMB's
        //'data' will contain 4 64 bit chuncks for each AMB, and each 64 bit chunk
        //contains the data for all 4 AMU's.
        ret = readSimulUvps((uint8_t*)data, sizeof(uint64_t), RSSi_AMU1.colAddr, ambMask, uvp);
      
        if (ret == COTA_ERROR_NONE)
        {
            //Iterate through the AMB's to get all four AMU rssi values.
            for (amb = 0; amb < MAX_NUM_AMB && (ret == COTA_ERROR_NONE); amb++)
            {
                if (!BIT_IN_MASK(amb, ambMask))
                {
                    continue;
                }
                
                regMask = GEN_SOLID_MASK(RSSi_AMU1.size);   //All four RSSI values have the same mask.
                
                rssiData[amb][uvp][0] = (data[amb] >> RSSi_AMU1.offset) & regMask;
                rssiData[amb][uvp][1] = (data[amb] >> RSSi_AMU2.offset) & regMask;
                //We have to add 32 bits to the offset below because these registers reside in the upper 32 bits of the 64 chunk.
                rssiData[amb][uvp][2] = (data[amb] >> (RSSi_AMU3.offset+(BITS_IN_BYTE*sizeof(uint32_t)))) & regMask;
                rssiData[amb][uvp][3] = (data[amb] >> (RSSi_AMU4.offset+(BITS_IN_BYTE*sizeof(uint32_t)))) & regMask;                
            }
        }
        
    }
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);
    
    return ret;
}
/**
 * @brief Scans the value  voltage control oscilator capcitor frequency selection 
 *        bits (VCOCFS) of the UVP until locking of the clock is attained.
 * @param ambNum Number indicating which AMB the UVP  is on.
 * @param uvpNum Number of UVP to enable/disable AMU's on.
 * @param cfsVal A pointer to return the value of the VCOCFS register. It trims to PLL frequency.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t LockDetect(AmbNum_t ambNum, UvpNum_t uvpNum, uint8_t * cfsVal)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint32_t i = 0;
    uint32_t data = 0;
    bool lockDetected = false;

    ret = (ret == COTA_ERROR_NONE) ?  UvpRegisterWrite(PLLLFMODE,     0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ?  UvpRegisterWrite(PLLDACEN,      1, ambNum, uvpNum) : ret;

    //Try scanning VCOCFS using a narrow voltage window.
    ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(PLLLCKRNGCTRL, 1, ambNum, uvpNum) : ret;   

    for (i = VCOCFS_VALUE_START; (i <= VCOCFS_VALUE_END) && (ret == COTA_ERROR_NONE) && (!lockDetected); i++)
    {
        ret = UvpRegisterWrite(VCOCFS, i, ambNum, uvpNum);
        vTaskDelay( pdMS_TO_TICKS(1));
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(LF_LOCKDET, &data, ambNum, uvpNum) : ret;  

        if ((data == 1) && (ret == COTA_ERROR_NONE))  //locked
        {
            if (i == VCOCFS_VALUE_START)
            {
                break;
            }

            lockDetected = true;
        }
    }

    if (!lockDetected)
    {
        //Try scanning VCOCFS using with large voltage window. 
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(PLLLCKRNGCTRL, 0, ambNum, uvpNum) : ret;

        for (i = VCOCFS_VALUE_START; (i <= VCOCFS_VALUE_END) && (ret == COTA_ERROR_NONE) && (!lockDetected); i++)
        {
            ret = UvpRegisterWrite(VCOCFS, i, ambNum, uvpNum);
            vTaskDelay( pdMS_TO_TICKS(1));
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(LF_LOCKDET, &data, ambNum, uvpNum) : ret;

            if ((data == 1) && (ret == COTA_ERROR_NONE))  //locked 
            {
                if (i == VCOCFS_VALUE_START)
                    break;

                lockDetected = true;
            }
        }
    }
    if (cfsVal != NULL)
        *cfsVal = i;

    return lockDetected ? COTA_ERROR_NONE : COTA_ERROR_LOCK_DETECT_FAILED;
}

/**
 * @brief Confirm lock detect on many UVP's
 *
 * @param ambMask A mask indicating which AMB's to attain clock lock for.
 * @param uvpMask A mask indicating which UVP's to attain clock lock for.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
static CotaError_t uvpConfirmLocks(AmbMask_t ambMask, UvpMask_t uvpMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
    AmbNum_t ambNum; 
    UvpNum_t uvpNum;

    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {      
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            ret = LockDetect(ambNum, uvpNum, NULL);
        }
    }

    return ret;
}

/**
 * @brief   In rssi voting, AMU's need to be diabled.  Unfortunately, the bits to disable
 *          or enable AMU's are found adjacent to non-zero registers in the UVP.  In order,
 *          to preserve these adjacent registers, we must save off the full byte where EN_AMU1 is located,
 *          and the full byte where EN_AMU2, EN_AMU3, and EN_AMU4 are located
 *          Testing has revealed that these bytes are the same for every UVP and 
 *          add do not change over time other than when we enable or disable AMU's
 * @param ambMask A mask indicating which AMB's to initialize UVP's for
 * @param uvpMask A mask indicating which UVP's to initialize on the AMB's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */ 
static CotaError_t cacheEnableAmuBytes(AmbMask_t ambMask, UvpMask_t uvpMask)
{ 
    AmbNum_t amb;
    UvpNum_t uvp;
    bool found = false;  //Indicates we found a valid UVP to read from.
    CotaError_t ret = COTA_ERROR_NONE;
    uint32_t data;
    
    //First find a valid amb and uvp
    for (amb = 0; (amb < MAX_NUM_AMB) && (!found); amb++)
    {
        if (BIT_IN_MASK(amb, ambMask))
        {
            for (uvp = 0; (uvp < UVPS_PER_AMB) && (!found); uvp++)
            {
                if (BIT_IN_MASK(uvp, uvpMask))
                {
                    ret = uvpRegisterRead32(EN_AMU_PAGE, EN_AMU1.colAddr, &data, amb, uvp);
                    
                    gAmu1Byte = (data >> EN_AMU1_BYTE_OFFSET);
                    
                    ret = (ret == COTA_ERROR_NONE) ? uvpRegisterRead32(EN_AMU_PAGE, EN_AMU2.colAddr, &data, amb, uvp): ret;
                    
                    gAmu234Byte = (data & EN_AMU234_BYTE_MASK);
                    found = true;
                }               
            }             
        }
    }
  
    return ret;  
}

/**
 * @brief Initialize the specified UVP's on the specified AMB's.
 *        This is designed for SWS1411 UVPv2 Variant 1
 *
 * @param ambMask A mask indicating which AMB's to initialize UVP's for
 * @param uvpMask A mask indicating which UVP's to initialize on the AMB's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t UvpInit(AmbMask_t ambMask, UvpMask_t uvpMask)
{
    static CotaError_t ret = COTA_ERROR_NONE;
    uint16_t ambNum;
    uint16_t uvpNum;
    
    /*
      This register-value list was pulled directly from the Venus code base and is
      not well-documented since migrating to this iteration of the Orion code base.
      We will need to revisit this initialization sequence if for no other reason
      than properly documenting this sequence.

      TODO Determine this initialization sequence is correct and comment the major steps
      properly.
     */
    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);

    //This test should pass for all AMB configs.  It's essential because it writes
    //a different value to every UVP and reads them back in parallel.
    TestParallelRead();
    
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LFR3CTRL,    1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LFR1CTRL,    1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LFC1CTRL,    4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LFC2CTRL,    1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(CPLKEN,      1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(CPIVAL,      4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(QOB_CHECK_DISABLE,     0, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(NDIVINT,     8, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(EN_AMU1,       1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(EN_AMU2,       1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(EN_AMU3,       1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(EN_AMU4,       1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(REFOSCAMPMODE, 0, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123PDRBST,  3, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PDRBST,    3, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123PDIBST,  0, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PDIBST,    0, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123AAFGAIN, 2, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4AAFGAIN,   2, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCPSEN,1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCPSEN,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCCOMPSELEN, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCCOMPSELEN,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCCOMPENCNT, 4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCCOMPENCNT,   4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCEN_TEST,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCEN_TEST,     1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCCMBUFFEN_TEST, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCCMBUFFEN_TEST,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LOWIF_I_SYMBLI,   49, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LOWIF_I_SYMBLQ,   76, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LOWIF_Q_SYMBLI,  196, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LOWIF_Q_SYMBLQ,   49, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_I_GAIN_AMU1,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_I_GAIN_AMU2,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_I_GAIN_AMU3,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_I_GAIN_AMU4,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_Q_GAIN_AMU1,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_Q_GAIN_AMU2,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_Q_GAIN_AMU3,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_Q_GAIN_AMU4,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU1PAHIGHPOWER,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU2PAHIGHPOWER,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU3PAHIGHPOWER,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PAHIGHPOWER,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ENCODE_SIZE,      32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(RSSI_GAIN_SCALE,  1024, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(RSSI_LOWTHRESH,   0,    ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(RSSI_HIGHTHRESH,   4095, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PA_COUNTER_VALID,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PA_POWERON_DEL,     32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PA_SEQUENCE_DEL,    4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PA_OFFSET_DEL,      2, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHDETECT_PIN_SEL,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ARB_PHASE_SEL,      0, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(SEL_PHASE_CONJUGATE, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU_PHASE_CALIBRATE,15, ambMask, uvpMask) : ret;
    //The following registers are used to enable the thermometers on the uvp's
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LDOADC1EN_IDLE, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TEMPADC_GAIN, 32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TMPSNSCMBUFFEN, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TMPSNSCOMPENCNT, 4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TMPSNSCOMPSELEN, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TMPSNSEN, 1 , ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TEMPADCEN, TEMPADCEN_OFF, ambMask, uvpMask) : ret;   //We must disable temperature ADC until it is measured.
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LDOADC1EN_SENDPWR, 1, ambMask, uvpMask) : ret;

    //We cache the amu bytes, but we also test the disable and enable the AMU's
    //to test them.  If there a problem with this process, the test readback test will then catch it.
    ret = (ret == COTA_ERROR_NONE) ? cacheEnableAmuBytes(ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? UvpDisableAmus(ambMask) : ret;    
    ret = (ret == COTA_ERROR_NONE) ? UvpEnableAmus(ambMask) : ret;
       
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            ret = (ret == COTA_ERROR_NONE) ? uvpInitReadBackTest(ambNum, uvpNum) : ret;
            if (ret != COTA_ERROR_NONE)
            {
                PostPrintf("READBACK TEST FAILED AMB %d UVP %d\r\n", ambNum, uvpNum);
            }
        }
    }

    ret = (ret == COTA_ERROR_NONE) ? uvpConfirmLocks(ambMask, uvpMask) : ret;
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);

    return ret;
}

/*
 * @brief Reads a register to see if it is the correct value
 * 
 * @param reg      A structure of type #UvpReg_t containing the location of the register in UVP memory (Pass a define from uvpDriver.h)
 * @param pData    The value to test the value read from the register against.
 * @param ambNum   Number indicating which AMB the UVP  is on.
 * @param uvpNum   Number of UVP to enable/disable AMU's on.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
static CotaError_t uvpTestReg(UvpReg_t reg, uint32_t data, AmbNum_t ambNum, UvpNum_t uvpNum)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint32_t dataRead = 0;
    uint32_t* pData = &dataRead;    
    uint32_t regMask = 0;

    regMask = GEN_SOLID_MASK(reg.size);

    ret = uvpRegisterRead32(reg.pageIndex, reg.colAddr, pData, ambNum, uvpNum);

    *pData = (*pData >> reg.offset) & regMask;

    if ((ret != COTA_ERROR_NONE) || (data != dataRead))
    {
        ret = COTA_ERROR_UVP_TEST_FAILED;
    }

    return ret;
}

/**
 * @brief A simple test of the UVP that writes a values to a register and then reads them back.
 *
 * @param ambNum Number indicating which AMB the UVP  is on.
 * @param uvpNum Number of UVP to enable/disable AMU's on.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
static CotaError_t uvpInitReadBackTest(AmbNum_t ambNum, UvpNum_t uvpNum)
{
    static CotaError_t ret = COTA_ERROR_NONE;

    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LFR3CTRL,    1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LFR1CTRL,    1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LFC1CTRL,    4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LFC2CTRL,    1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(CPLKEN,      1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(CPIVAL,      4, ambNum, uvpNum) : ret;  
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(NDIVINT,     8, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(QOB_CHECK_DISABLE,     0, ambNum, uvpNum) : ret;    
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(EN_AMU1,       1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(EN_AMU2,       1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(EN_AMU3,       1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(EN_AMU4,       1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(REFOSCAMPMODE, 0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123PDRBST,  3, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4PDRBST,    3, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123PDIBST,  0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4PDIBST,    0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123AAFGAIN, 2, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4AAFGAIN,   2, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCPSEN,1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCPSEN,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCCOMPSELEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCCOMPSELEN,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCCOMPENCNT, 4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCCOMPENCNT,   4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCEN_TEST,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCEN_TEST,     1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCCMBUFFEN_TEST, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCCMBUFFEN_TEST,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LOWIF_I_SYMBLI,   49, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LOWIF_I_SYMBLQ,   76, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LOWIF_Q_SYMBLI,  196, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LOWIF_Q_SYMBLQ,   49, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_I_GAIN_AMU1,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_I_GAIN_AMU2,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_I_GAIN_AMU3,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_I_GAIN_AMU4,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_Q_GAIN_AMU1,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_Q_GAIN_AMU2,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_Q_GAIN_AMU3,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_Q_GAIN_AMU4,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU1PAHIGHPOWER,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU2PAHIGHPOWER,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU3PAHIGHPOWER,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4PAHIGHPOWER,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ENCODE_SIZE,      32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(RSSI_GAIN_SCALE,  1024, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(RSSI_LOWTHRESH,   0,    ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(RSSI_HIGHTHRESH,   4095,    ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PA_COUNTER_VALID,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PA_POWERON_DEL,     32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PA_SEQUENCE_DEL,    4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PA_OFFSET_DEL,      2, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PHDETECT_PIN_SEL,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ARB_PHASE_SEL,      0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(SEL_PHASE_CONJUGATE, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU_PHASE_CALIBRATE,15, ambNum, uvpNum) : ret;

    //The following registers are used to enable the thermometers on the uvp's

    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LDOADC1EN_IDLE, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TEMPADC_GAIN, 32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TMPSNSCMBUFFEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TMPSNSCOMPENCNT, 4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TMPSNSCOMPSELEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TMPSNSEN, 1 , ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TEMPADCEN, TEMPADCEN_OFF, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LDOADC1EN_SENDPWR, 1, ambNum, uvpNum) : ret;

    //The following register exist next to EN_AMU* registers, so we test them as well,
    //even though we should never modify them.
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_Q_OFFST_AMU3, 0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCCLKEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PHIDIGFSEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(CODECLKEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(CLKGEN_EN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(CODECLK_PH, 1, ambNum, uvpNum) : ret;    

    return ret;
}

/**
 * @brief The goal is write to amb's a register with a different value per uvp,
 *        then read them back using parallel read to make sure they all correct.
 * 
 * @return COTA_ERROR_NONE if test passes; otherwise the test has failed
 */
static CotaError_t TestParallelRead()
{
    int32_t amb, uvp;
    CotaError_t ret = COTA_ERROR_NONE;
    uint32_t gainScale[TOTAL_UVPS];
    uint32_t readBack[TOTAL_UVPS];
    uint32_t readBackParallel[TOTAL_UVPS];

    
    //Fill the buffers with known values
    memset(gainScale, 0xff, sizeof(gainScale));
    memset(readBack, 0xff, sizeof(readBack));
    memset(readBackParallel, 0xff, sizeof(readBackParallel));
    
    //Create an array with indexed, so that no two values in the array are same
    for (amb = 0; (amb < MAX_NUM_AMB); amb++)
    {
        for (uvp = 0; (uvp < UVPS_PER_AMB); uvp++)
        {
            gainScale[UVP_INDEX(amb,uvp)] = UVP_INDEX(amb,uvp);
        }
    }
  
    //RSSI_GAIN_SCALE and ARB_PHASE_SEL live on two different pages in the uvp registers
    //so before we write to RSSI_GAIN_SCALE, we write to ARB_PHASE_SEL to change pages,
    //and thereby test we are broadcasting pages correctly on the next call.
    
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ARB_PHASE_SEL,      0, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;  
    ret = (ret == COTA_ERROR_NONE) ? WriteArrayUvps(RSSI_GAIN_SCALE, gainScale, TOTAL_UVPS, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ARB_PHASE_SEL,      0, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;      
    ret = (ret == COTA_ERROR_NONE) ? ReadArrayUvps(RSSI_GAIN_SCALE, readBack, TOTAL_UVPS, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ARB_PHASE_SEL,      0, AmbGetValidAmbs(), ALL_UVP_MASK) : ret;  
    ret = (ret == COTA_ERROR_NONE) ? ReadArrayUvps32(readBackParallel, TOTAL_UVPS, RSSI_GAIN_SCALE.pageIndex, RSSI_GAIN_SCALE.colAddr,  AmbGetValidAmbs(), ALL_UVP_MASK) : ret;
    for (amb = 0; (amb < MAX_NUM_AMB); amb++)
    {
        if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
        {
            for (uvp = 0; (uvp < UVPS_PER_AMB); uvp++)
            {
                readBackParallel[UVP_INDEX(amb,uvp)] = (readBackParallel[UVP_INDEX(amb,uvp)] >>  RSSI_GAIN_SCALE.offset) & GEN_SOLID_MASK( RSSI_GAIN_SCALE.size);
            }
        }
    }
    
    if (ret != COTA_ERROR_NONE)
    {
        POST_COTA_ERROR(ret);
        PostPrintf("Parallel read test failed because of failed uvp write or read\r\n");
    }  
    else
    {
        //For each AMB will we evaluate and post an error if something is wrong.
        for (amb = 0; (amb < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); amb++)
        {
            if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
            {
                for (uvp = 0; (uvp < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvp++)
                {
                    if ((readBack[UVP_INDEX(amb,uvp)] != gainScale[UVP_INDEX(amb,uvp)])  || 
                       (readBackParallel[UVP_INDEX(amb,uvp)] != gainScale[UVP_INDEX(amb,uvp)]))
                    {
                        PostPrintf("ERROR (amb=%d uvp=%d) Readback failed Expected %d Got %d and %d\r\n",
                                   amb,uvp, gainScale[UVP_INDEX(amb,uvp)], 
                                   readBack[UVP_INDEX(amb,uvp)],readBackParallel[UVP_INDEX(amb,uvp)] );
                        vTaskDelay( pdMS_TO_TICKS(100));
                    }
                }
            }
        }             
    }
    
    return ret;
}

/**
 * @brief Disable all AMU's for specified AMB's
 *
 * @param ambMask A mask indicating which AMB's to disable AMU's on.
 *
 * @todo We can optimize this further by doing 16 bit writes
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */ 
CotaError_t UvpDisableAmus(AmbMask_t ambMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
  
    if (ambMask)
    {
        xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
        
        ret = broadcastPageIndex(EN_AMU_PAGE, ambMask);
        
        if (ret != COTA_ERROR_NONE)
        {
            DBG1_SET(); 
            PostPrintf("UvpDisableAmus failed0\r\n");
            DBG1_RESET();
        }
         
        ret = (ret == COTA_ERROR_NONE) ? broadcastByte(EN_AMU1_COL_ADDR, gAmu1Byte & (~EN_AMU1_MASK), ambMask) : ret;

        if (ret != COTA_ERROR_NONE)
        {
            DBG1_SET();
            PostPrintf("UvpDisableAmus failed1\r\n");
            DBG1_RESET();
        } 
        
        ret = (ret == COTA_ERROR_NONE) ? broadcastByte(EN_AMU234_COL_ADDR, gAmu234Byte & (~EN_AMU234_MASK) , ambMask) : ret;
        if (ret != COTA_ERROR_NONE)
        {
            DBG1_SET();
            PostPrintf("UvpDisableAmus failed2\r\n");
            DBG1_RESET();
        }       
        xSemaphoreGiveRecursive(gUvpRecursiveMutex); 
    }

    return ret;
}

/**
 * @brief Enable all AMU's for the specified AMB's
 *
 * @param ambMask A mask indicating which AMB's to enable AMU's on.
 *
 * @todo We can optimize this further by doing 16 bit writes
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */ 
CotaError_t UvpEnableAmus(AmbMask_t ambMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
    DBG1_RESET();
    if (ambMask)
    {    
        xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
        
        ret = broadcastPageIndex(EN_AMU_PAGE, ambMask);
        
        if (ret != COTA_ERROR_NONE)
        {
            DBG1_SET();
            PostPrintf("UvpEnableAmus failed0\r\n");
            DBG1_RESET();
        }
                
        ret = (ret == COTA_ERROR_NONE) ? broadcastByte(EN_AMU1_COL_ADDR, gAmu1Byte , ambMask) : ret;
        if (ret != COTA_ERROR_NONE)
        {
            DBG1_SET();
            PostPrintf("UvpEnableAmus failed1\r\n");
            DBG1_RESET();
        }
        
        ret = (ret == COTA_ERROR_NONE) ? broadcastByte(EN_AMU234_COL_ADDR, gAmu234Byte , ambMask) : ret;
   
        if (ret != COTA_ERROR_NONE)
        {
            DBG1_SET();
            PostPrintf("UvpEnableAmus failed2\r\n");
            DBG1_RESET();
        }
        xSemaphoreGiveRecursive(gUvpRecursiveMutex); 
    }

    return ret;
}

/**
 * @brief Enable all AMU's for the specified AMB's
 *
 * @param pData      Array of mask that indicate with AMU's are enabled on each UVP
 * @param numValues  total number of values is the array (recomment TOTAL_UVPS)
 * @param ambMask    Mask indicating which AMB's to read from
 *
 * @todo We can optimize this further by doing 16 bit reads
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */ 
CotaError_t GetAmuEnableMask(AmuMask_t* pData, uint16_t numValues, AmbMask_t ambMask)
{
    uint8_t amu234[TOTAL_UVPS];
    AmbNum_t ambNum;
    UvpNum_t uvpNum;
    CotaError_t ret;
    
    //Set the 
    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    ret = broadcastPageIndex(EN_AMU_PAGE, ambMask);   
    ret = (ret == COTA_ERROR_NONE) ? ReadArrayUvps8(pData, numValues, EN_AMU1_COL_ADDR, ambMask, ALL_UVP_MASK) : ret;
    ret = (ret == COTA_ERROR_NONE) ? ReadArrayUvps8(amu234, TOTAL_UVPS, EN_AMU234_COL_ADDR, ambMask, ALL_UVP_MASK) : ret;
     
    xSemaphoreGiveRecursive(gUvpRecursiveMutex); 
    
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {
            pData[UVP_INDEX(ambNum, uvpNum)] = (pData[UVP_INDEX(ambNum, uvpNum)] & EN_AMU1_MASK) >> EN_AMU1_OFFSET;
            pData[UVP_INDEX(ambNum, uvpNum)] |= (amu234[UVP_INDEX(ambNum, uvpNum)] & EN_AMU234_MASK) << EN_AMU1.size;
        }
    }    
  
    return ret;
}

/**
 * @brief Sets the enable state AMU's for specified AMB's but only when a amu mask has changed
 *
 * @param pDataOld      Array of masks that indicate with AMU's are currently enabled on each UVP, if NULL, all AMUS are consider enabled.
 * @param pDataNew      Array of masks that indicate with AMU's are desired to be enabled on each UVP
 * @param numValues     total number of values in the array (recommend TOTAL_UVPS)
 * @param ambMask       Mask indicating which AMB's to read from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */ 
CotaError_t SetAmuEnableChanges(AmuMask_t* pDataOld, AmuMask_t* pDataNew, uint16_t numValues, AmbMask_t ambMask)
{
    AmbNum_t amb;
    UvpNum_t uvp;
    AmuMask_t oldMask;
    CotaError_t ret;
    uint8_t amu1byte;
    uint8_t amu234byte;
    
    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    //Let's put all the UVP's on the correct page
    ret = broadcastPageIndex(EN_AMU_PAGE, ambMask);   
    if (ret != COTA_ERROR_NONE)
    {
        DBG1_SET();
        PostPrintf("broadcast byte fail\r\n");
        DBG1_RESET();
    }
    
    //Loop through the AMB's
    for (amb = 0; (amb < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); amb++)
    {
        if (!BIT_IN_MASK(amb, ambMask))
        {
            continue;
        }
        
        //Loops through UVP's
        for (uvp = 0; (uvp < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvp++)
        {          
            //Find changes in enable masks between old and new data.
            if (((pDataOld == NULL) && (pDataNew[UVP_INDEX(amb, uvp)] != ALL_AMU_MASK)) ||
               ((pDataOld != NULL) && (pDataOld[UVP_INDEX(amb, uvp)] != pDataNew[UVP_INDEX(amb, uvp)])))
            {
                if (pDataOld == NULL)
                {
                    oldMask = ALL_AMU_MASK;
                }
                else
                {
                    oldMask = pDataOld[UVP_INDEX(amb, uvp)];
                }
                
                //Look for changes in the AMU1 enable, if so change EN_AMU1
                if ((oldMask & AMU1_MASK) != (pDataNew[UVP_INDEX(amb, uvp)]) & AMU1_MASK)
                {
                    if (pDataNew[UVP_INDEX(amb, uvp)] & AMU1_MASK)
                    {
                        amu1byte = gAmu1Byte | EN_AMU1_MASK;
                    }
                    else
                    {
                        amu1byte = gAmu1Byte & (~EN_AMU1_MASK);             
                    }
                    
                    ret = uvpRegisterWrite8(amu1byte, EN_AMU1_COL_ADDR, amb, uvp);
                    if (ret != COTA_ERROR_NONE)
                    {
                        DBG1_SET();
                        PostPrintf("first byte fail amb=%d uvp=%d\r\n", amb,uvp);
                        DBG1_RESET();
                    }
                }
                
                //Look for changes in AMU2,3,4.  If changed, set EN_AMU2,3,4
                if ((oldMask & AMU234_MASK) != (pDataNew[UVP_INDEX(amb, uvp)] & AMU234_MASK))
                {                                 
                    amu234byte = gAmu234Byte & (~EN_AMU234_MASK);
                    
                    amu234byte |= (pDataNew[UVP_INDEX(amb, uvp)] >> 1);
             
                    ret = (ret == COTA_ERROR_NONE) ? uvpRegisterWrite8(amu234byte, EN_AMU234_COL_ADDR, amb, uvp) : ret; 
                    if (ret != COTA_ERROR_NONE)
                    {            
                        DBG1_SET();
                        PostPrintf("second byte fail amb=%d uvp=%d\r\n", amb,uvp);
                        DBG1_RESET();
                    }
                }
            }
        }
    }  
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    return ret;
}

/**
 * @brief Dumps a UVP's memory
 * @param amb  The number of AMB where the UVP is located
 * @param uvp  The number of the UVP whose memory is to be dumped.
 * @param msg Message to fill that will be sent to the RPI.
 */
void UvpDump(AmbNum_t amb, UvpNum_t uvp, CtrlResp_t* msg)
{
    uint32_t data;
    uint8_t ii,jj;
    int len = sizeof(msg->id) +
              sizeof(msg->err) +
              sizeof(msg->uvpMemDump);

    msg->id = CTRL_UVP_MEM_DUMP_DATA;
    msg->err = COTA_ERROR_NONE;
  
    PostPrintf("Dump for AMB=%d UVP=%d\r\n", amb, uvp);
    PostPrintf("Page    col   Data\r\n", amb, uvp);
    for (ii=0; ii < NUMBER_OF_UVP_PAGES; ii++)
    {
        for (jj=0; jj < UVP_PAGE_SIZE; jj+=4)
        {
            msg->uvpMemDump.pageIndex = ii;
            msg->uvpMemDump.colAddr = jj;
            msg->err = uvpRegisterRead32(msg->uvpMemDump.pageIndex, msg->uvpMemDump.colAddr, &data, amb, uvp);
            msg->uvpMemDump.data = data;
            RpiSendMessage((uint8_t*) msg, len);
            if (msg->err == COTA_ERROR_NONE)
            {
                PostPrintf(" 0x%02x  0x%02x  0x%08x\r\n",
                    msg->uvpMemDump.pageIndex, msg->uvpMemDump.colAddr, data);
            }
            else
            {
                PostPrintf("Error %d\r\n", msg->err);
            }
            vTaskDelay( pdMS_TO_TICKS(5));
        }     
    }
}

/**
 * @brief Starts the UVP's from the point of being off.
 * @param ambMask A mask indicating which AMB's to start
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t StartUvps(AmbMask_t ambMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint16_t powerLevel;
    uint16_t actualPowerLevel;
    
    AmbEnable(AmbGetValidAmbs());
    vTaskDelay( pdMS_TO_TICKS(1000));
    ret = (ret == COTA_ERROR_NONE) ? AmbUvpEnable(AmbGetValidAmbs()) : ret;  
    vTaskDelay( pdMS_TO_TICKS(1000));
    ret = (ret == COTA_ERROR_NONE) ? UvpInit(AmbGetValidAmbs(), ALL_UVP_MASK) : ret;
    ret = (ret == COTA_ERROR_NONE) ? CfgGetParam(CFG_POWER_LEVEL, &powerLevel, sizeof(uint16_t)): ret;
    ret = (ret == COTA_ERROR_NONE) ? SetAmbPowerLevels(AmbGetValidAmbs(), powerLevel, &actualPowerLevel) : ret;
    if (ret == COTA_ERROR_NONE)
    {
        PostStringForTransmit("AMB's successfully started\r\n");
    }
    else
    {
        POST_COTA_ERROR(ret);
        PostStringForTransmit("AMB's failed to initialize\r\n");
        AmbEnable(AMB_DISABLE_ALL);
   
    } 
    return ret;
}


#ifdef UVP_TEST_ENABLED

volatile uint32_t someValue; ///< A value that gives the debugger something to show in testUvpWriteRead

/**
 * @brief A test that writes a value to the RSSI_GAIN_SCALE register and then reads it back.
 *
 * @param wdata  The value to write to the register
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
static CotaError_t testUvpWriteRead(uint32_t wdata)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint32_t data = 0;

    ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(RSSI_GAIN_SCALE, wdata, 1, 1) : ret;
    ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(RSSI_GAIN_SCALE, &data, 1, 1) : ret;

    if ((ret == COTA_ERROR_NONE) && (data == wdata))
    {
        someValue = data;
    }
    else
    {
        ret = COTA_ERROR_HAL;
        someValue = 0xffffffff;
    } 
    return ret;
}

#define TEST_PASSED "UvpTestsPassed!!\r\n"
/**
 * @brief A simple test of the UVP that writes a values to a register and then reads them back.
 */
void TestUvp(void)
{
    CotaError_t ret = COTA_ERROR_NONE;

    uint32_t tempData;
    int16_t  temp;
    uint8_t count = 0;
    uint32_t ready;
    uint32_t vcoVal = 0;
    char out[64];

    //Enable the uvp, using AMB 0
    AmbEnable(AMB0_MASK);
    AmbUvpEnable(AMB_DISABLE_ALL);  
    vTaskDelay( pdMS_TO_TICKS(1000));
    AmbUvpEnable(AMB0_MASK);  
    vTaskDelay( pdMS_TO_TICKS(1000));

    //Let's do some simple readback testing of a UVP register
    //The values passed to testUvpWriteRead are picked at random for the sake of testing.
    ret = testUvpWriteRead(0xCD);

    ret = (ret == COTA_ERROR_NONE) ? testUvpWriteRead(0x12) : ret;
    ret = (ret == COTA_ERROR_NONE) ? testUvpWriteRead(0x04) : ret;
    ret = (ret == COTA_ERROR_NONE) ? testUvpWriteRead(0x106) : ret;
  
  
    if (ret == COTA_ERROR_NONE)
    {
        //Let's now test to see if we can initialize all the necessary UVP registers, and read them back, then attain lockDetect
        ret = (ret == COTA_ERROR_NONE) ? UvpInit(AMB0_MASK, 0x1) : ret;
        ret = (ret == COTA_ERROR_NONE) ? uvpInitReadBackTest(1, 1) : ret;
        ret = (ret == COTA_ERROR_NONE) ? LockDetect(1, 1, NULL) : ret;
    
        ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(AMB0_MASK, 0x1, ALL_AMU_MASK) : ret;   
    
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, 1 , AMB0_MASK, 0x1) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(POWER_ON_PA, 1, AMB0_MASK, 0x1) : ret;

        vTaskDelay( pdMS_TO_TICKS(100));  
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(SEND_POWER_RDY, &ready, AMB0_MASK, 0x1) : ret;
        if (ready != 1)
        {
            ret = COTA_ERROR_UVP_TEST_FAILED;
        }
        //Let's print out the value of VCOCFS to see if it is reasonable.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(VCOCFS, &vcoVal, AMB0_MASK, 0x1) : ret;
        snprintf(out, 64, "VCOFS %d\r\n", vcoVal);
        CotaUartTransmit((uint8_t*)out, strlen(out),  pdMS_TO_TICKS(1000)); 
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(POWER_ON_PA, &vcoVal, AMB0_MASK, 0x1) : ret;
        snprintf(out, 64, "powerOnPA %d\r\n", vcoVal);
        CotaUartTransmit((uint8_t*)out, strlen(out),  pdMS_TO_TICKS(1000));   
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, 1 , AMB0_MASK, 0x1) : ret; 
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, 0 , AMB0_MASK, 0x1) : ret; 
    
        //Let's rotate TX across the AMU's in the UVP.
        while (ret == COTA_ERROR_NONE)
        {
            vTaskDelay( pdMS_TO_TICKS(5000));
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, 0 , AMB0_MASK, 0x1) : ret; 
            CotaUartTransmit((uint8_t*)"T", 1,  pdMS_TO_TICKS(1000));
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(TEMPADC_OUT, &tempData, AMB0_MASK, 0x1) : ret;
            temp = (int16_t)((tempData & 0x1000) ? (tempData | 0xE000) : tempData);
            snprintf(out, 64, " %f C AMU %d\r\n",(0.0155*temp + 43.55), count);
            CotaUartTransmit((uint8_t*)out, strlen(out),  pdMS_TO_TICKS(1000));
            ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(AMB0_MASK, 0x1, NUM_TO_MASK(count)) : ret;  
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, 1 , AMB0_MASK, 0x1) : ret;
            count++;
            count = count % 4;    
        }
    
    }
  
    if (ret != COTA_ERROR_NONE)
    {
        POST_COTA_ERROR(COTA_ERROR_UVP_TEST_FAILED);
    }
    else
    {
        POST_COTA_ERROR(COTA_ERROR_NONE);
        CotaUartTransmit(TEST_PASSED, strlen(TEST_PASSED), portMAX_DELAY);
    }

}
#endif
