/**
 * @file      nvm_driver.c
 *
 * @brief     This driver is responsible for storing structured data in NVM
 *
 * This driver is intended to be used with the EEPROM built into the Orion CCB, which has
 * a simple interface to write any number of bytes to any single 256-byte page.  For this
 * reason, the size of a memory slot allocated for each client is 32 bytes so that a whole
 * number of memory slots can be written to a single EEPROM memory page.
 *
 * The EERPOM chip has a special page at a separate I2C bus address.  The page is called
 * Identification Page, and this driver uses it to store system configuration, such as
 * which AMBs are installed, the board ID, various timing configuration parameters, and how
 * many client memory slots the main EEPROM memory contains.  This driver refers to this
 * page as the configuration data page.
 *
 * Every time a client memory slot is written new information to, its CRC-32 checksum
 * needs to be updated to indicate that the memory slot contains valid information.
 * The CRC-32 checksum is alway aligned to the end of the 32-byte memory slot.
 *
 * Likewise, every time the configuration data is written to the configuration data page,
 * the CRC-32 checksum must be updated to indicate the data on the page is valid.  Unlike
 * the client memory slot checksum, the configuration data checksum immediately follows
 * the configuration data and is not aligned to the end of the configuration data page.
 *
 * The configuration data page contains the system configuration accessible and modifiable
 * by external modules via API as well as information needed by this module internally, such
 * as the number of client memory slots stored in the main memory and the CRC-32 checksum
 * for the configuration data page.
 *
 *
 * @note       The first HW build had the 1-Mbit version of the chip installed, so the number
 *             of pages is limited to 512 and the memory address size is 17 bits.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "nvm_driver.h"
#include "main.h"
#include "orion_config.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include "string.h"
#include "stdbool.h"
#include "debug_off.h"
#include "cmsis_os.h"
#include "client_interface.h"
#include "crc.h"
#include "cli_task.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "client.h"

#if defined(USE_DEMO_BUILD)
    #include "Demo/demo_client_interface.h"
#elif defined(USE_FOREVER_TRACKER_BUILD)
    #include "ForeverTracker/ft_client_interface.h"
#else
    #error Build target not defined properly
#endif

#define CLIENT_SLOT_SIZE          (sizeof(NvmClientData_t))                                  ///< The size of the memory slot occupied by the registered client information in NVM, in bytes
#define CLIENT_SLOT_PAYLOAD_SIZE  (sizeof(NvmClientData_t) - CRC32_CHECKSUM_SIZE_BYTES)      ///< The size of the memory slot occupied by the registered client information in NVM excluding the 32-bit CRC checksum value, in bytes                          ///< The size of the memory alloted for storing rssi thresholds
#define RSSI_THRESH_PAYLOAD_SIZE  (sizeof(NvmRssiThreshData_t) - CRC32_CHECKSUM_SIZE_BYTES)  ///< The size of the memory alloted for storing rssi thresholds excluding the 32-bit CRC checksum value, in bytes
#define RSSI_THRESH_SIZE          (sizeof(NvmRssiThreshData_t)) 
#define CLIENT_START_ADDR         RSSI_THRESH_SIZE                                           ///< The start address in memory for the storage of clients.
#define RSSI_THRESH_START_ADDR    0                                                          ///< The start address in memory for the rssi thresholds.
#define CONFIG_PAGE_START_ADDR    0                                                          ///< The starting address of the board ID
#define CONFIG_PAGE_PAYLOAD_SIZE  ((uint8_t*)&gConfigPage.crc32 - (uint8_t*)&gConfigPage)    ///< The size of configuration page data in bytes excluding the 32-bit CRC checksum value
#define INVALID_SHORT_ID          0xFFFF                                                     ///< The value of short ID in a client configuration memory slot used as an invalid value to invalidate the whole slot
#define SLOT_NOT_FOUND            (-1)                                                       ///< A value indicating that a valid memory slot hasn't been found
#define MAX_CLIENT_COUNT          ((EEPROM_MEM_SIZE) / (CLIENT_SLOT_SIZE))                   ///< The total number of client memory slots that will fit in NVM

#define DEFAULT_SHORT_ID          1                                                          ///< Default short ID
#define DEFAULT_PRIORITY          64                                                         ///< Default priority
#define DEFAULT_VERSION           0                                                          ///< Default version
#define DEFAULT_HW_MODEL          0                                                          ///< Default HW model
#define DEFAULT_MOTION_EN         0                                                          ///< Default motion enabled flag
#define LOAD_PROG_WAIT            ((EEPROM_MEM_SIZE)/(16*CLIENT_SLOT_SIZE))                                       ///< Use to create a period in a loop that can take a while. 16 total periods.
#define CLIENT_ADDRESS_FROM_SLOT_NUM(slot) (((slot) * CLIENT_SLOT_SIZE) + CLIENT_START_ADDR) ///< Converts a slot number to an address of the EEPROM 

/** System configuration stored on the Identification Page of the EEPROM chip */
typedef PACKED struct _NvmConfigPage_t
{
    SysConfig_t  data;        ///< Configuration data stored and retrieved by external modules
    uint16_t     numClients;  ///< Number of clients stored in NVM
    uint32_t     crc32;       ///< The last field of the structure is the CRC-32 checksum for the entire page (excluding this field)
} NvmConfigPage_t;

// Ensure the structure above fits in one EEPROM page
static_assert((sizeof(NvmConfigPage_t) <= EEPROM_PAGE_SIZE), "The size of NvmConfigPage_t must not exceed the EEPROM page size");


/** @brief  Configuration stored for each client in the EEPROM chip
 *
 * This structure is chosen to be 32 bytes so that 8 of these can
 * fit in one 256-byte EEPROM page so we don't have to worry about
 * crossing page boundaries when reading/writing from/to EEPROM memory
 */
typedef PACKED struct _NvmClientData_t
{
    uint16_t        shortId;    ///< Short ID assigned to the client when it was registered
    ExtdAddr_t      longId;     ///< Long ID (IEEE 802.15.4 MAC address)
    CotaRcvrQuery_t queryType;  ///< Client query type
    uint8_t         priority;   ///< Charging priority
    uint16_t        version;    ///< Version (stored on discovery, reported for ESLs, which never get discovered again, via the client_detail cmd)
    uint32_t        hwModel;    ///< HW model (stored on discovery, reported for ESLs, which never get discovered again, via the client_detail cmd)
    uint8_t         motionEnabled; ///< Motion enabled flag
    uint8_t         reserved[9]; ///< Used to align the CRC32 field to the end of the memory slot for the client
    uint32_t        crc32;      ///< The last field of the structure is the CRC32 checksum for the entire structure (excluding this field)
} NvmClientData_t;

/** @brief  Configuration use to store rssi thresholds for vvotinb
 *
 * This structure is chosen to be  768 bytes so that 1 of these can
 * fit in one 3 256-byte EEPROM pages so we can read by simply
 * reading three blocks of rom.
 */
typedef PACKED struct _NvmRssiThreshData_t
{
    uint16_t uvpLowThreshValues[MAX_NUM_AMB][UVPS_PER_AMB];            ///< This is 128 bytes needed to store RSSI_LOW_THRESH values
    uint8_t  reserved[124];                                            ///< Used to align the CRC32 field to the end of the memory block
    uint32_t crc32;                                                    ///< The last field of the structure is the CRC32 checksum for the entire structure (excluding this field)    
} NvmRssiThreshData_t;

// Ensure 8 client configuration slots fit in one EEPROM page
static_assert((EEPROM_PAGE_SIZE % CLIENT_SLOT_SIZE == 0), "The size of an EEPROM page must be a multiple NvmClientData_t structure size");

static NvmConfigPage_t gConfigPage;                                     ///< An array of bytes read from EEPROM
static int32_t gLastUsedSlot = SLOT_NOT_FOUND;                          ///< Last used slot (if we erase another slot, this slot will be moved to take the erased slot's place)
static const uint16_t ERASED_SHORT_ID = INVALID_SHORT_ID;               ///< Erased short ID value
static const ExtdAddr_t DEFAULT_LONG_ID = {{0, 0, 0, 0, 0, 0, 0, 0}};   ///< Default long ID
static NvmRssiThreshData_t gRssiThreshStorage;                          ///< To save memory and stack, we will cache all these values here.
static SemaphoreHandle_t gNvmMutex = NULL;                              ///< Semaphore used to make this driver thread safe
static StaticSemaphore_t gNvmMutexBuff;                                 ///< The buffer holding the information for the #gNvmMutex mutex

static CotaError_t nvmUpdateConfigChecksum(void);
static CotaError_t nvmUpdateClientSlotChecksum(uint16_t slotNum);
static CotaError_t nvmLoadClients(void);
static CotaError_t nvmSetNumClients(uint16_t numClients);
static bool        findSlotNumOfClient(Client_t *client, uint32_t *slotNum);
static inline void populateNvmDataFromClient(Client_t *client, NvmClientData_t *nvmClientData);
static CotaError_t writeClientDataAt(uint32_t slotNum, NvmClientData_t *nvmClientData);



/**
 * @brief  Initializes the EEPROM driver.
 *
 * This function validates the data stored in NVM and loads system configuration
 * as well as client data into the module responsible for client management
 *
 *
 * @note Since this function calls EepromInit(), which uses
 *         semaphores, it must be called from a task context. This
 *         function also requires the client manager to be initialized
 *         first.
 *  
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
CotaError_t NvmInit(void)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    uint32_t crc32Stored;
    uint32_t crc32Computed;

    EepromInit();

    if (gNvmMutex == NULL)   //This function can be called twice, but we only need to create the mutex once.
    {
        gNvmMutex = xSemaphoreCreateRecursiveMutexStatic(&gNvmMutexBuff);
        if (gNvmMutex == NULL)
        {
            retVal = COTA_ERROR_SEMAPHORE_CREATION_FAILED;
        }
    }   
    // Read the CRC checksum and compare it to the computed value for the configuration data
    retVal = (retVal == COTA_ERROR_NONE) ? EepromReadConfigPage(CONFIG_PAGE_START_ADDR, (uint8_t*)&gConfigPage, sizeof(gConfigPage)) : retVal;
    crc32Stored = gConfigPage.crc32;
    crc32Computed = Crc32((uint8_t*)&gConfigPage, CONFIG_PAGE_PAYLOAD_SIZE);

    if ((crc32Stored != crc32Computed) || (retVal != COTA_ERROR_NONE))
    {
        NvmReset();
        if ((crc32Stored != crc32Computed) && (retVal == COTA_ERROR_NONE))
        {
            PostPrintf("Failed to confirm checksum in NvmInit\r\n"); 
        }
    }

    // Load all client data into the module responsible for client management
    if (retVal == COTA_ERROR_NONE)
    {
        retVal = nvmLoadClients();
    }

    return POST_COTA_ERROR(retVal);
}

/**
 * @brief  Resets the non-volatile memory back to default
 */
void NvmReset(void)
{
    ///@todo From the architectural point of view this doesn't make
    ///       sense - the NVM module shouldn't be aware of what
    ///       parameters are stored in NVM, so we'll probably need to
    ///       rethink this at some point in the future.  Maybe make
    ///       the orion_config module initialize the NVM module and if
    ///       it fails, have the orion_config overwrite the
    ///       configuration parameters.
    
    NvmLock();
    gConfigPage.data = GetDefaultSystemConfig();
    
    // Set the number of clients to 0
    gConfigPage.numClients = 0;
    
    // Compute the CRC-32 checksum
    gConfigPage.crc32 = Crc32((uint8_t*)&gConfigPage, CONFIG_PAGE_PAYLOAD_SIZE);
    
    // Save the whole configuration page into NVM
    EepromWriteConfigPage(CONFIG_PAGE_START_ADDR, (uint8_t*)&gConfigPage, sizeof(gConfigPage)); 
   
    NvmRssiThreshReset();
    
    NvmUnlock();
}


/**
 * @brief  Function that updates the checksum in the client slot
 *
 * @param  slotNum  Slot index
 *
 * @return COTA_ERROR_NONE on success or an error code on failure
 */
#pragma diag_suppress=Pe177  // Suppress the "function ... was declared but never referenced" warning
static CotaError_t nvmUpdateClientSlotChecksum(uint16_t slotNum)
{
    CotaError_t retVal;
    uint32_t crc32;
    uint32_t clientSlotAddr = 0;
    NvmClientData_t clientData;

    // Read the slot payload data (everything but the CRC checksum) from NVM
    clientSlotAddr = CLIENT_ADDRESS_FROM_SLOT_NUM(slotNum);
    retVal = EepromRead(clientSlotAddr, (uint8_t*)&clientData, CLIENT_SLOT_PAYLOAD_SIZE);  ///@todo  Only update the actual data

    if (retVal == COTA_ERROR_NONE)
    {
        // Compute the checksum value
        crc32 = Crc32((uint8_t*)&clientData, CLIENT_SLOT_PAYLOAD_SIZE);
        
        // Write the checksum value to NVM
        retVal = EepromWrite(clientSlotAddr + CLIENT_SLOT_PAYLOAD_SIZE, (uint8_t*)&crc32, sizeof(crc32));
    }
    
    return retVal;
}


/**
 * @brief  Function that updates the checksum of the configuration data page
 *
 * @return COTA_ERROR_NONE on success or an error code on failure
 *
 * @note   This function uses the local copy of the configuration data page
 *         to calculate the checksum, not the data stored in NVM
 */
static CotaError_t nvmUpdateConfigChecksum(void)
{
    CotaError_t retVal;
    uint32_t crc32;

    crc32 = Crc32((uint8_t*)&gConfigPage, CONFIG_PAGE_PAYLOAD_SIZE);
    retVal = EepromWriteConfigPage(CONFIG_PAGE_PAYLOAD_SIZE, (uint8_t*)&crc32, sizeof(crc32));
    
    return retVal;
}


/**
 * @brief  Function to retrieve configuration data structure
 *
 * @param  sysConfig  A pointer to a buffer of type #SysConfig_t
 *                    to receive system configuration data
 *
 * @return COTA_ERROR_NULL_VALUE_PASSED when sysConfig pointer is null;
 *         COTA_ERROR_NONE otherwise
 */
CotaError_t NvmGetConfigData(SysConfig_t * sysConfig)
{
    CotaError_t retVal = COTA_ERROR_NONE;
   
    NvmLock();
    
    if (sysConfig == NULL)
    {
        retVal = COTA_ERROR_NULL_VALUE_PASSED;
    }
    else
    {
        // Copy data to the provided buffer
        memcpy(sysConfig, &gConfigPage.data, sizeof(gConfigPage.data));
    }

    NvmUnlock();
    
    return retVal;
}


/**
 * @brief  Function to save the configuration data structure in NVM
 *
 * @param  newSysConfig  A pointer to a buffer of type #SysConfig_t
 *                       used to pass configuration data in by reference
 *
 * This function will check the differences between the current configuration
 * data and the data passed in and only write the differences to NVM to speed
 * up the update process.  It wil then update the CRC-32 checksum.
 *
 * @return COTA_ERROR_NONE on success or an error code on failure
 */
CotaError_t NvmSaveConfigData(SysConfig_t * newSysConfig)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    int16_t   startOfChange = sizeof(SysConfig_t);
    int16_t   endOfChange = -1;
    uint8_t * newData = (uint8_t*)newSysConfig;
    uint8_t * currentData = (uint8_t*)&gConfigPage.data;
  
    NvmLock();
    
    for (uint16_t i = 0; i < sizeof(SysConfig_t); i++)
    {
        if (newData[i] != currentData[i])
        {
            // Point startOfChange index to the first byte that's different
            if (startOfChange > i)
            {
                startOfChange = i;
            }

            // Point endOfChange index to the last byte that's different
            if (endOfChange < i)
            {
                endOfChange = i;
            }
        }
    }
    
    // Check if changes between the current data and the new data were found
    if (startOfChange <= endOfChange)
    {
        // Update the modified bytes in NVM
        uint16_t numBytesChanged = endOfChange - startOfChange + 1;
        retVal = EepromWriteConfigPage(startOfChange, newData + startOfChange, numBytesChanged);
    
        // Update the local copy of the data
        gConfigPage.data = *newSysConfig;
    
        // Update the CRC checksum in NVM
        if (retVal == COTA_ERROR_NONE)
        {
            retVal = nvmUpdateConfigChecksum();
        }
    }
    
    NvmUnlock();
    
    return retVal;
}


/**
 * @brief   This function updates the number of clients field of the configuration
 *          data page in NVM
 *
 * This function will update the number of clients as well as the CRC-32 checksum
 * of the configuration data page in NVM
 *
 * @param   numClients  The number of clients value to write to the configuration page
 *
 * @return  COTA_ERROR_NONE if the operation is successful; an error code if not
 */
static CotaError_t nvmSetNumClients(uint16_t numClients)
{
    CotaError_t retVal;
    int16_t startOfChange = (uint8_t*)&gConfigPage.numClients - (uint8_t*) &gConfigPage;  ///< The location of this variable in the EEPROM Identification Page
    // Update the Number of Clients in NVM
    gConfigPage.numClients = numClients;
    
    retVal = EepromWriteConfigPage(startOfChange, (uint8_t*)&numClients, sizeof(numClients));
    
    // Update the CRC checksum in NVM
    if (retVal == COTA_ERROR_NONE)
    {
        retVal = nvmUpdateConfigChecksum();
    }

    return retVal;
}


/**
 * @brief   This function loads all the clients stored in NVM into the client manager module
 *
 * @note    nvmClientData->nvmSlot contains the new slot number assigned to the client if
 *          the function returns COTA_ERROR_NONE;  the value passed into this function in
 *          nvmClientData->nvmSlot is ignored; however, the value returned from this
 *          function can be used by the caller to delete the client from NVM later on.
 *
 * @return  COTA_ERROR_NONE if the operation is successful; an error code if not
 */
static CotaError_t nvmLoadClients(void)
{
    CotaError_t      retVal = COTA_ERROR_NONE;
    uint16_t         clientsFoundCount = 0;
    uint16_t         nvmSlotNum = 0;
    uint32_t         clientSlotAddr = CLIENT_START_ADDR;
    uint32_t         crc32Computed;
    NvmClientData_t  clientData;
    ClientSlotData_t nvmClientData;
    
    PostPrintf("Locating %d receivers in NVM.\r\n", gConfigPage.numClients);
    // Load (gConfigPage.numClients) clients from NVM
    while ((clientsFoundCount < gConfigPage.numClients) &&
           (clientSlotAddr < EEPROM_MEM_SIZE) &&
           (retVal == COTA_ERROR_NONE))
    {
        retVal = EepromRead(clientSlotAddr, (uint8_t*)&clientData, CLIENT_SLOT_SIZE);

        if ((clientData.shortId != INVALID_SHORT_ID) && (retVal == COTA_ERROR_NONE))
        {
            // Check CRC checksum to validate the slot data
            crc32Computed = Crc32((uint8_t*)&clientData, CLIENT_SLOT_PAYLOAD_SIZE);
            if ((clientData.crc32 == crc32Computed) && (retVal == COTA_ERROR_NONE))
            {
                PostPrintf("Found 0x%016llx\r\n", *(uint64_t*)&clientData.longId);
                nvmClientData.shortId       = clientData.shortId;
                nvmClientData.longId        = clientData.longId;
                nvmClientData.queryType     = clientData.queryType;
                nvmClientData.priority      = clientData.priority;
                nvmClientData.version       = clientData.version;
                nvmClientData.hwModel       = clientData.hwModel;
                nvmClientData.motionEnabled = clientData.motionEnabled;
                nvmClientData.nvmSlot       = nvmSlotNum;

                POST_COTA_ERROR(CmAddClientFromNvm(nvmClientData));
                
                // Update the number of clients found in NVM
                clientsFoundCount++;
                
                // Update the last used memory slot
                gLastUsedSlot = nvmSlotNum;
            }
            else
            {
                // Erase the short ID to mark the slot as erased so we
                // don't have to compute the checksum for it next time
                retVal = EepromWrite(clientSlotAddr, (uint8_t*)&ERASED_SHORT_ID, sizeof(ERASED_SHORT_ID));
            }
        }

        nvmSlotNum++;
        clientSlotAddr = CLIENT_ADDRESS_FROM_SLOT_NUM(nvmSlotNum);

        // This loop can take a long time to run, so let's indicate
        // progress by periodically sending a '.' to the uart. 
        if ((nvmSlotNum % LOAD_PROG_WAIT) == 0)
        {
            PostPrintf(".");
        }
    }
    
    if ((clientsFoundCount != gConfigPage.numClients) && (retVal == COTA_ERROR_NONE))
    {
        PostPrintf("ERROR: Found only %d receivers in NVM. Expected %d.\r\n", clientsFoundCount, gConfigPage.numClients);
        // Update the total number of clients
        retVal = nvmSetNumClients(clientsFoundCount);
    }
    
    return retVal;
}

/**
 * @brief Updates client data in the NVM
 * @param client - Client with data to update
 * @return Returns COTA_ERROR_RCVR_NVM_NOT_FOUND on failure, COTA_ERROR_NONE on success
 */
CotaError_t NvmUpdateClient(Client_t *client)
{
    uint32_t slotNum;
    CotaError_t retVal;
    NvmClientData_t nvmClientData = {0};
    
    NvmLock();

    if (findSlotNumOfClient(client, &slotNum))
    {
        populateNvmDataFromClient(client, &nvmClientData);
        writeClientDataAt(slotNum, &nvmClientData);
    }
    else
    {
        retVal = COTA_ERROR_RCVR_NVM_NOT_FOUND;
    }

    NvmUnlock(); 

    return retVal;
}

/**
 * @brief The slot number given the client data. 
 * @param client - Client reference we are using to find the slot number.
 * @param slotNum an output parameter for the slot number of the client. 
 * @return true if the client is found, false if otherwise
 */
static bool findSlotNumOfClient(Client_t *client, uint32_t *slotNum)
{
    NvmClientData_t clientData;
    uint32_t clientSlotAddr;
    uint32_t currSlotNum;
    bool     slotNumFound; 

    currSlotNum = 0;
    slotNumFound = false; 

    // TODO currSlotNum is unsigned comparing to gLastUsedSlot signed.
    // need to unify types or cast appropriately. 
    while ((currSlotNum <= gLastUsedSlot) && !slotNumFound)
    {
        clientSlotAddr = CLIENT_ADDRESS_FROM_SLOT_NUM(currSlotNum);
        EepromRead(clientSlotAddr, (uint8_t*)&clientData, CLIENT_SLOT_SIZE);

        if (client->shortId == clientData.shortId)
        {
            *slotNum = currSlotNum;
            slotNumFound = true;
        }
        currSlotNum++;
    }

    return slotNumFound;
}

/**
 * @brief Helper function that copies pertinent Client_t data to NvmClientData_t
 * @param client - The reference client data
 * @param nvmClientData output parameter that all the data will be written to.
 */
static inline void populateNvmDataFromClient(Client_t *client, NvmClientData_t *nvmClientData)
{
    nvmClientData->shortId       = client->shortId;
    nvmClientData->longId        = client->longId;
    nvmClientData->queryType     = client->qStatus.header.queryType;
    nvmClientData->priority      = client->priority;
    nvmClientData->version       = client->version;
    nvmClientData->hwModel       = client->hwModel;
    nvmClientData->crc32         = Crc32((uint8_t*) nvmClientData, CLIENT_SLOT_PAYLOAD_SIZE);
}

/**
 * @brief Writes client data to a slot number.
 * @param slotNum - the slot number the NvmClientData_t will be written to.
 * @param nvmClientData - the nvmClientData to be written.
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
static CotaError_t writeClientDataAt(uint32_t slotNum, NvmClientData_t *nvmClientData)
{
    uint32_t clientSlotAddr = CLIENT_ADDRESS_FROM_SLOT_NUM(slotNum);
    return EepromWrite(clientSlotAddr, (uint8_t *) nvmClientData, CLIENT_SLOT_SIZE);
}

/**
 * @brief   This function creates a client memory slot to store its information in NVM
 *
 * @param   nvmClientData  A pointer to the structure describing a client
 *                         that needs to be saved in NVM
 *
 * @note    nvmClientData->nvmSlot contains the new slot number assigned to the client if
 *          the function returns COTA_ERROR_NONE  the value passed into this function in
 *          nvmClientData->nvmSlot is ignored; however, the value returned from this
 *          function can be used by the caller to delete the client from NVM later on.
 *
 * @return  COTA_ERROR_NONE if the operation is successful; an error code if not
 */
CotaError_t NvmAddClient(ClientSlotData_t * nvmClientData)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    NvmClientData_t clientData = {0};
    uint32_t clientSlotAddr = 0;
   
    NvmLock();
    
    // Check if memory is full
    if ((gLastUsedSlot + 1) == MAX_CLIENT_COUNT)
    {
        retVal = COTA_ERROR_NVM_MEMORY_FULL;
    }
    else
    {
        // Update last slot index
        if (gLastUsedSlot == SLOT_NOT_FOUND)
        {
            gLastUsedSlot = 0;
        }
        else
        {
            gLastUsedSlot++;
        }
        
        clientData.shortId       = nvmClientData->shortId;
        clientData.longId        = nvmClientData->longId;
        clientData.queryType     = nvmClientData->queryType;
        clientData.priority      = nvmClientData->priority;
        clientData.version       = nvmClientData->version;
        clientData.hwModel       = nvmClientData->hwModel;
        clientData.motionEnabled = nvmClientData->motionEnabled;
        clientData.crc32         = Crc32((uint8_t*)&clientData, CLIENT_SLOT_PAYLOAD_SIZE);

        // Write the client memory slot data to NVM
        clientSlotAddr = CLIENT_ADDRESS_FROM_SLOT_NUM(gLastUsedSlot);
        retVal = EepromWrite(clientSlotAddr, (uint8_t*)&clientData, CLIENT_SLOT_SIZE);
        
        // Update the number of clients
        if (retVal == COTA_ERROR_NONE)
        {
            gConfigPage.numClients++;
            retVal = nvmSetNumClients(gConfigPage.numClients);
            
            // Update the slot number used by the client manager
            if (retVal == COTA_ERROR_NONE)
            {
                nvmClientData->nvmSlot = gLastUsedSlot;
            }
        }
    }
    
    NvmUnlock();
    
    return retVal;
}


/**
 * @brief   This function deletes a client memory slot from the NVM
 *
 * @param   slotNum  Index of the memory slot for the client to be deleted
 *
 * @return  COTA_ERROR_NONE if the operation is successful; an error code if not
 *         
 */
CotaError_t NvmDeleteClient(uint16_t slotNum)
{
    CotaError_t retVal = COTA_ERROR_NONE;
    NvmClientData_t clientData;
    uint32_t clientSlotAddr = 0;
   
    NvmLock();
    
    if ((gConfigPage.numClients == 0) || (gLastUsedSlot == SLOT_NOT_FOUND) || (slotNum > gLastUsedSlot))
    {
        retVal = COTA_ERROR_NVM_INVALID_DELETE;
    }
    else
    {
        // Move the client data from the last slot to the deleted one
        // if the deleted slot is not the last slot
        if (slotNum < gLastUsedSlot)
        {
            // Read the client data from the last slot
            clientSlotAddr = CLIENT_ADDRESS_FROM_SLOT_NUM(gLastUsedSlot); 
            retVal = EepromRead(clientSlotAddr, (uint8_t*)&clientData, CLIENT_SLOT_SIZE);
            
            // Write the data from the last slot into the slot being deleted
            if (retVal == COTA_ERROR_NONE)
            {
                clientSlotAddr = CLIENT_ADDRESS_FROM_SLOT_NUM(slotNum); 
                retVal = EepromRead(clientSlotAddr, (uint8_t*)&clientData, CLIENT_SLOT_SIZE);
            }
        }

        
        // Erase the last client slot by writing invalid byte values to the client memory slot in NVM
        if (retVal == COTA_ERROR_NONE)
        {
            memset(&clientData, EERPOM_ERASED_BYTE, sizeof(clientData));
            clientSlotAddr = CLIENT_ADDRESS_FROM_SLOT_NUM(gLastUsedSlot);
            retVal = EepromWrite(clientSlotAddr, (uint8_t*)&clientData, CLIENT_SLOT_SIZE);
            
            if (retVal == COTA_ERROR_NONE)
            {
                if (gConfigPage.numClients == 1)
                {
                    // We just deleted the last remaining client
                    gLastUsedSlot = SLOT_NOT_FOUND;
                }

                // Update the number of clients by decrementing it by 1
                retVal = nvmSetNumClients(gConfigPage.numClients - 1);
            }
        }
    }
    
    NvmUnlock();
    
    return retVal;
}

/**
 * @brief  This function initializes a structure used to add
 *         a new client with default values
 *
 * @param  nvmClientData  Pointer to a structure to write the default values to
 *
 * @note   The nvmSlot field of the nvmClientData struct is not meant to be
 *         used directly by the caller
 */
void NvmClientDataInit(ClientSlotData_t * nvmClientData)
{
    nvmClientData->shortId       = DEFAULT_SHORT_ID;
    nvmClientData->longId        = DEFAULT_LONG_ID;
    nvmClientData->queryType     = DEFAULT_QUERY_TYPE;
    nvmClientData->priority      = DEFAULT_PRIORITY;
    nvmClientData->version       = DEFAULT_VERSION;
    nvmClientData->hwModel       = DEFAULT_HW_MODEL;
    nvmClientData->motionEnabled = DEFAULT_MOTION_EN;
    nvmClientData->nvmSlot       = (gLastUsedSlot == SLOT_NOT_FOUND) ? 0 : gLastUsedSlot + 1;
}

/**
 *  @brief Saves the threshold stored in #gRssiThreshStorage to EEPROM
 *         and updates the crc.
 */ 
CotaError_t NvmSaveRssiThreshToEeprom(void)
{
    uint32_t rssiThreshBlockAddr = 0;
    uint8_t* rssiThreshAddr = (uint8_t*)&gRssiThreshStorage;
    CotaError_t ret = COTA_ERROR_NONE;
        
    NvmLock();
        
    gRssiThreshStorage.crc32 =  Crc32((uint8_t*)&gRssiThreshStorage, RSSI_THRESH_PAYLOAD_SIZE);
    
    for (rssiThreshBlockAddr = 0; (rssiThreshBlockAddr < RSSI_THRESH_SIZE) && (ret == COTA_ERROR_NONE); rssiThreshBlockAddr+=EEPROM_PAGE_SIZE )
    {
        ret = EepromWrite(rssiThreshBlockAddr, rssiThreshAddr, EEPROM_PAGE_SIZE);
        rssiThreshAddr += EEPROM_PAGE_SIZE;
    }
    
    NvmUnlock();
    return ret;
}


/**
 *  @brief Retrieves the threshold stored in #gRssiThreshStorage to eeprom
 *         so that it can be cached and accessed.
 */ 
CotaError_t NvmGetRssiThreshFromEeprom(void)
{
    uint32_t rssiThreshBlockAddr = 0;
    uint8_t* rssiThreshAddr = (uint8_t*)&gRssiThreshStorage;
    uint32_t crc32Computed;
    CotaError_t ret = COTA_ERROR_NONE;
    
    NvmLock();
    
    for (rssiThreshBlockAddr = 0; (rssiThreshBlockAddr < RSSI_THRESH_SIZE) && (ret == COTA_ERROR_NONE); rssiThreshBlockAddr+=EEPROM_PAGE_SIZE )
    {
        ret = EepromRead(rssiThreshBlockAddr, rssiThreshAddr, EEPROM_PAGE_SIZE);
        rssiThreshAddr += EEPROM_PAGE_SIZE;
    }

    if (ret == COTA_ERROR_NONE)
    {
        //Check crc
        crc32Computed = Crc32((uint8_t*)&gRssiThreshStorage, RSSI_THRESH_PAYLOAD_SIZE);
        if (crc32Computed != gRssiThreshStorage.crc32)
        {
            //Bad data, let's 0 it out in ram and eeprom
            NvmRssiThreshReset();
            PostPrintf("Failed to load rssi threshold data\r\n");
        }
    }
    NvmUnlock();
    return ret;
}

/**
 * @brief returns a pointer to an array of size [MAX_NUM_AMB][UVPS_PER_AMB]
 *        which contain rssi low threshold data.
 */
uint16_t* NvmGetRssiLowThreshArray(void)
{
    return (uint16_t*)gRssiThreshStorage.uvpLowThreshValues;
}

/**
 * @brief Resets the rssi threshold block 
 */
void NvmRssiThreshReset(void)
{
    NvmLock();
    memset(&gRssiThreshStorage, 0, RSSI_THRESH_SIZE);
    NvmSaveRssiThreshToEeprom();
    NvmUnlock();
}

/**
 *  @brief Lock the nvm module so a thread can have exclusive use of cached data
 */
void NvmLock(void)
{
    xSemaphoreTakeRecursive(gNvmMutex, portMAX_DELAY);
}

/**
 *  @brief Unlocks the nvm module
 */
void NvmUnlock(void)
{
    xSemaphoreGiveRecursive(gNvmMutex);
}
