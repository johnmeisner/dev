/****************************************************************************//**
* @file      power_level.c
*
* @brief     Implements functions to control power of the UVP's
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "main.h"
#include "power_level.h"
#include "uvp_driver.h"
#include "amb_control.h"
#include "error.h"

typedef struct _DbmToRegEntry
{
    PowerRegister_t reg;        ///< Register value needed to set the power
    uint16_t        dBm;        ///< Single antenna power in dBm for this register
} DbmToRegEntry;

/**
 * @brief A map of power level register values to actual power values in dBm
 */
static DbmToRegEntry gDBmToRegMap[] = { 
    { AMU_POWER_HIGH,     20000 }, 
    { AMU_POWER_19_5_DBM, 19500 }, 
    { AMU_POWER_19_DBM,   19000 }, 
    { AMU_POWER_16_DBM,   16000 }, 
    { AMU_POWER_13_DBM,   13000 } 
};

#define  NUM_POWER_MAP_ENTRIES (sizeof(gDBmToRegMap)/sizeof(gDBmToRegMap[0]))  ///< The number of power level map entries

/**
 * @brief  Sets the power level of all the UVP's on an AMB.  UVP's can only support
 *         five power levels: 20.0 dBm, 19.5 dBm, 19 dBm, 16 dBm, and 13 dBm.  If one of these
 *         power levels is not requested, the nearest lower value will be chosen.
 * 
 * @param pow                 A value of type #PowerRegister_t
 * @param ambMask             A mask indicating which AMB's will have their power level set
 * @param uvpMask             A mask indicating which UVP's will have their power level set.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t SetUvpPowerLevels(PowerRegister_t pow, AmbMask_t ambMask, UvpMask_t uvpMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
    
    if (pow < AMU_POWER_HIGH)
    {
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU1PAHIGHPOWER, 0, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU2PAHIGHPOWER, 0, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU3PAHIGHPOWER, 0, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PAHIGHPOWER, 0, ambMask, uvpMask) : ret;
      
      
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU1PAPOWERMODE, pow, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU2PAPOWERMODE, pow, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU3PAPOWERMODE, pow, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PAPOWERMODE, pow, ambMask, uvpMask) : ret;
    }
    else
    {
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU1PAHIGHPOWER, 1, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU2PAHIGHPOWER, 1, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU3PAHIGHPOWER, 1, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PAHIGHPOWER, 1, ambMask, uvpMask) : ret;
    }
    return ret;
}

 /**
 * @brief  Gets the power level of a single the UVP on an AMB.  UVP's can only support
 *         five power levels: 20.0 dBm, 19.5 dBm, 19 dBm, 16 dBm, and 13 dBm.  If one of these
 *         power levels is not requested, the nearest lower value will be chosen.
 * 
 * @param pow                 The value for the power mode register or AMU_POWER_HIGH for 20 dBm
 * @param ambNum              The AMB number containing the UVP to read from
 * @param uvpNum              The UVP number to read from.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t GetUvpPowerLevels(PowerRegister_t* pow, AmbNum_t ambNum, UvpNum_t uvpNum)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint32_t data; 

    ret = UvpRegisterRead(AMU1PAHIGHPOWER, &data, ambNum, uvpNum);

    if ((ret == COTA_ERROR_NONE) && (data == 1))
    {
        *pow = AMU_POWER_HIGH;
    }
    else
    {
        ret = UvpRegisterRead(AMU1PAPOWERMODE, &data, ambNum, uvpNum);
        if (ret == COTA_ERROR_NONE)
        {
            *pow = (PowerRegister_t) data;
        }
    }
  
    return ret;
}

/**
 * @brief  Sets the power level of all the UVP's on an AMB.  UVP's can only support
 *         five power levels: 20.0 dBm, 19.5 dBm, 19 dBm, 16 dBm, and 13 dBm.  If one of these
 *         power levels is not requested, the nearest lower value will be chosen.
 * 
 * @param ambMask             A mask indicating which AMB's will have their UVP's power level set. 
 * @param requestedPowerLevel The requested power level in mdBm
 * @param actualPowerLevel    Pointer to a float to receive the power level the UVP will actually be set to.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t SetAmbPowerLevels(AmbMask_t ambMask, uint16_t requestedPowerLevel, uint16_t* actualPowerLevel)
{
    CotaError_t ret = COTA_ERROR_NONE;
    PowerRegister_t reg;

    *actualPowerLevel = 0;

    if (requestedPowerLevel > gDBmToRegMap[0].dBm)
    {   
      // Power level is greater than the max, so set the power level to the max.
        *actualPowerLevel = gDBmToRegMap[0].dBm;
        reg = gDBmToRegMap[0].reg;
    }
    else if (requestedPowerLevel < gDBmToRegMap[NUM_POWER_MAP_ENTRIES-1].dBm)
    {
      // Power level is lower than the minimum, so set the power level to the minium.
        *actualPowerLevel = gDBmToRegMap[NUM_POWER_MAP_ENTRIES-1].dBm;
        reg = gDBmToRegMap[NUM_POWER_MAP_ENTRIES-1].reg;
    }
    else
    {
      //Find the closet lower power level
        for (uint16_t ii = 0; ii < NUM_POWER_MAP_ENTRIES; ii++)
        {
            if (requestedPowerLevel >= gDBmToRegMap[ii].dBm)
            {
                reg = gDBmToRegMap[ii].reg;
                *actualPowerLevel = gDBmToRegMap[ii].dBm;
                break;
            }
        }
    }

    ret = SetUvpPowerLevels(reg, ambMask, ALL_UVP_MASK);

    return ret;
}

