/**
 * @file       proxy.h
 *
 * @brief      Proxy communication driver header file
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#ifndef _PROXY_DRIVER_HEADER_
#define _PROXY_DRIVER_HEADER_

#include "stdint.h"
#include "stdbool.h"
#include "ctrl_task.h"
#include "FreeRTOS.h"
#include "error.h"

bool        ProxyIsRxQueueEmpty(void);
void        ProxyDriverInit(void);
void        ProxyReset(void);
bool        ProxySendMessage(uint8_t * buf, uint8_t size);
CotaError_t ProxyGetMessage(uint8_t * buf, uint8_t size);
void        ProxySlaveReadyIrq(BaseType_t * const higherPriorityTaskWoken);
void        ProxySpiTxCpltIrq(BaseType_t * const higherPriorityTaskWoken);
void        ProxySpiRxCpltIrq(BaseType_t * const higherPriorityTaskWoken);

#endif   // ifndef _PROXY_DRIVER_HEADER_
