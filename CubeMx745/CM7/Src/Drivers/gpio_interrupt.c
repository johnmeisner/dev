/**
 * @file       gpio_interrupt.c
 *
 * @brief      GPIO interrupt processing module
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 */

#include "main.h"
#include "proxy.h"
#include "raspberry_pi.h"
#include "FreeRTOS.h"
#include "door_driver.h"
#include "gpio_interrupt.h"
#include "log.h"


/** 
 * Mask containing interrupt flags whose values of type
 * #GpioInterruptFlagMask_t match bit positions
 */
static volatile GpioInterruptFlagMask_t gGpioIntFlagMask = 0;

/** Lookup table containing interrupt line descriptions */
static char * GPIO_INT_LINE_DESC_LUT[GPIO_INT_COUNT] =
{
    [GPIO_INT_PROXY_BEACON_1] = "PROXY_BEACON1_INT",
    [GPIO_INT_PROXY_BEACON_2] = "PROXY_BEACON2_INT",
};


/**
 * @brief  Callback on proxy SR and RPi Host MR line change even
 *
 * @param  GPIO_Pin  Pin number for the pin that triggered the interrupt
 *
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    BaseType_t higherPriorityTaskWoken = pdFALSE;

    // Check if the proxy changed the state of the SR line
    if (GPIO_Pin == PROXY_SLAVE_READY_Pin)
    {
        ProxySlaveReadyIrq(&higherPriorityTaskWoken);
    }
    else if (GPIO_Pin == HOST_MR_Pin)
    {
        RpiMasterReadyIrq(&higherPriorityTaskWoken);
    }
#ifdef USE_FOREVER_TRACKER_BUILD
    else if (GPIO_Pin == COTA_BOX_Pin)
    {
        DoorOpenIrq(&higherPriorityTaskWoken);
    }
#endif
    else if (GPIO_Pin == PROXY_BEACON1_INT_Pin)
    {
        gGpioIntFlagMask |= 1 << GPIO_INT_PROXY_BEACON_1;
    }
    else if (GPIO_Pin == PROXY_BEACON2_INT_Pin)
    {
        gGpioIntFlagMask |= 1 << GPIO_INT_PROXY_BEACON_2;
    }
    else {
        // Do nothing
    }

    portYIELD_FROM_ISR(higherPriorityTaskWoken);
}


/**
 * @brief  Returns the bitmask containing currently pending GPIO flags
 *
 * This function returns the GPIO flags and optionally clears them
 * to mark them as processed.
 *
 * @param  clearFlags  true to clear interrupt flags; false for no change
 *
 * @return A mask with bit positions for 1's indicating interrupt numbers
 *         that were flagged.
 */
GpioInterruptFlagMask_t GpioGetInterruptFlags(bool clearFlags)
{
    uint32_t intFlagMask = 0;

    // It's fairly safe to assume that if we're reading gGpioIntFlagMask,
    // and its value is 0, then no interrupt has occurred, since reading
    // a 32-bit value is an atomic operation.
    if (gGpioIntFlagMask)
    {
        // Critical section so no interrupts can change gGpioIntFlagMask
        portENTER_CRITICAL();
        
        intFlagMask = gGpioIntFlagMask;
        if (clearFlags)
        {
            gGpioIntFlagMask = 0;
        }
        
        portEXIT_CRITICAL();
    }
    
    return intFlagMask;
}


/**
 * @brief  Returns the name of the signal corresponding to given interrupt line
 *
 * @param  intLine  Interrupt line ID
 *
 * @return The string with the name of the interrupt line or NULL for invalid interrupt line
 */
char * GetInterruptFlagDescription(GpioInterruptLines_t intLine)
{
    char * retVal = NULL;
    
    if (intLine < GPIO_INT_COUNT)
    {
        retVal = GPIO_INT_LINE_DESC_LUT[intLine];
    }
    
    return retVal;
}


/**
 * @brief Process GPIO interrupt flags inside the control task
 */
void ProcessInterruptFlags(void)
{
    uint32_t gpioInterruptFlagMask = 0;
    GpioInterruptLines_t intLineNum = (GpioInterruptLines_t)0;
    char * intLineName;
    
    gpioInterruptFlagMask = (uint32_t)GpioGetInterruptFlags(GPIO_INT_CLEAR);
    
    while (gpioInterruptFlagMask)
    {
        if (gpioInterruptFlagMask & 1)
        {
            intLineName = GetInterruptFlagDescription(intLineNum);
            if (intLineName != NULL)
            {
                LogInfo("Interrupt on line %s\r\n", intLineName);
            }
        }
        gpioInterruptFlagMask >>= 1;
        intLineNum++;
    }
}
