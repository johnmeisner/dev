/****************************************************************************//**
* @file      amb_control.c
*
* @brief     Implements functions to control the AMB's
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "main.h"
#include "orion_config.h"
#include "amb_control.h"

///@todo the above defines need to be deleted when we move to the ccb\

#define AMB_ENABLE_ON               GPIO_PIN_RESET          ///< State of gpio to enable an AMB
#define AMB_ENABLE_OFF              GPIO_PIN_SET            ///< State of gpio to disable an AMB
#define UVP_ENABLE_ON               GPIO_PIN_RESET          ///< State of gpio to enable an UVP
#define UVP_ENABLE_OFF              GPIO_PIN_SET            ///< State of gpio to disable an UVP
#define SPI_ENABLE_ON               GPIO_PIN_SET            ///< State of gpio to enable an SPI
#define SPI_ENABLE_OFF              GPIO_PIN_RESET          ///< State of gpio to disable an SPI
#define PU_ASSERT                   GPIO_PIN_SET            ///< State of gpio to raise an PU pin on UVP's
#define PU_DEASSERT                 GPIO_PIN_RESET          ///< State of gpio to lower an PU pin on UVP's
#define UVP_CS_ADDR_ON              GPIO_PIN_SET            ///< Indicates a UVP chip select pin is active.
#define UVP_CS_ADDR_OFF             GPIO_PIN_RESET          ///< Indicates a UVP chip select pin is inactive.
#define UVP_CS_MASK_0               0x1                     ///< Mask for UVP chip select pin 0
#define UVP_CS_MASK_1               0x2                     ///< Mask for UVP chip select pin 1
#define UVP_CS_MASK_2               0x4                     ///< Mask for UVP chip select pin 2
#define UVP_CS_MASK_3               0x8                     ///< Mask for UVP chip select pin 3 
#define SINGLE_MODE_ON              GPIO_PIN_SET            ///< State of gpio to enable single UVP interactions
#define SINGLE_MODE_OFF             GPIO_PIN_RESET          ///< State of gpio to disable single UVP interactions
#define TX_ON                       GPIO_PIN_SET            ///< State of gpio to enable transmitting on UVP's
#define TX_OFF                      GPIO_PIN_RESET          ///< State of gpio to disable transmitting on UVP's
#define RX_ON                       GPIO_PIN_SET            ///< State of gpio to enable receiving on UVP's
#define RX_OFF                      GPIO_PIN_RESET          ///< State of gpio to disable receiving on UVP's
#define PU_ALL_ON                   GPIO_PIN_SET            ///< State of gpio to enable phase update on all AMB's
#define PU_ALL_OFF                  GPIO_PIN_RESET          ///< State of gpio to not enable phase update on all AMB's
#define PD_DONE                     GPIO_PIN_SET            ///< State of gpio that occurs when phase detect is done
#define PD_IN_PROGRESS              GPIO_PIN_RESET          ///< State of gpio that occurs when phase detect is in progress
#define WRMHL_CSB_ENABLED           GPIO_PIN_RESET          ///< State of WRMHL_CSB to allow SPI transfers to AMB
#define WRMHL_CSB_DISABLED          GPIO_PIN_SET            ///< State of WRMHL_CSB to stop SPI transfers to AMB


/**
 * @brief This structure allows us to map AMB's to their SPI handles and GPIO pins
 */
typedef struct _AmbMap
{
    GPIO_TypeDef*         spiDisablePort; //< The port of the AMB's disable GPIO
    uint16_t              spiDisablePin;  //< The pin of the AMB's disable GPIO
    GPIO_TypeDef*         ambEnPort;      //< The port of the AMB's LDO enable GPIO           
    uint16_t              ambEnPin;       //< The pin of the AMB's LDO enable GPIO
    GPIO_TypeDef*         uvpEnPort;      //< The port of the AMB's UVP enable GPIO      
    uint16_t              uvpEnPin;       //< The pin of the AMB's UVP enable GPIO
    GPIO_TypeDef*         pdDonePort;     //< The port of the AMB's phase detect done GPIO       
    uint16_t              pdDonePin;      //< The pin of the AMB's phase detect done GPIO
    GPIO_TypeDef*         puPort;         //< The port of the AMB's phase detect done GPIO       
    uint16_t              puPin;          //< The pin of the AMB's phase detect done GPIO  
    SPI_HandleTypeDef*    spi;            //< The handle to the AMB's SPI port
} AmbMap_t;

/// @todo The following variable may need to be changed when the new ccb arrives
#if MASTER_AMB_SPI_PORT == 1
static SPI_HandleTypeDef* masterSpi = &hspi1; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 2
static SPI_HandleTypeDef* masterSpi = &hspi2; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 3
static SPI_HandleTypeDef* masterSpi = &hspi3; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 4
static SPI_HandleTypeDef* masterSpi = &hspi4; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 5
static SPI_HandleTypeDef* masterSpi = &hspi5; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 6
#error Code doesn't support SPI6 being master because SPI6 uses BDMA
#else
#error "MASTER_AMB_SPI_PORT is not valid"
#endif

/**
 * This array maps each AMB to its SPI handle and GPIO pins
 * 
 * @note  For AMB 0, 1, 3 (SPI 4, 5, 6), we set them up as slaves, but MISO/MOSI were mapped as if they were masters (by default). 
 *        so they have to their IO swapped.
 */
static AmbMap_t ambInfo[] = 
{
    {SPI_DISABLE_0_GPIO_Port, SPI_DISABLE_0_Pin, AMB_EN_0_GPIO_Port, AMB_EN_0_Pin, UVPEN_0_GPIO_Port, UVPEN_0_Pin, PD_DONE_0_GPIO_Port, PD_DONE_0_Pin, AMB0_PU_GPIO_Port, AMB0_PU_Pin, &AMB0_SPI_HANDLE},
    {SPI_DISABLE_1_GPIO_Port, SPI_DISABLE_1_Pin, AMB_EN_1_GPIO_Port, AMB_EN_1_Pin, UVPEN_1_GPIO_Port, UVPEN_1_Pin, PD_DONE_1_GPIO_Port, PD_DONE_1_Pin, AMB1_PU_GPIO_Port, AMB1_PU_Pin, &AMB1_SPI_HANDLE},
    {SPI_DISABLE_2_GPIO_Port, SPI_DISABLE_2_Pin, AMB_EN_2_GPIO_Port, AMB_EN_2_Pin, UVPEN_2_GPIO_Port, UVPEN_2_Pin, PD_DONE_2_GPIO_Port, PD_DONE_2_Pin, AMB2_PU_GPIO_Port, AMB2_PU_Pin, &AMB2_SPI_HANDLE},
    {SPI_DISABLE_3_GPIO_Port, SPI_DISABLE_3_Pin, AMB_EN_3_GPIO_Port, AMB_EN_3_Pin, UVPEN_3_GPIO_Port, UVPEN_3_Pin, PD_DONE_3_GPIO_Port, PD_DONE_3_Pin, AMB3_PU_GPIO_Port, AMB3_PU_Pin, &AMB3_SPI_HANDLE},
};
 
static AmbMask_t gValidAmbMask = AMB_DISABLE_ALL;  ///< A mask set at initialization indicating which AMB's are valid.
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim8;

/**
 * @brief Performs necessary intialization of GPIO pins
 */
void AmbControlInit(AmbMask_t validAmbs)
{
    gValidAmbMask = validAmbs;
    AmbEnableRx(false);
    AmbEnableTx(false);
    AmbEnablePuAll(false);
    AmbEnableWrmhlCsb(false);
    AmbSetPuState(AMB_DISABLE_ALL);
    AmbEnable(AMB_DISABLE_ALL);
}

/**
 * @brief Retrieve the valid amb mask, which indicates which amb's are being used.
 *
 * @return The valid amb mask
 */
AmbMask_t AmbGetValidAmbs(void)
{
    return gValidAmbMask;  
}

/**
 * @brief Sets up UVP's for sending/receiving SPI traffic
 *
 * @param ambMask   A mask specifying which AMB's to enble SPI traffic on.
 *                  1 will enable the SPI by pushing the disable pin low at the UVP,
 *                  0 will disable the SPI by pushing the disable pin high at the UVP
 */
void AmbSpiEnable(AmbMask_t ambMask)
{
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
            HAL_GPIO_WritePin(ambInfo[ii].spiDisablePort, ambInfo[ii].spiDisablePin, SPI_ENABLE_ON);  
        }
        else
        {
            HAL_GPIO_WritePin(ambInfo[ii].spiDisablePort, ambInfo[ii].spiDisablePin, SPI_ENABLE_OFF);  
        }
    }
}

/**
 * @brief Gets the state of the SPI enable gpios for all the AMBs
 *
 * @return A mask specifying which AMB's have SPI enabled, 1 for on, 0 or off
 */
AmbMask_t AmbGetSpiEnabled(void)
{
    AmbMask_t ambMask = 0;
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
       if (HAL_GPIO_ReadPin(ambInfo[ii].spiDisablePort, ambInfo[ii].spiDisablePin) == SPI_ENABLE_ON)
       {
            ambMask |= (1 << ii);
       }   
    } 
    return ambMask;
}


/**
 * @brief Sets the state of the UVP enable gpio for all the AMB's specified by the mask.  
 *        The UVP enable pin will not be set for an AMB unless the AMB enable
 *        pin for that UVP is also set.
 *
 * @param ambMask A mask specifying which AMB's to set the UVP enable gpio for, 1 for on, 0 or off
 *
 * @return COTA_ERROR_NONE on success; otherwise COTA_ERROR_AMB_NOT_ENABLED
 */ 
CotaError_t AmbUvpEnable(AmbMask_t ambMask)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
            if (HAL_GPIO_ReadPin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin) == AMB_ENABLE_ON)
            {
                HAL_GPIO_WritePin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin, UVP_ENABLE_ON);
            }
            else
            {
                //We cannot enable a UVP unless the AMB is also enabled.
                ret = COTA_ERROR_AMB_NOT_ENABLED;
            }
        }
        else
        {
            HAL_GPIO_WritePin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin, UVP_ENABLE_OFF);      
        }
    }
      
    return ret;
}

/**
 * @brief Gets the state of the UVP enable gpios for all the AMBs
 *        The UVP enable pin will not be set for an AMB unless the AMB enable
 *        pin for that uvp also set.
 *
 * @return A mask specifying which AMB's have their UVP enabled, 1 for on, 0 or off
 */
AmbMask_t AmbGetUvpEnabled(void)
{
    uint8_t ii;
    AmbMask_t ambMask = 0;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (HAL_GPIO_ReadPin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin) == UVP_ENABLE_ON)
        {
            ambMask |= (1 << ii);
        }   
    } 
    return ambMask;
}

/**
 * @brief Sets the state of the AMB enable GPIO for all the AMB's specified by the mask.  
 *        If an AMB is disabled, the UVP enable pins will also be disabled.
 *       
 * @param ambMask   A mask specifying which AMB's to set the LDO enable gpio for, and which to not enable. 1 for enable, 0 for disable.
 */ 
void AmbEnable(AmbMask_t ambMask)
{
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
            HAL_GPIO_WritePin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin, AMB_ENABLE_ON);    
        }
        else
        {
            HAL_GPIO_WritePin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin, UVP_ENABLE_OFF);
            HAL_GPIO_WritePin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin, AMB_ENABLE_OFF);      
        }
    }
}

/**
 * @brief Gets the state of the AMB enable GPIO's
 *
 * @return A mask specifying which AMB's have their LDO;s enabled. 1 for enable, 0 for disable.
 */
AmbMask_t AmbGetEnabled(void)
{
    uint8_t ii;
    AmbMask_t ambMask = 0;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (HAL_GPIO_ReadPin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin) == AMB_ENABLE_ON)
        {
            ambMask |= (1 << ii);
        }
    } 
    return ambMask;
}
/**
 * @brief Sets the state of the Phase update pin for each AMB
 *       
 * @param ambMask   A mask specifying which AMB's to set PU gpio for, and which to not enable. 1 for raise, 0 for drop.
 */ 
void AmbSetPuState(AmbMask_t ambMask)
{
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
            HAL_GPIO_WritePin(ambInfo[ii].puPort, ambInfo[ii].puPin, PU_ASSERT);    
        }
        else
        {
            HAL_GPIO_WritePin(ambInfo[ii].puPort, ambInfo[ii].puPin, PU_DEASSERT);      
        }
    }
}

/**
 * @brief Gets the state of the Phase Update for every AMB
 *
 * @return A mask specifying which state of PU update pins. 1 for raised, 0 for droped.
 */
AmbMask_t AmbGetPuState(void)
{
    uint8_t ii;
    AmbMask_t ambMask = 0;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (HAL_GPIO_ReadPin(ambInfo[ii].puPort, ambInfo[ii].puPin) == PU_ASSERT)
        {
            ambMask |= (1 << ii);
        }
       
    } 
    return ambMask;
}

/**
 * @brief Select the UVP number to interact with.  This also enables "Single" mode to allow individual UVP interactions
 *
 * @param uvpNum The UVP number to interact with. Or All_UVP_NUM, if all uvp's are to be selected.
 * @return COTA_ERROR_NONE on success; otherwise COTA_ERROR_UVP_UNKNOWN
 */
CotaError_t AmbSelectUvp(UvpNum_t uvpNum)
{ 
    CotaError_t ret = COTA_ERROR_NONE;
    
    if (uvpNum < UVPS_PER_AMB)
    {    
        HAL_GPIO_WritePin(SPI_SINGLE_GPIO_Port, SPI_SINGLE_Pin, SINGLE_MODE_ON); 
        
        HAL_GPIO_WritePin(CSB_ADD0_GPIO_Port, CSB_ADD0_Pin, (uvpNum & UVP_CS_MASK_0) ? UVP_CS_ADDR_ON : UVP_CS_ADDR_OFF);
        HAL_GPIO_WritePin(CSB_ADD1_GPIO_Port, CSB_ADD1_Pin, (uvpNum & UVP_CS_MASK_1) ? UVP_CS_ADDR_ON : UVP_CS_ADDR_OFF);
        HAL_GPIO_WritePin(CSB_ADD2_GPIO_Port, CSB_ADD2_Pin, (uvpNum & UVP_CS_MASK_2) ? UVP_CS_ADDR_ON : UVP_CS_ADDR_OFF);
        HAL_GPIO_WritePin(CSB_ADD3_GPIO_Port, CSB_ADD3_Pin, (uvpNum & UVP_CS_MASK_3) ? UVP_CS_ADDR_ON : UVP_CS_ADDR_OFF);
    }
    else if (uvpNum == ALL_UVP_NUM)
    {
        HAL_GPIO_WritePin(SPI_SINGLE_GPIO_Port, SPI_SINGLE_Pin, SINGLE_MODE_OFF);        
    }
    else
    {
        ret = COTA_ERROR_UVP_UNKNOWN;
    }
    
    return ret;
}

/**
 * @brief Gets selected UVP number
 *
 * @return returns the number of the uvp selected, or if the total amount of uvp's if they are selected
 */
uint8_t AmbGetSelectedUvp(void)
{
    uint8_t ret = 0;
    
    if (HAL_GPIO_ReadPin(SPI_SINGLE_GPIO_Port, SPI_SINGLE_Pin) == SINGLE_MODE_OFF)
    {
        ret = ALL_UVP_NUM;
    }
    else
    {
        if (HAL_GPIO_ReadPin(CSB_ADD0_GPIO_Port, CSB_ADD0_Pin) == UVP_CS_ADDR_ON)
        {
            ret |= UVP_CS_MASK_0;  
        }
        if (HAL_GPIO_ReadPin(CSB_ADD1_GPIO_Port, CSB_ADD1_Pin) == UVP_CS_ADDR_ON)
        {
            ret |= UVP_CS_MASK_1;  
        }
        if (HAL_GPIO_ReadPin(CSB_ADD2_GPIO_Port, CSB_ADD2_Pin) == UVP_CS_ADDR_ON)
        {
            ret |= UVP_CS_MASK_2;  
        }
        if (HAL_GPIO_ReadPin(CSB_ADD3_GPIO_Port, CSB_ADD3_Pin) == UVP_CS_ADDR_ON)
        {
            ret |= UVP_CS_MASK_3;
        }    
    }
    return ret;
}


/**
  * @brief  Enables or disables the TIM Capture Compare Channel xN.
  * @param  TIMx to select the TIM peripheral
  * @param  Channel specifies the TIM Channel
  *          This parameter can be one of the following values:
  *            @arg TIM_CHANNEL_1: TIM Channel 1
  *            @arg TIM_CHANNEL_2: TIM Channel 2
  *            @arg TIM_CHANNEL_3: TIM Channel 3
  * @param  ChannelNState specifies the TIM Channel CCxNE bit new state.
  *          This parameter can be: TIM_CCxN_ENABLE or TIM_CCxN_Disable.
  * @return None
  * @note   This function is borrowed from the ST HAL library
  */
static void TIM_CCxNChannelCmd(TIM_TypeDef *TIMx, uint32_t Channel, uint32_t ChannelNState)
{
    uint32_t tmp;

    tmp = TIM_CCER_CC1NE << (Channel & 0x1FU); /* 0x1FU = 31 bits max shift */

    /* Reset the CCxNE Bit */
    TIMx->CCER &=  ~tmp;

    /* Set or reset the CCxNE Bit */
    TIMx->CCER |= (uint32_t)(ChannelNState << (Channel & 0x1FU)); /* 0x1FU = 31 bits max shift */
}

/**
 * @brief Retrieves the number of valid AMB's
 * @return The number of valid AMB's
 */
uint8_t AmbValidCount(void)
{
    uint8_t count = 0;
    AmbNum_t amb;
    
    for (amb = 0; amb < MAX_NUM_AMB; amb++)
    {
        if (BIT_IN_MASK(amb, AmbGetValidAmbs()))
        {
            count++;
        }
    }
    return count;
}

/**
 * @brief Enable or disable the AMB_TX pin to the AMB's
 *
 * @param on If true, set AMB_TX pin high; otherwise set the pin low
 */
void AmbEnableTx(bool on)
{
    if (on)
    {
        // Enable timer channel outputs OC1 and OC1N corresponding to the Tx and Rx signals
        TIM_CCxChannelCmd(TIM1, TIM_CHANNEL_1, TIM_CCx_ENABLE);
        TIM_CCxNChannelCmd(TIM1, TIM_CHANNEL_1, TIM_CCxN_ENABLE);

        // Set polarity bits CC1P and CC1NP high to invert Rx and Tx signals (OC1 and OC1N timer outputs)
        TIM1->CCER |= TIM_CCER_CC1P;
        TIM1->CCER |= TIM_CCER_CC1NP;
        
        // Enable the main timer output
        __HAL_TIM_MOE_ENABLE(&htim1);
    }
    else
    {
        // Disable the main timer output
        __HAL_TIM_MOE_DISABLE_UNCONDITIONALLY(&htim1);

        // Disable the timer channel outputs OC1 and OC1N corresponding to the Tx and Rx signals
        TIM_CCxChannelCmd(TIM1, TIM_CHANNEL_1, TIM_CCx_DISABLE);
        TIM_CCxNChannelCmd(TIM1, TIM_CHANNEL_1, TIM_CCxN_DISABLE);

        // Reset the polarity flags for the Tx/Rx outputs
        TIM1->CCER &= ~TIM_CCER_CC1P;
        TIM1->CCER &= ~TIM_CCER_CC1NP;
    }
}

/**
 * @brief Enable or disable the AMB_RX pin to the AMB's
 *
 * @param on If true, set AMB_RX pin high; otherwise set the pin low
 */
void AmbEnableRx(bool on)
{
    if (on)
    {
        // Enable timer channel outputs OC1 and OC1N corresponding to the Tx and Rx signals
        TIM_CCxChannelCmd(TIM1, TIM_CHANNEL_1, TIM_CCx_ENABLE);
        TIM_CCxNChannelCmd(TIM1, TIM_CHANNEL_1, TIM_CCxN_ENABLE);

        // Enable the main timer output
        __HAL_TIM_MOE_ENABLE(&htim1);
    }
    else
    {
        // Disable the main timer output
        __HAL_TIM_MOE_DISABLE_UNCONDITIONALLY(&htim1);

        // Disable the timer channel outputs OC1 and OC1N corresponding to the Tx and Rx signals
        TIM_CCxChannelCmd(TIM1, TIM_CHANNEL_1, TIM_CCx_DISABLE);
        TIM_CCxNChannelCmd(TIM1, TIM_CHANNEL_1, TIM_CCxN_DISABLE);

        // Reset the polarity flags for the Tx/Rx outputs
        TIM1->CCER &= ~TIM_CCER_CC1P;
        TIM1->CCER &= ~TIM_CCER_CC1NP;
    }
}

/**
 * @brief Enable or disable the PU_ALL pin to the AMB's
 *
 * @param on If true, set PU_ALL pin high; otherwise set the pin low
 */
void AmbEnablePuAll(bool on)
{
    if (on)
    {
        // Enable OC3 timer output channel corresponding to PU_ALL signal
        TIM_CCxChannelCmd(TIM8, TIM_CHANNEL_3, TIM_CCx_ENABLE);
        
        // Set the MOE bit
        __HAL_TIM_MOE_ENABLE(&htim8);
    }
    else
    {
        // Disable the main timer output
        __HAL_TIM_MOE_DISABLE_UNCONDITIONALLY(&htim8);
        
        // Disable OC3 timer output channel corresponding to PU_ALL signal
        TIM_CCxChannelCmd(TIM8, TIM_CHANNEL_3, TIM_CCx_DISABLE);
        
        // Reset the OC3 output channel polarity
        TIM8->CCER &= ~TIM_CCER_CC3P;
    }
}

/**
 * @brief Enable or disable the WRMHLH_CSB pin to the AMB's
 *
 * @param on If true, set the WRMHLH_CSBe pin high, otherwise set the pin low.
 */
void AmbEnableWrmhlCsb(bool on)
{
    HAL_GPIO_WritePin(WRMHL_CSB_GPIO_Port, WRMHL_CSB_Pin, on ? WRMHL_CSB_ENABLED : WRMHL_CSB_DISABLED);  
}


/**
 * @brief Determines if Tx mode is enabled.
 *
 * @return true if AMB_TX pin is high, false if not.
 */
bool AmbInTxMode(void)
{
    return (HAL_GPIO_ReadPin(AMB_TX_GPIO_Port, AMB_TX_Pin) == TX_ON);
}

/**
 * @brief Determines if Rx mode is enabled.
 * 
 * @return true if AMB_RX pin is enabled, false if not.
 */
bool AmbInRxMode(void)
{
    return (HAL_GPIO_ReadPin(AMB_RX_GPIO_Port, AMB_RX_Pin) == RX_ON);
}

/**
 * @brief Determines if PU_ALL pin is high.
 * 
 * @return true if PU_ALL pin is high, false if not.
 */
bool AmbInPuAllMode(void)
{
    return (HAL_GPIO_ReadPin(PU_ALL_GPIO_Port, PU_ALL_Pin) == PU_ALL_ON);
}


/**
 * @brief Determines if WRMHL_CSB mode is enabled.
 * 
 * @return true if WRMHL_CSB pin is high, false if not.
 */
bool AmbIsWrmhlCsbEnabled(void)
{
    return (HAL_GPIO_ReadPin(WRMHL_CSB_GPIO_Port, WRMHL_CSB_Pin) == WRMHL_CSB_ENABLED);
}


/**
 * @brief Gets the state of the Phase Dedect for every AMB
 *
 * @return A mask specifying which state of PS update pins. 1 for phase detect done, 0 for phase detect in progress.
 */
AmbMask_t AmbGetPhaseDetect(void)
{
    uint8_t ii;
    AmbMask_t ambMask = 0;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (HAL_GPIO_ReadPin(ambInfo[ii].pdDonePort, ambInfo[ii].pdDonePin) == PD_DONE)
        {
            ambMask |= (1 << ii);
        }
    } 
    return ambMask;
}

/**
 * @brief  Retrieves the SPI handle for a single AMB
 * 
 * @param  ambNum The AMB number of the AMB
 *
 * @return A pointer to the SPI handle for the AMB
 */
SPI_HandleTypeDef* AmbGetSpiHandle(AmbNum_t ambNum)
{
    SPI_HandleTypeDef* spiHandle = NULL;

    if ( ambNum < MAX_NUM_AMB)
    {
        return ambInfo[ambNum].spi;
    }

    return spiHandle;
}

/**
 * @brief  Retrieves the SPI handle for the master SPI
 *
 * @return A pointer to the SPI handle for master SPI
 */
SPI_HandleTypeDef* AmbGetMasterSpiHandle(void)
{
    return masterSpi;
}
