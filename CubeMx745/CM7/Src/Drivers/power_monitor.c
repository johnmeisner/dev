/****************************************************************************//**
* @file      power_monitor.c
*
* @brief     Read thes PAC1934 the power monitors
*           
* @note      All of the PG3P3 lines have been removed from this module. The
*            reason for this is because of the hardware in the Orion system. At
*            the beginning of this project, out hardware engineer was concerned
*            about the amount of available pins the MCU. As a result, the PG3P3
*            line ('Power Good' for the 3.3V supply) was wired directly to the
*            Ethernet clock chip's enable line. This way, when the power on the
*            3.3v supply came up properly, the Ethernet clock would be sequenced
*            next to boot up.
*           
*            In hindsight, we had more than enough pins, and this made it
*            difficult to disable the Ethernet clock chip. Unfortunately we also
*            need to be able to disable the Ethernet clock chip for FCC testing of
*            the transmitter.
*           
*            Therefore, the solution was to de-populate a pull-up
*            resistor (R290) for the PG3P3 line. The 'PG' from the
*            3.3v supply is open-drain. The PG3P3 line becomes an
*            output instead of an input and can be used to disable the
*            Ethernet clock chip.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2020 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "FreeRTOS.h"
#include "semphr.h"
#include "string.h"
#include "main.h"
#include "sysclk_driver.h"
#include "orion_config.h"
#include "i2c_multiplex.h"
#include "ccb_temperature.h"
#include "amb_control.h"
#include "debug_off.h"
#include "power_monitor.h"
#include "cli_task.h"
#include "task.h"
#include "ctrl_interface.h"

#define PM_I2C_ADDR                 0x10    ///< The i2c address of the power monitors
#define PM_MULTIPLEX_NUM            0       ///< The number of the i2c multiplexer that the power monitors are connected
#define PM_AMB_MULTIPLEX_CHAN       2       ///< The channel for the AMB power monitors
#define PM_CCB_MULTIPLEX_CHAN       3       ///< The channel for CCB power monitors

#define PM_CTRL_OVF_POS             0       ///<  Inidcates an accuumlator has overflowed. 
#define PM_CTRL_OVF_ALERT_EN_POS    1       ///<  Enables the use of the ALERT pin to indicate overflows
#define PM_CTRL_ALERT_CC_EN_POS     2       ///<  Causes ALERT to be asserted at the end of conversion cycle
//@TODO should we tie the SLOW pin low, to enable maximum sampling by default?
#define PM_CTRL_ALERT_EN_POS        3       ///<  Transitions alert pin from SLOW ena ble/disable
#define PM_CTRL_SINGLE_SHOT_EN      4       ///<  Puts the device in single shot.  Need to send REFRESH to start the accumulators.
#define PM_CTRL_SLEEP_EN            5       ///<  Stops sampling
#define PM_CTRL_SAMPLE_RATE_POS     6       ///<  The lowest bit for the sample rate
#define PM_CTRL_SAMPLE_RATE_MASK    0x3     ///<  To bit mask for sample rate

#define PM_CHAN_DIS_1_POS           7       ///<  Disables channel 1
#define PM_CHAN_DIS_2_POS           6       ///<  Disables channel 2
#define PM_CHAN_DIS_3_POS           5       ///<  Disables channel 3
#define PM_CHAN_DIS_4_POS           4       ///<  Disables channel 4
#define PM_BUS_TIMEOUT_POS          3       ///<  Enables the timeout feature on bus
#define PM_BUS_NO_SKIP              2       ///<  Cause byte count data to be included in the to a block read.

#define PM_NEG_PWR_BIDI_SENSE1_POS  7       ///< Enable bidirectional reading for SENSE1
#define PM_NEG_PWR_BIDI_SENSE2_POS  6       ///< Enable bidirectional reading for SENSE2
#define PM_NEG_PWR_BIDI_SENSE3_POS  5       ///< Enable bidirectional reading for SENSE3
#define PM_NEG_PWR_BIDI_SENSE4_POS  4       ///< Enable bidirectional reading for SENSE4
#define PM_NEG_PWR_BIDI_VBUS1_POS   3       ///< Enable bidirectional reading for VBUS1
#define PM_NEG_PWR_BIDI_VBUS2_POS   2       ///< Enable bidirectional reading for VBUS2
#define PM_NEG_PWR_BIDI_VBUS3_POS   1       ///< Enable bidirectional reading for VBUS3
#define PM_NEG_PWR_BIDI_VBUS4_POS   0       ///< Enable bidirectional reading for VBUS4


#define PM_READ_WRITE_TIMEOUT        100                    ///< The power monitor read write timeout
#define PM_MAX_REG_SIZE              6                      ///< The maximum size of a power manager register
#define PM_SENSE_FULL_RANGE          ((uint64_t)100)        ///< Maximum value in milliVolts for VSENSE. VSENSE register values are relative to this value.
#define PM_VBUS_FULL_RANGE           ((uint64_t)32)         ///< Maximum value Volts for VBUS. VBUF register values are relative to this value.
#define PM_SENSE_RESITOR_INV         ((uint64_t)100)        ///< The sense register is 0.01 Ohms, this represent the inversion of the value.
#define PM_POWER_ACC_MAX             (((uint64_t)1) << 28)  ///< Maximum digital accumlator value.
#define PM_COUNT_REG_SIZE            3                      ///< The size in bytes of the accumulator count registers

#define POWER_GOOD_PIN_GOOD          GPIO_PIN_SET           ///< The value of GPIO when power id good.
#define POWER_GOOD_WAIT_INCREMENT_MS 5                      ///< When waiting for power good, this is how long we wait between checks.

/** Macro mapping power good result to string */
#define POWER_GOOD_STRING(gpioName) ((HAL_GPIO_ReadPin(gpioName##_GPIO_Port, gpioName##_Pin) == POWER_GOOD_PIN_GOOD) ? "Yes" : "No")
/** Macro mapping power good to true or false */
#define POWER_GOOD(gpioName) ((HAL_GPIO_ReadPin(gpioName##_GPIO_Port, gpioName##_Pin) == POWER_GOOD_PIN_GOOD))

#define POWER_GOOD_BIT(gpioName) ((HAL_GPIO_ReadPin(gpioName##_GPIO_Port, gpioName##_Pin) == POWER_GOOD_PIN_GOOD) ? NUM_TO_MASK(gpioName##_BIT) : 0)
#define POWER_ETH_BIT(gpioName) ((HAL_GPIO_ReadPin(gpioName##_GPIO_Port, gpioName##_Pin) == GPIO_PIN_SET) ? NUM_TO_MASK(gpioName##_BIT) : 0)
#define POWER_ETH_STRING(gpioName) ((HAL_GPIO_ReadPin(gpioName##_GPIO_Port, gpioName##_Pin) == GPIO_PIN_SET) ? "on" : "off")
/**
 *  @brief Power monitor registers
 */
typedef enum _PmRegAddr_t
{
    PM_REG_REFRESH      =   0x00,   ///< Restart or start the Accumulators (no need to send value, just need to touch it)
    PM_REG_CTRL         =   0x01,   ///< The location of the configuration register
    PM_REG_ACC_COUNT    =   0x02,   ///< The number of sample since the last refresh
    PM_V_POW1_ACC       =   0x03,   ///< The VPOWER accumulator for sense 1
    PM_V_POW2_ACC       =   0x04,   ///< The VPOWER accumulator for sense 2
    PM_V_POW3_ACC       =   0x05,   ///< The VPOWER accumulator for sense 3
    PM_V_POW4_ACC       =   0x06,   ///< The VPOWER accumulator for sense 4
    PM_V_BUS1           =   0x07,   ///< The most recent sample voltage on the sense1+ line
    PM_V_BUS2           =   0x08,   ///< The most recent sample voltage on the sense2+ line
    PM_V_BUS3           =   0x09,   ///< The most recent sample voltage on the sense3+ line
    PM_V_BUS4           =   0x0a,   ///< The most recent sample voltage on the sense4+ line
    PM_V_SENSE1         =   0x0b,   ///< The most recent sample of voltage over the sense1 resistor 
    PM_V_SENSE2         =   0x0c,   ///< The most recent sample of voltage over the sense2 resistor 
    PM_V_SENSE3         =   0x0d,   ///< The most recent sample of voltage over the sense3 resistor 
    PM_V_SENSE4         =   0x0e,   ///< The most recent sample of voltage over the sense4 resistor
    PM_V_AVG_VBUS1      =   0x0f,   ///< A rolling 8 sample average of the VBUS registers for sense1+
    PM_V_AVG_VBUS2      =   0x10,   ///< A rolling 8 sample average of the VBUS registers for sense2+
    PM_V_AVG_VBUS3      =   0x11,   ///< A rolling 8 sample average of the VBUS registers for sense3+
    PM_V_AVG_VBUS4      =   0x12,   ///< A rolling 8 sample average of the VBUS registers for sense4+
    PM_V_AVG_VSENSE1    =   0x13,   ///< A rolling 8 sample average of the VSENSE registers for sense1+
    PM_V_AVG_VSENSE2    =   0x14,   ///< A rolling 8 sample average of the VSENSE registers for sense2+
    PM_V_AVG_VSENSE3    =   0x15,   ///< A rolling 8 sample average of the VSENSE registers for sense3+
    PM_V_AVG_VSENSE4    =   0x16,   ///< A rolling 8 sample average of the VSENSE registers for sense4+
    PM_V_AVG_VPOWER1    =   0x17,   ///< Accumulates the product of Vsense and V bus for sense1, which is porportional to power.
    PM_V_AVG_VPOWER2    =   0x18,   ///< Accumulates the product of Vsense and V bus for sense2, which is porportional to power.
    PM_V_AVG_VPOWER3    =   0x19,   ///< Accumulates the product of Vsense and V bus for sense3, which is porportional to power.
    PM_V_AVG_VPOWER4    =   0x1a,   ///< Accumulates the product of Vsense and V bus for sense4, which is porportional to power.   
    PM_CHAN_DIS_AND_BUS =   0x1c,   ///< Controls which channels sample, and some I2C features.
    PM_NEG_PWR_EN       =   0x1d,   ///< Enables negative measurements for power and voltage for various channels
    PM_REG_REFRESH_G    =   0x1e,   ///< Restart or start the Accumulators (no need to send value, just need to touch it)
    PM_REG_REFRESH_V    =   0x1f,   ///< Refresh the values that are read through the registers
    PM_SLOW             =   0x20,   ///< Returns the status of slow pin.
    PM_CTRL_ACT         =   0x21,   ///< A mirror to the #PM_REG_CTRL which is updated after the conversion is complete.
    PM_DIS_ACT          =   0x22,   ///< Mirror upper 4 bits of the #PM_CHAN_DIS_AND_BUS
    PM_NEG_PWR_ACT      =   0x23,   ///< Mirrors $PM_NEG_PWR
    PM_CTRL_LAT         =   0x24,   ///< Maintains the value #PM_REG_CTRL before the refresh
    PM_DIS_LAT          =   0x25,   ///< Maintains the value #PM_DIS_LAT before the refresh
    PM_NEG_PWR          =   0x26,   ///< Maintains the value #PM_NEG_PWR before the refresh
    PM_PRODUCT_ID       =   0xfd,   ///< The product id of the chip
    PM_MAN_ID           =   0xfe,   ///< The manufacturer id of the chip
    PM_REV_ID           =   0xff,   ///< The revision id of the chip.
} PmRegAddr_t;

/**
 * @brief Sample rate setings
 */
typedef enum _PmSampleRate_t
{
    PM_SAMPLE_RATE_1024    = 0x0,    ///< Sample rate for 1024 sample/s    
    PM_SAMPLE_RATE_256     = 0x1,    ///< Sample rate for 256 sample/s    
    PM_SAMPLE_RATE_64      = 0x2,    ///< Sample rate for 64 sample/s    
    PM_SAMPLE_RATE_8       = 0x3     ///< Sample rate for 8 sample/s    
} PmSampleRate_t;

static I2CMultiplexerInfo_t     gPmAmbMultiInfo; ///< Holds the I2C muliplexer info for the AMBs
static I2CMultiplexerInfo_t     gPmCcbMultiInfo; ///< Holds the I2C muliplexer info for the CCB
/**
 * @brief Initializes the multiplexer structure for each power monitor sensor
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
CotaError_t InitializeCcbPowerMonitors(void)
{
    CotaError_t ret = COTA_ERROR_NONE;
    
    ret = InitI2CMultiplexer(&gPmCcbMultiInfo, PM_MULTIPLEX_NUM, PM_CCB_MULTIPLEX_CHAN, PM_I2C_ADDR);      
    ret = (ret == COTA_ERROR_NONE) ? InitI2CMultiplexer(&gPmAmbMultiInfo, PM_MULTIPLEX_NUM, PM_AMB_MULTIPLEX_CHAN, PM_I2C_ADDR) : ret;      
  
    return ret;  
}

/**
 *  @brief This will refresh all acccumulators and their counting registers.
 *
 *  @param pmInfo     Pointer to the multiplex control structure of type #I2CMultiplexerInfo_t
 *
 *  @note Need to wait 1ms after this call before value will be ready to read
 *
 *  @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
static CotaError_t RefreshPowerAcc(I2CMultiplexerInfo_t* pmInfo)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint8_t refreshByte = PM_REG_REFRESH;
    
    ret = I2CMultiplexWrite(pmInfo, &refreshByte, sizeof(refreshByte), PM_READ_WRITE_TIMEOUT);
    
    return ret;
}

/**
 *  @brief Fully refreshes power monitor chips and zeros out their accumulators
 *
 *  @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
CotaError_t RefreshPowerMonitors(void)
{   
    CotaError_t ret = COTA_ERROR_NONE;
    
    ret = RefreshPowerAcc(&gPmCcbMultiInfo);

    ret = (ret == COTA_ERROR_NONE) ? RefreshPowerAcc(&gPmAmbMultiInfo) : ret;
    
    return ret;  
}

/**
 *  @brief Determines if all the power supplies are good
 *
 *  @return true; all supplies are good.  false;  At least one supply is not good.
 */
bool IsPowerGood(void)
{
    bool powerGood = POWER_GOOD(PG1P8);
    
    powerGood = powerGood ? POWER_GOOD(PG1P8) : powerGood;
    // See notes in this module's file description for why this is commented out
    // powerGood = powerGood ? POWER_GOOD(PG3P3) : powerGood;
    powerGood = powerGood ? POWER_GOOD(PG5P0) : powerGood;
    powerGood = powerGood ? POWER_GOOD(PGCCBA) : powerGood;
    powerGood = powerGood ? POWER_GOOD(PGCCBB) : powerGood;
    powerGood = powerGood ? POWER_GOOD(PGPAA) : powerGood;
    powerGood = powerGood ? POWER_GOOD(PGPAB) : powerGood;
  
    return powerGood;
}

/**
 * @brief Waits until all the power good pins are set
 *
 * @param timeoutMs Timeout in milliseconds
 *
 * @return true, if power is good; false, if a timeout occurred
 */
bool WaitForPowerGood(uint16_t timeoutMs)
{
    uint32_t numIters = (timeoutMs/POWER_GOOD_WAIT_INCREMENT_MS) + 1;
    
    while((!IsPowerGood()) && (numIters > 0))
    {
        vTaskDelay(pdMS_TO_TICKS(POWER_GOOD_WAIT_INCREMENT_MS));
        numIters--;      
    }

    return IsPowerGood();
}
