/**
 * @file       gpio_interrupt.h
 *
 * @brief      GPIO interrupt processing module
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 */

#ifndef GPIO_INTERRUPT_H
#define GPIO_INTERRUPT_H

#include "FreeRTOS.h"

#define GPIO_INT_CLEAR  true  ///< A value that can be used as the clearFlags parameter with the GpioGetInterruptFlags() function

typedef uint32_t GpioInterruptFlagMask_t;  ///< Interrupt flag mask for interrupts in #GpioInterruptLines_t


/** Enumeration of all interrupts we can report from this module */
typedef enum _GpioInterruptLines_t
{
    GPIO_INT_PROXY_BEACON_1,  ///< Interrupt on PROXY_BEACON1_INT line
    GPIO_INT_PROXY_BEACON_2,  ///< Interrupt on PROXY_BEACON2_INT line
    GPIO_INT_COUNT            ///< Total number of GPIO interrupt lines
} GpioInterruptLines_t;


GpioInterruptFlagMask_t GpioGetInterruptFlags(bool clearFlags);
char * GetInterruptFlagDescription(GpioInterruptLines_t intLine);
void ProcessInterruptFlags(void);

#endif  // #ifndef GPIO_INTERRUPT_H
