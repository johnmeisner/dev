/****************************************************************************//**
* @file      orion_utilities.h
*
* @brief     A file that holds general utilities used by the orion system
* 
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2020 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "orion_utilities.h"

/**
 * @brief Determines difference between two times in ticks
 *
 * @note  Because ticks are unsigned 32-bit integers, there's no need for special
 *        logic for overflow condition handling.
 *
 * @note  Because the tick counter rolls over after 0xFFFFFFFF (it's a 32-bit int),
 *        the maximum possible time difference is 49 days assuming a tick period
 *        is equal to 1 millisecond.
 *
 * @param newTimeTicks  The newer time value in ticks
 * @param oldTimeTicks  The older time value in ticks
 *
 * @return The time difference in ticks
 */
uint32_t TimeDiffTicks(uint32_t newTimeTicks, uint32_t oldTimeTicks)
{
    return newTimeTicks - oldTimeTicks;
}
