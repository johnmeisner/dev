/****************************************************************************//**
* @file      uvp_driver.h
*
* @brief     Header file to read and write to uvp's (The asic that control receive and transmission
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _UVP_DRIVER_H_
#define _UVP_DRIVER_H_
#include "error.h"
#include "amb_control.h"
#include "ctrl_interface.h"
/**
 * The following are register definitions that are passed to UvpRegisterRead and
 * UvpRegisterWrite.
 * Currently we are using SWS1411 UVPv2 Variant 1, but these definition also apply to Variant 3
 */
#define  ENCODE_SIZE                      (UvpReg_t){ 3,  12,  6,  0,  0, 0, 0,  12 }
#define  DETECT_PH_CLIENT_ID_1            (UvpReg_t){ 4,  16,  3,  0,  0, 0, 0,  16 }
#define  CLIENT_SELECT_OUT                (UvpReg_t){ 4,  16,  3,  0,  0, 0, 3,  16 }
#define  PHASE_DETECT_DONE                (UvpReg_t){ 5,  20,  1,  0,  1, 0, 4,  20 }
#define  SEND_POWER_RDY                   (UvpReg_t){ 5,  20,  1,  0,  1, 0, 5,  20 }
#define  RSSi_AMU1                        (UvpReg_t){ 5,  20,  13, 0,  1, 0, 6,  20 }
#define  RSSi_AMU2                        (UvpReg_t){ 5,  22,  13, 0,  1, 0, 19, 20 }
#define  RSSi_AMU3                        (UvpReg_t){ 6,  24,  13, 0,  1, 0, 0,  24 }
#define  RSSi_AMU4                        (UvpReg_t){ 6,  25,  13, 0,  1, 0, 13, 24 }
#define  ADC_SAT                          (UvpReg_t){ 6,  27,  4,  0,  1, 0, 26, 24 }
#define  SYSCTRL_STATE                    (UvpReg_t){ 9,  39,  3,  0,  1, 0, 29, 36 }
#define  ARB_PHASE_SEL                    (UvpReg_t){ 9,  36,  8,  0,  0, 0, 0,  36 }
#define  ARBIT_PHASE_AMU1_CLNT1           (UvpReg_t){ 10, 40,  9,  0,  0, 0, 0,  40 }
#define  ARBIT_PHASE_AMU1_CLNT2           (UvpReg_t){ 10, 41,  9,  0,  0, 0, 9,  40 }
#define  ARBIT_PHASE_AMU1_CLNT3           (UvpReg_t){ 10, 42,  9,  0,  0, 0, 18, 40 }
#define  ARBIT_PHASE_AMU1_CLNT4           (UvpReg_t){ 11, 44,  9,  0,  0, 0, 0,  44 }
#define  ARBIT_PHASE_AMU1_CLNT5           (UvpReg_t){ 11, 45,  9,  0,  0, 0, 9,  44 }
#define  ARBIT_PHASE_AMU1_CLNT6           (UvpReg_t){ 11, 46,  9,  0,  0, 0, 18, 44 }
#define  ARBIT_PHASE_AMU1_CLNT7           (UvpReg_t){ 12, 48,  9,  0,  0, 0, 0,  48 }
#define  ARBIT_PHASE_AMU1_CLNT8           (UvpReg_t){ 12, 49,  9,  0,  0, 0, 9,  48 }
#define  ARBIT_PHASE_AMU2_CLNT1           (UvpReg_t){ 12, 50,  9,  0,  0, 0, 18, 48 }
#define  ARBIT_PHASE_AMU2_CLNT2           (UvpReg_t){ 13, 52,  9,  0,  0, 0, 0,  52 }
#define  ARBIT_PHASE_AMU2_CLNT3           (UvpReg_t){ 13, 53,  9,  0,  0, 0, 9,  52 }
#define  ARBIT_PHASE_AMU2_CLNT4           (UvpReg_t){ 13, 54,  9,  0,  0, 0, 18, 52 }
#define  ARBIT_PHASE_AMU2_CLNT5           (UvpReg_t){ 14, 56,  9,  0,  0, 0, 0,  56 }
#define  ARBIT_PHASE_AMU2_CLNT6           (UvpReg_t){ 14, 57,  9,  0,  0, 0, 9,  56 }
#define  ARBIT_PHASE_AMU2_CLNT7           (UvpReg_t){ 14, 58,  9,  0,  0, 0, 18, 56 }
#define  ARBIT_PHASE_AMU2_CLNT8           (UvpReg_t){ 15, 60,  9,  0,  0, 0, 0,  60 }
#define  ARBIT_PHASE_AMU3_CLNT1           (UvpReg_t){ 15, 61,  9,  0,  0, 0, 9,  60 }
#define  ARBIT_PHASE_AMU3_CLNT2           (UvpReg_t){ 15, 62,  9,  0,  0, 0, 18, 60 }
#define  ARBIT_PHASE_AMU3_CLNT3           (UvpReg_t){ 16, 64,  9,  0,  0, 0, 0,  64 }
#define  ARBIT_PHASE_AMU3_CLNT4           (UvpReg_t){ 16, 65,  9,  0,  0, 0, 9,  64 }
#define  ARBIT_PHASE_AMU3_CLNT5           (UvpReg_t){ 16, 66,  9,  0,  0, 0, 18, 64 }
#define  ARBIT_PHASE_AMU3_CLNT6           (UvpReg_t){ 17, 68,  9,  0,  0, 0, 0,  68 }
#define  ARBIT_PHASE_AMU3_CLNT7           (UvpReg_t){ 17, 69,  9,  0,  0, 0, 9,  68 }
#define  ARBIT_PHASE_AMU3_CLNT8           (UvpReg_t){ 17, 70,  9,  0,  0, 0, 18, 68 }
#define  ARBIT_PHASE_AMU4_CLNT1           (UvpReg_t){ 18, 72,  9,  0,  0, 0, 0,  72 }
#define  ARBIT_PHASE_AMU4_CLNT2           (UvpReg_t){ 18, 73,  9,  0,  0, 0, 9,  72 }
#define  ARBIT_PHASE_AMU4_CLNT3           (UvpReg_t){ 18, 74,  9,  0,  0, 0, 18, 72 }
#define  ARBIT_PHASE_AMU4_CLNT4           (UvpReg_t){ 19, 76,  9,  0,  0, 0, 0,  76 }
#define  ARBIT_PHASE_AMU4_CLNT5           (UvpReg_t){ 19, 77,  9,  0,  0, 0, 9,  76 }
#define  ARBIT_PHASE_AMU4_CLNT6           (UvpReg_t){ 19, 78,  9,  0,  0, 0, 18, 76 }
#define  ARBIT_PHASE_AMU4_CLNT7           (UvpReg_t){ 20, 80,  9,  0,  0, 0, 0,  80 }
#define  ARBIT_PHASE_AMU4_CLNT8           (UvpReg_t){ 20, 81,  9,  0,  0, 0, 9,  80 }
#define  TEMPADC_OUT                      (UvpReg_t){ 20, 82,  13, 0,  1, 0, 18, 80 }
#define  SYSNOTRESETED                    (UvpReg_t){ 20, 83,  1,  0,  0, 0, 31, 80 }
#define  UVP_RXCTRL                       (UvpReg_t){ 29, 119, 1,  0,  0, 0, 30, 116}
#define  UVP_TXCTRL                       (UvpReg_t){ 29, 119, 1,  0,  0, 0, 31, 116}
#define  UVP_PUCTRL                       (UvpReg_t){ 30, 120, 1,  0,  0, 0, 0,  120}
#define  REGFILE_PAGE                     (UvpReg_t){ 31, 127, 8,  0,  0, 0, 24, 124}
#define  OUTPHASE_CLNT1_AMU1              (UvpReg_t){ 32, 128, 10, 0,  1, 1, 0,  0 }
#define  OUTPHASE_CLNT2_AMU1              (UvpReg_t){ 32, 129, 10, 0,  1, 1, 10, 0 }
#define  OUTPHASE_CLNT3_AMU1              (UvpReg_t){ 32, 130, 10, 0,  1, 1, 20, 0 }
#define  OUTPHASE_CLNT4_AMU1              (UvpReg_t){ 33, 132, 10, 0,  1, 1, 0,  4 }
#define  OUTPHASE_CLNT5_AMU1              (UvpReg_t){ 33, 133, 10, 0,  1, 1, 10, 4 }
#define  OUTPHASE_CLNT6_AMU1              (UvpReg_t){ 33, 134, 10, 0,  1, 1, 20, 4 }
#define  OUTPHASE_CLNT7_AMU1              (UvpReg_t){ 34, 136, 10, 0,  1, 1, 0,  8 }
#define  OUTPHASE_CLNT8_AMU1              (UvpReg_t){ 34, 137, 10, 0,  1, 1, 10, 8 }
#define  OUTPHASE_CLNT1_AMU2              (UvpReg_t){ 34, 138, 10, 0,  1, 1, 20, 8 }
#define  OUTPHASE_CLNT2_AMU2              (UvpReg_t){ 35, 140, 10, 0,  1, 1, 0,  12 }
#define  OUTPHASE_CLNT3_AMU2              (UvpReg_t){ 35, 141, 10, 0,  1, 1, 10, 12 }
#define  OUTPHASE_CLNT4_AMU2              (UvpReg_t){ 35, 142, 10, 0,  1, 1, 20, 12 }
#define  OUTPHASE_CLNT5_AMU2              (UvpReg_t){ 36, 144, 10, 0,  1, 1, 0,  16 }
#define  OUTPHASE_CLNT6_AMU2              (UvpReg_t){ 36, 145, 10, 0,  1, 1, 10, 16 }
#define  OUTPHASE_CLNT7_AMU2              (UvpReg_t){ 36, 146, 10, 0,  1, 1, 20, 16 }
#define  OUTPHASE_CLNT8_AMU2              (UvpReg_t){ 37, 148, 10, 0,  1, 1, 0,  20 }
#define  OUTPHASE_CLNT1_AMU3              (UvpReg_t){ 37, 149, 10, 0,  1, 1, 10, 20 }
#define  OUTPHASE_CLNT2_AMU3              (UvpReg_t){ 37, 150, 10, 0,  1, 1, 20, 20 }
#define  OUTPHASE_CLNT3_AMU3              (UvpReg_t){ 38, 152, 10, 0,  1, 1, 0,  24 }
#define  OUTPHASE_CLNT4_AMU3              (UvpReg_t){ 38, 153, 10, 0,  1, 1, 10, 24 }
#define  OUTPHASE_CLNT5_AMU3              (UvpReg_t){ 38, 154, 10, 0,  1, 1, 20, 24 }
#define  OUTPHASE_CLNT6_AMU3              (UvpReg_t){ 39, 156, 10, 0,  1, 1, 0,  28 }
#define  OUTPHASE_CLNT7_AMU3              (UvpReg_t){ 39, 157, 10, 0,  1, 1, 10, 28 }
#define  OUTPHASE_CLNT8_AMU3              (UvpReg_t){ 39, 158, 10, 0,  1, 1, 20, 28 }
#define  OUTPHASE_CLNT1_AMU4              (UvpReg_t){ 40, 160, 10, 0,  1, 1, 0,  32 }
#define  OUTPHASE_CLNT2_AMU4              (UvpReg_t){ 40, 161, 10, 0,  1, 1, 10, 32 }
#define  OUTPHASE_CLNT3_AMU4              (UvpReg_t){ 40, 162, 10, 0,  1, 1, 20, 32 }
#define  OUTPHASE_CLNT4_AMU4              (UvpReg_t){ 41, 164, 10, 0,  1, 1, 0,  36 }
#define  OUTPHASE_CLNT5_AMU4              (UvpReg_t){ 41, 165, 10, 0,  1, 1, 10, 36 }
#define  OUTPHASE_CLNT6_AMU4              (UvpReg_t){ 41, 166, 10, 0,  1, 1, 20, 36 }
#define  OUTPHASE_CLNT7_AMU4              (UvpReg_t){ 42, 168, 10, 0,  1, 1, 0,  40 }
#define  OUTPHASE_CLNT8_AMU4              (UvpReg_t){ 42, 169, 10, 0,  1, 1, 10, 40 }
#define  PHDETECT_PIN_SEL                 (UvpReg_t){ 46, 187, 2,  0,  0, 1, 26, 56 }
#define  SYS_RST                          (UvpReg_t){ 46, 187, 1,  0,  0, 1, 28, 56 }
#define  LF_LOCKDET                       (UvpReg_t){ 46, 187, 1,  0,  1, 1, 29, 56 }
#define  LF_LOCKHI                        (UvpReg_t){ 46, 187, 1,  0,  1, 1, 30, 56 }
#define  RSSI_GAIN_SCALE                  (UvpReg_t){ 50, 200, 11, 0,  0, 1, 0,  72 }
#define  PA_POWERON_DEL                   (UvpReg_t){ 50, 201, 10, 0,  0, 1, 11, 72 }
#define  PA_OFFSET_DEL                    (UvpReg_t){ 50, 202, 5,  0,  0, 1, 21, 72 }
#define  PA_SEQUENCE_DEL                  (UvpReg_t){ 50, 203, 5,  0,  0, 1, 26, 72 }
#define  PA_SEQUENCE                      (UvpReg_t){ 50, 203, 1,  0,  0, 1, 31, 72 }
#define  PA_COUNTER_VALID                 (UvpReg_t){ 51, 204, 1,  0,  0, 1, 0,  76 }
#define  TEMPADCEN                        (UvpReg_t){ 51, 204, 1,  0,  0, 1, 1,  76 }
#define  LOWIF0P25FSEN                    (UvpReg_t){ 51, 204, 1,  1,  0, 1, 2,  76 }
#define  LOWIF_EN                         (UvpReg_t){ 51, 204, 1,  1,  0, 1, 3,  76 }
#define  LOWIF0P25FSPH_I                  (UvpReg_t){ 51, 204, 7,  1,  0, 1, 4,  76 }
#define  LOWIF0P25FSPW_I                  (UvpReg_t){ 51, 205, 7,  65, 0, 1, 11, 76 }
#define  LOWIF0P25FSPH_Q                  (UvpReg_t){ 51, 206, 7,  33, 0, 1, 18, 76 }
#define  LOWIF0P25FSPW_Q                  (UvpReg_t){ 51, 207, 7,  97, 0, 1, 25, 76 }
#define  LOWIF_I_SYMBLI                   (UvpReg_t){ 52, 208, 8,  0,  0, 1, 0,  80 }
#define  LOWIF_I_SYMBLQ                   (UvpReg_t){ 52, 209, 8,  0,  0, 1, 8,  80 }
#define  LOWIF_Q_SYMBLI                   (UvpReg_t){ 52, 210, 8,  0,  0, 1, 16, 80 }
#define  LOWIF_Q_SYMBLQ                   (UvpReg_t){ 52, 211, 8,  0,  0, 1, 24, 80 }
#define  SEL_PHASE_CONJUGATE              (UvpReg_t){ 53, 212, 1,  0,  0, 1, 0,  84 }
#define  ADCINTF_I_OFFST_AMU1             (UvpReg_t){ 53, 212, 13, 0,  0, 1, 1,  84 }
#define  PHASE_OFFSET_AMU1                (UvpReg_t){ 53, 213, 9,  0,  0, 1, 14, 84 }
#define  PHASE_OFFSET_AMU2                (UvpReg_t){ 53, 214, 9,  0,  0, 1, 23, 84 }
#define  PHASE_OFFSET_AMU3                (UvpReg_t){ 54, 216, 9,  0,  0, 1, 0,  88 }
#define  PHASE_OFFSET_AMU4                (UvpReg_t){ 54, 217, 9,  0,  0, 1, 9,  88 }
#define  ADCINTF_Q_OFFST_AMU1             (UvpReg_t){ 54, 218, 13, 0,  0, 1, 18, 88 }
#define  CORRELATOR_EN                    (UvpReg_t){ 54, 219, 1,  1,  0, 1, 31, 88 }
#define  AMU_PHASE_CALIBRATE              (UvpReg_t){ 55, 220, 4,  0,  0, 1, 1,  92 }
#define  ADCINTF_I_OFFST_AMU2             (UvpReg_t){ 55, 220, 13, 0,  0, 1, 5,  92 }
#define  ADCINTF_I_GAIN_AMU1              (UvpReg_t){ 55, 222, 7,  1,  0, 1, 18, 92 }
#define  ADCINTF_Q_GAIN_AMU1              (UvpReg_t){ 55, 223, 7,  1,  0, 1, 25, 92 }
#define  ADCINTF_I_GAIN_AMU2              (UvpReg_t){ 56, 224, 7,  1,  0, 1, 0,  96 }
#define  ADCINTF_Q_GAIN_AMU2              (UvpReg_t){ 56, 224, 7,  1,  0, 1, 7,  96 }
#define  ADCINTF_Q_OFFST_AMU2             (UvpReg_t){ 56, 225, 13, 0,  0, 1, 14, 96 }
#define  ADCINTF_I_GAIN_AMU3              (UvpReg_t){ 57, 228, 7,  1,  0, 1, 5,  100 }
#define  ADCINTF_Q_GAIN_AMU3              (UvpReg_t){ 57, 229, 7,  1,  0, 1, 12, 100 }
#define  ADCINTF_I_OFFST_AMU3             (UvpReg_t){ 57, 230, 13, 0,  0, 1, 19, 100 }
#define  ADCINTF_I_GAIN_AMU4              (UvpReg_t){ 58, 232, 7,  1,  0, 1, 0,  104 }
#define  ADCINTF_Q_GAIN_AMU4              (UvpReg_t){ 58, 232, 7,  1,  0, 1, 7,  104 }
#define  ADCINTF_Q_OFFST_AMU3             (UvpReg_t){ 58, 233, 13, 0,  0, 1, 14, 104 }
#define  ADCCLKEN                         (UvpReg_t){ 58, 234, 1,  1,  0, 1, 27, 104 }      
#define  PHIDIGFSEN                       (UvpReg_t){ 58, 235, 1,  1,  0, 1, 28, 104 }   
#define  CODECLKEN                        (UvpReg_t){ 58, 235, 1,  1,  0, 1, 29, 104 }   
#define  CLKGEN_EN                        (UvpReg_t){ 58, 235, 1,  1,  0, 1, 30, 104 }   
#define  EN_AMU1                          (UvpReg_t){ 58, 235, 1,  1,  0, 1, 31, 104 }
#define  EN_AMU2                          (UvpReg_t){ 59, 236, 1,  1,  0, 1, 0,  108 }
#define  EN_AMU3                          (UvpReg_t){ 59, 236, 1,  1,  0, 1, 1,  108 }
#define  EN_AMU4                          (UvpReg_t){ 59, 236, 1,  1,  0, 1, 2,  108 }
#define  CODECLK_PH                       (UvpReg_t){ 59, 236, 7,  1,  0, 1, 3,  108 }   
#define  ADCINTF_I_OFFST_AMU4             (UvpReg_t){ 59, 238, 13, 0,  0, 1, 17, 108 }
#define  ADCINTF_Q_OFFST_AMU4             (UvpReg_t){ 60, 240, 13, 0,  0, 1, 0,  112 }
#define  PAD_SPI_IN_PD                    (UvpReg_t){ 61, 244, 1,  0,  0, 1, 0,  116 }
#define  PAD_SPI_IN_CS                    (UvpReg_t){ 61, 244, 1,  0,  0, 1, 1,  116 }
#define  PAD_SPI_OUT_PU                   (UvpReg_t){ 61, 244, 1,  0,  0, 1, 2,  116 }
#define  PAD_SPI_OUT_PD                   (UvpReg_t){ 61, 244, 1,  0,  0, 1, 3,  116 }
#define  PAD_SPI_OUT_CS                   (UvpReg_t){ 61, 244, 1,  0,  0, 1, 4,  116 }
#define  PAD_CSB_CS                       (UvpReg_t){ 61, 244, 1,  0,  0, 1, 5,  116 }
#define  PAD_CSB_PU                       (UvpReg_t){ 61, 244, 1,  0,  0, 1, 6,  116 }
#define  PAD_CSB_PD                       (UvpReg_t){ 61, 244, 1,  0,  0, 1, 7,  116 }
#define  PAD_PADOUT_SL                    (UvpReg_t){ 61, 245, 1,  0,  0, 1, 8,  116 }
#define  PAD_PADOUT_PDRV0                 (UvpReg_t){ 61, 245, 1,  0,  0, 1, 9,  116 }
#define  PAD_PADOUT_PDRV1                 (UvpReg_t){ 61, 245, 1,  0,  0, 1, 10, 116 }
#define  VCOBUFFEN_IDLE                   (UvpReg_t){ 61, 245, 1,  1,  0, 1, 11, 116 }
#define  VCOBUFFEN_RXRDY                  (UvpReg_t){ 61, 245, 1,  1,  0, 1, 12, 116 }
#define  VCOBUFFEN_PHDETECT               (UvpReg_t){ 61, 245, 1,  1,  0, 1, 13, 116 }
#define  VCOBUFFEN_TXRDY                  (UvpReg_t){ 61, 245, 1,  1,  0, 1, 14, 116 }
#define  VCOBUFFEN_SENDPWR                (UvpReg_t){ 61, 245, 1,  1,  0, 1, 15, 116 }
#define  NDIVSEL                          (UvpReg_t){ 61, 246, 1,  0,  0, 1, 16, 116 }
#define  TMPSNSCMBUFFEN                   (UvpReg_t){ 61, 246, 1,  0,  0, 1, 17, 116 }
#define  TMPSNSEN                         (UvpReg_t){ 61, 246, 1,  0,  0, 1, 18, 116 }
#define  RESERVED09                       (UvpReg_t){ 61, 246, 1,  1,  0, 1, 19, 116 }
#define  TMPSNSCOMPENCNT                  (UvpReg_t){ 61, 246, 5,  4,  0, 1, 20, 116 }
#define  TMPSNSCOMPSELEN                  (UvpReg_t){ 61, 247, 1,  1,  0, 1, 25, 116 }
#define  PHASEREFBUFFSIIBSTBUFF1          (UvpReg_t){ 61, 247, 2,  3,  0, 1, 26, 116 }
#define  PHASEREFBUFFSIIBSTBUFF2          (UvpReg_t){ 61, 247, 2,  3,  0, 1, 28, 116 }
#define  SSBBUFFSIIBSTBUFF1               (UvpReg_t){ 61, 247, 2,  3,  0, 1, 30, 116 }
#define  AMU123PDRBST                     (UvpReg_t){ 62, 249, 2,  1,  0, 1, 14, 120 }
#define  AMU123PDIBST                     (UvpReg_t){ 62, 250, 3,  3,  0, 1, 16, 120 }
#define  AMU4PDRBST                       (UvpReg_t){ 62, 250, 2,  1,  0, 1, 19, 120 }
#define  AMU4PDIBST                       (UvpReg_t){ 62, 250, 3,  3,  0, 1, 21, 120 }
#define  AMU3PAPOWERMODE                  (UvpReg_t){ 62, 251, 2,  2,  0, 1, 28, 120 }
#define  SSBMODCTRL                       (UvpReg_t){ 63, 252, 2,  3,  0, 1, 2,  124 }
#define  AMU123AAFGAIN                    (UvpReg_t){ 63, 252, 2,  1,  0, 1, 6,  124 }
#define  AMU4AAFGAIN                      (UvpReg_t){ 63, 253, 2,  1,  0, 1, 12, 124 }
#define  AMU4AAFBW                        (UvpReg_t){ 63, 253, 2,  2,  0, 1, 14, 124 }
#define  AMU123SADCPSEN                   (UvpReg_t){ 63, 254, 1,  1,  0, 1, 18, 124 }
#define  AMU123SADCCOMPENCNT              (UvpReg_t){ 63, 254, 5,  4,  0, 1, 19, 124 }
#define  AMU123SADCCOMPSELEN              (UvpReg_t){ 64, 256, 1,  1,  0, 2, 0,  0 }
#define  AMU4SADCPSEN                     (UvpReg_t){ 64, 256, 1,  1,  0, 2, 1,  0 }
#define  AMU4SADCCOMPENCNT                (UvpReg_t){ 64, 256, 5,  4,  0, 2, 2,  0 }
#define  AMU4SADCCOMPSELEN                (UvpReg_t){ 64, 256, 1,  1,  0, 2, 7,  0 }
#define  AMU123RFAMPLOADCTRL              (UvpReg_t){ 64, 257, 4,  3,  0, 2, 8,  0 }
#define  AMU123RFAMPRESCTRL               (UvpReg_t){ 64, 257, 1,  0,  0, 2, 12, 0 }
#define  AMU123RFAMPGMRESCTRL             (UvpReg_t){ 64, 257, 2,  1,  0, 2, 13, 0 }
#define  AMU4PAPOWERMODE                  (UvpReg_t){ 64, 259, 2,  2,  0, 2, 28, 0 }
#define  CPLKEN                           (UvpReg_t){ 64, 259, 1,  0,  0, 2, 31, 0 }
#define  PDIVCTRL                         (UvpReg_t){ 65, 260, 8,  15, 0, 2, 0,  4 }
#define  NDIVINT                          (UvpReg_t){ 65, 261, 8,  50, 0, 2, 8,  4 }
#define  NDIVFRAC                         (UvpReg_t){ 65, 262, 16, 0,  0, 2, 16, 4 }
#define  CPIVAL                           (UvpReg_t){ 66, 264, 3,  2,  0, 2, 0,  8 }
#define  CPLK                             (UvpReg_t){ 66, 264, 3,  0,  0, 2, 3,  8 }
#define  VCOCFS                           (UvpReg_t){ 66, 264, 6,  0,  0, 2, 6,  8 }
#define  VCOUCFS                          (UvpReg_t){ 66, 265, 2,  1,  0, 2, 12, 8 }
#define  VCOIBST                          (UvpReg_t){ 66, 265, 2,  2,  0, 2, 14, 8 }
#define  VCOBUFFIBST                      (UvpReg_t){ 66, 266, 2,  0,  0, 2, 16, 8 }
#define  LFR1CTRL                         (UvpReg_t){ 66, 266, 3,  7,  0, 2, 18, 8 }
#define  LFR3CTRL                         (UvpReg_t){ 66, 266, 3,  7,  0, 2, 21, 8 }
#define  LFC1CTRL                         (UvpReg_t){ 66, 267, 3,  4,  0, 2, 24, 8 }
#define  LFC2CTRL                         (UvpReg_t){ 66, 267, 3,  6,  0, 2, 27, 8 }
#define  LFC3CTRL                         (UvpReg_t){ 66, 267, 2,  2,  0, 2, 30, 8 }
#define  PLLLCKRNGCTRL                    (UvpReg_t){ 67, 268, 2,  1,  0, 2, 3,  12 }
#define  LDOPLLANATRM                     (UvpReg_t){ 68, 272, 3,  4,  0, 2, 0,  16 }
#define  LDOPLLDIGTRM                     (UvpReg_t){ 68, 272, 3,  3,  0, 2, 3,  16 }
#define  LDOPLLSDTRM                      (UvpReg_t){ 68, 272, 3,  3,  0, 2, 6,  16 }
#define  LDODIGTRM                        (UvpReg_t){ 68, 273, 3,  1,  0, 2, 9,  16 }
#define  LDOBIASTRM                       (UvpReg_t){ 68, 273, 3,  1,  0, 2, 12, 16 }
#define  LDODIGPADSTRM                    (UvpReg_t){ 68, 273, 3,  1,  0, 2, 15, 16 }
#define  LDOBIASEN_IDLE                   (UvpReg_t){ 68, 274, 1,  1,  0, 2, 18, 16 }
#define  RESERVED12                       (UvpReg_t){ 68, 274, 1   0,  1, 2, 19, 16 }
#define  LDOPLLANAEN_IDLE                 (UvpReg_t){ 68, 274, 1,  1,  0, 2, 20, 16 }
#define  LDOPLLDIGEN_IDLE                 (UvpReg_t){ 68, 274, 1,  1,  0, 2, 21, 16 }
#define  LDOPLLSDEN_IDLE                  (UvpReg_t){ 68, 274, 1,  1,  0, 2, 22, 16 }
#define  LDOADC1EN_IDLE                   (UvpReg_t){ 68, 274, 1,  0,  0, 2, 23, 16 }
#define  LDOADC2EN_IDLE                   (UvpReg_t){ 68, 275, 1,  0,  0, 2, 24, 16 }
#define  LDOADC3EN_IDLE                   (UvpReg_t){ 68, 275, 1,  0,  0, 2, 25, 16 }
#define  REFGENEN_IDLE                    (UvpReg_t){ 68, 275, 1,  1,  0, 2, 26, 16 }
#define  LDOADC4EN_IDLE                   (UvpReg_t){ 68, 275, 1,  0,  0, 2, 27, 16 }
#define  CPEN_IDLE                        (UvpReg_t){ 68, 275, 1,  1,  0, 2, 28, 16 }
#define  NDIVEN_IDLE                      (UvpReg_t){ 68, 275, 1,  1,  0, 2, 29, 16 }
#define  DIV4EN_IDLE                      (UvpReg_t){ 68, 275, 1,  1,  0, 2, 30, 16 }
#define  CML2CMOSEN_IDLE                  (UvpReg_t){ 68, 275, 1,  1,  0, 2, 31, 16 }
#define  LDOPLLANAEN_SENDPWR              (UvpReg_t){ 74, 296, 1,  1,  0, 2, 0,  40 }
#define  LDOPLLDIGEN_SENDPWR              (UvpReg_t){ 74, 296, 1,  1,  0, 2, 1,  40 }
#define  LDOPLLSDEN_SENDPWR               (UvpReg_t){ 74, 296, 1,  1,  0, 2, 2,  40 }
#define  LDOADC1EN_SENDPWR                (UvpReg_t){ 74, 296, 1,  0,  0, 2, 3,  40 }
#define  LDOADC2EN_SENDPWR                (UvpReg_t){ 74, 296, 1,  0,  0, 2, 4,  40 }
#define  LDOADC3EN_SENDPWR                (UvpReg_t){ 74, 296, 1,  0,  0, 2, 5,  40 }
#define  REFGENEN_SENDPWR                 (UvpReg_t){ 74, 296, 1,  1,  0, 2, 6,  40 }
#define  LDOADC4EN_SENDPWR                (UvpReg_t){ 74, 296, 1,  0,  0, 2, 7,  40 }
#define  CPEN_SENDPWR                     (UvpReg_t){ 74, 297, 1,  1,  0, 2, 8,  40 }
#define  NDIVEN_SENDPWR                   (UvpReg_t){ 74, 297, 1,  1,  0, 2, 9,  40 }
#define  DIV4EN_SENDPWR                   (UvpReg_t){ 74, 297, 1,  1,  0, 2, 10, 40 }
#define  CML2CMOSEN_SENDPWR               (UvpReg_t){ 74, 297, 1,  1,  0, 2, 11, 40 }
#define  VCOEN_SENDPWR                    (UvpReg_t){ 74, 297, 1,  1,  0, 2, 12, 40 }
#define  REFOSCEN_SENDPWR                 (UvpReg_t){ 74, 297, 1,  0,  0, 2, 13, 40 }
#define  SDEN_SENDPWR                     (UvpReg_t){ 74, 297, 1,  0,  0, 2, 14, 40 }
#define  PDIVEN_SENDPWR                   (UvpReg_t){ 74, 297, 1,  1,  0, 2, 15, 40 }
#define  AMU123RFAMPEN_SENDPWR            (UvpReg_t){ 74, 298, 1,  0,  0, 2, 16, 40 }
#define  AMU4RFAMPEN_SENDPWR              (UvpReg_t){ 74, 298, 1,  1,  0, 2, 17, 40 }
#define  AMU123RFAMPRFSWITCHCTRL_SENDPWR  (UvpReg_t){ 74, 298, 1,  1,  0, 2, 18, 40 }
#define  AMU4RFAMPRFSWITCHCTRL_SENDPWR    (UvpReg_t){ 74, 298, 1,  1,  0, 2, 19, 40 }
#define  AMU123PDEN_SENDPWR               (UvpReg_t){ 74, 298, 1,  1,  0, 2, 20, 40 }
#define  AMU4PDEN_SENDPWR                 (UvpReg_t){ 74, 298, 1,  1,  0, 2, 21, 40 }
#define  AMU123AAFEN_SENDPWR              (UvpReg_t){ 74, 298, 1,  1,  0, 2, 22, 40 }
#define  AMU4AAFEN_SENDPWR                (UvpReg_t){ 74, 298, 1,  1,  0, 2, 23, 40 }
#define  AMU123SADCEN_SENDPWR             (UvpReg_t){ 74, 299, 1,  1,  0, 2, 24, 40 }
#define  AMU123SADCCMBUFFEN_SENDPWR       (UvpReg_t){ 74, 299, 1,  1,  0, 2, 25, 40 }
#define  AMU4SADCEN_SENDPWR               (UvpReg_t){ 74, 299, 1,  1,  0, 2, 26, 40 }
#define  AMU4SADCCMBUFFEN_SENDPWR         (UvpReg_t){ 74, 299, 1,  1,  0, 2, 27, 40 }
#define  AMU123PIEN_SENDPWR               (UvpReg_t){ 74, 299, 1,  1,  0, 2, 28, 40 }
#define  AMU4PIEN_SENDPWR                 (UvpReg_t){ 74, 299, 1,  1,  0, 2, 29, 40 }
#define  AMU123GAINSTGEN_SENDPWR          (UvpReg_t){ 74, 299, 1,  1,  0, 2, 30, 40 }
#define  RESERVED29                       (UvpReg_t){ 74, 299, 1,  1,  0, 2, 31, 40 }
#define  AMU4GAINSTGEN_SENDPWR            (UvpReg_t){ 75, 300, 1,  0,  0, 2, 0,  44 }
#define  RESERVED30                       (UvpReg_t){ 75, 300, 1,  1,  0, 2, 1,  44 }
#define  SSBMIXEN_SENDPWR                 (UvpReg_t){ 75, 300, 1,  0,  0, 2, 2,  44 }
#define  RESERVED31                       (UvpReg_t){ 75, 300, 1,  0,  0, 2, 3,  44 }
#define  SSBMIXSTARTUP_SENDPWR            (UvpReg_t){ 75, 300, 1,  0,  0, 2, 4,  44 }
#define  PHASEREFBUFFSEN_SENDPWR          (UvpReg_t){ 75, 300, 1,  1,  0, 2, 5,  44 }
#define  PHASEREFBUFFSSTRT_SDPWR          (UvpReg_t){ 75, 300, 1,  0,  0, 2, 6,  44 }
#define  SSBBUFFSEN_SENDPWR               (UvpReg_t){ 75, 300, 1,  0,  0, 2, 7,  44 }
#define  SSBBUFFSSTARTUP_SENDPWR          (UvpReg_t){ 75, 301, 1,  0,  0, 2, 8,  44 }
#define  TEMPADC_OFFST                    (UvpReg_t){ 75, 301, 13, 0,  0, 2, 9,  44 }
#define  TEMPADC_GAIN                     (UvpReg_t){ 75, 302, 7,  1,  0, 2, 22, 44 }
#define  RESERVED32                       (UvpReg_t){ 75, 303, 1,  0,  0, 2, 29, 44 }
#define  RESERVED33                       (UvpReg_t){ 75, 275, 2,  0,  1, 2, 30, 44 }
#define  RSSI_LOWTHRESH                   (UvpReg_t){ 76, 304, 12, 0,  0, 2, 0,  48 }
#define  RSSI_HIGHTHRESH                  (UvpReg_t){ 76, 305, 12, 0,  0, 2, 12, 48 }
#define  REFOSCAMPMODE                    (UvpReg_t){ 78, 312, 1,  1,  0, 2, 22, 56 }
#define  REFOSCAMPGAIN                    (UvpReg_t){ 78, 312, 2,  1,  0, 2, 23, 56 }
#define  AMU1PAPOWERMODE                  (UvpReg_t){ 78, 312, 2,  2,  0, 2, 27, 56 }
#define  AMU2PAPOWERMODE                  (UvpReg_t){ 78, 312, 2,  2,  0, 2, 29, 56 }
#define  AMU1PAHIGHPOWER                  (UvpReg_t){ 78, 312, 1,  0,  0, 2, 8,  56 }
#define  AMU2PAHIGHPOWER                  (UvpReg_t){ 78, 312, 1,  0,  0, 2, 9,  56 }
#define  AMU3PAHIGHPOWER                  (UvpReg_t){ 78, 312, 1,  0,  0, 2, 10, 56 }
#define  AMU4PAHIGHPOWER                  (UvpReg_t){ 78, 312, 1,  0,  0, 2, 11, 56 }
#define  POWER_ON_PA                      (UvpReg_t){ 80, 320, 1,  0,  0, 2, 0,  64 }
#define  ADCSAT_CHECK_DISABLE             (UvpReg_t){ 80, 320, 1,  0,  0, 2, 2,  64 }
#define  QOB_CHECK_DISABLE                (UvpReg_t){ 80, 320, 1,  0,  0, 2, 3,  64 }
#define  AMU123RFAMPRFSWITCHCTRL_TEST     (UvpReg_t){ 80, 322, 1,  0,  0, 2, 23, 64 }
#define  AMU4RFAMPRFSWITCHCTRL_TEST       (UvpReg_t){ 80, 323, 1,  0,  0, 2, 25, 64 }
#define  AMU123PAEN_TEST                  (UvpReg_t){ 80, 323, 1,  0,  0, 2, 26, 64 }
#define  AMU4PAEN_TEST                    (UvpReg_t){ 80, 323, 1,  0,  0, 2, 28, 64 }
#define  AMU123PIEN_TEST                  (UvpReg_t){ 80, 323, 1,  0,  0, 2, 30, 64 }
#define  AMU4PIEN_TEST                    (UvpReg_t){ 80, 323, 1,  0,  0, 2, 31, 64 }
#define  AMU123SADCCMBUFFEN_TEST          (UvpReg_t){ 81, 324, 1,  0,  0, 2, 2,  68 }
#define  AMU123SADCEN_TEST                (UvpReg_t){ 81, 324, 1,  0,  0, 2, 3,  68 }
#define  AMU4SADCCMBUFFEN_TEST            (UvpReg_t){ 81, 324, 1,  0,  0, 2, 4,  68 }
#define  AMU4SADCEN_TEST                  (UvpReg_t){ 81, 324, 1,  0,  0, 2, 5,  68 }
#define  PLLLFMODE                        (UvpReg_t){ 81, 327, 1,  0,  0, 2, 27, 68 }
#define  PLLLFDACOUT                      (UvpReg_t){ 81, 327, 4,  7,  0, 2, 28, 68 }
#define  PLLLCKDETEN                      (UvpReg_t){ 82, 328, 1,  1,  0, 2, 0,  72 }
#define  PLLDACEN                         (UvpReg_t){ 82, 328, 1,  0,  0, 2, 1,  72 }

#define MAX_NUM_AMU             4                                           ///< The number of AMUs available on a UVP.
#define ALL_AMU_MASK            ((1UL << (MAX_NUM_AMU)) - 1)                ///< The mask for selecting all AMU's
#define NO_AMU_MASK             0x0                                         ///< Mask to disable all AMU's
#define UVP_INDEX(amb, uvp)     ((((uint32_t)amb) << 4) | ((uint32_t)uvp))  ///< Maps a amb, uvp number combo to a single index
#define UVP_TEMP_SIGN_BIT       0x1000                                      ///< The sign bit for temperature values
#define UVP_TEMP_SIGN_EXT       0xE000                                      ///< The sign extension for temperature values
#define UVP_TEMP_SLOPE_NUM      155                                         ///< The numerator of the slope needed to convert UVP temperatures to centidegrees C
#define UVP_TEMP_SLOPE_DEN      100                                         ///< The denomentator of the slope needed to convert UVP temperatures to centidegrees C
#define UVP_TEMP_OFFSET         4355                                        ///< The offset needed to convert UVP temperatures to centidegrees C
#define TO_CENT(val)            (100*(val))                                 ///< Converts a unit to centi units
#define NUMBER_OF_UVP_PAGES     3                                           ///< The number of UVP memory pages
#define UVP_PAGE_SIZE           128                                         ///< Number of bytes in a UVP page
/**
 * @brief Contains all the information necessary to update a uvp register.
 */
typedef struct _UvpReg_t
{
    uint16_t rowAddr;       ///< The row address is the result of floor(byteAddr/32)
    uint16_t byteAddr;      ///< The location of the register
    int32_t  size;          ///< Size of the value in bits
    int32_t  defaultValue;  ///< The default value of the register
    int32_t  isReadonly;    ///< Indicates if the register is read only
    int32_t  pageIndex;     ///< The page is the rowAddr divided by 32
    int32_t  offset;        ///< Offset from bit0 in bits
    uint16_t colAddr;       ///< The column address is the remainder of rowAddr divided by 32, then multipied by 4 (because each address points to 32 bits).
} UvpReg_t;


#ifdef UVP_TEST_ENABLED
void TestUvp(void);
#endif

void InitUvpDriver(void);
CotaError_t StartUvps(AmbMask_t ambMask);
CotaError_t UvpDisableAmus(AmbMask_t ambMask);
CotaError_t UvpEnableAmus(AmbMask_t ambMask);
CotaError_t GetAmuEnableMask(AmuMask_t* pData, uint16_t numValues, AmbMask_t ambMask);
CotaError_t SetAmuEnableChanges(AmuMask_t* pDataOld, AmuMask_t* pDataNew, uint16_t numValues, AmbMask_t ambMask);
CotaError_t UvpRegisterRead(UvpReg_t reg, uint32_t* pData, AmbNum_t ambNum, UvpNum_t uvpNum);
CotaError_t UvpRegisterWrite(UvpReg_t reg, uint32_t data, AmbNum_t ambNum, UvpNum_t uvpNum);
CotaError_t ReadArrayUvps(UvpReg_t pReg, uint32_t* pData, uint16_t numValues, AmbMask_t ambMask, UvpMask_t uvpMask);CotaError_t WriteManyUvps(UvpReg_t pReg, uint32_t data, AmbMask_t ambMask, UvpMask_t uvpMask);
CotaError_t UvpInit(AmbMask_t ambMask, UvpMask_t uvpMask);
CotaError_t UvpAmuEnable(AmbNum_t ambNum, UvpNum_t uvpNum, AmuMask_t amuMask);
CotaError_t UvpManyAmuEnable(AmbMask_t ambMask, UvpMask_t uvpMask, AmuMask_t amuMask);
CotaError_t WriteArrayUvps(UvpReg_t pReg, uint32_t* pData, uint32_t numValues, AmbMask_t ambMask, UvpMask_t uvpMask);
CotaError_t UvpGetTemperatureArray(int16_t* pTemp, uint16_t numValues, AmbMask_t ambMask, UvpMask_t uvpMask);
CotaError_t UpdateUvpTemperatureCache(int16_t* maxDelta, bool updateAlways);
CotaError_t GetAverageUvpTempsFromCache(int16_t* uvpAvgTemps, uint8_t numAvgTemps);
CotaError_t ReadArrayUvps32(uint32_t* pData, uint16_t numValues, uint8_t pageIndex, uint16_t colAddr, AmbMask_t ambMask, UvpMask_t uvpMask);
CotaError_t ReadArrayUvps64(uint64_t* pData, uint16_t numValues, uint8_t pageIndex, uint16_t colAddr, AmbMask_t ambMask, UvpMask_t uvpMask);
CotaError_t LockDetect(AmbNum_t ambNum, UvpNum_t uvpNum, uint8_t * cfsVal);
CotaError_t GetRssiValues(uint16_t rssiData[MAX_NUM_AMB][UVPS_PER_AMB][MAX_NUM_AMU], AmbMask_t ambMask, UvpMask_t uvpMask);
void UvpDump(AmbNum_t amb, UvpNum_t uvp, CtrlResp_t* msg);
#endif  //#ifndef _UVP_DRIVER_H_

