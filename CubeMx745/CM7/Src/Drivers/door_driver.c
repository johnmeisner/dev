/****************************************************************************//**
* @file      door_driver.c
*
* @brief     Monitors the state of the door
*
*
*           This driver monitors the state of the door and sends a message to the ctrl task when
*           the door is open or closed under the following conditions:
* 
*           1) The door open message is sent quickly (meaning a few ms)
*              when any door interrupt is detected while the door is closed.
*           2) The door close message is sent when the door is detected close, a second
*              goes by without any interrupts (debounce), and the door is detected close again.
*
*           ********  door closed    ***********    1s int free + door closed  *********************
*           * Init *  ------------>  * Debouce *  ---------------------------->* CLOSED (Send msg) *
*           ********                 ***********  <-----------                 *********************
*              |                           |             Closed door                    |
*              |                        Interrupt            |                       Any interrupt
*              |                       or door open          |<------------------|
*          door open                       |                 |                   |      |
*              |                          \|/                |                   |     \|/
*              |                      ***********-------------                  **************************
*              |--------------------->*   Open  *<-----Open Door----------------* Open Detect (Send msg) *
*                                     ***********                               **************************


* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2020 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "orion_config.h"

#ifdef USE_FOREVER_TRACKER_BUILD
#include "FreeRTOS.h"
#include "semphr.h"
#include "string.h"
#include "main.h"
#include "timers.h"
#include "door_driver.h"
#include "tps_driver.h"
#include "cli_task.h"
#include "fan_driver.h"

#define DOOR_DEBOUCE_TIME           1000        ///< A debounce time for detecting the door close in milliseconds 
#define DOOR_INTERRUPT_DELAY        1           ///< The minimum time between an open door interrupt and a report to the control task in milliseconds.                

#define IS_DOOR_CLOSED()              (HAL_GPIO_ReadPin(COTA_BOX_GPIO_Port, COTA_BOX_Pin) == GPIO_PIN_SET) ///< Macro that returns true if door is closed.
/**
 *  @brief Door driver states
 */
typedef enum _doorDriverState_t
{
    DOOR_STATE_OPEN,                    ///< Indicates the door is open and we have posted the open message to the control task.
    DOOR_STATE_OPEN_DETECTED,           ///< Indicates a door open interrupt has been detected, but we have not posted the open message to the control task.
    DOOR_STATE_CLOSED,                  ///< Indicates the door is closed
    DOOR_STATE_DEBOUNCE,                ///< Indicates we are in a debouncing period trying to detect if the door is closed

} doorDriverState_t;

static TimerHandle_t                gDoorDebounceTimerHandle    = NULL;             ///< FreeRTOS timer handle for the door driver
static StaticTimer_t                gDoorDebounceTimerBuffer;                       ///< FreeRTOS timer buffer for the door driver
static volatile doorDriverState_t   gDoorDriverState            = DOOR_STATE_OPEN;  ///< State of the driver

/**
 * @brief This is the callback to the timer, it also a statemachine to periodically check to see if the door closed when the door is open.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void doorDebounceTimerCallback(TimerHandle_t  timer)
{
    switch (gDoorDriverState)
    {
    case DOOR_STATE_OPEN:
        if (IS_DOOR_CLOSED())  //This would require a fast open and close, but it is possible, so we have to handle it.
        {   
            gDoorDriverState = DOOR_STATE_DEBOUNCE;
            xTimerChangePeriod( gDoorDebounceTimerHandle, pdMS_TO_TICKS(DOOR_DEBOUCE_TIME), NO_WAIT);
            xTimerStart( gDoorDebounceTimerHandle, NO_WAIT);
        }
        break;
        
    case DOOR_STATE_OPEN_DETECTED:
        CtrlSendMsg(CTRL_SYS_DOOR_OPEN, NULL, 0);
        gDoorDriverState = DOOR_STATE_OPEN;
        if (IS_DOOR_CLOSED())  //This would require a fast open and close, but it is possible, so we have to handle it.
        {   
            gDoorDriverState = DOOR_STATE_DEBOUNCE;
            xTimerChangePeriod( gDoorDebounceTimerHandle, pdMS_TO_TICKS(DOOR_DEBOUCE_TIME), NO_WAIT);
            xTimerStart( gDoorDebounceTimerHandle, NO_WAIT);
        }
        break;
        
    case DOOR_STATE_CLOSED:
        break;  
        
    case DOOR_STATE_DEBOUNCE:
        if (IS_DOOR_CLOSED())
        {   
            CtrlSendMsg(CTRL_SYS_DOOR_CLOSED, NULL, 0);
            gDoorDriverState = DOOR_STATE_CLOSED;
        }
        else
        {
            gDoorDriverState = DOOR_STATE_OPEN;
        }
        break;
    default:
        break;
    }
}

/**
 *  @brief  Initializes the door timer
 *  @return COTA_ERROR_NONE if successfull; otherwise 
 */
CotaError_t InitDoorDriver(void)
{
    CotaError_t err = COTA_ERROR_NONE;
    if (gDoorDebounceTimerHandle)
    {
        xTimerStop(gDoorDebounceTimerHandle, NO_WAIT);
    }      
    else
    {
        gDoorDebounceTimerHandle = xTimerCreateStatic(
            "Door debounce Timer",
            pdMS_TO_TICKS(DOOR_INTERRUPT_DELAY),
            pdFALSE,
            (void *) 0,
            doorDebounceTimerCallback,
            &gDoorDebounceTimerBuffer);
        if (!gDoorDebounceTimerHandle)
        {
            err = COTA_ERROR_DOOR_INIT_FAILED;
            //If we have an error in door driver intitialize, let's assume the door is open.
            CtrlSendMsg(CTRL_SYS_DOOR_OPEN, NULL, 0);
        }
        SetFansFull(true);  //Fans always on full for FOREVER_TRACKER
        if (IS_DOOR_CLOSED())
        {
            gDoorDriverState = DOOR_STATE_DEBOUNCE;
            xTimerChangePeriod( gDoorDebounceTimerHandle, pdMS_TO_TICKS(DOOR_DEBOUCE_TIME), NO_WAIT);
            xTimerStart( gDoorDebounceTimerHandle, NO_WAIT);
        }
    }
    
    return err;
}
/**
 * @brief  Interupt called when door is opened.  It will immediately stop a TPS cycle
 *
 * @param  higherPriorityTaskWoken  Determines if context switch is needed after the interrupt
 */
void DoorOpenIrq(BaseType_t * const higherPriorityTaskWoken)
{
    if (gDoorDriverState == DOOR_STATE_CLOSED)
    {
        //Any activity on the door interupt in this state should trigger a door open event
        gDoorDriverState = DOOR_STATE_OPEN_DETECTED;
        StopTpsCycle();
        xTimerChangePeriodFromISR( gDoorDebounceTimerHandle, pdMS_TO_TICKS(DOOR_INTERRUPT_DELAY), higherPriorityTaskWoken );
        xTimerStartFromISR( gDoorDebounceTimerHandle, higherPriorityTaskWoken);
    }
    else if (gDoorDriverState == DOOR_STATE_OPEN_DETECTED)
    {
        //A rare possibitlity but someone could open and close the door real fast
        //We can't change the state here because we must wait for the timer to send CTRL_SYS_DOOR_OPEN to the ctrl_task.
        //The timer callback will check the state and start deboucing if it's closed.
    }
    else if (gDoorDriverState == DOOR_STATE_OPEN)
    {
        //If we get an interrupt here, the door may have closed. If closed, we start debouncing.
        if (IS_DOOR_CLOSED())
        {
            gDoorDriverState = DOOR_STATE_DEBOUNCE;
            xTimerChangePeriodFromISR( gDoorDebounceTimerHandle, pdMS_TO_TICKS(DOOR_DEBOUCE_TIME), higherPriorityTaskWoken );
            xTimerStartFromISR( gDoorDebounceTimerHandle, higherPriorityTaskWoken);
        }
    }
    else if (gDoorDriverState == DOOR_STATE_DEBOUNCE)
    {
        //Any interrupt during the debounce invalidates the debounce and we assume we are open.  We do not
        //stop the timer because we need to check timer callback to check the door state.
        gDoorDriverState = DOOR_STATE_OPEN;     
    }
}

/**
 * @brief Checks the door gpio state

 * @return true if the door is closed, false otherwise
 */
bool IsDoorClosed(void)
{
    return (gDoorDriverState == DOOR_STATE_CLOSED);
}

#endif
