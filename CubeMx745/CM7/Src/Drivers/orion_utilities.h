/****************************************************************************//**
* @file      orion_utilities.h
*
* @brief     Header file for orion utilities
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2020 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _ORION_UTILITIES_H_
#define _ORION_UTILITIES_H_
#include "FreeRTOS.h"
#include "orion_config.h"

uint32_t TimeDiffTicks(uint32_t newTimeTicks, uint32_t oldTimeTicks);

#endif  //#ifndef _ORION_UTILITIES_H_

