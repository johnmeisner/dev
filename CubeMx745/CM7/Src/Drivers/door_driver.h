/****************************************************************************//**
* @file      door_driver.h
*
* @brief     Header file to monitor the door state
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2020 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "orion_config.h"

#if defined(USE_FOREVER_TRACKER_BUILD)

#ifndef _DOOR_DRIVER_H_
#define _DOOR_DRIVER_H_
#include "FreeRTOS.h"
#include "semphr.h"
#include "stm32h7xx_hal.h"
#include "stdbool.h"
#include "error.h"
#include "ctrl_interface.h"

void DoorOpenIrq(BaseType_t * const higherPriorityTaskWoken);
CotaError_t InitDoorDriver(void);
bool IsDoorClosed(void);

#endif // #ifndef _DOOR_DRIVER_H_
#endif // #ifndef USE_FOREVER_TRACKER_BUILD
