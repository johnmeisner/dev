/****************************************************************************//**
* @file      ccb_temperature.h
*
* @brief     Header file to enable reading of ccb temperatures
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _CCB_TEMPERATURE_H_
#define _CCB_TEMPERATURE_H_
#include "error.h"

#define TEMP_CCB_NUM_OF_THERM         3                         ///< The number of CCB temperature sensors
#define CELSIUS_TO_CENTICELSIUS(temp) ((temp) * 100)            ///< Converts celsius to centicelsius
#define CENTICELSIUS_TO_CELSIUS(temp) (((float)temp)/100.0f)    ///< Converts centicelsius to celsius

/**
 * @brief Used to set the temperature conversion rate
 */ 
typedef enum _TempConvRate_t
{
    TEMP_CONV_RATE_0_25SEC  = 0x0,    ///< Read the temperature every 0.25 seconds
    TEMP_CONV_RATE_1SEC     = 0x1,    ///< Read the temperature every 1 second
    TEMP_CONV_RATE_4SEC     = 0x2,    ///< Read the temperature every 4 seconds
    TEMP_CONV_RATE_8SEC     = 0x3     ///< Read the temperature every 8 seconds
} TempConvRate_t;

typedef uint8_t TempNum_t;

CotaError_t InitializeCCBTemperatures(void);
CotaError_t StartCCBTemperatureAcquisition(TempConvRate_t convRate);
CotaError_t ReadCCBTemperature(TempNum_t tempNum, int16_t* temperature);
#endif // #ifndef _CCB_TEMPERATURE_H_

