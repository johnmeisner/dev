/****************************************************************************//**
* @file      spi_driver.c
*
* @brief     Driver to read and write data over SPI
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "FreeRTOS.h"
#include "string.h"
#include "main.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "spi_driver.h"
#include "semphr.h"
#include "task.h"
#include "debug_off.h"
#include "proxy.h"
#include "raspberry_pi.h"
#include "cli_task.h"
#include "orion_config.h"

#if defined( __ICCARM__ )
  #define BDMA_BUFFER \
      _Pragma("location=\".bdma_buffer\"")
#else
  #error Need to define BDMA_BUFFER for this compiler
#endif        
        
        
/**
 * @note  We need to explicity disable pages in the D-cache while using DMA so that will load from memory on the next read.
 *        Since D-cache pages can only be disabled on a 32 byte basis, we need to make sure the globals below are aligned to 
 *        32 bytes and consume entire pages.
 */
#define SPI_SIMUL_READ_SIZE 9                                   ///< If reading from multiple AMB's simulataneously, this is the maximum bytes that be read from any single UVP
#define SPI_DMA_BUF_SIZE    (((SPI_MAX_NUM_BYTES-1)/32+1)*32)   ///< This is designed to make sure arrays lengths are aligned to 32 bytes.  For example, a size of 31 bytes evaluates to 32 in this expression.  A size of 1 also evaluates to 32.
#define SPI_WAIT_TIMEOUT    10                                  ///< Number of milliseconds to wait for SPI transaction before timing out.      
/**
 * Globals used for DMA are best alligned to 32 bytes and kept a length of 32 bytes.
 *  The reason is that we must clear the cache for writes and invalidate pages in the D-cache for reads.
 *  Pages in the D-cache are 32 bytes. Problems can develop if the other variables share the same page with
 *  these globals.  A read of another variable for example could validate a page that needs to stay
 *  invalid for example.
 */
#pragma data_alignment=32 
DMA_BUFFER static uint8_t gMasterRxBuf[SPI_DMA_BUF_SIZE];       ///< A DMA enabled buffer receiving data over the master SPI port.

#pragma data_alignment=32
DMA_BUFFER static uint8_t gMasterTxBuf[SPI_DMA_BUF_SIZE];       ///< A DMA enabled buffer containing data to send over the master SPI port.

#pragma data_alignment=32
DMA_BUFFER static uint8_t gSlaveRxBuf[SPI_DMA_BUF_SIZE];        ///< A DMA enabled buffer receiving data over one of the slave SPI ports

#pragma data_alignment=32
DMA_BUFFER static uint8_t gSlaveTxBuf[SPI_DMA_BUF_SIZE];        ///< A DMA enabled buffer containing data to send over one of the slave SPI ports.

#pragma data_alignment=32
BDMA_BUFFER static uint8_t gSlaveRxBufBDma[SPI_DMA_BUF_SIZE];   ///< A Basic DMA enabled buffer receiving data over one of the slave SPI ports

#pragma data_alignment=32
BDMA_BUFFER static uint8_t gSlaveTxBufBDma[SPI_DMA_BUF_SIZE];   ///< A Basic DMA enabled buffer containing data to send over one of the slave SPI ports.

static SemaphoreHandle_t  gSpiSemaphoreHandle;                  ///< Counting semaphore used to signal complete spi transactions
static StaticSemaphore_t  gSpiSemaphoreBuffer;                  ///< Buffer containing information about gSpiSemaphoreHandle

extern SPI_HandleTypeDef PROXY_SPI_HANDLE;                      ///< Proxy SPI port handle
extern SPI_HandleTypeDef RPI_SPI_HANDLE;                        ///< Raspberry Pi SPI port handle

/**
 * @brief A callback indicating a SPI transaction has completed. It gives the SPI semaphore, incrementing the count by one.
 * 
 * @param hspi Handle to the SPI port
 */
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
    if ((hspi != &PROXY_SPI_HANDLE ) && (hspi != &RPI_SPI_HANDLE))
    {
        BaseType_t xHigherPriorityTaskWoken = pdFALSE;
        
        if (hspi == AmbGetMasterSpiHandle())
        {
            SCB_InvalidateDCache_by_Addr((uint32_t*)gMasterRxBuf, SPI_DMA_BUF_SIZE);
        }
        else if (hspi == &hspi6)
        {
            SCB_InvalidateDCache_by_Addr((uint32_t*)gSlaveRxBufBDma, SPI_DMA_BUF_SIZE);
        }
        else
        {
            SCB_InvalidateDCache_by_Addr((uint32_t*)gSlaveRxBuf, SPI_DMA_BUF_SIZE);             
        }
             
        xSemaphoreGiveFromISR(gSpiSemaphoreHandle, &xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}

/**
 * @brief Initializes the SPI driver
 */
void InitSpiDriver(void)
{
    gSpiSemaphoreHandle = xSemaphoreCreateCountingStatic( MAX_NUM_AMB, 0, &gSpiSemaphoreBuffer);
    while (gSpiSemaphoreHandle == NULL);
}

/**
 * @brief Checks #gSpiSemaphoreHandle is at a count of zero,
 *        if it's not, we issue a warning and take the semaphore until it is
 *        at zero.
 */
static void CheckSpiSemaphore(void)
{
    UBaseType_t uGetCount = 0;
    uint8_t ii;
   
    uGetCount = uxSemaphoreGetCount( gSpiSemaphoreHandle);

    if (uGetCount != 0)
    {   
        //Hopefully this will never happen, but if it does we need to fix it.
        PostPrintf("Warning SpiSimulTransfer end count is %d\r\n", uGetCount);
        for (ii = 0; ii < uGetCount; ii++)
        {
           xSemaphoreTake(gSpiSemaphoreHandle, pdMS_TO_TICKS(SPI_WAIT_TIMEOUT));
        }
    }  
}


/**
 * @brief This function will send data to a specified SPI and then retrieve the data
 *        it sends back to you.
 *
 * @param hSpi          A pointer to the handle of the SPI port to transfer data on
 * @param pTxData       Pointer to buffer holding data to send
 * @param pRxData       Pointer to buffer receiving data
 * @param size          The number of bytes of data to exchange
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 *
 * @note   This function is designed to work with AMB SPI ports, where the master AMB SPI
 *         port clock line is permanently connected to the slave AMB SPI port clock lines.
 */
CotaError_t SpiTransfer(SPI_HandleTypeDef *hSpi, uint8_t *pTxData, uint8_t *pRxData, uint16_t size)
{
    HAL_StatusTypeDef retVal = HAL_ERROR;
    
    if (size < SPI_MAX_NUM_BYTES)
    {
        AmbEnableWrmhlCsb(true);
        if ((hSpi == AmbGetMasterSpiHandle()))  //This code assume spi6 is not the master
        {      
            memcpy(gMasterTxBuf,pTxData,size);
            SCB_CleanDCache_by_Addr((uint32_t*)gMasterTxBuf, SPI_DMA_BUF_SIZE);

            retVal = HAL_SPI_TransmitReceive_DMA(hSpi, gMasterTxBuf, gMasterRxBuf, size);
            if (retVal == HAL_OK)
            {
                if (xSemaphoreTake(gSpiSemaphoreHandle, pdMS_TO_TICKS(SPI_WAIT_TIMEOUT)) == pdPASS)
                {
                    retVal = (hSpi->State == HAL_SPI_STATE_READY) ? HAL_OK : HAL_ERROR;
                }
                else
                {
                    retVal = HAL_ERROR;
                }
            } 

            memcpy(pRxData, gMasterRxBuf, size);
        }
        else
        {
            // Handle the slave SPI transfers
            if (hSpi == &hspi6)  //spi6 uses a different type of DMA called "Basic DMA".  Its memory must be in a special location.
            {
                memcpy(gSlaveTxBufBDma, pTxData, size);
                SCB_CleanDCache_by_Addr((uint32_t*)gSlaveTxBufBDma, SPI_DMA_BUF_SIZE);
                
                // Prepare the slave SPI port for a transfer when the clock is started by the master
                retVal = HAL_SPI_TransmitReceive_DMA(hSpi, gSlaveTxBufBDma, gSlaveRxBufBDma, size);
                
                // Start clocking the data into the slave SPI port using the master SPI port clock signal
                retVal = (retVal == HAL_OK) ? HAL_SPI_TransmitReceive_DMA(AmbGetMasterSpiHandle(), gMasterTxBuf, gMasterRxBuf, size) : retVal;
            }
            else
            {
                memcpy(gSlaveTxBuf, pTxData, size);
                memset(gSlaveRxBuf, 0x11, SPI_DMA_BUF_SIZE);
                SCB_CleanDCache_by_Addr((uint32_t*)gSlaveTxBuf, SPI_DMA_BUF_SIZE);     
                
                // Prepare the slave SPI port for a transfer when the clock is started by the master
                retVal = HAL_SPI_TransmitReceive_DMA(hSpi, gSlaveTxBuf, gSlaveRxBuf, size);
                
                // Start clocking the data into the slave SPI port using the master SPI port clock signal
                retVal = (retVal == HAL_OK) ? HAL_SPI_TransmitReceive_DMA(AmbGetMasterSpiHandle(), gMasterTxBuf, gMasterRxBuf, size) : retVal;
               
            }

            if (retVal == HAL_OK)
            {
                retVal = (xSemaphoreTake(gSpiSemaphoreHandle, pdMS_TO_TICKS(SPI_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;

                if (retVal == HAL_OK)
                {
                    retVal = (xSemaphoreTake(gSpiSemaphoreHandle, pdMS_TO_TICKS(SPI_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
                }

                if (retVal == HAL_OK)
                {
                    retVal = ((hSpi->State == HAL_SPI_STATE_READY) && (AmbGetMasterSpiHandle()->State == HAL_SPI_STATE_READY)) ? HAL_OK : HAL_ERROR;       
                }
            }

            if (hSpi == &hspi6)
            {
                memcpy(pRxData, gSlaveRxBufBDma, size);
            }
            else
            {
                memcpy(pRxData, gSlaveRxBuf, size);
            }
        }

        AmbEnableWrmhlCsb(false);
    }
      
    CheckSpiSemaphore();
    
    return (retVal == HAL_OK) ? COTA_ERROR_NONE : POST_COTA_ERROR(COTA_ERROR_SPI_TRANSFER_FAILED);
}

/**
 * @brief This function will read data from multiple AMB's simulataneously  (max 64 bit data chunks)
 * 
 * @note  Before calling this function, the address pins need to be set to one UVP per AMB, otherwise the read makes no sense.
 *
 * @param pTxData       The data to be written to all four AMB's   
 * @param pRxData       Pointer to the receive buffer.  The buffer must be 4 times the size of the data because each AMB's data is written sequentially.
 *                      Meaning, the data from AMB0 starts at byte 0, data from AMB1 starts at byte size, data from AMB2 starts at byte 2*size, etc.
 *                      pRxData can be NULL, which will cause received data to not be processed.
 * @param size          The number of bytes of data to read from each AMB (max 9)
 *
 * @param ambMask       The mask that will select which AMB's to transfer data to and from.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #CotaError_t
 */
CotaError_t SpiSimulTransfer(uint8_t* pTxData, uint8_t *pRxData, uint16_t size, AmbMask_t ambMask)
{
    HAL_StatusTypeDef retVal = HAL_OK;
    uint8_t ii;
    uint8_t spiMasterIndex = 0;
    uint8_t waitCount = 0;

    //Size cannot be greater than 9
    if (size <= SPI_SIMUL_READ_SIZE)
    {
        AmbEnableWrmhlCsb(true);
        //First we schedule all slave transfers
        for (ii = 0; (ii < MAX_NUM_AMB) && (retVal == HAL_OK); ii++)
        {
            if (AmbGetSpiHandle(ii) == AmbGetMasterSpiHandle())
            {
                spiMasterIndex = ii;  //Let's skip the master, but note its index so we can schedule it last and trigger the transfer
            }          
          
            if (BIT_IN_MASK(ii, ambMask))
            {
                memcpy(&gSlaveTxBuf[ii*size], pTxData, size); 
                
                if (AmbGetSpiHandle(ii) == &hspi6)
                {
                    memcpy(gSlaveTxBufBDma,pTxData,size);
                    SCB_CleanDCache_by_Addr((uint32_t*)gSlaveTxBufBDma, SPI_DMA_BUF_SIZE);
                    retVal = HAL_SPI_TransmitReceive_DMA(AmbGetSpiHandle(ii), gSlaveTxBufBDma, gSlaveRxBufBDma, size);
                    waitCount++;
                }
                else if (AmbGetSpiHandle(ii) != AmbGetMasterSpiHandle())
                {
                    SCB_CleanDCache_by_Addr((uint32_t*)gSlaveTxBuf, SPI_DMA_BUF_SIZE);
                    retVal = HAL_SPI_TransmitReceive_DMA(AmbGetSpiHandle(ii), &gSlaveTxBuf[ii*size], &gSlaveRxBuf[ii*size], size);
                    waitCount++;
                }
            }
        }
        //Let's make sure D-cache does not interfere  
        SCB_CleanDCache_by_Addr((uint32_t*)gSlaveTxBuf, SPI_DMA_BUF_SIZE);

        //Schedule the master tranfer
        retVal = (retVal == HAL_OK) ? HAL_SPI_TransmitReceive_DMA(AmbGetMasterSpiHandle(), &gSlaveTxBuf[spiMasterIndex*size], &gSlaveRxBuf[spiMasterIndex*size], size) : retVal; 
        waitCount++;
        
        if (retVal == HAL_OK)
        {
            //Let's wait on confirmation on that all the transfers completed.
            for (ii = 0; (ii < waitCount) && (retVal == HAL_OK); ii++)
            {
                retVal = (xSemaphoreTake(gSpiSemaphoreHandle, pdMS_TO_TICKS(SPI_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
 
            }
   
            //Let's check the state of the SPI ports to see if the transfers are completed.
            for (ii = 0; (ii < MAX_NUM_AMB) && (retVal == HAL_OK); ii++)
            {
                if (BIT_IN_MASK(ii, ambMask))
                {
                    if (AmbGetSpiHandle(ii)->State != HAL_SPI_STATE_READY)
                    {
                        retVal = HAL_ERROR;
                    }
                }
            }
        }  
        
        //If requested, let's copy the received data to the passed in buffer
        if ((pRxData != NULL) && (retVal == HAL_OK))
        {
            for (ii = 0; (ii < MAX_NUM_AMB) && (retVal == HAL_OK); ii++)
            {
                if (BIT_IN_MASK(ii, ambMask))
                {
                    if (AmbGetSpiHandle(ii) == AmbGetMasterSpiHandle())
                    {
                        memcpy(&pRxData[ii*size], &gSlaveRxBuf[spiMasterIndex*size], size);
                    }
                    else if (AmbGetSpiHandle(ii) == &hspi6)
                    {
                        memcpy(&pRxData[ii*size], gSlaveRxBufBDma, size);
                    }
                    else
                    {
                        memcpy(&pRxData[ii*size],  &gSlaveRxBuf[ii*size], size);
                    }
                }
            }            
        }
        
        AmbEnableWrmhlCsb(false);
    }    
    
    CheckSpiSemaphore();
    
    return (retVal == HAL_OK) ? COTA_ERROR_NONE : POST_COTA_ERROR(COTA_ERROR_SPI_SIMUL_TRANS_FAILED);
}


/**
 * @brief  This function is called by the SPI driver after the SPI Rx transfer is completed
 */
void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi)
{
    BaseType_t higherPriorityTaskWoken = pdFALSE;
    
    if (hspi == &PROXY_SPI_HANDLE)
    {
        ProxySpiRxCpltIrq(&higherPriorityTaskWoken);
    }
    else
    {
        // Do nothing
    }
    
    portYIELD_FROM_ISR(higherPriorityTaskWoken);
}


/**
 * @brief  This function is called by the SPI driver after the SPI Tx transfer is completed
 *
 */
void HAL_SPI_TxCpltCallback(SPI_HandleTypeDef *hspi)
{
    BaseType_t higherPriorityTaskWoken = pdFALSE;
    
    if (hspi == &PROXY_SPI_HANDLE)
    {
        ProxySpiTxCpltIrq(&higherPriorityTaskWoken);
    }
    else
    {
        // Do nothing
    }

    portYIELD_FROM_ISR(higherPriorityTaskWoken);
}
