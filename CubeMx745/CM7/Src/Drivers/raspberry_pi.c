/**
 * @file       raspberry_pi.c
 *
 * @brief      Raspberry Pi communication driver implementation
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 *
 * Raspberry Pi Communication Protocol
 * ===================================
 * 
 * Communication Bus Signal Description
 * ------------------------------------
 * MR - Master ready
 * SR - Slave ready
 * MISO - SPI master in, slave out (master is RPi, slave is the ST MCU)
 * MOSI - SPI master out, slave in
 * CLK  - SPI clock line, driver by the RPi
 *
 * The link layer protocol is based on the TI NPI protocol where
 * bidirectional communication is not supported:
 * https://e2e.ti.com/cfs-file/__key/communityserver-discussions-components-files/156/NPI-User_2700_s-Guide.pdf
 * http://software-dl.ti.com/simplelink/esd/simplelink_cc13x2_26x2_sdk/3.40.00.02/exports/docs/ble5stack/ble_user_guide/html/ble-stack-common/npi-index.html
 *
 * Incoming Messages
 * -----------------
 * 
 * Since this driver is controlling the SPI slave port, we don't know
 * exactly how many bytes will be received, so we need to be prepared
 * to accept the maximum allowed number of bytes, which is
 * MAX_FRAME_SIZE.
 *
 * This driver supports bi-directional transfer, but it is an un-used
 * mechanic at the higher levels of abstraction. It is intended to
 * handle the potential race conditions associated with
 * uni-directional protocols. In this way, no information can be
 * loss.
 *
 * @verbatim
 *
 * Incoming message format:
 * -------------------------------------------------------------
 * |  Offset  | Payload | Payload |       | Payload |  CRC-32  |
 * |  to CRC  |  Byte   |  Byte   |  ---  |  Byte   | Checksum |
 * |   0, 1   |   0     |   1     |       |   N-1   |  0,1,2,3 |
 * -------------------------------------------------------------
 * \_2 bytes_/ \_ _ _ _ _ _N payload bytes_ _ _ _ _/\_4 bytes_/
 * \_ _ _ _ _ _ _ _ _ _N+6 total frame bytes_ _ _ _ _ _ _ _ _ _/
 *
 * 
 * @endverbatim
 *
 * The driver has to read N+6 bytes of the SPI frame to get N payload
 * bytes.  The first two bytes in the frame are the offset value to
 * the CRC-32 checksum field of the SPI frame.  This is followed by
 * the payload field of N bytes followed by 4 bytes of the CRC-32
 * checksum value calculated using all the offset and payload field
 * bytes as input.  For MAX_FRAME_SIZE=1024, N+6 cannot exceed 1024,
 * and, therefore, the payload size N is limited to 1024-6 = 1018
 * bytes.
 *
 * Payload format:
 * ------------------------------------------------------------------------------------------
 * |    Msg 1   |    Msg 1    |    Msg 2   |    Msg 2    |  ---  |    Msg X   |    Msg X    |
 * |   Length   |   Payload   |   Length   |   Payload   |  ---  |   Length   |   Payload   |
 * |    0, 1    |   0...Q-1   |    0, 1    |   0...R-1   |  ---  |    0, 1    |   0...S-1   |
 * ------------------------------------------------------------------------------------------
 * \__2 bytes__/ \__Q bytes__/\__2 bytes__/ \__R bytes__/         \__2 bytes__/ \__S bytes__/
 *
 *
 * The payload contains multiple messages starting with message
 * payload length followed by the payload bytes.
 *
 * Outgoing Messages
 * -----------------
 *
 * The Raspberry Pi will assume that the message will be the maximum
 * allowed size since it doesn't know the frame size ahead of time,
 * but the MCU only needs to provide the SPI frame buffer, and the
 * rest of the bytes outside of the frame are not important and will
 * be read by RPi as "garbage" data.
 * 
 *
 * @verbatim
 *
 * Outgoing message format:
 *  -----------------------------------------------------------------------------------------------------
 *  |  Offset  | Payload | Payload |       | Payload |  CRC-32  | Garbage | Garbage | Garbage | Garbage |
 *  |  to CRC  |  Byte   |  Byte   |  ---  |  Byte   | Checksum |  Byte   |  Byte   |  Byte   |  Byte   |
 *  |   0, 1   |   0     |   1     |       |   N-1   |  0,1,2,3 |   0     |   1     |   ---   |   M-1   |
 *  -----------------------------------------------------------------------------------------------------
 *  \_2 bytes_/ \_ _ _ _ _ _N payload bytes_ _ _ _ _/ \_4 bytes_/\_ _ _ _ _ _M garbage bytes_ _ _ _ _ _/
 *  \_ _ _ _ _ _ _ _ _ _ _ _ _ _ _N+M+6=MAX_FRAME_SIZE total frame bytes_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _/
 *
 *
 * @endverbatim
 *
 *
 * Payload format:
 * ------------------------------------------------------------------------------------------
 * |    Msg 1   |    Msg 1    |    Msg 2   |    Msg 2    |  ---  |    Msg X   |    Msg X    |
 * |   Length   |   Payload   |   Length   |   Payload   |  ---  |   Length   |   Payload   |
 * |    0, 1    |   0...Q-1   |    0, 1    |   0...R-1   |  ---  |    0, 1    |   0...S-1   |
 * ------------------------------------------------------------------------------------------
 * \__2 bytes__/ \__Q bytes__/\__2 bytes__/ \__R bytes__/         \__2 bytes__/ \__S bytes__/
 *
 *
 * The payload contains multiple messages starting with message payload length followed by
 * the payload bytes.
 */

/* Includes ---------------------------------------------------------------*/

#include "raspberry_pi.h"
#include "semphr.h"
#include "task.h"
#include "main.h"
#include "orion_config.h"
#include "stdbool.h"
#include "string.h"
#include "stdio.h"
#include "debug_off.h"
#include "rpi_msg_interface.h"
#include "crc.h"

/* Private defines ---------------------------------------------------------*/

#define FRAME_PAYLOAD_OFFSET     CRC_OFST_FIELD_SIZE         ///< The byte offset to the first payload byte
#define RPI_TX_QUEUE_LEN         5                           ///< Allow up to 5 outgoing messages in the queue
#define RPI_TX_QUEUE_ITEM_SIZE   sizeof(RpiMsg_t)            ///< The size of a message buffer required to store a RPi Tx message
#define SR_LINE_ASSERTED         GPIO_PIN_RESET              ///< The state of an asserted slave ready (SR) pin
#define MR_LINE_ASSERTED         GPIO_PIN_RESET              ///< The state of an asserted master ready (MR) pin
#define SR_LINE_DEASSERTED       GPIO_PIN_SET                ///< The state of a de-asserted slave ready (SR) pin
#define MR_LINE_DEASSERTED       GPIO_PIN_SET                ///< The state of a de-asserted master ready (MR) pin
#define NO_WAIT                  0                           ///< The timeout parameter for the queue and semaphore API to never block on their function calls
#define INITIAL_MSG_COUNT        0                           ///< Initial count value for the message counting semaphore
#define CHECKSUM_SIZE            sizeof(Checksum_t)          ///< The size of the CRC-32 checksum value

/**
   A timeout value for the RPi task to check MR line in case the MR
   edge is missed. Unit is milliseconds.
 */
#define RPI_TASK_TIMEOUT  1000

/**
   A timeout value for a the RPi task to check if an event was missed
   while in the middle of a transfer. 
 */
#define RPI_STATE_TIMEOUT 2

/**
  The maximum count value for the message counting semaphore
   (RPI_TX_QUEUE_LEN is for outgoing messages and another 1 is
   for received messages)
*/
#define MAX_TOTAL_MSG_COUNT      ((RPI_TX_QUEUE_LEN) + 1)

/** Assert slave ready line */
#define SR_ASSERT()              HAL_GPIO_WritePin(HOST_SR_GPIO_Port, HOST_SR_Pin, SR_LINE_ASSERTED)
/** De-assert slave ready line */
#define SR_DEASSERT()            HAL_GPIO_WritePin(HOST_SR_GPIO_Port, HOST_SR_Pin, SR_LINE_DEASSERTED)
/** Read master ready line */
#define READ_MR_STATE()          HAL_GPIO_ReadPin(HOST_MR_GPIO_Port, HOST_MR_Pin) 

// Define the preprocessor operator to place DMA buffers in the DMA memory region
#if defined( __ICCARM__ )
  #define DMA_BUFFER \
      _Pragma("location=\".dma_buffer\"")
#else
  #error Need to define DMA_BUFFER for this compiler
#endif


/* Private typedef ---------------------------------------------------------*/

/**
 * @brief  Enumeration of the RPi state machine states
 */
typedef enum _RpiSm_t
{
    RPI_SM_IDLE,        ///< Idle state
    RPI_SM_RDY,         ///< Ready to transmit (SPI transaction started; waiting for completion)
    RPI_SM_PARSE_MSG,   ///< Parse potential Messages received by the RPi 
    RPI_SM_FINISHED,    ///< SPI transfer has ended and has been acknowledged by the RPi
} RpiSm_t;

/**
 * @brief  Structure for a Tx/Rx RPi message
 *
 * Because the size of the message is unknown, all fields will
 * have variable positions except for the first field - the CRC offset.
 * The total structure size is MAX_FRAME_SIZE.
 */
typedef PACKED struct _RpiMsg_t
{
    /** SPI frame byes, including the payload and CRC checksum */
    uint8_t  data[MAX_FRAME_SIZE];  
} RpiMsg_t;

typedef uint32_t Checksum_t;


/* Private variables ---------------------------------------------------------*/

/**
 * Semaphore to count pending messages to transmit/receive and block
 * on if no incoming/outgoing messages are available (message semaphore)
 */
static SemaphoreHandle_t gRpiMessageSem = NULL;        ///< Semaphore to signal a new SPI transaction (new message being received/transmitted)
static SemaphoreHandle_t gRpiStateSem = NULL;          ///< Semaphore to signal a state machine change (state machine semaphore)
static QueueHandle_t     gRpiTxQueue = NULL;           ///< RPi queue handle for messages to be transmitted
static TaskHandle_t      gRpiTaskHandle = NULL;        ///< RPi task handle
static RpiSm_t           gRpiSmState = RPI_SM_IDLE;    ///< RPi state machine state
static volatile bool     gMrLineAsserted = false;      ///< True if Master Ready (MR) line is asserted (logic low)
static volatile bool     gSpiXferCompleted = false;    ///< True if a transfer over the SPI bus has been completed
static volatile bool     gIsRxTest         = true;     ///< If true, run Rx test when the user button is pressed.  If false, run Tx test.
static StaticSemaphore_t gRpiStateSemBuff;             ///< The buffer holding the #gRpiStateSem information
static StaticSemaphore_t gRpiMsgSemBuff;               ///< The buffer holding the #gRpiMessageSem information
static StaticQueue_t     gRpiTxQueueStructure;                                       // Queue data structure
static uint8_t           gRpiTxQueueStorage[RPI_TX_QUEUE_LEN * RPI_TX_QUEUE_ITEM_SIZE];       // Queue data storage

/**
 * Globals used for DMA are best aligned to 32 bytes and kept a
 *  length of 32 bytes.  The reason is that we must clear the cache
 *  for writes and invalidate pages in the D-cache for reads.  Pages
 *  in the D-cache are 32 bytes. Problems can develop if the other
 *  variables share the same page with these globals.  A read of
 *  another variable for example could validate a page that needs to
 *  stay invalid for example.
 *
 * At this moment, the DMA implementation is not used due to a
 *  bug. For now, this is not important and non-blocking. If the CPU
 *  load from handling RPi messages becomes too great, then we will
 *  need to revisit this. The buffer initialization has been kept
 *  since it does no harm. 
 */
#pragma data_alignment=32
DMA_BUFFER static RpiMsg_t gRpiRxBuf; ///< Holds a message that is being sent to us from the RPi

#pragma data_alignment=32
DMA_BUFFER static RpiMsg_t gRpiTxBuf; ///< Holds a message that will be sent to the RPi
    

extern SPI_HandleTypeDef RPI_SPI_HANDLE;


/* Function prototypes ---------------------------------------------------------*/

static bool msgIsValid(RpiMsg_t *msg);
static void RpiTask(void *pvParameters);
void TxCpltCallback(void);


/* Functions -------------------------------------------------------------------*/


/**
* @brief  Creates the RPi communication task and initializes the RPi comm module
*/
void RpiDriverInit(void)
{
    BaseType_t xReturned;

    // Create semaphore to wait for state machine state transitions on
    gRpiStateSem = xSemaphoreCreateBinaryStatic(&gRpiStateSemBuff);
    
    // Halt on error
    while (gRpiStateSem == NULL);


    // Create semaphore to wait for incoming/outgoing messages on
    gRpiMessageSem = xSemaphoreCreateCountingStatic(MAX_TOTAL_MSG_COUNT, INITIAL_MSG_COUNT, &gRpiMsgSemBuff);
    
    // Halt on error
    while (gRpiMessageSem == NULL);

    // Create a Tx and Rx message queues
    gRpiTxQueue = xQueueCreateStatic(RPI_TX_QUEUE_LEN, RPI_TX_QUEUE_ITEM_SIZE, &gRpiTxQueueStorage[0], &gRpiTxQueueStructure);

    // Halt on error  
    while (gRpiTxQueue == NULL);

    /*
    * TPS Task Creation
    */
    xReturned = xTaskCreate(RpiTask,                // Function that implements the task
                            "RPi task",             // Text name for the task
                            RPI_TASK_STACK_SIZE,    // Stack size in 32-bit words
                            NULL,                   // Parameter passed into the task
                            RPI_TASK_PRIORITY,      // Priority at which the task is created
                            &gRpiTaskHandle);       // Used to pass out the created task's handle
    /// @todo  Use xTaskCreateStatic instead

    while (xReturned != pdPASS);
    
    SR_DEASSERT();

    memset(&gRpiTxBuf, 0, sizeof(RpiMsg_t));
    memset(&gRpiRxBuf, 0, sizeof(RpiMsg_t));
}


/**
 * @brief  The Raspberry Pi task responsible for sending messages to the
 *         Raspberry Pi and receiving messages from the Raspberry Pi.
 *
 * @param  pvParameters  Parameters to pass to the task (not used)
 */
static void RpiTask(void *pvParameters)
{
    while (true)
    {
        // Wait for the Pi to make a request (assert the MR line) or
        // for the application to queue up a message for transmission
        DBG3_RESET();
        // A timeout here allows the driver to work if we miss a MR
        // transition event. This will always occur if the MCU is
        // reset while the RPi is trying to send a message and MR is
        // asserted .
        xSemaphoreTake(gRpiMessageSem, pdMS_TO_TICKS(RPI_TASK_TIMEOUT));
        DBG3_SET();
        
        // Run the state machine loop until the Finished state is reached
        while (gRpiSmState != RPI_SM_FINISHED)
        {
            // Wait for a state machine state transition event
            DBG4_RESET();
            if (gRpiSmState != RPI_SM_IDLE)
            {
                xSemaphoreTake(gRpiStateSem, pdMS_TO_TICKS(RPI_STATE_TIMEOUT));
            }
            DBG4_SET();

            // Read the MR line (note that the race condition involving the MR line
            // can only occur in the Idle state, so we deal with that separately below)
            gMrLineAsserted = (READ_MR_STATE() == MR_LINE_ASSERTED);


            switch (gRpiSmState)
            {
                case RPI_SM_IDLE:
                {
                    gSpiXferCompleted = false;

                    // Check if the RPi wants to send us a message
                    if (gMrLineAsserted || uxQueueMessagesWaiting(gRpiTxQueue))
                    {
                        xQueueReceive(gRpiTxQueue, &gRpiTxBuf, NO_WAIT);
                        
                        // We're waiting to receive a message from the
                        // RPi and MR just got asserted Prepare the
                        // SPI port to receive data into the Rx queue
                        HAL_SPI_TransmitReceive_IT(&RPI_SPI_HANDLE,
                                                   (uint8_t *)&gRpiTxBuf,
                                                   (uint8_t *)&gRpiRxBuf,
                                                   sizeof(RpiMsg_t));

                        gRpiSmState = RPI_SM_RDY;
                        
                        // Now assert the SR line to let the RPi know
                        // we're ready to transfer data
                        SR_ASSERT();
                    }
                    else
                    {
                        // If master has not been asserted, and there
                        // are no messages on the queue, then timed
                        // out on the gRpiMessageSem. This is useful
                        // if we miss the MR transition for whatever
                        // reason.
                        gRpiSmState = RPI_SM_FINISHED;
                    }
                }
                break;
              
                
                case RPI_SM_RDY:
                {
                    // Check if the SPI transmission is completed
                    if (gSpiXferCompleted && !gMrLineAsserted)
                    {
                        // We're done with the transfer
                        gRpiSmState = RPI_SM_PARSE_MSG;
                    }
                    // We are here because MR got de-asserted by RPi,
                    // possibly before we finished the SPI transfer,
                    // so we need to abort the possibly ongoing SPI
                    // transfer.
                    else if (!gSpiXferCompleted && !gMrLineAsserted)
                    {
                        // RPi has de-asserted MR - prepare to
                        // transition to the next state
                        gRpiSmState = RPI_SM_FINISHED;
                        
                        // Abort the ongoing SPI transaction (even if
                        // the SPI transfer has completed) so we can
                        // get the callback function to give the
                        // semaphore that allows the state machine to
                        // run to the next state.
                        HAL_SPI_Abort_IT(&RPI_SPI_HANDLE);
                    }
                    else
                    {
                        // Nothing
                    }
                }
                break;

                case RPI_SM_PARSE_MSG:
                {
                    if (msgIsValid(&gRpiRxBuf))
                    {
                        // Last SPI transfer was completed or aborted
                        if ((!gMrLineAsserted) && gSpiXferCompleted)
                        {      
                            // Validate the received message
                            uint16_t crcOffset = *((uint16_t *)&gRpiRxBuf.data[0]) & 0xFFFF;
                            Checksum_t calculatedCrc = Crc32((uint8_t *) &gRpiRxBuf, crcOffset);
                            Checksum_t receivedMsgCrc = 0;
                        
                            receivedMsgCrc = *(Checksum_t*) &gRpiRxBuf.data[crcOffset];
                        
                            if (receivedMsgCrc == calculatedCrc)
                            {
                                // Copy the frame payload (this doesn't
                                // include the CRC or the CRC offset
                                // fields)
                                RpiFrameToMessages((uint8_t *)&gRpiRxBuf.data[FRAME_PAYLOAD_OFFSET],
                                                   (crcOffset - FRAME_PAYLOAD_OFFSET));
                            }
                        }
                    }

                    memset(&gRpiTxBuf, 0, sizeof(RpiMsg_t));
                    memset(&gRpiRxBuf, 0, sizeof(RpiMsg_t));
                    
                    gRpiSmState = RPI_SM_FINISHED;
                        
                    // De-assert the SR line
                    SR_DEASSERT();

                    // added grind delay to lengthen de-assertion pulse
                    // this should add around 60 us minimum to the de-asserted pulse
                    // Only needed because the RPi may miss this pulse if it's too short
                    for (int grind = 0; grind < 200000; grind++);
                    // optimizations may remove this. 
                        
                    // Post the state machine semaphore to go to the next state
                    xSemaphoreGive(gRpiStateSem);
                }
                break;
            }  // switch (gRpiSmState)
        }  // while (gRpiSmState != RPI_SM_FINISHED)
        
        // The state machine has reached the finished state causing
        // the state machine loop to end, so reset the state machine
        // to the Idle state
        gRpiSmState = RPI_SM_IDLE;
        
        // Indicate we're at the end of the state machine loop back to
        // the Idle state
        DBG4_RESET();
    }
}

/**
 * @brief Checks if the msg is valid.
 *
 *        Currently, we only care if the msg is full of 0
 *        data. Otherwise, we consider the message to be valid.
 *
 * @param msg - An Rpi message
 * @return true if valid, false if otherwise. 
 */
static bool msgIsValid(RpiMsg_t *msg)
{
    for (uint16_t i = 0; i < CRC_OFST_FIELD_SIZE; ++i)
    {
        if (msg->data[i] != 0)
        {
            return true;
        }
    }

    return false;
}

/**
 * @brief  Callback on RPi MR line change event
 *
 * @param  higherPriorityTaskWoken  Determines if context switch is needed after the interrupt
 *
 */
void RpiMasterReadyIrq(BaseType_t * const higherPriorityTaskWoken)
{
    if ((gRpiSmState == RPI_SM_IDLE) &&
        (READ_MR_STATE() == MR_LINE_ASSERTED))
    {
        // Indicate to the message processing loop that the RPi is
        // initiating a message transmission
        DBG1_SET();
        if (gRpiMessageSem)
        {
            xSemaphoreGiveFromISR(gRpiMessageSem, higherPriorityTaskWoken);
        }
        DBG1_RESET();
    }
    // When we initiate a transfer to the RPi, this interrupt will
    //   fire when the MR is toggled. If the MCU initiates this
    //   transfer, it will assert SR and the MR will respond by
    //   becoming asserted. In this case a message transfer has been
    //   set-up and the RPi can begin outputting a clock. We do not
    //   want the state machine to transition until the message
    //   transfer is complete, which we determine by the MR
    //   de-asserting.
    else if ((gRpiSmState == RPI_SM_RDY) &&
        (READ_MR_STATE() == MR_LINE_DEASSERTED))
    {
        gSpiXferCompleted = true;
        xSemaphoreGiveFromISR(gRpiStateSem, higherPriorityTaskWoken);
    }
    else
    {
        // Indicate to the state machine that MR line was asserted/de-asserted, so:
        // - Either the RPi is ready to receive a message from us, or
        // - The RPi is terminating a SPI transaction.
        DBG2_SET();
        if (gRpiStateSem)
        {
            xSemaphoreGiveFromISR(gRpiStateSem, higherPriorityTaskWoken);
        }
        DBG2_RESET();
    }
}

/**
 * @brief   Function to send a message to RPi
 *
 * @details This function will put a message on the queue and signal the RPi
 *          message processing task to start processing the new message.
 *
 * @note    Do not call this function from ISR context
 *
 * @param   buf      Pointer to a buffer with data to transmit to the RPi
 * @param   size     Number of bytes in the buffer pointed to by buf
 *
 * @return  true if successful; false if the transmit queue is full
 */
bool RpiSendFrame(uint8_t * buf, uint16_t size)
{
    BaseType_t retVal = pdFALSE;
    // Only send messages that fit in the SPI frame
    if (size <= MAX_FRAME_SIZE)
    {
        // Put the new message on the queue
        retVal = xQueueSend(gRpiTxQueue, buf, NO_WAIT);

        // Only unblock the message processing loop if we successfully
        // added the new message to the queue
        if (retVal == pdTRUE)
        {
            // Unblock the RPi message processing loop
            DBG1_SET();
            xSemaphoreGive(gRpiMessageSem);
            DBG1_RESET();
        }
        else
        {
            LogWarning("Failed to queue message on outgoing RPi queue.\r\n");
        }
    }

    return (retVal == pdTRUE);
}

/**
 * @brief  This function is called by the HAL SPI driver
 *         after the SPI transaction is aborted
 *
 */
void HAL_SPI_AbortCpltCallback(SPI_HandleTypeDef *hspi)
{
    BaseType_t higherPriorityTaskWoken = pdFALSE;

    // Unblock the RPi state machine loop
    gSpiXferCompleted = true;
    xSemaphoreGiveFromISR(gRpiStateSem, &higherPriorityTaskWoken);

    portYIELD_FROM_ISR(higherPriorityTaskWoken);
}
