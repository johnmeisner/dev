/****************************************************************************//**
* @file      sysclk_driver.h
*
* @brief     Header file to control the system clock that is sent to the AMB's
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _SYSCLK_DRIVER_H_
#define _SYSCLK_DRIVER_H_
#include "error.h"
#include "stdbool.h"
#include "ctrl_interface.h"

/**
 * @brief Used to set system clock configuration
 */ 
typedef enum _sysClkConfig_t
{
    SYS_CLK_CONFIG_AMB_ONLY,        //Use XAXB sources; only use clocks OUT1, OUT2, OUT8, and OUT9 for the AMB's
    SYS_CLK_CONFIG_AMB_TAM,         //Use XAXB sources; Add the TAM clock (OUT5)
    SYS_CLK_CONFIG_AMB_MASTER,      //Use XAXB sources; Add the Master clock for dual transmitter mode (OUT4)
    SYS_CLK_CONFIG_AMB_MASTER_TAM,  //Use XAXB sources; Use both TAM and Master
    SYS_CLK_CONFIG_AMB_SLAVE,       //Use IN1 source (which comes from Master).  
    SYS_CLK_CONFIG_AMB_SLAVE_TAM,   //Use IN1 source (which comes from Master).  Enable the TAM clock
    SYS_CLK_MAX_CONFIG
} SysClkConfig_t;


void InitSysClkDriver(void);
CotaError_t SystemClockInit(void);
CotaError_t ChangeFreq(uint32_t freq);
CotaError_t ChangeFreqWithCommChan(uint8_t commChannel);
bool IsValidCommCh(uint8_t commChannel);
void SysClock_I2C_TxCpltCallback(I2C_HandleTypeDef *hi2c);
void SysClock_I2C_RxCpltCallback(I2C_HandleTypeDef *hi2c);
void SysClkDump(CtrlResp_t* msg);
CotaError_t SafeSysClkWriteReg(uint16_t addr, uint8_t regVal);
CotaError_t SysClkReadReg(uint16_t addr, uint8_t* regVal);
void SysClkI2CDisable(void);
CotaError_t EnableSystemClock(bool enabled);
CotaError_t SetResetEthernetClock(bool setreset);
CotaError_t UpdateTransmitFrequency(void);
void DisableSystemClockIfClockIdleIsSet(void);
#endif // #ifndef _SYSCLK_DRIVER_H_
