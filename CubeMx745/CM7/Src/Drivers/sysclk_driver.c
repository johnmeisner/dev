/****************************************************************************//**
* @file      sysclk_driver.c
*
* @brief     Control the system clock that is sent to the UVP's
*            @todo We need to test the TAM clock, master clocks, and slave clock inputs
*
*            Information about system clocks can attained by referencing the 
*            "Si5341, Si5340 Rev D Family Reference Manual"
*
*            The register are also set with help of a clock builder application  which uses the input file
*            153_125MHz_10249B_testing_lowest_final.slabtimeproj located in this project.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "FreeRTOS.h"
#include "semphr.h"
#include "string.h"
#include "main.h"
#include "sysclk_driver.h"
#include "orion_config.h"
#include "debug_off.h"
#include "task.h"
#include "cli_task.h"
#include "stdio.h"
#include "stdbool.h"
#include "error.h"
#include "ctrl_interface.h"
#include "rpi_msg_interface.h"
#include "nvm_driver.h"

extern I2C_HandleTypeDef hi2c2;                         ///The handle to the I2C hal driver for the system clock

#define SYSCLK_I2C_ADDR         (0x74)                  ///< The 7 bit I2C address of system clock
#define SYSCLK_ADDR_BYTE        (SYSCLK_I2C_ADDR << 1)  ///< The byte that is passed as the address to the hal driver, which requires the address sit in the upper 7 bits
#define PAGE_REG_ADDR           0x1                     ///< The register address in every page that sets the page number
#define MAX_NUM_REG             1                       ///< The maximum number of clock configurations
#define I2C_WAIT_TIMEOUT        100                     ///< A timeout to wait to I2C Tx or Rx completion in milliseconds
#define INVALID_PAGE            0xFF                    ///< An invalid page number to the system clock.  Indicates the page number must be updated on the last call.
#define VARIANT_1_CLOCK         0                       ///< The index in the register array for the VARIANT 1 clock settings
#define SYSCLK_DMA_BUFFER_LEN   32                      ///< The system clock buffer length, which we align to 32 bytes

#define MAX_NUM_NDIV            5                       ///< The maximum number of the N dividers
#define FREQ_RATIO_V1           16                      ///< Used to adjust tx frequency
#define IN_FREQ_MHZ             50                      ///< Used to adjust tx frequency
#define SYSCLK_PAGE_ADDR(addr)  (addr >> 8)             ///< Gets the page addr from a register address
#define SYSCLK_REG_ADDR(addr)   (addr & 0xff)           ///< Get the position in the page from a register address
#define MAX_SYSCLK_ADDR         0xBFF                   ///< The maximum system clock address
#define AMB_NUM_N_DIV           4                       ///< AMB's use the first four N dividers

//defines for registers used for changing XA/XB input to IN1 for Slave mode
#define LOSIN_INTR_MSK_REG             0x0018         ///< Register enables mask for different input clocks
#define LOSIN_INTR_MSK_IN1_SIZE        1              ///< Size of bit field needed to set to 0 for IN1 Clock
#define LOSIN_INTR_MSK_IN1_OFFSET      1              ///< Interupt offset bit for IN1 clock
#define LOSIN_INTR_MSK_IN1_VAL         0              ///< Value to set the bit to
#define IN_SEL_REGCTRL_REG             0x0021         ///< Input clock select register
#define IN_SEL_REGCTRL_SIZE            2              ///< Size of field to change inside register
#define IN_SEL_REGCTRL_OFFSET          1              ///< Offset of field to change inside register
#define IN_SEL_REGCTRL_VAL             1              ///< Value to set the reg ctrl field to
#define LOS_EN_REG                     0x002C         ///< Register that enable LOS for input clocks
#define LOS_EN_REG_IN1_SIZE            1              ///< LOS_EN bit size for IN1
#define LOS_EN_REG_IN1_OFFSET          1              ///< LOS_EN bit offset for IN1
#define LOS_EN_REG_IN1_VAL             1              ///< LOS_EN bit value for IN1
#define LOS_EN_REG_XAXB_SIZE           1              ///< LOS_EN bit size for XAXB
#define LOS_EN_REG_XAXB_OFFSET         4              ///< LOS_EN bit offset for XAXB
#define LOS_EN_REG_XAXB_VAL            0              ///< LOS_EN bit value for XAXB
#define LOS_VAL_TIME_REG               0x002D         ///< Register for LOS_VAL_TIME
#define LOS_VAL_TIME_REG_IN1_SIZE      2              ///< Size of the IN1 field for LOS_VAL_TIME
#define LOS_VAL_TIME_REG_IN1_OFFSET    2              ///< The offset of the IN1 field for LOS_VAL_TIME
#define LOS_VAL_TIME_REG_IN1_VAL       1              ///< The value of the IN1 field for LOS_VAL_TIME
#define LOS1_TRG_THRESH_REG            0x0030         ///< Start of two bytes storing the 16 LOS trigger threshold value for IN1
#define LOS1_TRG_THRESH_BYTE           2              ///< Number of bytes used by this register
#define LOS1_TRG_THRESH_VAL            0x008A         ///< Value of LOS1 trigger threshold
#define LOS1_CLR_THRESH_REG            0x0038         ///< Start of two bytes storing the 16 LOS clear threshold value for IN1
#define LOS1_CLR_THRESH_BYTE           2              ///< Number of bytes used by this register
#define LOS1_CLR_THRESH_VAL            0x008A         ///< Value of LOS1 trigger threshold
#define LOS1_DIV_SEL_REG               0x0042         ///< LOS divider select register for IN1
#define LOS1_DIV_SEL_SIZE              5              ///< Size of the LOS divider select field for IN1
#define LOS1_DIV_SEL_OFFSET            0              ///< Offset of the LOS divider select field for IN1
#define LOS1_DIV_SEL_VAL               5              ///< Value for LOS1_DIV_SEL field
#define P1_DIV_REG                     0x0212         ///< Start of 6 byte P1 divider value
#define P1_DIV_BYTES                   6              ///< Number of bytes used by the P1 divider
#define P1_DIV_VAL                     0x000000000001 ///< P1 divider value
#define P1_SET_REG                     0x218          ///< Start of 4 byte P1_SET register
#define P1_SET_BYTES                   4              ///< Number of bytes used by the P1 set register
#define P1_SET_VALUE                   0x00000001     ///< Value for P1_SET register
#define XAXB_CONF_REG                  0x090E         ///< XAXB configuration register
#define XAXB_CONF_EXTCLK_EN_SIZE       1              ///< XAXB external clock enable bit size
#define XAXB_CONF_EXTCLK_EN_OFFSET     0              ///< XAXB external clock enable bit offset
#define XAXB_CONF_EXTCLK_EN_VAL        0              ///< XAXB external clock enable bit value
#define XAXB_CONF_PDNB_SIZE            1              ///< XAXB PDNB bit size
#define XAXB_CONF_PDNB_OFFSET          1              ///< XAXB PDNB bit offset
#define XAXB_CONF_PDNB_EN_VAL          0              ///< XAXB PDNB bit value
#define CLK_INPUT_CTRL_REG             0x0949         ///< Register for clock input control
#define CLK_INPUT_IN1_EN_SIZE          1              ///< The size of the IN1 enable clock input control bit
#define CLK_INPUT_IN1_EN_OFFSET        1              ///< The offset of the IN1 enable clock input control bit
#define CLK_INPUT_IN1_EN_VAL           1              ///< The value of the IN1 enable clock input control bit
#define INX_TO_PFD_EN                  0x094A         ///< Routes input clock to the phase detector
#define IN1_TO_PFD_EN_SIZE             1              ///< Bit size to routes IN1 clock to the phase detector
#define IN1_TO_PFD_EN_OFFSET           5              ///< Bit offset to routes IN1 clock to the phase detector
#define IN1_TO_PFD_EN_VAL              1              ///< Bit offset to routes IN1 clock to the phase detector

//defines for register to enable the N4 divider if OUT4 (Master) of OUT5 (TAM) clock are enabled
#define N4_NUM_VAL                     0x004360000000 ///< The value of the N4 numerator
#define N4_DEN_REG                     0x0334         ///< The N4 divder denominator value
#define N4_DEN_VAL                     0x80000000     ///< The value of the N4 denominator
#define N4_DELAY_VAL                   0x0000         ///< The value of the N4 denominator
#define NCLK_TO_OUT_EN_REG             0xA03          ///< The register to allow N divider to be used to OUT clocks
#define NCLK_TO_OUT_EN_N4_SIZE         1              ///< The size of the N4 enable bit
#define NCLK_TO_OUT_EN_N4_OFFSET       4              ///< The offset of the N4 enable bit
#define NCLK_TO_OUT_EN_N4_VAL          1              ///< The value of the N4 enable bit
#define N_PDNB_REG                     0x0A05         ///< N diver power down register (0 = power down)
#define N4_PDNB_REG_SIZE               1              ///< The size of the N4 power down bit
#define N4_PDNB_REG_OFFSET             4              ///< The offset of the N4 power down bit
#define N4_PDNB_REG_VAL                1              ///< The value of the N4 power down bit
#define N_CLK_DIS_REG                  0x0B4A         ///< N Divider disable register )1 = disable)
#define N4_CLK_DIS_SIZE                1              ///< The size of the N4 clock disable bit
#define N4_CLK_DIS_OFFSET              4              ///< The offset of the N4 clock disable bit
#define N4_CLK_DIS_VAL                 0              ///< The value of the N4 clock disable bit

//define for enabling OUT4 (Master) clock
#define OUT4_CONFIG_REG                0x011C         ///< Out4 configuration register
#define OUT4_CONFIG_VAL                0x06           ///< Out4 value enabling the OUT4
#define OUT4_FORMAT_REG                0x011D         ///< Out4 format register
#define OUT4_FORMAT_VAL                0x09           ///< Out4 format register default value
#define OUT4_AMP_CM_REG                0x011E         ///< Out4 common mode and amplitude register
#define OUT4_AMP_CM_VAL                0x1D           ///< Out4 common mode and amplitude default value
#define OUT4_MUX_INV_REG               0x011F         ///< Out4 mux and inversion register
#define OUT4_MUX_INV_VAL               0x1C           ///< Out4 mux and inversion default value

//define for enabling OUT5 (TAM) clock
#define OUT5_CONFIG_REG                0x0121         ///< Out5 configuration register
#define OUT5_CONFIG_VAL                0x06           ///< Out5 value enabling the OUT4
#define OUT5_FORMAT_REG                0x0122         ///< Out5 format register
#define OUT5_FORMAT_VAL                0x09           ///< Out5 format register default value
#define OUT5_AMP_CM_REG                0x0123         ///< Out5 common mode and amplitude register
#define OUT5_AMP_CM_VAL                0x1D           ///< Out5 common mode and amplitude default value
#define OUT5_MUX_INV_REG               0x0124         ///< Out5 mux and inversion register
#define OUT5_MUX_INV_VAL               0x1C           ///< Out5 mux and inversion default value

//defines used set different fields of output clock registers
//These register can be adjusted during FCC testing and be configured by settings in the NVM
#define SYS_CLK_CM_MODE_SIZE        4                   ///< The size of the common mode field
#define SYS_CLK_CM_MODE_OFFSET      0                   ///< The offset of the common mode field
#define SYS_CLK_AMPLITUDE_SIZE      3                   ///< The size of the amplitude field
#define SYS_CLK_AMPLITUDE_OFFSET    4                   ///< The offset of the amplitude field
#define SYS_CLK_FORMAT_SIZE         3                   ///< The size of the format field
#define SYS_CLK_FORMAT_OFFSET       0                   ///< The offset of the format field
#define SYS_CLK_INV_SIZE            2                   ///< The size of the inversion field
#define SYS_CLK_INV_OFFSET          6                   ///< The offset of the inversion field

//define used enable or disable system clocks
#define SYS_CLK_OUTALL_DISABLE_LOW  0x0102              ///< A register the disable all clock outputs when set to 0
#define SYS_CLK_OUTALL_ENABLE       1                   ///< The value for SYS_CLK_OUTALL_DISABLE_LOW to enable all clock outputs.
#define SYS_CLK_OUTALL_DISABLE      0                   ///< The value for SYS_CLK_OUTALL_DISABLE_LOW to disable all clock outputs.

#define SYS_CLK_CAL_DELAY_MS        300                 ///< From ClockBuilder: Delay is worst case time for device to complete any calibration
                                                        /// that is running due to device state change previous to this script being processed.
//register used to change the transmit frequency
#define M_DIV_NUMERATOR_REG         0x0235              ///< The M divider numerator register   
#define M_DIV_NUMERATOR_BYTES       6                   ///< The M divider numerator register size in bytes
#define M_DIV_DENOMENATOR_REG       0x023B              ///< The M divider denominator register   
#define M_DIV_DENOMENATOR_BYTES     4                   ///< The M divider denominator register size in bytes
#define R0_DIV_REG                  0x024A              ///< The R0 divider register
#define RX_DIV_BYTES                3                   ///< The R0 divider size in bytes
#define N0_DIV_DEN_REG              0x308               ///< The N0 divider denominator register
#define NX_DIV_DEN_BYTES            4                   ///< The N dividers denominator size in bytes

#define NX_NUM_BYTES                6                   ///< The number of bytes in the Nx numerators
#define N0_NUM_REG                  0x0302              ///< The N0 divder numerator value
#define N1_NUM_REG                  0x030D              ///< The N1 divder numerator value
#define N2_NUM_REG                  0x0318              ///< The N2 divder numerator value
#define N3_NUM_REG                  0x0323              ///< The N3 divder numerator value
#define N4_NUM_REG                  0x032E              ///< The N4 divder numerator value

#define NX_DELAY_REG_SIZE           2                   ///< The size in bytes the N delay registers
#define N0_DELAY_REG                0x0359              ///< The N0 divder delay value
#define N1_DELAY_REG                0x035B              ///< The N1 divder delay value
#define N2_DELAY_REG                0x035D              ///< The N2 divder delay value
#define N3_DELAY_REG                0x035F              ///< The N3 divder delay value
#define N4_DELAY_REG                0x0361              ///< The N4 divder delay value

#define N0_UPDATE_REG               0x030C              ///< N0 update register
#define N1_UPDATE_REG               0x0317              ///< N1 update register
#define N2_UPDATE_REG               0x0322              ///< N2 update register
#define N3_UPDATE_REG               0x032D              ///< N3 update register
#define N4_UPDATE_REG               0x0338              ///< N4 update register

static SemaphoreHandle_t gSysClkI2CTxSem;               ///< Semaphore signaling a complete i2c transaction
static StaticSemaphore_t gSysClkI2CTxSemBuff;           ///< The buffer holding the information for the #gSysClkI2CTxSem semaphore

static SemaphoreHandle_t gSysClkI2CRxSem;               ///< Semaphore signaling a complete i2c transaction
static StaticSemaphore_t gSysClkI2CRxSemBuff;           ///< The buffer holding the information for the #gSysClkI2CRxSemBuff semaphore
static uint8_t gLastPageWritten = INVALID_PAGE;         ///< We need to keep track of the last page of addresses we write to in the system clock so we don't have to keep re-writing it.
static uint8_t gIdleClock;                              ///< If true, the clock will go idle when not in use
static bool gSysClkIsEnabled = false;                   ///< If true, the sysclk is enabled. False if disabled.

/**
 * @brief stucture mapping comm channel to the desired transmit frequency
 */
typedef struct _MapComToFreqInMHz
{
    uint8_t  commChannel;   ///< The comm channel
    uint16_t freqInMhz;     ///< Transmit frequenxy in Mhz
} MapComToFreqInMHz;

/**
 * @brief Array mapping comm channel to the desired transmit frequency
 */
static MapComToFreqInMHz gComToFreq[] = {
    { 24, 2460},
    { 25, 2450},
    { 26, 2440},
};

const int32_t gComMapSize = (sizeof(gComToFreq)/sizeof(gComToFreq[0]));   ///< Number of entries in the comm to frequency map

/**
 * @brief An array specifying the location of the common mode and amplitude registers for the system clock
 */
static uint16_t gSysClkAmpCmRegs[] = {
    0x010A,
    0x010F,
    0x0114,
    0x0119,
    0x011E,
    0x0123,
    0x0128,
    0x012D,
    0x0132,
    0x013C,  
};

const int32_t gSysClkAmpCmRegsSize = (sizeof(gSysClkAmpCmRegs)/sizeof(gSysClkAmpCmRegs[0]));   ///< Number of output clock CM AMP registers

/**
 * @brief An array specifying the location of the format registers for the system clock
 */
static uint16_t gSysClkFormatRegs[] = {
    0x0109,
    0x010E,
    0x0113,
    0x0118,
    0x011D,
    0x0122,
    0x0127,
    0x012C,
    0x0131,
    0x013B,  
};

const int32_t gSysClkFormatRegsSize = (sizeof(gSysClkFormatRegs)/sizeof(gSysClkFormatRegs[0]));   ///< Number of output clock Format registers

/**
 * @brief An array specifying the location of the inversion registers for the system clock for the AMB's
 *        note: These are indexed by AMB, not by the clock output number
 */
static uint16_t gSysClkAmbInversionRegs[] = {
    0x013D,           //Inversion register for AMB0 (clock 9)
    0x0110,           //Inversion register for AMB1 (clock 1)    
    0x0115,           //Inversion register for AMB2 (clock 2)
    0x0133,           //Inversion register for AMB3 (clock 8)    
};

const int32_t gSysClkAmbInversionRegsSize = (sizeof(gSysClkAmbInversionRegs)/sizeof(gSysClkAmbInversionRegs[0]));   ///< Number of AMB inversion registers

/**
 * @brief This structure maps the clock register address to their values. 
   @note  Even though there is only one set of register values at this point,
          we maintain the array structure, so we can add more later if needed.
 */
typedef struct _ClockTreeRegister_t
{
    uint16_t addr;                      ///< The address in the clock to write to
    uint8_t  regs[MAX_NUM_REG];         ///< The values to write to the clock register

} ClockTreeRegister_t;

/**
 *  @brief Various clock configuations only differ by a few registers, so we define them in this structure
 *         so we don't use a bunch of memory storing the same register for each clock config. 
 */  
typedef struct _ClockTreeOverlayRegister_t
{
    uint16_t addr;                                  ///< The address in the clock to write to
    uint8_t  regs[SYS_CLK_MAX_CONFIG];              ///< The values to write to the clock register

} ClockTreeOverlayRegister_t;

/**
 * @brief the preamble registers are written first 
 */
static ClockTreeRegister_t gPreambleList[] = {
    {0x0B24, {0xC0}},
    {0x0B25, {0x00}},
    {0x0502, {0x01}},
    {0x0505, {0x03}},
    {0x0957, {0x17}},
    {0x0B4E, {0x1A}}
};
const int32_t gPreambleListSize = sizeof(gPreambleList)/sizeof(gPreambleList[0]);  ///< Number of registers to write in the preambleList

/**
 * @brief These register values are written after the pre-amble
 * @note This register list is generated by clockbuilder using the *.slabtimeproj project file located in EWARM
 */
static ClockTreeRegister_t gRegisterList[] = {
    {0x0006, {0x00}},
    {0x0007, {0x00}},
    {0x0008, {0x00}},
    {0x000B, {0x74}},
    {0x0017, {0xD0}},
    {0x0018, {0xFF}},
    {0x0021, {0x0F}},
    {0x0022, {0x00}},
    {0x002B, {0x02}},
    {0x002C, {0x20}},
    {0x002D, {0x00}},
    {0x002E, {0x00}},
    {0x002F, {0x00}},
    {0x0030, {0x00}},
    {0x0031, {0x00}},
    {0x0032, {0x00}},
    {0x0033, {0x00}},
    {0x0034, {0x00}},
    {0x0035, {0x00}},
    {0x0036, {0x00}},
    {0x0037, {0x00}},
    {0x0038, {0x00}},
    {0x0039, {0x00}},
    {0x003A, {0x00}},
    {0x003B, {0x00}},
    {0x003C, {0x00}},
    {0x003D, {0x00}},
    {0x0041, {0x00}},
    {0x0042, {0x00}},
    {0x0043, {0x00}},
    {0x0044, {0x00}},
    {0x009E, {0x00}},
    {0x0102, {0x01}},
    {0x0108, {0x01}},
    {0x0109, {0x09}},
    {0x010A, {0x3B}},
    {0x010B, {0x28}},
    {0x010D, {0x06}},
    {0x010E, {0x09}},
    {0x010F, {0x0D}},
    {0x0110, {0x18}},
    {0x0112, {0x06}},
    {0x0113, {0x09}},
    {0x0114, {0x0D}},
    {0x0115, {0x19}},
    {0x0117, {0x01}},
    {0x0118, {0x09}},
    {0x0119, {0x3B}},
    {0x011A, {0x28}},
    {0x011C, {0x01}},
    {0x011D, {0x09}},
    {0x011E, {0x33}},
    {0x011F, {0x08}},
    {0x0121, {0x01}},
    {0x0122, {0x09}},
    {0x0123, {0x3B}},
    {0x0124, {0x28}},
    {0x0126, {0x01}},
    {0x0127, {0x09}},
    {0x0128, {0x33}},
    {0x0129, {0x08}},
    {0x012B, {0x01}},
    {0x012C, {0x09}},
    {0x012D, {0x3B}},
    {0x012E, {0x28}},
    {0x0130, {0x06}},
    {0x0131, {0x09}},
    {0x0132, {0x0D}},
    {0x0133, {0x1A}},
    {0x013A, {0x06}},
    {0x013B, {0x09}},
    {0x013C, {0x0D}},
    {0x013D, {0x1B}},
    {0x013F, {0x00}},
    {0x0140, {0x00}},
    {0x0141, {0x40}},
    {0x0206, {0x00}},
    {0x0208, {0x00}},
    {0x0209, {0x00}},
    {0x020A, {0x00}},
    {0x020B, {0x00}},
    {0x020C, {0x00}},
    {0x020D, {0x00}},
    {0x020E, {0x00}},
    {0x020F, {0x00}},
    {0x0210, {0x00}},
    {0x0211, {0x00}},
    {0x0212, {0x00}},
    {0x0213, {0x00}},
    {0x0214, {0x00}},
    {0x0215, {0x00}},
    {0x0216, {0x00}},
    {0x0217, {0x00}},
    {0x0218, {0x00}},
    {0x0219, {0x00}},
    {0x021A, {0x00}},
    {0x021B, {0x00}},
    {0x021C, {0x00}},
    {0x021D, {0x00}},
    {0x021E, {0x00}},
    {0x021F, {0x00}},
    {0x0220, {0x00}},
    {0x0221, {0x00}},
    {0x0222, {0x00}},
    {0x0223, {0x00}},
    {0x0224, {0x00}},
    {0x0225, {0x00}},
    {0x0226, {0x00}},
    {0x0227, {0x00}},
    {0x0228, {0x00}},
    {0x0229, {0x00}},
    {0x022A, {0x00}},
    {0x022B, {0x00}},
    {0x022C, {0x00}},
    {0x022D, {0x00}},
    {0x022E, {0x00}},
    {0x022F, {0x00}},
    {0x0235, {0x00}},
    {0x0236, {0x00}},
    {0x0237, {0x00}},
    {0x0238, {0xC0}},
    {0x0239, {0x86}},
    {0x023A, {0x00}},
    {0x023B, {0x00}},
    {0x023C, {0x00}},
    {0x023D, {0x00}},
    {0x023E, {0x80}},
    {0x024A, {0x00}},
    {0x024B, {0x00}},
    {0x024C, {0x00}},
    {0x024D, {0x00}},
    {0x024E, {0x00}},
    {0x024F, {0x00}},
    {0x0250, {0x00}},
    {0x0251, {0x00}},
    {0x0252, {0x00}},
    {0x0253, {0x00}},
    {0x0254, {0x00}},
    {0x0255, {0x00}},
    {0x0256, {0x00}},
    {0x0257, {0x00}},
    {0x0258, {0x00}},
    {0x0259, {0x00}},
    {0x025A, {0x00}},
    {0x025B, {0x00}},
    {0x025C, {0x00}},
    {0x025D, {0x00}},
    {0x025E, {0x00}},
    {0x025F, {0x00}},
    {0x0260, {0x00}},
    {0x0261, {0x00}},
    {0x0262, {0x00}},
    {0x0263, {0x00}},
    {0x0264, {0x00}},
    {0x0268, {0x00}},
    {0x0269, {0x00}},
    {0x026A, {0x00}},
    {0x026B, {0x31}},
    {0x026C, {0x30}},
    {0x026D, {0x32}},
    {0x026E, {0x34}},
    {0x026F, {0x39}},
    {0x0270, {0x42}},
    {0x0271, {0x00}},
    {0x0272, {0x00}},
    {0x0302, {0x00}},
    {0x0303, {0x00}},
    {0x0304, {0x00}},
    {0x0305, {0x00}},
    {0x0306, {0x16}},
    {0x0307, {0x00}},
    {0x0308, {0x00}},
    {0x0309, {0x00}},
    {0x030A, {0x00}},
    {0x030B, {0x80}},
    {0x030C, {0x00}},
    {0x030D, {0x00}},
    {0x030E, {0x00}},
    {0x030F, {0x00}},
    {0x0310, {0x00}},
    {0x0311, {0x16}},
    {0x0312, {0x00}},
    {0x0313, {0x00}},
    {0x0314, {0x00}},
    {0x0315, {0x00}},
    {0x0316, {0x80}},
    {0x0317, {0x00}},
    {0x0318, {0x00}},
    {0x0319, {0x00}},
    {0x031A, {0x00}},
    {0x031B, {0x00}},
    {0x031C, {0x16}},
    {0x031D, {0x00}},
    {0x031E, {0x00}},
    {0x031F, {0x00}},
    {0x0320, {0x00}},
    {0x0321, {0x80}},
    {0x0322, {0x00}},
    {0x0323, {0x00}},
    {0x0324, {0x00}},
    {0x0325, {0x00}},
    {0x0326, {0x00}},
    {0x0327, {0x16}},
    {0x0328, {0x00}},
    {0x0329, {0x00}},
    {0x032A, {0x00}},
    {0x032B, {0x00}},
    {0x032C, {0x80}},
    {0x032D, {0x00}},
    {0x032E, {0x00}},
    {0x032F, {0x00}},
    {0x0330, {0x00}},
    {0x0331, {0x00}},
    {0x0332, {0x00}},
    {0x0333, {0x00}},
    {0x0334, {0x00}},
    {0x0335, {0x00}},
    {0x0336, {0x00}},
    {0x0337, {0x00}},
    {0x0338, {0x00}},
    {0x0339, {0x1F}},
    {0x033B, {0x00}},
    {0x033C, {0x00}},
    {0x033D, {0x00}},
    {0x033E, {0x00}},
    {0x033F, {0x00}},
    {0x0340, {0x00}},
    {0x0341, {0x00}},
    {0x0342, {0x00}},
    {0x0343, {0x00}},
    {0x0344, {0x00}},
    {0x0345, {0x00}},
    {0x0346, {0x00}},
    {0x0347, {0x00}},
    {0x0348, {0x00}},
    {0x0349, {0x00}},
    {0x034A, {0x00}},
    {0x034B, {0x00}},
    {0x034C, {0x00}},
    {0x034D, {0x00}},
    {0x034E, {0x00}},
    {0x034F, {0x00}},
    {0x0350, {0x00}},
    {0x0351, {0x00}},
    {0x0352, {0x00}},
    {0x0353, {0x00}},
    {0x0354, {0x00}},
    {0x0355, {0x00}},
    {0x0356, {0x00}},
    {0x0357, {0x00}},
    {0x0358, {0x00}},
    {0x0359, {0x00}},
    {0x035A, {0x0D}},
    {0x035B, {0x00}},
    {0x035C, {0x1B}},
    {0x035D, {0x00}},
    {0x035E, {0x28}},
    {0x035F, {0x00}},
    {0x0360, {0x36}},
    {0x0361, {0xE6}},
    {0x0362, {0x35}},
    {0x0802, {0x00}},
    {0x0803, {0x00}},
    {0x0804, {0x00}},
    {0x0805, {0x00}},
    {0x0806, {0x00}},
    {0x0807, {0x00}},
    {0x0808, {0x00}},
    {0x0809, {0x00}},
    {0x080A, {0x00}},
    {0x080B, {0x00}},
    {0x080C, {0x00}},
    {0x080D, {0x00}},
    {0x080E, {0x00}},
    {0x080F, {0x00}},
    {0x0810, {0x00}},
    {0x0811, {0x00}},
    {0x0812, {0x00}},
    {0x0813, {0x00}},
    {0x0814, {0x00}},
    {0x0815, {0x00}},
    {0x0816, {0x00}},
    {0x0817, {0x00}},
    {0x0818, {0x00}},
    {0x0819, {0x00}},
    {0x081A, {0x00}},
    {0x081B, {0x00}},
    {0x081C, {0x00}},
    {0x081D, {0x00}},
    {0x081E, {0x00}},
    {0x081F, {0x00}},
    {0x0820, {0x00}},
    {0x0821, {0x00}},
    {0x0822, {0x00}},
    {0x0823, {0x00}},
    {0x0824, {0x00}},
    {0x0825, {0x00}},
    {0x0826, {0x00}},
    {0x0827, {0x00}},
    {0x0828, {0x00}},
    {0x0829, {0x00}},
    {0x082A, {0x00}},
    {0x082B, {0x00}},
    {0x082C, {0x00}},
    {0x082D, {0x00}},
    {0x082E, {0x00}},
    {0x082F, {0x00}},
    {0x0830, {0x00}},
    {0x0831, {0x00}},
    {0x0832, {0x00}},
    {0x0833, {0x00}},
    {0x0834, {0x00}},
    {0x0835, {0x00}},
    {0x0836, {0x00}},
    {0x0837, {0x00}},
    {0x0838, {0x00}},
    {0x0839, {0x00}},
    {0x083A, {0x00}},
    {0x083B, {0x00}},
    {0x083C, {0x00}},
    {0x083D, {0x00}},
    {0x083E, {0x00}},
    {0x083F, {0x00}},
    {0x0840, {0x00}},
    {0x0841, {0x00}},
    {0x0842, {0x00}},
    {0x0843, {0x00}},
    {0x0844, {0x00}},
    {0x0845, {0x00}},
    {0x0846, {0x00}},
    {0x0847, {0x00}},
    {0x0848, {0x00}},
    {0x0849, {0x00}},
    {0x084A, {0x00}},
    {0x084B, {0x00}},
    {0x084C, {0x00}},
    {0x084D, {0x00}},
    {0x084E, {0x00}},
    {0x084F, {0x00}},
    {0x0850, {0x00}},
    {0x0851, {0x00}},
    {0x0852, {0x00}},
    {0x0853, {0x00}},
    {0x0854, {0x00}},
    {0x0855, {0x00}},
    {0x0856, {0x00}},
    {0x0857, {0x00}},
    {0x0858, {0x00}},
    {0x0859, {0x00}},
    {0x085A, {0x00}},
    {0x085B, {0x00}},
    {0x085C, {0x00}},
    {0x085D, {0x00}},
    {0x085E, {0x00}},
    {0x085F, {0x00}},
    {0x0860, {0x00}},
    {0x0861, {0x00}},
    {0x090E, {0x03}},
    {0x091C, {0x04}},
    {0x0943, {0x00}},
    {0x0949, {0x00}},
    {0x094A, {0x00}},
    {0x094E, {0x49}},
    {0x094F, {0x02}},
    {0x095E, {0x00}},
    {0x0A02, {0x00}},
    {0x0A03, {0x0F}},
    {0x0A04, {0x0F}},
    {0x0A05, {0x0F}},
    {0x0A14, {0x00}},
    {0x0A1A, {0x00}},
    {0x0A20, {0x00}},
    {0x0A26, {0x00}},
    {0x0A2C, {0x00}},
    {0x0B44, {0x0F}},
    {0x0B4A, {0x10}},
    {0x0B57, {0x03}},
    {0x0B58, {0x01}},
};

const int32_t gRegisterListSize = sizeof(gRegisterList)/sizeof(gRegisterList[0]);   ///< Number of registers to write in the registerList
/**
 * @brief These register values are written after all the registers above
 */
static ClockTreeRegister_t gPostambleList[] = {
    {0x001C, {0x01}},  //soft reset
    {0x0B24, {0xC3}},  //Post amble
    {0x0B25, {0x02}}   //Post amble
};
const int32_t gPostambleListSize = (sizeof(gPostambleList)/sizeof(gPostambleList[0])); ///< Number of registers to write in the postAmbleList

/**
 * @brief An event that occurs when transmit over i2c is complete
 *
 * @param hi2c handle to i2c bus generating event
 */ 
void SysClock_I2C_TxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(gSysClkI2CTxSem, &xHigherPriorityTaskWoken);
    DBG1_RESET();
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @brief An event that occurs when receive over i2c is complete
 * 
 * @param hi2c handle to i2c bus generating event
 */ 
void SysClock_I2C_RxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(gSysClkI2CRxSem, &xHigherPriorityTaskWoken); 
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @brief Write to the page address of the system clock
 * 
 * @param page The page value to write
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_WRITE_PAGE on failure.
 */
static CotaError_t sysClkPageWrite(uint8_t page)
{
    HAL_StatusTypeDef ret = HAL_OK;
    uint8_t sendData[2];
    
    if (page != gLastPageWritten)
    {
        sendData[0] = PAGE_REG_ADDR;
        sendData[1] = page;

        ret = HAL_I2C_Master_Transmit_IT(&SYSCLK_I2C_HANDLE, SYSCLK_ADDR_BYTE, sendData, sizeof(sendData));
        
        if (ret == HAL_OK)
        {
            ret = (xSemaphoreTake(gSysClkI2CTxSem, pdMS_TO_TICKS(I2C_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
        }
        
        gLastPageWritten = (ret == HAL_OK) ? page : INVALID_PAGE;       
    }
    return (ret == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_SYSCLK_FAILED_TO_WRITE_PAGE;
}

/**
 * @brief Write a system clock register to its address
 * 
 * @param addr      The address of the register
 * @param regVal    The value to write to the register
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG on failure.
 */
static CotaError_t sysClkWriteReg(uint16_t addr, uint8_t regVal)
{
    CotaError_t ret;
    HAL_StatusTypeDef retHal;
    uint8_t sendData[2];

    DBG2_RESET();
    ret = sysClkPageWrite(SYSCLK_PAGE_ADDR(addr));
    DBG2_SET();
    retHal = HAL_OK;
    
    if (ret == COTA_ERROR_NONE)
    {
        sendData[0] = SYSCLK_REG_ADDR(addr);
        sendData[1] = regVal;
        
        DBG3_RESET();   
        retHal = HAL_I2C_Master_Transmit_IT(&SYSCLK_I2C_HANDLE, SYSCLK_ADDR_BYTE, sendData, sizeof(sendData));  
        DBG3_SET();
        
        if (retHal == HAL_OK)
        {
            DBG4_RESET();
            retHal = (xSemaphoreTake(gSysClkI2CTxSem, pdMS_TO_TICKS(I2C_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
            DBG4_SET();
        }
    
        ret = (retHal == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG;
    }
    DBG1_SET();
    return ret;
}

/**
 * @brief Write a multibyte register value to a base address
 * @param addr The address containing the least significant byte of the regvalue
 * @param numBytes The number bytes for the register (max sizeof(uint64_t)) 
 * @param regVal  The register value to write.
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_READ_REG on failure.
 */
static CotaError_t sysClkWriteLargeRegVal(uint16_t addr, uint8_t numBytes, uint64_t regVal)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint8_t *data = (uint8_t*)&regVal;
    uint8_t ii;
    
    if (numBytes <= sizeof(uint64_t))
    {
        for (ii = 0; (ii < numBytes) && (ret == COTA_ERROR_NONE); ii++)
        {
            ret = sysClkWriteReg(addr + ii, data[ii]);
        }      
    }
    else
    {
        ret = COTA_ERROR_BAD_PARAMETER;
    }
        
    return ret;
        
}

/**
 * @brief Read a multibyte register value to a base address
 * @param addr The address containing the least significant byte of the regvalue
 * @param numBytes The number bytes for the register (max sizeof(uint64_t)) 
 * @param regVal  Pointer to uint64_t to receive the data
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_READ_REG on failure.
 */
static CotaError_t sysClkReadLargeRegVal(uint16_t addr, uint8_t numBytes, uint64_t* regVal)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint8_t *data = (uint8_t*)regVal;
    uint8_t ii;
     
    *regVal = 0;
    
    if (numBytes <= sizeof(uint64_t))
    {
        for (ii = 0; (ii < numBytes) && (ret == COTA_ERROR_NONE); ii++)
        {
            ret = SysClkReadReg(addr + ii, &data[ii]);
        }      
    }
    else
    {
        ret = COTA_ERROR_BAD_PARAMETER;
    }
        
    return ret;
}

/**
 * @brief Read a system clock register
 *        A clock register is read by first writing the page, then writing the 
 *        register location in the page, then reading the value.
 *        
 * @param addr      The address of the register
 * @param regVal    The value to read from the register
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_READ_REG on failure.
 */
CotaError_t SysClkReadReg(uint16_t addr, uint8_t* regVal)
{
    CotaError_t ret = sysClkPageWrite(SYSCLK_PAGE_ADDR(addr));
    HAL_StatusTypeDef retHal = HAL_OK;
    uint8_t regAddr = SYSCLK_REG_ADDR(addr);
    
    if (ret == COTA_ERROR_NONE)
    {                  
        retHal = HAL_I2C_Master_Transmit_IT(&SYSCLK_I2C_HANDLE, SYSCLK_ADDR_BYTE, &regAddr, sizeof(uint8_t));  
      
        if (retHal == HAL_OK)
        {
            retHal = (xSemaphoreTake(gSysClkI2CTxSem, pdMS_TO_TICKS(I2C_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
        }
        
        retHal = (retHal == HAL_OK) ? HAL_I2C_Master_Receive_IT(&SYSCLK_I2C_HANDLE, SYSCLK_ADDR_BYTE, regVal, sizeof(uint8_t)) : retHal;  
        
        if (retHal == HAL_OK)
        {
            retHal = (xSemaphoreTake(gSysClkI2CRxSem, pdMS_TO_TICKS(I2C_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
        }
                    
        ret = (retHal == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_SYSCLK_FAILED_TO_READ_REG;

    }
    return POST_COTA_ERROR(ret);
}

/**
 * @brief Read a system clock register then modifies and writes it back
 *  
 * @param addr      The address of the register
 * @param regVal    The new value of the field to modify in the register
 * @param size      A size indicating the region to modify
 * @param offset    The offset of region in the register to modify.
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_READ_REG on failure.
 */
CotaError_t sysClkModifyReg(uint16_t addr, uint8_t regVal, uint8_t size, uint8_t offset)
{
    uint8_t reg;
    uint8_t mask = (uint8_t)((1UL << (size)) - 1); 
    CotaError_t ret = SysClkReadReg(addr, &reg);
  
    if (ret == COTA_ERROR_NONE)
    {
        mask <<=offset;
        reg = reg & (~mask);
        reg |= ((regVal << offset) & mask);
      
        ret = (ret == COTA_ERROR_NONE) ? sysClkWriteReg(addr, reg) : ret;      
    }
    
    return ret;  
}

#define SYSCLK_TEST
/**
 * @brief Writes a list of registers to the system clock.
 * 
 * @param regList       An array of registers to write to.
 * @param numRegs       The number of registers to write to.
 * @param index         The index in the regs field to use.
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG on failure.
 */
static CotaError_t sysClkWriteRegList(ClockTreeRegister_t* regList, uint32_t numRegs, uint8_t index)
{
    uint32_t ii = 0;
    CotaError_t ret = COTA_ERROR_NONE;

    uint8_t regVal = 0;
#ifdef SYSCLK_TEST
    uint8_t regRead;
#endif
    
    for (ii = 0; (ii < numRegs) && (ret == COTA_ERROR_NONE); ii++)
    {  
        regVal = regList[ii].regs[index];
 
        ret = sysClkWriteReg(regList[ii].addr, regVal);
        
        if (ret != COTA_ERROR_NONE)
        {
           POST_COTA_ERROR(ret);         
        }  
         //vTaskDelay( pdMS_TO_TICKS(100));
#ifdef SYSCLK_TEST    
        // When testing, we read back what we wrote to see if the clock got the value
        if (ret != COTA_ERROR_NONE)
        {
           POST_COTA_ERROR(ret);         
        }
        else if (regList[ii].addr != 0x001C)
        {           
            ret = (ret == COTA_ERROR_NONE) ? SysClkReadReg(regList[ii].addr, &regRead) : ret;
                     
            if ((ret == COTA_ERROR_NONE) && (regRead != regVal))
            {
                ret = COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG;
                POST_COTA_ERROR(ret);   
            }
            else if (ret != COTA_ERROR_NONE)
            {
                POST_COTA_ERROR(ret);         
            }            
        }
#endif
    }  
    
    return ret;
}

/**
 * @brief Applies the system clock amplitude register to all clocks
 * 
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t sysClkApplyAmplitude(void)
{
    uint8_t amp;
    uint8_t ii;
    CotaError_t ret = COTA_ERROR_NONE;
    
    CfgGetParam(CFG_SYSCLK_AMPLITUDE, (void*)&amp, sizeof(amp));
  
    for (ii = 0; (ii < gSysClkAmpCmRegsSize) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = sysClkModifyReg(gSysClkAmpCmRegs[ii], amp, SYS_CLK_AMPLITUDE_SIZE, SYS_CLK_AMPLITUDE_OFFSET);      
    }
    
    return ret;
}
 
/**
 * @brief Applies the system clock common mode register to all clocks
 * 
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t sysClkApplyCommonMode(void)
{
    uint8_t cm;
    uint8_t ii;
    CotaError_t ret = COTA_ERROR_NONE;
    
    CfgGetParam(CFG_SYSCLK_COMMON_MODE, (void*)&cm, sizeof(cm));
  
    for (ii = 0; (ii < gSysClkAmpCmRegsSize) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = sysClkModifyReg(gSysClkAmpCmRegs[ii], cm, SYS_CLK_CM_MODE_SIZE, SYS_CLK_CM_MODE_OFFSET);      
    }
    
    return ret;
}

/**
 * @brief Applies the system clock format register to all clocks
 * 
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t sysClkApplyFormat(void)
{
    uint8_t format;
    uint8_t ii;
    CotaError_t ret = COTA_ERROR_NONE;
    
    CfgGetParam(CFG_SYSCLK_FORMAT, (void*)&format, sizeof(format));
  
    for (ii = 0; (ii < gSysClkFormatRegsSize) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = sysClkModifyReg(gSysClkFormatRegs[ii], format, SYS_CLK_FORMAT_SIZE, SYS_CLK_FORMAT_OFFSET);      
    }
    
    return ret;
}

/**
 * @brief Applies the system clock inversion values to all AMB clocks
 * 
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t sysClkApplyAMBInversions(void)
{
    uint8_t inversions;
    uint8_t ii;
    uint8_t mask = (uint8_t)((1UL << SYS_CLK_INV_SIZE) - 1);
    CotaError_t ret = COTA_ERROR_NONE;
    
    CfgGetParam(CFG_SYSCLK_AMB_INVERSION, (void*)&inversions, sizeof(inversions));
  
    for (ii = 0; (ii < gSysClkAmbInversionRegsSize) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = sysClkModifyReg(gSysClkAmbInversionRegs[ii], inversions & mask, SYS_CLK_INV_SIZE, SYS_CLK_INV_OFFSET);      
        inversions >>= SYS_CLK_INV_SIZE;
    }
    
    return ret;  
}


/** 
 * @brief This function is used when the transmitter is a slave. It must use IN1 as input which is connected to
 *        the clock of the master transmitter
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t changeInputClockToIn1(void)
{
    CotaError_t ret = sysClkModifyReg(LOSIN_INTR_MSK_REG, LOSIN_INTR_MSK_IN1_VAL, LOSIN_INTR_MSK_IN1_SIZE, LOSIN_INTR_MSK_IN1_OFFSET);  
    
    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(IN_SEL_REGCTRL_REG, IN_SEL_REGCTRL_VAL, IN_SEL_REGCTRL_SIZE, IN_SEL_REGCTRL_OFFSET) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(LOS_EN_REG, LOS_EN_REG_IN1_VAL, LOS_EN_REG_IN1_SIZE, LOS_EN_REG_IN1_OFFSET) : ret;
   
    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(LOS_EN_REG, LOS_EN_REG_XAXB_VAL, LOS_EN_REG_XAXB_SIZE, LOS_EN_REG_XAXB_OFFSET) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(LOS_VAL_TIME_REG, LOS_VAL_TIME_REG_IN1_VAL, LOS_VAL_TIME_REG_IN1_SIZE, LOS_VAL_TIME_REG_IN1_OFFSET) : ret;    
     
    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteLargeRegVal(LOS1_TRG_THRESH_REG, LOS1_TRG_THRESH_BYTE, LOS1_TRG_THRESH_VAL) : ret;  

    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteLargeRegVal(LOS1_CLR_THRESH_REG, LOS1_CLR_THRESH_BYTE, LOS1_CLR_THRESH_VAL) : ret;  
    
    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(LOS1_DIV_SEL_REG, LOS1_DIV_SEL_VAL, LOS1_DIV_SEL_SIZE, LOS1_DIV_SEL_OFFSET) : ret;    
    
    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteLargeRegVal(P1_DIV_REG, P1_DIV_BYTES, P1_DIV_VAL) : ret;

    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteLargeRegVal(P1_SET_REG, P1_SET_BYTES, P1_SET_VALUE) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(XAXB_CONF_REG, XAXB_CONF_EXTCLK_EN_VAL, XAXB_CONF_EXTCLK_EN_SIZE, XAXB_CONF_EXTCLK_EN_OFFSET) : ret;     

    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(CLK_INPUT_CTRL_REG, CLK_INPUT_IN1_EN_VAL, CLK_INPUT_IN1_EN_SIZE, CLK_INPUT_IN1_EN_OFFSET) : ret;     

    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(INX_TO_PFD_EN, IN1_TO_PFD_EN_VAL, IN1_TO_PFD_EN_SIZE, IN1_TO_PFD_EN_OFFSET) : ret;     

    return ret;
}

/** 
 * @brief Enable the N4 divider which is needed for the Master clock and the TAM clock
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t enableN4Divider(void)
{
    CotaError_t ret = sysClkWriteLargeRegVal(N4_NUM_REG, NX_NUM_BYTES, N4_NUM_VAL);    
    
    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteLargeRegVal(N4_DEN_REG, NX_DIV_DEN_BYTES, N4_DEN_VAL) : ret;

    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteLargeRegVal(N4_DELAY_REG, NX_DELAY_REG_SIZE, N4_DEN_VAL) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(NCLK_TO_OUT_EN_REG, NCLK_TO_OUT_EN_N4_VAL, NCLK_TO_OUT_EN_N4_SIZE, NCLK_TO_OUT_EN_N4_OFFSET) : ret; 
    
    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(N_PDNB_REG, N4_PDNB_REG_VAL, N4_PDNB_REG_SIZE, N4_PDNB_REG_OFFSET) : ret; 

    ret = (ret == COTA_ERROR_NONE) ? sysClkModifyReg(N_CLK_DIS_REG, N4_CLK_DIS_VAL, N4_CLK_DIS_SIZE, N4_CLK_DIS_OFFSET) : ret;
    
    return ret;
}

/** 
 * @brief Enable master clock which output to the slave transmitter in dual transmitter mode
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t enableMasterClock(void)
{
    CotaError_t ret = sysClkWriteReg(OUT4_CONFIG_REG, OUT4_CONFIG_VAL);
    
    ret = (ret == COTA_ERROR_NONE) ?  sysClkWriteReg(OUT4_FORMAT_REG, OUT4_FORMAT_VAL) : ret; 

    ret = (ret == COTA_ERROR_NONE) ?  sysClkWriteReg(OUT4_AMP_CM_REG, OUT4_AMP_CM_VAL) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ?  sysClkWriteReg(OUT4_MUX_INV_REG, OUT4_MUX_INV_VAL) : ret; 

    return ret;
}

/** 
 * @brief Enable the TAM clock
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t enableTamClock(void)
{
    CotaError_t ret = sysClkWriteReg(OUT5_CONFIG_REG, OUT5_CONFIG_VAL);
    
    ret = (ret == COTA_ERROR_NONE) ?  sysClkWriteReg(OUT5_FORMAT_REG, OUT5_FORMAT_VAL) : ret; 

    ret = (ret == COTA_ERROR_NONE) ?  sysClkWriteReg(OUT5_AMP_CM_REG, OUT5_AMP_CM_VAL) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ?  sysClkWriteReg(OUT5_MUX_INV_REG, OUT5_MUX_INV_VAL) : ret; 

    return ret;
}


/**
 * @brief Initialize the system clock by writing many registers over I2c
 * 
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
CotaError_t SystemClockInit(void)
{
    CotaError_t ret = COTA_ERROR_NONE;
    SysClkConfig_t sysClkCfg = SYS_CLK_MAX_CONFIG;
        
    DBG1_SET();
    DBG2_SET();
    DBG3_SET();
    DBG4_SET();

    CfgGetParam(CFG_SYSTEM_CLOCK, &sysClkCfg, sizeof(SysClkConfig_t));
    
    if (sysClkCfg >= SYS_CLK_MAX_CONFIG)
    {
        PostPrintf("System clock configuration invalid\r\n.");
        ret = COTA_ERROR_BAD_PARAMETER;
    }

    //Let's reset the clock chip
    HAL_GPIO_WritePin(SYSCLK_RESET_GPIO_Port, SYSCLK_RESET_Pin, GPIO_PIN_RESET); 
    
    vTaskDelay( pdMS_TO_TICKS(100));
    HAL_GPIO_WritePin(SYSCLK_RESET_GPIO_Port, SYSCLK_RESET_Pin, GPIO_PIN_SET);
    vTaskDelay( pdMS_TO_TICKS(100));
    
    //Write the preamble, which makes it safe for us to write to the register
    ret = sysClkWriteRegList(gPreambleList, gPreambleListSize, VARIANT_1_CLOCK);
    vTaskDelay( pdMS_TO_TICKS(SYS_CLK_CAL_DELAY_MS));

    //Write the default register list.
    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteRegList(gRegisterList, gRegisterListSize, VARIANT_1_CLOCK) : ret;
    
    //In slave mode, the transmitter needs a clock externally from the master transmitter, so we must change
    //the input clock from XAXB to IN1
    if ((sysClkCfg == SYS_CLK_CONFIG_AMB_SLAVE) || 
        (sysClkCfg == SYS_CLK_CONFIG_AMB_SLAVE_TAM) )
    {      
        ret = (ret == COTA_ERROR_NONE) ? changeInputClockToIn1() : ret;
    }
    
    //If we use TAM or the transmitter is a master to another slave transmitter,
    //we must the enable the N4 divider so that we can output the 50 Mhz TAM or master clocks
    if ((sysClkCfg == SYS_CLK_CONFIG_AMB_MASTER) || 
        (sysClkCfg == SYS_CLK_CONFIG_AMB_TAM) || 
        (sysClkCfg == SYS_CLK_CONFIG_AMB_MASTER_TAM) ||
        (sysClkCfg == SYS_CLK_CONFIG_AMB_SLAVE_TAM) )
    {      
        ret = (ret == COTA_ERROR_NONE) ? enableN4Divider() : ret;
    }    
   
    //If this transmitter is master, we must enable the master clock
    if ((sysClkCfg == SYS_CLK_CONFIG_AMB_MASTER) || 
        (sysClkCfg == SYS_CLK_CONFIG_AMB_MASTER_TAM) )
    {    
        ret = (ret == COTA_ERROR_NONE) ? enableMasterClock() : ret;    
    }
    
    //If this transmitter uses TAM, we must turn on the clock for TAM.
    if ((sysClkCfg == SYS_CLK_CONFIG_AMB_TAM) || 
        (sysClkCfg == SYS_CLK_CONFIG_AMB_MASTER_TAM) ||
        (sysClkCfg == SYS_CLK_CONFIG_AMB_SLAVE_TAM) )
    {      
        ret = (ret == COTA_ERROR_NONE) ? enableTamClock() : ret;
    }          
      
    //We now apply the amplitude, common mode, and format to all output clocks
    ret = (ret == COTA_ERROR_NONE) ? sysClkApplyAmplitude() : ret;
    ret = (ret == COTA_ERROR_NONE) ? sysClkApplyCommonMode() : ret;
    ret = (ret == COTA_ERROR_NONE) ? sysClkApplyFormat() : ret;
    //We apply the Inversion settings the to AMB clocks
    ret = (ret == COTA_ERROR_NONE) ? sysClkApplyAMBInversions() : ret;
    
    //The post amble enable the clocks so we are ready to go.
    vTaskDelay( pdMS_TO_TICKS(SYS_CLK_CAL_DELAY_MS));
    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteRegList(gPostambleList, gPostambleListSize, VARIANT_1_CLOCK) : ret;
    
#if SYSCLK_FREQ_TEST    
    ret = (ret == COTA_ERROR_NONE) ? ChangeFreq(2460) : ret;
    ret = (ret == COTA_ERROR_NONE) ? ChangeFreq(2450) : ret;
    ret = (ret == COTA_ERROR_NONE) ? ChangeFreq(2440) : ret;
#endif

    // Indicates whether we disable the clock between charging cycles
    CfgGetParam(CFG_SYSCLK_IDLE_DISABLE, &gIdleClock, sizeof(gIdleClock));
       
    return ret;
}

/**
 * @brief Creates the semaphores needed to communicate with the system clock
 */
void InitSysClkDriver(void)
{       
  gSysClkI2CTxSem = xSemaphoreCreateBinaryStatic(&gSysClkI2CTxSemBuff);
  while (gSysClkI2CTxSem == NULL);
  
  gSysClkI2CRxSem = xSemaphoreCreateBinaryStatic(&gSysClkI2CRxSemBuff);
  while (gSysClkI2CRxSem == NULL);
}

/**
 * @brief Read M divider numerator registers
 * @param mNum 48-bit M divider numerator register value
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t readMNum(uint64_t *mNum)
{
    CotaError_t ret = COTA_ERROR_NONE;
    
    *mNum = 0;
    
    ret = sysClkReadLargeRegVal(M_DIV_NUMERATOR_REG, M_DIV_NUMERATOR_BYTES, mNum);
    
    return ret;
}

/**
 * @brief Read M divider denominator registers
 * @param mDen 32-bit M divider denominator register value
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t readMDen(uint32_t *mDen)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint64_t val = 0;
    
    ret = sysClkReadLargeRegVal(M_DIV_DENOMENATOR_REG, M_DIV_DENOMENATOR_BYTES, &val);
    
    *mDen = (uint32_t)val;
      
    return ret;
}

/**
 * @brief Read the R0 divider register
 * @param rReg 24-bit R0 divider register value
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t readRReg(uint32_t *rReg)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint64_t val = 0;
    
    ret = sysClkReadLargeRegVal(R0_DIV_REG, RX_DIV_BYTES, &val);
    
    *rReg = (uint32_t)val;
    
    return ret;
}

/**
 * @brief Read N0 divider denominator register
 * @param nDen 32-bit N0 divider denominator register value
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t readNDen(uint32_t *nDen)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint64_t val = 0;
    
    ret = sysClkReadLargeRegVal(N0_DIV_DEN_REG, NX_DIV_DEN_BYTES, &val);
      
    *nDen = (uint32_t)val;
    
    return ret;
}

/**
 * @brief Write N divider numerator registers.
 * @param n N divider to write
 * @param nNum N divider numerator value
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t writeNNum(uint8_t n, uint64_t nNum)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint16_t regAddr[MAX_NUM_NDIV] =   {N0_NUM_REG, N1_NUM_REG, N2_NUM_REG, N3_NUM_REG, N4_NUM_REG};
    
    if (n >= MAX_NUM_NDIV)
    {
        ret = COTA_ERROR_BAD_PARAMETER;
    }
    else
    {
        ret = sysClkWriteLargeRegVal(regAddr[n], NX_NUM_BYTES, nNum);
    }
    
    return ret;
}

/**
 * @brief Write N delay register.
 * @param n N delay to write
 * @param nDelay Actual delay value to be written to delay register
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t writeNDelay(uint8_t n, uint16_t nDelay)
{
    CotaError_t ret = COTA_ERROR_NONE;
    uint16_t regAddr[MAX_NUM_NDIV] = {N0_DELAY_REG, N1_DELAY_REG, N2_DELAY_REG, N3_DELAY_REG, N4_DELAY_REG};
    
    if (n >= MAX_NUM_NDIV)
    {
        ret = COTA_ERROR_BAD_PARAMETER;
    }
    else
    {
        ret = sysClkWriteLargeRegVal(regAddr[n], NX_DELAY_REG_SIZE, nDelay);
    }
    
    return ret;
}

/**
 * @brief Write the Nth divider update register for the new frequency to take effect
 * @param n N divider to write
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
static CotaError_t writeNUpdate(uint8_t n)
{
    CotaError_t ret;
    uint16_t regAddr[MAX_NUM_NDIV] = {0x30C, 0x317, 0x322, 0x32D, 0x338};
    uint8_t data = 1;
    
    if (n >= MAX_NUM_NDIV)
    {
        ret = COTA_ERROR_BAD_PARAMETER;
    }
    else
    {
        ret = sysClkWriteReg(regAddr[n], data);
    }
    return ret;
}


/**
 *  @brief Change the clock frequency thru clock register configurations
 *  @param freq The desired frequency in MHZ
 *  @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
CotaError_t ChangeFreq(uint32_t freq)
{
    double   refFreq = 0;
    double   nSkew;
    uint64_t freqVco = 0;
    uint64_t nNum;
    uint32_t nDen;
    uint64_t mNum;
    uint32_t mDen;
    uint32_t rReg;
    uint32_t rVal;
    CotaError_t ret;
    uint8_t ii;
    uint16_t nDelay;
        
    ret = readMNum(&mNum);
           
    ret = (ret == COTA_ERROR_NONE) ? readMDen(&mDen) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ? readNDen(&nDen) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ? readRReg(&rReg) : ret;   
    
    if (ret == COTA_ERROR_NONE)
    {
        refFreq = ((double)freq) / FREQ_RATIO_V1;
        
        freqVco = (mNum * IN_FREQ_MHZ/ mDen);
        
        rVal = (rReg + 1) * 2;  //R_VALUE = (R_REG + 1) x 2
        
        //Calculate the new N divider Numerator value
        nNum = (uint64_t)((nDen * freqVco) / (refFreq * rVal));
        
        //The AMB's use N-divders 0-3, so must update them all
        for (ii = 0; (ii < AMB_NUM_N_DIV) && (ret == COTA_ERROR_NONE); ii++)
        {
            ret = writeNNum(ii, nNum);            
        }

        //Here we offset each clock off by a quarter cycle to minimize radiation.
        for (ii = 0; (ii < AMB_NUM_N_DIV) && (ret == COTA_ERROR_NONE); ii++)
        {
            nSkew  = ii*(1 / refFreq) / 4;               //clock skew is 1/4 cycle in us
            nDelay = (uint16_t)(nSkew * 256 * freqVco);  //The formula is coming from the clock tree document
            ret = writeNDelay(ii, nDelay);
        }
      
        //The AMB's use N-divders 0-3, so must update them all
        for (ii = 0; (ii < AMB_NUM_N_DIV) && (ret == COTA_ERROR_NONE); ii++)
        {
            ret = writeNUpdate(ii);            
        }  
    }
    
    if (ret == COTA_ERROR_NONE)
    {
        PostPrintf("Tx Frequency set to %d Mhz.\r\n", freq);
    }
                   
    return ret;
}

/**
 *  @brief Change the clock frequency using the comm channel to choose the frequency
 *  @param commChannel The current comm channel
 *  @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
CotaError_t ChangeFreqWithCommChan(uint8_t commChannel)
{
    CotaError_t ret = COTA_ERROR_NONE;
    MapComToFreqInMHz* mapPtr  = NULL;
    uint8_t ii;
    
    for (ii = 0; (ii < gComMapSize) && (mapPtr == NULL); ii++)
    {
        if (gComToFreq[ii].commChannel == commChannel)
        {
            mapPtr = &gComToFreq[ii];
        }
    }
    
    if (mapPtr)
    {
        ret = ChangeFreq(mapPtr->freqInMhz);
    }
    else
    {
        ret = COTA_ERROR_BAD_PARAMETER;
    }
    
    return ret;
}

/**
   @brief Determines if a selected communication channel is valid.
   @param commChannel - The commChannel that we are checking
   @return true if commChannel is valid, false if otherwise. 
 */
bool IsValidCommCh(uint8_t commChannel)
{
    uint8_t ii;
    bool    ret = false; ;
    
    for (ii = 0; ii < gComMapSize; ii++)
    {
        if (gComToFreq[ii].commChannel == commChannel)
        {
            ret = true;
            break;
        }
    }
    return ret;
}

/**
 *  @brief Dumps the system clock registers
 *  @param msg A working buffer passed from the caller so this function can post to the PI
 */
void SysClkDump(CtrlResp_t* msg)
{
    uint16_t ii;
    uint8_t val;

    int len = sizeof(msg->sysClkDump.addr);
    len += sizeof(msg->sysClkDump.val);
    len += sizeof(msg->id);
    len += sizeof(msg->err);

    msg->id = CTRL_SYS_CLK_DUMP_DATA;
    msg->err = COTA_ERROR_NONE;
    
    PostPrintf(" Addr  val\r\n");
    for (ii = 0; ii <= MAX_SYSCLK_ADDR; ii++)
    {
        msg->err = SysClkReadReg(ii, &val);
        msg->sysClkDump.addr = ii;
        msg->sysClkDump.val = val;
        RpiSendMessage((uint8_t*) msg, len);        
        
        if (msg->err == COTA_ERROR_NONE)
        {
            PostPrintf("0x%04x 0x%02x\r\n", ii, val);
        }
        else
        {
            PostPrintf("0x%04x Error\r\n", ii);
        }
        vTaskDelay(pdMS_TO_TICKS(10));
    }
}

/**
 * @brief Disables i2c communication to the clock so we can use
 *        an external programmer.
 */
void SysClkI2CDisable(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    HAL_I2C_DeInit(&SYSCLK_I2C_HANDLE);
    GPIO_InitStruct.Pin = CLKD_SCK_Pin|CLKD_SDA_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(CLKD_SCK_GPIO_Port, &GPIO_InitStruct);
}

/**
 * @brief Write a system clock register to its address but we call the preamble before
 *        the write so it's safe.   The postamble is called after the write.
 * 
 * @param addr      The address of the register
 * @param regVal    The value to write to the register
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG on failure.
 */
CotaError_t SafeSysClkWriteReg(uint16_t addr, uint8_t regVal)
{
    CotaError_t ret = sysClkWriteRegList(gPreambleList, gPreambleListSize, VARIANT_1_CLOCK);
    vTaskDelay( pdMS_TO_TICKS(SYS_CLK_CAL_DELAY_MS));   
    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteReg(addr, regVal) : ret;
    vTaskDelay( pdMS_TO_TICKS(SYS_CLK_CAL_DELAY_MS)); 
    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteRegList(gPostambleList, gPostambleListSize, VARIANT_1_CLOCK) : ret;

    return ret;
}

/**
 *  @brief Set or Reset the Ethernet clock based on a supplied param value
 *  @param  setReset true set GPIO, false reset GPIO
 *  @return COTA_ERROR_NONE
 */
CotaError_t SetResetEthernetClock(bool setReset)
{
    CotaError_t err = COTA_ERROR_NONE;
    PostPrintf("Ethernet Clock %s.\r\n", setReset ? "on" : "off");

    // Set the GPIO pin state based on the incoming enable value.
    GPIO_PinState val = ((setReset) ?  GPIO_PIN_SET : GPIO_PIN_RESET);

    // PG3P3 controls the Ethernet clock enable line
    // (line ETH_OSC_ENABLE on the Orion CCB schematic).
    HAL_GPIO_WritePin(PG3P3_GPIO_Port, PG3P3_Pin, val);

   return err;
}

/**
 *  @brief  Enable or disable all the system clock outputs
 *  @param  enable true enables all system clock outputs and false disable the outputs
 *  @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
CotaError_t EnableSystemClock(bool enable)
{
    static bool firstTimeEnabled = false;
    CotaError_t err = COTA_ERROR_NONE;
    
    if (!firstTimeEnabled ||
        gSysClkIsEnabled != enable)
    {
        gSysClkIsEnabled = enable;
        firstTimeEnabled = true;
        LogInfo("System Clock %s.\r\n", enable ? "on" : "off");
        //SYS_CLK_OUTALL_ENABLE is the value that causes the clocks to be on.  SYS_CLK_OUTALL_DISABLE is the value that disables the clocks
        err =  sysClkWriteReg(SYS_CLK_OUTALL_DISABLE_LOW, enable ? SYS_CLK_OUTALL_ENABLE : SYS_CLK_OUTALL_DISABLE);
    }
    
    return err;
}

/**
 * @brief 
 */
void DisableSystemClockIfClockIdleIsSet(void)
{
    if (gIdleClock)
    {
        EnableSystemClock(false);
    }
}

/**
 * @brief  Sometimes frequency can be linked to the com channel.  If it is, the function sets the frequency
 * @return On success, COTA_ERROR_NONE; otherwise an error code of type #CotaError_t
 */
CotaError_t UpdateTransmitFrequency(void)
{
    uint16_t txFreq = 0;
    uint8_t commChannel = 0;
    CotaError_t ret = CfgGetParam(CFG_COMM_CHANNEL, &commChannel, sizeof(uint8_t));
    
    ret = (ret == COTA_ERROR_NONE) ? CfgGetParam(CFG_TX_FREQUENCY_MHZ, &txFreq, sizeof(uint16_t)): ret;
    
    if (txFreq == 0)
    {
        ret = (ret == COTA_ERROR_NONE) ? ChangeFreqWithCommChan(commChannel) : ret;
    }
    else
    {
        ret = (ret == COTA_ERROR_NONE) ? ChangeFreq(txFreq) : ret;
    }
    
    return ret;
}
