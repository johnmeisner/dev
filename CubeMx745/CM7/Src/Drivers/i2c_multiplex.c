/****************************************************************************//**
* @file      i2c_multiplex.c
*
* @brief     Manages i2c communication through the i2c multiplexers
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/


#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "string.h"
#include "main.h"
#include "sysclk_driver.h"
#include "eeprom_driver.h"
#include "orion_config.h"
#include "i2c_multiplex.h"
#include "debug_off.h"


extern I2C_HandleTypeDef EEPROM_I2C_HANDLE;                             ///< The handle to the EEPROM I2C driver
extern I2C_HandleTypeDef SYSCLK_I2C_HANDLE;                             ///< The handle to the System Clock I2C driver
extern I2C_HandleTypeDef MULIPLEXER_I2C_HANDLE;                         ///< The handle to the I2C Multiplexer driver


#define ADDR_TO_BYTE(addr)          (addr << 1)                         ///< Moves an I2C address to the top seven bits in the byte
#define I2C_MULTI_COM_ADDR          0x70                                ///< The upper five bits of the address are set for the multiplexers.  The lower two bit are controlled by external pins
#define I2C_MULTI_ADDR(addr)        (I2C_MULTI_COM_ADDR | (addr & 0x3)) ///< Forms a complete multiplexer i2c address by adding in the lower two bits
#define MAX_NUM_I2C_MULTIPLEXERS    3                                   ///< The maximum number of i2c multiplexers
#define CHANNELS_PER_MULTIPLEXER    4                                   ///< The number of channels per i2c multiplexer  
#define INVALID_MULTI_CHANNEL       0xff                                ///< An invalid multiplexer channel selection
#define CHANNEL_TO_REG(chan)        (1 << chan)                         ///< converts a channel number to a register value that is sent to the multiplexer register


static SemaphoreHandle_t gI2CMultiplexTransSem;                         ///< Semaphore signaling a complete i2c transaction
static StaticSemaphore_t gI2CMultiplexTransSemBuff;                     ///< The buffer holding the information for the #gI2CMultiplexTransSem semaphore

static SemaphoreHandle_t gI2CMultiMutex;                                ///< Semaphore used to make this driver thread safe
static StaticSemaphore_t gI2CMultiMutexBuff;                            ///< The buffer holding the information for the #gI2CMultiMutexBuff semaphore

static uint8_t gCurrentChannels[MAX_NUM_I2C_MULTIPLEXERS] = {INVALID_MULTI_CHANNEL, INVALID_MULTI_CHANNEL, INVALID_MULTI_CHANNEL};  ///< Holds the current selected channel for each i2c multiplexer

/**
 * @brief Checks the validity of data in a structure of type #I2CMultiplexerInfo_t
 * 
 * @param multiInfo a structure containing the information the multiplexers need to interact with the device.
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
static CotaError_t checkMultiInfo(I2CMultiplexerInfo_t* multiInfo)
{
    CotaError_t ret = COTA_ERROR_NONE;
    
    if (multiInfo->multiNum >= MAX_NUM_I2C_MULTIPLEXERS)
    { 
        ret = COTA_ERROR_I2C_INVALID_MULTIPLEXER;
    }
    else if (multiInfo->chan >= CHANNELS_PER_MULTIPLEXER)
    {
        ret = COTA_ERROR_I2C_INVLAID_MULTI_CHANNEL;
    }      
  
    return ret;
}

/**
 * @brief Initializes the #I2CMultiplexerInfo_t structure so it can be used to read or write to a device on the i2c multiplexers
 * 
 * @param multiInfo an uninitialized structure that will contain the infromation the multiplexers need to interact with the device.
 * @param multiNum  The number of the mulitiplexer connected to the device
 * @param multiChan The channel of the multiplexer the device is attached to.
 * @param devAddr   The i2c address of the device
 *  
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
CotaError_t InitI2CMultiplexer(I2CMultiplexerInfo_t* multiInfo, uint8_t multiNum, uint8_t multiChan, uint8_t devAddr)
{
    CotaError_t ret = COTA_ERROR_NONE;

    multiInfo->multiNum = multiNum;
    multiInfo->chan = multiChan;
    multiInfo->devAddr = devAddr;      

    ret = checkMultiInfo(multiInfo);
    
    return POST_COTA_ERROR(ret);
}

/**
 * @brief Selects the channel the multiplexer connects to by writing to its register.
 *        We select a channel by writing a bit corresponding to the desired channel to the register of the multiplexer
 *        B7   B6   B5   B4   B3    B2    B1    B0
 *        INT3 INT2 INT1 INT0 Chan3 Chan2 Chan1 Chan0
 *
 * @param multiInfo a structure containing the information the multiplexers need to interact with the device.
 * @param timeout   A time in milliseconds to wait for the I2C transaction to complete
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
static CotaError_t selectMultiChannel(I2CMultiplexerInfo_t* multiInfo, uint32_t timeout)
{
    CotaError_t ret = COTA_ERROR_NONE;
    HAL_StatusTypeDef retHal = HAL_OK;
    uint8_t multiAddr = I2C_MULTI_ADDR(multiInfo->multiNum);
    uint8_t chanByte  = CHANNEL_TO_REG(multiInfo->chan);
#ifdef MULTIPLEXER_READBACK    
    uint8_t chanRead = 0;
#endif
    
    if (multiInfo->chan != gCurrentChannels[multiInfo->multiNum])
    {
        retHal = HAL_I2C_Master_Transmit_IT(&MULIPLEXER_I2C_HANDLE, ADDR_TO_BYTE(multiAddr), &chanByte, sizeof(uint8_t));
      
        if (retHal == HAL_OK)
        {
            retHal = (xSemaphoreTake(gI2CMultiplexTransSem, pdMS_TO_TICKS(timeout)) == pdPASS) ? HAL_OK : HAL_ERROR;
        }
        
        if (retHal == HAL_OK)
        {
            gCurrentChannels[multiInfo->multiNum] = multiInfo->chan;
        }
        else
        {
            gCurrentChannels[multiInfo->multiNum] = INVALID_MULTI_CHANNEL;
            ret = COTA_ERROR_I2C_FAILED_TO_WRITE_MULTI_CHAN;
        }
            
#ifdef MULTIPLEXER_READBACK        
        retHal = HAL_I2C_Master_Receive_IT(&MULIPLEXER_I2C_HANDLE, ADDR_TO_BYTE(multiAddr), &chanRead, sizeof(uint8_t));  
        
        if (retHal == HAL_OK)
        {
            retHal = (xSemaphoreTake(gI2CMultiplexTransSem, pdMS_TO_TICKS(timeout)) == pdPASS) ? HAL_OK : HAL_ERROR;
        }
#endif
        
    }
  
    return ret;
}

/**
 * @brief An event that occurs when transmit over i2c is complete
 *        Since there is only one "complete" callback for all i2c Tx 
 *        transactions, this ISR has to route its occurrence to
 *        other i2c buses, such as the one going to the clock.
 */ 
void HAL_I2C_MasterTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    if (hi2c == (&EEPROM_I2C_HANDLE))
    {
        //EepromTxDoneCallback(hi2c);
    }
    
    if (hi2c == (&SYSCLK_I2C_HANDLE))
    {
        SysClock_I2C_TxCpltCallback(hi2c);
    }
    
    if (hi2c == (&MULIPLEXER_I2C_HANDLE))
    {
        xSemaphoreGiveFromISR(gI2CMultiplexTransSem, &xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}

/**
 * @brief An event that occurs when receive over i2c is complete
 *        Since there is only one "complete" callback for all i2c Rx 
 *        transactions, this ISR has to route its occurrence to
 *        other i2c buses, such as the one going to the clock.
 */ 
void HAL_I2C_MasterRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
      
    if (hi2c == (&EEPROM_I2C_HANDLE))
    {
        //EepromRxDoneCallback(hi2c);
    }

    if (hi2c == (&SYSCLK_I2C_HANDLE))
    {
        SysClock_I2C_RxCpltCallback(hi2c);
    }
    
    if (hi2c == (&MULIPLEXER_I2C_HANDLE))
    {
        xSemaphoreGiveFromISR(gI2CMultiplexTransSem, &xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
}

/**
 * @brief Writes data to a device attached to an i2c multiplexer
 * 
 * @param multiInfo a structure containing the information the multiplexers need to interact with the device.
 * @param pData     Pointer to an array containing the data
 * @param size      The mamximum number of bytes to send
 * @param timeout   Maximum time to wait for the data in milliseconds
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
CotaError_t I2CMultiplexWrite(I2CMultiplexerInfo_t* multiInfo, uint8_t* pData, uint32_t size, uint32_t timeout)
{
    CotaError_t ret = checkMultiInfo(multiInfo);
    HAL_StatusTypeDef retHal = HAL_OK;   

    if (xSemaphoreTake(gI2CMultiMutex, pdMS_TO_TICKS(timeout)) == pdPASS)
    {   
        ret = (ret == COTA_ERROR_NONE) ? selectMultiChannel(multiInfo, timeout) : ret;
        
        if (ret == COTA_ERROR_NONE)
        {
            retHal = HAL_I2C_Master_Transmit_IT(&MULIPLEXER_I2C_HANDLE, ADDR_TO_BYTE(multiInfo->devAddr), pData, size);
            
            if (retHal == HAL_OK)
            {
                retHal = (xSemaphoreTake(gI2CMultiplexTransSem, pdMS_TO_TICKS(timeout)) == pdPASS) ? HAL_OK : HAL_ERROR;
            }
            
            ret = (retHal != HAL_OK) ? COTA_ERROR_I2C_FAILED_TO_WRITE : ret;      
        }
        
        xSemaphoreGive( gI2CMultiMutex );
    }
    else
    {
        ret = COTA_ERROR_I2C_FAILED_TO_WRITE;
    }
    
    return ret;
}

/**
 * @brief Reads data from a device attached to an i2c multiplexer
 * 
 * @param multiInfo a structure containing the information the multiplexers need to interact with the device.
 * @param pData     Pointer to an array to receive the data
 * @param size      The mamximum number of bytes to receive
 * @param timeout   Maximum time to wait for the data in milliseconds
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
CotaError_t I2CMultiplexRead(I2CMultiplexerInfo_t* multiInfo, uint8_t* pData, uint32_t size, uint32_t timeout)
{
    CotaError_t ret = checkMultiInfo(multiInfo);
    HAL_StatusTypeDef retHal = HAL_OK;   
   
    if (xSemaphoreTake(gI2CMultiMutex, pdMS_TO_TICKS(timeout)) == pdPASS)
    {
    
        ret = (ret == COTA_ERROR_NONE) ? selectMultiChannel(multiInfo, timeout) : ret;
        
        if (ret == COTA_ERROR_NONE)
        {
            retHal = HAL_I2C_Master_Receive_IT(&MULIPLEXER_I2C_HANDLE, ADDR_TO_BYTE(multiInfo->devAddr), pData, size);
            
            if (retHal == HAL_OK)
            {
                retHal = (xSemaphoreTake(gI2CMultiplexTransSem, pdMS_TO_TICKS(timeout)) == pdPASS) ? HAL_OK : HAL_ERROR;
            }
            
            ret = (retHal != HAL_OK) ? COTA_ERROR_I2C_FAILED_TO_READ : ret;      
        }
      
        xSemaphoreGive( gI2CMultiMutex );   
    
    }
    else
    {
        ret = COTA_ERROR_I2C_FAILED_TO_READ;      
    }
    
    return ret;
}

CotaError_t I2CMultiplexMemWrite(I2CMultiplexerInfo_t* multiInfo, uint16_t MemAddress, uint16_t MemAddSize, uint8_t* pData, uint32_t size, uint32_t timeout)
{
    CotaError_t ret = checkMultiInfo(multiInfo);
    HAL_StatusTypeDef retHal = HAL_OK;   

    if (xSemaphoreTake(gI2CMultiMutex, pdMS_TO_TICKS(timeout)) == pdPASS)
    {   
        ret = (ret == COTA_ERROR_NONE) ? selectMultiChannel(multiInfo, timeout) : ret;
        
        if (ret == COTA_ERROR_NONE)
        {
            retHal = HAL_I2C_Mem_Write_IT(&MULIPLEXER_I2C_HANDLE, ADDR_TO_BYTE(multiInfo->devAddr), MemAddress, MemAddSize, pData, size);
            
            if (retHal == HAL_OK)
            {
                retHal = (xSemaphoreTake(gI2CMultiplexTransSem, pdMS_TO_TICKS(timeout)) == pdPASS) ? HAL_OK : HAL_ERROR;
            }
            
            ret = (retHal != HAL_OK) ? COTA_ERROR_I2C_FAILED_TO_WRITE : ret;      
        }
        
        xSemaphoreGive( gI2CMultiMutex );
    }
    else
    {
        ret = COTA_ERROR_I2C_FAILED_TO_WRITE;
    }
    
    return ret;    
}

/**
 * @brief Reads data from a memory access device attached to an i2c multiplexer
 * 
 * @param multiInfo a structure containing the information the multiplexers need to interact with the device.
 * @param MemAddress Memory address to read from
 * @param MemAddSize Memory address size
 * @param pData     Pointer to an array to receive the data
 * @param size      The mamximum number of bytes to receive
 * @param timeout   Maximum time to wait for the data in milliseconds
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #CotaError_t
 */
CotaError_t I2CMultiplexMemRead(I2CMultiplexerInfo_t* multiInfo, uint16_t MemAddress, uint16_t MemAddSize, uint8_t* pData, uint32_t size, uint32_t timeout)
{
    CotaError_t ret = checkMultiInfo(multiInfo);
    HAL_StatusTypeDef retHal = HAL_OK;   
   
    if (xSemaphoreTake(gI2CMultiMutex, pdMS_TO_TICKS(timeout)) == pdPASS)
    {
        ret = (ret == COTA_ERROR_NONE) ? selectMultiChannel(multiInfo, timeout) : ret;
        
        if (ret == COTA_ERROR_NONE)
        {
            retHal = HAL_I2C_Mem_Read_IT(&MULIPLEXER_I2C_HANDLE, ADDR_TO_BYTE(multiInfo->devAddr), MemAddress, MemAddSize, pData, size);
            
            if (retHal == HAL_OK)
            {
                retHal = (xSemaphoreTake(gI2CMultiplexTransSem, pdMS_TO_TICKS(timeout)) == pdPASS) ? HAL_OK : HAL_ERROR;
            }
            
            ret = (retHal != HAL_OK) ? COTA_ERROR_I2C_FAILED_TO_READ : ret;      
        }
      
        xSemaphoreGive( gI2CMultiMutex );   
    
    }
    else
    {
        ret = COTA_ERROR_I2C_FAILED_TO_READ;      
    }
    
    return ret;
}



/**
 * @brief Creates the semaphore needed to communicate with the i2c multiplexers
 */
void InitI2CMultiplexerSemaphores(void)
{ 
    gI2CMultiplexTransSem = xSemaphoreCreateBinaryStatic(&gI2CMultiplexTransSemBuff);
    while (gI2CMultiplexTransSem == NULL);
    gI2CMultiMutex = xSemaphoreCreateMutexStatic(&gI2CMultiMutexBuff); 
    while (gI2CMultiMutex == NULL);
    xSemaphoreGive( gI2CMultiMutex );
}

/**
 * @brief Registers a semaphore that will be "given" if an interrupt is detected from the device.
 *  
 * @todo  This will be implemented in the future.  In order to monitor interrupts we need to create a task
 *        that waits for interrupts from IRQ_C1, IRQ_C2.  When receiving an interrupt, it must read 
 *        the register from the multiplexer to determine which device threw an interrupt,
 *        then "Give" that semaphore that was registered to it.
 *
 * @param multiInfo   a structure containing the information the multiplexers need to interact with the device.
 * @param semaphore  The semaphore we want given when an interrupt is received from the device.
 */ 
CotaError_t RegisterI2CMultiplexerSemaphore(I2CMultiplexerInfo_t* multiInfo, SemaphoreHandle_t* semaphore)
{
    return COTA_ERROR_NOT_IMPLEMENTED;
}

/**
 * @brief Resets the I2C multiplexer by toggling their its gpio
 */
void ResetI2CMultiplexer(void)
{
    HAL_GPIO_WritePin(I2C_RESET_GPIO_Port, I2C_RESET_Pin, GPIO_PIN_RESET); 
    vTaskDelay( pdMS_TO_TICKS(1));
    HAL_GPIO_WritePin(I2C_RESET_GPIO_Port, I2C_RESET_Pin, GPIO_PIN_SET);
    vTaskDelay( pdMS_TO_TICKS(1));
}

/**
 * @brief Interrupt called when a I2C Mem Tx is finished
 *
 * @param hi2c The handle to I2C bus that finished the mem tx
 */
void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
      
    if (hi2c == (&EEPROM_I2C_HANDLE))
    {
         Eeprom_I2C_MemTxCpltCallback(hi2c);
    }

    if (hi2c == (&SYSCLK_I2C_HANDLE))
    {
        //SysClock_I2C_RxCpltCallback(hi2c);
    }
    
    if (hi2c == (&MULIPLEXER_I2C_HANDLE))
    {
        xSemaphoreGiveFromISR(gI2CMultiplexTransSem, &xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
  
}

/**
 * @brief Interrupt called when a I2C Mem Rx is finished
 *
 * @param hi2c The handle to I2C bus that finished the mem rx
 */
void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
      BaseType_t xHigherPriorityTaskWoken = pdFALSE;
      
    if (hi2c == (&EEPROM_I2C_HANDLE))
    {
         Eeprom_I2C_MemRxCpltCallback(hi2c);
    }

    if (hi2c == (&SYSCLK_I2C_HANDLE))
    {
        //SysClock_I2C_RxCpltCallback(hi2c);
    }
    
    if (hi2c == (&MULIPLEXER_I2C_HANDLE))
    {
        xSemaphoreGiveFromISR(gI2CMultiplexTransSem, &xHigherPriorityTaskWoken);
        portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    }
  
  
}