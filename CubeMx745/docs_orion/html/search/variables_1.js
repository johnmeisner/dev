var searchData=
[
  ['batterycharge',['batteryCharge',['../struct___resp_reduced_rcvr_info__t.html#adc6da04258f2c25f5080b3e809f1b63c',1,'_RespReducedRcvrInfo_t::batteryCharge()'],['../_cota_common_types_8h.html#adc6da04258f2c25f5080b3e809f1b63c',1,'batteryCharge():&#160;CotaCommonTypes.h']]],
  ['batteryvoltage',['batteryVoltage',['../client_8h.html#a513aabaf4b76cee401555e324d944b29',1,'client.h']]],
  ['beaconencoding',['beaconEncoding',['../_cota_common_types_8h.html#a42288450aa67cced567863a8e741b674',1,'CotaCommonTypes.h']]],
  ['beat',['beat',['../struct___msg_static_charge__t.html#a49b5eabc8894906818f9f606cd67105e',1,'_MsgStaticCharge_t']]],
  ['bldtype',['bldType',['../struct___resp_version__t.html#afd4a411792b2ae3ae09ea12e58c4a328',1,'_RespVersion_t']]],
  ['buff',['buff',['../struct___resp_log__t.html#aee22317222ce4274d1dab3ae6fcbbc3f',1,'_RespLog_t']]],
  ['byteaddr',['byteAddr',['../struct___uvp_reg__t.html#ab0b43d6ff4f082100c115c890fab0f6f',1,'_UvpReg_t']]],
  ['bytes',['bytes',['../_cota_common_types_8h.html#a089a312f6bb9fa09600b17de27de9ae9',1,'CotaCommonTypes.h']]]
];
