var searchData=
[
  ['r0_5fdiv_5freg',['R0_DIV_REG',['../sysclk__driver_8c.html#a394bf539bb53d09717ecd595131489e8',1,'sysclk_driver.c']]],
  ['read_5faccess_5ftype_5f8_5fbit',['READ_ACCESS_TYPE_8_BIT',['../proxy__fw__update__driver_8c.html#ad5545d573f36c7bc7437aa394b7e3977',1,'proxy_fw_update_driver.c']]],
  ['read_5fmr_5fstate',['READ_MR_STATE',['../raspberry__pi_8c.html#af25fc755ba41d56ed387745a188878ee',1,'raspberry_pi.c']]],
  ['read_5fsr_5fstate',['READ_SR_STATE',['../proxy_8c.html#a1ef148132b6b40ad65255416cd240268',1,'proxy.c']]],
  ['read_5fwrite_5fbuf_5fsize',['READ_WRITE_BUF_SIZE',['../proxy__fw__update__driver_8c.html#a99b58bb0c5ca5c3214ed6d1d78e6babf',1,'proxy_fw_update_driver.c']]],
  ['refgenen_5fidle',['REFGENEN_IDLE',['../uvp__driver_8h.html#a9395e0862241c4bb0bbe203d4dab97de',1,'uvp_driver.h']]],
  ['refgenen_5fsendpwr',['REFGENEN_SENDPWR',['../uvp__driver_8h.html#a38dc0c446acd852454236eb1b133218d',1,'uvp_driver.h']]],
  ['refoscampgain',['REFOSCAMPGAIN',['../uvp__driver_8h.html#a876aa5b426832cea0449e6257e09a7d0',1,'uvp_driver.h']]],
  ['refoscampmode',['REFOSCAMPMODE',['../uvp__driver_8h.html#afa416a5e4d1ec17b6a66afab1459f217',1,'uvp_driver.h']]],
  ['refoscen_5fsendpwr',['REFOSCEN_SENDPWR',['../uvp__driver_8h.html#ae44b4a1f064f84e9e2ffd7a6918b8250',1,'uvp_driver.h']]],
  ['regfile_5fpage',['REGFILE_PAGE',['../uvp__driver_8h.html#a7b31682492de236226d49fa48e9409a7',1,'uvp_driver.h']]],
  ['reserved09',['RESERVED09',['../uvp__driver_8h.html#ada40eb49338ec70278c236a657bfe08e',1,'uvp_driver.h']]],
  ['reserved12',['RESERVED12',['../uvp__driver_8h.html#a71618dc3e60f01656cf5780c152b9584',1,'uvp_driver.h']]],
  ['reserved29',['RESERVED29',['../uvp__driver_8h.html#afd92b7d25c1280aaf162449c63ef718a',1,'uvp_driver.h']]],
  ['reserved30',['RESERVED30',['../uvp__driver_8h.html#a0e911d6d8fa599f03858311e71bce74a',1,'uvp_driver.h']]],
  ['reserved31',['RESERVED31',['../uvp__driver_8h.html#a72b79c3c496b526ec3d51b9698f27ed1',1,'uvp_driver.h']]],
  ['reserved32',['RESERVED32',['../uvp__driver_8h.html#afa9e190bc325486126db6b75a594ccd4',1,'uvp_driver.h']]],
  ['reserved33',['RESERVED33',['../uvp__driver_8h.html#a4cf3b46f4e1d1f2d98c970e517140c9f',1,'uvp_driver.h']]],
  ['rpi_5fmax_5fmsg_5fsize',['RPI_MAX_MSG_SIZE',['../rpi__msg__interface_8h.html#af1547ce88bd39a22590ab7b45df29158',1,'rpi_msg_interface.h']]],
  ['rpi_5fstate_5ftimeout',['RPI_STATE_TIMEOUT',['../raspberry__pi_8c.html#a29fc0f702f8e1d1abb262ad3fdec833e',1,'raspberry_pi.c']]],
  ['rpi_5ftask_5ftimeout',['RPI_TASK_TIMEOUT',['../raspberry__pi_8c.html#a7a3f8d18a91564f538bc38ed7c9557e9',1,'raspberry_pi.c']]],
  ['rpi_5ftx_5fqueue_5fitem_5fsize',['RPI_TX_QUEUE_ITEM_SIZE',['../raspberry__pi_8c.html#a957832a136f05da0e854b8f6af345eb0',1,'raspberry_pi.c']]],
  ['rpi_5ftx_5fqueue_5flen',['RPI_TX_QUEUE_LEN',['../raspberry__pi_8c.html#a0b186dc46a0e9b3b6cc8c7df429014c8',1,'raspberry_pi.c']]],
  ['rssi_5famu1',['RSSi_AMU1',['../uvp__driver_8h.html#aae6f561943cac1e686f67470c595cee3',1,'uvp_driver.h']]],
  ['rssi_5famu2',['RSSi_AMU2',['../uvp__driver_8h.html#ada41f92a8e06ccd3426d9af11908cba5',1,'uvp_driver.h']]],
  ['rssi_5famu3',['RSSi_AMU3',['../uvp__driver_8h.html#a213a320f26cfe583eaf473779b0fa41f',1,'uvp_driver.h']]],
  ['rssi_5famu4',['RSSi_AMU4',['../uvp__driver_8h.html#adfe27d7771154e842927feeaa91258a5',1,'uvp_driver.h']]],
  ['rssi_5fextract_5famu0',['RSSI_EXTRACT_AMU0',['../gather__and__vote__task_8c.html#a26bdccc5c1bad7d407b7b77cc0096f13',1,'gather_and_vote_task.c']]],
  ['rssi_5fextract_5famu1',['RSSI_EXTRACT_AMU1',['../gather__and__vote__task_8c.html#ac3691aaf957dbed44cde10b69b44162b',1,'gather_and_vote_task.c']]],
  ['rssi_5fextract_5famu2',['RSSI_EXTRACT_AMU2',['../gather__and__vote__task_8c.html#ae3a8a2c99a627bcf7860a447e5873340',1,'gather_and_vote_task.c']]],
  ['rssi_5fextract_5famu3',['RSSI_EXTRACT_AMU3',['../gather__and__vote__task_8c.html#ada939dc19f3148826896936a335b5360',1,'gather_and_vote_task.c']]],
  ['rssi_5fgain_5fscale',['RSSI_GAIN_SCALE',['../uvp__driver_8h.html#a3c5276132f98e3ea4fca12d68b2db6c2',1,'uvp_driver.h']]],
  ['rssi_5fhighthresh',['RSSI_HIGHTHRESH',['../uvp__driver_8h.html#adaccb11108ed1258a164c1f5e704d784',1,'uvp_driver.h']]],
  ['rssi_5flowthresh',['RSSI_LOWTHRESH',['../uvp__driver_8h.html#a88b2961b3c5110604ee885527e353f32',1,'uvp_driver.h']]],
  ['rssi_5fthresh_5fpayload_5fsize',['RSSI_THRESH_PAYLOAD_SIZE',['../nvm__driver_8c.html#aa2a67a4346ef5ae39cc18432bd82bd50',1,'nvm_driver.c']]],
  ['rssi_5fthresh_5fsize',['RSSI_THRESH_SIZE',['../nvm__driver_8c.html#a77c49b2c5ae8b40754a51de579997a75',1,'nvm_driver.c']]],
  ['rssi_5fthresh_5fstart_5faddr',['RSSI_THRESH_START_ADDR',['../nvm__driver_8c.html#af6038add4819c373cd0ea414a9258f3c',1,'nvm_driver.c']]],
  ['rx_5fdiv_5fbytes',['RX_DIV_BYTES',['../sysclk__driver_8c.html#aa0af01dd3f341911f566ef6ad43057bb',1,'sysclk_driver.c']]],
  ['rx_5foff',['RX_OFF',['../amb__control_8c.html#a04233129cab07f89b58b629e5a0a7a67',1,'amb_control.c']]],
  ['rx_5fon',['RX_ON',['../amb__control_8c.html#a359db88bcffa6b39bdb64ec00ec3a1ae',1,'amb_control.c']]],
  ['rx_5fpacket_5fheader_5fsize',['RX_PACKET_HEADER_SIZE',['../proxy__fw__update__driver_8c.html#a1f6842870b25a29549aca0c9f91fbbeb',1,'proxy_fw_update_driver.c']]]
];
