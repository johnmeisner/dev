var searchData=
[
  ['waitforphasedetectdone',['waitForPhaseDetectDone',['../calibrator_8c.html#a267d351d9d0479674c3011740e8d8f68',1,'calibrator.c']]],
  ['waitforpowergood',['WaitForPowerGood',['../power__monitor_8c.html#aa500cc7ad89c7f2f6b5614d07709f5f4',1,'WaitForPowerGood(uint16_t timeoutMs):&#160;power_monitor.c'],['../power__monitor_8h.html#aa500cc7ad89c7f2f6b5614d07709f5f4',1,'WaitForPowerGood(uint16_t timeoutMs):&#160;power_monitor.c']]],
  ['waitfortxready',['waitForTxReady',['../calibrator_8c.html#a33d77153dfabc4613cb7b270832c3812',1,'calibrator.c']]],
  ['walk',['Walk',['../client__interface_8c.html#ab559ca1660ee4e874271109bc4c37f10',1,'Walk(void(*func)(Client_t *client)):&#160;client_manager.c'],['../client__manager_8c.html#a9bf45a0a687e0a19abc7e1a1a91179d4',1,'Walk(void(*func)(Client_t *client)):&#160;client_manager.c'],['../cqt__scheduler_8c.html#ab559ca1660ee4e874271109bc4c37f10',1,'Walk(void(*func)(Client_t *client)):&#160;client_manager.c']]],
  ['walktofindstate',['WalkToFindState',['../client__avl__tree_8c.html#abe782207937d6ecb2350bd95fe5c4df8',1,'WalkToFindState(ClientState_t key, ClientNode_t *node, Client_t *oclient):&#160;client_avl_tree.c'],['../client__avl__tree_8h.html#abe782207937d6ecb2350bd95fe5c4df8',1,'WalkToFindState(ClientState_t key, ClientNode_t *node, Client_t *oclient):&#160;client_avl_tree.c']]],
  ['walktree',['WalkTree',['../client__avl__tree_8c.html#a1e07af4edc45e21a18ea3fdef7215069',1,'WalkTree(ClientNode_t *node, void(*func)(Client_t *client)):&#160;client_avl_tree.c'],['../client__avl__tree_8h.html#a1e07af4edc45e21a18ea3fdef7215069',1,'WalkTree(ClientNode_t *node, void(*func)(Client_t *client)):&#160;client_avl_tree.c']]],
  ['writearrayuvps',['WriteArrayUvps',['../uvp__driver_8c.html#adafdabb800e84bd782487396772499bd',1,'WriteArrayUvps(UvpReg_t pReg, uint32_t *pData, uint32_t numValues, AmbMask_t ambMask, UvpMask_t uvpMask):&#160;uvp_driver.c'],['../uvp__driver_8h.html#adafdabb800e84bd782487396772499bd',1,'WriteArrayUvps(UvpReg_t pReg, uint32_t *pData, uint32_t numValues, AmbMask_t ambMask, UvpMask_t uvpMask):&#160;uvp_driver.c']]],
  ['writebytestobuffer',['writeBytesToBuffer',['../variable__queue_8c.html#a70a0e3f7fb17f17387d454774004d138',1,'variable_queue.c']]],
  ['writeclientdataat',['writeClientDataAt',['../nvm__driver_8c.html#a0f7c8bddb3b45dd173ae702ab7610a0f',1,'nvm_driver.c']]],
  ['writefanregister',['writeFanRegister',['../fan__driver_8c.html#a42a02d91d4c7f7dc12a4f57a9ed1c09f',1,'fan_driver.c']]],
  ['writemanyuvps',['WriteManyUvps',['../uvp__driver_8c.html#a15a0bc4833823459185a952ba88fe3c5',1,'WriteManyUvps(UvpReg_t pReg, uint32_t data, AmbMask_t ambMask, UvpMask_t uvpMask):&#160;uvp_driver.c'],['../uvp__driver_8h.html#a15a0bc4833823459185a952ba88fe3c5',1,'WriteManyUvps(UvpReg_t pReg, uint32_t data, AmbMask_t ambMask, UvpMask_t uvpMask):&#160;uvp_driver.c']]],
  ['writendelay',['writeNDelay',['../sysclk__driver_8c.html#ac922d77a728a0afe928547e41f61b658',1,'sysclk_driver.c']]],
  ['writennum',['writeNNum',['../sysclk__driver_8c.html#a20f7c174b56bf742380d136583633644',1,'sysclk_driver.c']]],
  ['writenupdate',['writeNUpdate',['../sysclk__driver_8c.html#abacfe7bfb745e94ee478229a7c3ab8a5',1,'sysclk_driver.c']]],
  ['writephaseoffset',['WritePhaseOffset',['../calibrator_8c.html#aa1c28c4750f2b76c482b296e9f12e8ad',1,'calibrator.c']]]
];
