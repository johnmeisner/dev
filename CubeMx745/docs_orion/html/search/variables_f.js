var searchData=
[
  ['qstatus',['qStatus',['../struct___client__t.html#a282c6ad54280c3a592019858efdbfe2a',1,'_Client_t']]],
  ['queryneededflag',['queryNeededFlag',['../struct___client__t.html#a1c43d4aabe3a3a3260f8d6abd058d73b',1,'_Client_t']]],
  ['querytype',['queryType',['../struct___msg_reg_rcvr__t.html#afd9021f4bfe53aab62d3b6c312467588',1,'_MsgRegRcvr_t::queryType()'],['../struct___msg_set_rcvr_config__t.html#afd9021f4bfe53aab62d3b6c312467588',1,'_MsgSetRcvrConfig_t::queryType()'],['../struct___nvm_client_data__t.html#afd9021f4bfe53aab62d3b6c312467588',1,'_NvmClientData_t::queryType()'],['../struct___client_slot_data__t.html#afd9021f4bfe53aab62d3b6c312467588',1,'_ClientSlotData_t::queryType()'],['../_cota_common_types_8h.html#afd9021f4bfe53aab62d3b6c312467588',1,'queryType():&#160;CotaCommonTypes.h'],['../proxy_client_msg_inf_8h.html#afd9021f4bfe53aab62d3b6c312467588',1,'queryType():&#160;proxyClientMsgInf.h'],['../proxy_host_msg_inf_8h.html#ab2a4982b5af7d4f25c4f5805ce7e9a7b',1,'queryType():&#160;proxyHostMsgInf.h']]]
];
