var indexSectionsWithContent =
{
  0: "_abcdefghijlmnopqrstuvwxy",
  1: "_c",
  2: "acdefgilmnoprstuv",
  3: "abcdefghijlmnoprstuvwx",
  4: "abcdefghijlmnopqrstuvwy",
  5: "acdefgilmnprstuv",
  6: "_cdlp",
  7: "acdfghlnprstw",
  8: "abcdefghijlmnopqrstuvwx",
  9: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

