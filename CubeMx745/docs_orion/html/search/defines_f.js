var searchData=
[
  ['qob_5fcheck_5fdisable',['QOB_CHECK_DISABLE',['../uvp__driver_8h.html#ae2dd9fbe58c0ebef742bda3f049877b8',1,'uvp_driver.h']]],
  ['qspi_5fclk_5fgpio_5fport',['QSPI_CLK_GPIO_Port',['../main_8h.html#a99c65c07809ccb78eb67a76f75febd71',1,'main.h']]],
  ['qspi_5fclk_5fpin',['QSPI_CLK_Pin',['../main_8h.html#aac181cd2961f99e122ffb12b2ff0f70f',1,'main.h']]],
  ['qspi_5fio0_5fgpio_5fport',['QSPI_IO0_GPIO_Port',['../main_8h.html#a146416ed82bea7772af33038653c1980',1,'main.h']]],
  ['qspi_5fio0_5fpin',['QSPI_IO0_Pin',['../main_8h.html#aaad848d5bade3ca8a6bc8aa5e388b825',1,'main.h']]],
  ['qspi_5fio1_5fgpio_5fport',['QSPI_IO1_GPIO_Port',['../main_8h.html#a5ba1facc1a471d780a2c96959cf1815b',1,'main.h']]],
  ['qspi_5fio1_5fpin',['QSPI_IO1_Pin',['../main_8h.html#a8778f102951b42fb3254709c0361527b',1,'main.h']]],
  ['qspi_5fio2_5fgpio_5fport',['QSPI_IO2_GPIO_Port',['../main_8h.html#a780c5d7739d250767f3a4cc42f4cf0fd',1,'main.h']]],
  ['qspi_5fio2_5fpin',['QSPI_IO2_Pin',['../main_8h.html#ae8f7d598f41b2455814528e4daa35152',1,'main.h']]],
  ['qspi_5fio3_5fgpio_5fport',['QSPI_IO3_GPIO_Port',['../main_8h.html#afd224a40e965b68124f91f968ce38f55',1,'main.h']]],
  ['qspi_5fio3_5fpin',['QSPI_IO3_Pin',['../main_8h.html#a86e291b1b0f221a502530d71ea8529d0',1,'main.h']]],
  ['qspi_5fncs_5fgpio_5fport',['QSPI_NCS_GPIO_Port',['../main_8h.html#acb43c5fa4a88387496447e58f48f3efb',1,'main.h']]],
  ['qspi_5fncs_5fpin',['QSPI_NCS_Pin',['../main_8h.html#a85b708169003371ea2d46ab1188b8e9e',1,'main.h']]]
];
