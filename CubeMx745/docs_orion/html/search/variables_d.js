var searchData=
[
  ['off_5fmsg',['OFF_MSG',['../cli__task_8h.html#aedf21a32ebcdb7b4aa7b61aa462613d3',1,'cli_task.h']]],
  ['offset',['offset',['../struct___uvp_reg_read__t.html#aa9fff43968831437a312428836cab362',1,'_UvpRegRead_t::offset()'],['../struct___fan_reg__t.html#aa9fff43968831437a312428836cab362',1,'_FanReg_t::offset()'],['../struct___uvp_reg__t.html#a97bd6c077f3c7769f575b82988b9b668',1,'_UvpReg_t::offset()']]],
  ['on_5fmsg',['ON_MSG',['../cli__task_8h.html#a84bd05620b9cca003e4ad3591f262831',1,'cli_task.h']]],
  ['otafwcrccalc',['otaFwCrcCalc',['../_cota_common_types_8h.html#a0cb31a3219700d9ae358591c110ceb3e',1,'CotaCommonTypes.h']]],
  ['otafwcrcref',['otaFwCrcRef',['../_cota_common_types_8h.html#a4eb8f034f9d312c45ba3a3822297ed6f',1,'CotaCommonTypes.h']]],
  ['otafwtime',['otaFwTime',['../_cota_common_types_8h.html#a1876e4934d815e85c3a31156ed48b15e',1,'CotaCommonTypes.h']]],
  ['otafwver',['otaFwVer',['../_cota_common_types_8h.html#a45228cc37dba46341314ad7eb39fdd7c',1,'CotaCommonTypes.h']]]
];
