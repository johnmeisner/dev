var searchData=
[
  ['celsius_5fto_5fcenticelsius',['CELSIUS_TO_CENTICELSIUS',['../ccb__temperature_8h.html#a2104d616a72f0312cdcf1d93455789bb',1,'ccb_temperature.h']]],
  ['centicelsius_5fto_5fcelsius',['CENTICELSIUS_TO_CELSIUS',['../ccb__temperature_8h.html#adf69abd4d7d872a8285bafd8f715b8c2',1,'ccb_temperature.h']]],
  ['channel_5fto_5freg',['CHANNEL_TO_REG',['../i2c__multiplex_8c.html#a0ba1d2be13ed16ece9e2176d49ae574d',1,'i2c_multiplex.c']]],
  ['channels_5fper_5fmultiplexer',['CHANNELS_PER_MULTIPLEXER',['../i2c__multiplex_8c.html#ac12a2da61c3245ab2a24770c3e6fd71b',1,'i2c_multiplex.c']]],
  ['checksum_5fsize',['CHECKSUM_SIZE',['../rpi__msg__interface_8c.html#ad8ef1a732c15e66fe4bbaa087462f507',1,'CHECKSUM_SIZE():&#160;rpi_msg_interface.c'],['../proxy_8c.html#ad8ef1a732c15e66fe4bbaa087462f507',1,'CHECKSUM_SIZE():&#160;proxy.c'],['../raspberry__pi_8c.html#ad8ef1a732c15e66fe4bbaa087462f507',1,'CHECKSUM_SIZE():&#160;raspberry_pi.c']]],
  ['cli_5fcommand_5fstorage_5farea_5fsize',['CLI_COMMAND_STORAGE_AREA_SIZE',['../cli__task_8c.html#a479fedffda91325739d743e23d1f6998',1,'cli_task.c']]],
  ['cli_5ftx_5fmessage_5fbuffer_5fsize',['CLI_TX_MESSAGE_BUFFER_SIZE',['../cli__task_8c.html#aef15e255fc5f56bdbde43c454703022a',1,'cli_task.c']]],
  ['client_5faddress_5ffrom_5fslot_5fnum',['CLIENT_ADDRESS_FROM_SLOT_NUM',['../nvm__driver_8c.html#a9e6bb1f7c662607cebc2b94961d13b9a',1,'nvm_driver.c']]],
  ['client_5fselect_5fout',['CLIENT_SELECT_OUT',['../uvp__driver_8h.html#a5a7c8880e8bb680cc79bfce2959ad24c',1,'uvp_driver.h']]],
  ['client_5fslot_5fpayload_5fsize',['CLIENT_SLOT_PAYLOAD_SIZE',['../nvm__driver_8c.html#ac4a43993896378149c066479910004e8',1,'nvm_driver.c']]],
  ['client_5fslot_5fsize',['CLIENT_SLOT_SIZE',['../nvm__driver_8c.html#a75d6f366facb481f6e2ac2157fc85af3',1,'nvm_driver.c']]],
  ['client_5fstart_5faddr',['CLIENT_START_ADDR',['../nvm__driver_8c.html#a518126f92ff3a5d5ad2b94379d4cd40d',1,'nvm_driver.c']]],
  ['clk_5finput_5fctrl_5freg',['CLK_INPUT_CTRL_REG',['../sysclk__driver_8c.html#aa90143fdffd1f0001f51c757eaebfc8e',1,'sysclk_driver.c']]],
  ['clk_5finput_5fin1_5fen_5foffset',['CLK_INPUT_IN1_EN_OFFSET',['../sysclk__driver_8c.html#af0c69b707127e4cbbf78cc9941ce9d18',1,'sysclk_driver.c']]],
  ['clk_5finput_5fin1_5fen_5fsize',['CLK_INPUT_IN1_EN_SIZE',['../sysclk__driver_8c.html#aaae59613e97f7a1154c6430e9cc09b97',1,'sysclk_driver.c']]],
  ['clk_5finput_5fin1_5fen_5fval',['CLK_INPUT_IN1_EN_VAL',['../sysclk__driver_8c.html#a76a534dfba8cee4b53a3b71e647325ad',1,'sysclk_driver.c']]],
  ['clkd_5fsck_5fgpio_5fport',['CLKD_SCK_GPIO_Port',['../main_8h.html#ac01278a3426d53eb122e8782f4cc63a9',1,'main.h']]],
  ['clkd_5fsck_5fpin',['CLKD_SCK_Pin',['../main_8h.html#a0b68c8d3836cf74de49b4a06adb90bc2',1,'main.h']]],
  ['clkd_5fsda_5fgpio_5fport',['CLKD_SDA_GPIO_Port',['../main_8h.html#ac0a168f510026a24364d171831fc4c3f',1,'main.h']]],
  ['clkd_5fsda_5fpin',['CLKD_SDA_Pin',['../main_8h.html#adbc77c652a175193298f00194b55b58d',1,'main.h']]],
  ['clkgen_5fen',['CLKGEN_EN',['../uvp__driver_8h.html#aac96ca235ca99d0abbfd41a91efa2925',1,'uvp_driver.h']]],
  ['cmd_5fdownload',['CMD_DOWNLOAD',['../proxy__fw__update__driver_8c.html#a6097de8a0b45497dac49bd4d599e77be',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fget_5fstatus',['CMD_GET_STATUS',['../proxy__fw__update__driver_8c.html#a66f7b6f26a4f3c2ff469ccf6ae05c44a',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fmemory_5fread',['CMD_MEMORY_READ',['../proxy__fw__update__driver_8c.html#a886321d363f5a6dd43a5124f41104495',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fpacket_5fheader_5fsize',['CMD_PACKET_HEADER_SIZE',['../proxy__fw__update__driver_8c.html#ad1da090a2d581624138820089de69f44',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fpacket_5fsize_5fdownload',['CMD_PACKET_SIZE_DOWNLOAD',['../proxy__fw__update__driver_8c.html#a49acee0e816d6992b2613889bf7d5b3f',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fpacket_5fsize_5ferase',['CMD_PACKET_SIZE_ERASE',['../proxy__fw__update__driver_8c.html#a184dcadc3a542e8dd7f789a97e353cc8',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fpacket_5fsize_5fping',['CMD_PACKET_SIZE_PING',['../proxy__fw__update__driver_8c.html#a241809a67109b217146b393ce9330cf5',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fpacket_5fsize_5fread',['CMD_PACKET_SIZE_READ',['../proxy__fw__update__driver_8c.html#aea41e13defbafbc675098f95b8eac42c',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fpacket_5fsize_5fstatus',['CMD_PACKET_SIZE_STATUS',['../proxy__fw__update__driver_8c.html#a19334dd77031cea65e1fd316882f0e2e',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fpacket_5fsize_5fstatus_5fresp',['CMD_PACKET_SIZE_STATUS_RESP',['../proxy__fw__update__driver_8c.html#a1bb186f7c1644c0396f413a7dbce90ff',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fping',['CMD_PING',['../proxy__fw__update__driver_8c.html#a7ed51c574a0b4fbc7f75646bf1805de3',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fsector_5ferase',['CMD_SECTOR_ERASE',['../proxy__fw__update__driver_8c.html#a37153d871f9e079923d99fb7ca18371b',1,'proxy_fw_update_driver.c']]],
  ['cmd_5fsend_5fdata',['CMD_SEND_DATA',['../proxy__fw__update__driver_8c.html#a752c4ae0ea8115ede1ea38ff7df9a0de',1,'proxy_fw_update_driver.c']]],
  ['cml2cmosen_5fidle',['CML2CMOSEN_IDLE',['../uvp__driver_8h.html#af6df832dad282c474a72e4cb5006b4e2',1,'uvp_driver.h']]],
  ['cml2cmosen_5fsendpwr',['CML2CMOSEN_SENDPWR',['../uvp__driver_8h.html#ab6b6018364f17133e4e8da6e5cd46691',1,'uvp_driver.h']]],
  ['codeclk_5fph',['CODECLK_PH',['../uvp__driver_8h.html#a622ed7b4f5325d7566d6dc974017e573',1,'uvp_driver.h']]],
  ['codeclken',['CODECLKEN',['../uvp__driver_8h.html#a3508856d34edf2b829de8ee422f517ac',1,'uvp_driver.h']]],
  ['comm_5fchannel_5fa',['COMM_CHANNEL_A',['../_cota_frequencies_8h.html#a1b2be05336dbe444c9041612deed35e3',1,'CotaFrequencies.h']]],
  ['comm_5fchannel_5fb',['COMM_CHANNEL_B',['../_cota_frequencies_8h.html#a8c5a14b1530595594015414bc99cc75e',1,'CotaFrequencies.h']]],
  ['comm_5fchannel_5fc',['COMM_CHANNEL_C',['../_cota_frequencies_8h.html#af14da332fd1d641dfe826a396b5395c4',1,'CotaFrequencies.h']]],
  ['comm_5fchannel_5fd',['COMM_CHANNEL_D',['../_cota_frequencies_8h.html#a5904213b56112804e5dbf8d07ca6cbdb',1,'CotaFrequencies.h']]],
  ['comm_5fchannel_5fe',['COMM_CHANNEL_E',['../_cota_frequencies_8h.html#ade1713c2f14d644f0eaf7631ce0212de',1,'CotaFrequencies.h']]],
  ['comm_5fchannel_5ff',['COMM_CHANNEL_F',['../_cota_frequencies_8h.html#a38263d204cb99a669bc575e36c1987dd',1,'CotaFrequencies.h']]],
  ['config_5fpage_5fpayload_5fsize',['CONFIG_PAGE_PAYLOAD_SIZE',['../nvm__driver_8c.html#a714fd2b7c98686d480495b90cfc69bc6',1,'nvm_driver.c']]],
  ['config_5fpage_5fstart_5faddr',['CONFIG_PAGE_START_ADDR',['../nvm__driver_8c.html#afd12a892658696be9d75ca3bf254f104',1,'nvm_driver.c']]],
  ['configassert',['configASSERT',['../_free_r_t_o_s_config_8h.html#a228c70cd48927d6ab730ed1a6dfbe35f',1,'FreeRTOSConfig.h']]],
  ['configcommand_5fint_5fmax_5foutput_5fsize',['configCOMMAND_INT_MAX_OUTPUT_SIZE',['../_free_r_t_o_s_config_8h.html#a44c717cda063be24eede219ea3404bb9',1,'FreeRTOSConfig.h']]],
  ['configcpu_5fclock_5fhz',['configCPU_CLOCK_HZ',['../_free_r_t_o_s_config_8h.html#aa68082df879e6fc96bcb9b26513639e7',1,'FreeRTOSConfig.h']]],
  ['configkernel_5finterrupt_5fpriority',['configKERNEL_INTERRUPT_PRIORITY',['../_free_r_t_o_s_config_8h.html#ac42cff506ad61d4174fa23e952e3225e',1,'FreeRTOSConfig.h']]],
  ['configlibrary_5flowest_5finterrupt_5fpriority',['configLIBRARY_LOWEST_INTERRUPT_PRIORITY',['../_free_r_t_o_s_config_8h.html#a10da20f180ec9bd131b0052a802dbc39',1,'FreeRTOSConfig.h']]],
  ['configlibrary_5fmax_5fsyscall_5finterrupt_5fpriority',['configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY',['../_free_r_t_o_s_config_8h.html#a2254bd235d882be3061bcad0b1e8be98',1,'FreeRTOSConfig.h']]],
  ['configmax_5fco_5froutine_5fpriorities',['configMAX_CO_ROUTINE_PRIORITIES',['../_free_r_t_o_s_config_8h.html#ae8f3fd645e6e78dfeb8a6e874af6195a',1,'FreeRTOSConfig.h']]],
  ['configmax_5fpriorities',['configMAX_PRIORITIES',['../_free_r_t_o_s_config_8h.html#a9a78f5ac61e6cb172dadf2a51f11db38',1,'FreeRTOSConfig.h']]],
  ['configmax_5fsyscall_5finterrupt_5fpriority',['configMAX_SYSCALL_INTERRUPT_PRIORITY',['../_free_r_t_o_s_config_8h.html#a54bfc31c410ee452577a25a4552c3704',1,'FreeRTOSConfig.h']]],
  ['configmax_5ftask_5fname_5flen',['configMAX_TASK_NAME_LEN',['../_free_r_t_o_s_config_8h.html#ac388dc4041aab6997348828eb27fc1a8',1,'FreeRTOSConfig.h']]],
  ['configminimal_5fstack_5fsize',['configMINIMAL_STACK_SIZE',['../_free_r_t_o_s_config_8h.html#a6c534a6cf8a00528fe0be42083484f9a',1,'FreeRTOSConfig.h']]],
  ['configprio_5fbits',['configPRIO_BITS',['../_free_r_t_o_s_config_8h.html#a5796db11ec6f9aa38d017d2ac393c5ba',1,'FreeRTOSConfig.h']]],
  ['configqueue_5fregistry_5fsize',['configQUEUE_REGISTRY_SIZE',['../_free_r_t_o_s_config_8h.html#aa4b5138c4e42a180f0abd4f2455f90fb',1,'FreeRTOSConfig.h']]],
  ['configsupport_5fdynamic_5fallocation',['configSUPPORT_DYNAMIC_ALLOCATION',['../_free_r_t_o_s_config_8h.html#afa3f8d6510699d972c9af08fa41f66dc',1,'FreeRTOSConfig.h']]],
  ['configsupport_5fstatic_5fallocation',['configSUPPORT_STATIC_ALLOCATION',['../_free_r_t_o_s_config_8h.html#a1d6797a53b62d501b095f964036a7ac4',1,'FreeRTOSConfig.h']]],
  ['configtick_5frate_5fhz',['configTICK_RATE_HZ',['../_free_r_t_o_s_config_8h.html#a2f0258dd1e3b877e5bc013be54c2db6a',1,'FreeRTOSConfig.h']]],
  ['configtimer_5fqueue_5flength',['configTIMER_QUEUE_LENGTH',['../_free_r_t_o_s_config_8h.html#abb9aa0f31c1f3b14a15083a3c6120918',1,'FreeRTOSConfig.h']]],
  ['configtimer_5ftask_5fpriority',['configTIMER_TASK_PRIORITY',['../_free_r_t_o_s_config_8h.html#a05c75ff9029ba3f0ab5bde9196f1e873',1,'FreeRTOSConfig.h']]],
  ['configtimer_5ftask_5fstack_5fdepth',['configTIMER_TASK_STACK_DEPTH',['../_free_r_t_o_s_config_8h.html#aed7c7ebcdee603583a55e8ce04e55841',1,'FreeRTOSConfig.h']]],
  ['configtotal_5fheap_5fsize',['configTOTAL_HEAP_SIZE',['../_free_r_t_o_s_config_8h.html#a9f213227674effff0122a75d94d87938',1,'FreeRTOSConfig.h']]],
  ['configuse_5f16_5fbit_5fticks',['configUSE_16_BIT_TICKS',['../_free_r_t_o_s_config_8h.html#aac311ed9b9e5ae4d2d9648b33a24acce',1,'FreeRTOSConfig.h']]],
  ['configuse_5fco_5froutines',['configUSE_CO_ROUTINES',['../_free_r_t_o_s_config_8h.html#a57990715eb06402474b8b47e1d562616',1,'FreeRTOSConfig.h']]],
  ['configuse_5fcounting_5fsemaphores',['configUSE_COUNTING_SEMAPHORES',['../_free_r_t_o_s_config_8h.html#a55778995203c57369d2fbfb10224943d',1,'FreeRTOSConfig.h']]],
  ['configuse_5fidle_5fhook',['configUSE_IDLE_HOOK',['../_free_r_t_o_s_config_8h.html#ac637ae45863c19fa2e919db0ed49301f',1,'FreeRTOSConfig.h']]],
  ['configuse_5fmalloc_5ffailed_5fhook',['configUSE_MALLOC_FAILED_HOOK',['../_free_r_t_o_s_config_8h.html#abdf48e7c9cf513f083aa9cbed0dd7cd7',1,'FreeRTOSConfig.h']]],
  ['configuse_5fmutexes',['configUSE_MUTEXES',['../_free_r_t_o_s_config_8h.html#a543bf3c79008974cc1d36bab51d94fbf',1,'FreeRTOSConfig.h']]],
  ['configuse_5fport_5foptimised_5ftask_5fselection',['configUSE_PORT_OPTIMISED_TASK_SELECTION',['../_free_r_t_o_s_config_8h.html#aebb8c3a87d591f60f974772be0ee798d',1,'FreeRTOSConfig.h']]],
  ['configuse_5fpreemption',['configUSE_PREEMPTION',['../_free_r_t_o_s_config_8h.html#adde83486022745409c40605922b0bdd6',1,'FreeRTOSConfig.h']]],
  ['configuse_5frecursive_5fmutexes',['configUSE_RECURSIVE_MUTEXES',['../_free_r_t_o_s_config_8h.html#a9fe02d866cb1c4fbaa0c3de79f53d42d',1,'FreeRTOSConfig.h']]],
  ['configuse_5ftick_5fhook',['configUSE_TICK_HOOK',['../_free_r_t_o_s_config_8h.html#a23c5922c077106fad3f70b54d9071466',1,'FreeRTOSConfig.h']]],
  ['configuse_5ftimers',['configUSE_TIMERS',['../_free_r_t_o_s_config_8h.html#ac342ae309b0c53828d2ecad3e6de355b',1,'FreeRTOSConfig.h']]],
  ['configuse_5ftrace_5ffacility',['configUSE_TRACE_FACILITY',['../_free_r_t_o_s_config_8h.html#a27f5ee137dc9f125681a31f0b0a4b3be',1,'FreeRTOSConfig.h']]],
  ['correlator_5fen',['CORRELATOR_EN',['../uvp__driver_8h.html#aee94dd6b74c4125c7ec085c883591d5d',1,'uvp_driver.h']]],
  ['cota_5fbox_5fexti_5firqn',['COTA_BOX_EXTI_IRQn',['../main_8h.html#a4513ed2f962357dcd12d05c9726b4b61',1,'main.h']]],
  ['cota_5fbox_5fgpio_5fport',['COTA_BOX_GPIO_Port',['../main_8h.html#add629aa45d397f2f0d9d0b3d49dd0d0d',1,'main.h']]],
  ['cota_5fbox_5fpin',['COTA_BOX_Pin',['../main_8h.html#a3a401d7c7fedba86dd8aadc519c1c244',1,'main.h']]],
  ['cota_5fcomm_5ffreq_5fspacing_5fhz',['COTA_COMM_FREQ_SPACING_HZ',['../_cota_frequencies_8h.html#ae3df7a4f84c83e9905ef1e422964056e',1,'CotaFrequencies.h']]],
  ['cota_5fcomm_5ffreq_5fspacing_5fmhz',['COTA_COMM_FREQ_SPACING_MHZ',['../_cota_frequencies_8h.html#a42feeed58b90a68c9cef2e41741cefe4',1,'CotaFrequencies.h']]],
  ['cota_5ferror_5fmap_5fsize',['COTA_ERROR_MAP_SIZE',['../error__task_8c.html#a9eff5328bee8b8587de1c6c89ee83d70',1,'error_task.c']]],
  ['cota_5fmax_5fcomm_5fchannel',['COTA_MAX_COMM_CHANNEL',['../_cota_frequencies_8h.html#a0e576940719473d3b5cae19c860d11cd',1,'CotaFrequencies.h']]],
  ['cota_5fmax_5fcomm_5ffreq_5fhz',['COTA_MAX_COMM_FREQ_HZ',['../_cota_frequencies_8h.html#a6f96fbf667582103ef307ed869ca42c0',1,'CotaFrequencies.h']]],
  ['cota_5fmax_5fcomm_5ffreq_5fmhz',['COTA_MAX_COMM_FREQ_MHZ',['../_cota_frequencies_8h.html#a12003bcfa49c5407638aee7f076ef7cd',1,'CotaFrequencies.h']]],
  ['cota_5fmin_5fcomm_5fchannel',['COTA_MIN_COMM_CHANNEL',['../_cota_frequencies_8h.html#aa2d8f3b8bca0e38c1f8db93afcd8a872',1,'CotaFrequencies.h']]],
  ['cota_5fmin_5fcomm_5ffreq_5fhz',['COTA_MIN_COMM_FREQ_HZ',['../_cota_frequencies_8h.html#ac0df2be7470ffd3da9d7b3b633377b1e',1,'CotaFrequencies.h']]],
  ['cota_5fmin_5fcomm_5ffreq_5fmhz',['COTA_MIN_COMM_FREQ_MHZ',['../_cota_frequencies_8h.html#a6739ddd5e0a670c96452c62cb2ff2968',1,'CotaFrequencies.h']]],
  ['cota_5fpacked',['COTA_PACKED',['../_cota_compiler_8h.html#a68798e6bacc9c0ac7fd032213b7db0f7',1,'CotaCompiler.h']]],
  ['cota_5fstatic_5fassert',['COTA_STATIC_ASSERT',['../_cota_compiler_8h.html#a9eca1c1a4aabd014fe86117c6c0f6ea7',1,'CotaCompiler.h']]],
  ['cota_5fuart_5fport',['COTA_UART_PORT',['../uart__driver_8c.html#a0b63349af5e591a1df6b7c2eb9ac0a89',1,'uart_driver.c']]],
  ['cpen_5fidle',['CPEN_IDLE',['../uvp__driver_8h.html#aa439c4e2ff1c52a1f1770c45593b1f99',1,'uvp_driver.h']]],
  ['cpen_5fsendpwr',['CPEN_SENDPWR',['../uvp__driver_8h.html#ad574c0cd99bff74cbc868cb1afffb2e3',1,'uvp_driver.h']]],
  ['cpival',['CPIVAL',['../uvp__driver_8h.html#aeb9869cd24910adb1ded0387f805790d',1,'uvp_driver.h']]],
  ['cplk',['CPLK',['../uvp__driver_8h.html#ad05878fa5854536bc89f4455459679e0',1,'uvp_driver.h']]],
  ['cplken',['CPLKEN',['../uvp__driver_8h.html#af3f4c354a4c5e69dc1e9e72ba4866718',1,'uvp_driver.h']]],
  ['crc32_5fchecksum_5fsize_5fbytes',['CRC32_CHECKSUM_SIZE_BYTES',['../nvm__driver_8h.html#acdfd44c7c3ac52f02bef2b50da609c75',1,'nvm_driver.h']]],
  ['crc32_5fpoly',['CRC32_POLY',['../crc_8c.html#a39e1b7cfc3c0d80ea907e6486d288796',1,'crc.c']]],
  ['crc_5ffield_5fsize',['CRC_FIELD_SIZE',['../raspberry__pi_8h.html#a675c19b75f90406545022c9d2952ecf1',1,'raspberry_pi.h']]],
  ['crc_5fofst_5ffield_5fsize',['CRC_OFST_FIELD_SIZE',['../raspberry__pi_8h.html#a84b190d2bc87ded3ca2fbf0b835a0bb6',1,'raspberry_pi.h']]],
  ['cs_5fassert',['CS_ASSERT',['../proxy__fw__update__driver_8c.html#ae88f5cc834e5acaf108d21face11c05d',1,'proxy_fw_update_driver.c']]],
  ['cs_5fdeassert',['CS_DEASSERT',['../proxy__fw__update__driver_8c.html#a779d18fe15638464288b9b1167ec64f6',1,'proxy_fw_update_driver.c']]],
  ['csb_5fadd0_5fgpio_5fport',['CSB_ADD0_GPIO_Port',['../main_8h.html#a380d6cb2e4a0732a84c8ca92867af34e',1,'main.h']]],
  ['csb_5fadd0_5fpin',['CSB_ADD0_Pin',['../main_8h.html#a2bc85af4ea4a0d21517a3636a7a9344b',1,'main.h']]],
  ['csb_5fadd1_5fgpio_5fport',['CSB_ADD1_GPIO_Port',['../main_8h.html#ae61d6df51d64e1df35c1336c3d5268ff',1,'main.h']]],
  ['csb_5fadd1_5fpin',['CSB_ADD1_Pin',['../main_8h.html#a73c1044068bea60ddeeb37ed9826816c',1,'main.h']]],
  ['csb_5fadd2_5fgpio_5fport',['CSB_ADD2_GPIO_Port',['../main_8h.html#a91abb515cf37d7b6de0aba6114c44543',1,'main.h']]],
  ['csb_5fadd2_5fpin',['CSB_ADD2_Pin',['../main_8h.html#ad7a326c8f6238f9305f6622aa1482a6e',1,'main.h']]],
  ['csb_5fadd3_5fgpio_5fport',['CSB_ADD3_GPIO_Port',['../main_8h.html#a84186e5deac9b83d26a554957c5bb2c7',1,'main.h']]],
  ['csb_5fadd3_5fpin',['CSB_ADD3_Pin',['../main_8h.html#ab0ea4e0ad4fe37bdc823ef60a22a2414',1,'main.h']]],
  ['csi_5fvalue',['CSI_VALUE',['../stm32h7xx__hal__conf_8h.html#a4dcbff36a4b1cfd045c01d59084255d0',1,'stm32h7xx_hal_conf.h']]],
  ['ctrl_5fcnt_5fsem_5fmax',['CTRL_CNT_SEM_MAX',['../ctrl__task_8c.html#aa43b99fcb41ddb5d10ef54709a363f40',1,'ctrl_task.c']]],
  ['ctrl_5fcnt_5fsem_5fmin',['CTRL_CNT_SEM_MIN',['../ctrl__task_8c.html#a484712f7f494e1a928bf568b9126b83f',1,'ctrl_task.c']]],
  ['ctrl_5fintf_5fsize',['CTRL_INTF_SIZE',['../ctrl__interface_8c.html#af5073ebe5f7fdcbf21172bd9f4373229',1,'ctrl_interface.c']]],
  ['ctrl_5fno_5fdelay',['CTRL_NO_DELAY',['../ctrl__interface_8h.html#a7e1109a9dc781027634fca9c2e967202',1,'ctrl_interface.h']]],
  ['ctrl_5ftask_5fmax_5fcmd_5fsize',['CTRL_TASK_MAX_CMD_SIZE',['../ctrl__task_8h.html#ae35a4da49e2f67654d51556b2911b64f',1,'ctrl_task.h']]],
  ['ctrl_5ftask_5fmax_5fmsg_5fsize',['CTRL_TASK_MAX_MSG_SIZE',['../ctrl__task_8h.html#a09dc8897f0937fc0b525ed92af164b12',1,'ctrl_task.h']]],
  ['ctrl_5ftask_5fresponse_5fsize',['CTRL_TASK_RESPONSE_SIZE',['../ctrl__task_8h.html#ac444b6930966766346291b938b1bc5a7',1,'ctrl_task.h']]]
];
