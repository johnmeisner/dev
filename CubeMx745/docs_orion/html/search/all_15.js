var searchData=
[
  ['val',['val',['../struct___msg_set_config_param__t.html#a624e72e514cfa5653db60eefbdd0481e',1,'_MsgSetConfigParam_t::val()'],['../struct___msg_sys_clk_write__t.html#ae1f13e96d928504e11ec4da744f67f9b',1,'_MsgSysClkWrite_t::val()'],['../struct___resp_get_config_param__t.html#a624e72e514cfa5653db60eefbdd0481e',1,'_RespGetConfigParam_t::val()'],['../struct___resp_uvp_read__t.html#a97ad06b300d95ba1d023dcb1e825335f',1,'_RespUvpRead_t::val()'],['../struct___resp_sys_clk_read__t.html#ae1f13e96d928504e11ec4da744f67f9b',1,'_RespSysClkRead_t::val()'],['../struct___resp_sys_clk_dump__t.html#ae1f13e96d928504e11ec4da744f67f9b',1,'_RespSysClkDump_t::val()'],['../struct___uvp_reg_write__t.html#a97ad06b300d95ba1d023dcb1e825335f',1,'_UvpRegWrite_t::val()']]],
  ['value',['value',['../struct___msg_power_monitor_enable__t.html#a0376be5904d0dd864b7d97c9ce1295ab',1,'_MsgPowerMonitorEnable_t']]],
  ['vapplicationidlehook',['vApplicationIdleHook',['../freertos_8c.html#aed09350c45d767229fd9e382244de3f1',1,'freertos.c']]],
  ['vapplicationmallocfailedhook',['vApplicationMallocFailedHook',['../freertos_8c.html#a92b5bd8c1938973f010621d23aa88c3d',1,'freertos.c']]],
  ['var_5fqueue_5freserved_5fspace',['VAR_QUEUE_RESERVED_SPACE',['../variable__queue_8c.html#aa50ef71212b73018b0113533dffbbb0e',1,'variable_queue.c']]],
  ['variable_5fparam_5fcount',['VARIABLE_PARAM_COUNT',['../system__commands_8c.html#aa9a5fbeebcc21f0514a2489a43a57acc',1,'system_commands.c']]],
  ['variable_5fqueue_2ec',['variable_queue.c',['../variable__queue_8c.html',1,'']]],
  ['variable_5fqueue_2eh',['variable_queue.h',['../variable__queue_8h.html',1,'']]],
  ['variablequeuecontrol_5ft',['VariableQueueControl_t',['../variable__queue_8h.html#a1e8e43b5cf2af5b7f55d6870b5459a40',1,'variable_queue.h']]],
  ['variablequeuecreatestatic',['VariableQueueCreateStatic',['../variable__queue_8c.html#a1ad05a0c5e4d0578d5d976d9c33a1fdb',1,'VariableQueueCreateStatic(uint16_t storageSizeBytes, uint8_t *storageArea, VariableQueueControl_t *queueControl):&#160;variable_queue.c'],['../variable__queue_8h.html#a30de488afaba4c1d29650e53e9af3390',1,'VariableQueueCreateStatic(uint16_t bufferSizeBytes, uint8_t *storageArea, VariableQueueControl_t *queueControl):&#160;variable_queue.c']]],
  ['variablequeuereceive',['VariableQueueReceive',['../variable__queue_8c.html#a5b884f1b7fca4f496719b2f21164c93f',1,'VariableQueueReceive(VariableQueueControl_t *queueControl, uint8_t *rxData, uint16_t length, TickType_t ticksToWait):&#160;variable_queue.c'],['../variable__queue_8h.html#a5b884f1b7fca4f496719b2f21164c93f',1,'VariableQueueReceive(VariableQueueControl_t *queueControl, uint8_t *rxData, uint16_t length, TickType_t ticksToWait):&#160;variable_queue.c']]],
  ['variablequeuesend',['VariableQueueSend',['../variable__queue_8c.html#a72922f7fc8eeaf70434776f335c1ce62',1,'VariableQueueSend(VariableQueueControl_t *queueControl, uint8_t *txData, uint16_t length):&#160;variable_queue.c'],['../variable__queue_8h.html#a72922f7fc8eeaf70434776f335c1ce62',1,'VariableQueueSend(VariableQueueControl_t *queueControl, uint8_t *txData, uint16_t length):&#160;variable_queue.c']]],
  ['variablequeuespacesavailable',['VariableQueueSpacesAvailable',['../variable__queue_8c.html#a744d61fc72a9aa8a97a05dbca98fb710',1,'VariableQueueSpacesAvailable(VariableQueueControl_t *queueControl):&#160;variable_queue.c'],['../variable__queue_8h.html#a744d61fc72a9aa8a97a05dbca98fb710',1,'VariableQueueSpacesAvailable(VariableQueueControl_t *queueControl):&#160;variable_queue.c']]],
  ['variant_5f1_5fclock',['VARIANT_1_CLOCK',['../sysclk__driver_8c.html#a09ef0db3087b466a8cb2729de7d263f8',1,'sysclk_driver.c']]],
  ['vcobuffen_5fidle',['VCOBUFFEN_IDLE',['../uvp__driver_8h.html#ae67e4c8aea9157af2e7277b41c8a572a',1,'uvp_driver.h']]],
  ['vcobuffen_5fphdetect',['VCOBUFFEN_PHDETECT',['../uvp__driver_8h.html#a0776739dd752fc0e8f3b531a54598928',1,'uvp_driver.h']]],
  ['vcobuffen_5frxrdy',['VCOBUFFEN_RXRDY',['../uvp__driver_8h.html#a898c995ee8cd00ed4627868e1b0fc9c4',1,'uvp_driver.h']]],
  ['vcobuffen_5fsendpwr',['VCOBUFFEN_SENDPWR',['../uvp__driver_8h.html#a9d3dde833d8efdbe9b93737f0e02d3bd',1,'uvp_driver.h']]],
  ['vcobuffen_5ftxrdy',['VCOBUFFEN_TXRDY',['../uvp__driver_8h.html#a17f07a35d880a7233204fd152d7fbaf3',1,'uvp_driver.h']]],
  ['vcobuffibst',['VCOBUFFIBST',['../uvp__driver_8h.html#a52a83e7b67c0fff8b4b172d4da13918c',1,'uvp_driver.h']]],
  ['vcocfs',['VCOCFS',['../uvp__driver_8h.html#ab1b438e188942d8d8163b8a70e579bc0',1,'uvp_driver.h']]],
  ['vcocfs_5fvalue_5fend',['VCOCFS_VALUE_END',['../uvp__driver_8c.html#abe0561a4cb5c719f20aa155d64af3e17',1,'uvp_driver.c']]],
  ['vcocfs_5fvalue_5fstart',['VCOCFS_VALUE_START',['../uvp__driver_8c.html#a52cac1d404398c9fa35a939a0f1b680a',1,'uvp_driver.c']]],
  ['vcoen_5fsendpwr',['VCOEN_SENDPWR',['../uvp__driver_8h.html#a0f7dab54e0f6dd127b72eb63cc572806',1,'uvp_driver.h']]],
  ['vcoibst',['VCOIBST',['../uvp__driver_8h.html#a23bcdddee0d16e6da6730bb8486fc280',1,'uvp_driver.h']]],
  ['vcoucfs',['VCOUCFS',['../uvp__driver_8h.html#aa8f750c82b5d69f90aacc2176ec450bc',1,'uvp_driver.h']]],
  ['vdd_5fvalue',['VDD_VALUE',['../stm32h7xx__hal__conf_8h.html#aae550dad9f96d52cfce5e539adadbbb4',1,'stm32h7xx_hal_conf.h']]],
  ['version',['version',['../struct___client__t.html#ab6d7b6f8c2ceaba7acda80aaf05f4899',1,'_Client_t::version()'],['../struct___ctrl_resp__t.html#a0f7e072ea54fe31b29d099f686242dad',1,'_CtrlResp_t::version()'],['../struct___nvm_client_data__t.html#ab6d7b6f8c2ceaba7acda80aaf05f4899',1,'_NvmClientData_t::version()'],['../struct___client_slot_data__t.html#ab6d7b6f8c2ceaba7acda80aaf05f4899',1,'_ClientSlotData_t::version()'],['../struct___fw_image_header_ota__t.html#acd99bb05ca015e7d74448acb1deba7ca',1,'_FwImageHeaderOta_t::version()'],['../struct___fw_image_identifier_factory__t.html#acd99bb05ca015e7d74448acb1deba7ca',1,'_FwImageIdentifierFactory_t::version()'],['../proxy_client_msg_inf_8h.html#ab6d7b6f8c2ceaba7acda80aaf05f4899',1,'version():&#160;proxyClientMsgInf.h'],['../proxy_host_msg_inf_8h.html#ab6d7b6f8c2ceaba7acda80aaf05f4899',1,'version():&#160;proxyHostMsgInf.h']]],
  ['version_5ffrom_5fsource_5fcontrol_2eh',['version_from_source_control.h',['../version__from__source__control_8h.html',1,'']]],
  ['voteenabled',['VoteEnabled',['../gather__and__vote__task_8c.html#a961b99a03a0acaf56123a6da4c56b80f',1,'VoteEnabled(void):&#160;gather_and_vote_task.c'],['../gather__and__vote__task_8h.html#a961b99a03a0acaf56123a6da4c56b80f',1,'VoteEnabled(void):&#160;gather_and_vote_task.c']]],
  ['votefailed',['VoteFailed',['../gather__and__vote__task_8h.html#a5a1355ac5dc05ddf1f211c1a9d02cbff',1,'gather_and_vote_task.h']]],
  ['vportsvchandler',['vPortSVCHandler',['../_free_r_t_o_s_config_8h.html#ad43047b3ea0a146673e30637488bf754',1,'FreeRTOSConfig.h']]]
];
