var searchData=
[
  ['safesysclkwritereg',['SafeSysClkWriteReg',['../sysclk__driver_8c.html#ae97b457c6a0fb6615260b11afe1949ca',1,'SafeSysClkWriteReg(uint16_t addr, uint8_t regVal):&#160;sysclk_driver.c'],['../sysclk__driver_8h.html#ae97b457c6a0fb6615260b11afe1949ca',1,'SafeSysClkWriteReg(uint16_t addr, uint8_t regVal):&#160;sysclk_driver.c']]],
  ['samplebeaconcmdcallback',['SampleBeaconCmdCallback',['../system__commands_8c.html#a16a5c531f46984d4b9f8cdcaed6ed91e',1,'system_commands.c']]],
  ['saveclient',['SaveClient',['../client_8c.html#a4cdedf095b092cc822520b67eb4df2f2',1,'SaveClient(Client_t client):&#160;client_manager.c'],['../client__interface_8c.html#a4cdedf095b092cc822520b67eb4df2f2',1,'SaveClient(Client_t client):&#160;client_manager.c'],['../client__manager_8c.html#aee95df20a61046223df23b72c37419bd',1,'SaveClient(Client_t client):&#160;client_manager.c'],['../cqt__scheduler_8c.html#a4cdedf095b092cc822520b67eb4df2f2',1,'SaveClient(Client_t client):&#160;client_manager.c']]],
  ['sden_5fsendpwr',['SDEN_SENDPWR',['../uvp__driver_8h.html#a699e59088629b47ecfad6f5e78c5c0cb',1,'uvp_driver.h']]],
  ['searchtree',['SearchTree',['../client__avl__tree_8c.html#a433bef6f5f71aa68643a350692bee059',1,'SearchTree(ExtdAddr_t key, ClientNode_t *node, Client_t *oclient):&#160;client_avl_tree.c'],['../client__avl__tree_8h.html#a433bef6f5f71aa68643a350692bee059',1,'SearchTree(ExtdAddr_t key, ClientNode_t *node, Client_t *oclient):&#160;client_avl_tree.c']]],
  ['searchtreeshortid',['SearchTreeShortId',['../client__avl__tree_8c.html#a1e92f29c7582be38e8d06dd360466e02',1,'SearchTreeShortId(ShrtAddr_t key, ClientNode_t *node, Client_t *oclient):&#160;client_avl_tree.c'],['../client__avl__tree_8h.html#a1e92f29c7582be38e8d06dd360466e02',1,'SearchTreeShortId(ShrtAddr_t key, ClientNode_t *node, Client_t *oclient):&#160;client_avl_tree.c']]],
  ['seconds',['seconds',['../union___timestamp__t.html#a6d5694839ec935781627e5c52de21fda',1,'_Timestamp_t']]],
  ['segmid',['segmId',['../_cota_common_types_8h.html#a724a16a5f8df874b4a67c6e2a41a28a1',1,'CotaCommonTypes.h']]],
  ['sel_5fphase_5fconjugate',['SEL_PHASE_CONJUGATE',['../uvp__driver_8h.html#a5cfdba6c1b8685987c57cb117e2d5de4',1,'uvp_driver.h']]],
  ['selectmultichannel',['selectMultiChannel',['../i2c__multiplex_8c.html#ab6bb8ef51047f7a450afc5398122d8b4',1,'i2c_multiplex.c']]],
  ['selectuvpcmdcallback',['SelectUvpCmdCallback',['../system__commands_8c.html#a3cf0fa1d753a036f9a9e2e2e403b9b07',1,'system_commands.c']]],
  ['send_5fpower_5frdy',['SEND_POWER_RDY',['../uvp__driver_8h.html#a30ded0b22f090e2f58749459b8f9d6bc',1,'uvp_driver.h']]],
  ['senddisc',['sendDisc',['../struct___ctrl_msg__t.html#a76e46819369ed59471e48da37aedfaaf',1,'_CtrlMsg_t']]],
  ['senddisccmdcallback',['SendDiscCmdCallback',['../system__commands_8c.html#a1597b223d01359238dea00ea5e1462ca',1,'system_commands.c']]],
  ['seq_5fstart_5fdelay_5f0_5fs',['SEQ_START_DELAY_0_s',['../fan__driver_8c.html#ab7cc6980e6a79d7375774c14d168b220ae8bff4bd5ce491903e81ee601b2a0bd7',1,'fan_driver.c']]],
  ['seq_5fstart_5fdelay_5f1_5fs',['SEQ_START_DELAY_1_s',['../fan__driver_8c.html#ab7cc6980e6a79d7375774c14d168b220a1a2ab7c72dd62f16258a0abb4f06dc27',1,'fan_driver.c']]],
  ['seq_5fstart_5fdelay_5f250_5fms',['SEQ_START_DELAY_250_ms',['../fan__driver_8c.html#ab7cc6980e6a79d7375774c14d168b220af883b85ab9477d55a2011b9dc5b6c4a0',1,'fan_driver.c']]],
  ['seq_5fstart_5fdelay_5f2_5fs',['SEQ_START_DELAY_2_s',['../fan__driver_8c.html#ab7cc6980e6a79d7375774c14d168b220a4b7b3c89ac43bfd62fe67bc0c01bb93e',1,'fan_driver.c']]],
  ['seq_5fstart_5fdelay_5f4a_5fs',['SEQ_START_DELAY_4a_s',['../fan__driver_8c.html#ab7cc6980e6a79d7375774c14d168b220a09c976c2c72345785bf8b12091b76076',1,'fan_driver.c']]],
  ['seq_5fstart_5fdelay_5f500_5fms',['SEQ_START_DELAY_500_ms',['../fan__driver_8c.html#ab7cc6980e6a79d7375774c14d168b220abc514b42008694045edb3991a00a5ea5',1,'fan_driver.c']]],
  ['setallfansspeed',['setAllFansSpeed',['../fan__driver_8c.html#a86ce2efb758175b3d81f4013c195e570',1,'fan_driver.c']]],
  ['setambon',['setAmbOn',['../struct___ctrl_msg__t.html#abf66266e145299e05e20dedad0dbff4a',1,'_CtrlMsg_t']]],
  ['setamboncmdcallback',['SetAmbOnCmdCallback',['../system__commands_8c.html#ae5b96e9571df62daefbf480b58a90811',1,'system_commands.c']]],
  ['setambpowerlevels',['SetAmbPowerLevels',['../power__level_8c.html#ac3ed024ca85e878cdb29f65c65e17d0a',1,'SetAmbPowerLevels(AmbMask_t ambMask, uint16_t requestedPowerLevel, uint16_t *actualPowerLevel):&#160;power_level.c'],['../power__level_8h.html#ac3ed024ca85e878cdb29f65c65e17d0a',1,'SetAmbPowerLevels(AmbMask_t ambMask, uint16_t requestedPowerLevel, uint16_t *actualPowerLevel):&#160;power_level.c']]],
  ['setamuenablechanges',['SetAmuEnableChanges',['../uvp__driver_8c.html#a1967fb003f88fd835fef136823e1265b',1,'SetAmuEnableChanges(AmuMask_t *pDataOld, AmuMask_t *pDataNew, uint16_t numValues, AmbMask_t ambMask):&#160;uvp_driver.c'],['../uvp__driver_8h.html#a1967fb003f88fd835fef136823e1265b',1,'SetAmuEnableChanges(AmuMask_t *pDataOld, AmuMask_t *pDataNew, uint16_t numValues, AmbMask_t ambMask):&#160;uvp_driver.c']]],
  ['setchargingbeginforautocal',['SetChargingBeginForAutoCal',['../calibrator_8c.html#a9e885b19e57f769bba1bea5116467ffa',1,'SetChargingBeginForAutoCal(void):&#160;calibrator.c'],['../calibrator_8h.html#a9e885b19e57f769bba1bea5116467ffa',1,'SetChargingBeginForAutoCal(void):&#160;calibrator.c']]],
  ['setclockconfig',['setClockConfig',['../struct___ctrl_msg__t.html#a4c82a9eb881f095b43d972148d38ce43',1,'_CtrlMsg_t']]],
  ['setcommchannel',['setCommChannel',['../struct___ctrl_msg__t.html#aa39be05599ec145568af0a4f73532186',1,'_CtrlMsg_t']]],
  ['setcommchannelcmdcallback',['SetCommChannelCmdCallback',['../system__commands_8c.html#abe0b6013e5d34c7b28ce81f38c733696',1,'system_commands.c']]],
  ['setconfigparam',['setConfigParam',['../struct___ctrl_msg__t.html#a802842056ffc41b81c2459d05212d743',1,'_CtrlMsg_t']]],
  ['setdebugmode',['SetDebugMode',['../generic__sm_8c.html#afd689e69879134068ea41e2be9063192',1,'SetDebugMode(bool val):&#160;generic_sm.c'],['../generic__sm_8h.html#afd689e69879134068ea41e2be9063192',1,'SetDebugMode(bool val):&#160;generic_sm.c']]],
  ['setemergencyfanon',['SetEmergencyFanOn',['../fan__driver_8c.html#ac06452163337e86c135447c6a5ae3c4c',1,'SetEmergencyFanOn(bool fullFanOn):&#160;fan_driver.c'],['../fan__driver_8h.html#ac06452163337e86c135447c6a5ae3c4c',1,'SetEmergencyFanOn(bool fullFanOn):&#160;fan_driver.c']]],
  ['setfansfull',['SetFansFull',['../fan__driver_8c.html#a11d04fac62953924baec3ec8b2238a53',1,'SetFansFull(bool enable):&#160;fan_driver.c'],['../fan__driver_8h.html#a11d04fac62953924baec3ec8b2238a53',1,'SetFansFull(bool enable):&#160;fan_driver.c']]],
  ['setfansfullcallback',['SetFansFullCallback',['../system__commands_8c.html#a8ffe02e539fe1826ff97d3531c8af581',1,'system_commands.c']]],
  ['setfanspeed',['setFanSpeed',['../fan__driver_8c.html#a039e264bec7ead3d9b8a29c5db53961e',1,'fan_driver.c']]],
  ['setfantachtargetcount',['setFanTachTargetCount',['../fan__driver_8c.html#a225fd0b046448513b3ef35fa79f3fd44',1,'fan_driver.c']]],
  ['setledstate',['setLedState',['../proxy_host_msg_inf_8h.html#af6f691c327775156e6c2a461b7bfafeb',1,'proxyHostMsgInf.h']]],
  ['setmanualcalibrationflag',['SetManualCalibrationFlag',['../calibrator_8c.html#a9f5299c334df1fe479b7d91fe8267430',1,'SetManualCalibrationFlag(bool enable):&#160;calibrator.c'],['../calibrator_8h.html#a9f5299c334df1fe479b7d91fe8267430',1,'SetManualCalibrationFlag(bool enable):&#160;calibrator.c']]],
  ['setnvmconfigcallback',['SetNvmConfigCallback',['../system__commands_8c.html#a6d0eb35fd78ed32a1393ae462b457c5f',1,'system_commands.c']]],
  ['setpowerlevel',['setPowerLevel',['../struct___ctrl_msg__t.html#a78d1c4f3efeffb33f5d5266bff13128c',1,'_CtrlMsg_t::setPowerLevel()'],['../struct___ctrl_resp__t.html#a69c3b17a2bf6af987d2f189fde903abe',1,'_CtrlResp_t::setPowerLevel()']]],
  ['setpowerlevelcmdcallback',['SetPowerLevelCmdCallback',['../system__commands_8c.html#a968cf980047bcbacd3874d3affe19cf4',1,'system_commands.c']]],
  ['setpuall',['setPuAll',['../struct___ctrl_msg__t.html#af00f41a1b93412eb539afc0102f809c0',1,'_CtrlMsg_t']]],
  ['setpuallcmdcallback',['SetPuAllCmdCallback',['../system__commands_8c.html#a873f976f79d10658983dee92bbc72419',1,'system_commands.c']]],
  ['setpuon',['setPuOn',['../struct___ctrl_msg__t.html#a11aee04354f708d052ed8f3925c353a6',1,'_CtrlMsg_t']]],
  ['setpuoncmdcallback',['SetPuOnCmdCallback',['../system__commands_8c.html#a8a038f7e6a706c0c0a5331749d567a7b',1,'system_commands.c']]],
  ['setquerytypecallback',['SetQueryTypeCallback',['../system__commands_8c.html#a118eabf1ef50823bb166bf208e0bb695',1,'system_commands.c']]],
  ['setrcvrconfig',['setRcvrConfig',['../struct___ctrl_msg__t.html#a26808bedf400dbb8e175df530748a455',1,'_CtrlMsg_t']]],
  ['setresetethernetclock',['SetResetEthernetClock',['../sysclk__driver_8c.html#ab3e799ba9dff4d8cd08c2b5f0d970dba',1,'SetResetEthernetClock(bool setReset):&#160;sysclk_driver.c'],['../sysclk__driver_8h.html#aeec814016c7f0463d68583a7922b54e1',1,'SetResetEthernetClock(bool setreset):&#160;sysclk_driver.c']]],
  ['setrssifilterencmdcallback',['SetRssiFilterEnCmdCallback',['../system__commands_8c.html#a3b89a25b98c0538d87a1136b586e2deb',1,'system_commands.c']]],
  ['setrssithresh',['SetRssiThresh',['../gather__and__vote__task_8c.html#a691aa7dd730665b08830f8bee07f0de3',1,'SetRssiThresh(MsgSetRssiThreshold_t *rssiThresh):&#160;gather_and_vote_task.c'],['../gather__and__vote__task_8h.html#a691aa7dd730665b08830f8bee07f0de3',1,'SetRssiThresh(MsgSetRssiThreshold_t *rssiThresh):&#160;gather_and_vote_task.c']]],
  ['setrssithreshcmdcallback',['SetRssiThreshCmdCallback',['../system__commands_8c.html#a35341b5d8272e111926585c86b753b93',1,'system_commands.c']]],
  ['setrssithreshold',['setRssiThreshold',['../struct___ctrl_msg__t.html#a6fd4fe78181f80c0bd6395b82141e26e',1,'_CtrlMsg_t']]],
  ['setrssivoteen',['SetRssiVoteEn',['../gather__and__vote__task_8c.html#a01e58efa36cd115db35bf4d24010b499',1,'SetRssiVoteEn(bool enable):&#160;gather_and_vote_task.c'],['../gather__and__vote__task_8h.html#a01e58efa36cd115db35bf4d24010b499',1,'SetRssiVoteEn(bool enable):&#160;gather_and_vote_task.c']]],
  ['setrx',['setRx',['../struct___ctrl_msg__t.html#afb6c04b06733bef63b0df9213cd23dc3',1,'_CtrlMsg_t']]],
  ['setrxcmdcallback',['SetRxCmdCallback',['../system__commands_8c.html#a3de35c8df965a1f893deb82ef2c832bc',1,'system_commands.c']]],
  ['setspienable',['setSpiEnable',['../struct___ctrl_msg__t.html#a90fa80aeb1f77166cb858e31ea0559a2',1,'_CtrlMsg_t']]],
  ['setspiencmdcallback',['SetSpiEnCmdCallback',['../system__commands_8c.html#af70dd374cb9afa6dfbc5d8d321a80f5f',1,'system_commands.c']]],
  ['setsysclkcmdcallback',['SetSysClkCmdCallback',['../system__commands_8c.html#ad45ab0a238f9415d3510bd7c6be08df6',1,'system_commands.c']]],
  ['setsystemlevel',['SetSystemLevel',['../log_8c.html#a5b49a7dae5addbdddc6cd5cb73005999',1,'SetSystemLevel(LogLevel_t level):&#160;log.c'],['../log_8h.html#a5b49a7dae5addbdddc6cd5cb73005999',1,'SetSystemLevel(LogLevel_t level):&#160;log.c']]],
  ['settpsactiveforautocal',['SetTpsActiveForAutoCal',['../calibrator_8c.html#a2c11370f140453dcc0152d0011ad309a',1,'SetTpsActiveForAutoCal(void):&#160;calibrator.c'],['../calibrator_8h.html#a2c11370f140453dcc0152d0011ad309a',1,'SetTpsActiveForAutoCal(void):&#160;calibrator.c']]],
  ['settpsdelay',['SetTpsDelay',['../tps__driver_8c.html#ace29146249891485b3ad9aa539217777',1,'SetTpsDelay(uint16_t timeUs):&#160;tps_driver.c'],['../tps__driver_8h.html#a2c356d8ad10f0606b373009204ef7469',1,'SetTpsDelay(uint16_t time):&#160;tps_driver.c']]],
  ['settx',['setTx',['../struct___ctrl_msg__t.html#afa6d3e2b093b0c07a196f1489fa6e0cd',1,'_CtrlMsg_t']]],
  ['settxcmdcallback',['SetTxCmdCallback',['../system__commands_8c.html#a45df06c63f97d581395b637a1e3b8b98',1,'system_commands.c']]],
  ['setuvpinit',['setUvpInit',['../struct___ctrl_msg__t.html#aace5c9f6ec7677f50bcb50c197b9f7b1',1,'_CtrlMsg_t']]],
  ['setuvpnum',['setUvpNum',['../struct___ctrl_msg__t.html#a4a96b3a05b8e43ed2c4423d279b8d091',1,'_CtrlMsg_t']]],
  ['setuvpon',['setUvpOn',['../struct___ctrl_msg__t.html#a311739c03d3425c2bba5672d223c7514',1,'_CtrlMsg_t']]],
  ['setuvponcmdcallback',['SetUvpOnCmdCallback',['../system__commands_8c.html#aea51be8d31a72a32924cf8d61ab21b91',1,'system_commands.c']]],
  ['setuvppowerlevels',['SetUvpPowerLevels',['../power__level_8c.html#a43af918f909b9606826b7fd00b7b472c',1,'SetUvpPowerLevels(PowerRegister_t pow, AmbMask_t ambMask, UvpMask_t uvpMask):&#160;power_level.c'],['../power__level_8h.html#a43af918f909b9606826b7fd00b7b472c',1,'SetUvpPowerLevels(PowerRegister_t pow, AmbMask_t ambMask, UvpMask_t uvpMask):&#160;power_level.c']]],
  ['setuvpthreshen',['SetUvpThreshEn',['../gather__and__vote__task_8c.html#a6e286a0425f6ddf915610a401c9ba678',1,'SetUvpThreshEn(bool enable):&#160;gather_and_vote_task.c'],['../gather__and__vote__task_8h.html#a6e286a0425f6ddf915610a401c9ba678',1,'SetUvpThreshEn(bool enable):&#160;gather_and_vote_task.c']]],
  ['setvalidambmaskcmdcallback',['SetValidAmbMaskCmdCallback',['../system__commands_8c.html#aeb2599f2d0ea8e34baa647d2c109aa50',1,'system_commands.c']]],
  ['short_5fid_5finit_5fval',['SHORT_ID_INIT_VAL',['../client__interface_8c.html#ae769a098e40bc4c4e92c23d9fb98be54',1,'client_interface.c']]],
  ['shortaddr',['shortAddr',['../_cota_common_types_8h.html#adf35797a6252cf8c8ca17201d5676dab',1,'shortAddr():&#160;CotaCommonTypes.h'],['../proxy_host_msg_inf_8h.html#adf35797a6252cf8c8ca17201d5676dab',1,'shortAddr():&#160;proxyHostMsgInf.h']]],
  ['shortaddress',['shortAddress',['../proxy_host_msg_inf_8h.html#a1e3c449a0789a33544de0421409b154e',1,'proxyHostMsgInf.h']]],
  ['shortid',['shortId',['../struct___client__t.html#a1e1ee36bad3f371b2cbc437136c4fe9a',1,'_Client_t::shortId()'],['../struct___resp_reduced_rcvr_info__t.html#a1e1ee36bad3f371b2cbc437136c4fe9a',1,'_RespReducedRcvrInfo_t::shortId()'],['../struct___nvm_client_data__t.html#a85211e3833a02cdebce652f5c82d7ba4',1,'_NvmClientData_t::shortId()'],['../struct___client_slot_data__t.html#a85211e3833a02cdebce652f5c82d7ba4',1,'_ClientSlotData_t::shortId()']]],
  ['shorttimercallback',['shortTimerCallback',['../calibrator_8c.html#a22d72c2bd167540d18cc82cd5105e7cb',1,'calibrator.c']]],
  ['shrtaddr_5ft',['ShrtAddr_t',['../_cota_common_types_8h.html#a45997dc7e2423dff89fc33f048f395a5',1,'CotaCommonTypes.h']]],
  ['shutoff',['shutoff',['../proxy_host_msg_inf_8h.html#aef1d81858aa2cb194c01b2dc918dabd2',1,'proxyHostMsgInf.h']]],
  ['side_5fof_5famb_5fend',['SIDE_OF_AMB_END',['../calibrator_8c.html#ac7e3d0b907c46705778a2243eb2f7bc4',1,'calibrator.c']]],
  ['side_5fof_5famb_5fstart',['SIDE_OF_AMB_START',['../calibrator_8c.html#a2e25b8e1e09c242de0e6aa962a058932',1,'calibrator.c']]],
  ['signalsem',['signalSem',['../struct___variable_queue_control__t.html#a837585026a14232c48a382354c7ced35',1,'_VariableQueueControl_t']]],
  ['signalsembuf',['signalSemBuf',['../struct___variable_queue_control__t.html#a3a720c1531aa94162cc938d044218c18',1,'_VariableQueueControl_t']]],
  ['signaltpsdone',['SignalTpsDone',['../ctrl__task_8h.html#ae18e5265ea575abba69f3a1a16fe8af1',1,'ctrl_task.h']]],
  ['single_5fmode_5foff',['SINGLE_MODE_OFF',['../amb__control_8c.html#a340581abd28fb58d7746614668b55a58',1,'amb_control.c']]],
  ['single_5fmode_5fon',['SINGLE_MODE_ON',['../amb__control_8c.html#a55e5f1e6b86c01a895d0ac3345225fbd',1,'amb_control.c']]],
  ['singleambcalibrate',['SingleAmbCalibrate',['../calibrator_8c.html#adabc2abe77bd88ff6d48302365c5adee',1,'calibrator.c']]],
  ['size',['size',['../struct___client_tree__t.html#aaba88b24a21a6c70c895c0d55f4a69a0',1,'_ClientTree_t::size()'],['../struct___uvp_reg_read__t.html#ae5dc6ffcd9b7605c7787791e40cc6bb0',1,'_UvpRegRead_t::size()'],['../struct___prx_fw_upd_packet__t.html#ab2c6b258f02add8fdf4cfc7c371dd772',1,'_PrxFwUpdPacket_t::size()'],['../struct___prx_fw_upd_packet__t.html#aaba88b24a21a6c70c895c0d55f4a69a0',1,'_PrxFwUpdPacket_t::size()'],['../struct___fan_reg__t.html#ae5dc6ffcd9b7605c7787791e40cc6bb0',1,'_FanReg_t::size()'],['../struct___uvp_reg__t.html#af8fd9ce16b371c8a3bf5e0b37a69419f',1,'_UvpReg_t::size()'],['../struct___fw_image_header_ota__t.html#ab2c6b258f02add8fdf4cfc7c371dd772',1,'_FwImageHeaderOta_t::size()'],['../_cota_common_types_8h.html#ae5dc6ffcd9b7605c7787791e40cc6bb0',1,'size():&#160;CotaCommonTypes.h']]],
  ['sizeof_5fchecksum',['SIZEOF_CHECKSUM',['../proxy_8c.html#a40ea2167148f6a27e1f35bf74f01cb10',1,'proxy.c']]],
  ['sizeof_5fmsg_5flen',['SIZEOF_MSG_LEN',['../proxy_8c.html#a3ef71279261090b212d495f6e4afdefe',1,'proxy.c']]],
  ['sleep_5fflag_5fbit',['SLEEP_FLAG_BIT',['../_cota_common_types_8h.html#abb6502047b332afdf643b0a360003c61',1,'CotaCommonTypes.h']]],
  ['sleepflag',['sleepFlag',['../_cota_common_types_8h.html#a59e2a83999576aa38ad97be9dc54ba63',1,'CotaCommonTypes.h']]],
  ['slot_5fnot_5ffound',['SLOT_NOT_FOUND',['../nvm__driver_8c.html#ad859824d4ccd08e30c77fee415a3f81d',1,'nvm_driver.c']]],
  ['soc',['soc',['../_cota_common_types_8h.html#aef60bf7095fc035eaad9a63b4ecc5e5c',1,'CotaCommonTypes.h']]],
  ['source_5fcontrol_5fversion',['SOURCE_CONTROL_VERSION',['../version__from__source__control_8h.html#a681f4ca51fac5d8609e119fbd3864644',1,'version_from_source_control.h']]],
  ['speed',['speed',['../struct___fan_map_temp_to_speed__t.html#aae32029df16a54aa86c0aec2df9f7bb7',1,'_FanMapTempToSpeed_t']]],
  ['spi',['spi',['../struct___amb_map.html#a06909907ded69cbc00db1f401a90f8ee',1,'_AmbMap']]],
  ['spi1_5firqhandler',['SPI1_IRQHandler',['../stm32h7xx__it_8c.html#a9bbd8c17ce4f49adcca47d11f482aab6',1,'SPI1_IRQHandler(void):&#160;stm32h7xx_it.c'],['../stm32h7xx__it_8h.html#a9bbd8c17ce4f49adcca47d11f482aab6',1,'SPI1_IRQHandler(void):&#160;stm32h7xx_it.c']]],
  ['spi2_5firqhandler',['SPI2_IRQHandler',['../stm32h7xx__it_8c.html#a68d8880cd80cb17a2501487c3d649ea1',1,'SPI2_IRQHandler(void):&#160;stm32h7xx_it.c'],['../stm32h7xx__it_8h.html#a68d8880cd80cb17a2501487c3d649ea1',1,'SPI2_IRQHandler(void):&#160;stm32h7xx_it.c']]],
  ['spi3_5firqhandler',['SPI3_IRQHandler',['../stm32h7xx__it_8c.html#a82987561e28d02b184ecb0515a5e5d2b',1,'SPI3_IRQHandler(void):&#160;stm32h7xx_it.c'],['../stm32h7xx__it_8h.html#a82987561e28d02b184ecb0515a5e5d2b',1,'SPI3_IRQHandler(void):&#160;stm32h7xx_it.c']]],
  ['spi4_5firqhandler',['SPI4_IRQHandler',['../stm32h7xx__it_8c.html#ae5dcb5c1516cad8503ba1c778107c580',1,'SPI4_IRQHandler(void):&#160;stm32h7xx_it.c'],['../stm32h7xx__it_8h.html#ae5dcb5c1516cad8503ba1c778107c580',1,'SPI4_IRQHandler(void):&#160;stm32h7xx_it.c']]],
  ['spi5_5firqhandler',['SPI5_IRQHandler',['../stm32h7xx__it_8c.html#aed54c44d27370a9a2dc70bd1db8e740b',1,'SPI5_IRQHandler(void):&#160;stm32h7xx_it.c'],['../stm32h7xx__it_8h.html#aed54c44d27370a9a2dc70bd1db8e740b',1,'SPI5_IRQHandler(void):&#160;stm32h7xx_it.c']]],
  ['spi6_5firqhandler',['SPI6_IRQHandler',['../stm32h7xx__it_8c.html#ad7366f9eed119e113cd18e124723fe1a',1,'SPI6_IRQHandler(void):&#160;stm32h7xx_it.c'],['../stm32h7xx__it_8h.html#ad7366f9eed119e113cd18e124723fe1a',1,'SPI6_IRQHandler(void):&#160;stm32h7xx_it.c']]],
  ['spi_5fdisable_5f0_5fgpio_5fport',['SPI_DISABLE_0_GPIO_Port',['../main_8h.html#a5fd96bb8284a36f3730de93cbeec8d96',1,'main.h']]],
  ['spi_5fdisable_5f0_5fpin',['SPI_DISABLE_0_Pin',['../main_8h.html#a6ecab3de554d7c0baa48fb6373515b24',1,'main.h']]],
  ['spi_5fdisable_5f1_5fgpio_5fport',['SPI_DISABLE_1_GPIO_Port',['../main_8h.html#a74b2e6c37471f53ab583d5bb8390334f',1,'main.h']]],
  ['spi_5fdisable_5f1_5fpin',['SPI_DISABLE_1_Pin',['../main_8h.html#a3e98d0aee066d69c1ad3f972cf576892',1,'main.h']]],
  ['spi_5fdisable_5f2_5fgpio_5fport',['SPI_DISABLE_2_GPIO_Port',['../main_8h.html#a715f282f73752c1a62316d7e24576041',1,'main.h']]],
  ['spi_5fdisable_5f2_5fpin',['SPI_DISABLE_2_Pin',['../main_8h.html#a41dd7169ca3ca8cea00fd664e28ccbec',1,'main.h']]],
  ['spi_5fdisable_5f3_5fgpio_5fport',['SPI_DISABLE_3_GPIO_Port',['../main_8h.html#a965f3419ffa89314217dec370b82af5b',1,'main.h']]],
  ['spi_5fdisable_5f3_5fpin',['SPI_DISABLE_3_Pin',['../main_8h.html#ab4aecc944c2526f2d53ff6a3afb3a008',1,'main.h']]],
  ['spi_5fdma_5fbuf_5fsize',['SPI_DMA_BUF_SIZE',['../spi__driver_8c.html#ab31732c4055c533a3d5f1034c9921b75',1,'spi_driver.c']]],
  ['spi_5fdriver_2ec',['spi_driver.c',['../spi__driver_8c.html',1,'']]],
  ['spi_5fdriver_2eh',['spi_driver.h',['../spi__driver_8h.html',1,'']]],
  ['spi_5fenable_5foff',['SPI_ENABLE_OFF',['../amb__control_8c.html#a767ab0d27ecc991becc4d18dec1c13be',1,'amb_control.c']]],
  ['spi_5fenable_5fon',['SPI_ENABLE_ON',['../amb__control_8c.html#a667c4764d62f22b4164486dfd0103b23',1,'amb_control.c']]],
  ['spi_5fmax_5fnum_5fbytes',['SPI_MAX_NUM_BYTES',['../spi__driver_8h.html#acc9e6cd5122c0633d8a76596a4b892f9',1,'spi_driver.h']]],
  ['spi_5fsimul_5fread_5fsize',['SPI_SIMUL_READ_SIZE',['../spi__driver_8c.html#afd3804937e8d53dca4615e02b47c08be',1,'spi_driver.c']]],
  ['spi_5fsingle_5fgpio_5fport',['SPI_SINGLE_GPIO_Port',['../main_8h.html#acb8e6332a58f6061d9b7fcaf40083c39',1,'main.h']]],
  ['spi_5fsingle_5fpin',['SPI_SINGLE_Pin',['../main_8h.html#aa15018a7904d7b7d993c0a5015526873',1,'main.h']]],
  ['spi_5fwait_5ftimeout',['SPI_WAIT_TIMEOUT',['../spi__driver_8c.html#af59d67d67d946e7e8e728dc586182ea5',1,'spi_driver.c']]],
  ['spidisablepin',['spiDisablePin',['../struct___amb_map.html#a7cfadd62184e2370374d4ec572b21e83',1,'_AmbMap']]],
  ['spidisableport',['spiDisablePort',['../struct___amb_map.html#ad2b5604ad7ceb34a9d1fd485e34b2cba',1,'_AmbMap']]],
  ['spin_5fup_5f0_5f5',['SPIN_UP_0_5',['../fan__driver_8c.html#a4ff1f64572f61d367ca2cec4208a7b14a558cb87c08bea5039eb82c5952d39e9d',1,'fan_driver.c']]],
  ['spin_5fup_5f1_5f0',['SPIN_UP_1_0',['../fan__driver_8c.html#a4ff1f64572f61d367ca2cec4208a7b14aa0d19110c8f407520494a98fbb6458b1',1,'fan_driver.c']]],
  ['spin_5fup_5f2_5f0',['SPIN_UP_2_0',['../fan__driver_8c.html#a4ff1f64572f61d367ca2cec4208a7b14a062c04977bbdd695ddafa85e9380683a',1,'fan_driver.c']]],
  ['spin_5fup_5foff',['SPIN_UP_OFF',['../fan__driver_8c.html#a4ff1f64572f61d367ca2cec4208a7b14a06fd8531d919aa4c23873a47d5a253c0',1,'fan_driver.c']]],
  ['spisimultransfer',['SpiSimulTransfer',['../spi__driver_8c.html#a1468c0f02a2ee5426f66301673f86dbe',1,'SpiSimulTransfer(uint8_t *pTxData, uint8_t *pRxData, uint16_t size, AmbMask_t ambMask):&#160;spi_driver.c'],['../spi__driver_8h.html#a1468c0f02a2ee5426f66301673f86dbe',1,'SpiSimulTransfer(uint8_t *pTxData, uint8_t *pRxData, uint16_t size, AmbMask_t ambMask):&#160;spi_driver.c']]],
  ['spitransfer',['SpiTransfer',['../spi__driver_8c.html#a6319ba5a22490bc29408eefc737a0511',1,'SpiTransfer(SPI_HandleTypeDef *hSpi, uint8_t *pTxData, uint8_t *pRxData, uint16_t size):&#160;spi_driver.c'],['../spi__driver_8h.html#a833c60288024945951aacc06ee90aeec',1,'SpiTransfer(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size):&#160;spi_driver.c']]],
  ['sr_5fassert',['SR_ASSERT',['../raspberry__pi_8c.html#ae56ea29b0b40a2ad734d41d1c111fe24',1,'raspberry_pi.c']]],
  ['sr_5fdeassert',['SR_DEASSERT',['../raspberry__pi_8c.html#a14a33f564c8f65894fcf9d5d9faad5ce',1,'raspberry_pi.c']]],
  ['sr_5fline_5fasserted',['SR_LINE_ASSERTED',['../proxy_8c.html#a87da7fc79ea9f049bf24a1c77eb952f2',1,'SR_LINE_ASSERTED():&#160;proxy.c'],['../raspberry__pi_8c.html#a87da7fc79ea9f049bf24a1c77eb952f2',1,'SR_LINE_ASSERTED():&#160;raspberry_pi.c']]],
  ['sr_5fline_5fdeasserted',['SR_LINE_DEASSERTED',['../proxy_8c.html#a616ac49057d782908ebe7259dfcfc4c6',1,'SR_LINE_DEASSERTED():&#160;proxy.c'],['../raspberry__pi_8c.html#a616ac49057d782908ebe7259dfcfc4c6',1,'SR_LINE_DEASSERTED():&#160;raspberry_pi.c']]],
  ['ssbbuffsen_5fsendpwr',['SSBBUFFSEN_SENDPWR',['../uvp__driver_8h.html#abdcef8df454f27822e26bcb96e3365e3',1,'uvp_driver.h']]],
  ['ssbbuffsiibstbuff1',['SSBBUFFSIIBSTBUFF1',['../uvp__driver_8h.html#a2e6a08f072683204e52753eade08ab3b',1,'uvp_driver.h']]],
  ['ssbbuffsstartup_5fsendpwr',['SSBBUFFSSTARTUP_SENDPWR',['../uvp__driver_8h.html#af716caa8c3a712db117cfcda8bc594b3',1,'uvp_driver.h']]],
  ['ssbmixen_5fsendpwr',['SSBMIXEN_SENDPWR',['../uvp__driver_8h.html#a391b57600265a1fd59756c2f112fcb5e',1,'uvp_driver.h']]],
  ['ssbmixstartup_5fsendpwr',['SSBMIXSTARTUP_SENDPWR',['../uvp__driver_8h.html#ab1a611b9d2f071c403daeecf69395fcd',1,'uvp_driver.h']]],
  ['ssbmodctrl',['SSBMODCTRL',['../uvp__driver_8h.html#ab550c530f47a6c589c02972196787133',1,'uvp_driver.h']]],
  ['startccbtemperatureacquisition',['StartCCBTemperatureAcquisition',['../ccb__temperature_8c.html#a4737ff1f0b93343b11a71bf7996d9f3d',1,'StartCCBTemperatureAcquisition(TempConvRate_t convRate):&#160;ccb_temperature.c'],['../ccb__temperature_8h.html#a4737ff1f0b93343b11a71bf7996d9f3d',1,'StartCCBTemperatureAcquisition(TempConvRate_t convRate):&#160;ccb_temperature.c']]],
  ['startcharging',['startCharging',['../struct___ctrl_msg__t.html#a92544966572de0248c84c80ac6d70825',1,'_CtrlMsg_t']]],
  ['startchargingcmdcallback',['StartChargingCmdCallback',['../system__commands_8c.html#a4f0056328b49d9e63123494396965d27',1,'system_commands.c']]],
  ['startclitask',['StartCliTask',['../cli__task_8c.html#af69069e8b665c799c38cd809f674a820',1,'cli_task.c']]],
  ['startdiscmsg',['startDiscMsg',['../proxy_host_msg_inf_8h.html#ad19388ed9c03d7aea4a24e92a1a913eb',1,'proxyHostMsgInf.h']]],
  ['startgatherandvotetask',['StartGatherAndVoteTask',['../gather__and__vote__task_8h.html#a962b812c8b0368e1cfb2affaa299f736',1,'gather_and_vote_task.h']]],
  ['startstatictunecmdcallback',['StartStaticTuneCmdCallback',['../system__commands_8c.html#af0438015678830c564cdc29234ce9ca8',1,'system_commands.c']]],
  ['startuvps',['StartUvps',['../uvp__driver_8c.html#abd6d067145671c962c8ccf1eb20cd550',1,'StartUvps(AmbMask_t ambMask):&#160;uvp_driver.c'],['../uvp__driver_8h.html#abd6d067145671c962c8ccf1eb20cd550',1,'StartUvps(AmbMask_t ambMask):&#160;uvp_driver.c']]],
  ['state',['state',['../struct___client__t.html#a7c2c9a5919febbac18df2db0d1c0866d',1,'_Client_t::state()'],['../struct___resp_get_system_state__t.html#a0b57aa10271a66f3dc936bba1d2f3830',1,'_RespGetSystemState_t::state()'],['../struct___resp_reduced_rcvr_info__t.html#a7c2c9a5919febbac18df2db0d1c0866d',1,'_RespReducedRcvrInfo_t::state()'],['../struct___state_machine_state__t.html#ad554edfdaf2f315169fb7923dfa78942',1,'_StateMachineState_t::state()'],['../proxy_host_msg_inf_8h.html#a0b57aa10271a66f3dc936bba1d2f3830',1,'state():&#160;proxyHostMsgInf.h']]],
  ['state_5fmachine_2ec',['state_machine.c',['../state__machine_8c.html',1,'']]],
  ['state_5fmachine_2eh',['state_machine.h',['../state__machine_8h.html',1,'']]],
  ['stateid_5ft',['StateId_t',['../state__machine_8h.html#a19bd00d45ca156dbc4b55081d8f4a2e1',1,'state_machine.h']]],
  ['statemachinestate_5ft',['StateMachineState_t',['../state__machine_8h.html#a595461de9be43d71875fc4eab1930ea5',1,'state_machine.h']]],
  ['statemachinetransition_5ft',['StateMachineTransition_t',['../state__machine_8h.html#af529d6a00f62e4aff01634e30fba21eb',1,'state_machine.h']]],
  ['statemask',['stateMask',['../struct___resp_get_power_monitor__t.html#a0ad4e2f511be6ab404cadfdf1ba6f62c',1,'_RespGetPowerMonitor_t']]],
  ['statename',['stateName',['../struct___state_machine_state__t.html#ac5838132035f6886a0d9a34b9d0e3860',1,'_StateMachineState_t']]],
  ['static_5fcharge_5fiteration_5fdelay',['STATIC_CHARGE_ITERATION_DELAY',['../gather__and__vote__task_8c.html#a4d29b01edfe87498239b3c048e91a0ac',1,'gather_and_vote_task.c']]],
  ['static_5fcharge_5ftime_5fdisplay_5fperiod',['STATIC_CHARGE_TIME_DISPLAY_PERIOD',['../gather__and__vote__task_8c.html#a17b00045cc42c10de89bfe8f742a763c',1,'gather_and_vote_task.c']]],
  ['staticcharge',['staticCharge',['../struct___ctrl_msg__t.html#a5cbc27c4d22abc99ad3c3bb44a37a043',1,'_CtrlMsg_t']]],
  ['staticchargecmdcallback',['StaticChargeCmdCallback',['../system__commands_8c.html#a43246d57df3d1b08d8db0d4590cc6f89',1,'system_commands.c']]],
  ['statictune',['StaticTune',['../calibrator_8c.html#adf237d81481452cbcefef882bb0c23f8',1,'StaticTune(AmbMask_t ambMask):&#160;calibrator.c'],['../calibrator_8h.html#adf237d81481452cbcefef882bb0c23f8',1,'StaticTune(AmbMask_t ambMask):&#160;calibrator.c']]],
  ['status',['status',['../struct___prx_fw_get_status_rsp__t.html#ade818037fd6c985038ff29656089758d',1,'_PrxFwGetStatusRsp_t::status()'],['../union_cota_f_w_update_last_op_status__t.html#ade818037fd6c985038ff29656089758d',1,'CotaFWUpdateLastOpStatus_t::status()'],['../_cota_common_types_8h.html#a611387033ca8713feeba0a7db3f29484',1,'status():&#160;CotaCommonTypes.h'],['../proxy_client_msg_inf_8h.html#a7b55385229359635a8fe558fc3774b97',1,'status():&#160;proxyClientMsgInf.h'],['../proxy_host_msg_inf_8h.html#a5393c99e246925076b1dfd69a64177ef',1,'status():&#160;proxyHostMsgInf.h']]],
  ['statusflags',['statusFlags',['../struct___resp_reduced_rcvr_info__t.html#a9d483842246b35782300a2323917a5cc',1,'_RespReducedRcvrInfo_t::statusFlags()'],['../_cota_common_types_8h.html#a9d483842246b35782300a2323917a5cc',1,'statusFlags():&#160;CotaCommonTypes.h']]],
  ['stm32h7xx_5fhal_5fconf_2eh',['stm32h7xx_hal_conf.h',['../stm32h7xx__hal__conf_8h.html',1,'']]],
  ['stm32h7xx_5fhal_5fmsp_2ec',['stm32h7xx_hal_msp.c',['../stm32h7xx__hal__msp_8c.html',1,'']]],
  ['stm32h7xx_5fit_2ec',['stm32h7xx_it.c',['../stm32h7xx__it_8c.html',1,'']]],
  ['stm32h7xx_5fit_2eh',['stm32h7xx_it.h',['../stm32h7xx__it_8h.html',1,'']]],
  ['stopcharging',['stopCharging',['../struct___ctrl_msg__t.html#a0ebe9b88ff6ee838561a4f22cddadff8',1,'_CtrlMsg_t']]],
  ['stopchargingcmdcallback',['StopChargingCmdCallback',['../system__commands_8c.html#a020d01f177da1ba938f658aed0333f0d',1,'system_commands.c']]],
  ['stopchargingsoclevel',['stopChargingSocLevel',['../_cota_common_types_8h.html#a625022d9b3dbf367f7215b4c4115586f',1,'CotaCommonTypes.h']]],
  ['stoptpscycle',['StopTpsCycle',['../tps__driver_8c.html#a3ec1fd2db7f8c804ccd6ce1bb308c12c',1,'StopTpsCycle(void):&#160;tps_driver.c'],['../tps__driver_8h.html#a3ec1fd2db7f8c804ccd6ce1bb308c12c',1,'StopTpsCycle(void):&#160;tps_driver.c']]],
  ['storagearea',['storageArea',['../struct___variable_queue_control__t.html#ac16936fc4a6dda1c3a038874b8de97c1',1,'_VariableQueueControl_t']]],
  ['string_5fterm',['STRING_TERM',['../cli__task_8c.html#ad510e0f46507f3131cddc5603b772e5b',1,'cli_task.c']]],
  ['success',['success',['../union_cota_f_w_update_last_op_status__t.html#a33fcc21e19e4b604852ba0c3283911db',1,'CotaFWUpdateLastOpStatus_t']]],
  ['success_5fmsg',['SUCCESS_MSG',['../cli__task_8h.html#a8e2ce6556a67a4b7c4ed51ce8621cf7e',1,'cli_task.h']]],
  ['sys_5fclk_5famplitude_5foffset',['SYS_CLK_AMPLITUDE_OFFSET',['../sysclk__driver_8c.html#a86dd7008ec7d394a0b28f5b6efae16a9',1,'sysclk_driver.c']]],
  ['sys_5fclk_5famplitude_5fsize',['SYS_CLK_AMPLITUDE_SIZE',['../sysclk__driver_8c.html#a6173ec655634e23435609525be0fad3b',1,'sysclk_driver.c']]],
  ['sys_5fclk_5fcal_5fdelay_5fms',['SYS_CLK_CAL_DELAY_MS',['../sysclk__driver_8c.html#a78ec207a984773416805203f810df7cd',1,'sysclk_driver.c']]],
  ['sys_5fclk_5fcm_5fmode_5foffset',['SYS_CLK_CM_MODE_OFFSET',['../sysclk__driver_8c.html#abf334e8acf5a131a64d66fb87fe330b6',1,'sysclk_driver.c']]],
  ['sys_5fclk_5fcm_5fmode_5fsize',['SYS_CLK_CM_MODE_SIZE',['../sysclk__driver_8c.html#a8da90f2aaf22d08248b7277d3484c723',1,'sysclk_driver.c']]],
  ['sys_5fclk_5fconfig_5famb_5fmaster',['SYS_CLK_CONFIG_AMB_MASTER',['../sysclk__driver_8h.html#ad9d32863100cb307973ff0eee5666118ac517ee49b48676d15a235e09e40dd584',1,'sysclk_driver.h']]],
  ['sys_5fclk_5fconfig_5famb_5fmaster_5ftam',['SYS_CLK_CONFIG_AMB_MASTER_TAM',['../sysclk__driver_8h.html#ad9d32863100cb307973ff0eee5666118a873166b80bcb3dae5b5f579837d27cfc',1,'sysclk_driver.h']]],
  ['sys_5fclk_5fconfig_5famb_5fonly',['SYS_CLK_CONFIG_AMB_ONLY',['../sysclk__driver_8h.html#ad9d32863100cb307973ff0eee5666118af687a2251426734cd51a0a943027adef',1,'sysclk_driver.h']]],
  ['sys_5fclk_5fconfig_5famb_5fslave',['SYS_CLK_CONFIG_AMB_SLAVE',['../sysclk__driver_8h.html#ad9d32863100cb307973ff0eee5666118ac4e5be90727314e7930d4dba1c0afda2',1,'sysclk_driver.h']]],
  ['sys_5fclk_5fconfig_5famb_5fslave_5ftam',['SYS_CLK_CONFIG_AMB_SLAVE_TAM',['../sysclk__driver_8h.html#ad9d32863100cb307973ff0eee5666118a66bc5f0e80f548732c43c06c251b30b5',1,'sysclk_driver.h']]],
  ['sys_5fclk_5fconfig_5famb_5ftam',['SYS_CLK_CONFIG_AMB_TAM',['../sysclk__driver_8h.html#ad9d32863100cb307973ff0eee5666118af508969245511f12f299890da5fa7f12',1,'sysclk_driver.h']]],
  ['sys_5fclk_5fformat_5foffset',['SYS_CLK_FORMAT_OFFSET',['../sysclk__driver_8c.html#a9ce49e9c0716df616d6d36c538dd96ab',1,'sysclk_driver.c']]],
  ['sys_5fclk_5fformat_5fsize',['SYS_CLK_FORMAT_SIZE',['../sysclk__driver_8c.html#a3aac254da4c34264814d5c16c4f0b45b',1,'sysclk_driver.c']]],
  ['sys_5fclk_5finv_5foffset',['SYS_CLK_INV_OFFSET',['../sysclk__driver_8c.html#a841af1deb616f6b5ce1412bf3025f7a8',1,'sysclk_driver.c']]],
  ['sys_5fclk_5finv_5fsize',['SYS_CLK_INV_SIZE',['../sysclk__driver_8c.html#a17fd4c4b4d3a7de804864fde6b085547',1,'sysclk_driver.c']]],
  ['sys_5fclk_5fmax_5fconfig',['SYS_CLK_MAX_CONFIG',['../sysclk__driver_8h.html#ad9d32863100cb307973ff0eee5666118a7ecf962f69fe241bf7bc3eb057e78f27',1,'sysclk_driver.h']]],
  ['sys_5fclk_5foutall_5fdisable',['SYS_CLK_OUTALL_DISABLE',['../sysclk__driver_8c.html#a14480052854be314ea346c5866e3bde4',1,'sysclk_driver.c']]],
  ['sys_5fclk_5foutall_5fdisable_5flow',['SYS_CLK_OUTALL_DISABLE_LOW',['../sysclk__driver_8c.html#a94ba45bea8cb93bc3104e33452987cfe',1,'sysclk_driver.c']]],
  ['sys_5fclk_5foutall_5fenable',['SYS_CLK_OUTALL_ENABLE',['../sysclk__driver_8c.html#acfc61da0d48253a2df7a5006c9a2d29a',1,'sysclk_driver.c']]],
  ['sys_5frst',['SYS_RST',['../uvp__driver_8h.html#a4c6501ce83b22c9ce466c6888b1d4404',1,'uvp_driver.h']]],
  ['sys_5futil_2ec',['sys_util.c',['../sys__util_8c.html',1,'']]],
  ['sys_5futil_2eh',['sys_util.h',['../sys__util_8h.html',1,'']]],
  ['sysclk_5faddr_5fbyte',['SYSCLK_ADDR_BYTE',['../sysclk__driver_8c.html#a7638282f7d7f177d518da3f427e8fda1',1,'sysclk_driver.c']]],
  ['sysclk_5fdma_5fbuffer_5flen',['SYSCLK_DMA_BUFFER_LEN',['../sysclk__driver_8c.html#a4496d82357464a25eaac7abe73f53a73',1,'sysclk_driver.c']]],
  ['sysclk_5fdriver_2ec',['sysclk_driver.c',['../sysclk__driver_8c.html',1,'']]],
  ['sysclk_5fdriver_2eh',['sysclk_driver.h',['../sysclk__driver_8h.html',1,'']]],
  ['sysclk_5fi2c_5faddr',['SYSCLK_I2C_ADDR',['../sysclk__driver_8c.html#ae7c50216e0b28262f963953632cfa7ef',1,'sysclk_driver.c']]],
  ['sysclk_5fi2c_5fhandle',['SYSCLK_I2C_HANDLE',['../i2c__multiplex_8c.html#afd96629da3707f5a2580905700efac46',1,'i2c_multiplex.c']]],
  ['sysclk_5fpage_5faddr',['SYSCLK_PAGE_ADDR',['../sysclk__driver_8c.html#a95def7d4303537beeec20bb71828686b',1,'sysclk_driver.c']]],
  ['sysclk_5freg_5faddr',['SYSCLK_REG_ADDR',['../sysclk__driver_8c.html#acc0ef03f8d4fa6434bb713ad0d474616',1,'sysclk_driver.c']]],
  ['sysclk_5freset_5fgpio_5fport',['SYSCLK_RESET_GPIO_Port',['../main_8h.html#ac95b90f7e728f206ef3e77aea7915c41',1,'main.h']]],
  ['sysclk_5freset_5fpin',['SYSCLK_RESET_Pin',['../main_8h.html#a01cecbfa00748c13f78e95b570317684',1,'main.h']]],
  ['sysclk_5ftest',['SYSCLK_TEST',['../sysclk__driver_8c.html#a37d27ec138334b366890233d01809949',1,'sysclk_driver.c']]],
  ['sysclkapplyambinversions',['sysClkApplyAMBInversions',['../sysclk__driver_8c.html#a106abd71d7b30f5b27eb7b261c9c7396',1,'sysclk_driver.c']]],
  ['sysclkapplyamplitude',['sysClkApplyAmplitude',['../sysclk__driver_8c.html#a670d334bd5c0f49fb01c3e3dd94a7337',1,'sysclk_driver.c']]],
  ['sysclkapplycommonmode',['sysClkApplyCommonMode',['../sysclk__driver_8c.html#a790ccd902e7097aa7b1f07235182e4c4',1,'sysclk_driver.c']]],
  ['sysclkapplyformat',['sysClkApplyFormat',['../sysclk__driver_8c.html#a15c61068b4b57f30cc86cb7123385ae3',1,'sysclk_driver.c']]],
  ['sysclkconfig_5ft',['SysClkConfig_t',['../sysclk__driver_8h.html#adf47d24ec5e769629a8c21fbdfc63614',1,'sysclk_driver.h']]],
  ['sysclkdump',['sysClkDump',['../struct___ctrl_resp__t.html#a5bdd023f4a47966ec0320ee52104f5b5',1,'_CtrlResp_t::sysClkDump()'],['../sysclk__driver_8c.html#a515b30bd38a90aa10514a4eef8bb3f32',1,'SysClkDump(CtrlResp_t *msg):&#160;sysclk_driver.c'],['../sysclk__driver_8h.html#a515b30bd38a90aa10514a4eef8bb3f32',1,'SysClkDump(CtrlResp_t *msg):&#160;sysclk_driver.c']]],
  ['sysclkdumpcallback',['SysClkDumpCallback',['../system__commands_8c.html#a3f0c81f1bc47556ac847a7df28d5913a',1,'system_commands.c']]],
  ['sysclki2cdisable',['SysClkI2CDisable',['../sysclk__driver_8c.html#a38abba27d421e2ba3a3b35490fe9466a',1,'SysClkI2CDisable(void):&#160;sysclk_driver.c'],['../sysclk__driver_8h.html#a38abba27d421e2ba3a3b35490fe9466a',1,'SysClkI2CDisable(void):&#160;sysclk_driver.c']]],
  ['sysclkmodifyreg',['sysClkModifyReg',['../sysclk__driver_8c.html#a608b87572f3901bfeaa71993df5a756f',1,'sysclk_driver.c']]],
  ['sysclkpagewrite',['sysClkPageWrite',['../sysclk__driver_8c.html#aca782d844bfa11ca119e6188a5edb6ed',1,'sysclk_driver.c']]],
  ['sysclkread',['sysClkRead',['../struct___ctrl_resp__t.html#a5fc81b56494eb43977dd6aa3fd0c45c9',1,'_CtrlResp_t']]],
  ['sysclkreadarg',['sysClkReadArg',['../struct___ctrl_msg__t.html#a424f8cf3de4467b159d36291c8d1b3a2',1,'_CtrlMsg_t']]],
  ['sysclkreadlargeregval',['sysClkReadLargeRegVal',['../sysclk__driver_8c.html#a44209527f0ad7f4d77c54199e8c6bc13',1,'sysclk_driver.c']]],
  ['sysclkreadreg',['SysClkReadReg',['../sysclk__driver_8c.html#a233036cd9521efce5cc987f0a239f281',1,'SysClkReadReg(uint16_t addr, uint8_t *regVal):&#160;sysclk_driver.c'],['../sysclk__driver_8h.html#a233036cd9521efce5cc987f0a239f281',1,'SysClkReadReg(uint16_t addr, uint8_t *regVal):&#160;sysclk_driver.c']]],
  ['sysclkwriteargs',['sysClkWriteArgs',['../struct___ctrl_msg__t.html#a701b2fc7c2eb04b42b0a9cca064195a1',1,'_CtrlMsg_t']]],
  ['sysclkwritelargeregval',['sysClkWriteLargeRegVal',['../sysclk__driver_8c.html#af2c5c7826d507e68abc54ae1d98f13aa',1,'sysclk_driver.c']]],
  ['sysclkwritereg',['sysClkWriteReg',['../sysclk__driver_8c.html#aa3395f2084ae689978c9742c25760cee',1,'sysclk_driver.c']]],
  ['sysclkwritereglist',['sysClkWriteRegList',['../sysclk__driver_8c.html#a13ccd60414557efb4229629b0737c1b4',1,'sysclk_driver.c']]],
  ['sysclock_5fi2c_5frxcpltcallback',['SysClock_I2C_RxCpltCallback',['../sysclk__driver_8c.html#a9f8c75e3cc5936f01d6b174274c2f152',1,'SysClock_I2C_RxCpltCallback(I2C_HandleTypeDef *hi2c):&#160;sysclk_driver.c'],['../sysclk__driver_8h.html#a9f8c75e3cc5936f01d6b174274c2f152',1,'SysClock_I2C_RxCpltCallback(I2C_HandleTypeDef *hi2c):&#160;sysclk_driver.c']]],
  ['sysclock_5fi2c_5ftxcpltcallback',['SysClock_I2C_TxCpltCallback',['../sysclk__driver_8c.html#a48731b32a06b93f5b1eecf75dba82106',1,'SysClock_I2C_TxCpltCallback(I2C_HandleTypeDef *hi2c):&#160;sysclk_driver.c'],['../sysclk__driver_8h.html#a48731b32a06b93f5b1eecf75dba82106',1,'SysClock_I2C_TxCpltCallback(I2C_HandleTypeDef *hi2c):&#160;sysclk_driver.c']]],
  ['sysctrl_5fstate',['SYSCTRL_STATE',['../uvp__driver_8h.html#ac3e1f78105e86c4ed864b7dfeecdbe14',1,'uvp_driver.h']]],
  ['sysnotreseted',['SYSNOTRESETED',['../uvp__driver_8h.html#a5b9cfd87dc3f582c80518b8e624ff443',1,'uvp_driver.h']]],
  ['system_5fcommands_2ec',['system_commands.c',['../system__commands_8c.html',1,'']]],
  ['system_5fcommands_2eh',['system_commands.h',['../system__commands_8h.html',1,'']]],
  ['systemclockinit',['SystemClockInit',['../sysclk__driver_8c.html#a42cf85d3f5c128f028cc0c2d06f3571d',1,'SystemClockInit(void):&#160;sysclk_driver.c'],['../sysclk__driver_8h.html#a42cf85d3f5c128f028cc0c2d06f3571d',1,'SystemClockInit(void):&#160;sysclk_driver.c']]]
];
