REM  This file runs the Powershell script by the same name that generates a .c file containing FW version information

ECHO OFF
PUSHD %~dp0%
PowerShell.exe -ExecutionPolicy Bypass -Command "& '%~dpn0.ps1'"
POPD
