/**
 * @file       CotaCommonTypes.h
 * 
 * @brief      Header file containing data structures and definitions for common data packets
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2013-2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *  
 *  
 *  
 *  @warning  All data must be transmitted fully packed without any alignment. 
 *            Typically, compilers require an alignment control attribute like
 *            "__packed" or a "pack" pragma directive.  All nested data structures
 *            need to have an alignment of 1 byte to achieve proper full packing.
 *
 */

// Define to prevent recursive inclusion ------------------------------------
#ifndef COTA_COMMON_TYPES_H
#define COTA_COMMON_TYPES_H

// Includes -----------------------------------------------------------------
#include "CotaCompiler.h"
#include "stdint.h"

// Public define ------------------------------------------------------------
                                      
#define EXTADDR_LEN                   (8)       ///< Extended address length
#define ASIC_SN_BYTES                 (16)      ///< ASIC serial number length, the number of bytes in the Cota ASIC`s serial number
#define ASIC_SN_WORDS                 (ASIC_SN_BYTES/sizeof(uint32_t))  ///< ASIC serial number length, the number of 32-bit words in the Cota ASIC`s serial number
                                      
#define NUM_ENCODING_BYTES            (8)       ///< Number of bytes in the Tone Beacon encodeing 
#define MIN_TPS_CLIENTS               (1)       ///< Minimum number of clients that must be present for a ton ecycle to be valid
#define MAX_TPS_CLIENTS               (8)       ///< maximum number of clients that may be present for a ton ecycle to be valid
#define MIN_CQT_CLIENTS               (1)       
#define MAX_CUSTOM_DATA_SIZE          (64)      ///< Maximum number of payload bytes that may be present in a custom data query packet, including required payload info
#define MAX_CLIENT_CMD_PAYLOAD_SIZE   (128)     ///< Maximum number of payload bytes in the generic client command message
#define MAX_PROXY_CMD_PAYLOAD_SIZE    (64)      ///< Maximum number of payload bytes in the generic proxy command message
#define SLEEP_FLAG_BIT                (1)       ///< Bit Mask for sleepFlag in CotaTrackerStatus_t

#define MAX_APP_MESSAGE_CONTENT_SIZE  (8)       ///< Size of outgoing application message
#define MAX_APP_MESSAGE_SIZE          (MAX_APP_MESSAGE_CONTENT_SIZE + 1)   ///< Total maximum size of the message

#define JOIN_SUCCESS                  (1)       ///< Indicates the client has successfully joined and sent a response

// Public macros -------------------------------------------------------------

// Public typedef -----------------------------------------------------------

/** Enumeration of receiver query types */
typedef enum _CotaRcvrQuery_t
{
  RCVR_QUERY_RESERVED_1 = 0,
  RCVR_QUERY_RESERVED_2 = 1,
  RCVR_QUERY_RESERVED_3 = 2,
  RCVR_QUERY_RESERVED_4 = 3,
  RCVR_QUERY_RESERVED_5 = 4,
  RCVR_QUERY_STANDARD   = 5,   ///< Standard query with minimal information requested
  RCVR_QUERY_CUSTOM     = 6,   ///< Query that includes standard information request and custom data field
  RCVR_QUERY_OPTIMIZED  = 7    ///< Optimized query with compressed standard and optional custom data field
} CotaRcvrQuery_t;

/** Enum value for debug pin states */
typedef enum
{
    DBG_PIN_ON = 0,     ///< Debug pin on
    DBG_PIN_OFF,        ///< Debug pin off
    NUM_DBG_PIN_STATES  ///< Number of debug states
} DebugPinStates_t;

/** Valid light ring states */
typedef enum _LightRingState_t
{
  LR_STATE_IDLE  = 0,     ///< Idle state state
  LR_STATE_CALIB = 1,     ///< Calibration state
  LR_STATE_WAIT  = 2,     ///< Waiting to discover registered clients state
  LR_STATE_READY = 3,     ///< Ready to start power transmission state
  LR_STATE_TX    = 4,     ///< Transmitting power state
  LR_STATE_IDENT = 5,     ///< Transmitter identification state
  LR_STATE_DEBUG = 6,     ///< The transmitter is in debug/pause mode
  LR_STATE_HOLD  = 127,   ///< Hold state (freezes the current light pattern)
  LR_STATE_ERROR = 128,   ///< Error state
} LightRingState_t;  

/**
 * @brief Structure that hold the long address
 */ 
typedef PACKED struct
{
    uint8_t bytes[EXTADDR_LEN];        ///< Long (extended) address as used in IEEE 802.15.4, 8 bytes
} ExtdAddr_t;

typedef uint16_t ShrtAddr_t;         ///< Short address as used in IEEE 802.15.4, 16 bits

/**
 * @brief Union that holds the asic chip serial number
 */
typedef PACKED union
{
    uint8_t  bytes[ASIC_SN_BYTES];      ///< 128-bit ASIC chip serial number
    uint32_t words[ASIC_SN_WORDS];      ///< 128-bit ASIC chip serial number
} AsicSerialNum_t;
    
/**
 * @brief TPS message header
 */
typedef PACKED struct
{
    uint8_t  tpsSeqNum;                ///< Sequence number for tracking purpose
    uint8_t  totalClients;             ///< Total number of client in this TPS
    uint8_t  tpsPeriod;                ///< TPS period specifies the number of Beacon Beats (BBs) to enumerate client tone and power slots before restarting at slot 1 again
} CotaTpsMsgHdr_t;

/** Generic query status byte */
typedef PACKED struct
{
    uint8_t  powerRq:1,                ///< client want power in the next cycles
             clientRxCharge:1,         ///< the client received power in the last cycle
             targetRxCharge:1,         ///< If present, the external target device received power  If no ext device present, use false
             errorStatus:2,            ///< use proxyClientErrStatus_t
             missedTpsCycles:3;        ///< Number of missed TPS cycles
} CotaClientStatus_t;

/** ForeverTracker query status byte */
typedef PACKED struct
{
    uint8_t  powerRq:1,                ///< client want power in the next cycles
             clientRxCharge:1,         ///< the client received power in the last cycle
             targetRxCharge:1,         ///< If present, the external target device received power If no ext device present, use false
             errorStatus:2,            ///< use proxyClientErrStatus_t
             missedTpsCycles:2,        ///< Number of missed TPS cycles
             sleepFlag:1;              ///< Receiver reported tracker board is asleep
} CotaTrackerStatus_t;

/** Client/proxy response to client query; see #PRX_HOST_CLIENT_QUERY_DATA */
typedef PACKED struct
{
  ShrtAddr_t      clientShortAddr; ///< Client short address
  uint8_t         linkQuality;     ///< Link quality
  int8_t          rssi;            ///< RSSI value (dbm)
  CotaRcvrQuery_t queryType;       ///< Query type
} CotaClientQueryHeader_t;

/** cota client standard status; see #PRX_CLT_MSG_STATUS_STD */
typedef PACKED struct
{
    int8_t              peakPower;           ///< dBm
    int8_t              avgPower;            ///< dBm
    int16_t             peakNetCurrent;      ///< mA
    uint8_t             batteryCharge;       ///< %charged or exception code
    PACKED union
    {
        CotaClientStatus_t  statusFlags;        ///< Status flags
        CotaTrackerStatus_t trackerStatusFlags; ///< Status flags for ForeverTracker
    };
} CotaStatusStd_t;

/** cota query data; see #PRX_CLT_MSG_STATUS_STD */
typedef PACKED struct
{
    CotaClientQueryHeader_t  header;  ///< The header for the client query
    CotaStatusStd_t          status;  ///< The status for the client query
} ClientQueryDataStd_t;

/** cota query data; see #PRX_CLT_MSG_STATUS_CSTM */
typedef PACKED struct
{
    CotaClientQueryHeader_t  header; ///< The header for the client query
    CotaStatusStd_t          status; ///< The status for the client query
    uint8_t                  size;   ///< Size in bytes of the extended data
    uint8_t                  data[MAX_CUSTOM_DATA_SIZE]; ///< The extended data
} ClientQueryDataCstm_t;

/**
 * @brief Contains client profile information
 */
typedef PACKED struct
{
    uint16_t  chargeProfile0;   ///< Basic: Reserved to be assigned by Ossia.  Base is a profile supported by any chager, set to 0 if there is no universal profile
    uint16_t  chargeProfile1;   ///< Common: Reserved to be assigned by Ossia  Common is a profile supported by most chargers with last major release, set to 0 if there is no Common profile
    uint16_t  chargeProfile2;   ///< Advanced: Reserved to be assigned by Ossia  Advanced is a profile supported by a limited subset of chargers or only with those with update some last major release, set to 0 if there is no Advanced profile
    uint16_t  dataProfile0;     ///< Basic: Reserved to be assigned by Ossia  @sa Charger Profile
    uint16_t  dataProfile1;     ///< Common: Reserved to be assigned by Ossia  @sa Charger Profile
    uint16_t  dataProfile2;     ///< Advanced: Reserved to be assigned by Ossia  @sa Charger Profile
} ClientProfiles_t;

/**
 * @brief A command payload buffer
 */
typedef PACKED struct
{
    uint8_t  bytes[MAX_CLIENT_CMD_PAYLOAD_SIZE];  ///< payload buffer
} CotaClientCmdPayload_t;

typedef PACKED struct
{
    uint8_t  bytes[MAX_PROXY_CMD_PAYLOAD_SIZE];  ///< payload buffer
} CotaProxyCmdPayload_t;

/** Response from client application */
typedef PACKED struct
{
    uint8_t data[MAX_APP_MESSAGE_SIZE]; ///< The data in the app message
} CotaAppData_t;

/** Reqeust from receiver for response form proxy */
typedef PACKED struct
{
    ExtdAddr_t extAddr;
} CotaAnnouncement_t;


/**
 * @brief TPS schedule table entry tone and power parameters
 * 
 * In Venus systems, Tone Power Schedule (TPS) is mechanism for coordinating between the receivers (clients) 
 * and the transmitter such that a client would know when to tone (which tone slot has been assigned to 
 * a client) and when it should expect to receive power (which power slot has been allocated for a specific 
 * client).  In the current design, client`s toning and power delivery are decoupled such that a client 
 * does not need to tone in order to receive power; however, the implementation needs make sure that a 
 * client should tone at least once in a TPS prior to power being sent to that client to ensure that power 
 * is focused at the current client`s location.
 * 
 * EXAMPLE 1: The following is a system that has 2 clients and all have the same priority. Assume the number 
 * of power slots between tones is 5.
 * 
 * Fields                       | Value     | Notes
 * -----------------------------|-----------|------
 * Total Clients                | 2         | .
 * Repeat Period                | 2         | .
 * Short Address( 1st Client)   | 0x0123    | .
 * Tone Slot                    | 1         | .
 * Tone Period                  | 2         | .
 * Power Slot                   | 1         | .
 * Power Period                 | 10        | .
 * Power Duration               | 5         | .
 * Short Address( 2nd Client)   | 0x1234    | .
 * Tone Slot                    | 2         | .
 * Tone Period                  | 2         | .
 * Power Slot                   | 6         | .
 * Power Period                 | 10        | .
 * Power Duration               | 5         | .
 * 
 * 
 * Example 2: The system has 4 clients with 2 different priorities using fixed power duration. First 2 clients 
 * receive more power than the second 2 clients. Assume the number of power slots between tones is 5.
 * 
 * 
 * Fields                       | Value     | Notes
 * -----------------------------|-----------|------
 * Total Clients                | 4         | .
 * Repeat Period                | 6         | .
 * Short Address( 1st Client)   | 0x0123    | .
 * Tone Slot                    | 1         | .
 * Tone Period                  | 4         | Tone every time
 * Power Slot                   | 1         | .
 * Power Period                 | 20        |   Repeat within the repeat period
 * Power Duration               | 5         | .
 * Short Address( 2nd Client)   | 0x1234    | .
 * Tone Slot                    | 2         | .
 * Tone Period                  | 8         |   Tone every other time
 * Power Slot                   | 6         | .
 * Power Period                 | 20        |   Repeat within the repeat period
 * Power Duration               | 5         | .
 * Short Address( 3rd Client)   | 0x2345    | .
 * Tone Slot                    | 3         | .
 * Tone Period                  | 4         |   Tone every time
 * Power Slot                   | 11        | .
 * Power Period                 | 30        |   Does not repeat within the period
 * Power Duration               | 5         | .
 * Short Address( 4th Client)   | 0x4567    | .
 * Tone Slot                    | 4         | .
 * Tone Period                  | 8         | Tone every other time
 * Power Slot                   | 16        | .
 * Power Period                 | 30        | .
 * Power Duration               | 5         | .
 * 
 */
typedef PACKED struct 
{
    uint8_t     toneStartSlot;                        ///< Starting tone slot
    uint8_t     tonePeriod;                           ///< Repeat interval
    uint8_t     powerStartSlot;                       ///< Starting power slot
    uint8_t     powerPeriod;                          ///< Repeat interval
    uint8_t     powerSlotsUsed;                       ///< Number of consecutive slots to send power
} CotaTpsSchTableEntryTonePower_t;

/** TPS schedule table entry */
typedef PACKED struct 
{
    ShrtAddr_t                       clientShortAddr; ///< The client short address assigned to the client
    CotaTpsSchTableEntryTonePower_t  tpParams;        ///< Miscellaneous tone and power parameters
} CotaTpsSchTableEntry_t;

/** TPS table from host; see #HOST_PRX_TPS_TABLE */
typedef PACKED struct
{
    CotaTpsMsgHdr_t         header;                       ///< The header for TPS table mmessage
    CotaTpsSchTableEntry_t  tpsSchEntry[MAX_TPS_CLIENTS]; ///< The schedule for the TPS
} CotaTpsTableMsg_t;

/**
 *   Tone power configuration (TPC) is a one-time configuration after a client joins the network, @sa HOST_PRX_TONE_POWER_CONFIG  
 *   Note:  Client short address is not transmitted to the client as a part of the payload.  @sa PrxHostTonePowerConfig_t for
 *   the data coming from host, which includes the short client address.
 */
typedef PACKED struct
{
    uint16_t    toneSlotDuration;                     ///< Tone slot duration in us units
    uint16_t    powerSlotDuration;                    ///< Power Slot duration in us units
    uint8_t     powerSlotCount;                       ///< Number of power slots between tones
    uint8_t     totalTones;                           ///< Total number of tone times in a TPS 
    uint8_t     beaconEncoding[NUM_ENCODING_BYTES];   ///< 64 bits beacon encoding pattern. Default is set to all 0.
} CotaTonePowerConfig_t;


/** Receiver Configuration message - see #PRX_CLT_MSG_RCVR_CONFIG */
typedef PACKED struct
{
    ShrtAddr_t  shortAddr;                            ///< Short address assigned to the receiver
    uint16_t    toneSlotDuration;                     ///< Tone slot duration in us units
    uint16_t    powerSlotDuration;                    ///< Power Slot duration in us units
    uint8_t     powerSlotCount;                       ///< Number of power slots between tones (or following tone slots)
    uint8_t     totalTones;                           ///< Total number of tone slots in a TPS cycle
    int8_t      commPwrLevel;                         ///< Comm power level (-21 to +5 dBm)
    uint8_t     trackerSleepDelay;                    ///< Time in seconds from sleep request to actual tracker transition to sleep state
    uint8_t     stopChargingSocLevel;                 ///< Battery SOC level at/above which the receiver will stop requesting power from the Tx
} CotaRcvrConfig_t;

/**
 * Receiver Configuration Response message
 * See #PRX_HOST_RCVR_CONFIG_RSP and #PRX_CLT_MSG_RCVR_CONFIG_RSP
 */
typedef PACKED struct
{
    uint32_t  fwVersion;   ///< FW version
    uint32_t  hwModel;     ///< HW model
} CotaConfigRsp_t;

/** Go message; see #HOST_PRX_RCVR_GO and #PRX_CLT_MSG_RCVR_GO */
typedef PACKED struct
{
    ShrtAddr_t  shortAddr;   ///< Receiver short address
} CotaRcvrGo_t;


//////////////////////////////////
// OTA Firmware Update Messages //
//////////////////////////////////

#define FW_UPDATE_MAX_SEGMENT_PAYLOAD_SIZE  95          ///< Maximum segment payload size used for FW Segment Download messages
#define FW_UPDATE_SECRET_ERASE_KEY          0xABCDEF01  ///< This code must be sent with the Erase command to confirm the command
#define FW_IMAGE_START_MAGIC_WORD           0x0551A07A  ///< ("OSSIAOTA") - marks the beginning of the OTA FW image
#define INVALID_FW_CRC_VALUE                0           ///< Value returned as an invalid CRC value the FW reference or calculated CRC-32 checksum
#define INVALID_SEGMENT_CRC_VALUE           0           ///< Value returned as an invalid CRC value for the last downloaded FW image segment
#define INVALID_FW_TIMESTAMP_VALUE          0           ///< Value returned as an invalid value for the FW image timestamp
#define INVALID_FW_VERSION_VALUE            0           ///< Value returned as an invalid value for the FW version
#define OTA_FW_IMG_START_ADDRESS            0x20000     ///< Starting address of the OTA FW image
#define FACTORY_FW_IMG_ID_ADDRESS           0x40        ///< Starting address of the factory FW image identifier structure
#define FW_IMG_SIZE_FIELD_SIZE              3           ///< The size of the fwImgSize field of the #CotaFwUpdateSetup_t type in bytes

/**
 * FW image ID (fwId) allowed values for #CotaFwActivate_t
 * See #PRX_CLT_MSG_FW_ACTIVATE
 */
typedef enum
{
    COTA_FW_IMAGE_TYPE_FACTORY,  ///< Facotry FW image
    COTA_FW_IMAGE_TYPE_OTA,      ///< OTA updated FW image
    COTA_FW_IMAGE_TYPE_COUNT     ///< Total number of possible FW images
} CotaFWImageType_t;


/**
 * @brief  Last requested FW Update operation success value
 *
 * This is used as bit 0 of the status field in #CotaFwStatusRsp_t
 * See #PRX_CLT_MSG_FW_STATUS_RSP message
 */
typedef enum
{
    FW_UPDATE_OP_STATUS_FAILURE = 0,   ///< Last FW update operation was a failure
    FW_UPDATE_OP_STATUS_SUCCESS = 1,   ///< Last FW update operation was a success or no operation was requested
    FW_UPDATE_OP_STATUS_COUNT          ///< The total number of values for last FW update operation success/failure flag
} CotaFwUpdLastOpSuccess_t;


/** Build timestamp encoded as a 32-bit integer */
typedef PACKED union _Timestamp_t
{
    uint32_t timestamp;        ///< 32-bit packed timestamp
    struct
    {
        uint32_t seconds : 6;  ///< Seconds
        uint32_t minutes : 6;  ///< Minutes
        uint32_t hours   : 5;  ///< Hours
        uint32_t day     : 5;  ///< Day
        uint32_t month   : 4;  ///< Month
        uint32_t year    : 6;  ///< Years since 2000
    };
} Timestamp_t;
    

/** OTA FW image header placed at address
 *  OTA_FW_IMG_START_ADDRESS by the application code */
typedef PACKED struct _FwImageHeaderOta_t
{
    uint32_t    magicValue;   ///< Identifier of the start of the FW image header; must be set to FW_IMAGE_START_MAGIC_WORD
    uint32_t    crcRef;       ///< CRC-32 checksum of the FW image, which is used as a reference value and comes with the FW image file
    uint32_t    crcCalc;      ///< The value of the CRC-32 checksum of the FW image file calculated after the FW image has been programmed
    uint32_t    size;         ///< Size of FW image, starting with the beginning of this header
    uint32_t    version;      ///< Firmware image version
    Timestamp_t timestamp;    ///< FW build timestamp
} FwImageHeaderOta_t;


/** Factory FW image identifier placed at address
 *  FACTORY_FW_IMG_ID_ADDRESS by the application code */
typedef PACKED struct _FwImageIdentifierFactory_t
{
    uint32_t    version;      ///< Firmware image version
    Timestamp_t timestamp;    ///< FW build timestamp
} FwImageIdentifierFactory_t;


/**
 * @brief  Last FW update operation status bitfield
 *
 * This is used in #CotaFwStatusRsp_t
 * See #PRX_CLT_MSG_FW_STATUS_RSP
 */
typedef union
{
    uint8_t  status;           ///< FW update status

    /** Bitfield for FW update status */
    PACKED struct
    {
        uint8_t success   : 1;   ///< A success/failure value of type #CotaFwUpdLastOpSuccess_t
        uint8_t lastOp    : 3;   ///< Last FW update operation of type #CotaFwUpdLastOp_t
        uint8_t lastError : 4;   ///< Error code of type #CotaFwUpdLastOpError_t for the last operation
    };
} CotaFWUpdateLastOpStatus_t;


/**
 * FW Update Setup message
 * See #PRX_CLT_MSG_FW_UPDATE_SETUP
 */
typedef PACKED struct
{
    uint16_t  panId;                              ///< PAN ID for firmware updates
    uint16_t  updaterShortId;                     ///< Short ID of the FW updating device
    uint16_t  rcvrShortId;                        ///< The short ID to be assiged to the receiver
    int8_t    rcvrCommPwr;                        ///< Comm power level of the receiver
    uint8_t   fwImgSize[FW_IMG_SIZE_FIELD_SIZE];  ///< Firmware image size in bytes
    uint8_t   fwImgSegmSize;                      ///< Firmware image download segment size in bytes
    uint16_t  fwImgSegmCount;                     ///< Firmware image download segment count
    uint32_t  fwImgCrcRef;                        ///< Reference CRC-32 checksum for the downloaded FW image
} CotaFwUpdateSetup_t;


/**
 * FW Segment Download message
 * See #PRX_CLT_MSG_FW_SEGMENT_DOWNLOAD
 */
typedef PACKED struct
{
    uint16_t  segmId;                                            ///< Downloaded segment number
    uint8_t   payloadSize;                                       ///< The size of the payload
    uint8_t   payloadData[FW_UPDATE_MAX_SEGMENT_PAYLOAD_SIZE];   ///< The short ID to be assiged to the receiver
} CotaFwSegmentDownload_t;


/**
 * FW Segment Download message
 * See #PRX_CLT_MSG_FW_SEGMENT_DOWNLOAD_ACK
 */
typedef PACKED struct
{
    CotaFwUpdLastOpSuccess_t  status;        ///< Last segment download status
    uint16_t                  lastSegmId;    ///< Last downloaded segment number
    uint32_t                  lastSegmCrc;   ///< The CRC-32 checksum of the last downloaded segment
} CotaFwSegmentDownloadAck_t;


/**
 * FW Status Response message
 * See #PRX_CLT_MSG_FW_STATUS_RSP
 */
typedef PACKED struct
{
    CotaFWUpdateLastOpStatus_t status;         ///< Last FW update operation status
    CotaFWImageType_t          activeFw;       ///< Currently running firmware
    uint32_t                   factoryFwVer;   ///< Factory FW version
    Timestamp_t                factoryFwTime;  ///< Factory FW timestamp
    uint32_t                   otaFwVer;       ///< OTA FW image version
    Timestamp_t                otaFwTime;      ///< OTA FW image timestamp
    uint32_t                   otaFwCrcRef;    ///< Reference CRC-32 checksum
    uint32_t                   otaFwCrcCalc;   ///< Calculated CRC-32 checksum

    /** Bitfield for battery SOC and battery charging status */
    PACKED struct
    {
        uint8_t soc           : 7;  ///< Battery state of charge
        uint8_t wiredCharging : 1;  ///< Is battery plugged into a wired charger?
    };
} CotaFwStatusRsp_t;


/**
 * FW Activate message
 * See #PRX_CLT_MSG_FW_ACTIVATE
 */
typedef PACKED struct
{
    CotaFWImageType_t  fwId;   ///< FW image to activate
} CotaFwActivate_t;


/**
 * FW Erase message
 * See #PRX_CLT_MSG_FW_ERASE
 */
typedef PACKED struct
{
    uint32_t  eraseKey;   ///< Erase key; must be equal to #FW_UPDATE_SECRET_ERASE_KEY
} CotaFwErase_t;


/**
 * Last requested FW Update operation
 * This is used as bits 1 - 3 for the status field in #CotaFwStatusRsp_t
 * See #PRX_CLT_MSG_FW_STATUS_RSP message
 */
typedef enum
{
    FW_UPDATE_LAST_OP_NONE,               ///< Initial value after boot-up
    FW_UPDATE_LAST_OP_STATUS_REQ,         ///< Last FW update operation was Status Request (only used when other operations were successful)
    FW_UPDATE_LAST_OP_UPDATE_SETUP,       ///< Last FW update operation was Setup
    FW_UPDATE_LAST_OP_SEGMENT_DOWNLOAD,   ///< Last FW update operation was Download Segment
    FW_UPDATE_LAST_OP_ACTIVATE,           ///< Last FW update operation was Activate FW image
    FW_UPDATE_LAST_OP_ERASE,              ///< Last FW update operation was Erase OTA FW image
    FW_UPDATE_LAST_OP_COUNT               ///< The total number of values for last FW update operations
} CotaFwUpdLastOp_t;


/**
 * @brief  Error code of the last FW update command execution
 *
 * This is used as bits 4 - 7 of the status field in #CotaFwStatusRsp_t
 * See #PRX_CLT_MSG_FW_STATUS_RSP message
 */
typedef enum
{
    FW_UPDATE_OP_ERROR_NONE,                       ///< Last FW update operation had not failures
    FW_UPDATE_OP_ERROR_INVALID_ERASE_KEY,          ///< Invalid erase key was specified in the FW Erase message
    FW_UPDATE_OP_ERROR_ERASE_FAILED,               ///< Erase flash memory operation resulted in a failure
    FW_UPDATE_OP_ERROR_FLASH_WRITE_FAILED,         ///< Write to flash memory operation resulted in a failure
    FW_UPDATE_OP_ERROR_FLASH_WRITE_BEFORE_ERASE,   ///< Attempted to write to flash memory before erasing it
    FW_UPDATE_OP_ERROR_INVALID_SEGMEMT_ID,         ///< FW Segment Download message contained an invalid segment ID/number
    FW_UPDATE_OP_ERROR_INVALID_FW_ID,              ///< Invalid FW ID was specified in FW Activate message
    FW_UPDATE_OP_ERROR_FACTORY_FW_NOT_ACTIVATED,   ///< FW Activate message resulted in the failure to jump to the factory FW image
    FW_UPDATE_OP_ERROR_OTA_FW_NOT_ACTIVATED,       ///< FW Activate message resulted in the failure to jump to the OTA FW image
    FW_UPDATE_OP_ERROR_OTA_FW_CRC_CHECK,           ///< CRC check of the OTA FW image failed
    FW_UPDATE_OP_ERROR_FW_IMG_SIZE,                ///< FW downlaod image size was too large
    FW_UPDATE_OP_ERROR_SEGMENT_SIZE,               ///< FW download segment size was too large
    FW_UPDATE_OP_ERROR_INVALID_OP_FOR_FW_IMG,      ///< Invalid/unsupported operation for the currently running FW image was specified
    FW_UPDATE_OP_ERROR_COUNT                       ///< The total number of values for the last FW update operation success/failure flag
} CotaFwUpdLastOpError_t;



// Public variables ---------------------------------------------------------

// Public function prototypes -----------------------------------------------

#endif   // COTA_COMMON_TYPES_H
