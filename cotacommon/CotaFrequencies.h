/**
 * @file       CotaFrequencies.h
 * 
 * @brief      COTA frequencies used for data communication, beacons and power transmission.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2018-2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 * @warning Preliminary Code. Check with Ossia frequently for updates.
 *
 */

// Define to prevent recursive inclusion ------------------------------------
#ifndef COTAFREQUENCIES_H
#define COTAFREQUENCIES_H

// Public define ------------------------------------------------------------

// Channel number for data communication over IEEE 802.15.4
#define COMM_CHANNEL_A              24           ///< Channel number for channel A
#define COMM_CHANNEL_B              25           ///< Channel number for channel B
#define COMM_CHANNEL_C              26           ///< Channel number for channel C
#define COMM_CHANNEL_D              13           ///< Channel number for channel D
#define COMM_CHANNEL_E              12           ///< Channel number for channel E
#define COMM_CHANNEL_F              11           ///< Channel number for channel F
                                    
#define COTA_MIN_COMM_CHANNEL       11           ///< Lowest IEEE 802.15.4 channel used by Cota for data communication
#define COTA_MAX_COMM_CHANNEL       26           ///< Highest IEEE 802.15.4 channel used by Cota for data communication

#define COTA_MIN_COMM_FREQ_MHZ      2405         ///< Lowest IEEE 802.15.4 frequency used by Cota for data communication, in MHz
#define COTA_MAX_COMM_FREQ_MHZ      2480         ///< Highest IEEE 802.15.4 frequency used by Cota for data communication, in MHz
#define COTA_COMM_FREQ_SPACING_MHZ  5            ///< Spacing between IEEE 802.15.4 channels used by Cota, in MHz
#define COTA_MIN_COMM_FREQ_HZ       2405000000   ///< Lowest IEEE 802.15.4 frequency used by Cota for data communication, in Hz
#define COTA_MAX_COMM_FREQ_HZ       2480000000   ///< Highest IEEE 802.15.4 frequency used by Cota for data communication, in Hz
#define COTA_COMM_FREQ_SPACING_HZ   5000000      ///< Spacing between IEEE 802.15.4 channels used by Cota, in Hz

// NOTE:  This acually sets the beacon frequency, which for a Sirius system is 100kHz lower than the power Tx frequency
#define BEACON_FREQ_A           2460000000ul     ///<  Beacon Frequency in Hz for #COMM_CHANNEL_A
#define BEACON_FREQ_B           2450000000ul     ///<  Beacon Frequency in Hz for #COMM_CHANNEL_B
#define BEACON_FREQ_C           2440000000ul     ///<  Beacon Frequency in Hz for #COMM_CHANNEL_C
#define BEACON_FREQ_D           2440000000ul     ///<  Beacon Frequency in Hz for #COMM_CHANNEL_D
#define BEACON_FREQ_E           2460000000ul     ///<  Beacon Frequency in Hz for #COMM_CHANNEL_E
#define BEACON_FREQ_F           2420000000ul     ///<  Beacon Frequency in Hz for #COMM_CHANNEL_F

#define BEACON_FREQ_MHZ_A       2460ul           ///<  Beacon Frequency in MHz for #COMM_CHANNEL_A
#define BEACON_FREQ_MHZ_B       2450ul           ///<  Beacon Frequency in MHz for #COMM_CHANNEL_B
#define BEACON_FREQ_MHZ_C       2440ul           ///<  Beacon Frequency in MHz for #COMM_CHANNEL_C
#define BEACON_FREQ_MHZ_D       2440ul           ///<  Beacon Frequency in MHz for #COMM_CHANNEL_D
#define BEACON_FREQ_MHZ_E       2460ul           ///<  Beacon Frequency in MHz for #COMM_CHANNEL_E
#define BEACON_FREQ_MHZ_F       2420ul           ///<  Beacon Frequency in MHz for #COMM_CHANNEL_F

#define DEFAULT_BEACON_FREQ_MHZ 2450ul           ///< Default beacon frequency
// Public variables ---------------------------------------------------------

// Public function prototypes -----------------------------------------------

#endif /* COTAFREQUENCIES_H */
