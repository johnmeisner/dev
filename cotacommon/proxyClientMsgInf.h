/**
 * @file       proxyClientMsgInf.h
 *
 * @brief      The header file defining the Cota proxy/receiver message interface
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2015 - 2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


// Define to prevent recursive inclusion ------------------------------------
#ifndef PROXY_CLIENT_MSG_IF_H
#define PROXY_CLIENT_MSG_IF_H

#ifdef __cplusplus
extern "C"
{
#endif

// Includes -----------------------------------------------------------------
#include "CotaCompiler.h"
#include "CotaCommonTypes.h"
#include "stdint.h"

// Public define ------------------------------------------------------------*/
#define TPX_MAX_CLIENTS                     (8)     ///< Maximum number of cota clients
#define PXY_CLIENT_CUSTOM_STATUS_MAX_DATA   (64)    ///< Always keep this as asmall as possible!
#define PXY_CLIENT_XTND_STATUS_MAX_DATA     (32)    ///< Always keep this as asmall as possible!

// Public macros ------------------------------------------------------------

// Public typedef -----------------------------------------------------------

/** Messages exchanged between proxy and receivers */
typedef enum
{
    PRX_CLT_MSG_DISC_REQ                = 0x00,       ///< Discovery request message sent to the client
    PRX_CLT_MSG_DISC_RESP               = 0x02,       ///< Response to the discovery message, from the client
    PRX_CLT_MSG_JOIN_REQ                = 0x01,       ///< Join request sent to the client
    PRX_CLT_MSG_JOIN_RESP               = 0x09,       ///< Response to the join request, from the client
    PRX_CLT_MSG_DISCONNECT_REQ          = 0x0A,       ///< Disconnect request sent to the client
    PRX_CLT_MSG_TPS                     = 0x03,       ///< Tone Power Schedule sent to the client
    PRX_CLT_MSG_CQT                     = 0x04,       ///< Client Query Table entry sent to the client
    PRX_CLT_MSG_STATUS_STD              = 0x05,       ///< Standard minimal status message received from the client
    PRX_CLT_MSG_STATUS_CSTM             = 0x06,       ///< Custom status message (must include all of PRX_CLT_MSG_STATUS_STD) received from the client
    PRX_CLT_MSG_STATUS_OPTIMIZED        = 0x07,       ///< Compressed data from the standard query and optional custom data
    PRX_CLT_MSG_TP_CONFIG               = 0x08,       ///< Tone Power Configuration message sent to the client
    PRX_CLT_MSG_GO                      = 0x0B,       ///< Go message used to synchronize receivers and transmitters for Venus/Mars/Orion systems
    PRX_CLT_MSG_APP_CMD                 = 0x0C,       ///< Generic app command
    PRX_CLT_MSG_APP_RESP                = 0x0D,       ///< Response to the generic app command from application
    PRX_CLT_MSG_CS_TEST                 = 0x0E,       ///< Carrier sense test (CCA test) for power message sent to the clients
    PRX_CLT_MSG_ANNOUNCEMENT            = 0x0F,       ///< Announcement message periodically sent by receivers on trailer trackers
    PRX_CLT_MSG_ANNOUNCEMENT_ACK        = 0x10,       ///< Announcement Acknowledgement message sent to the trailer tracker receivers
    PRX_CLT_MSG_RCVR_CONFIG             = 0x11,       ///< Receiver Configuration message
    PRX_CLT_MSG_RCVR_CONFIG_RSP         = 0x12,       ///< Receiver Configuration Response message
    PRX_CLT_MSG_RCVR_GO                 = 0x13,       ///< Go message used to start a TPS cycle on ForeverTracker receivers
    PRX_CLT_MSG_DOOR_OPEN               = 0x14,       ///< Door Open message
                                        
    PRX_CLT_MSG_FW_UPDATE_SETUP         = 0x16,       ///< FW update setup message
    PRX_CLT_MSG_FW_SEGMENT_DOWNLOAD     = 0x17,       ///< FW segment download message
    PRX_CLT_MSG_FW_SEGMENT_DOWNLOAD_ACK = 0x18,       ///< FW segment download acknowledgement message
    
    PRX_CLT_MSG_FW_STATUS_RQST          = 0x19,       ///< Request for information about FW and system
    PRX_CLT_MSG_FW_STATUS_RSP           = 0x1A,       ///< Information about FW and system
    PRX_CLT_MSG_FW_ACTIVATE             = 0x1B,       ///< Reboots the MCU into the selected FW image
    PRX_CLT_MSG_FW_ERASE                = 0x1C,       ///< Erese secondary FW image command
                                        
    PRX_CLT_MSG_CCA_TEST                = 0xFC,       ///< Used for sending broadcast messages on beacon frequency for CCA test before sending a beacon
    PRX_CLT_MSG_TEST                    = 0xFD,       ///< Used for sending messages to no one in particular for compliance testing measurements
                                        
    PRX_CLT_MSG_PRINTF                  = 0xFE,       ///< not implemented @todo provide support for debug messages to host
    PRX_CLT_MSG_ERR_CODE                = 0xFF,       ///< not implemented @todo provide support for error and warning messages to host
} proxyClientMsgType_t;  

typedef enum {
    PRX_CLT_ERR_STATUS_OK      = 0x00,       ///< OK, no issues
    PRX_CLT_ERR_STATUS_MINOR   = 0x01,       ///< Minor problem
    PRX_CLT_ERR_STATUS_MAJOR   = 0x02,       ///< Serious problem
    PRX_CLT_ERR_STATUS_HALT    = 0x03,       ///< Unrecoverable issue
} proxyClientErrStatus_t;


/** Defines the type of the messages exchanged between receivers and the proxy */
typedef uint8_t PrxCltMsgType_t;

/**
 * @typedef PrxCltDiscoveryMsg_t
 * @brief Discovered client message from client to proxy
 * 
 * When a client is discovered, it must transmit information about its capabilities
 *  and features to the host.
 * 
 * @see PRX_HOST_CLIENT_DISCOVERED for how this is handled in the host
 */
typedef PACKED struct
{
    uint16_t          version;               ///< Client firmware version
    uint32_t          clientHwModel;         ///< Unique model number as assigned by Ossia
    AsicSerialNum_t   asicSerialNumber;      ///< The ASIC has a serial number
    ExtdAddr_t        extAddr;               ///< Client long address
    int8_t            toneBeaconPower;       ///< Tone Beacon Power in dBm
    int8_t            txClientPower;         ///< Maximum power in dBm that the client can output.
    uint8_t           maxClientPower;        ///< Maximum power in dBm that the client can accept.
    uint8_t           minNetRfPower;         ///< Minimum power (dBm) must be received by the client in order to be net positive
    int8_t            maxBatteryCurrent;     ///< Maximum amount of current flowing to the battery in dBmA that the client supports.
    ClientProfiles_t  clientProfiles;        ///< Reserved to be assigned by Ossia.
    uint8_t           clientFlags;           ///< For unknown devices.  (0)  Supports encryption,  (1-7) reserved  @todo replace with custom type.  
} PrxCltDiscoveryMsg_t;

/** Cota join network message */
typedef PACKED struct
{
    uint16_t    proxyPanId;                           ///< Proxy PAN ID
    ShrtAddr_t  proxyShortAddr;                       ///< Proxy short address
} PrxCltDiscMsg_t;

/** Cota join message */
typedef PACKED struct
{
    uint16_t    clientPanId;                          ///< Client PAN ID
    ShrtAddr_t  clientShortAddr;                      ///< Client short address
} PrxCltJoinReqMsg_t;

/** Cota Join Network Response message */
typedef PACKED struct
{
    ExtdAddr_t  extAddr;                              ///< Client long address
    uint8_t     status;                               ///< Status (1=Success; 0=Failure)
} PrxCltJoinRspMsg_t;

/** Cota Disconnect message */
typedef PACKED struct
{
    ShrtAddr_t  clientShortAddr;                      ///< Client short address
} PrxCltDisconnectReqMsg_t;

/** Cota go message */
typedef PACKED struct
{
    uint8_t tpsSeqNum;                                ///< Sequency number of this go message
} PrxCltTpsGoMsg_t;

/** Cota client query message - see #PRX_CLT_MSG_CQT */
typedef PACKED struct
{
    CotaRcvrQuery_t queryType;                        ///< Query type
} PrxCltQueryMsg_t;

/**
 *  cota client custom status, See PRX_CLT_MSG_STATUS_XTND
 *  @note keep order intact as it help with building the host proxy packet
 */
typedef PACKED struct
{
    uint8_t  customLength;                                ///< length of the custom area
    uint8_t  customData[PXY_CLIENT_XTND_STATUS_MAX_DATA]; ///< not to exceed
} ProxyClientStatusXtnd_t;

/** Generic client command message #PRX_CLT_MSG_CLIENT_CMD */
typedef PACKED struct
{
    uint8_t                 payloadSize;   ///< payload size
    CotaClientCmdPayload_t  payload;       ///< payload, including command type and included data
} PrxCltCmdMsg_t;

/** 
 * @brief Optimized receiver query data;
 * @sa    #PRX_CLT_MSG_STATUS_OPTIMIZED and #RCVR_QUERY_OPTIMIZED
 */
typedef PACKED struct
{
    uint8_t          linkQuality;                 ///< Link quality
    int8_t           rssi;                        ///< RSSI value (dbm)
    CotaStatusStd_t  status;                      ///< Status fields
    uint8_t          data[MAX_CUSTOM_DATA_SIZE];  ///< Custom data
} PrxCltQueryOptimized_t;

/** Announcement ACK message - see #PRX_CLT_MSG_ANNOUNCEMENT_ACK */
typedef PACKED struct
{
    uint16_t    proxyPanId;                       ///< Proxy PAN ID
    ShrtAddr_t  proxyShortAddr;                   ///< Proxy short address
} PrxCltAnnouncementAck_t;


/** 802.15.4 MAC payload */
typedef PACKED struct
{
    PrxCltMsgType_t msgType;
    union 
    {
        PrxCltDiscMsg_t            discoveryMsg;            ///< A message from the proxy to the client inviting the client to get discovered and report its capabilities
        PrxCltDiscoveryMsg_t       discoveryRspMsg;         ///< A response message from the client to the proxy where the client reports its capabilities
        PrxCltJoinReqMsg_t         joinClientCmdMsg;        ///< A command from the proxy to the client to join the network
        PrxCltJoinRspMsg_t         joinClientRspMsg;        ///< A response message from the client to the proxy when the client has joined the network
        PrxCltDisconnectReqMsg_t   disconnectClientCmdMsg;  ///< A command from the proxy to the client to join the network
        CotaTpsTableMsg_t          tpsMsg;                  ///< A broadcast message from the proxy to all the clients to communicate a new TPS schedule
        PrxCltTpsGoMsg_t           tpsGoMsg;                ///< A broadcast message from the proxy to all the clients to sync the start of a new TPS cycle for TSP schedule with a matching sequence number
        CotaTonePowerConfig_t      tpConfig;                ///< A message from the proxy to the client to configure timing parameters
        PrxCltQueryMsg_t           cqtMsg;                  ///< A message from the proxy to the client to query its state
        ClientQueryDataStd_t       clientQueryDataStd;      ///< A response message from the client to the standard query sent by proxy
        ClientQueryDataCstm_t      clientQueryData;         ///< A response message from the client to the custom query sent by proxy
        PrxCltQueryOptimized_t     clientQueryResp;         ///< Optimized query data response message used on ForeverTracker
        PrxCltCmdMsg_t             clientCmdMsg;            ///< A message from proxy to client containing a generic command payload
        CotaAppData_t              clientAppRspMsg;         ///< A response message from client application to the proxy in response to a generic command message
        CotaRcvrConfig_t           rcvrConfig;              ///< Receiver Configuration message
        CotaConfigRsp_t            configRsp;               ///< Receiver Configuration Response message from the receiver
        CotaRcvrGo_t               goMsg;                   ///< Go message for Xirgo/TrailerTracker/ForeverTracker
        CotaFwUpdateSetup_t        fwUpdateSetup;           ///< FW Update Setup message used to establish a network and prepare Rx for FW updates
        CotaFwActivate_t           fwActivate;              ///< FW Activate message used to activate factory or OTA FW image
        CotaFwStatusRsp_t          fwStatusRsp;             ///< FW Status Response message used in response to FW Update Setup and FW Status Request messages
        CotaFwErase_t              fwErase;                 ///< FW Erase message
        CotaFwSegmentDownload_t    fwSegmDownload;          ///< FW Segement Download command used for FW updates
        CotaFwSegmentDownloadAck_t fwSegmDownloadAck;       ///< FW Segment Download ACK message used to acknowledge FW Segement Download commands
    };
} ProxyClientMacPayload_t;
   
// Public variables ---------------------------------------------------------

// Public function prototypes -----------------------------------------------

#ifdef __cplusplus
}
#endif

#endif /* PROXY_CLIENT_MSG_IF_H */
