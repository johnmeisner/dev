/**
 * @file       CotaCompiler.h
 *
 * @brief      Cota compiler-specific definitions
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2015-2020 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 */


/* Define to prevent recursive inclusion ------------------------------------*/
#ifndef COTA_COMPILER_H
#define COTA_COMPILER_H


/* Public define ------------------------------------------------------------*/

#define COTA_STATIC_ASSERT(condition) ((void)sizeof(char[1 - 2*!(condition)]))  ///< Portable alternative to static_assert().  Call from inside a function only

// Compiler-specific aliases 
#if defined(ewarm) || defined(__IAR_SYSTEMS_ICC__)
    // This is used by the IAR compiler for building FW for the target device
    #define PACKED        __packed                  ///< IAR compiler pragma for packed structure
#elif UNITY_GCC
    // UNITY_GCC does not work for the unity test because the syntax
    // between the IAR and GCC is inconsistent. IAR does the following.
    //typedef __packed struct
    // while GCC does the following
    //typedef struct __attribute__((packed))

    // #define PACKED   __attribute__((packed))        ///< Type attribute for GCC
    #define PACKED 
#else
    #error "No compiler defined!"                   ///< Compiler-specific values must be defined
#endif

#define COTA_PACKED   PACKED                        ///< @note This may be compiler-specific @todo may need to be replaced with compiler specific value.

#endif   // #ifndef COTA_COMPILER_H
