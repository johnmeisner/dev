/**
 * @file  proxyHostMsgInf.h
 *
 * @brief Cota proxy-host message interface header file
 *
 * This file has the function prototypes of Cota proxy-host interface module.
 * All communications between the Cota Daemon and the clients are done thru the proxy board. The proxy is connected to
 * a host system through a SPI bus. The proxy firmware and proxy driver on the host system leverage the TI Network
 * Processor Interface (NPI) protocol to send messages to each other.  See NPI user guide for more info on the protocol.
 *
 * Proxy Message Format
 * ====================
 *
 *  All messages sent or received
 *   from the proxy will need to conform to the following format. The proxy driver calculate and compare check
 *   sums on all messages received. Any message that fails the check sum test will be discarded at the driver level.
 *
 *  Content         | Bytes | Definitions
 *  ----------------|-------|------------
 *  SOF             |   1   | Start of frame = 0xFE
 *  Length          |   1   | Length of the entire message including message type and payload
 *  Message Type    |   1   | Message type defined in table below
 *  Message Payload |   n   | Message payload
 *  FCS             |   1   | Frame check sum cover length, message type, and message payload FCS = XOR (length, message type and payload)
 *
 *
 *  @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA, INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2015-2020 OSSIA, INC. (SUBJECT TO
 *             LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
*/

// Define to prevent recursive inclusion ------------------------------------
#ifndef PROXY_HOST_MSG_INF_H
#define PROXY_HOST_MSG_INF_H



//  Includes -----------------------------------------------------------------
#include "CotaCompiler.h"
#include "CotaCommonTypes.h"

//  Public define ------------------------------------------------------------
#define NUM_CHANNELS             16 ///< The number of 802.15.4 channels
#define DISCOVERY_PERIOD_SINGLE  0  ///< Discovery period value indicating only one Discovery message is to be sent out


//  Public typedef -----------------------------------------------------------
/** Proxy host message enum type */
typedef enum _ProxyHostStatus_t
{
    PRX_HOST_STAT_FAIL      = 0,
    PRX_HOST_STAT_SUCCESS   = 1,
} ProxyHostStatus_t;

/** Proxy host message enum type */
typedef enum
{
    // Commands from host to proxy
    HOST_PRX_START_DISCOVERY      = 0x00,   ///< Start discovery command
    HOST_PRX_STOP_DISCOVERY       = 0x01,   ///< Stop discovery command
    HOST_PRX_JOIN_CLIENT          = 0x02,   ///< Join (AKA assign short address) command
    HOST_PRX_TPS_TABLE            = 0x03,   ///< TPS (tone-power schedule) command
    HOST_PRX_CQ_TABLE             = 0x04,   ///< CQT (client query table) command
    HOST_PRX_GET_PROXY_INFO       = 0x05,   ///< Command to retrieve proxy FW revision and status
    HOST_PRX_TONE_POWER_CONFIG    = 0x06,   ///< TPC (tone-power configuration) message
    HOST_PRX_DISCONNECT_CLIENT    = 0x07,   ///< Disconnect client command
    HOST_PRX_CLIENT_CMD           = 0x08,   ///< Client application command
    HOST_PRX_PROXY_CMD            = 0x09,   ///< Proxy commands
    HOST_PRX_FW_UPD_CMD           = 0x0A,   ///< Firmware update message
    HOST_PRX_FILE_UPLOAD_CMD      = 0x0B,   ///< File upload messages
    HOST_PRX_GET_RSSI_CMD         = 0x0C,   ///< Command to get RSSI measurements for all channels
    HOST_PRX_SET_LR_STATE         = 0x0D,   ///< Set a new light ring state
    HOST_PRX_RCVR_CONFIG          = 0x0E,   ///< Receiver Configuration message
    HOST_PRX_RCVR_GO              = 0x0F,   ///< Go message for individual Xirgo/ForeverTracker receivers
    HOST_PRX_PROXY_SETUP          = 0x10,   ///< Proxy Setup message

    // Response messages from proxy to host
    PRX_HOST_CLIENT_DISCOVERED    = 0x20,   ///< Discovery response message
    PRX_HOST_CLIENT_JOINED        = 0x21,   ///< Join response message
    PRX_HOST_CLIENT_QUERY_DATA    = 0x22,   ///< Client query data
    PRX_HOST_TPS_GO               = 0x23,   ///< TPS confirmation message; sent out either after TPS msg Tx failure or after the Go message is transmitted
    PRX_HOST_PROXY_INFO           = 0x24,   ///< Proxy FW revision and status message
    PRX_HOST_CLIENT_QUERY_CFN     = 0x25,   ///< Client query confirmation message
    PRX_HOST_TPC_CFN              = 0x26,   ///< TPC message confirmation
    PRX_HOST_DISCONNECT_CFN       = 0x27,   ///< Disconnect message confirmation
    PRX_HOST_CLIENT_CMD_CFN       = 0x28,   ///< Client application command confirmation
    PRX_HOST_APP_DATA             = 0x29,   ///< Client application data
    PRX_HOST_FW_UPD_CNF           = 0x2A,   ///< Client FW update confirmation message
    PRX_HOST_FW_UPD_RSP           = 0x2B,   ///< Client FW update response message
    PRX_HOST_FILE_UPLOAD_CNF      = 0x2C,   ///< File upload confirmation message
    PRX_HOST_FILE_UPLOAD_RSP      = 0x2D,   ///< File upload response message
    PRX_HOST_PROXY_INIT_REQUEST   = 0x30,   ///< Request from proxy to host to send Start Discovery command to initialize the proxy
    PRX_HOST_PROXY_DATA           = 0x31,   ///< Reponse from proxy to the command HOST_PRX_PROXY_CMD
    PRX_HOST_GET_RSSI_RSP         = 0x32,   ///< Response from the proxy with RSSI values for all the 802.15.4 channels
    PRX_HOST_DOOR_SW_STATE        = 0x33,   ///< Message from proxy with door state information
    PRX_HOST_ANNOUNCEMENT         = 0x34,   ///< Announcement message forwarded from the receiver to the host
    PRX_HOST_RCVR_CONFIG_RSP      = 0x35,   ///< Receiver Configuration Response message
    PRX_HOST_PROXY_STATUS         = 0x36,   ///< Proxy Status message

    HOST_PRX_SET_LED              = 0xF0,   ///< For debug only, ok to remove if LED test is not needed
    PRX_HOST_NULL                 = 0xF1,   ///< Dummy request sent from proxy to host to flush out any pending NPI messages
    PRX_HOST_ERROR                = 0xFF,   ///< Indicates there was an error in the message transmission.
} ProxyHostMsgType_t;

/** Proxy-host message type def */
typedef uint8_t  PrxHostMsgType_t;

/**
 * @typedef PrxHostDiscoveryMsg_t
 * @brief Discovered client message from proxy to host
 *
 * When a client is discovered, it must transmit information about its capabilities
 *  and features to the host.
 *
 * @see PRX_HOST_CLIENT_DISCOVERED for how this is handled in the host
 */
typedef PACKED struct
{
    uint16_t          version;              ///< Client firmware version
    uint32_t          clientHwModel;        ///< Unique model number as assigned by Ossia
    AsicSerialNum_t   asicSerialNumber;     ///< The ASIC has a serial number
    ExtdAddr_t        extAddr;              ///< Client long address
    uint8_t           linkQuality;          ///< Link quality
    int8_t            rssiRxPower;          ///< RSSI value (dbm) of the packet as received by the proxy from the client
    int8_t            toneBeaconPower;      ///< Tone Beacon Power in dBm
    int8_t            txClientPower;        ///< Maximum power in dBm that the client can accept.
    uint8_t           maxClientPower;       ///< Maximum power in dBm that the client can accept.
    uint8_t           minNetRfPower;        ///< Minimum power (dBm) must be received by the client in order to be net positive
    int8_t            maxBatteryCurrent;    ///< Maximum amount of current flowing to the battery in dBmA that the client supports.
    ClientProfiles_t  clientProfiles;       ///< Reserved to be assigned by Ossia.
    uint8_t           clientFlags;          ///< For unknown devices.  (0)  Supports encryption,  (1-7) reserved  \todo replace with custom type.
} PrxHostDiscoveryMsg_t;

/** LED states */
typedef enum
{
    LED_ON = 0,
    LED_OFF,
    NUM_LED_STATES
} LedOnOff_t;

/** setled value on eval board */
typedef PACKED struct
{
    uint8_t     led;           ///< which LED from 1 to 4, plus 0 as all LEDs
    LedOnOff_t  state;         ///< zero is off, one is on
} PrxHostSetLed_t;

/** Start discovery broadcast message, see #HOST_PRX_START_DISCOVERY */
typedef PACKED struct
{
    ShrtAddr_t  shortAddr;    ///< short address
    uint16_t    panId;        ///< pan id
    uint16_t    period;       ///< period in seconds, or DISCOVERY_PERIOD_SINGLE to issue a single Discovery message
    uint8_t     commChannel;  ///< IEEE 802.15.4 MAC communication channel
} PrxHostStartDiscMsg_t;

// HOST_PRX_STOP_DISCOVERY message has no content

/** assign client short address message #HOST_PRX_JOIN_CLIENT */
typedef PACKED struct
{
    ExtdAddr_t  extAddr;      ///< IEEE 802.15.4 MAC addreesss
    ShrtAddr_t  shortAddr;    ///< client short address
} PrxHostJoinClientMsg_t;

/** disconnect client message #HOST_PRX_DISCONNECT_CLIENT */
typedef PACKED struct
{
    ShrtAddr_t  shortAddr;    ///< client short address
} PrxHostDisconnectClientMsg_t;

/** generic client command message #HOST_PRX_CLIENT_CMD */
typedef PACKED struct
{
    ExtdAddr_t              extAddr;       ///< client long address
    uint8_t                 payloadSize;   ///< payload size
    CotaClientCmdPayload_t  payload;       ///< payload, including command type and included data
} PrxHostClientCmdMsg_t;

/** generic proxy command message #HOST_PRX_PROXY_CMD */
typedef PACKED struct
{
    uint8_t                payloadSize;   ///< payload size
    uint8_t                msgType;       ///< message type
    CotaProxyCmdPayload_t  payload;       ///< payload data
} PrxHostProxyCmdMsg_t;

/** Receiver Query Table entry */
typedef PACKED struct
{
    ShrtAddr_t       shortAddr;    ///< Short address
    CotaRcvrQuery_t  queryType;    ///< Query type
} PrxHostCqTableEntry_t;

/** Query message header */
typedef PACKED struct
{
    uint8_t  totalNumClients;
} PrxHostCqtHdr_t;

/**
 * Client query table from host, see #HOST_PRX_CQ_TABLE
 * Note:  The size of this structure is unknown since the number of clients can vary
 */
typedef PACKED struct
{
    PrxHostCqtHdr_t        header;
    PrxHostCqTableEntry_t  clientQueryEntry[MIN_CQT_CLIENTS];
} PrxHostCqTablePartialMsg_t;

/** Tone power configuration, see #HOST_PRX_TONE_POWER_CONFIG */
typedef PACKED struct
{
    ShrtAddr_t             shortAddress;   ///< Short address of the client
    CotaTonePowerConfig_t  data;           ///< Payload to client
} PrxHostTonePowerConfig_t;

/** Set light ring state command; see #HOST_PRX_SET_LR_STATE */
typedef PACKED struct
{
    LightRingState_t       state;   ///< New light ring state
} PrxHostLightRingMsg_t;

/** Response Messages **/

/** Client join network confirmation message from proxy, See #PRX_HOST_CLIENT_JOINED */
typedef PACKED struct
{
    ExtdAddr_t          extAddr;       ///< long address
    ShrtAddr_t          shortAddr;     ///< short address
    ProxyHostStatus_t   status;        ///< status
} PrxHostClientJoinRspMsg_t;

// No data for Go message, see PRX_HOST_TPS_GO

/** Proxy firmware revision response, see #PRX_HOST_PROXY_INFO */
typedef PACKED struct
{
    uint32_t  revision;               ///< The high 16-bit word represents the FW major version and the low 16-bit word represents the FW minor version.
    PACKED union
    {
        PACKED struct
        {
            uint16_t  crcError     : 1;   ///< CRC error flag; 0=no error, 1=error
            uint16_t  txNotAllowed : 1;   ///< Tx Not Allowed flag
            uint16_t  reserved     : 14;  ///< Reserved for future use
        };
        uint16_t    status;             ///< This field allows access to all flags in the bitfield at the same time
    };
} PrxHostProxyFwInfo_t;

/** Client query confirmation, see #PRX_HOST_CLIENT_QUERY_CFN */
typedef PACKED struct
{
    ShrtAddr_t          shortAddr;     ///< Receiver short address
    uint8_t             queryType;     ///< Requested receiver/client query type
    uint8_t             linkQuality;   ///< Reported link quality
    int8_t              rssi;          ///< Reported RSSI
    ProxyHostStatus_t   status;        ///< Command completion status
    uint16_t            duration;      ///< Command processing duration
} PrxHostCqRecCfn_t;

/** TPC Confirmation, see #PRX_HOST_TPC_CFN */
typedef PACKED struct
{
    ShrtAddr_t          shortAddr;
    ProxyHostStatus_t   status;
} PrxHostTpcCfn_t;

/** Generic Client Command Confirmation, see #PRX_HOST_CLIENT_CMD_CFN */
typedef PACKED struct
{
    ExtdAddr_t          extAddr;       ///< long address
    ProxyHostStatus_t   status;        ///< status
} PrxHostClientCmdCfn_t;

/** Leave Network Confirmation, see #PRX_HOST_DISCONNECT_CFN */
typedef PACKED struct
{
    ShrtAddr_t          shortAddr;     ///< short address
    ProxyHostStatus_t   status;        ///< status
} PrxHostLeaveNetworkCfn_t;

/** Response to generic client command message from the application, see #PRX_HOST_APP_DATA */
typedef PACKED struct
{
    ShrtAddr_t     shortAddr;     ///< short address
    CotaAppData_t  data;          ///< payload data
} PrxHostAppData_t;

/** Response from proxy to the generic proxy command message, see #HOST_PRX_PROXY_CMD */
typedef PACKED struct
{
    uint8_t                payloadSize;   ///< size of payload data
    uint8_t                msgType;       ///< payload data type
    CotaProxyCmdPayload_t  payload;       ///< payload data
} PrxHostPrxCmdResp_t;

/** Status of TPS/Go messages sent to the client - returned by the proxy to the host */
typedef PACKED struct
{
    ProxyHostStatus_t   status;        ///< TPS successfully transmitted status
    uint16_t            duration;      ///< Command processing duration
} PrxHostTpsRsp_t;

/** RSSI information response message from proxy, See #PRX_HOST_GET_RSSI_RSP */
typedef PACKED struct
{
    int8_t   rssi[NUM_CHANNELS];          ///< RSSI values for all 802.15.4 channels
} PrxHostRssiRspMsg_t;

/** Door switch info message, see #PRX_HOST_DOOR_SW_STATE */
typedef PACKED struct
{
    uint8_t  state;                       ///< Immediate shutoff flag for door open/closed state (1=open; 0=closed)
} PrxHostImmediateShutoff_t;

/**
 * @brief  Announcement message forwarded from a receiver to the host
 *
 * See #PRX_HOST_ANNOUNCEMENT
 */
typedef PACKED struct
{
    ExtdAddr_t  extAddr;       ///< long address
} PrxHostAnnouncement_t;

/** Receiver configuration message, see #HOST_PRX_RCVR_CONFIG */
typedef PACKED struct
{
    ExtdAddr_t        extAddr;      ///< long address of receiver
    CotaRcvrConfig_t  config;       ///< Configuration for receiver
} PrxHostRcvrConfig_t;

/** Receiver Configuration Response, see #PRX_HOST_RCVR_CONFIG_RSP */
typedef PACKED struct
{
    ShrtAddr_t          shortAddr;      ///< Receiver short address
    CotaConfigRsp_t     configResp;     ///< Configuration Response message data
    ProxyHostStatus_t   status;         ///< Status flag
} PrxHostRcvrCfgRsp_t;

/** Proxy Setup message, see #HOST_PRX_PROXY_SETUP */
typedef PACKED struct
{
    uint8_t     commChannel;          ///< IEEE 802.15.4 MAC communication channel (11 - 26)
    uint16_t    proxyPanId;           ///< Proxy PAN ID
    ShrtAddr_t  proxyShortAddr;       ///< Proxy short address
    uint16_t    doorOpenMsgPeriodMs;  ///< Door Open message Tx period, in milliseconds (0 = disable Door Open messages)
    uint16_t    announcementDelayMs;  ///< Delay (in milliseconds) before acknowledging and forwarding another announcement message
    int8_t      rssiFilterMinValueDbm;///< The minimum value of RSSI (in dBm) that will allow an announcement message to be acted upon
} PrxHostProxySetup_t;

/** Proxy Status message, see #PRX_HOST_PROXY_STATUS */
typedef PACKED struct
{
    uint32_t  fwVersion;    ///< FW version

    /** Proxy status */
    PACKED union
    {
        /** Proxy status */
        PACKED struct
        {
            uint16_t  crcError   :  1;  ///< CRC error flag; 0 = no error, 1 = error
            uint16_t  initNeeded :  1;  ///< Proxy initialization needed (0 = initialization completed; 1 = initialization needed)
            uint16_t  reserved   : 14;  ///< Reserved for future use
        };
        uint16_t    status;           ///< Proxy status
    };
} PrxHostProxyStatus_t;


/**
 * @brief  host-proxy message payload
 * @note   Proxy initialization request message only has the msgType field, and no other data
 */
typedef PACKED struct
{
    PrxHostMsgType_t msgType;
    union
    {
        PrxHostStartDiscMsg_t        startDiscMsg;
        PrxHostJoinClientMsg_t       joinClientMsg;
        PrxHostDisconnectClientMsg_t disconnectClientMsg;
        CotaTpsTableMsg_t            tpsMsg;
        PrxHostCqTablePartialMsg_t   cqtMsg;
        PrxHostDiscoveryMsg_t        discClientMsg;
        PrxHostClientJoinRspMsg_t    clientJoinRspMsg;
        PrxHostTonePowerConfig_t     tpConfig;
        PrxHostProxyFwInfo_t         proxyFwInfo;
        CotaClientQueryHeader_t      clientQueryDataHdr;
        ClientQueryDataCstm_t        clientQueryMsg;
        PrxHostCqRecCfn_t            clientQueryRecCfn;
        PrxHostTpcCfn_t              clientTpcCfn;
        PrxHostSetLed_t              setLedState;
        PrxHostClientCmdMsg_t        clientCmdMsg;
        PrxHostClientCmdCfn_t        clientCmdCfn;
        PrxHostLeaveNetworkCfn_t     leaveNetworkCfn;
        PrxHostAppData_t             clientAppData;
        PrxHostProxyCmdMsg_t         proxyCmdMsg;
        PrxHostPrxCmdResp_t          proxyCmdRespData;
        PrxHostTpsRsp_t              tpsRsp;
        PrxHostRssiRspMsg_t          rssiRsp;             ///< RSSI measurements for 802.15.4 channels
        PrxHostImmediateShutoff_t    shutoff;             ///< Message to the host that TPS needs to be shut off
        PrxHostLightRingMsg_t        lightRingMsg;        ///< Light ring state change command
        PrxHostAnnouncement_t        announcementMsg;     ///< Announcement message to the host
        PrxHostRcvrConfig_t          rcvrConfigMsg;       ///< Receiver Configuration message
        PrxHostRcvrCfgRsp_t          rcvrConfigRsp;       ///< Receiver Configuration Response message
        PrxHostProxyStatus_t         proxyStatus;         ///< Proxy Status message
        CotaRcvrGo_t                 goMsg;               ///< Go message for Xirgo/TrailerTracker/ForeverTracker
        PrxHostProxySetup_t          proxySetup;          ///< Proxy Setup message
    };
} PrxHostNpiPayload_t;


// Public variables ---------------------------------------------------------

// Public function prototypes -----------------------------------------------

#endif /* PROXY_HOST_MSG_INF_H */
