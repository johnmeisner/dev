// Main task implementation file

#include "mainTask.h"
#include "main.h"
#include "string.h"
#include "cmsis_os.h"
#include "FreeRTOS_CLI.h"
#include <stdio.h>
#include "stm32h7xx_hal.h"

#define MAX_OUTPUT_LENGTH   40
#define MAX_INPUT_LENGTH    20
#define STRING_TERM         "\r\n"  // The character sequence that terminates a string and starts a new one
#define CLIENT_LIST_SIZE    (sizeof(CLIENT_LIST) / sizeof(CLIENT_LIST[0]))
#define CLI_CMD_FIRST_CALL  0       // Initial step of commend execution will start next
#define CLI_CMD_PROC_CALL   1       // Iterative step of commend execution will start next (this is when command processing needs to happen in the same way multiple times) 
#define CLI_CMD_LAST_CALL   -1      // Final step of commend execution is expected


static osThreadId_t gCliTaskHandle;      // CLI task handle
static osSemaphoreId_t UartRxSemHandle;  // Semaphore to block the CLI thread on while waiting for UART entries
static const char * WELCOME_MSG = "Welcome to CLI!" "\r\n" "Type Help to view a list of registered commands.";
static const char * CLIENT_LIST[] = {
    "0x12345678",
    "0xABCDEF00",
    "0xAABBCCDD",
};

extern UART_HandleTypeDef huart3;


static BaseType_t StartChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static void StartCliTask(void *argument);


// This creates the CLI processing task/thread
void CreateCliTask(void)
{
    // Define the CLI processing task
    const osThreadAttr_t defaultTask_attributes = {
      .name = "CLI Task",
      .priority = (osPriority_t) osPriorityNormal,
      .stack_size = 1024
    };

    // Create the CLI task
    gCliTaskHandle = osThreadNew(StartCliTask, NULL, &defaultTask_attributes);
    (void)gCliTaskHandle; // Avoid unused variable compiler warning
}


// The CLI processing task that's registered to be executed by the OS
static void StartCliTask(void *argument)
{
    //Peripheral_Descriptor_t xConsole;
    uint8_t cRxedChar;
    uint8_t cInputIndex = 0;
    BaseType_t xMoreDataToFollow;
    /* The input and output buffers are declared static to keep them off the stack. */
    static char pcOutputString[ MAX_OUTPUT_LENGTH ];
    static char pcInputString[ MAX_INPUT_LENGTH ];

    // Structure that defines the "start_charging" CLI command
    static const CLI_Command_Definition_t cmdStartCharging =
    {
        "start_charging",                                              // The command string to type
        "\r\nstart_charging:\r\n Start charging a given client\r\n",   // Help string
        StartChargingCmdCallback,                                      // The function to run when this command is entered
        1                                                              // Exactly one parameters is expected
    };
    // Register CLI command "start_charging"
    FreeRTOS_CLIRegisterCommand(&cmdStartCharging);
    
    // Create the UART Rx semaphore with count set to 0 (which means that the next call to osSemaphoreAcquire will block)
    const osSemaphoreAttr_t UartRxSem_attributes = {
        .name = "UartRxSem"
    };
    UartRxSemHandle = osSemaphoreNew(1, 0, &UartRxSem_attributes);
    
    /* This code assumes the peripheral being used as the console has already
    been opened and configured, and is passed into the task as the task
    parameter.  Cast the task parameter to the correct type. */
    //xConsole = ( Peripheral_Descriptor_t ) pvParameters;

    /* Send a welcome message to the user knows they are connected. */
    //FreeRTOS_write( xConsole, pcWelcomeMessage, strlen( pcWelcomeMessage ) );
    HAL_UART_Transmit(&huart3, (uint8_t*)WELCOME_MSG, strlen(WELCOME_MSG), HAL_MAX_DELAY);

    for( ;; )
    {
        /* This implementation reads a single character at a time.  Wait in the
        Blocked state until a character is received. */
        //FreeRTOS_read( xConsole, &cRxedChar, sizeof( cRxedChar ) );
        HAL_UART_Receive_IT(&huart3, &cRxedChar, sizeof(cRxedChar));
        osSemaphoreAcquire(UartRxSemHandle, portMAX_DELAY);

        if( cRxedChar == '\r' )
        {
            /* A newline character was received, so the input command string is
            complete and can be processed.  Transmit a line separator, just to
            make the output easier to read. */
            //FreeRTOS_write( xConsole, "\r\n", strlen( \r\n );
            HAL_UART_Transmit(&huart3, (uint8_t*)STRING_TERM, strlen(STRING_TERM), HAL_MAX_DELAY);

            /* The command interpreter is called repeatedly until it returns
            pdFALSE.  See the Implementing a command documentation for an
            exaplanation of why this is. */
            do
            {
                /* Send the command string to the command interpreter.  Any
                output generated by the command interpreter will be placed in the
                pcOutputString buffer. */
                xMoreDataToFollow = FreeRTOS_CLIProcessCommand
                                    (
                                        pcInputString,   /* The command string.*/
                                        pcOutputString,  /* The output buffer. */
                                        MAX_OUTPUT_LENGTH/* The size of the output buffer. */
                                    );

                /* Write the output generated by the command interpreter to the
                console. */
                //FreeRTOS_write( xConsole, pcOutputString, strlen( pcOutputString ) );
                HAL_UART_Transmit(&huart3, (uint8_t*)pcOutputString, strlen(pcOutputString), HAL_MAX_DELAY);

            } while( xMoreDataToFollow != pdFALSE );

            /* All the strings generated by the input command have been sent.
            Processing of the command is complete.  Clear the input string ready
            to receive the next command. */
            cInputIndex = 0;
            memset( pcInputString, 0x00, MAX_INPUT_LENGTH );
        }
        else
        {
            /* The if() clause performs the processing after a newline character
            is received.  This else clause performs the processing if any other
            character is received. */

            if( cRxedChar == '\n' )
            {
                // Ignore carriage returns
                // Note that depending on how your terminal program is set up, you may need to swap \n and \r processing
            }
            else if( cRxedChar == '\b' )
            {
                /* Backspace was pressed.  Erase the last character in the input
                buffer - if there are any. */
                if( cInputIndex > 0 )
                {
                    cInputIndex--;
                    pcInputString[ cInputIndex ] = '\0';
                }
            }
            else
            {
                /* A character was entered.  It was not a new line, backspace
                or carriage return, so it is accepted as part of the input and
                placed into the input buffer.  When a n is entered the complete
                string will be passed to the command interpreter. */
                if( cInputIndex < MAX_INPUT_LENGTH )
                {
                    pcInputString[ cInputIndex ] = cRxedChar;
                    cInputIndex++;
                }
            }
        }
    }
}

// This function is registered with the CLI to be called back when the "start_charging" command needs to be processed
// Note that the snprintf function requires more RAM than the defaul CubeMx configuration allows
static BaseType_t StartChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
  const char *pcParameter;                          // Pointer to the substring with the command parameter within the initial command line
  BaseType_t xParameterStringLength;                // The length of the command parameter
  BaseType_t retVal = pdFALSE;                      // Nothing else to return
  static BaseType_t callState = CLI_CMD_FIRST_CALL; // State machine for processing the command
  static BaseType_t clientId = 0;                   // Client number from client list to process

	/* Remove compile time warnings about unused parameters, and check the
	write buffer is not NULL.  NOTE - for simplicity, this example assumes the
	write buffer length is adequate, so does not check for buffer overflows. */
	( void ) xWriteBufferLen;
	//configASSERT( pcWriteBuffer );

  if (callState == CLI_CMD_FIRST_CALL)
  {
  /* Obtain the parameter string. */
		pcParameter = FreeRTOS_CLIGetParameter(
                    pcCommandString,         // The command string itself
                    1,                       // Return parameter #1
                    &xParameterStringLength  // Obtain the parameter string length
                  );
    
    if (strncmp(pcParameter, "all", strlen("all")) == 0)
		{
      snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "Started charging clients:");
      callState = CLI_CMD_PROC_CALL;
      // This function needs to be called again to print a list of clients
    }
    else
    {
      // Print client name and schedule command execution status reporting in the last call to this function
      snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "Started charging client %s", pcParameter);
      callState = CLI_CMD_LAST_CALL;
    }

    retVal = pdTRUE;
  }
  else if (callState == CLI_CMD_PROC_CALL)
  {
    // Subsequent calls handled here to process a list of clients
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "%s", CLIENT_LIST[clientId]);
    clientId++;
    if (clientId >= CLIENT_LIST_SIZE)
    {
      clientId = 0;
      callState = CLI_CMD_LAST_CALL;
    }
    retVal = pdTRUE;
  }
  else
  {
    // This is the last call to this function to print the command execution status
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "Status: %s" "\r\n", "SUCCESS");
    callState = CLI_CMD_FIRST_CALL;
  }

	// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
	return retVal;
}

// Callback function that's called when UART has received another character from the terminal
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  // Release the semaphore to unblock the CLI processing thread
  osSemaphoreRelease(UartRxSemHandle);
}
