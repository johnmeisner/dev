/****************************************************************************//**
* @file      uart_driver.c
*
* @brief     Code implementing prototypes to provide thread safe access to the UART
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "main.h"
#include "error.h"
#include "string.h"
#include "uart_driver.h"
#include "projdefs.h"

extern UART_HandleTypeDef huart3;                 ///< Pointer to UART structure
static SemaphoreHandle_t gCotaUartMutex;          ///< Mutex protecting the UART calls
static StaticSemaphore_t gCotaUartMutexBuff;      ///< The buffer holding the information for the mutex
static SemaphoreHandle_t gCotaUartRxSem;          ///< Semaphore signaling a complete receive
static StaticSemaphore_t gCotaUartRxSemBuff;      ///< The buffer holding the information for the #gCotaUartRxSem sempahore
static SemaphoreHandle_t gCotaUartWaitForTx;      ///< Semaphore waiting for Uart transmit to complete
static StaticSemaphore_t gCotaUartWaitForTxBuff;  ///< The buffer holding the information for the #gCotaUartWaitForTx sempahore


#ifdef PROXY_TEST
    extern void TxCpltCallback(void);
#endif


/**
 * @brief Create the mutex needed to make UART calls thread safe
 */
void InitCotaUart(void)
{
  gCotaUartMutex = xSemaphoreCreateMutexStatic(&gCotaUartMutexBuff);
  while (gCotaUartMutex == NULL);
  xSemaphoreGive( gCotaUartMutex );
  
  gCotaUartRxSem = xSemaphoreCreateBinaryStatic(&gCotaUartRxSemBuff);
  while (gCotaUartRxSem == NULL);  
 
  gCotaUartWaitForTx = xSemaphoreCreateBinaryStatic(&gCotaUartWaitForTxBuff);
  while (gCotaUartWaitForTx == NULL);  
}

/**
 * @brief Transmits data over UART in a thread safe way.
 *
 * @note  You cannot post to the error handler here because these functions are used by the error handler
 *
 * @param pData    Data to transmit
 * @param size     size of the transmitted data in bytes
 * @param timeout  Timeout to block on transfer in ticks
 * 
 * @return COTA_ERROR_NONE on success,otherwise a failure has occured.
 */
cotaError_t CotaUartTransmit(uint8_t *pData, uint16_t size, uint32_t timeout)
{
  HAL_StatusTypeDef ret = HAL_ERROR;
  
#ifndef PROXY_TEST
  if ( xSemaphoreTake( gCotaUartMutex, timeout ) == pdPASS)
  { 
    ret = HAL_UART_Transmit_IT(&huart3, pData, size);
    if (ret == HAL_OK)
    {
      xSemaphoreTake(gCotaUartWaitForTx, portMAX_DELAY);
    }
    
    xSemaphoreGive( gCotaUartMutex );
  }

#endif   // #ifndef PROXY_TEST
  return (ret == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_UART_TX_FAILED;
}

/**
 * @brief An interrupt handler that indicates data has been transmitted by the UART
 *
 * @param huart Handle to the UART receiving data
 */ 
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  static BaseType_t xHigherPriorityTaskWoken;
  xSemaphoreGiveFromISR(gCotaUartWaitForTx, &xHigherPriorityTaskWoken);
  
#ifdef PROXY_TEST
  TxCpltCallback();
#endif
}

/**
 * @brief An interrupt handler that indicates data has been received by the UART
 *
 * @param huart Handle to the UART receiving data
 */ 
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  static BaseType_t xHigherPriorityTaskWoken;
  xSemaphoreGiveFromISR(gCotaUartRxSem, &xHigherPriorityTaskWoken);
}

/**
 * @brief Enables the receive of data over UART in a thread safe way.  Then waits for the data to arrive.
 *
 * @note  You cannot post to the error handler here because these functions are used by the error handler
 * @note  Currently, this only being used by CLI.
 *
 * @param pData    Pointer to buffer to receive data.
 * @param size     size of data to receive
 * @param timeout  Timeout to block on transfer in ticks
 * 
 * @return COTA_ERROR_NONE on success,otherwise a failure has occured.
 */
cotaError_t  CotaUartWaitForRx(uint8_t *pData, uint16_t size, uint32_t timeout)
{
  HAL_StatusTypeDef ret = HAL_ERROR;

  if (xSemaphoreTake( gCotaUartMutex, timeout ) == pdPASS)
  {
    ret = HAL_UART_Receive_IT(&huart3, pData, size);
    
    xSemaphoreGive( gCotaUartMutex );
    
    if (xSemaphoreTake(gCotaUartRxSem, portMAX_DELAY) != pdPASS)
    {
      ret = HAL_ERROR;
    }
  }
  return (ret == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_HAL;
  
}

#ifdef UART_DRIVER_TEST 
void uart1Task(void *pvParameters);
void uart2Task(void *pvParameters);

/**
 * @brief A task sending a string to uart, used for testing
 * @param pvParameters not used
 */
void uart1Task(void *pvParameters)
{
  char str[] = "Task 1 to Task 2: No, You're ugly.\n\r";
  while (1)
  {
    CotaUartTransmit((uint8_t*)str, strlen(str), portMAX_DELAY);
    vTaskDelay( pdMS_TO_TICKS(30));
  } 
}

/**
 * @brief A task sending a string to uart, used for testing
 * @param pvParameters not used
 */
void uart2Task(void *pvParameters)
{
  char str[] = "Task 2 to Task 1: No, you're a bonehead.\n\r";
  while (1)
  {
    CotaUartTransmit((uint8_t*)str, strlen(str), portMAX_DELAY);
    vTaskDelay( pdMS_TO_TICKS(20));
  } 
}
              
/**
 * @brief Start two tasks sending UART to see the the uart_driver is thread safe.
 */
void InitUartTest(void)
{

  TaskHandle_t xHandle1;
  TaskHandle_t xHandle2;  

  xTaskCreate(uart1Task, "Uart Test 1", 2048, NULL,  2, &xHandle1 );  
  xTaskCreate(uart2Task, "Uart Test 2", 2048, NULL,  2, &xHandle2 );  
}

#endif