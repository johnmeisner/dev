/****************************************************************************//**
* @file      power_level.c
*
* @brief     Implements functions to control power of the UVP's
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "main.h"
#include "power_level.h"
#include "uvp_driver.h"
#include "amb_control.h"
#include "error.h"

typedef struct _dbmToRegEntry
{
    powerRegister_t reg;        ///< Register value needed to set the power
    float           dBm;        ///< Single antenna power in dBm for this register
} dbmToRegEntry;

/**
 * @brief A map of power level register values to actual power values in dBm
 */
static dbmToRegEntry dBmToRegMap[] = { 
    { AMU_POWER_HIGH,     20.0f }, 
    { AMU_POWER_19_5_DBM, 19.5f }, 
    { AMU_POWER_19_DBM,   19.0f }, 
    { AMU_POWER_16_DBM,   16.0f }, 
    { AMU_POWER_13_DBM,   13.0f } 
};

#define  NUM_POWER_MAP_ENTRIES (sizeof(dBmToRegMap)/sizeof(dBmToRegMap[0]))  ///< The number of power level map entries

/**
 * @brief  Sets the power level of all the UVP's on an AMB.  UVP's can only support
 *         five power levels: 20.0 dBm, 19.5 dBm, 19 dBm, 16 dBm, and 13 dBm.  If one of these
 *         power levels is not requested, the nearest lower value will be chosen.
 * 
 * @param pow                 A value of type #powerRegister_t
 * @param ambMask             A mask indicating which AMB's will have their power level set
 * @param uvpMask             A mask indicating which UVP's will have their power level set.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t SetUvpPowerLevels(powerRegister_t pow, uint32_t ambMask, uint16_t uvpMask)
{
    cotaError_t ret = COTA_ERROR_NONE;
    
    if (pow < AMU_POWER_HIGH)
    {
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU1PAHIGHPOWER, 0, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU2PAHIGHPOWER, 0, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU3PAHIGHPOWER, 0, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PAHIGHPOWER, 0, ambMask, uvpMask) : ret;
      
      
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU1PAPOWERMODE, pow, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU2PAPOWERMODE, pow, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU3PAPOWERMODE, pow, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PAPOWERMODE, pow, ambMask, uvpMask) : ret;
    }
    else
    {
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU1PAHIGHPOWER, 1, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU2PAHIGHPOWER, 1, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU3PAHIGHPOWER, 1, ambMask, uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PAHIGHPOWER, 1, ambMask, uvpMask) : ret;
    }
    return ret;
}

 /**
 * @brief  Gets the power level of a single the UVP on an AMB.  UVP's can only support
 *         five power levels: 20.0 dBm, 19.5 dBm, 19 dBm, 16 dBm, and 13 dBm.  If one of these
 *         power levels is not requested, the nearest lower value will be chosen.
 * 
 * @param power                he value for the power mode register or AMU_POWER_HIGH for 20 dBm
 * @param ambMask             A mask indicating which AMB's will have their power level set
 * @param uvpMask             A mask indicating which UVP's will have their power level set.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t GetUvpPowerLevels(powerRegister_t* pow, ambNum_t ambNum, uvpNum_t uvpNum)
{
    cotaError_t ret = COTA_ERROR_NONE;
    uint32_t data; 

    ret = UvpRegisterRead(AMU1PAHIGHPOWER, &data, ambNum, uvpNum);

    if((ret == COTA_ERROR_NONE) && (data == 1))
    {
        *pow = AMU_POWER_HIGH;
    }
    else
    {
        ret = UvpRegisterRead(AMU1PAPOWERMODE, &data, ambNum, uvpNum);
        if(ret == COTA_ERROR_NONE)
        {
            *pow = (powerRegister_t) data;
        }
    }
  
    return ret;
}

/**
 * @brief  Sets the power level of all the UVP's on an AMB.  UVP's can only support
 *         five power levels: 20.0 dBm, 19.5 dBm, 19 dBm, 16 dBm, and 13 dBm.  If one of these
 *         power levels is not requested, the nearest lower value will be chosen.
 * 
 * @param ambMask             A mask indicating which AMB's will have their UVP's power level set. 
 * @param requestedPowerLevel The requested power level in dBm
 * @param actualPowerLevel    Pointer to a float to receive the power level the UVP will actually be set to.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t SetAmbPowerLevels(uint32_t ambMask,  float requestedPowerLevel, float* actualPowerLevel)
{
    cotaError_t ret = COTA_ERROR_NONE;
    powerRegister_t reg;

    *actualPowerLevel = 0;

    if (requestedPowerLevel > dBmToRegMap[0].dBm)
    {   
      // Power level is greater than the max, so set the power level to the max.
        *actualPowerLevel = dBmToRegMap[0].dBm;
        reg = dBmToRegMap[0].reg;
    }
    else if (requestedPowerLevel < dBmToRegMap[NUM_POWER_MAP_ENTRIES-1].dBm)
    {
      // Power level is lower than the minimum, so set the power level to the minium.
        *actualPowerLevel = dBmToRegMap[NUM_POWER_MAP_ENTRIES-1].dBm;
        reg = dBmToRegMap[NUM_POWER_MAP_ENTRIES-1].reg;
    }
    else
    {
      //Find the closet lower power level
        for (uint16_t ii = 0; ii < NUM_POWER_MAP_ENTRIES; ii++)
        {
            if (requestedPowerLevel >= dBmToRegMap[ii].dBm)
            {
                reg = dBmToRegMap[ii].reg;
                *actualPowerLevel = dBmToRegMap[ii].dBm;
                break;
            }
        }
    }

    ret = SetUvpPowerLevels(reg, ambMask, ALL_UVP_MASK);

    return ret;
}

