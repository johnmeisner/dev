/****************************************************************************//**
* @file      amb_control.c
*
* @brief     Implements functions to control the AMB's
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "main.h"
#include "orion_config.h"
#include "amb_control.h"

/// @todo The following defines are for development on the disco board and should be deleted when we get the new ccb
#define AMB_PD_DONE_TEST_GPIO_Port  AMB_EN_TEST_GPIO_Port
#define AMB_PD_DONE_TEST_Pin        AMB_EN_TEST_Pin
#define AMB0_SPI_DISABLE_GPIO_Port  AMB_SPI_DISABLE_TEST_GPIO_Port
#define AMB1_SPI_DISABLE_GPIO_Port  AMB_SPI_DISABLE_TEST_GPIO_Port
#define AMB2_SPI_DISABLE_GPIO_Port  AMB_SPI_DISABLE_TEST_GPIO_Port
#define AMB3_SPI_DISABLE_GPIO_Port  AMB_SPI_DISABLE_TEST_GPIO_Port
#define AMB0_SPI_DISABLE_Pin        AMB_SPI_DISABLE_TEST_Pin
#define AMB1_SPI_DISABLE_Pin        AMB_SPI_DISABLE_TEST_Pin
#define AMB2_SPI_DISABLE_Pin        AMB_SPI_DISABLE_TEST_Pin
#define AMB3_SPI_DISABLE_Pin        AMB_SPI_DISABLE_TEST_Pin
#define AMB0_PD_DONE_GPIO_Port      AMB_PD_DONE_TEST_GPIO_Port
#define AMB1_PD_DONE_GPIO_Port      AMB_PD_DONE_TEST_GPIO_Port
#define AMB2_PD_DONE_GPIO_Port      AMB_PD_DONE_TEST_GPIO_Port
#define AMB3_PD_DONE_GPIO_Port      AMB_PD_DONE_TEST_GPIO_Port
#define AMB0_PD_DONE_Pin            AMB_PD_DONE_TEST_Pin
#define AMB1_PD_DONE_Pin            AMB_PD_DONE_TEST_Pin
#define AMB2_PD_DONE_Pin            AMB_PD_DONE_TEST_Pin
#define AMB3_PD_DONE_Pin            AMB_PD_DONE_TEST_Pin
#define AMB0_UVP_EN_GPIO_Port       AMB_UVP_EN_TEST_GPIO_Port
#define AMB1_UVP_EN_GPIO_Port       AMB_UVP_EN_TEST_GPIO_Port
#define AMB2_UVP_EN_GPIO_Port       AMB_UVP_EN_TEST_GPIO_Port
#define AMB3_UVP_EN_GPIO_Port       AMB_UVP_EN_TEST_GPIO_Port
#define AMB0_UVP_EN_Pin             AMB_UVP_EN_TEST_Pin
#define AMB1_UVP_EN_Pin             AMB_UVP_EN_TEST_Pin
#define AMB2_UVP_EN_Pin             AMB_UVP_EN_TEST_Pin
#define AMB3_UVP_EN_Pin             AMB_UVP_EN_TEST_Pin
#define AMB0_EN_GPIO_Port           AMB_EN_TEST_GPIO_Port
#define AMB1_EN_GPIO_Port           AMB_EN_TEST_GPIO_Port
#define AMB2_EN_GPIO_Port           AMB_EN_TEST_GPIO_Port
#define AMB3_EN_GPIO_Port           AMB_EN_TEST_GPIO_Port
#define AMB0_EN_Pin                 AMB_EN_TEST_Pin
#define AMB1_EN_Pin                 AMB_EN_TEST_Pin
#define AMB2_EN_Pin                 AMB_EN_TEST_Pin
#define AMB3_EN_Pin                 AMB_EN_TEST_Pin
#define AMB_ADD1_GPIO_Port          AMB_EN_TEST_GPIO_Port
#define AMB_ADD2_GPIO_Port          AMB_EN_TEST_GPIO_Port
#define AMB_ADD3_GPIO_Port          AMB_EN_TEST_GPIO_Port
#define AMB_ADD4_GPIO_Port          AMB_EN_TEST_GPIO_Port
#define AMB_ADD1_Pin                AMB_EN_TEST_Pin
#define AMB_ADD2_Pin                AMB_EN_TEST_Pin
#define AMB_ADD3_Pin                AMB_EN_TEST_Pin
#define AMB_ADD4_Pin                AMB_EN_TEST_Pin
#define hspi1                       hspi2
#define hspi3                       hspi2
#define hspi4                       hspi2
///@todo the above defines need to be deleted when we move to the ccb

/**
 * @brief This structure allows us to map AMB's to their SPI handles and GPIO pins
 */
typedef struct _ambMap
{
    GPIO_TypeDef*         spiDisablePort; //< The port of the AMB's disable GPIO
    uint16_t              spiDisablePin;  //< The pin of the AMB's disable GPIO
    GPIO_TypeDef*         ambEnPort;      //< The port of the AMB's LDO enable GPIO           
    uint16_t              ambEnPin;       //< The pin of the AMB's LDO enable GPIO
    GPIO_TypeDef*         uvpEnPort;      //< The port of the AMB's UVP enable GPIO      
    uint16_t              uvpEnPin;       //< The pin of the AMB's UVP enable GPIO
    GPIO_TypeDef*         pdDonePort;     //< The port of the AMB's phase detect done GPIO       
    uint16_t              pdDonePin;      //< The pin of the AMB's phase detect done GPIO
    SPI_HandleTypeDef*    spi;            //< The handle to the AMB's SPI port
} ambMap_t;

/// @todo The following variable may need to be changed when the new ccb arrives
#if MASTER_AMB_SPI_PORT == 0
static SPI_HandleTypeDef* masterSpi = &hspi1; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 1
static SPI_HandleTypeDef* masterSpi = &hspi2; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 2
static SPI_HandleTypeDef* masterSpi = &hspi3; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 3
static SPI_HandleTypeDef* masterSpi = &hspi4; //< The handle of the master SPI port, which will control the SCLK
#else
#error "MASTER_AMB_SPI_PORT is not valid"
#endif

/**
 * This array maps each AMB to its SPI handle and GPIO pins
 */
static ambMap_t ambInfo[] = 
{
    {AMB0_SPI_DISABLE_GPIO_Port, AMB0_SPI_DISABLE_Pin, AMB0_EN_GPIO_Port, AMB0_EN_Pin, AMB0_UVP_EN_GPIO_Port, AMB0_UVP_EN_Pin, AMB0_PD_DONE_GPIO_Port, AMB0_PD_DONE_Pin, &hspi1},
    {AMB1_SPI_DISABLE_GPIO_Port, AMB1_SPI_DISABLE_Pin, AMB1_EN_GPIO_Port, AMB1_EN_Pin, AMB1_UVP_EN_GPIO_Port, AMB1_UVP_EN_Pin, AMB1_PD_DONE_GPIO_Port, AMB1_PD_DONE_Pin, &hspi2},
    {AMB2_SPI_DISABLE_GPIO_Port, AMB2_SPI_DISABLE_Pin, AMB2_EN_GPIO_Port, AMB2_EN_Pin, AMB2_UVP_EN_GPIO_Port, AMB2_UVP_EN_Pin, AMB2_PD_DONE_GPIO_Port, AMB2_PD_DONE_Pin, &hspi3},
    {AMB3_SPI_DISABLE_GPIO_Port, AMB3_SPI_DISABLE_Pin, AMB3_EN_GPIO_Port, AMB3_EN_Pin, AMB3_UVP_EN_GPIO_Port, AMB3_UVP_EN_Pin, AMB3_PD_DONE_GPIO_Port, AMB3_PD_DONE_Pin, &hspi4},
};

/**
 * @brief Enable the clock for a GPIO port
 * 
 * @note We needed to add this because CubeMx didn't generate the clock enable for GPIO's
 *
 * @param portBase    The port base of the GPIO (see #GPIO_TypeDef)
 */ 
static void enableGPIOClock(GPIO_TypeDef* portBase)
{
    switch((uint32_t)portBase)
    { 
    case (uint32_t)GPIOA:
        __HAL_RCC_GPIOA_CLK_ENABLE();
        break;
    case (uint32_t)GPIOB:
        __HAL_RCC_GPIOB_CLK_ENABLE();
        break;
    case (uint32_t)GPIOC:
        __HAL_RCC_GPIOC_CLK_ENABLE();
        break;
    case (uint32_t)GPIOD:
        __HAL_RCC_GPIOD_CLK_ENABLE();
        break;
    case (uint32_t)GPIOE:
        __HAL_RCC_GPIOE_CLK_ENABLE();
        break;
    case (uint32_t)GPIOF:
        __HAL_RCC_GPIOF_CLK_ENABLE();
        break;
    case (uint32_t)GPIOG:
        __HAL_RCC_GPIOG_CLK_ENABLE();
        break;
    case (uint32_t)GPIOH:
         __HAL_RCC_GPIOH_CLK_ENABLE();
        break;
    case (uint32_t)GPIOI:
        __HAL_RCC_GPIOI_CLK_ENABLE();
        break;
    case (uint32_t)GPIOJ:
        __HAL_RCC_GPIOJ_CLK_ENABLE();
        break;
    case (uint32_t)GPIOK:
        __HAL_RCC_GPIOK_CLK_ENABLE();
        break;
    default:
        break;   
    }
}

/**
 * @brief Configures an single output GPIO
 *        @note  A fully functional CubeMx would have configured GPIO ports for you if it was working correctly. We only need to do this because CubeMx forgets to.
 * @param portBase    The port base of the GPIO (see #GPIO_TypeDef)
 * @param gpioPinMask The pin of the GPIO
 */ 
static void configureOutputGPIO(GPIO_TypeDef* portBase, uint16_t gpioPinMask)
{
    GPIO_InitTypeDef gpioInit = {0};

    enableGPIOClock(portBase);
    gpioInit.Pin = gpioPinMask;
    gpioInit.Mode = GPIO_MODE_OUTPUT_PP;
    gpioInit.Pull = GPIO_NOPULL;
    gpioInit.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

    HAL_GPIO_Init(portBase, &gpioInit);
}  
  
/**
 * @brief Configures an single input GPIO
 *        @note A fully functional CubeMx would have configured GPIO ports for you if it was working correctly. We only need to do this because CubeMx forgets to.
 * @param portBase    The port base of the GPIO (see #GPIO_TypeDef)
 * @param gpioPinMask The pin of the GPIO
 */ 
static void configureInputGPIO(GPIO_TypeDef* portBase, uint16_t gpioPinMask)
{
    GPIO_InitTypeDef gpioInit = {0};

    enableGPIOClock(portBase);
    gpioInit.Pin = gpioPinMask;
    gpioInit.Mode = GPIO_MODE_INPUT;
    gpioInit.Pull = GPIO_NOPULL;

    HAL_GPIO_Init(portBase, &gpioInit);
}  
  
/**
 * @brief Performs necessary intialization of GPIO pins
 */
void AmbControlInit(void)
{
    uint8_t ii;

    ///@todo Add TX_CTRL, RXCTL, and PU control pins
    configureOutputGPIO(AMB_ADD1_GPIO_Port, AMB_ADD1_Pin);
    configureOutputGPIO(AMB_ADD2_GPIO_Port, AMB_ADD2_Pin);
    configureOutputGPIO(AMB_ADD3_GPIO_Port, AMB_ADD3_Pin);
    configureOutputGPIO(AMB_ADD4_GPIO_Port, AMB_ADD4_Pin);
    configureOutputGPIO(AMB_SINGLE_GPIO_Port, AMB_SINGLE_Pin);

    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        configureOutputGPIO(ambInfo[ii].spiDisablePort, ambInfo[ii].spiDisablePin);
        configureOutputGPIO(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin);
        configureOutputGPIO(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin);
        /// @todo This line needs to put uncommented when we move to the CCB
        //configureInputGPIO(ambInfo[ii].pdDonePort, ambInfo[ii].pdDonePin);    
    }

    SetSpiEnabledPins(ALL_AMB_MASK, false);
    SetAmbEnablePins(ALL_AMB_MASK, false);
    SetUvpEnable(ALL_AMB_MASK, false);
}

/**
 * @brief Sets the address selected a certain UVP for read/write operations
 *
 * @param uvpNum The zero based number of the UVP to select
 */
void SetAmbAddrPins(uvpNum_t uvpNum)
{ 
    if (uvpNum < UVPS_PER_AMB)
    {    
        HAL_GPIO_WritePin(AMB_ADD1_GPIO_Port, AMB_ADD1_Pin, (uvpNum & 0x1) ? GPIO_PIN_SET : GPIO_PIN_RESET);
        HAL_GPIO_WritePin(AMB_ADD2_GPIO_Port, AMB_ADD2_Pin, (uvpNum & 0x2) ? GPIO_PIN_SET : GPIO_PIN_RESET);
        HAL_GPIO_WritePin(AMB_ADD3_GPIO_Port, AMB_ADD3_Pin, (uvpNum & 0x4) ? GPIO_PIN_SET : GPIO_PIN_RESET); 
        HAL_GPIO_WritePin(AMB_ADD4_GPIO_Port, AMB_ADD4_Pin, (uvpNum & 0x8) ? GPIO_PIN_SET : GPIO_PIN_RESET);
    }
}

/**
 * @brief Sets the state of the disable SPI for all the AMB's specified by the mask
 *
 * @param ambMask   A mask specifying which AMB's to set the AMB disable gpio for
 * @param pinState  The state to set the pin to.
 */

void SetSpiEnabledPins(uint32_t ambMask, bool on)
{
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
            HAL_GPIO_WritePin(ambInfo[ii].spiDisablePort, ambInfo[ii].spiDisablePin, on ? GPIO_PIN_RESET : GPIO_PIN_SET);  
        }
    }
}

/**
 * @brief Sets the state of the UVP enable gpio for all the AMB's specified by the mask.  
 *        The UVP enable pin will not be set for an AMB unless the AMB enable
 *        pin for that uvp also set.
 *
 * @param ambMask   A mask specifying which AMB's to set the UVP enable gpio for
 * @param on        true to enable the UVP's, false to disable the UVP's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */ 
cotaError_t SetUvpEnable(uint32_t ambMask, bool on)
{
    HAL_StatusTypeDef ret = HAL_OK;
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
            if ((on == false) || (HAL_GPIO_ReadPin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin) == GPIO_PIN_SET))
            {
                HAL_GPIO_WritePin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin, on ? GPIO_PIN_SET: GPIO_PIN_RESET);
            }
            else
            {
                //We cannot enable a UVP unless the AMB is also enabled.
                ret = HAL_ERROR;
            }
        }
    }
  
    return (ret == HAL_OK) ? COTA_ERROR_NONE: COTA_ERROR_UVP_ENABLE;
}

/**
 * @brief Sets the state of the amb enable GPIO for all the AMB's specified by the mask.  
 *        If an AMB is disabled, the UVP enable pin will also be disabled.
 *
 * @param ambMask   A mask specifying which AMB's to set the LDO enable gpio for
 * @param on        true enables the AMB's, false disables the AMB's
 */ 
void SetAmbEnablePins(uint32_t ambMask, bool on)
{
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
            if (on == false)
            {
                //Make sure UVP's are also off if we disable an AMB
                HAL_GPIO_WritePin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin, GPIO_PIN_RESET);
            }
            HAL_GPIO_WritePin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin, on ? GPIO_PIN_SET: GPIO_PIN_RESET);       
        }
    }
}

/**
 * @brief  Retrieves the SPI handle for a single AMB
 * 
 * @param  The AMB number of the AMB
 *
 * @return A pointer to the SPI handle for the AMB
 */
SPI_HandleTypeDef* GetSpiHandle(ambNum_t ambNum)
{
    SPI_HandleTypeDef* spiHandle = NULL;

    if ( ambNum < MAX_NUM_AMB)
    {
        return ambInfo[ambNum].spi;
    }

    return spiHandle;
}

/**
 * @brief  Retrieves the SPI handle for the master SPI
 *
 * @return A pointer to the SPI handle for master SPI
 */
SPI_HandleTypeDef* GetMasterSpiHandle(void)
{
    return masterSpi;
}


/**
 * @brief Sets the state of the RX enable GPIO for all the AMB's specified by the mask.  
 *        If an AMB is disabled, the RX enable pin will also be disabled.
 *
 * @param ambMask   A mask specifying which AMB's to set RX pin for
 * @param on        true enables the AMB's, false disables the AMB's
 */ 
void SetAmbRxEnable(uint32_t ambMask, bool on)
{
  ///@todo implement this function
}

/**
 * @brief Sets the state of the TX enable GPIO for all the AMB's specified by the mask.  
 *        If an AMB is disabled, the RX enable pin will also be disabled.
 *
 * @param ambMask   A mask specifying which AMB's to set TX pin for
 * @param on        true enables the AMB's, false disables the AMB's
 */ 
void SetAmbTxEnable(uint32_t ambMask, bool on)
{
  ///@todo implement this function
}


/**
 * @brief Sets the state of the PU enable GPIO for all the AMB's specified by the mask.  
 *        If an AMB is disabled, the RX enable pin will also be disabled.
 *
 * @param ambMask   A mask specifying which AMB's to set PU pin for
 * @param on        true enables the AMB's, false disables the AMB's
 */ 
void SetAmbPuEnable(uint32_t ambMask, bool on)
{
  ///@todo implement this function
}