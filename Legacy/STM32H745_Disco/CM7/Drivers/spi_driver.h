/****************************************************************************//**
* @file      spi_driver.h
*
* @brief     Header file containing prototypes to send data over SPI
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _SPI_DRIVER_H_
#define _SPI_DRIVER_H_
#include "error.h"
#define SPI_MAX_NUM_BYTES 64  ///< The maximum number of bytes that can be sent over SPI.  Recommend 9 bytes x 4 for AMB spi transters.

void InitSpiDriver(void);
cotaError_t SpiTransfer(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size);
cotaError_t SpiSimulTransfer(uint8_t* pTxData, uint8_t *pRxData, uint16_t size, uint32_t ambMask);

#endif //#ifndef _SPI_DRIVER_H_