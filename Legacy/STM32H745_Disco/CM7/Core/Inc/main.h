/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PROXY_GO_IN_Pin GPIO_PIN_15
#define PROXY_GO_IN_GPIO_Port GPIOA
#define AMB_SPI_DISABLE_TEST_Pin GPIO_PIN_4
#define AMB_SPI_DISABLE_TEST_GPIO_Port GPIOB
#define PROXY_GO_TEST_Pin GPIO_PIN_15
#define PROXY_GO_TEST_GPIO_Port GPIOH
#define OSC32_OUT_Pin GPIO_PIN_15
#define OSC32_OUT_GPIO_Port GPIOC
#define OSC32_IN_Pin GPIO_PIN_14
#define OSC32_IN_GPIO_Port GPIOC
#define LCD_DISP_Pin GPIO_PIN_7
#define LCD_DISP_GPIO_Port GPIOD
#define AMB_UVP_EN_TEST_Pin GPIO_PIN_3
#define AMB_UVP_EN_TEST_GPIO_Port GPIOE
#define USB_OTG_FS2_ID_Pin GPIO_PIN_10
#define USB_OTG_FS2_ID_GPIO_Port GPIOA
#define USER_BUTTON_Pin GPIO_PIN_13
#define USER_BUTTON_GPIO_Port GPIOC
#define USER_BUTTON_EXTI_IRQn EXTI15_10_IRQn
#define TIM15_PWM_OUTPUT_Pin GPIO_PIN_6
#define TIM15_PWM_OUTPUT_GPIO_Port GPIOE
#define TIM1_PWM_OUTPUT_Pin GPIO_PIN_8
#define TIM1_PWM_OUTPUT_GPIO_Port GPIOA
#define DBG5_Pin GPIO_PIN_11
#define DBG5_GPIO_Port GPIOI
#define LD2_Pin GPIO_PIN_13
#define LD2_GPIO_Port GPIOI
#define LCD_INT_Pin GPIO_PIN_2
#define LCD_INT_GPIO_Port GPIOG
#define OSC_OUT_Pin GPIO_PIN_1
#define OSC_OUT_GPIO_Port GPIOH
#define OSC_IN_Pin GPIO_PIN_0
#define OSC_IN_GPIO_Port GPIOH
#define LCD_BL_Pin GPIO_PIN_0
#define LCD_BL_GPIO_Port GPIOK
#define RSSI_VOTING_DONE_Pin GPIO_PIN_1
#define RSSI_VOTING_DONE_GPIO_Port GPIOK
#define AMB_EN_TEST_Pin GPIO_PIN_8
#define AMB_EN_TEST_GPIO_Port GPIOF
#define AMB_SINGLE_Pin GPIO_PIN_0
#define AMB_SINGLE_GPIO_Port GPIOC
#define DBG4_Pin GPIO_PIN_4
#define DBG4_GPIO_Port GPIOH
#define VCP_TX_Pin GPIO_PIN_10
#define VCP_TX_GPIO_Port GPIOB
#define VCP_RX_Pin GPIO_PIN_11
#define VCP_RX_GPIO_Port GPIOB
#define PROXY_SR_Pin GPIO_PIN_10
#define PROXY_SR_GPIO_Port GPIOH
#define PROXY_SR_EXTI_IRQn EXTI15_10_IRQn
#define USB_OTG_FS2_OverCurrent_Pin GPIO_PIN_11
#define USB_OTG_FS2_OverCurrent_GPIO_Port GPIOH
#define USB_OTG_FS2_OverCurrent_EXTI_IRQn EXTI15_10_IRQn
#define PRX_SR_TEST_Pin GPIO_PIN_12
#define PRX_SR_TEST_GPIO_Port GPIOH
#define DBG1_Pin GPIO_PIN_12
#define DBG1_GPIO_Port GPIOD
#define DBG2_Pin GPIO_PIN_13
#define DBG2_GPIO_Port GPIOD
#define LD1_Pin GPIO_PIN_2
#define LD1_GPIO_Port GPIOJ
#define DBG3_Pin GPIO_PIN_8
#define DBG3_GPIO_Port GPIOH
#define LCD_RST_Pin GPIO_PIN_12
#define LCD_RST_GPIO_Port GPIOB
#define PRX_MR_TEST_Pin GPIO_PIN_3
#define PRX_MR_TEST_GPIO_Port GPIOA
#define PRX_MR_TEST_EXTI_IRQn EXTI3_IRQn
#define PROXY_MR_Pin GPIO_PIN_4
#define PROXY_MR_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
