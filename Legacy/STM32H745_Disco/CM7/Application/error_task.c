/**
* @file       error_task.c
*
* @brief      The purpose of this header is to supply an interface to communicate errors
*             to external entities such as a UART or a Raspberry PI.
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*/


#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "error.h"
#include "string.h"
#include "stdio.h"
#include "uart_driver.h"
#include "orion_config.h"

#ifdef COTA_ERROR_ENABLED                     ///< must be defined in the compile line to enable error handling.


#define ERROR_HANDLER_QUEUE_DEPTH   5         ///< The maximum number of error codes the error queue can hold.
#define ERROR_HANDLER_BUFFER_SIZE   64        ///< The maximum size of an output message for errors
#define DONT_WAIT_FOR_QUEUE         0         ///< When posting to the queue we don't wait for availble spots.

#ifdef USE_COTA_ERROR_STRINGS                 ///< This define is used to control whether strings will be availble to describe the errors.

    #define COTA_ERROR_ENTRY(num, string) {num, string}
    typedef struct _cotaErrorInfo
    {
      cotaError_t errNum;   ///< The error code of type #cotaError_t
      char*       errMsg;   ///< A string describing the error
    } cotaErrorInfo;

#else

    #define COTA_ERROR_ENTRY(num, string) {num}
    typedef struct _cotaErrorInfo
    {
      cotaError_t errNum;   ///< The error code of type #cotaError_t
    } cotaErrorInfo;

#endif

/**
 * This array maps an error code to its mask and potentially a descriptive string.
 */
static cotaErrorInfo cotaErrorMap[] = {
  COTA_ERROR_ENTRY(COTA_ERROR_NONE,                           "SUCCESS"),
  COTA_ERROR_ENTRY(COTA_ERROR_HAL,                            "Driver Error"),
  COTA_ERROR_ENTRY(COTA_ERROR_QUEUE_FAILED,                   "Error handler queue not created."),
  COTA_ERROR_ENTRY(COTA_ERROR_TASK_FAILED,                    "Error handler task failed to create."),  
  COTA_ERROR_ENTRY(COTA_ERROR_SPI_TRANSFER_FAILED,            "A SPI transfer has failed."),
  COTA_ERROR_ENTRY(COTA_ERROR_SPI_SIMUL_TRANS_FAILED,         "A simlultaneous SPI transfer has failed."),
  COTA_ERROR_ENTRY(COTA_ERROR_UVP_SIMUL_READ_TO_LARGE,        "A simulataneous read of simulataneous UVP has a buffer that is too large."),
  COTA_ERROR_ENTRY(COTA_ERROR_UART_TX_FAILED,                 "A UART transmit failed"),
  COTA_ERROR_ENTRY(COTA_ERROR_UVP_LOCK_FAILED,                "A UVP lock could not be confirmed."),
  COTA_ERROR_ENTRY(COTA_ERROR_UVP_WAIT_FOR_TRANSMIT_TIMEOUT,  "A UVP could not confirm a transmit occurred."), 
  COTA_ERROR_ENTRY(COTA_ERROR_UVP_REG_VALUE_NOT_IN_MASK,      "An attempt to set a UVP value failed because the value couldn't fit in the mask."),
  COTA_ERROR_ENTRY(COTA_ERROR_UVP_WAIT_FOR_PHASE_DET_TIMEOUT, "An error occurred while waiting for a phase detect from a UVP."),
  COTA_ERROR_ENTRY(COTA_ERROR_UVP_ENABLE,                     "An error occurred while enabling UVP's.")
};

#define COTA_ERROR_MAP_SIZE (sizeof(cotaErrorMap)/sizeof(cotaErrorMap[0]))

/**
 * @brief Outputs error code information to the UART
 * 
 * @param  cotaError An error code of type #cotaError_t
 */ 
static void OutputError(cotaError_t errorCode)
{
  uint16_t ii;
  char uartBuff[ERROR_HANDLER_BUFFER_SIZE];
  
  for (ii = 0; ii < COTA_ERROR_MAP_SIZE; ii++)
  {
    if (cotaErrorMap[ii].errNum == errorCode)
    {
#ifdef USE_COTA_ERROR_STRINGS
      snprintf(uartBuff, ERROR_HANDLER_BUFFER_SIZE, "ERROR %d : %s\n\r", errorCode, cotaErrorMap[ii].errMsg); 
#else
      snprintf(uartBuff, ERROR_HANDLER_BUFFER_SIZE, "ERROR %d\n\r", errorCode); 
#endif    
      CotaUartTransmit((uint8_t*)uartBuff, strlen(uartBuff), portMAX_DELAY);
      
      break;
    }
  }
}

/**
 * @brief  Outputs an error to UART. 
 *
 * @param  cotaError An error code of type #cotaError_t,  If it's COTA_ERROR_NONE, the error is not posted
 * 
 * @return returns the cotaError passed so you can place this in the final return call of the function.
 */
cotaError_t PostCotaError(cotaError_t cotaError)
{
  if (cotaError != COTA_ERROR_NONE)
  {
    OutputError(cotaError);
  }
  return cotaError;
}

#endif //#ifdef COTA_ERROR_ENABLED
