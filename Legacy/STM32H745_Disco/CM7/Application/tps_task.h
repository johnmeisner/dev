/**
 * @file       tps_task.h
 *
 * @brief      TPS task header file
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _TPS_TASK_H_
#define _TPS_TASK_H_

#include "FreeRTOS.h"
#include "semphr.h"

void CreateTpsStateMachineTask(void);
void TpsRunStateMachineFromIsr(void);
void TpsTimerCallback(void);

#endif /*#ifndef _TPS_TASK_H_*/
