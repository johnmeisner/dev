/** 
 *
 * @file       cli_task.c
 *
 * @brief      Cota CLI task implementation file
 *

 * @note that the snprintf function requires more RAM than the defaul CubeMx configuration allows
 *
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "cli_task.h"
#include "main.h"
#include "string.h"
#include "cmsis_os.h"
#include "FreeRTOS_CLI.h"
#include <stdio.h>
#include "stm32h7xx_hal.h"
#include "uart_driver.h"

#define MAX_OUTPUT_LENGTH   130         ///< snprintf function requires more RAM than the defaul CubeMx configuration allows
#define MAX_INPUT_LENGTH    20          ///< Limited input to save memory
#define STRING_TERM         "\r\n"      ///< The character sequence that terminates a string and starts a new one
#define CLIENT_LIST_SIZE    (sizeof(CLIENT_LIST) / sizeof(CLIENT_LIST[0]))  ///<element count  of initalizing data
#define CLI_CMD_FIRST_CALL  0           ///< Initial step of commend execution will start next
#define CLI_CMD_PROC_CALL   1           ///< Iterative step of commend execution will start next (this is when command processing needs to happen in the same way multiple times) 
#define CLI_CMD_LAST_CALL   -1          ///< Final step of commend execution is expected


static osThreadId_t gCliTaskHandle;      ///< CLI task handle
static const char * WELCOME_MSG = "\n\r\033[2JWelcome to Cota CLI!" "\r\n" "Type Help to view a list of registered commands."  "\r\n" ; // Note the weird escape value in string is the ANSI temrinal code to clear screen

static BaseType_t ChannelOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ChannelOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t CalibrateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AddClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RegisterClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RemoveClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t IdentifyClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientFwUpdateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientFwUpdateRespCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientSleepCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientLeaveCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetGoodChannelsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetComChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RunCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t PauseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RebootCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ResetCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ResetFpgaCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ResetProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t CheckProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ProxyCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ProxyCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RestartCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ShutdownCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SendCqtCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SendDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SendTpcCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SendTpsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StopDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t InfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetGoodChannelsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetComChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetTxFreqCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ChannelInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ProxyInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t VersionsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientListCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientDetailCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientConfigCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t DevicesInRangeCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t DevicesStatusCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StartChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StopChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetChargingInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetChargerIdCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetMotionEngineCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetMotionEngineCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t MotionDevicesCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSystemTempCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSystemStateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StatusCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t IdentifyChargerCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StartSendingDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StopSendingDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t NormalizePhaseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t DebugLogCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);



static void StartCliTask(void *argument);

/** This is an array of CLI_Command_Definition_t used to initialize the command CLI list
 *
 *  This is used in StartCliTask()
 * 
 * @todo optimize to move this to flash, and keep it there.  
 * @todo if memory optimizatoin is need, consider reducin/eliminating the about command verbage or generate the leading cmd name & arg names on the fly???  
 *
 */
static const  CLI_Command_Definition_t  CliCmds[] = 
{
{"channel_on",  "\r\nchannel_on   [Channel Number]  Turn ON specified channel or all",  *(&(ChannelOnCmdCallback)), 0},
{"channel_off",  "\r\nchannel_off   [Channel Number]  Turn OFF specified channel or all",  *(&(ChannelOffCmdCallback)), 1},
{"calibrate",  "\r\ncalibrate   []  Calibrate the array only if the array is already on",  *(&(CalibrateCmdCallback)), 1},
{"add_client",  "\r\nadd_client   [Client ID]  Add a new client to the client list",  *(&(AddClientCmdCallback)), 0},
{"register_client",  "\r\nregister_client   [Client ID][QueryType][Priority]  Register a new client with the system. QueryType and Priority are optional.",  *(&(RegisterClientCmdCallback)), 1},
{"remove_rx",  "\r\nremove_rx   [Client ID]  Removes a receiver from the list of registered receivers",  *(&(RemoveClientCmdCallback)), 3},
{"identify_client",  "\r\nidentify_client   [Client ID][Duration]  Identify the client (blink client's LED)",  *(&(IdentifyClientCmdCallback)), 1},
{"client_command",  "\r\nclient_command   [Client ID][Data]  Send a generic command to a client (or all clients)",  *(&(ClientCommandCmdCallback)), 2},
{"client_command_data",  "\r\nclient_command_data   [Client ID]  Get client command data",  *(&(ClientCommandDataCmdCallback)), 2},
{"client_fw_update",  "\r\nclient_fw_update   [Client ID][Data]  Send a fw update packet to a client (or all clients)",  *(&(ClientFwUpdateCmdCallback)), 1},
{"client_fw_update_resp",  "\r\nclient_fw_update_resp   [Client ID]  Get fw update response from a specific client",  *(&(ClientFwUpdateRespCmdCallback)), 2},
{"client_sleep",  "\r\nclient_sleep   [Client ID | all]  Command a client (or all) to enter deep sleep mode",  *(&(ClientSleepCmdCallback)), 1},
{"client_leave",  "\r\nclient_leave   [Client ID | all]  Command a client (or all) to leave the network",  *(&(ClientLeaveCmdCallback)), 1},
{"set_good_channels",  "\r\nset_good_channels   [0x0-0xFFFFFFFF]  Set good channels to use as default settings",  *(&(SetGoodChannelsCmdCallback)), 1},
{"set_com_channel",  "\r\nset_com_channel   [24-26]  Set COM channel for the system",  *(&(SetComChannelCmdCallback)), 1},
{"run",  "\r\nrun   []  Start running the Power Cycle automatically",  *(&(RunCmdCallback)), 1},
{"pause",  "\r\npause   []  Pause the Power Cycle and enter debug mode",  *(&(PauseCmdCallback)), 0},
{"reboot",  "\r\nreboot   []  Reboot the system (graceful channels shutdown and reboot)",  *(&(RebootCmdCallback)), 0},
{"reset",  "\r\nreset   []  Reset the Array and reload the configuration",  *(&(ResetCmdCallback)), 0},
{"reset_FPGA",  "\r\nreset_FPGA   []  Reset the FPGA (graceful channels shutdown then FPGA reset)",  *(&(ResetFpgaCmdCallback)), 0},
{"reset_proxy",  "\r\nreset_proxy   []  Reset the Proxy and resend a discovery message",  *(&(ResetProxyCmdCallback)), 0},
{"check_proxy",  "\r\ncheck_proxy   []  Check if the proxy is responding",  *(&(CheckProxyCmdCallback)), 0},
{"proxy_command",  "\r\nproxy_command   [Data]  Send a generic command to the proxy",  *(&(ProxyCommandCmdCallback)), 0},
{"proxy_command_data",  "\r\nproxy_command_data   []  Get proxy command data",  *(&(ProxyCommandDataCmdCallback)), 1},
{"restart",  "\r\nrestart   [Client ID | all]  Restart the Cota Daemon (optionally tell clients to leave and rejoin)",  *(&(RestartCmdCallback)), 0},
{"shutdown",  "\r\nshutdown   [Client ID | all]  Shutdown the system (optionally put clients to sleep as well)",  *(&(ShutdownCmdCallback)), 1},
{"send_cqt",  "\r\nsend_cqt   []  Send a one shot CQT.",  *(&(SendCqtCmdCallback)), 1},
{"send_disc",  "\r\nsend_disc   [Channel]  Send a client discovery message. Channel is optional",  *(&(SendDiscCmdCallback)), 0},
{"send_tpc",  "\r\nsend_tpc   [Client ID]  Send a one shot TPC(Tone Power Config)",  *(&(SendTpcCmdCallback)), 1},
{"send_tps",  "\r\nsend_tps   []  Send a one shot TPS",  *(&(SendTpsCmdCallback)), 1},
{"stop_disc",  "\r\nstop_disc   []  Command the Proxy to stop discovering clients",  *(&(StopDiscCmdCallback)), 0},
{"info",  "\r\ninfo   []  Get info of all channels in the system",  *(&(InfoCmdCallback)), 0},
{"get_good_channels",  "\r\nget_good_channels   []  Get good channel settings",  *(&(GetGoodChannelsCmdCallback)), 0},
{"get_com_channel",  "\r\nget_com_channel   []  Get current COM channel",  *(&(GetComChannelCmdCallback)), 0},
{"get_tx_freq",  "\r\nget_tx_freq   []  Get current Tx frequency setting",  *(&(GetTxFreqCmdCallback)), 0},
{"channel_info",  "\r\nchannel_info   [Channel (0-31)]  Get specified channel information",  *(&(ChannelInfoCmdCallback)), 0},
{"proxy_info",  "\r\nproxy_info   []  Get Proxy information",  *(&(ProxyInfoCmdCallback)), 1},
{"versions",  "\r\nversions   []  Get FPGA, Driver, Daemon, OS, and build versions",  *(&(VersionsCmdCallback)), 0},
{"client_list",  "\r\nclient_list   [Client Status]  Get a list of all clients. Client status is optional",  *(&(ClientListCmdCallback)), 0},
{"client_detail",  "\r\nclient_detail   [Client ID]  Get detailed information for a specific client",  *(&(ClientDetailCmdCallback)), 1},
{"client_data",  "\r\nclient_data   [Client ID]  Get custom data for a specific client",  *(&(ClientDataCmdCallback)), 1},
{"client_config",  "\r\nclient_config   [Client ID][QueryType(5=STD, 6=EXT)]  Set configuration for a specific client",  *(&(ClientConfigCmdCallback)), 1},
{"devices_in_range",  "\r\ndevices_in_range   []  Get a list of all devices (clients)",  *(&(DevicesInRangeCmdCallback)), 2},
{"devices_status",  "\r\ndevices_status   [Client ID]  Get devices (clients) status",  *(&(DevicesStatusCmdCallback)), 0},
{"start_charging",  "\r\nstart_charging   [Client ID | all]  Start charging a specific client or 'all' clients",  *(&(StartChargingCmdCallback)), 1},
{"stop_charging",  "\r\nstop_charging   [Client ID | all]  Stop charging a specific client or 'all' clients",  *(&(StopChargingCmdCallback)), 1},
{"get_charging_info",  "\r\nget_charging_info   [Client ID | all]  Get client charging info(RF power, battery level..etc)",  *(&(GetChargingInfoCmdCallback)), 1},
{"get_charger_id",  "\r\nget_charger_id   []  Get the charger unique ID",  *(&(GetChargerIdCmdCallback)), 1},
{"set_motion_engine",  "\r\nset_motion_engine   [sensitivity(1-5)]  Set motion detection sensitivity",  *(&(SetMotionEngineCmdCallback)), 0},
{"get_motion_engine",  "\r\nget_motion_engine   []  Get motion detection sensitivity",  *(&(GetMotionEngineCmdCallback)), 1},
{"motion_devices",  "\r\nmotion_devices   [Client ID]  Configure devices participating in motion detection",  *(&(MotionDevicesCmdCallback)), 0},
{"set_power_level",  "\r\nset_power_level   [PowerLevel(12-21)]  Configure system power level. Default = 20",  *(&(SetPowerLevelCmdCallback)), 1},
{"get_power_level",  "\r\nget_power_level   []  Get current system power level",  *(&(GetPowerLevelCmdCallback)), 1},
{"get_system_temp",  "\r\nget_system_temp   []  Get the current system temperature",  *(&(GetSystemTempCmdCallback)), 0},
{"get_system_state",  "\r\nget_system_state   []  Get the current system state",  *(&(GetSystemStateCmdCallback)), 0},
{"status",  "\r\nstatus   []  Get the current system status",  *(&(StatusCmdCallback)), 0},
{"identify_charger",  "\r\nidentify_charger   []  Set the light ring to signal charger presence.",  *(&(IdentifyChargerCmdCallback)), 0},
{"start_sending_data",  "\r\nstart_sending_data   [Interval][Client ID]  Start sending data to the cloud for these clients",  *(&(StartSendingDataCmdCallback)), 0},
{"stop_sending_data",  "\r\nstop_sending_data   [Client ID | all]  Stop sending data to the cloud for these clients",  *(&(StopSendingDataCmdCallback)), 2},
{"normalize_phase",  "\r\nnormalize_phase   [Client ID]  Start establishing phase references. Queue Size(1-10) parameter is optional",  *(&(NormalizePhaseCmdCallback)), 1},
{"debug_log",  "\r\ndebug_log   [1=MM, 2=DAE, 3=both][0-4][0x0-0xFF]  Turn on/off MessageManager/Daemon logs",  *(&(DebugLogCmdCallback)), 1},


 
};

/*********** Call Back Functions ******************/


/** This function is registered with the CLI to be called back when the "ChannelOnCmdCallback" command needs to be processed
 *
 */
static BaseType_t ChannelOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ChannelOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t CalibrateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t AddClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RegisterClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RemoveClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t IdentifyClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientFwUpdateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientFwUpdateRespCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientSleepCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientLeaveCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SetGoodChannelsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SetComChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RunCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t PauseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RebootCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ResetCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ResetFpgaCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ResetProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t CheckProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ProxyCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ProxyCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RestartCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ShutdownCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SendCqtCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SendDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SendTpcCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SendTpsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StopDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t InfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetGoodChannelsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetComChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetTxFreqCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ChannelInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ProxyInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t VersionsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientListCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientDetailCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientConfigCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t DevicesInRangeCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t DevicesStatusCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StartChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StopChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetChargingInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetChargerIdCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SetMotionEngineCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetMotionEngineCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t MotionDevicesCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetSystemTempCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetSystemStateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StatusCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t IdentifyChargerCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StartSendingDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StopSendingDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t NormalizePhaseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t DebugLogCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
	BaseType_t retVal = pdFALSE;
	( void ) xWriteBufferLen;	//fixes compiler warnings
	snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
	return retVal; 			// Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}






/** This creates the CLI processing task/thread
 *
 */
void CreateCliTask(void)
{
    // Define the CLI processing task
    const osThreadAttr_t defaultTask_attributes = {
      .name = "CLI Task",
      .priority = (osPriority_t) osPriorityNormal,
      .stack_size = 1024
    };

    // Create the CLI task
    gCliTaskHandle = osThreadNew(StartCliTask, NULL, &defaultTask_attributes);
    (void)gCliTaskHandle; // Avoid unused variable compiler warning
}


/** The CLI processing task that's registered to be executed by the OS
 *
 * @todo move the storage of the command list from ram to flash 
 */
static void StartCliTask(void *argument)
{
    //Peripheral_Descriptor_t xConsole;
    uint8_t cRxedChar;
    uint8_t cInputIndex = 0;
    BaseType_t xMoreDataToFollow;
    /* The input and output buffers are declared static to keep them off the stack. */
    static char pcOutputString[ MAX_OUTPUT_LENGTH ];
    static char pcInputString[ MAX_INPUT_LENGTH ];

     // Register the CLI commands from the table
    for (uint16_t i = 0; i <  sizeof(CliCmds)/sizeof(CLI_Command_Definition_t); i++)
    {
      FreeRTOS_CLIRegisterCommand(&CliCmds[i]);
    }
    
    /* This code assumes the peripheral being used as the console has already
    been opened and configured, and is passed into the task as the task
    parameter.  Cast the task parameter to the correct type. */
    //xConsole = ( Peripheral_Descriptor_t ) pvParameters;

    /* Send a welcome message to the user knows they are connected. */
    //FreeRTOS_write( xConsole, pcWelcomeMessage, strlen( pcWelcomeMessage ) );
    CotaUartTransmit((uint8_t*)WELCOME_MSG, strlen(WELCOME_MSG), HAL_MAX_DELAY);

    for ( ;; )
    {
        /* This implementation reads a single character at a time.  Wait in the
        Blocked state until a character is received. */
        //FreeRTOS_read( xConsole, &cRxedChar, sizeof( cRxedChar ) );
        CotaUartWaitForRx(&cRxedChar, sizeof(cRxedChar), portMAX_DELAY);

        if ( cRxedChar == '\r' )
        {
            /* A newline character was received, so the input command string is
            complete and can be processed.  Transmit a line separator, just to
            make the output easier to read. */
            //FreeRTOS_write( xConsole, "\r\n", strlen( \r\n );
            CotaUartTransmit((uint8_t*)STRING_TERM, strlen(STRING_TERM), HAL_MAX_DELAY);

            /* The command interpreter is called repeatedly until it returns
            pdFALSE.  See the Implementing a command documentation for an
            exaplanation of why this is. */
            do
            {
                /* Send the command string to the command interpreter.  Any
                output generated by the command interpreter will be placed in the
                pcOutputString buffer. */
                xMoreDataToFollow = FreeRTOS_CLIProcessCommand
                                    (
                                        pcInputString,   /* The command string.*/
                                        pcOutputString,  /* The output buffer. */
                                        MAX_OUTPUT_LENGTH/* The size of the output buffer. */
                                    );

                /* Write the output generated by the command interpreter to the
                console. */
                //FreeRTOS_write( xConsole, pcOutputString, strlen( pcOutputString ) );
                CotaUartTransmit((uint8_t*)pcOutputString, strlen(pcOutputString), HAL_MAX_DELAY);

            } while ( xMoreDataToFollow != pdFALSE );

            /* All the strings generated by the input command have been sent.
            Processing of the command is complete.  Clear the input string ready
            to receive the next command. */
            cInputIndex = 0;
            memset( pcInputString, 0x00, MAX_INPUT_LENGTH );
        }
        else
        {
            /* The if() clause performs the processing after a newline character
            is received.  This else clause performs the processing if any other
            character is received. */

            if ( cRxedChar == '\n' )
            {
                // Ignore carriage returns
                // Note that depending on how your terminal program is set up, you may need to swap \n and \r processing
            }
            else if ( cRxedChar == '\b' )
            {
                /* Backspace was pressed.  Erase the last character in the input
                buffer - if there are any. */
                if ( cInputIndex > 0 )
                {
                    cInputIndex--;
                    pcInputString[ cInputIndex ] = '\0';
                }
            }
            else
            {
                /* A character was entered.  It was not a new line, backspace
                or carriage return, so it is accepted as part of the input and
                placed into the input buffer.  When a n is entered the complete
                string will be passed to the command interpreter. */
                if ( cInputIndex < MAX_INPUT_LENGTH )
                {
                    pcInputString[ cInputIndex ] = cRxedChar;
                    cInputIndex++;
                }
            }
        }
    }
}




