/**
 * @file       state_machine.h
 *
 * @brief      State machine header file
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_

#include "state_machine.h"
#include <stdbool.h>

typedef int State_Id_t;
typedef struct _StateMachineState_t StateMachineState_t;
typedef struct _StateMachineTransition_t StateMachineTransition_t;  // TODO:  Try using incomplete struct declarations (e.g. struct _StateMachineTransition_t)
typedef void (*EntryFcn_t)(void);
typedef bool (*CheckFcn_t)(void);
typedef void (*ExitFcn_t) (void);


struct _StateMachineTransition_t
{
  CheckFcn_t           checkCondition;  ///< Condition check function for transition
  ExitFcn_t            exitFcn;         ///< Exit function
  StateMachineState_t* nextState;       ///< next state machine state after transition
};

struct _StateMachineState_t
{
  State_Id_t                state;              ///< State ID
  EntryFcn_t                entryFcn;           ///< State entry function
  char*                     stateName;          ///< State name string
  StateMachineTransition_t* transitionOptions;  ///< State transition options
};

StateMachineState_t* RunStateMachine(StateMachineState_t* currState);

#endif   // #ifndef _STATE_MACHINE_H_
