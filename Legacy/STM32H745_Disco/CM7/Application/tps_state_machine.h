/**
 * @file       tps_state_machine.h
 *
 * @brief      TPS state machine header file
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef STATE_MACHINE_H_
#define STATE_MACHINE_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "state_machine.h"


typedef enum _TPS_STATES
{
  TPS_STATE_ERROR = 0,
  TPS_STATE_WAIT_FOR_START,
  TPS_STATE_INTER_TPS,
  TPS_STATE_IDLE,
  TPS_STATE_RX_READY,
  TPS_STATE_PU_PULSE,
  TPS_STATE_TX_READY,
  TPS_STATE_SEND_POWER
} TPS_STATES;

typedef enum _tpsEvent_t
{
  EVENT_DEASSERT = 0,
  EVENT_ASSERT
} tpsEvent_t;



StateMachineState_t* StateMachineInit(void);

#endif /* STATE_MACHINE_H_ */
