/****************************************************************************//**
* @file      calibrator.h
*
* @brief     Header file containing prototypes to calibrate the system
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _CALIBRATOR_H_
#define _CALIBRATOR_H_

#include "error.h"

cotaError_t Calibrate(uint32_t ambMask, uint16_t aclMask);

#endif //#ifndef _CALIBRATOR_H_