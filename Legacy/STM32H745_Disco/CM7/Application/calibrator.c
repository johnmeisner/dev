/****************************************************************************//**
* @file      calibrator.c
*
* @brief     Implements functions the calibrate function to calibrate the system
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "main.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "power_level.h"
#include "calibrator.h"
#include <stdio.h>

#define MAX_TX_WAIT_COUNT   20      ///< The maximum times to poll the SEND_POWER_RDY register before throwing an error.
#define MAX_PU_WAIT_COUNT   20      ///< The maximum times to poll the PHASE_DETECT_DONE register before throwing an error.
#define MAX_PHASE_OFFSET    512     ///< The range of bits used to represent the phase

#define UVP_CLIENT_1_MASK   0x1     ///< Mask of UVP client 1
#define UVP_CLIENT_2_MASK   0x2     ///< Mask of UVP client 2
#define UVP_CLIENT_3_MASK   0x4     ///< Mask of UVP client 3
#define UVP_CLIENT_4_MASK   0x8     ///< Mask of UVP client 4
#define UVP_CLIENT_5_MASK   0x10    ///< Mask of UVP client 5
#define UVP_CLIENT_6_MASK   0x20    ///< Mask of UVP client 6
#define UVP_CLIENT_7_MASK   0x40    ///< Mask of UVP client 7
#define UVP_CLIENT_8_MASK   0x80    ///< Mask of UVP client 8
#define UVP_ALL_CLIENT_MASK (UVP_CLIENT_1_MASK | UVP_CLIENT_2_MASK | UVP_CLIENT_3_MASK | UVP_CLIENT_4_MASK | UVP_CLIENT_5_MASK | UVP_CLIENT_6_MASK | UVP_CLIENT_7_MASK | UVP_CLIENT_8_MASK)  ///< Mask for all clients

#define UVP_RXCTRL_SET          1   ///< Value to turn RX on
#define UVP_RXCTRL_RESET        0   ///< Value to turn RX on
#define UVP_TXCTRL_SET          1   ///< Value to turn TX on
#define UVP_TXCTRL_RESET        0   ///< Value to turn TX on
#define UVP_PUCTRL_SET          1   ///< Value to turn PU on
#define UVP_PUCTRL_RESET        0   ///< Value to turn PU on
#define PHASE_CONJUGATE_ENABLE  1   ///< Value to enable phase conjugate mode
#define PHASE_CONJUGATE_DISABLE 0   ///< Value to disable phase conjugate mode


/**
 * @brief Check the reference AMB resides in the ambMask, if not it finds
 *        another AMB to use.
 * @param ambMask A mask indicating which AMB's are being calibrated
 *
 * @return The reference AMB number.
 */
static uint8_t checkAndGetReferenceAmb(uint32_t ambMask)
{
    uint8_t ambRefNum = CAL_REF_AMB_NUM;
    uint8_t ii;

    if (!BIT_IN_MASK(CAL_REF_AMB_NUM, ambMask))
    {
        for (ii = 0; ii < MAX_NUM_AMB; ii++)
        {
            if (BIT_IN_MASK(ii,ambMask))
            {
                ambRefNum = ii;
                break;
            }
        }    
    }
    return ambRefNum;
}

/**
 * @brief Select the beaconing AMB for an AMB being calibrated.
 * 
 * @todo Revisit this with Caner
 * 
 * @param ambMask   A mask indicating which AMB's are being calibrated.
 * @param ambSelNum The individual AMB being calibrated
 *
 * @return Return a recommended num for selecting a calibration AMB.
 */
static uint8_t getBeaconAmb(uint32_t ambMask, ambNum_t ambSelNum)
{
    uint8_t num = ambSelNum;
    uint8_t ii;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        num++;
        num %= MAX_NUM_AMB;

        if (BIT_IN_MASK(num,ambMask))
        {
            break;
        }
    } 
      
    return num;
}

/**
 * @brief Wait for the send power ready register to indicate that the UVP is transmitting
 * 
 * @param ambNum The AMB number containing the UVP transmitting.
 * @param uvpNum The UVP number that is transmitting.
 * 
 * @return  COTA_ERROR_NONE on success, otherwise an error code of type #cotaError_t.
 */
static cotaError_t waitForTxReady(ambNum_t ambNum, uvpNum_t uvpNum)
{
    cotaError_t ret = COTA_ERROR_NONE;
    uint16_t count = 0;
    uint32_t ready = 0;

    while ((ready == 0) && (count++ < MAX_TX_WAIT_COUNT) && (ret == COTA_ERROR_NONE))
    {
        ret = UvpRegisterRead(SEND_POWER_RDY, &ready, ambNum, uvpNum); 
    }

    if ((ready == 0) && (ret == COTA_ERROR_NONE))
    {
        ret = COTA_ERROR_UVP_WAIT_FOR_TRANSMIT_TIMEOUT;
    }
  
    return ret;
}

/**
 * @brief Wait for the phase detect register to indicate that the phases have been read or updated.
 * 
 * @param ambNum The AMB number containing the UVP undergoing phase detect.
 * @param uvpNum The UVP number that is undergoing phase detect.
 * 
 * @return  COTA_ERROR_NONE on success, otherwise an error code of type #cotaError_t.
 */
static cotaError_t waitForPhaseDetectDone(ambNum_t ambNum, uvpNum_t uvpNum)
{
    cotaError_t ret = COTA_ERROR_NONE;
    uint16_t count = 0;
    uint32_t done = 0;

    while ((done == 0) && (count++ < MAX_TX_WAIT_COUNT) && (ret == COTA_ERROR_NONE))
    {
        ret = UvpRegisterRead(PHASE_DETECT_DONE, &done, ambNum, uvpNum);   
    }

    if ((done == 0) && (ret == COTA_ERROR_NONE))
    {
        ret = COTA_ERROR_UVP_WAIT_FOR_PHASE_DET_TIMEOUT;
    }
  
  return ret;
}

/**
 * @brief Write the phase offsets for a specific UVP. 
 *
 * @param ambNum    The AMB number containing the UVP to set the phase offset for
 * @param uvpNum    The UVP number to set the phase offset for.
 * @param amuMask   Indicates which AMUs should have their phase offsets set.
 *
 * @return Return 0 on success and negative values for errors
 */
static cotaError_t WritePhaseOffset(uint32_t offset, ambNum_t ambNum, uvpNum_t uvpNum, amuNum_t amuMask)
{
    amuNum_t amuNum;
    cotaError_t ret = COTA_ERROR_NONE;
    uvp_reg_t phaseOffsetReg[AMUS_PER_UVP] = {PHASE_OFFSET_AMU1, PHASE_OFFSET_AMU2, PHASE_OFFSET_AMU3, PHASE_OFFSET_AMU4};

    for (amuNum = 0; (amuNum < AMUS_PER_UVP) && (ret == COTA_ERROR_NONE); amuNum++)
    {
        if (BIT_IN_MASK(amuNum, amuMask))
        {
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(phaseOffsetReg[amuNum], offset, ambNum, uvpNum) : ret;
        }
    }
    return ret;
}

/**
 * @brief Read detected phase for a specific AMU in a UVP
 * @param out_phase [out] - Phase read directly from the AMUs
 * @param ambNum Indicates which AMB to read from
 * @param uvpNum Indicate which UVP to read from
 * @param amuNum Indicate which AMU to read from
 * @return  COTA_ERROR_NONE on success, otherwise an error code of type #cotaError_t.
 */
static cotaError_t readAmuPhase(uint32_t *outPhase, ambNum_t ambNum, uvpNum_t uvpNum, amuNum_t amuNum)
{   
    uvp_reg_t amuArray[MAX_NUM_AMU] = {OUTPHASE_CLNT1_AMU1, OUTPHASE_CLNT1_AMU2, OUTPHASE_CLNT1_AMU3, OUTPHASE_CLNT1_AMU4};
    return UvpRegisterRead(amuArray[amuNum], outPhase, ambNum, uvpNum);
}

/**
 * @brief This functions calibrates the antennas on the system.
 *
 * ambMask A mask indicating which AMB's will be calibrated.
 * uvpMask A mask indicating which UVP's will be calibrated
 * 
 * @return COTA_ERROR_NONE on success, otherwise an error code of type #cotaError_t.
 */ 
cotaError_t Calibrate(uint32_t ambMask, uint16_t uvpMask)
{
    uint32_t minRssiThresh[TOTAL_UVPS];
    cotaError_t ret = COTA_ERROR_NONE;
    uint8_t refAmbNum;
    uint8_t beaconAmbNum;
    ambNum_t ambNum;
    uvpNum_t uvpNum;
    uint32_t phaseRef;
    uint32_t phaseAut;
    int phaseOffset;
    powerRegister_t startPower;

    //Let's make sure RX, TX, PU pins are disabled.  We will be controlling UVP's 
    //individually with their registers.
    SetAmbRxEnable(VALID_AMBS, false);
    SetAmbTxEnable(VALID_AMBS, false);
    SetAmbPuEnable(VALID_AMBS, false);

    //We are using lower power levels during calibration, so we need to set RSSI_LOWTHRESH to zero,
    //but we need to save their current values so we can restore them after calibration.
    ret = (ret == COTA_ERROR_NONE) ? ReadArrayUvps(RSSI_LOWTHRESH, minRssiThresh, TOTAL_UVPS, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(RSSI_LOWTHRESH, 0, ambMask, uvpMask) : ret; 

    //At the start we also need to ensure the RX, TX, and PU are turned off at the register level too.
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_RXCTRL, UVP_RXCTRL_RESET, VALID_AMBS, ALL_UVP_MASK) : ret;   
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_TXCTRL, UVP_TXCTRL_RESET, VALID_AMBS, ALL_UVP_MASK) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_PUCTRL, UVP_PUCTRL_RESET, VALID_AMBS, ALL_UVP_MASK) : ret;

    //A UVP has many 'clients' that store phase information.  Let's use client 1.
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(CLIENT_SELECT_OUT, 0, ambMask, uvpMask) : ret;
    //This setting ensures that when we update the phase, we use client 1.
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(DETECT_PH_CLIENT_ID_1, 0, ambMask, uvpMask) : ret;   

    //Let's also make sure all the AMU's of all the UVP's are disabled.
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHASE_OFFSET_AMU1, 0, VALID_AMBS, ALL_UVP_MASK) : ret;   
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHASE_OFFSET_AMU2, 0, VALID_AMBS, ALL_UVP_MASK) : ret;   
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHASE_OFFSET_AMU3, 0, VALID_AMBS, ALL_UVP_MASK) : ret;   
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHASE_OFFSET_AMU4, 0, VALID_AMBS, ALL_UVP_MASK) : ret;
  
    //All UVP's should be set to the same power levels.  Let's read one, so we can set them back after calibration.
    for ( ambNum = 0; (ambNum < MAX_NUM_AMB); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }
        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {
            if (!BIT_IN_MASK(ambNum, ambMask))
            {
              continue;
            }
            break;
        }
        break;
    }
    
    ret = (ret == COTA_ERROR_NONE) ? GetUvpPowerLevels(&startPower, ambNum, uvpNum) : ret;

    //We calibrate using the lowest power setting for the UVP's.
    ret = (ret == COTA_ERROR_NONE) ? SetUvpPowerLevels(AMU_POWER_13_DBM, ambMask, uvpMask) : ret;

    //Let's determine what AMB we are going to use for a reference to calibrate to.
    refAmbNum = checkAndGetReferenceAmb(ambMask);

    //Let's enable the selected reference AMU on all UVP's
    ret = (ret == COTA_ERROR_NONE) ? UvpManyAmuEnable(ambMask, uvpMask, NUM_TO_MASK(CAL_REF_AMU_NUM) ) : ret;

    //Alright it's time to calibrate!  Let's loop through the AMB's and ignore the one's not in the ambMask
    for ( ambNum = 0; (ambNum < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); ambNum++)
    {    
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }
        
        //We need to determine what AMB will serve as the beacon.  This varies depending on which AMB we are calibrating
        beaconAmbNum = getBeaconAmb(ambMask, ambNum);
        
        //Enable the AMU that will serve as the beacon.
        ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(beaconAmbNum, CAL_BEACON_UVP_NUM, NUM_TO_MASK(CAL_BEACON_AMU_NUM)) : ret;  
        
        //This tells the UVP to let the phase be selected by the user for all 'clients' on the beaconing UVP
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARB_PHASE_SEL, UVP_ALL_CLIENT_MASK, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;
        
        //The following sets the phases for all AMU's for 'client' 1 on the beaconing UVP.
        //@todo confer with Caner to see if we still need these.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU1_CLNT1, 0, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU2_CLNT1, 256, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU3_CLNT1, 256, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARBIT_PHASE_AMU4_CLNT1, 0, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;

        // Set all UVP's on the AMB to receive mode.
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_RXCTRL, UVP_RXCTRL_SET, NUM_TO_MASK(ambNum), uvpMask) : ret;
      
        // Set the reference UVP to receive mode
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_SET, refAmbNum, CAL_REF_UVP_NUM) : ret;
       
        //Start transmitting out of the beacon
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_SET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;

        ret = (ret == COTA_ERROR_NONE) ? waitForTxReady( beaconAmbNum, CAL_BEACON_UVP_NUM): ret;

        //Let's maske sure the phases are updated for beacon transmit.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;

        //If the reference AMB is different than the one we are currently calibrating.  We need to sample
        //its phase separately.
        if (ambNum != refAmbNum)
        {
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, refAmbNum, CAL_REF_UVP_NUM) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, refAmbNum, CAL_REF_UVP_NUM) : ret;       
        }
        
        ///@todo consider setting the PU in hardware and waiting for phase detect pin.
        //We need to update the phase for all UVP's on the AMB we are calibrating.
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_PUCTRL, UVP_PUCTRL_SET, NUM_TO_MASK(ambNum), uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_PUCTRL, UVP_PUCTRL_RESET, NUM_TO_MASK(ambNum), uvpMask) : ret;
        
        //Wait for phase detects to be done.
        ret = (ret == COTA_ERROR_NONE) ? waitForPhaseDetectDone( refAmbNum, CAL_REF_UVP_NUM) : ret; 

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {    
            if (BIT_IN_MASK(uvpNum, uvpMask))
            {
                ret = waitForPhaseDetectDone(ambNum, uvpNum);
            }
        }
          
        //Take the UVP's and the reference UVP out of receive mode, also stop the beacon from transmitting.
        ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(UVP_RXCTRL, UVP_RXCTRL_RESET, NUM_TO_MASK(ambNum), uvpMask) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_RESET, refAmbNum, CAL_REF_UVP_NUM) : ret;    
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_RESET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;

        //Let's put the beacon in receive mode.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_SET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret; 

        //Now put the reference UVP in TX mode.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_SET, refAmbNum, CAL_REF_UVP_NUM) : ret;    
        ret = (ret == COTA_ERROR_NONE) ? waitForTxReady( refAmbNum, CAL_REF_UVP_NUM): ret;

        //Update the phase of the transmission phase to the conjugate of what was read.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, refAmbNum, CAL_REF_UVP_NUM) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, refAmbNum, CAL_REF_UVP_NUM) : ret;   
            
        //Configure the Cal UVP to select detected phase instead of phase conjugate
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(SEL_PHASE_CONJUGATE, PHASE_CONJUGATE_DISABLE,  beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;

        //Read and update the phase for the beacon.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;  
        ret = (ret == COTA_ERROR_NONE) ? waitForPhaseDetectDone(beaconAmbNum, CAL_BEACON_UVP_NUM): ret;

        //Read the phase it detected by the beacon when the refence AMU is transmitting.
        ret = (ret == COTA_ERROR_NONE) ? readAmuPhase(&phaseRef, beaconAmbNum, CAL_BEACON_UVP_NUM, CAL_REF_AMU_NUM) : ret;

        //Stop the reference from transmitting.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_RESET, refAmbNum, CAL_REF_UVP_NUM) : ret;    
   
        //Let's now iterate through the UVP's, ignoring ones that we don't want to calibrate.
        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {      
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }
      
            //We skip the reference UVP if it's on this AMB.
            if ((ambNum == refAmbNum) && (uvpNum == CAL_REF_UVP_NUM))
            {
                continue;
            }
      
            //Start the UVP transmitting, then wait till it's ready.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_SET, ambNum, uvpNum) : ret;
            ret = (ret == COTA_ERROR_NONE) ? waitForTxReady(ambNum, uvpNum): ret;         
 
            //Update the phase of the transmision signal to the conjugate of what was read earlier
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, ambNum, uvpNum) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, ambNum, uvpNum) : ret;       

            //Read and update the pase for the beacon.
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_SET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, UVP_PUCTRL_RESET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret; 

            ret = (ret == COTA_ERROR_NONE) ? waitForPhaseDetectDone(beaconAmbNum, CAL_BEACON_UVP_NUM): ret;
            ret = (ret == COTA_ERROR_NONE) ? readAmuPhase(&phaseAut, beaconAmbNum, CAL_BEACON_UVP_NUM, CAL_BEACON_AMU_NUM) : ret;

            //Stop transmitting on the UVP
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, UVP_TXCTRL_RESET, ambNum, uvpNum) : ret;

            phaseOffset = phaseRef - phaseAut;
      
            if (phaseOffset < 0)
            {
                phaseOffset += MAX_PHASE_OFFSET;
            }
            
            //Write the offset phase (otherwise known as the calibration phase) to the UVP
            ret = (ret == COTA_ERROR_NONE) ? WritePhaseOffset(phaseOffset, ambNum, uvpNum, ALL_AMU_MASK) : ret;
        }
    
        //Take the beacon out of receive mode.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_RXCTRL, UVP_RXCTRL_RESET, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;

        //unselect user phase determination of the beacon.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(ARB_PHASE_SEL, 0, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;
        
        //restore the phase conjugate mode of the beacon
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(SEL_PHASE_CONJUGATE, PHASE_CONJUGATE_ENABLE, beaconAmbNum, CAL_BEACON_UVP_NUM) : ret;
        
        //Enable the reference AMU on the beacon since it was changed earlier.
        ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(beaconAmbNum, CAL_BEACON_UVP_NUM, NUM_TO_MASK(CAL_REF_AMU_NUM)) : ret; 
    
    }
  
    //Restore the RSSI_LOWTHRESH values
    ret = (ret == COTA_ERROR_NONE) ? WriteArrayUvps(RSSI_LOWTHRESH, minRssiThresh, TOTAL_UVPS, ambMask, uvpMask) : ret;

    //Enable all AMU's on the UVP's  
    ret = (ret == COTA_ERROR_NONE) ? UvpManyAmuEnable(ambMask, uvpMask, ALL_AMU_MASK ) : ret;

    //Restore power levels to what they were before calibration
    ret = (ret == COTA_ERROR_NONE) ? SetUvpPowerLevels(startPower, ambMask, uvpMask) : ret;
    
    return ret; 
}
