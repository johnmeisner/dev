/**
* @file       gather_and_vote_task.h
*
* @brief      Gather and Vote task header file
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*/

#ifndef _GATHER_AND_VOTE_TASK_INCLUDE_H_
#define _GATHER_AND_VOTE_TASK_INCLUDE_H_

#include "semphr.h"

extern SemaphoreHandle_t gGatherAndVoteSem;

void CreateGatherAndVoteTask(void);

#endif /*#ifndef _GATHER_AND_VOTE_TASK_INCLUDE_H_*/