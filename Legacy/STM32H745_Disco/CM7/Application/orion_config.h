/****************************************************************************//**
* @file      orion_config.h
*
* @brief     Header file contain general configuration settings for the application
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _OSSIA_CONFIG_H_
#define _OSSIA_CONFIG_H_

/*
* Cota Transmitter Task Definitions 
*/
#define GATHER_AND_RSSI_VOTING_STACK_SIZE      1024         ///< The size of the stack allocated for the GatherAndVoteTask by FreeRTOS
#define GATHER_AND_RSSI_VOTING_TASK_PRIORITY   3            ///< Priority for the GatherAndVoteTask

#define PROXY_TASK_STACK_SIZE                  1024         ///< Proxy task stack size in bytes
#define PROXY_TASK_PRIORITY                    5            ///< Proxy task stack priority

#define ERROR_HANDLER_STACK_SIZE    1024                    ///< Stack size for the error task
#define ERROR_HANDLER_TASK_PRIORITY 1                       ///< Priority of the error task

#define MASTER_AMB_SPI_PORT   1                             ///< The spi port designated as the master SPI port for AMB data transfers
#define MAX_NUM_AMB           4                             ///< The number of AMB's available on the transmitter.
#define VALID_AMBS            0xF                           ///< A mask indicating which AMB's are actually connected to the CCB and ready to do.

#define CAL_REF_AMB_NUM       0                             ///< The AMB number containing the reference antenna.
#define CAL_REF_UVP_NUM       0                             ///< The UVP number containing the reference antenna.
#define CAL_REF_AMU_NUM       2                             ///< The AMU number of the reference antenna.
#define CAL_BEACON_UVP_NUM    13                            ///< The UVP number to use for the calibration beacon
#define CAL_BEACON_AMU_NUM    1                             ///< The AMU number to use for the calibration beacon

#endif //#ifndef _OSSIA_CONFIG_H_
