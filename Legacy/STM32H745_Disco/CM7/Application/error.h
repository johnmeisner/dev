/**
* @file       error.h
*
* @brief      Cota error logging header file.
*
*             @note  Do not use prototypes defined in this header.  Use the MACROS.
*                    We want to able to save memory by removing error handling.  
*                    By using the MACRO's, we can easily remove error handling by not 
*                    including COTA_ERROR_ENABLED in the compile line.
*
* @todo This is just a simple shell of what should be a larger debug message handler that includes
        a separate task to queue up messages, and masks to prioritize messages.
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*/

#ifndef _COTA_ERROR_INCLUDE_H_
#define _COTA_ERROR_INCLUDE_H_

typedef enum _cotaError_t
{
  COTA_ERROR_NONE                           = 0,  ///< Indicates a Non-Error from a function call
  COTA_ERROR_HAL                            = 1,  ///< Indicates there was an error from a HAL driver
  COTA_ERROR_QUEUE_FAILED                   = 2,  ///< Indicates the error handler failed to create its queue
  COTA_ERROR_TASK_FAILED                    = 3,  ///< Indicates the error handler failed to create its task.
  COTA_ERROR_SPI_TRANSFER_FAILED            = 4,  ///< A SPI transfered has failed
  COTA_ERROR_SPI_SIMUL_TRANS_FAILED         = 5,  ///< A simlultaneous SPI transfered has failed
  COTA_ERROR_UVP_SIMUL_READ_TO_LARGE        = 6,  ///< A simulataneous read of several UVP's has a buffer that is too large.
  COTA_ERROR_UART_TX_FAILED                 = 7,  ///< A UART transmit failed
  COTA_ERROR_UVP_LOCK_FAILED                = 8,  ///< An attempt to lock a UVP clock failed 
  COTA_ERROR_UVP_TEST_FAILED                = 9,  ///< The uvp test failed.
  COTA_ERROR_UVP_WAIT_FOR_TRANSMIT_TIMEOUT  = 10, ///< An error occured while waiting for a UVP to transmit
  COTA_ERROR_UVP_REG_VALUE_NOT_IN_MASK      = 11, ///< A UVP register value did not fit in its mask.
  COTA_ERROR_UVP_WAIT_FOR_PHASE_DET_TIMEOUT = 12, ///< An error occured while wait for a phase detect from a UVP
  COTA_ERROR_UVP_ENABLE                     = 13, ///< An error occured while enabling UVP's
} cotaError_t;

#ifdef COTA_ERROR_ENABLED
cotaError_t PostCotaError(cotaError_t cotaError);
#define POST_COTA_ERROR(num)      (PostCotaError(num))
#else
#define POST_COTA_ERROR(num) (num)
#endif

#endif