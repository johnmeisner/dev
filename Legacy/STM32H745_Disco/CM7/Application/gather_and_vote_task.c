/**
 * @file       gather_and_vote_task.c
 *
 * @brief      Gather and Vote task implementation
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.

  @verbatim
  ===============================================================================
   The Gather and Vote - Download Phase, RSSI, and complete RSSI Voting
  ===============================================================================
  -------------------------------------------------------------------------------
   Introduction 
  -------------------------------------------------------------------------------

  This task will complete the following tasks in order:
    1) Gather all RSSI data from the UVPs. 
    2) Complete RSSI Voting. Based on the results of RSSI voting, either continue
         or terminate transmission.
    3) Gather all phase data from the UVPs.

  Information for how RSSI and Phase information gets downloaded can be found as 
    part of the SiWare documentation. For more information, those documents can
    be found on either ossianet, or SVN.

  -------------------------------------------------------------------------------
   RSSI Voting
  -------------------------------------------------------------------------------

  To meet SAR (Specific Absorbtion Rate) requirements established by the FCC, we 
    do a procedure called "RSSI Voting". If a transmitter becomes blocked by a 
    body, we will disable transmission out of select areas of the transmitter, 
    or terminate transmission altogether. 

  We do this in two ways:
    1) Using the RSSI_LOWTHRESH register in the UVPs. When a client outputs a 
         beacon as part of TPS, each UVP will compare the RSSI of the recently 
         measured beacon, with the RSSI saved in the RSSI_LOWTHRESH register. 
         If the measured RSSI is less than the RSSI in RSSI_LOWTHRESH, then the
         AMU has 'violated' the low threshold. Violating AMUs will not transmit. 
         This entire operation occurs in hardware on the UVP.

    2) Executing a process called "RSSI Voting". When a client outputs a beacon as 
         part of TPS, the MCU will download all the RSSI and Phase data from the 
         UVPs. Once it downloads the RSSI data, we do RSSI Voting. For RSSI 
         Voting, we will count the number of 'violating' AMUs. If the number of 
         violating AMUs is greater than a configurable amount, then transmission
         is terminated (for either a single AMB, or for the rest of the TPS cycle).
         This entire operation occurs in software on the MCU.

  -------------------------------------------------------------------------------
   How to Complete the Operation
  -------------------------------------------------------------------------------
  In order to do RSSI Voting properly, the following steps must be taken.

  At manufacturing:
    1) At manufacture time, take an RSSI treshold baseline. Pair the transmitter 
         with a client, capture a beacon, then save all the RSSI values to an 
         array. Then subtract a constant value (in dB) from the RSSI, this will 
         become the RSSI threshold baseline. This baseline will need to be 
         saved and reloaded at boot-up.

  At boot-up:
    2) For each of these baseline values, write the value to the RSSI_LOWTHRESH 
         register on the corresponding AMUs for the UVPs.

  During operation:
    3) After the phase update pulse that occurs during the Rx phase of TPS, we 
         download all the RSSI data from the UVPs.

    4) Compare all the measured RSSI with the threshold baseline and count the
         the number of AMUs that measured a Violating RSSI. If the number of 
         violating AMUs was greater than a set value, we will either disable an
         AMB, or disable transmission entirely.

  @endverbatim 
*/

/*
* Includes
*/
#include "FreeRTOS.h"
#include "gather_and_vote_task.h" 
#include "stm32h7xx_hal.h"
#include "cmsis_os.h"

#include "stdbool.h"
#include "stdint.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "orion_config.h"
#include "error.h"
#include "main.h"

#include "projdefs.h"
#include "task.h"
#include "semphr.h"

#include "debug_off.h"
/*
* Defines
*/
#define RSSI_EXTRACT_AMU0(lRssiData)    (lRssiData) //((lRssiData >> 6 )  & 0x1fff)  ///< Extracts a RSSI for AMU0 from a 64-bits worth of traffic from the UVP
#define RSSI_EXTRACT_AMU1(lRssiData)    (lRssiData) //((lRssiData >> 19)  & 0x1fff)  ///< Extracts a RSSI for AMU1 from a 64-bits worth of traffic from the UVP
#define RSSI_EXTRACT_AMU2(lRssiData)    (lRssiData) //((lRssiData >> 32)  & 0x1fff)  ///< Extracts a RSSI for AMU2 from a 64-bits worth of traffic from the UVP
#define RSSI_EXTRACT_AMU3(lRssiData)    (lRssiData) //((lRssiData >> 45)  & 0x1fff)  ///< Extracts a RSSI for AMU3 from a 64-bits worth of traffic from the UVP

#define DEFAULT_NUM_VIOLATING_ANTENNAS  32  ///< The default value for setting the number of Violating antennas for RSSI voting. 

/*
* Task Architecture Variables
*/
SemaphoreHandle_t    gGatherAndVoteSem        = NULL;  ///< The Gather and Vote Task semaphore 
static TaskHandle_t  gGatherAndVoteTaskHandle = NULL;  ///< The Gather and Vote Task handle

/*
* Private (static) Variables
*/
static uint32_t gMaxNumOfViolatingAntenna       = DEFAULT_NUM_VIOLATING_ANTENNAS; ///< The number of violating AMUs that disables transmission
static uint16_t gRssiBaselineThesholds [MAX_NUM_AMB][UVPS_PER_AMB][MAX_NUM_AMU];  ///< An array of RSSI threshold baselines 

static uint16_t gPhaseData             [MAX_NUM_AMB][UVPS_PER_AMB][MAX_NUM_AMU];  ///< A multi-dimensional array that holds the phase data for each beacon
static uint16_t gRssiData              [MAX_NUM_AMB][UVPS_PER_AMB][MAX_NUM_AMU];  ///< A multi-dimensional array that holds the RSSI data for each beacon

/*
* Private (static) functions
*/
static void GatherAndVoteTask(void *pvParameters);
static BaseType_t GatherAndVoteTaskInit(void);
static cotaError_t gatherAllRssiData(void);
static cotaError_t gatherAllPhaseData(void);
static void completeRssiVoting(void);
static void disableTransmission(uint32_t ambMask);
static void setRssiVotingDecision(uint8_t set);
//static void enableTransmission(void);

/**
* @brief The GatherAndVote task. This task will download all RSSI and Phase 
*          data from the UVPs, and perform RSSI voting to disable transmission.
*/
static void GatherAndVoteTask (void *pvParameters)
{
  for (;;)
  {
    xSemaphoreTake(gGatherAndVoteSem, portMAX_DELAY);
    
    // Set RSSI Voting low as we are about to get new beacon data
    setRssiVotingDecision(0); ///@todo For Demo purposes, remove in CCB iteration of code
    
    if (gatherAllRssiData())
    {
      ///@todo add error logging
    }else{}
    
    // RSSI Voting is time sensitive, so we do RSSI Voting before gathering phase data. 
    completeRssiVoting();
    setRssiVotingDecision(1); ///@todo For Demo purposes, remove in CCB iteration of code
    
    if (gatherAllPhaseData())
    {
      ///@todo add error logging
    }else{}
    
  }
}

/**
* @brief This function creates the GatherAndVote task
*/
void CreateGatherAndVoteTask(void)
{
  BaseType_t xReturned;
  
  while (GatherAndVoteTaskInit() != pdPASS);
  
  /*
  * TPS Task Creation
  */
  xReturned = xTaskCreate(
                  GatherAndVoteTask,                    /* Function that implements the task. */
                  "Gather Phase/RSSI and Rssi Voting",  /* Text name for the task. */
                  GATHER_AND_RSSI_VOTING_STACK_SIZE,    /* Stack size in bytes. */
                  NULL,                                 /* Parameter passed into the task. */
                  GATHER_AND_RSSI_VOTING_TASK_PRIORITY, /* Priority at which the task is created. */
                  &gGatherAndVoteTaskHandle );           /* Used to pass out the created task's handle. */
  
  while (xReturned != pdPASS);
    
}

/**
* @brief  This function initializes all resources for the GatherAndVote task
* @return pdPASS or pdFAIL if the operation succeeds or fails
*/
static BaseType_t GatherAndVoteTaskInit(void)
{
  BaseType_t retSuccess;
  
  retSuccess = pdPASS;
  
  gGatherAndVoteSem = xSemaphoreCreateBinary();
  if (gGatherAndVoteSem == NULL)
  {
    retSuccess = pdFAIL;
  }
  
  return retSuccess;
}

/**
* @brief  This function asserts/unasserts the RSSI_VOTING_DONE gpio. 
* @todo   When we begin development on the CCB, this function should disappear.
*/
static void setRssiVotingDecision(uint8_t set)
{
  HAL_GPIO_WritePin(RSSI_VOTING_DONE_GPIO_Port, RSSI_VOTING_DONE_Pin, ((GPIO_PinState) (set ? GPIO_PIN_SET : GPIO_PIN_RESET)));
}

/**
* @brief  This function gathers all Phase data from the UVPs and stores it in a 
*           buffer.
* @return A cotaError_t that denotes if the function call was successful.
*/
static cotaError_t gatherAllPhaseData(void)
{
  cotaError_t retSuccess;
  static uint32_t spiReadData[TOTAL_UVPS];
  uint8_t amb, uvp;
  
  /* Important note:
  *    All of the PCB/code names and indices are 0-indexed. SiWare's asic
  *    starts at 1. Therefore, SiWare registers such as OUTPHASE_CLNT1_AMU1 is 
  *    for AMU0 as so on. 
  */   
  
  /*
  * Read AMU0 Data for each AMB/ACL
  */
  DBG2_SET();
  retSuccess = ReadArrayUvps(OUTPHASE_CLNT1_AMU1, spiReadData, TOTAL_UVPS, ALL_AMB_MASK, ALL_UVP_MASK); // 32-bit read
  
  for (amb = 0; amb < MAX_NUM_AMB; amb++)
  {
    for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
    {
      gPhaseData[amb][uvp][AMU0] = spiReadData[UVP_INDEX(amb,uvp)];
    }
  }
  DBG2_RESET();
  DBG2_SET();
  
  /*
  * Read AMU1 Data for each AMB/ACL
  */
  retSuccess = ReadArrayUvps(OUTPHASE_CLNT1_AMU2, spiReadData, TOTAL_UVPS, ALL_AMB_MASK, ALL_UVP_MASK); // 32-bit read
  
  for (amb = 0; amb < MAX_NUM_AMB; amb++)
  {
    for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
    {
      gPhaseData[amb][uvp][AMU1] = spiReadData[UVP_INDEX(amb,uvp)];
    }
  }
  DBG2_RESET();
  DBG2_SET();
  /*
  * Read AMU2 Data for each AMB/ACL
  */
  retSuccess = ReadArrayUvps(OUTPHASE_CLNT1_AMU3, spiReadData, TOTAL_UVPS, ALL_AMB_MASK, ALL_UVP_MASK); // 32-bit read
    
  for (amb = 0; amb < MAX_NUM_AMB; amb++)
  {
    for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
    {
      gPhaseData[amb][uvp][AMU2] = spiReadData[UVP_INDEX(amb,uvp)];
    }
  }
  DBG2_RESET();
  DBG2_SET();
  /* 
  * Read AMU3 Data for each AMB/ACL
  */
  retSuccess = ReadArrayUvps(OUTPHASE_CLNT1_AMU4, spiReadData, TOTAL_UVPS, ALL_AMB_MASK, ALL_UVP_MASK); // 32-bit read
  
  for (amb = 0; amb < MAX_NUM_AMB; amb++)
  {
    for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
    {
      gPhaseData[amb][uvp][AMU3] = spiReadData[UVP_INDEX(amb,uvp)];
    }
  }
  DBG2_RESET();
  
  ///@todo when driver returns cotaError_t types, remove this and update this function. 
  return retSuccess;
}

/**
* @brief  This function gathers all RSSI data from the UVPs and stores it in a 
*           buffer.
* @todo   This function will need to be modified when all SPI drivers have been 
*           verified.
* @return A cotaError_t that denotes if the function call was successful.
*/
static cotaError_t gatherAllRssiData(void)
{
//  uint64_t spiReadData[MAX_NUM_UVP];
  static uint32_t spiReadData[TOTAL_UVPS];
  cotaError_t retSuccess;
  int amb, uvp;
  
  amb = 0;
  uvp = 0;
  
    /* Important note:
  *    All of the PCB/code names and indices are 0-indexed. SiWare's asic
  *    starts at 1. Therefore, SiWare registers such as OUTPHASE_CLNT1_AMU1 is 
  *    for AMU0 as so on. 
  */ 
  
  // This code will effciently pull RSSI data while the SPIDriver calls are being debugged. 
//  // By reading 64-bits from RSSi_AMU1, we read all RSSI values from all AMUs
//  retSuccess = ReadArrayUvps64(spiReadData, MAX_NUM_UVP, RSSi_AMU1.pageIndex, RSSi_AMU1.colAddr, MAX_AMB_MASK, MAX_ACL_MASK); // 64-bit read
//  
//  for (amb = 0; amb < MAX_NUM_AMB; amb++)
//  {
//    for (uvp = 0; uvp < MAX_NUM_ACL; uvp++)
//    {
//      gRssiData[amb][uvp][AMU0] = RSSI_EXTRACT_AMU0(spiReadData[UVP_INDEX(amb,uvp)]);
//      gRssiData[amb][uvp][AMU1] = RSSI_EXTRACT_AMU1(spiReadData[UVP_INDEX(amb,uvp)]);
//      gRssiData[amb][uvp][AMU2] = RSSI_EXTRACT_AMU2(spiReadData[UVP_INDEX(amb,uvp)]);
//      gRssiData[amb][uvp][AMU3] = RSSI_EXTRACT_AMU3(spiReadData[UVP_INDEX(amb,uvp)]);
//    }
//  }
  
  /*
  * Read AMU0 Data for each AMB/ACL
  */
  DBG2_SET();
  retSuccess = ReadArrayUvps(RSSi_AMU1, spiReadData, TOTAL_UVPS, ALL_AMB_MASK, ALL_UVP_MASK); // 32-bit read
  
  for (amb = 0; amb < MAX_NUM_AMB; amb++)
  {
    for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
    {
      gRssiData[amb][uvp][AMU0] = RSSI_EXTRACT_AMU0(spiReadData[UVP_INDEX(amb,uvp)]);
    }
  }
  DBG2_RESET();
  DBG2_SET();
  
  retSuccess = ReadArrayUvps(RSSi_AMU2, spiReadData, TOTAL_UVPS, ALL_AMB_MASK, ALL_UVP_MASK); // 32-bit read
  
  for (amb = 0; amb < MAX_NUM_AMB; amb++)
  {
    for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
    {
      gRssiData[amb][uvp][AMU1] = RSSI_EXTRACT_AMU1(spiReadData[UVP_INDEX(amb,uvp)]);
    }
  }
  DBG2_RESET();
  DBG2_SET();
  
  /*
  * Read AMU1 Data for each AMB/ACL
  */
  retSuccess = ReadArrayUvps(RSSi_AMU3, spiReadData, TOTAL_UVPS, ALL_AMB_MASK, ALL_UVP_MASK); // 32-bit read
  
  for (amb = 0; amb < MAX_NUM_AMB; amb++)
  {
    for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
    {
      gRssiData[amb][uvp][AMU2] = RSSI_EXTRACT_AMU2(spiReadData[UVP_INDEX(amb,uvp)]);
    }
  }
  DBG2_RESET();
  DBG2_SET();
  
  retSuccess = ReadArrayUvps(RSSi_AMU4, spiReadData, TOTAL_UVPS, ALL_AMB_MASK, ALL_UVP_MASK); // 32-bit read
  
  for (amb = 0; amb < MAX_NUM_AMB; amb++)
  {
    for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
    {
      gRssiData[amb][uvp][AMU3] = RSSI_EXTRACT_AMU3(spiReadData[UVP_INDEX(amb,uvp)]);
    }
  }
  DBG2_RESET();
  
  ///@todo when driver returns cotaError_t types, remove this and update this function. 

  return retSuccess;
}

/**
* @brief This function completes RSSI Voting, which will terminate TPS during a 
           TPS cycle if RSSI values do not check out. 
* @todo  When we get to CCB design, disableTransmission code will need to 
           be fleshed out.
*/
static void completeRssiVoting(void)
{
  uint16_t amb, uvp, amu;
  uint16_t numViolatingAmu;
  
  numViolatingAmu = 0;
  
  for (amb = 0; amb < MAX_NUM_AMB; amb++)
  {
    for (uvp = 0; uvp < UVPS_PER_AMB; uvp++)
    {
      for (amu = 0; amu < MAX_NUM_AMU; amu++)
      {
        if (gRssiData[amb][uvp][amu] < gRssiBaselineThesholds[amb][uvp][amu])
          numViolatingAmu++; 
      }
    }
    
    if (numViolatingAmu >= gMaxNumOfViolatingAntenna)
    {
      disableTransmission(NUM_TO_MASK(amb));
      break;
    }
  }
}

/**
* @brief This function disables transmission during a TPS cycle. 
* @todo  When the CCB comes in, the function should get fleshed out. 
*/
static void disableTransmission(uint32_t ambMask)
{
  // Disable TxLine
}
