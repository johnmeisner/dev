/**
 * @FILE       debug_on.h
 *
 * @BRIEF      Debug pin control macros for modules with debug pins enabled
 *
 * @COPYRIGHT  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#define DBG1_SET()    HAL_GPIO_WritePin(DBG1_GPIO_Port, DBG1_Pin, GPIO_PIN_SET)     ///< Set debug pin 1 high
#define DBG2_SET()    HAL_GPIO_WritePin(DBG2_GPIO_Port, DBG2_Pin, GPIO_PIN_SET)     ///< Set debug pin 2 high
#define DBG3_SET()    HAL_GPIO_WritePin(DBG3_GPIO_Port, DBG3_Pin, GPIO_PIN_SET)     ///< Set debug pin 3 high
#define DBG4_SET()    HAL_GPIO_WritePin(DBG4_GPIO_Port, DBG4_Pin, GPIO_PIN_SET)     ///< Set debug pin 4 high
#define DBG5_SET()    HAL_GPIO_WritePin(DBG5_GPIO_Port, DBG5_Pin, GPIO_PIN_SET)     ///< Set debug pin 5 high
#define DBG1_RESET()  HAL_GPIO_WritePin(DBG1_GPIO_Port, DBG1_Pin, GPIO_PIN_RESET)   ///< Set debug pin 1 low
#define DBG2_RESET()  HAL_GPIO_WritePin(DBG2_GPIO_Port, DBG2_Pin, GPIO_PIN_RESET)   ///< Set debug pin 2 low
#define DBG3_RESET()  HAL_GPIO_WritePin(DBG3_GPIO_Port, DBG3_Pin, GPIO_PIN_RESET)   ///< Set debug pin 3 low
#define DBG4_RESET()  HAL_GPIO_WritePin(DBG4_GPIO_Port, DBG4_Pin, GPIO_PIN_RESET)   ///< Set debug pin 4 low
#define DBG5_RESET()  HAL_GPIO_WritePin(DBG5_GPIO_Port, DBG5_Pin, GPIO_PIN_RESET)   ///< Set debug pin 5 low
