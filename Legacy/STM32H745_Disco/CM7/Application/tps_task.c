/**
 * @file       tps_task.c
 *
 * @brief      TPS task implementation
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "FreeRTOS.h"
#include "tps_task.h" 
#include "stm32h7xx_hal.h"
#include "cmsis_os.h"

#include "tps_state_machine.h"
#include "stdbool.h"
#include "stdint.h"

#include "projdefs.h"
#include "task.h"
#include "semphr.h"
#include "gather_and_vote_task.h"

#include "main.h"
#include "amb_control.h"
#include "uvp_driver.h"


#define TPS_SM_STACK_SIZE    500   ///< The stack size that is allocated for the TPS task
#define TPS_BB_COUNT         50    ///< Number of beacon beats per TPS cycle
// The lower the priority number, the lower the priority of the task. IDLE Task has priority 0
#define TPS_TASK_PRIORITY    4     ///< The Priority of TPS Task

static void TriggerPuPulse(void);
static BaseType_t TpsTaskInit(void);
static void TpsTask(void *pvParameters);

static TaskHandle_t tpsTaskHandle = NULL;
static SemaphoreHandle_t gTpsSem = NULL;
static StateMachineState_t *gCurrState;

static uint8_t rxLine;
static uint8_t txLine;
static bool    puEvent;

extern TIM_HandleTypeDef htim15;

void TpsTask(void *pvParameters)
{
#ifdef UVP_TEST_ENABLED
  TestUvp();
#endif
  while (TpsTaskInit() != pdPASS);

  // Advance the state machine from initialization state to the next state
  gCurrState = RunStateMachine(gCurrState);
  
  for (;;)
  {
    xSemaphoreTake(gTpsSem, portMAX_DELAY);
    
    // Update the current state with the next state in the state machine. 
    gCurrState = RunStateMachine(gCurrState);
  }
}

void CreateTpsStateMachineTask(void)
{
  BaseType_t xReturned;
  
  /*
  * TPS Task Creation
  */
  xReturned = xTaskCreate(
                  TpsTask,           /* Function that implements the task. */
                  "TPS Task",        /* Text name for the task. */
                  TPS_SM_STACK_SIZE, /* Stack size in bytes. */
                  NULL,              /* Parameter passed into the task. */
                  TPS_TASK_PRIORITY, /* Priority at which the task is created. */
                  &tpsTaskHandle );  /* Used to pass out the created task's handle. */
  
  while (xReturned != pdPASS);
    
}

static BaseType_t TpsTaskInit(void)
{
  BaseType_t retSuccess;
  
  retSuccess = pdPASS;
  
  gTpsSem = xSemaphoreCreateBinary();
  if (gTpsSem == NULL)
  {
    retSuccess = pdFAIL;
  }
  
  gCurrState = StateMachineInit();
  
  return retSuccess;
}

/**
  * @brief  This function unblocks the main TPS task processing loop
  *
  * @note   This function is designed to be called from ISR context.
  *
  */
void TpsRunStateMachineFromIsr(void)
{
  static BaseType_t xHigherPriorityTaskWoken = pdFALSE;
  xSemaphoreGiveFromISR(gTpsSem, &xHigherPriorityTaskWoken);
  return;
}


static void TriggerPuPulse(void)
{
  puEvent = true;

  // Start phase update (PU) pulse generation after a delay
  HAL_TIM_PWM_Start_IT(&htim15, TIM_CHANNEL_2);
}

/**
  * @brief  Function called when timer 1 expires
  * @retval None
  */
void TpsTimerCallback(void)
{
  // This code may be useful for the real CCB.  It's not doing anything in the demo project running on the Discovery board
  
  // static unsigned char ucLocalTickCount = 0;
  // static BaseType_t xHigherPriorityTaskWoken = pdFALSE;

  //xHigherPriorityTaskWoken = pdFALSE;

  // Rx active
  rxLine = 1;
  txLine = 0;
    
  // xSemaphoreGiveFromISR(gTpsSem, &xHigherPriorityTaskWoken);
  // portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}


// TODO: Move this to main.c
void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef *htim)
{
  static BaseType_t xHigherPriorityTaskWoken = pdFALSE;
  static uint8_t isRxPulse;
  
  
  if (htim->Instance == TIM1)
  {
    if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)
    {
      // PU pulse 2 (during Tx phase)
      TriggerPuPulse();

      // Tx active
      rxLine = 0;
      txLine = 1;
      isRxPulse = 0;
    }
    else if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_2)
    {
      // PU pulse 1 (during Rx phase)
      TriggerPuPulse();
      isRxPulse = 1;
    }
    else{}
  }
  else if (htim->Instance == TIM15)
  {
    // Stop phase update (PU) pulse generation after the PU pulse
    HAL_TIM_PWM_Stop_IT(&htim15, TIM_CHANNEL_2);
    
    static int puPulseCounter = 0;
    puPulseCounter++;

    // Half-way through the TPS cycle, toggle the green LED
    if ((puPulseCounter / 2) == (TPS_BB_COUNT / 2))
    {
      HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);
    }

    // At the end of the TPS cycle (after the last Rx pulse), stop PWM to end TPS
    if ((puPulseCounter / 2) >= TPS_BB_COUNT)
    {
      HAL_GPIO_TogglePin(LD1_GPIO_Port, LD1_Pin);

      puPulseCounter = 0;
      
      // Signal to the TPS state machine to move forward to the next state
      TpsRunStateMachineFromIsr();
    }
    if (isRxPulse)
    {
      xSemaphoreGiveFromISR(gGatherAndVoteSem, &xHigherPriorityTaskWoken);
      portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
    }
  }
}