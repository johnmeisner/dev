/****************************************************************************//**
* @file      power_level_mock.c
*
* @brief     This implements power level mocks used in the testing.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "main.h"
#include "power_level.h"
#include "uvp_driver.h"
#include "amb_control.h"
#include "error.h"

cotaError_t SetUvpPowerLevels(powerRegister_t pow, uint32_t ambMask, uint16_t uvpMask)
{
    cotaError_t ret = COTA_ERROR_NONE;
    
    return ret;
  
}

cotaError_t SetAmbPowerLevels(uint32_t ambMask,  float requestedPowerLevel, float* actualPowerLevel)
{
    cotaError_t ret = COTA_ERROR_NONE;

    return ret;
}

cotaError_t GetUvpPowerLevels(powerRegister_t* pow, ambNum_t ambNum, uvpNum_t uvpNum)
{
    return COTA_ERROR_NONE;
}