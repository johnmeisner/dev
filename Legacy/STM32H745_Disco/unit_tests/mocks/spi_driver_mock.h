/****************************************************************************//**
* @file      spi_driver_mock.h
*
* @brief     This contains the prototypes that control the spi driver mock.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/#ifndef _SPI_DRIVER_MOCK_H_
#define _SPI_DRIVER_MOCK_H_


void SetSpiTransferReturnValue(cotaError_t errVal);
void InitUvpSimulatorForCalibrationTest(void);
void TearDownUvpSimulator(void);
bool CalibrationSimulatorInCorrectState(void);
#endif //_SPI_DRIVER_MOCK_H_
