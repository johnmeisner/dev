/****************************************************************************//**
* @file      amb_control_mock.c
*
* @brief     This header includes prototypes to control the amb control mock for unit testing.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _AMB_CONTROL_MOCK_H_
#define _AMB_CONTROL_MOCK_H_

uint32_t GetUvpNumberFromMock(void);
uint32_t GetSpiEnabledFromMock(void);

#endif //_AMB_CONTROL_MOCK_H_
