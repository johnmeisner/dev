/****************************************************************************//**
* @file      amb_control_mock.c
*
* @brief     This implements amb control mocks used in the testing.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "main.h"
#include "amb_control.h"

static uint32_t mockUvpNumber;
static uint32_t mockSpiEnabled;


uint32_t GetUvpNumberFromMock(void)
{
  return mockUvpNumber;
}

uint32_t GetSpiEnabledFromMock(void)
{
  return mockSpiEnabled;
}

//mock
void AmbControlInit(void)
{

}

//mock
void SetAmbAddrPins(uvpNum_t uvpNum)
{ 
  mockUvpNumber = uvpNum;
}

//mock
void SetSpiEnabledPins(uint32_t ambMask, bool on)
{
  if (on)
  {
    mockSpiEnabled |= ambMask;
  }
  else
  {
    mockSpiEnabled &= (~ambMask);
  }
}

//mock
cotaError_t SetUvpEnable(uint32_t ambMask, bool on)
{
  return COTA_ERROR_NONE;
}

//mock
void SetAmbEnablePins(uint32_t ambMask, bool on)
{
}

//mock
SPI_HandleTypeDef* GetSpiHandle(ambNum_t ambNum)
{  
  return NULL;
}

//mock
SPI_HandleTypeDef* GetMasterSpiHandle(void)
{
  return NULL;
}

//mock
void SetAmbRxEnable(uint32_t ambMask, bool on)
{
}

//mock
void SetAmbTxEnable(uint32_t ambMask, bool on)
{
}

//mock
void SetAmbPuEnable(uint32_t ambMask, bool on)
{
}