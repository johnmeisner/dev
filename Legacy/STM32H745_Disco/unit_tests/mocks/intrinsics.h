/****************************************************************************//**
* @file      intrinsics.c
*
* @brief     This header is implement automatically by the IAR compiler, so we implement 
*            an emptry version of it for unit testing so other compilers can compile the code.
*             
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/#ifndef _INSTRINSICS_MOCK_H_
#define _INSTRINSICS_MOCK_H_


#endif //_INSTRINSICS_MOCK_H_
