Legacy contains all the projects that happen in this repository prior to the CCB project being designed. 

The project is are as follows:

CubeMx743 - The first project that demonstrated the FreeRTOS-CLI. Simple, with some directions for importing CLI into a new project. 
STM32H745_Disco_ - The second project, with the beginning of the CCB software development. This was primarily used to test TPS, but didn't have enough pins to adequately test the rest of the peripherals before the CCB came in. Therefore, we migrated the code to the STMH745_Nucleo.
STM32H745_Nucleo_ The last project before the CCB came in. This was the workhorse for a majority of the project, and most of the git revision history will be found in this directory. 
