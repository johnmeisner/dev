In order to run the unit test, you need to make sure gcc and make are installed on you computer.
I instatlled the 32 bit tools "Ruby+Devkit 2.6.4-1 (x86)" located at https://rubyinstaller.org/downloads/
Note: The 64 bit tools have trouble compiling the unit test, since they compiling arm code intended for 32 bit system.
After I installed the tools, I had to add the two directories to my path:

C:\Ruby26\msys32\mingw32\bin;
C:\Ruby26\msys32\usr\bin;

After that, I can run the tests. simply type make.

c:\ossia\orion\STM32H745_Disco\unit_tests>make
rm -f calibration_test.exe
gcc -std=c89 -Wall -Wextra -Wpointer-arith -Wcast-align -Wwrite-strings -Wswitch-default -Wunreachable-code -Winit-self -Wmissing-field-initializers -Wno-unknown-pragmas -Wstrict-prototypes -Wundef -Wold-style-definition -m32 -Wno-undef -Wno-unused-parameter -Wno-unused-value -std=c99 -I../../unitytest/src -I..\CM7\Core\Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS-Plus/Source/FreeRTOS-Plus-CLI -I../CM7/Application -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/IAR/ARM_CM4F -I..\..\unitytest\src -I..\..\cotacommon -I.\mocks -DCORE_CM7 -DUSE_HAL_DRIVER -DSTM32H745xx -DUNIT_TESTS ../../unitytest/src/unity.c tests/calibration_test.c ../CM7/Core/Src/calibrator.c ../CM7/Core/Src/uvp_driver.c ./mocks/amb_control_mock.c ./mocks/spi_driver_mock.c ./mocks/power_level_mock.c ./mocks/hal_mocks.c ./mocks/freertos_mocks.c -o calibration_test.exe
./calibration_test.exe
tests/calibration_test.c:39:test_calibrationFailsIfFirstSpiTransferFails:PASS
tests/calibration_test.c:40:test_CalibrationPass:PASS

-----------------------
2 Tests 0 Failures 0 Ignored
OK
