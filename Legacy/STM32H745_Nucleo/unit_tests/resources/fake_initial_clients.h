/****************************************************************************//**
* @file      fake_initial_clients.h
*
* @brief     This is the header function that implements the clients for unit test
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#ifndef _FAKE_INIT_CLIENTS_H_
#define _FAKE_INIT_CLIENTS_H_

#include "CotaCommonTypes.h"

typedef struct _fakeClientList_t
{
    ExtdAddr_t  *clientIdArray;     ///< An array of randomly generated longIds
    uint8_t     *batteryLevelArray; ///< An array of randomly generated battery levels
    unsigned int arraySize;         ///< The size of both the clientId and batteryLevel arrays
} fakeClientList_t;

int createFakeClients(fakeClientList_t *fakeClientList, unsigned int size);
void freeFakeClientList(fakeClientList_t *fakeClientList);


#endif /* #ifndef _FAKE_INIT_CLIENTS_H_ */
