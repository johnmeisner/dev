/****************************************************************************//**
* @file      client_manager_test.c
*
* @brief     This implements the client manager unit tests.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "intrinsics.h"
#include "unity.h"
#include "error.h"
#include "avl_test_helper.h"
#include <stdio.h>

#define TEST_SUCCESS 0
#define TEST_FAILURE 1
#define NUM_CLIENT_IN_CLIENT_MANAGER

void setUp(void)
{
    
}

void tearDown(void)
{

}

void test_AddRemoveClients(void)
{
    cotaError_t errorCode;

    errorCode = COTA_ERROR_NONE;

    errorCode = AddRemoveClients();

    TEST_ASSERT_MESSAGE((errorCode == COTA_ERROR_NONE),
                        "An error code was returned during operation");
}

int main(void)
{   
    UNITY_BEGIN();
    RUN_TEST(test_AddRemoveClients);
    return UNITY_END();
}
