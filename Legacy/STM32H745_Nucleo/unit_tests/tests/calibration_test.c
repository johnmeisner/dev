/****************************************************************************//**
* @file      calibration_test.c
*
* @brief     This implements calibration unit tests.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "unity.h"
#include "main.h"
#include "orion_config.h"
#include "amb_control.h"
#include "calibrator.h"
#include "spi_driver_mock.h"
#include "amb_control_mock.h"
#include <stdio.h>

void setUp(void)
{
  InitializeSpiSimulator();
}

void tearDown(void)
{
  SetSpiTransferReturnValue(COTA_ERROR_NONE);
  TearDownUvpSimulator();
}

void test_calibrationFailsIfFirstSpiTransferFails(void)
{
   SetSpiTransferReturnValue(COTA_ERROR_SPI_TRANSFER_FAILED);
   TEST_ASSERT_EQUAL(COTA_ERROR_SPI_TRANSFER_FAILED, Calibrate(ALL_AMB_MASK, ALL_UVP_MASK));
}

void test_CalibrationPass(void)
{
  cotaError_t errorCode;
  InitUvpSimulatorForCalibrationTest();
  errorCode = Calibrate(ALL_AMB_MASK, ALL_UVP_MASK);
  TEST_ASSERT_EQUAL(COTA_ERROR_NONE,errorCode);
  TEST_ASSERT_TRUE(CalibrationSimulatorInCorrectState());
}

int main(void) {
    UNITY_BEGIN();
    RUN_TEST(test_calibrationFailsIfFirstSpiTransferFails);
    RUN_TEST(test_CalibrationPass);
    return UNITY_END();
}
