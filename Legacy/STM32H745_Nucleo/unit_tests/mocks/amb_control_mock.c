/****************************************************************************//**
* @file      amb_control_mock.c
*
* @brief     This implements amb control mocks used in the testing.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "main.h"
#include "amb_control.h"

static uvpNum_t mockUvpNumber;
static ambMask_t mockSpiEnabled;
static ambMask_t mockAmbEnabled;
static ambMask_t mockPUState;
static ambMask_t mockUvpEnabled;
static bool mockTx;
static bool mockRx;


uint32_t GetUvpNumberFromMock(void)
{
  return mockUvpNumber;
}

uint32_t GetSpiEnabledFromMock(void)
{
  return mockSpiEnabled;
}

//mock
void AmbControlInit(void)
{
}

//mock
void AmbEnable(ambMask_t ambMask)
{
  mockAmbEnabled = ambMask;
}

//mock
cotaError_t AmbUvpEnable(ambMask_t ambMask)
{
  mockUvpEnabled = ambMask;
  return COTA_ERROR_NONE;
}

//mock
void AmbSpiEnable(ambMask_t ambMask)
{
  mockSpiEnabled = ambMask;
}

void AmbSetPUState(ambMask_t ambMask)
{
  mockPUState = ambMask;
}

cotaError_t AmbSelectUvp(uvpNum_t uvpNum)
{
  mockUvpNumber = uvpNum;
  return COTA_ERROR_NONE;
}

void AmbEnableTx(bool on)
{ 
  mockTx = on;
}
void AmbEnableRx(bool on)
{
  mockRx = on;
}

ambMask_t AmbGetEnabled(void)
{
  return mockAmbEnabled;
}

//mock
ambMask_t AmbGetUvpEnabled(void)
{
  return mockUvpEnabled;
}

//mock
ambMask_t AmbGetSpiEnabled(void)
{
  return mockSpiEnabled;
}

//mock
ambMask_t AmbGetPuState(void)
{
  return mockPUState;
}

//mock
ambMask_t AmbGetPhaseDetect(void)
{
  return 0;
}

//mock
uint8_t  AmbGetSelectedUvp(void)
{
  return mockUvpNumber;
}

//mock
bool AmbInRxMode(void)
{
  return mockRx;
}

//mock
bool AmbInTxMode(void)
{
  return mockTx;
}

//mock
SPI_HandleTypeDef* AmbGetSpiHandle(ambNum_t ambNum)
{
  return NULL;
}

//mock
SPI_HandleTypeDef* AmbGetMasterSpiHandle(void)
{
  return NULL;
}

