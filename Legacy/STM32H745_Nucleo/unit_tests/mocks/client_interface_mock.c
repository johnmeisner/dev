 /**
 * @file       client_interface_mock.c
 *
 * @brief      Mock interface for managing clients
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "client.h"
#include "client_interface_mock.h"
#include "client_manager.h"
#include "error.h"
#include "CotaCommonTypes.h"

/**
 * @brief Adds a new client to the clientManager. This version is only for unit test.
 * @param newClientId the longId of the client we want to add to the database. 
 * @param batteryLevel This is a Unit Test parameter. This is the battery level that will be assigned to the client
 * @param shortId This is a Unit Test parameter. An output paramater for the shortId that was assigned to the client
 * @return COTA_ERROR_NONE for success, COTA_ERROR_CLIENT_ALREADY_REGISTERED if client is already in the tree, 
 *           or COTA_ERROR_MAX_NUM_CLIENTS_STORED if memory is full
 */
cotaError_t CmAddClient(ExtdAddr_t newClientId, uint8_t batteryLevel, ShrtAddr_t *shortId)
{
    client_t tempClient;

    tempClient.longId = newClientId;
    tempClient.shortId = GetNewShortId();
    tempClient.batteryLevel = (batteryLevel & BATTERY_LEVEL_MASK); // Battery level is a 7 bit value
    tempClient.state = CLIENT_ERROR;
    tempClient.isValid = 1;

    // It's useful to return more data for unit test
    (*shortId) = tempClient.shortId;
    
    return InsertNode(tempClient, GetTreeHead());  
}

/**
 * @brief Gets whatever the maximum number of clients is in the clientManager is. This is useful in Unit Test.
 * @return MAX_NUM_CLIENTS
 */
int getMaxClients(void)
{
    return MAX_NUM_CLIENTS;
}

