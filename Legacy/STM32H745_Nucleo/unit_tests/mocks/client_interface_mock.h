/**
 * @file       client_interface_mock.h
 *
 * @brief      Header for mock client interface functions. 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CLIENT_INTERFACE_MOCK_H_
#define _CLIENT_INTERFACE_MOCK_H_

#include "error.h"
#include "CotaCommonTypes.h"
#include "client_manager.h"

cotaError_t CmAddClient(ExtdAddr_t newClientId, uint8_t batteryLevel, ShrtAddr_t *shortId);
int GetMaxClients(void);

#endif // #endif _CLIENT_INTERFACE_MOCK_H_
