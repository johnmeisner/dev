/****************************************************************************//**
* @file      spi_driver_mock.c
*
* @brief     This implements spi driver mocks used in the testing.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include <stdlib.h>
#include <time.h>
#include "unity.h"
#include "FreeRTOS.h"
#include "string.h"
#include "main.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "spi_driver.h"
#include "semphr.h"
#include "task.h"
#include "spi_driver_mock.h"
#include "amb_control_mock.h"

#define MAX_PHASE_OFFSET          512
#define PHASE_MASK                (MAX_PHASE_OFFSET - 1)
#define UVP_READ_MASK             0x80      //Indicates a UVP read is initiated to the UVP.
#define MAX_NUM_READ_PHASES       (3*UVPS_PER_AMB+4)  // 17 +17 + 17;
#define DWORD_TO_DATA(data, buf)  {buf[1] = (data & 0xff); buf[2] = ((data >> 8) & 0xff); buf[3] = ((data >> 16) & 0xff); buf[4] = ((data >> 24) & 0xff);}
#define DATA_TO_DWORD(buf)        (buf[1] | (buf[2] << 8) | (buf[3] << 16) | (buf[4] << 24))
#define BOOL_TO_NUM(aBool)        (aBool ? 1 : 0)

//This structure allows us to simulate a UVP.  This helps us test the calibration routine.
typedef struct _uvpInfo_t
{
  bool      txEnabled;                      //Reflects the current state of TX enabled
  uint32_t  numberOfTxs;                    //Tracks the number of times this UVP is put in Tx.
  uint32_t  numberOfTxPhaseUpdates;         //Tracks the number of times a phase update occurs in Tx
  bool      rxEnabled;                      //Reflects the current state of Rx enabled.
  uint32_t  numberOfRxs;                    //Tracks the number of time the UVP is put in Rx
  bool      puEnabled;                      //Reflects the current state of PU enabled
  bool      phaseConjugated;                //Reflects the current state of phaseConjugated.
  uint8_t   amuEnabled;                     //A mask indicating which AMU's are enabled.
  bool      txReady;                        //Indicates the Tx ready state of the UVP
  uint32_t  txReadyCount;                   //The number of txReady reads to wait of entering Tx until txReady become true.
  bool      puReady;                        //Indicates the Pu ready state.
  bool      puActive;                       //Indicates that the Phases update has started and the system is waiting to puReady
  uint32_t  puReadyCount;                   //Number of reads to wait until puReady is set to true.
  uint8_t   pageIndex;                      //The current pageIndex of register read/writes.
  uint16_t  readPhase[MAX_NUM_READ_PHASES]; //To simulate a read phase that occurs when puReady comes on during an Rx, we assigned random value.
  uint16_t  numReadPhases;                  //The number of phases that have been 'read'
  uint16_t  calPhase[AMUS_PER_UVP];         //The phase offset register values.
  uint32_t  rssiLowThresh[3];               //We need three values to keep track of the rssi low thresh value through calibration.
  uint8_t   arbPhaseSelect;                 //The value of the ARB_PHASE_SEL register
  uint16_t  phaseAmuClient1[4];             //The value of ARBIT_PHASE_AMU*_CLNT1 registers
  bool      highPower[4];                   //The value of the AMU*PAHIGHPOWER registers
  uint8_t   power[4];                       //The value of the AMU*PAPOWERMODE registers
} uvpInfo_t;

static cotaError_t gSpiTransferReturnCode = COTA_ERROR_NONE;  //A global indicating the return code of the spi transfer functions
static uint32_t gTxReadyPollCount = 1;                        //Number of reads to wait before letting txReady turn on
static uint32_t gPuReadyPollCount = 1;                        //Number of reads to wait before letting puReady turn on
static bool     gAllAmusSet       = false;                    //Flag indicating the all AMU's have been set at least once.
static uvpInfo_t allUvps[TOTAL_UVPS];                         //All the UVP information
static bool uvpSimulatorEnabled = false;                      //Flag indicated the UVP simulator has been initialized.

//An array of UVP registers that we attempt to simulate
static uvp_reg_t simulatedUvpRegs[64];

#define NUMBER_OF_SIMULATED_REGS (sizeof(simulatedUvpRegs)/sizeof(simulatedUvpRegs[0]))

static uint16_t getRandom16(void)
{
  uint16_t val1 = (rand() & 0xff);
  uint16_t val2 = (rand() & 0xff);
  return val1 | (val2 << 8);
}

static uint32_t getRandom32(void)
{
  uint32_t val1 = getRandom16();
  uint32_t val2 = getRandom16();
  return val1 | (val2 << 16);
}


void InitializeSpiSimulator(void)
{
  simulatedUvpRegs[0] = RSSI_LOWTHRESH;
  simulatedUvpRegs[1] = UVP_RXCTRL;
  simulatedUvpRegs[2] = UVP_TXCTRL;
  simulatedUvpRegs[3] = UVP_PUCTRL;
  simulatedUvpRegs[4] = SEL_PHASE_CONJUGATE;
  simulatedUvpRegs[5] = CLIENT_SELECT_OUT;
  simulatedUvpRegs[6] = DETECT_PH_CLIENT_ID_1;
  simulatedUvpRegs[7] = PHASE_OFFSET_AMU1;
  simulatedUvpRegs[8] = PHASE_OFFSET_AMU2;
  simulatedUvpRegs[9] = PHASE_OFFSET_AMU3;
  simulatedUvpRegs[10] = PHASE_OFFSET_AMU4;
  simulatedUvpRegs[11] = EN_AMU1;
  simulatedUvpRegs[12] = EN_AMU2; 
  simulatedUvpRegs[13] = EN_AMU3;
  simulatedUvpRegs[14] = EN_AMU4;
  simulatedUvpRegs[15] = SEND_POWER_RDY;
  simulatedUvpRegs[16] = PHASE_DETECT_DONE;
  simulatedUvpRegs[17] = ARB_PHASE_SEL;
  simulatedUvpRegs[18] = ARBIT_PHASE_AMU1_CLNT1;
  simulatedUvpRegs[19] = ARBIT_PHASE_AMU2_CLNT1;
  simulatedUvpRegs[20] = ARBIT_PHASE_AMU3_CLNT1;
  simulatedUvpRegs[21] = ARBIT_PHASE_AMU4_CLNT1;
  simulatedUvpRegs[22] = AMU1PAHIGHPOWER;
  simulatedUvpRegs[23] = AMU2PAHIGHPOWER;
  simulatedUvpRegs[24] = AMU3PAHIGHPOWER;
  simulatedUvpRegs[25] = AMU4PAHIGHPOWER;
  simulatedUvpRegs[26] = AMU1PAPOWERMODE;
  simulatedUvpRegs[27] = AMU2PAPOWERMODE;
  simulatedUvpRegs[28] = AMU3PAPOWERMODE;
  simulatedUvpRegs[29] = AMU4PAPOWERMODE; 
  simulatedUvpRegs[30] = OUTPHASE_CLNT1_AMU1;
  simulatedUvpRegs[31] = OUTPHASE_CLNT1_AMU2;
  simulatedUvpRegs[32] = OUTPHASE_CLNT1_AMU3;
  simulatedUvpRegs[33] = OUTPHASE_CLNT1_AMU4;
}

//Tests that only one SPI is enabled at a time,
//and this allows us to tell which AMB we are transfering data to.
static uint8_t getSpiEnabledNum(void)
{
  uint32_t mask = GetSpiEnabledFromMock();
  TEST_ASSERT_TRUE(IS_SINGLE_ITEM_SELECTED(mask));
  return MASK_TO_NUM(mask);
}

//The goal here is to test that at least one simulated register appears in this pageIndex and colAddr.
static bool findRegAtLeastOneReg( uvp_reg_t* reg, uint8_t pageIndex, uint16_t colAddr)
{
  uint32_t ii;
  bool ret = false;
  
  for (ii = 0; ii < NUMBER_OF_SIMULATED_REGS; ii++)
  {
    if ((simulatedUvpRegs[ii].pageIndex == pageIndex) && (simulatedUvpRegs[ii].colAddr == colAddr))
    {
      *reg = simulatedUvpRegs[ii];
      ret = true;
      break;
    }      
  }
  return ret;
}
 
//This is a way of comparing two registers to see if they are the same.
static bool isRegSame(uvp_reg_t reg1, uvp_reg_t reg2)
{
  return ((reg1.pageIndex == reg2.pageIndex) && (reg1.colAddr == reg2.colAddr) && (reg1.offset == reg2.offset) && (reg1.size == reg2.size));
}

//This totals the number of UVPs currently transmitoing.
static uint32_t CountUvpsInTransmit(void)
{
  uint32_t count = 0;
  uint32_t ii;
  
  for (ii = 0; ii < TOTAL_UVPS; ii++)
  {
    if (allUvps[ii].txEnabled)
    {
      count++;
    }
  }
  return count;
}

//Looks at the power levels of all UVP's and confirms they are all at the lowest level.
static bool UvpsAreAtLowestPowerLevel(void)
{
  bool ret = true; 
  uint32_t ii;
  uint32_t jj;
  
  for (ii = 0; ii < TOTAL_UVPS && ret; ii++)
  {
    for (jj = 0; jj < AMUS_PER_UVP && ret; jj++)
    {
      if (allUvps[ii].power[0] || allUvps[ii].highPower[0])
      {
        ret = false;
      }
    }
  }
  return ret;
  
  
}

//We need to make sure that during calibration the only AMU's active are those used
//by the reference and the beacon.
static void CheckCalRefAmuForAllUvps(void)
{
  uint32_t noAmus = 0;         //Number of UVP's with no Amu's enabled.
  uint32_t refAmus = 0;        //Number of UVP's with reference Amu enabled.
  uint32_t beaconAmus = 0;     //Number of UVP's with beacon Amu enabled.
  uint32_t beaconRefAmus = 0;  //Number of UVP's with the beacon Amu and the refence AMU enabled.
  uint32_t others = 0;
  uint32_t ii;
  
  for (ii = 0; ii < TOTAL_UVPS; ii++)
  {
    if (allUvps[ii].amuEnabled == 0)
    {
      noAmus++;
    }
    else if ((allUvps[ii].amuEnabled & NUM_TO_MASK(CAL_REF_AMU_NUM)) == NUM_TO_MASK(CAL_REF_AMU_NUM))
    {
      refAmus++;
    }
    else if ((allUvps[ii].amuEnabled & NUM_TO_MASK(CAL_BEACON_AMU_NUM)) == NUM_TO_MASK(CAL_BEACON_AMU_NUM))
    {
      beaconAmus++;
    }
    else if ((allUvps[ii].amuEnabled & (NUM_TO_MASK(CAL_BEACON_AMU_NUM) | NUM_TO_MASK(CAL_REF_AMU_NUM))) == (NUM_TO_MASK(CAL_BEACON_AMU_NUM)  | NUM_TO_MASK(CAL_REF_AMU_NUM)))
    {
      beaconRefAmus++;
    }
    else
    {
      others++;
    }
  }
  if (others == 0)  //If we are getting others we are likely beginning the process of turning on all AMU's at the end of calibration.  No need to do these checks.
  {
    if (gAllAmusSet) //We need to had until all the UVP's are initialy set before examing these numbers.
    {
      TEST_ASSERT_TRUE(beaconRefAmus < 2);   //The can be no more than one uvp in this state, this a UVP transition to or from the beacon state.
      TEST_ASSERT_TRUE(noAmus < 2);          //You can't have any more than one of these.  Again a UVP transitions to or from the beacon state.
      TEST_ASSERT_TRUE(beaconRefAmus + noAmus < 2);   //The can be no more than one uvp in this state, this a UVP transition to or from the beacon state.
      TEST_ASSERT_TRUE(refAmus >= TOTAL_UVPS - 1);
    }
    else
    {
      if (refAmus == TOTAL_UVPS)
      {
        gAllAmusSet = true;               //Once we set all the UVP's to use the refence AMU's, we can start checking AMU state for throughly.
      }
    }
  }
  
}

//When reading a register, sometimes we need to report values, or take action.  This is where 
//we simulate UVP register reads.
static void ReadAllRegistersInThis32ByteSectionAndPutThemInTheRightPlaceInRxData(uint8_t* pRxData, uint8_t pageIndex, uint16_t colAddr)
{
  uint32_t ii;
  uint32_t dataPage = 0;
  uint32_t value;
  uint32_t mask;
  uint32_t uvpNum = GetUvpNumberFromMock();
  uint32_t ambNum = getSpiEnabledNum();
  uvpInfo_t* curUvp = &allUvps[UVP_INDEX(ambNum, uvpNum)];

  for (ii = 0; ii < NUMBER_OF_SIMULATED_REGS; ii++)
  {
    if ((simulatedUvpRegs[ii].pageIndex == pageIndex) && (simulatedUvpRegs[ii].colAddr == colAddr))
    {
      value = 0;

      if (isRegSame(simulatedUvpRegs[ii], RSSI_LOWTHRESH))
      {        
        value = curUvp->rssiLowThresh[0];
      }
      else if (isRegSame(simulatedUvpRegs[ii], UVP_RXCTRL)) 
      {
        value = BOOL_TO_NUM(curUvp->rxEnabled);
      }
      else if (isRegSame(simulatedUvpRegs[ii],UVP_TXCTRL))
      {
        value = BOOL_TO_NUM(curUvp->txEnabled);
      }
      else if (isRegSame(simulatedUvpRegs[ii],UVP_PUCTRL))
      {
        value = BOOL_TO_NUM(curUvp->puEnabled);
      }
      else if (isRegSame(simulatedUvpRegs[ii],SEL_PHASE_CONJUGATE))
      {
        value = BOOL_TO_NUM(curUvp->phaseConjugated);
      }
      else if (isRegSame(simulatedUvpRegs[ii],CLIENT_SELECT_OUT))
      {
        value = 0;
      }
      else if (isRegSame(simulatedUvpRegs[ii],DETECT_PH_CLIENT_ID_1))
      {
        value = 0;
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_OFFSET_AMU1))
      {
        value = curUvp->calPhase[0];
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_OFFSET_AMU2))
      {
        value = curUvp->calPhase[1];
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_OFFSET_AMU3))
      {
        value = curUvp->calPhase[2];
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_OFFSET_AMU4))
      {
        value = curUvp->calPhase[3]; 
      }
      else if (isRegSame(simulatedUvpRegs[ii],EN_AMU1))
      {
        value = BOOL_TO_NUM(curUvp->amuEnabled & NUM_TO_MASK(AMU0));
      }
      else if (isRegSame(simulatedUvpRegs[ii],EN_AMU2)) 
      {
        value = BOOL_TO_NUM(curUvp->amuEnabled & NUM_TO_MASK(AMU1));
      }
      else if (isRegSame(simulatedUvpRegs[ii],EN_AMU3))
      {
        value = BOOL_TO_NUM(curUvp->amuEnabled & NUM_TO_MASK(AMU2));
      }
      else if (isRegSame(simulatedUvpRegs[ii],EN_AMU4))
      {
        value = BOOL_TO_NUM(curUvp->amuEnabled & NUM_TO_MASK(AMU3));
      }
      else if (isRegSame(simulatedUvpRegs[ii],SEND_POWER_RDY))
      {
        if ((!curUvp->txReady) && curUvp->txEnabled)  //If Tx is enabled, we will wait to set Tx Ready until a certain of number of reads have occured.
        {
          if (curUvp->txReadyCount) curUvp->txReadyCount--;
          if (curUvp->txReadyCount == 0) curUvp->txReady = true;            
        }
        value = BOOL_TO_NUM(curUvp->txReady);
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_DETECT_DONE))
      {
        if (curUvp->puActive && (!curUvp->puReady))  //We must wait a few reads before allowing puReady to become true.
        {
          if (curUvp->puReadyCount) curUvp->puReadyCount--;
          if (curUvp->puReadyCount == 0)
          {
            curUvp->puReady = true;                 //When puReady becomes true, it's time to simulate a phase read, with a random number.
            curUvp->puActive = false;
            curUvp->readPhase[curUvp->numReadPhases]= (getRandom16() & PHASE_MASK);  //We need to keep track of phase reads, so we can check calibration values.
            curUvp->numReadPhases++;
            TEST_ASSERT_NOT_EQUAL(MAX_NUM_READ_PHASES, curUvp->numReadPhases);
          }
        }
        value = (curUvp->puReady);
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARB_PHASE_SEL))
      {
        value = curUvp->arbPhaseSelect;
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARBIT_PHASE_AMU1_CLNT1))
      {
        value = curUvp->phaseAmuClient1[0];
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARBIT_PHASE_AMU2_CLNT1))
      {
        value = curUvp->phaseAmuClient1[1];
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARBIT_PHASE_AMU3_CLNT1))
      {
        value = curUvp->phaseAmuClient1[2];
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARBIT_PHASE_AMU4_CLNT1))
      {
        value = curUvp->phaseAmuClient1[3];
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU1PAHIGHPOWER))
      {
        value = BOOL_TO_NUM(curUvp->highPower[0]);
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU2PAHIGHPOWER))
      {
        value = BOOL_TO_NUM(curUvp->highPower[1]);
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU3PAHIGHPOWER))
      {
        value = BOOL_TO_NUM(curUvp->highPower[2]);
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU4PAHIGHPOWER))
      {
        value = BOOL_TO_NUM(curUvp->highPower[3]);
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU1PAPOWERMODE))
      {
        value = curUvp->power[0];
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU2PAPOWERMODE))
      {
        value = curUvp->power[1];
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU3PAPOWERMODE))
      {
        value = curUvp->power[2];
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU4PAPOWERMODE))
      {
        value = curUvp->power[3];
      }
      else if (isRegSame(simulatedUvpRegs[ii], OUTPHASE_CLNT1_AMU1))
      {
        //Reads the last phase read.
        value = 0;
        if (curUvp->numReadPhases)
        {
          value = curUvp->readPhase[curUvp->numReadPhases-1];
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii], OUTPHASE_CLNT1_AMU2))
      {
        //Reads the last phase read.
        value = 0;
        if (curUvp->numReadPhases)
        {
          value = curUvp->readPhase[curUvp->numReadPhases-1];
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii], OUTPHASE_CLNT1_AMU3))
      {
        //Reads the last phase read.
        value = 0;
        if (curUvp->numReadPhases)
        {
          value = curUvp->readPhase[curUvp->numReadPhases-1];          
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii], OUTPHASE_CLNT1_AMU4))
      {
        //Reads the last phase read.
        value = 0;
        if (curUvp->numReadPhases)
        {
          value = curUvp->readPhase[curUvp->numReadPhases-1];
        }
      }
      
      mask = GEN_SOLID_MASK(simulatedUvpRegs[ii].size);
      
      value &= mask;
      
      dataPage |= (value << simulatedUvpRegs[ii].offset);     
    }      
  } 
  
  DWORD_TO_DATA(dataPage, pRxData);
  
}

//Here we simulate a register write.
static void WriteAndTakeActionIfRegistersInThis32BytesHaveChange(uint8_t* pTxData, uint8_t pageIndex, uint16_t colAddr)
{
  uint32_t dataPage = DATA_TO_DWORD(pTxData);
  uint32_t value;
  uint32_t mask;
  uint32_t ii;
  uint32_t uvpNum = GetUvpNumberFromMock();
  uint32_t ambNum = getSpiEnabledNum();
  uvpInfo_t* curUvp = &allUvps[UVP_INDEX(ambNum, uvpNum)];
    
  for (ii = 0; ii < NUMBER_OF_SIMULATED_REGS; ii++)
  {
    if ((simulatedUvpRegs[ii].pageIndex == pageIndex) && (simulatedUvpRegs[ii].colAddr == colAddr))
    {
      mask = GEN_SOLID_MASK(simulatedUvpRegs[ii].size);
      
      value = ((dataPage >> simulatedUvpRegs[ii].offset) & mask);
            
      if (isRegSame(simulatedUvpRegs[ii], RSSI_LOWTHRESH))  
      {
        // We are fortunate that this register does not share a 32 bit dword with other simulated regs. So we can always set it
        curUvp->rssiLowThresh[2] = curUvp->rssiLowThresh[1];
        curUvp->rssiLowThresh[1] = curUvp->rssiLowThresh[0];
        curUvp->rssiLowThresh[0] = value;
      }
      else if (isRegSame(simulatedUvpRegs[ii], UVP_RXCTRL)) 
      {
        if (value != BOOL_TO_NUM(curUvp->rxEnabled))
        {
          curUvp->rxEnabled = (value == 1);
          
          if (curUvp->rxEnabled)
          {
            //If we enter the RX, make sure Tx and Pu are off, and increment the Rx tracking counter.
            TEST_ASSERT_FALSE(curUvp->txEnabled);
            TEST_ASSERT_FALSE(curUvp->puEnabled);
            curUvp->numberOfRxs++;
          } 
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii],UVP_TXCTRL))
      {
        if (value != BOOL_TO_NUM(curUvp->txEnabled))
        {
          curUvp->txEnabled = (value == 1);
          if (curUvp->txEnabled)
          {
            //On entering Tx, check the Tx state of all UVP's.
            //Set the txReadPollCount so Tx ready will come on after a few reads.
            TEST_ASSERT_TRUE(CountUvpsInTransmit() == 1);
            TEST_ASSERT_FALSE(curUvp->rxEnabled);
            TEST_ASSERT_FALSE(curUvp->puEnabled);
            TEST_ASSERT_FALSE(curUvp->txReady);
            TEST_ASSERT_EQUAL_UINT32(curUvp->txReadyCount, 0);
            curUvp->numberOfTxs++;

            curUvp->txReadyCount = gTxReadyPollCount;
          }
          else
          {
            curUvp->txReady = false; //on exiting Tx, turn of the tx ready
          }
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii],UVP_PUCTRL))
      {     
        if (value != BOOL_TO_NUM(curUvp->puEnabled))
        {
          curUvp->puEnabled = (value == 1);
          //Check that this occurs when we are in Tx or Rx
          TEST_ASSERT_TRUE(curUvp->txEnabled || curUvp->rxEnabled);
          if (!curUvp->puEnabled)  //We take action when we tranistion from 1 to 0.
          {
            if (curUvp->rxEnabled)
            {
              //Set the Pu ready counter if phase update occurs in Rx mode.
              curUvp->puReady = false;
              curUvp->puActive = true;
              curUvp->puReadyCount = gPuReadyPollCount;
            }
            if (curUvp->txEnabled)
            {
              curUvp->numberOfTxPhaseUpdates++;
            }
          }
        }        
      }
      else if (isRegSame(simulatedUvpRegs[ii],SEL_PHASE_CONJUGATE))
      {
        if (value != BOOL_TO_NUM(curUvp->phaseConjugated))
        {
          curUvp->phaseConjugated = (value == 1);
        }          
      }
      else if (isRegSame(simulatedUvpRegs[ii],CLIENT_SELECT_OUT))
      {
        TEST_ASSERT_EQUAL_UINT32(value,0);
      }
      else if (isRegSame(simulatedUvpRegs[ii],DETECT_PH_CLIENT_ID_1))
      {
        TEST_ASSERT_EQUAL_UINT32(value,0);
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_OFFSET_AMU1))
      {
        curUvp->calPhase[0] = value;
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_OFFSET_AMU2))
      {
        curUvp->calPhase[1] = value;
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_OFFSET_AMU3))
      {
        curUvp->calPhase[2] = value;
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_OFFSET_AMU4))
      { 
        curUvp->calPhase[3] = value;
      }
      else if (isRegSame(simulatedUvpRegs[ii],EN_AMU1))
      {
        if (value)
        {
          curUvp->amuEnabled |= NUM_TO_MASK(AMU0);
        }
        else
        {
          curUvp->amuEnabled &= (~NUM_TO_MASK(AMU0));
        }
        CheckCalRefAmuForAllUvps();
      }
      else if (isRegSame(simulatedUvpRegs[ii],EN_AMU2)) 
      {
        if (value)
        {
          curUvp->amuEnabled |= NUM_TO_MASK(AMU1);
        }
        else
        {
          curUvp->amuEnabled &= (~NUM_TO_MASK(AMU1));
        }
        CheckCalRefAmuForAllUvps();
      }
      else if (isRegSame(simulatedUvpRegs[ii],EN_AMU3))
      {
        if (value)
        {
          curUvp->amuEnabled |= NUM_TO_MASK(AMU2);
        }
        else
        {
          curUvp->amuEnabled &= (~NUM_TO_MASK(AMU2));
        }
        CheckCalRefAmuForAllUvps();
      }
      else if (isRegSame(simulatedUvpRegs[ii],EN_AMU4))
      {
        if (value)
        {
          curUvp->amuEnabled |= NUM_TO_MASK(AMU3);
        }
        else
        {
          curUvp->amuEnabled &= (~NUM_TO_MASK(AMU3));
        }
        CheckCalRefAmuForAllUvps();
      }
      else if (isRegSame(simulatedUvpRegs[ii],SEND_POWER_RDY))
      {
        //ignore, read only
      }
      else if (isRegSame(simulatedUvpRegs[ii],PHASE_DETECT_DONE))
      {
        //ignore, read only
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARB_PHASE_SEL))
      {
        //The ARB_PHASE_SEL must be 0 or have the lowest bit set.
        TEST_ASSERT_TRUE((value == 0) || (value & 0x1));
        if (curUvp->arbPhaseSelect != value)
        {
          curUvp->arbPhaseSelect = value;
          if (value & 1)
          {
            TEST_ASSERT_TRUE(UvpsAreAtLowestPowerLevel());  //Here we know the power levels have been set, let's check them
          }
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARBIT_PHASE_AMU1_CLNT1))
      {
        if (curUvp->phaseAmuClient1[0] != value)
        {
          curUvp->phaseAmuClient1[0] = value;
          TEST_ASSERT_TRUE((curUvp->arbPhaseSelect & 0x1));
          CheckCalRefAmuForAllUvps();
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARBIT_PHASE_AMU2_CLNT1))
      {
        if (curUvp->phaseAmuClient1[1] != value)
        {
          curUvp->phaseAmuClient1[1] = value;
          TEST_ASSERT_TRUE((curUvp->arbPhaseSelect & 0x1));
          CheckCalRefAmuForAllUvps();
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARBIT_PHASE_AMU3_CLNT1))
      {
        if (curUvp->phaseAmuClient1[2] != value)
        {
          curUvp->phaseAmuClient1[2] = value;
          TEST_ASSERT_TRUE((curUvp->arbPhaseSelect & 0x1));
          CheckCalRefAmuForAllUvps();
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii], ARBIT_PHASE_AMU4_CLNT1))
      {
        if (curUvp->phaseAmuClient1[3] != value)
        {
          curUvp->phaseAmuClient1[3] = value;
          TEST_ASSERT_TRUE((curUvp->arbPhaseSelect & 0x1));
          CheckCalRefAmuForAllUvps();
        }
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU1PAHIGHPOWER))
      {
        curUvp->highPower[0] = (value == 1);
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU2PAHIGHPOWER))
      {
        curUvp->highPower[1] = (value == 1);
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU3PAHIGHPOWER))
      {
        curUvp->highPower[2] = (value == 1);
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU4PAHIGHPOWER))
      {
        curUvp->highPower[3] = (value == 1);
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU1PAPOWERMODE))
      {
        curUvp->power[0] = value;
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU2PAPOWERMODE))
      {
        curUvp->power[1] = value;
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU3PAPOWERMODE))
      {
        curUvp->power[2] = value;
      }
      else if (isRegSame(simulatedUvpRegs[ii], AMU4PAPOWERMODE))
      {
        curUvp->power[3] = value;
      } 
    }      
  } 
  
  DWORD_TO_DATA(dataPage, pTxData);
  
}

//This is called by SPI mocks to run the UVP simulator
cotaError_t simulateUvpTransferForCalibration(uint8_t *pTxData, uint8_t *pRxData, uint16_t size)
{
  uint32_t uvpNum = GetUvpNumberFromMock();
  uint32_t ambNum = getSpiEnabledNum();
  bool     isRead = false;
  uvp_reg_t reg;
  TEST_ASSERT_TRUE(UVP_INDEX(ambNum,uvpNum) < TOTAL_UVPS);
  if (size == 2)
  {
    //Here, we are writing a page index.  Make sure we set the page Index and test first byte of the transfer
    TEST_ASSERT_EQUAL_HEX8(0x7F, pTxData[0]);
    allUvps[UVP_INDEX(ambNum,uvpNum)].pageIndex = pTxData[1];    
  }
  else if (size == 5)
  {
    //Determine if this is write or a read.
    if (pTxData[0] & UVP_READ_MASK)
    {
      isRead = true;
      pTxData[0] &= (~UVP_READ_MASK);
    }
    
    //Test there is at least one simulated register in this 32 bit data.
    TEST_ASSERT_TRUE(findRegAtLeastOneReg(&reg, allUvps[UVP_INDEX(ambNum,uvpNum)].pageIndex, pTxData[0]));
    
    if (isRead)
    {
      ReadAllRegistersInThis32ByteSectionAndPutThemInTheRightPlaceInRxData(pRxData,allUvps[UVP_INDEX(ambNum,uvpNum)].pageIndex, pTxData[0]);     
    }
    else
    {
      WriteAndTakeActionIfRegistersInThis32BytesHaveChange(pTxData, allUvps[UVP_INDEX(ambNum,uvpNum)].pageIndex, pTxData[0]);
    }    
  }
  else
  {
    TEST_ASSERT_TRUE(false);  //This should never happen.
  }
  
  return COTA_ERROR_NONE;
}

//mock
void InitSpiSemaphore(void)
{
}

//mock
cotaError_t SpiTransfer(SPI_HandleTypeDef *hSpi, uint8_t *pTxData, uint8_t *pRxData, uint16_t size)
{
  cotaError_t ret = gSpiTransferReturnCode;
  if (uvpSimulatorEnabled)
  {
    ret = simulateUvpTransferForCalibration(pTxData, pRxData,  size);
  }

  return ret;
}

//mock
cotaError_t SpiSimulTransfer(uint8_t* pTxData, uint8_t *pRxData, uint16_t size, ambMask_t ambMask)
{
  return COTA_ERROR_NONE;
}

//Allows the SpiTransfer return code to be set.
void SetSpiTransferReturnValue(cotaError_t errVal)
{
  gSpiTransferReturnCode = errVal;
}

void InitUvpSimulatorForCalibrationTest(void)
{
  uint32_t ii;
  int stime;
  long ltime;
  
  ltime = time(NULL);
  stime = (unsigned) ltime/2;
  srand(stime);
  uvpSimulatorEnabled = true; 
  for (ii = 0; ii < TOTAL_UVPS; ii++)
  {
    memset(allUvps, 0, sizeof(uvpInfo_t));
    //Initialize rssi low thresh value to random numbers, to check that calibration restores them at the end.
    allUvps[ii].rssiLowThresh[0] = getRandom32();
  }

}

//Tears down the UVP simulator.
void TearDownUvpSimulator(void)
{
  uvpSimulatorEnabled = false;
}

//After calibration all AMU's should be enabled.
static bool checkAllAmusOn(void)
{
  bool ret = true;
  uint32_t ii;
  
  for (ii = 0; ii < TOTAL_UVPS; ii++)
  { 
    if ((allUvps[ii].amuEnabled & ALL_AMU_MASK) != ALL_AMU_MASK)
    {
      ret = false;
      break;
    }
  }
  
  return ret;
}

//This will count the reference UVP's. 
//It will have exactly four phase reads.
//There should be only one.
static bool getReferenceIndex(uint32_t* refIndex)
{
  uint32_t count = 0;
  uint32_t ii;
 
  for (ii = 0; ii < TOTAL_UVPS; ii++)
  { 
    if (allUvps[ii].numberOfRxs == MAX_NUM_AMB)
    {
      count++;
      *refIndex = ii;
      TEST_ASSERT_EQUAL_UINT32(MAX_NUM_AMB, allUvps[ii].numberOfTxs);
    }
  }
 
  return (count == 1);
}

//Get the index in the allUvp array of the beacons, which contain the phase data we need to check the calibration values against.
static bool getBeaconIndexesAndCheckThem(uint32_t*  beaconIndexes)
{
  uint32_t count = 0;
  uint32_t ii;
  uint32_t totalBeaconReads = 0;
 
  for (ii = 0; ii < MAX_NUM_AMB; ii++)
  {
    beaconIndexes[ii] = 0xffffffff;
  }
  
  for (ii = 0; ii < TOTAL_UVPS; ii++)
  { 
    if ((allUvps[ii].numReadPhases == UVPS_PER_AMB + 1) ||  //This served as the beacon for calibrating the reference AMB
       (allUvps[ii].numReadPhases == (UVPS_PER_AMB +2 )) || //This served as a the beacon for an AMB without the reference antenna
       (allUvps[ii].numReadPhases == (2*UVPS_PER_AMB+2)) || //This served as a beacon for two AMB's, one of which contained the reference
       (allUvps[ii].numReadPhases == (2*UVPS_PER_AMB+3)) || //This served as a beacon for two AMB's, neither contained the reference
       (allUvps[ii].numReadPhases == (3*UVPS_PER_AMB+3)) || //This served as a beacon for three AMB's, one of which contained the reference
       (allUvps[ii].numReadPhases == (3*UVPS_PER_AMB+4)))   //This served as a beacon for three AMB's, neither contained the reference
    {
      TEST_ASSERT_NOT_EQUAL(MAX_NUM_AMB, count);
      beaconIndexes[count] = ii;
      count++;
      totalBeaconReads += allUvps[ii].numReadPhases;
      
      //The beacon transmit history, should be one for every time it served, plus once we it was calibrated
      if ((allUvps[ii].numberOfRxs == UVPS_PER_AMB+1) ||
       (allUvps[ii].numberOfRxs == (UVPS_PER_AMB +2)))
      {
        TEST_ASSERT_EQUAL_UINT32(2, allUvps[ii].numberOfTxs);
        TEST_ASSERT_EQUAL_UINT32(2, allUvps[ii].numberOfRxs);
      }
      else if ((allUvps[ii].numberOfRxs == (2*UVPS_PER_AMB+2)) ||
       (allUvps[ii].numberOfRxs == ((2*UVPS_PER_AMB+3))))
      {
        TEST_ASSERT_EQUAL_UINT32(3, allUvps[ii].numberOfTxs);
        TEST_ASSERT_EQUAL_UINT32(3, allUvps[ii].numberOfRxs);
      }
      else if ((allUvps[ii].numberOfRxs == (3*UVPS_PER_AMB+3)) ||
       (allUvps[ii].numberOfRxs == ((3*UVPS_PER_AMB+4))))
      {
        TEST_ASSERT_EQUAL_UINT32(4, allUvps[ii].numberOfTxs);
        TEST_ASSERT_EQUAL_UINT32(4, allUvps[ii].numberOfRxs);
      }
    }
  }
  
  //The total number of beacon reads is totaled as follows:
  //3 AMB's will contribute 17 beacon reads, 
  //1 AMB will contribute only 16 beacons since it contains the reference
  //Each beacon will need to read a phase again when it is calibrated.
  TEST_ASSERT_EQUAL_UINT32((4*UVPS_PER_AMB+3+count),totalBeaconReads);
  
  return ((count > 1) && (count <= MAX_NUM_AMB));
}

//Each UVP was assigned a random RSSI threshold at the beginning.
//The calibration should read those values, set them to zero, then
//restore after it's done.
void checkRSSIThreshHistory(void)
{
  uint32_t ii;
  uint32_t mask = GEN_SOLID_MASK(RSSI_LOWTHRESH.size);
  uint32_t offset = RSSI_LOWTHRESH.offset;
  uint32_t origValue;
  for (ii = 0; ii < TOTAL_UVPS; ii++)
  {  
    TEST_ASSERT_EQUAL_UINT32(0, allUvps[ii].rssiLowThresh[1]);
    origValue = (allUvps[ii].rssiLowThresh[2] >> offset) & mask;
    TEST_ASSERT_EQUAL_UINT32(origValue, allUvps[ii].rssiLowThresh[0]);   
  }
}

//compare an section of beacon phase data to see if it matches an array of calibration phases.
//The first element in refAndData will be the phase when the reference was in Tx read by the beacon.
//The next 15 or 16 values, are the phases read by the beacon when the AUT's were in Tx.
static bool checkCalPhases(uint16_t* refAndData, uint16_t* calPh, uint32_t numElems)
{
  bool alike = true;
  uint32_t ii;

  for (ii = 0; ii < numElems; ii++)
  {
    int diff = refAndData[0] - refAndData[ii+1]; //This is the calibration phase.
    if (diff < 0)
    {
      diff += MAX_PHASE_OFFSET;
    }

    if ((((uint16_t)diff)&PHASE_MASK) != calPh[ii])
    {
      alike = false;
    }    
  }
  
  return alike;
}

//There is a problem.  We are not sure exactly whick phases in the beacon phase data were used to generate a set
//of calibration for an AMB.  There are numerous possibilities, so have check many places, depending on the data 
//in the beacon.
bool isCalPhaseDataInThisBeacon(uint32_t beaconIndex, uint16_t* calPhases, uint32_t numPhases, uint32_t* foundIndex)
{
  bool refOnOneOfTheseBoards = false;
  bool ret = false;
  
  if (numPhases == (UVPS_PER_AMB-1))
  {
      refOnOneOfTheseBoards = true;
  }
  else
  {
    TEST_ASSERT_EQUAL_UINT32(UVPS_PER_AMB, numPhases);  //If the numPhases is not 15, it must 16.
  }    
  //Regardless of the type beacon, the data can always potentially be at the start
  //or the one off the start.
  ret = checkCalPhases(&allUvps[beaconIndex].readPhase[0], calPhases, numPhases);
  if (ret)
  {
    *foundIndex = 0;
  }
  else
  {
    ret = checkCalPhases(&allUvps[beaconIndex].readPhase[1], calPhases, numPhases);
    if (ret)
    {
      *foundIndex = 1;
    }
  }

  //If a beacon contains two sets of AMB phase data, there are several more location we can check.
  if ((allUvps[beaconIndex].numberOfRxs >= (2*UVPS_PER_AMB+2)) && (!ret))
  {
    //possibilities here for 2 sets of cal data and the single reading
    //|ref+15uvps  |ref & 16uvps|Single      |  index 0,  16 
    //|ref+15uvps  |Single      |ref & 16uvps|  index 0,  17
    //|Single      |ref & 15uvps|ref & 16uvps|  index 1,  17
    //|Single      |ref & 16uvps|ref & 15uvps|  index 17, 18
    //|ref & 16uvps|Single      |ref & 15uvps|  index 0,  18 
    //|ref & 16uvps|ref & 15uvps|Single      |  index 0,  17
    //|ref+16uvps  |ref & 16uvps|Single      |  index 0,  17
    //|ref+16uvps  |Single      |ref & 16uvps|  index 0,  18
    //|Single      |ref & 16uvps|ref & 16uvps|  index 1,  18

    ret = checkCalPhases(&allUvps[beaconIndex].readPhase[UVPS_PER_AMB], calPhases, numPhases);
    if (ret)
    {
      *foundIndex = UVPS_PER_AMB;
    }
    else
    {
      ret = checkCalPhases(&allUvps[beaconIndex].readPhase[UVPS_PER_AMB+1], calPhases, numPhases);
      if (ret)
      {
        *foundIndex = UVPS_PER_AMB+1;
      }
      else
      {
        ret = checkCalPhases(&allUvps[beaconIndex].readPhase[UVPS_PER_AMB+2], calPhases, numPhases);
        if (ret)
        {
          *foundIndex = UVPS_PER_AMB+2;
        }        
      }
    }
  }
  //If a beacon contains three sets of AMB phase data, there are several more location we can check.
  if ((allUvps[beaconIndex].numberOfRxs >= (3*UVPS_PER_AMB+3)) && refOnOneOfTheseBoards)
  {
    //possibilities here for 2 sets of cal data and the single reading
    //|ref+15uvps  |ref & 16uvps|ref & 16uvps|Single      |  index 0, 16, 33
    //|ref+15uvps  |ref & 16uvps|Single      |ref & 16uvps|  index 0, 16, 34
    //|ref+15uvps  |Single      |ref & 16uvps|ref & 16uvps|  index 0, 17, 34
    //|ref & 16uvps|Single      |ref & 15uvps|ref & 16uvps|  index 0, 18, 34
    //|ref & 16uvps|Single      |ref & 16uvps|ref & 15uvps|  index 0, 18, 35
    //|ref & 16uvps|ref & 15uvps|Single      |ref & 16uvps|  index 0, 18, 34
    //|ref & 16uvps|ref & 16uvps|Single      |ref & 15uvps|  index 0, 18, 35
    //|ref & 16uvps|ref & 15uvps|ref & 16uvps|Single      |  index 0, 18, 33
    //|ref & 16uvps|ref & 16uvps|ref & 15uvps|Single      |  index 0, 18, 34
    //|Single      |ref & 15uvps|ref & 16uvps|ref & 16uvps|  index 1, 17, 34
    //|Single      |ref & 16uvps|ref & 15uvps|ref & 16uvps|  index 1, 18, 34
    //|Single      |ref & 16uvps|ref & 16uvps|ref & 15uvps|  index 1, 18, 35
    //|Single      |ref & 16uvps|ref & 16uvps|ref & 16uvps|  index 1, 18, 35   
    //|ref & 16uvps|Single      |ref & 16uvps|ref & 16uvps|  index 0, 18, 35   
    //|ref & 16uvps|ref & 16uvps|Single      |ref & 16uvps|  index 0, 17, 35   
    //|ref & 16uvps|ref & 16uvps|ref & 16uvps|Single      |  index 0, 17, 34 

    ret = checkCalPhases(&allUvps[beaconIndex].readPhase[2*UVPS_PER_AMB+1], calPhases, numPhases);
    if (ret)
    {
      *foundIndex = 2*UVPS_PER_AMB+1;
    }
    else
    {
      ret = checkCalPhases(&allUvps[beaconIndex].readPhase[2*UVPS_PER_AMB+2], calPhases, numPhases);
      if (ret)
      {
        *foundIndex = 2*UVPS_PER_AMB+2;
      }
      else
      {
        ret = checkCalPhases(&allUvps[beaconIndex].readPhase[2*UVPS_PER_AMB+3], calPhases, numPhases);
        if (ret)
        {
          *foundIndex = 2*UVPS_PER_AMB+3;
        }        
      }
    }    
  }    
  
  return ret;
}

//We want to make sure that phases written the phase offset register correspond to phase data
//stored in the beacons.  Unforunately, we don't know where exactly that data is in the beacons,
//but we can't hunt for it.
static void checkCalibrationPhases(uint32_t refIndex, uint32_t* beaconIndexes)
{
  //This is difficult problem because we don't which beacon has the phase data for each UVP.
  //But we do know how the phases are organized in a beacon's simulated UVP structure.
  uint32_t ambNum;
  uint32_t uvpNum;
  uint32_t ii;
  uint32_t jj;
  uint32_t packCounter;
  uint16_t calibratedPhaseTestArray[UVPS_PER_AMB];
  bool     foundData;
  uint32_t foundIndexes[MAX_NUM_AMB];
  uint32_t foundBeaconIndexes[MAX_NUM_AMB];
  
  //Let's first test that each calibration phase for each AMU on a single UVP are identical.
  for (ii = 0; ii < TOTAL_UVPS; ii++)
  {
    for (jj = 1; jj < AMUS_PER_UVP; jj++)
    {
      TEST_ASSERT_EQUAL_UINT16(allUvps[ii].calPhase[0], allUvps[ii].calPhase[jj]);
    }
  }

  //Test that calibration phase for the refernce is 0.
  TEST_ASSERT_EQUAL_UINT16(0, allUvps[refIndex].calPhase[0]);
  
  //Let's get phase data each AMB, and go hunting for it in the beacons.
  for (ambNum = 0; ambNum < MAX_NUM_AMB; ambNum++)
  {
    packCounter = 0;
    //Let's fill a test array with calibrated phases to make easier to compare with phases read by beacon.
    for (uvpNum = 0; uvpNum < UVPS_PER_AMB; uvpNum++)
    {
      if (refIndex != UVP_INDEX(ambNum,uvpNum))
      {
        calibratedPhaseTestArray[packCounter++] = allUvps[UVP_INDEX(ambNum, uvpNum)].calPhase[0];
      }
    }
    //Here's the difficult part.  If done right, there is a 16 byte or 17 byte section in one of
    //the beacon uvp's that contains data like ref phase + 15/16 aut phases, where the calphase = ref -  read phase.
    //We now must find them now.
    foundData = false;
    //Interate through each beacon.
    for (jj = 0; jj < MAX_NUM_AMB; jj++)
    {
      if (beaconIndexes[jj] == 0xffffffff)
      {
        foundIndexes[jj] = 0xffffffff;
        foundBeaconIndexes[jj] = 0xffffffff;
        continue;
      }
      //Try to found the data, if found note which beacon and which position in the data it was found at.
      foundData = isCalPhaseDataInThisBeacon(beaconIndexes[jj], calibratedPhaseTestArray, packCounter, &foundIndexes[jj]);
      if (foundData)
      {
        foundBeaconIndexes[jj] = jj;
        break;    
      }        
    }
    
    TEST_ASSERT_TRUE(foundData);
  }
  
  //At this point, we able to confirm the calibration phases can be reconstructed with the phases read by the beacons.
  //However, each AMB's calibration must be found in a different location.  Let's confirm that now.
  for (ii = 0; ii < MAX_NUM_AMB; ii++)
  {
    if (foundBeaconIndexes[ii] == 0xffffffff)
    {
      continue;
    }
    for (jj == ii+1; jj << MAX_NUM_AMB; jj++)
    {
      if (foundIndexes[jj] == 0xffffffff)
      {
        continue;
      }
      TEST_ASSERT_TRUE((foundBeaconIndexes[ii] != foundBeaconIndexes[ii]) && (foundIndexes[jj] != foundIndexes[jj]));       
    }
  }
  
  //Finished
}

//We need to see if the UVP's are in a good state after the calibration.
bool CalibrationSimulatorInCorrectState(void)
{
  uint32_t refIndex;
  uint32_t beaconIndexes[MAX_NUM_AMB];
  TEST_ASSERT_TRUE(checkAllAmusOn());
  TEST_ASSERT_TRUE(getReferenceIndex(&refIndex));
  TEST_ASSERT_TRUE(getBeaconIndexesAndCheckThem(beaconIndexes));
  checkRSSIThreshHistory();
  checkCalibrationPhases(refIndex, beaconIndexes);
  return true;
}
