/**
 * @file       system_commands.c
 *
 * @brief      Contains all the CLI commands for the system. Later, when the RPi is online,
 *             this module will need to be adapted for an abstracted source. 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

// system_commands.c is the modern Orion style commands, and has a different command list
#ifndef VENUS_BW_COMPATIBILITY
/*******************************************************************************************
 * Includes
 *******************************************************************************************/
#include "cli_task.h"
#include "main.h"
#include "string.h"
#include "cmsis_os.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stm32h7xx_hal.h"
#include "uart_driver.h"
#include "amb_control.h"
#include "system_commands.h"
#include "CotaCommonTypes.h"
#include "client.h"
#include "ctrl_interface.h"

/*******************************************************************************************
 * Defines 
 ******************************************************************************************/
#define CLI_CMD_ID_POS             0            ///< The position in an buffer sent to the control task where the ID is stored.
#define CLI_PROXY_NUM_ARGS_POS     1            ///< The position in an buffer sent to the control task where the number of proxy arguments is stored.
#define CLI_PROXY_ARGS_START_POS   2            ///< The position in an buffer sent to the control task where the proxy arguments start being stored.

/*******************************************************************************************
 * Typedefs
 ******************************************************************************************/

/*******************************************************************************************
 * Private Variable Declarations
 *******************************************************************************************/

/*
  Function Callbacks
 */
static BaseType_t AmbOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AmbOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t UvpOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t UvpOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t PuRaiseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t PuDropCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t TxOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t TxOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RxOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RxOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SelectUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSelectedUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetPdDoneCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AmbSpiEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AmbSpiDisCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RegisterClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RemoveClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ProxyCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetLedCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t DiscClientListCmdCallback (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t JoinClientListCmdCallback (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t TpcClientListCmdCallback (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RdyClientListCmdCallback (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
/*
  Helper functions
 */
static bool isArgAll(const char* pArg, BaseType_t argLen);
static bool getNumArg(const char* pArg, BaseType_t argLen, long* num);
static void fillBufferWithSelectedUvpState(char *pcWriteBuffer, size_t xWriteBufferLen);
static void fillBufferWithUvpEnState(char *pcWriteBuffer, size_t xWriteBufferLen);
static void fillBufferWithAmbEnState(char *pcWriteBuffer, size_t xWriteBufferLen);
static void fillBufferWithAPUEnState(char *pcWriteBuffer, size_t xWriteBufferLen);
static void fillBufferWithASpiEnState(char *pcWriteBuffer, size_t xWriteBufferLen);
static bool parseLongId(const char* pArg, ExtdAddr_t *longId);

static const CLI_Command_Definition_t gCliCmds[] = 
{
    { "amb_on",        "\r\namb_on       [AMB Number | all] Turn on specified AMB or all",  AmbOnCmdCallback, 1},
    { "amb_off",       "\r\namb_off      [AMB Number | all] Turn off specified AMB or all",  AmbOffCmdCallback, 1},
    { "uvp_on",        "\r\nuvp_on       [AMB Number | all] Turn on UVP's on a specified AMB or all",  UvpOnCmdCallback, 1},
    { "uvp_off",       "\r\nuvp_off      [AMB Number | all] Turn off UVP's on a specified AMB or all",  UvpOffCmdCallback, 1},
    { "select_uvp",    "\r\nselect_uvp   [UVP Number 0-15 | all] Selects UVP's to interact with",  SelectUvpCmdCallback, 1},
    { "get_selected_uvp", "\r\nget_select_uvp [] Get the selected UVP number",  GetSelectedUvpCmdCallback, 0},
    { "pu_raise",      "\r\npu_raise     [AMB Number | all] Raise specified AMB's phase update pin or all",  PuRaiseCmdCallback, 1},
    { "pu_drop",       "\r\npu_drop      [AMB Number | all] Drop specified AMB's phase update pin or all",  PuDropCmdCallback, 1},
    { "tx_on",         "\r\ntx_on        [] Turn on the TX pin",  TxOnCmdCallback, 0},
    { "tx_off",        "\r\ntx_off       [] Turn off the TX pin",  TxOffCmdCallback, 0},
    { "get_pd_done",   "\r\nget_pd_done  [] Get the state of the phase detect pins for every AMB",  GetPdDoneCmdCallback, 0},
    { "get_tx",        "\r\nget_tx       [] Gets the TX pin state",  GetTxCmdCallback, 0},
    { "rx_on",         "\r\nrx_on        [] Turn on the RX pin",  RxOnCmdCallback, 0},
    { "rx_off",        "\r\nrx_off       [] Turn off the RX pin",  RxOffCmdCallback, 0},
    { "get_rx",        "\r\nget_rx       [] Gets the RX pin state",  GetRxCmdCallback, 0},
    { "amb_spi_en",    "\r\namb_spi_en   [AMB Number 0-3 | all] Enable SPI transfers for the specified AMB or all",  AmbSpiEnCmdCallback, 1},
    { "amb_spi_dis",   "\r\namb_spi_dis  [AMB Number 0-3 | all] Disable SPI transfers for the specified AMB or all",  AmbSpiDisCmdCallback, 1},
    { "register_client", "\r\nregister_client  [Client ID][QueryType][Priority] Register a new client with the system. QueryType and Priority are optional.",  RegisterClientCmdCallback, 1},
    { "remove_rx",       "\r\nremove_client    [Client ID] Removes a receiver from the list of registered receivers",  RemoveClientCmdCallback, 3},
    { "proxy_command",   "\r\nproxy_command    [Data] Send a generic command to the proxy",  ProxyCommandCmdCallback, -1},
    { "set_led",         "\r\nset_led          [LED#][0=off | 1=on]  Turn on/off LED",  SetLedCmdCallback, 2},
    { "disc_cl",         "\r\ndisc_cl     Displays each client that is in the discovered state", DiscClientListCmdCallback},
    { "join_cl",         "\r\njoin_cl     Displays each client that is in the joined state", JoinClientListCmdCallback},
    { "tpc_cl",          "\r\ntpc_cl      Displays each client that is in the TPC state", TpcClientListCmdCallback},
    { "rdy_cl",          "\r\nrdy_cl      Displays each client that is in the ready state", RdyClientListCmdCallback}
};

const CLI_Command_Definition_t *GetCliCmds(void)
{
    return gCliCmds;
}

uint16_t GetCliCmdSize(void)
{
    return sizeof(gCliCmds)/sizeof(gCliCmds[0]);
}
/*******************************************************************************************
 * Private Function Declarations 
 *******************************************************************************************/



/**
 * @brief Process the amb_on command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AmbOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbEnable(ALL_AMB_MASK);
        fillBufferWithAmbEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetEnabled();
        ambMask |= NUM_TO_MASK(num);
        AmbEnable(ambMask);
        fillBufferWithAmbEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
        return retVal;
}

/**
 * @brief Process the amb_off command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AmbOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
        BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbEnable(AMB_DISABLE_ALL);
        fillBufferWithAmbEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetEnabled();
        ambMask &= (~(NUM_TO_MASK(num)));
        AmbEnable(ambMask);
        fillBufferWithAmbEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
        return retVal;
}

/**
 * @brief Process the uvp_on command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t UvpOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        if (AmbUvpEnable(ALL_AMB_MASK) == COTA_ERROR_NONE)
        {
            fillBufferWithUvpEnState(pcWriteBuffer, xWriteBufferLen);
        }
        else
        {
            snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: One of the AMB's was off and it's UVP could not be enabled\r\n");
        }
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetUvpEnabled();
        ambMask |= NUM_TO_MASK(num);
        if (AmbUvpEnable(ambMask) == COTA_ERROR_NONE)
        {
            fillBufferWithUvpEnState(pcWriteBuffer, xWriteBufferLen);
        }
        else
        {
            snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Attempt to enable UVP on AMB that is off.\r\n");
        }
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
        return retVal;
}



/**
 * @brief Process the uvp_off command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t UvpOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbUvpEnable(AMB_DISABLE_ALL);
        fillBufferWithUvpEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetUvpEnabled();
        ambMask &= (~(NUM_TO_MASK(num)));
        AmbUvpEnable(ambMask);
        fillBufferWithUvpEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
        return retVal;
}


/**
 * @brief Process the pu_raise command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t PuRaiseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbSetPUState(ALL_AMB_MASK);
        fillBufferWithAPUEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetPuState();
        ambMask |= NUM_TO_MASK(num);
        AmbSetPUState(ambMask);
        fillBufferWithAPUEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
        return retVal;
}

/**
 * @brief Process the pu_drop command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t PuDropCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbSetPUState(AMB_DISABLE_ALL);
        fillBufferWithAPUEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetPuState();
        ambMask &= (~(NUM_TO_MASK(num)));
        AmbSetPUState(ambMask);
        fillBufferWithAPUEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
        return retVal;
}

/**
 * @brief Process the tx_on command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t TxOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    
    AmbEnableTx(true);
    
    if (AmbInTxMode())
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, SUCCESS_MSG);
    }
    else
    {
         snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);     
    }
    
    return retVal;
}

/**
 * @brief Process the tx_off command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t TxOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    
    AmbEnableTx(false);
    
    if (!AmbInTxMode())
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, SUCCESS_MSG);
    }
    else
    {
         snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);     
    }
    
    return retVal;
}

/**
 * @brief Process the get_tx command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
   
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\nTx is %s\r\n", AmbInTxMode() ? ENABLED_MSG : DISABLED_MSG);

    return retVal;
}

/**
 * @brief Process the rx_on command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t RxOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    
    AmbEnableRx(true);
    
    if (AmbInRxMode())
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, SUCCESS_MSG);
    }
    else
    {
         snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);     
    }
    
    return retVal;
}

/**
 * @brief Process the rx_off command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t RxOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    
    AmbEnableRx(false);
    
    if (!AmbInRxMode())
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, SUCCESS_MSG);
    }
    else
    {
         snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);     
    }
    
    return retVal;
}

/**
 * @brief Process the get_rx command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
   
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\nRx is %s\r\n", AmbInRxMode() ? ENABLED_MSG : DISABLED_MSG);

    return retVal;
}

/**
 * @brief Select the UVP number to interact with.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SelectUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        if (AmbSelectUvp(ALL_UVP_NUM) == COTA_ERROR_NONE)
        {
            fillBufferWithSelectedUvpState(pcWriteBuffer, xWriteBufferLen);
        }
        else
        {
            snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);  
        }
    }
    else if (getNumArg(pArg, argLen, &num) && (num < UVPS_PER_AMB))
    {
        if (AmbSelectUvp(num) == COTA_ERROR_NONE)
        {
            fillBufferWithSelectedUvpState(pcWriteBuffer, xWriteBufferLen);
        }
        else
        {
            snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);  
        }
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
        return retVal;  
}

/**
 * @brief Retrieves the UVP number that is currently selected
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetSelectedUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    fillBufferWithSelectedUvpState(pcWriteBuffer, xWriteBufferLen);
    return pdFALSE;
}

/**
 * @brief Retrieves the state of phase detect of each AMB
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetPdDoneCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    ambMask_t ambMask = AmbGetPhaseDetect();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "PD FOR AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "Done" : "In Progress");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
    return pdFALSE;
}

/**
 * @brief Process the amb_spi_en command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AmbSpiEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
        BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbSpiEnable(ALL_AMB_MASK);
        fillBufferWithASpiEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetSpiEnabled();
        ambMask |= NUM_TO_MASK(num);
        AmbSpiEnable(ambMask);
        fillBufferWithASpiEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
        return retVal;
}

/**
 * @brief Process the amb_spi_dis command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AmbSpiDisCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbSpiEnable(AMB_DISABLE_ALL);
        fillBufferWithASpiEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetSpiEnabled();
        ambMask &= (~(NUM_TO_MASK(num)));
        AmbSpiEnable(ambMask);
        fillBufferWithASpiEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
        return retVal;
}

static BaseType_t RegisterClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    char *pArg;
    BaseType_t argLen;
    regClient_t regData;

    // Contains a Long ID of the client
    pArg = (char *)FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);

    // Null terminate the parameter
    pArg[argLen] = 0x00;

    if(parseLongId(pArg, &(regData.longId)))
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "Registering Client w/ Id: 0x%llX\r\n",
                 *((uint64_t *) &regData));
        
        CtrlSendMsg(CTRL_REGISTER_CLIENT, (uint8_t*) &regData, sizeof(regClient_t));
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "ERROR: Couldn't parse longId: %s\r\n", pArg);
    }
    
    return pdFALSE;
}

static BaseType_t RemoveClientCmdCallback  (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    return pdFALSE;
}

static BaseType_t DiscClientListCmdCallback (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_CLIENT_LIST_DISC, NULL, sizeof(CtrlMsgId_t));
    
    return pdFALSE;
}

static BaseType_t JoinClientListCmdCallback (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_CLIENT_LIST_JOIN, NULL, sizeof(CtrlMsgId_t));
    return pdFALSE;
}

static BaseType_t TpcClientListCmdCallback (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_CLIENT_LIST_TPC, NULL, sizeof(CtrlMsgId_t));
    return pdFALSE;
}

static BaseType_t RdyClientListCmdCallback (char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    CtrlSendMsg(CTRL_CLIENT_LIST_RDY, NULL, sizeof(CtrlMsgId_t));
    return pdFALSE;
}


/**
 * @brief Process the proxy command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t ProxyCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    uint16_t sizeSent;
    uint8_t ii;
    uint8_t buf[MAX_PROXY_CMD_PAYLOAD_SIZE];
    const char* pArg;
    
    buf[CLI_CMD_ID_POS] = HOST_PRX_PROXY_CMD;
    ii = CLI_PROXY_ARGS_START_POS;  //Data for proxy command starts at the position 2.
    do
    {
        pArg = FreeRTOS_CLIGetParameter( pcCommandString, (ii - CLI_PROXY_ARGS_START_POS) + 1 , &argLen);  //argument start at position 1
        buf[ii] = (uint8_t) strtoul(pArg, NULL, 0); 
        ii++;        
    } while(pArg && (ii < MAX_PROXY_CMD_PAYLOAD_SIZE));
    
    buf[CLI_PROXY_NUM_ARGS_POS] = ii - CLI_PROXY_ARGS_START_POS; //We need to subract the header length which is two bytes.
    
    sizeSent = CliSendCommand(buf, ii);
    
    if (sizeSent == ii)
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "SUCCESS\r\n");     
    }
    else
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR: Command failedto post to queue\r\n");
    }
    
    return retVal;                      // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}

/**
*   @brief set the debug LEDs
*
*
*
*/
static BaseType_t SetLedCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
        BaseType_t retVal = pdFALSE;    // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
        ( void ) xWriteBufferLen;           //fixes compiler warnings
    char const *pcParameter1, *pcParameter2;
    BaseType_t xParameter1StringLength, xParameter2StringLength;
    pcParameter1 = FreeRTOS_CLIGetParameter( pcCommandString, 1,&xParameter1StringLength);      //ARG 1 - amb numeral
    pcParameter2 = FreeRTOS_CLIGetParameter( pcCommandString, 2,&xParameter2StringLength);      //ARG 2 - BOOL on or off
    if ( (xParameter1StringLength > 1) || (xParameter1StringLength < 1) || (pcParameter1[0] < '0') || (pcParameter1[0] > '4') )
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR:  Invalid LED number  Hint: use 0 to 4\r\n");
        return retVal;
    }
    if ( (xParameter2StringLength != 1) || (pcParameter2[0] < '0') || (pcParameter2[0] > '1') ){ 
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR:  Invalid LED state - hint: 0 = off or 1 = on\r\n");
        return retVal;      
    }
        
    switch (pcParameter1[0])
    {       
    case '0':
        //as the input is the ascii value from the CLI, the code below takes the offset of the ascii value for zero, '0', from the ascii value inputted by the user to calculate the value
        HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);      
        break;
    case '1':
        HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);
        break;
    case '2':
        HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);
        break;
    case '3':
        HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);
        break;
    case '4':
        HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);
        break;
    default:
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR:  Invalid LED number\r\n");
        return retVal;
    }
    
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "  LED%i %s\r\n", (pcParameter1[0]-'0'), (pcParameter2[0] == '0' ? "Cleared" : "Set") );
        return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}



/********************************************************************************
  Parsing helper functions
 ********************************************************************************/
/**
 * @brief Tests if an argument is 'all' in a case insensitive manner
 *
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 *
 * @return true if 'all', otherwise false 
 */
static bool isArgAll(const char* pArg, BaseType_t argLen)
{
  return (strncasecmp(pArg, "all", argLen) == 0);
}

/**
 * @brief Retrieves a number argument from a string
 * 
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 * @param num    Pointer to type long to receive the value of the string.
 *
 * @return true if the conversion is successful, false otherwise
 */
static bool getNumArg(const char* pArg, BaseType_t argLen, long* num)
{
  char* endPtr;
  *num = strtol(pArg, &endPtr,  0);
  return ((pArg + argLen) == endPtr);   //This tests there was no stray characters in the arg
}

/**
 * @brief Fill the buffer with the current state of AMB enable pins.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithAmbEnState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    ambMask_t ambMask = AmbGetEnabled();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "On" : "Off");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
}

/**
 * @brief Fill the buffer wth the current UVP selection number
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithSelectedUvpState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    uvpNum_t uvpNum = AmbGetSelectedUvp();
    
    if (uvpNum == ALL_UVP_NUM)
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "SELECTED UVP: all\r\n");
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "SELECTED UVP: %d\r\n", uvpNum);      
    }
}

/**
 * @brief Fill the buffer with the current state of the UVP enable pins.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithUvpEnState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    ambMask_t ambMask = AmbGetUvpEnabled();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "UVP EN FOR AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "On" : "Off");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
}

/**
 * @brief Fill the buffer with the current state of the phase update pins.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithAPUEnState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    ambMask_t ambMask = AmbGetPuState();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "PU for AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "Raised" : "Dropped");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
}

/**
 * @brief Fill the buffer with the current state of the spi enabled pins.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithASpiEnState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    ambMask_t ambMask = AmbGetSpiEnabled();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "SPI EN for AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "On" : "Off");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
}

static bool parseLongId(const char* pArg, ExtdAddr_t *longId)
{
    char *endptr = NULL;
    uint64_t temp;
    
    temp = strtoll(pArg, &endptr, 16);

    // error parsing temp if endptr is NULL
    if ((*endptr != NULL) || (temp == 0) || (longId == NULL))
    {
        return false;
    }
    
    memcpy((void *) longId, (void *) &temp, sizeof(ExtdAddr_t));

    return IsLongValid(*longId);
}

#endif // #ifndef VENUS_BW_COMPATIBILITY 
