/****************************************************************************//**
* @file      orion_config.h
*
* @brief     Header file contain general configuration settings for the application
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _OSSIA_CONFIG_H_
#define _OSSIA_CONFIG_H_

#include "proxyHostMsgInf.h"

/*
* Cota Transmitter Task Definitions 
*/
#define GATHER_AND_RSSI_VOTING_STACK_SIZE       1024    ///< The size of the stack allocated for the GatherAndVoteTask by FreeRTOS
#define GATHER_AND_RSSI_VOTING_TASK_PRIORITY    3       ///< Priority for the GatherAndVoteTask

#define PROXY_TASK_STACK_SIZE                   1024    ///< Proxy task stack size in bytes
#define PROXY_TASK_PRIORITY                     5       ///< Proxy task priority

#define FAN_DRIVER_TASK_STACK_SIZE              1536    ///< Fan driver task stack size in bytes  (Added 512 bytes to support two large arrays)
#define FAN_DRIVER_TASK_PRIORITY                7       ///< Fan driver task priority

#define CLI_SEND_TASK_STACK_SIZE                1024    ///< Uart driver task stack size in bytes  (Added 512 bytes to support two large arrays)
#define CLI_SEND_TASK_PRIORITY                  6       ///< Uart driver task priority

#define ERROR_HANDLER_STACK_SIZE                1024    ///< Stack size for the error task
#define ERROR_HANDLER_TASK_PRIORITY             1       ///< Priority of the error task

#define CTRL_TASK_STACK_SIZE                    1024    ///< Stack size for the control task
#define CTRL_TASK_PRIORITY                      2       ///< Priority of the control task

#define MASTER_AMB_SPI_PORT                     1       ///< The spi port designated as the master SPI port for AMB data transfers
#define MAX_NUM_AMB                             4       ///< The number of AMB's available on the transmitter.
#define MAX_NUM_CLIENTS                         8       ///< 8 for ease of development. will need to be 1024 
#define VALID_AMBS                              0xF     ///< A mask indicating which AMB's are actually connected to the CCB and ready to do.

#define CAL_REF_AMB_NUM                         0       ///< The AMB number containing the reference antenna.
#define CAL_REF_UVP_NUM                         0       ///< The UVP number containing the reference antenna.
#define CAL_REF_AMU_NUM                         2       ///< The AMU number of the reference antenna.
#define CAL_BEACON_UVP_NUM                      13      ///< The UVP number to use for the calibration beacon
#define CAL_BEACON_AMU_NUM                      1       ///< The AMU number to use for the calibration beacon

#define SYSCLK_I2C_HANDLE                       hi2c1   ///< The I2C handle going to the system clock
#define MULIPLEXED_I2C_HANDLE                   hi2c1   ///< The I2C handle going to the I2C multiplexers.  @todo The I2C handles need to be different. We need to change this we get to the CCB.

#define CRITICAL_CCB_TEMPERATURE                60      ///< If any CCB thermometer exceeds this temperature (in C), fans go full on, and charging is prohibited. 
#define CRITICAL_UVP_TEMPERATURE                60      ///< If any average UVP temperaure exceeds this temperature (in C), fans go full on, and charging is prohibited.

#define MIN(a, b)              ((a) < (b)) ? (a) : (b)  ///< Returns the smallest of a and b
#define MAX(a, b)          ((a >= b) ? a : b)           ///< Returns the maximum between the two passed in parameters

void OrionConfigInit(void);
void SetPanId(uint16_t panId);
void GetPanId(uint16_t *panId);
void SetProxyId(uint16_t proxyId);
void GetProxyId(uint16_t *proxyId);
void SetCommChannel(uint8_t channel);
void GetCommChannel(uint8_t *channel);
void SetDiscPeriod(uint16_t discPeriod);
void GetDiscPeriod(uint16_t *discPeriod);
void SetToneSlotDuration(uint16_t toneSlotDur);
void GetToneSlotDuration(uint16_t *toneSlotDur);
void SetPowerSlotDuration(uint16_t powerSlotDur);
void GetPowerSlotDuration(uint16_t *powerSlotDur);
void SetPowerSlotCount(uint8_t powerSlotCnt);
void GetPowerSlotCount(uint8_t *powerSlotCnt);
void SetTotalTones(uint8_t totalTones);
void GetTotalTones(uint8_t *totalTones);
void SetProxyCommandResp(uint8_t* buf, uint8_t size);
void GetProxyCommandResp(uint8_t* buf, uint8_t* size);
#endif //#ifndef _OSSIA_CONFIG_H_
