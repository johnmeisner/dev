/**
* @file       error_task.c
*
* @brief      The purpose of this header is to supply an interface to communicate errors
*             to external entities such as a UART or a Raspberry PI.
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*/


#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "error.h"
#include "string.h"
#include "stdio.h"
#include "uart_driver.h"
#include "orion_config.h"

#ifdef COTA_ERROR_ENABLED                     ///< must be defined in the compile line to enable error handling.


#define ERROR_HANDLER_QUEUE_DEPTH   5         ///< The maximum number of error codes the error queue can hold.
#define ERROR_HANDLER_BUFFER_SIZE   64        ///< The maximum size of an output message for errors
#define DONT_WAIT_FOR_QUEUE         0         ///< When posting to the queue we don't wait for availble spots.

#ifdef USE_COTA_ERROR_STRINGS                 ///< This define is used to control whether strings will be availble to describe the errors.

    #define COTA_ERROR_ENTRY(num, string) {num, string}
    typedef struct _cotaErrorInfo
    {
      cotaError_t errNum;   ///< The error code of type #cotaError_t
      char*       errMsg;   ///< A string describing the error
    } cotaErrorInfo;

#else

    #define COTA_ERROR_ENTRY(num, string) {num}
    typedef struct _cotaErrorInfo
    {
      cotaError_t errNum;   ///< The error code of type #cotaError_t
    } cotaErrorInfo;

#endif

/**
 * This array maps an error code to its mask and potentially a descriptive string.
 */
static cotaErrorInfo cotaErrorMap[] = {
    COTA_ERROR_ENTRY(COTA_ERROR_NONE,                           "SUCCESS"),
    COTA_ERROR_ENTRY(COTA_ERROR_HAL,                            "Driver Error"),
    COTA_ERROR_ENTRY(COTA_ERROR_QUEUE_FAILED,                   "Error handler queue not created"),
    COTA_ERROR_ENTRY(COTA_ERROR_TASK_FAILED,                    "Error handler task failed to create"),  
    COTA_ERROR_ENTRY(COTA_ERROR_SPI_TRANSFER_FAILED,            "A SPI transfer has failed"),
    COTA_ERROR_ENTRY(COTA_ERROR_SPI_SIMUL_TRANS_FAILED,         "A simlultaneous SPI transfer has failed"),
    COTA_ERROR_ENTRY(COTA_ERROR_UVP_SIMUL_READ_TO_LARGE,        "A simulataneous read of simulataneous UVP has a buffer that is too large"),
    COTA_ERROR_ENTRY(COTA_ERROR_UART_TX_FAILED,                 "A UART transmit failed"),
    COTA_ERROR_ENTRY(COTA_ERROR_UVP_LOCK_FAILED,                "A UVP lock could not be confirmed"),
    COTA_ERROR_ENTRY(COTA_ERROR_UVP_REG_VALUE_NOT_IN_MASK,      "An attempt to set a UVP value failed because the value couldn't fit in the mask"),
    COTA_ERROR_ENTRY(COTA_ERROR_AMB_NOT_ENABLED,                "An attempt was made to enable a UVP on an AMB not enabled"),
    COTA_ERROR_ENTRY(COTA_ERROR_UVP_UNKNOWN,                    "An attempt was made to select an unknown UVP number"),
    COTA_ERROR_ENTRY(COTA_ERROR_UVP_WAIT_FOR_TRANSMIT_TIMEOUT,  "A UVP could not confirm a transmit occurred"), 
    COTA_ERROR_ENTRY(COTA_ERROR_UVP_WAIT_FOR_PHASE_DET_TIMEOUT, "An error occurred while waiting for a phase detect from a UVP"),
    COTA_ERROR_ENTRY(COTA_ERROR_UVP_ENABLE,                     "An error occurred while enabling UVP's"),
    COTA_ERROR_ENTRY(COTA_ERROR_SYSCLK_FAILED_TO_WRITE_PAGE,    "An error occured while selecting the page for the system clock"),
    COTA_ERROR_ENTRY(COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG,     "An error occured while writing a register to the system clock"),
    COTA_ERROR_ENTRY(COTA_ERROR_SYSCLK_FAILED_TO_READ_REG,      "An error occured while reading a register from the system clock"),
    COTA_ERROR_ENTRY(COTA_ERROR_I2C_INVALID_MULTIPLEXER,        "An error occurred because an invalid i2c multiplexer was initialized"),
    COTA_ERROR_ENTRY(COTA_ERROR_I2C_INVLAID_MULTI_CHANNEL,      "An error occurred beacuse an invalid i2c multiplexer channel was intialized"),
    COTA_ERROR_ENTRY(COTA_ERROR_I2C_FAILED_TO_WRITE_MULTI_CHAN, "An error occurred while selecting the channel on an i2c multiplexer"),
    COTA_ERROR_ENTRY(COTA_ERROR_I2C_FAILED_TO_WRITE,            "A failue occurred while writing to an i2c device"),
    COTA_ERROR_ENTRY(COTA_ERROR_I2C_FAILED_TO_READ,             "A failure occurred while reading from an i2c device"),
    COTA_ERROR_ENTRY(COTA_ERROR_NOT_IMPLEMENTED,                "A function was called that is not implemented"),
    COTA_ERROR_ENTRY(COTA_ERROR_INVALID_CCB_TERMOMETEER,        "An attempt was made to read a temperature from a thermometer that doesn't exist"),
    COTA_ERROR_ENTRY(COTA_ERROR_BAD_PARAMETER,                  "A bad parameter was passed to a function"),
    COTA_ERROR_ENTRY(COTA_ERROR_OVER_CRITICAL_TEMPERATURE,      "A critical temperature has been exceeded"),
    COTA_ERROR_ENTRY(COTA_ERROR_FAILED_TO_CREATE_VAR_QUEUE,     "A variable length queue could not be created"),
    COTA_ERROR_ENTRY(COTA_ERROR_FAILED_TO_SEND_TO_VAR_QUEUE,    "A message could not be sent to a variable queue"),
    COTA_ERROR_ENTRY(COTA_ERROR_CLI_TX_FAILED,                  "A message to the CLI could not be sent"),
    COTA_ERROR_ENTRY(COTA_ERROR_CLIENT_MANAGER_INIT_FAILED,     "The Client Manager failed to properly initialize"),
    COTA_ERROR_ENTRY(COTA_ERROR_MAX_NUM_CLIENTS_STORED,         "The maximum number of clients has been "),
    COTA_ERROR_ENTRY(COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND,      "Client Id is not recognized"),
    COTA_ERROR_ENTRY(COTA_ERROR_CLIENT_ALREADY_REGISTERED,      "Tried to add a client that was already registered"),
    COTA_ERROR_ENTRY(COTA_ERROR_NULL_VALUE_PASSED,              "A NULL value was passed to a function when a non-NULL value was expected"),
    COTA_ERROR_ENTRY(COTA_ERROR_FAILED_TO_REMOVE_NODE,          "Failed to remove a client from the client manager"),
    COTA_ERROR_ENTRY(COTA_ERROR_COMM_ACK_TIMER_START_FAIL,      "Failed to start the comm acknowledgment timeout timer"),
    COTA_ERROR_ENTRY(COTA_ERROR_CONFIG_MUTEX_FAILED_TAKE,       "The mutex in the #orion_config.c file failed to take"),
    COTA_ERROR_ENTRY(COTA_ERROR_CONFIG_MUTEX_FAILED_GIVE,       "The mutex in the #orion_config.c file failed to give"),
    COTA_ERROR_ENTRY(COTA_ERROR_FAILED_TO_SEND_PROXY_MSG,       "Failed to send a proxy message because the queue is full"),
    COTA_ERROR_ENTRY(COTA_ERROR_FAILED_TO_PROXY_MSG,            "Ctrl Task failed to parse proxy message"),
};

#define COTA_ERROR_MAP_SIZE (sizeof(cotaErrorMap)/sizeof(cotaErrorMap[0]))

/**
 * @brief Outputs error code information to the UART
 * 
 * @param  cotaError An error code of type #cotaError_t
 */ 
static void OutputError(cotaError_t errorCode)
{
  uint16_t ii;
  char uartBuff[ERROR_HANDLER_BUFFER_SIZE];
  
  for (ii = 0; ii < COTA_ERROR_MAP_SIZE; ii++)
  {
    if (cotaErrorMap[ii].errNum == errorCode)
    {
#ifdef USE_COTA_ERROR_STRINGS
      snprintf(uartBuff, ERROR_HANDLER_BUFFER_SIZE, "ERROR %d : %s\n\r", errorCode, cotaErrorMap[ii].errMsg); 
#else
      snprintf(uartBuff, ERROR_HANDLER_BUFFER_SIZE, "ERROR %d\n\r", errorCode); 
#endif    
      CotaUartTransmit((uint8_t*)uartBuff, strlen(uartBuff), portMAX_DELAY);
      
      break;
    }
  }
}

/**
 * @brief  Outputs an error to UART. 
 *
 * @param  cotaError An error code of type #cotaError_t,  If it's COTA_ERROR_NONE, the error is not posted
 * 
 * @return returns the cotaError passed so you can place this in the final return call of the function.
 */
cotaError_t PostCotaError(cotaError_t cotaError)
{
  if (cotaError != COTA_ERROR_NONE)
  {
    OutputError(cotaError);
  }
  return cotaError;
}

#endif //#ifdef COTA_ERROR_ENABLED
