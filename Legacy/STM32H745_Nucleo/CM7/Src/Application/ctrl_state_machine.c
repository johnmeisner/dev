/**
 * @file       ctrl_state_machine.c
 *
 * @brief      Contains the control task state machine skeleton
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 * @details    Below is a graphical representation of the state machine. For descriptions
 *             on each state, see the state declarations below.
 *             
 * @code{.c}
 *                              --------------------
 *                              |                   |
 *                   ---------->|       Init        |
 *                  /           |                   |
 *                 /            --------------------
 *                /                        |
 *               /                         |
 *              /                          |
 *             \/                         \|/
 *     --------------------       --------------------
 *    |                    |     |                   |
 *    |       Debug        |<----|       Ready       |<--> ...To States listed below
 *    |                    |     |                   |
 *     --------------------       --------------------
 *                                        /|\
 *                                         |
 *                                         |
 *                                        \|/
 *                               --------------------
 *                               |                   |
 *                               |       TPS         |
 *                               |                   |
 *                               --------------------
 *
 *
 *
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|       Disc        |
 *                               |                   |
 *                               --------------------  
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|       Join        |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|        TPC        |
 *                               |                   |
 *                               --------------------
 *                                --------------------
 *                               |                   |
 *     From Ready state... <---->|      Query        |
 *                               |                   |
 *                               --------------------
 *                                --------------------
 *                               |                   |
 *     From Ready state... <---->|    Calibration    |
 *                               |                   |
 *                               --------------------
 *                               --------------------
 *                               |                   |
 *     From Ready state... <---->|     Schedule      |
 *                               |                   |
 *                               --------------------
 * @endcode
 **/

#include "state_machine.h"
#include "stddef.h"
#include "ctrl_state_machine.h"

/**
 * @brief State that indicates an error in the state machine has occurred
 */
static StateMachineState_t ctrlError;

/**
 * @brief Puts the transmitter in debug mode. In debug mode, the system disables
 *        all timers and yields more control to the user.
 */
static StateMachineState_t ctrlDebug;

/**
 * @brief Starts system resources (timers) to prepare the system for the ready state
 */
static StateMachineState_t ctrlInit;

/**
 * @brief The state of the system when it is ready to service any need based on
 *        flags set. This is the 'center' state that can reach all other states. 
 */
static StateMachineState_t ctrlRdy;

/**
 * @brief On entry to this state, a discovery signal is sent. An
 *        acknowledgment timeout timer will cause this state to exit.
 */
static StateMachineState_t ctrlDisc;

/**
 * @brief On entry to this state, a join signal is sent. An
 *        acknowledgment timeout timer will cause this state to exit.
 */
static StateMachineState_t ctrlJoin;

/**
 * @brief On entry to this state, a TPC message is send. An
 *        acknowledgment timeout timer will cause this state to exit.
 */
static StateMachineState_t ctrlTpc;

/**
 * @brief On entry to this state, a query message is sent. An
 *        acknowledgment timeout timer will cause this state to exit.
 */
static StateMachineState_t ctrlQuery;

/**
 * @brief This state determines if its appropriate to calibrate the system. The
 *        system could require a calibration either based on the amount of time that
 *        has passed, or the measured temperature of the system.
 */
static StateMachineState_t ctrlCalibration;

/**
 * @brief In this state, clients are scheduled for TPS. This set is required to execute
 *        before #ctrlTPS can be entered.
 */
static StateMachineState_t ctrlSched;

/**
 * @brief This is the TPS or Charging state of the transmitter. When charging, the
 *        types of messages (task-to-task) is reduced. This architecture prevents
 *        the transmitter from telling the proxy to send a message while the
 *        system is charging clients
 */
static StateMachineState_t ctrlTps;

/*
 * Error State Definitions 
 */
static StateMachineTransition_t ctrlErrorTxnOptions[];

static StateMachineState_t ctrlError =
{
    .state             = CTRL_STATE_ERROR,
    .entryFcn          = ErrorEntry,
    .stateName         = "Control task error",
    .transitionOptions =  ctrlErrorTxnOptions
};

static StateMachineTransition_t ctrlErrorTxnOptions[] =
{
    {
        .checkCondition = CtrlMachineError,
        .exitFcn        = NULL,
        .nextState      = &ctrlError
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Init State Definitions
 */
static StateMachineTransition_t ctrlInitTxnOptions[];

static StateMachineState_t ctrlInit =
{
    .state             = CTRL_STATE_INIT,
    .entryFcn          = InitEntry,
    .stateName         = "Control task initialization",
    .transitionOptions = ctrlInitTxnOptions
};

static StateMachineTransition_t ctrlInitTxnOptions[] =
{
    {
        .checkCondition = InitToDebugCheck,
        .exitFcn        = InitToDebugExit,
        .nextState      = &ctrlDebug
    },
    {
        .checkCondition = InitToRdyCheck,
        .exitFcn        = InitToRdyExit,
        .nextState      = &ctrlRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Debug State Definitions
 */
static StateMachineTransition_t ctrlDebugTxnOptions[];

static StateMachineState_t ctrlDebug =
{
    .state             = CTRL_STATE_DEBUG,
    .entryFcn          = DebugEntry,
    .stateName         = "Control task debug mode",
    .transitionOptions = ctrlDebugTxnOptions
};

static StateMachineTransition_t ctrlDebugTxnOptions[] =
{
    {
        .checkCondition = DebugToInitCheck,
        .exitFcn        = DebugToInitExit,
        .nextState      = &ctrlInit
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Ready State Definitions
 */
static StateMachineTransition_t ctrlRdyTxnOptions[];

static StateMachineState_t ctrlRdy =
{
    .state             = CTRL_STATE_RDY,
    .entryFcn          = ReadyEntry,
    .stateName         = "Control task ready mode",
    .transitionOptions = ctrlRdyTxnOptions
};

static StateMachineTransition_t ctrlRdyTxnOptions[] =
{
    {
        .checkCondition = RdyToDebugCheck,
        .exitFcn        = RdyToDebugExit,
        .nextState      = &ctrlDebug
    },
    {
        .checkCondition = RdyToDiscCheck,
        .exitFcn        = RdyToDiscExit,
        .nextState      = &ctrlDisc
    },
    {
        .checkCondition = RdyToJoinCheck,
        .exitFcn        = RdyToJoinExit,
        .nextState      = &ctrlJoin
    },
    {
        .checkCondition = RdyToTpcCheck,
        .exitFcn        = RdyToTpcExit,
        .nextState      = &ctrlTpc
    },
    {
        .checkCondition = RdyToQueryCheck,
        .exitFcn        = RdyToQueryExit,
        .nextState      = &ctrlQuery
    },
    {
        .checkCondition = RdyToCalCheck,
        .exitFcn        = RdyToCalExit,
        .nextState      = &ctrlCalibration
    },
    {
        .checkCondition = RdyToSchedCheck,
        .exitFcn        = RdyToSchedExit,
        .nextState      = &ctrlSched
    },
    {
        .checkCondition = RdyToTpsCheck,
        .exitFcn        = RdyToTpsExit,
        .nextState      = &ctrlTps
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Discovery State Definitions
 */
static StateMachineTransition_t ctrlDiscTxnOptions[];

static StateMachineState_t ctrlDisc =
{
    .state             = CTRL_STATE_SEND_DISC,
    .entryFcn          = DiscEntry,
    .stateName         = "Control task Sending discovery messages",
    .transitionOptions = ctrlDiscTxnOptions
};

static StateMachineTransition_t ctrlDiscTxnOptions[] =
{
    {
        .checkCondition = DiscToRdyCheck,
        .exitFcn        = DiscToRdyExit,
        .nextState      = &ctrlRdy
    },
    NULL_TRANSITION_STRUCT
};


/*
 * Join State Definitions
 */
static StateMachineTransition_t ctrlJoinTxnOptions[];

static StateMachineState_t ctrlJoin =
{
    .state             = CTRL_STATE_SEND_JOIN,
    .entryFcn          = JoinEntry,
    .stateName         = "Control task Sending join messages",
    .transitionOptions = ctrlJoinTxnOptions
};

static StateMachineTransition_t ctrlJoinTxnOptions[] =
{
    {
        .checkCondition = JoinToRdyCheck,
        .exitFcn        = JoinToRdyExit,
        .nextState      = &ctrlRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Tpc State Definitions
 */
static StateMachineTransition_t ctrlTpcTxnOptions[];

static StateMachineState_t ctrlTpc =
{
    .state             = CTRL_STATE_SEND_TPC,
    .entryFcn          = TpcEntry,
    .stateName         = "Control task Sending TPC messages",
    .transitionOptions = ctrlTpcTxnOptions
};

static StateMachineTransition_t ctrlTpcTxnOptions[] =
{
    {
        .checkCondition = TpcToRdyCheck,
        .exitFcn        = TpcToRdyExit,
        .nextState      = &ctrlRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Query State Definitions
 */
static StateMachineTransition_t ctrlQueryTxnOptions[];

static StateMachineState_t ctrlQuery =
{
    .state             = CTRL_STATE_SEND_QUERY,
    .entryFcn          = QueryEntry,
    .stateName         = "Control task Sending query messages",
    .transitionOptions = ctrlQueryTxnOptions
};

static StateMachineTransition_t ctrlQueryTxnOptions[] =
{
    {
        .checkCondition = QueryToRdyCheck,
        .exitFcn        = QueryToRdyExit,
        .nextState      = &ctrlRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Calibration State Definitions
 */
static StateMachineTransition_t ctrlCalTxnOptions[];

static StateMachineState_t ctrlCalibration =
{
    .state             = CTRL_STATE_CAL,
    .entryFcn          = CalEntry,
    .stateName         = "Control task Calibrating system",
    .transitionOptions = ctrlCalTxnOptions
};

static StateMachineTransition_t ctrlCalTxnOptions[] =
{
    {
        .checkCondition = CalToRdyCheck,
        .exitFcn        = CalToRdyExit,
        .nextState      = &ctrlRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * Schedule State Definitions
 */
static StateMachineTransition_t ctrlSchedTxnOptions[];

static StateMachineState_t ctrlSched =
{
    .state             = CTRL_STATE_SCHED,
    .entryFcn          = SchedEntry,
    .stateName         = "Control task Scheduling Clients",
    .transitionOptions = ctrlSchedTxnOptions
};

static StateMachineTransition_t ctrlSchedTxnOptions[] =
{
    {
        .checkCondition = SchedToRdyCheck,
        .exitFcn        = SchedToRdyExit,
        .nextState      = &ctrlRdy
    },
    NULL_TRANSITION_STRUCT
};

/*
 * TPS (Charging) State Definitions
 */
static StateMachineTransition_t ctrlTpsTxnOptions[];

static StateMachineState_t ctrlTps =
{
    .state             = CTRL_STATE_TPS,
    .entryFcn          = TpsEntry,
    .stateName         = "Control task Charging Clients",
    .transitionOptions = ctrlTpsTxnOptions
};

static StateMachineTransition_t ctrlTpsTxnOptions[] =
{
    {
        .checkCondition = TpsToRdyCheck,
        .exitFcn        = TpsToRdyExit,
        .nextState      = &ctrlRdy
    },
    NULL_TRANSITION_STRUCT
};

StateMachineState_t *CtrlSmInit(void)
{
    return &ctrlInit;
}



__weak void ErrorEntry       (void){ return; }
__weak void DebugEntry       (void){ return; }
__weak void InitEntry        (void){ return; }
__weak void ReadyEntry       (void){ return; }
__weak void TpsEntry         (void){ return; }
__weak void DiscEntry        (void){ return; }
__weak void TpcEntry         (void){ return; }
__weak void JoinEntry        (void){ return; }
__weak void QueryEntry       (void){ return; }
__weak void CalEntry         (void){ return; }
__weak void SchedEntry       (void){ return; }

// By having the error return true, the error state works a non-blocking while(1) loop
__weak bool CtrlMachineError (void){ return true; }

__weak bool InitToRdyCheck   (void){ return false; }
__weak bool InitToDebugCheck (void){ return false; }
__weak bool DebugToInitCheck (void){ return false; }
__weak bool RdyToDebugCheck  (void){ return false; }
__weak bool RdyToTpsCheck    (void){ return false; }
__weak bool RdyToDiscCheck   (void){ return false; }
__weak bool RdyToJoinCheck   (void){ return false; }
__weak bool RdyToTpcCheck    (void){ return false; }
__weak bool RdyToQueryCheck  (void){ return false; }
__weak bool RdyToCalCheck    (void){ return false; }
__weak bool RdyToSchedCheck  (void){ return false; }
__weak bool TpsToRdyCheck    (void){ return false; }
__weak bool DiscToRdyCheck   (void){ return false; }
__weak bool JoinToRdyCheck   (void){ return false; }
__weak bool TpcToRdyCheck    (void){ return false; }
__weak bool QueryToRdyCheck  (void){ return false; }
__weak bool CalToRdyCheck    (void){ return false; }
__weak bool SchedToRdyCheck  (void){ return false; }
                            
__weak void InitToRdyExit    (void){ return; }
__weak void InitToDebugExit  (void){ return; }
__weak void DebugToInitExit  (void){ return; }
__weak void RdyToDebugExit   (void){ return; }
__weak void RdyToTpsExit     (void){ return; }
__weak void RdyToDiscExit    (void){ return; }
__weak void RdyToJoinExit    (void){ return; }
__weak void RdyToTpcExit     (void){ return; }
__weak void RdyToQueryExit   (void){ return; }
__weak void RdyToCalExit     (void){ return; }
__weak void RdyToSchedExit   (void){ return; }
__weak void TpsToRdyExit     (void){ return; }
__weak void DiscToRdyExit    (void){ return; }
__weak void JoinToRdyExit    (void){ return; }
__weak void TpcToRdyExit     (void){ return; }
__weak void QueryToRdyExit   (void){ return; }
__weak void CalToRdyExit     (void){ return; }
__weak void SchedToRdyExit   (void){ return; }



