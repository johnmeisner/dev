/**
 * @file       ctrl_state_machine.h
 *
 * @brief      Header file for the state machine for the control task
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CTRL_STATE_MACHINE_H_
#define _CTRL_STATE_MACHINE_H_

/**
 * @brief An enum that denotes what state is being defined in state definitions
 */
typedef enum _ctrlStates_t
{
    CTRL_STATE_ERROR = 0,    ///< Denotes a error state for the control task 
    CTRL_STATE_DEBUG,        ///< Give the user more intimate control of the system. We cannot charging clients in this state
    CTRL_STATE_INIT,         ///< The transmitter is initializing its hardware and software resources. 
    CTRL_STATE_RDY,          ///< The transmitter is ready to begin TPS. All communication channels are handled in this task
    CTRL_STATE_SEND_DISC,    ///< Send discovery message to clients that are in range. 
    CTRL_STATE_SEND_TPC,     ///< Send TPC messages to all clients that are joined. 
    CTRL_STATE_SEND_JOIN,    ///< Send join messages to clients that have been registered to ensure they have properly joined the network
    CTRL_STATE_SEND_QUERY,   ///< Send queries to a select number of clients to update their information in the transmitter database.
    CTRL_STATE_CAL,          ///< Check, based on software timers, if an adequate amount of time and temperature drift calibrate the tile. 
    CTRL_STATE_SCHED,        ///< Take the active clients and schedule for power transmission. 
    CTRL_STATE_TPS           ///< The transmitter is charging clients. The functionality of the transmitter is reduced for operation critical tasks.
} ctrlStates_t;


/*
 * State Machine Function Prototypes
 */
void ErrorEntry (void);
void DebugEntry (void);
void InitEntry  (void);
void ReadyEntry (void);
void DiscEntry  (void);
void JoinEntry  (void);
void TpcEntry   (void);
void QueryEntry (void);
void CalEntry   (void);
void SchedEntry (void);
void TpsEntry   (void);

bool CtrlMachineError (void);

bool InitToRdyCheck   (void);
bool InitToDebugCheck (void);
bool DebugToInitCheck (void);
bool RdyToDebugCheck  (void);
bool RdyToTpsCheck    (void);
bool RdyToDiscCheck   (void);
bool RdyToJoinCheck   (void);
bool RdyToTpcCheck    (void);
bool RdyToQueryCheck  (void);
bool RdyToCalCheck    (void);
bool RdyToSchedCheck  (void);
bool DiscToRdyCheck   (void);
bool JoinToRdyCheck   (void);
bool TpcToRdyCheck    (void);
bool QueryToRdyCheck  (void);
bool CalToRdyCheck    (void);
bool SchedToRdyCheck  (void);
bool TpsToRdyCheck    (void);

void InitToRdyExit   (void);
void InitToDebugExit (void);
void DebugToInitExit (void);
void RdyToDebugExit  (void);
void RdyToTpsExit    (void);
void RdyToDiscExit   (void);
void RdyToDebugExit  (void);
void RdyToTpsExit    (void);
void RdyToDiscExit   (void);
void RdyToJoinExit   (void);
void RdyToTpcExit    (void);
void RdyToQueryExit  (void);
void RdyToCalExit    (void);
void RdyToSchedExit  (void);
void TpsToRdyExit    (void);
void DiscToRdyExit   (void);
void JoinToRdyExit   (void);
void TpcToRdyExit    (void);
void QueryToRdyExit  (void);
void CalToRdyExit    (void);
void SchedToRdyExit  (void);

StateMachineState_t *CtrlSmInit(void);

#endif /* #ifndef _CTRL_STATE_MACHINE_H_ */
