/**
 * @file       orion_config.c 
 *
 * @brief      This is a mutex protected configuration file for the Orion system. Any
 *             data saved within this file will be saved to the EEProm at shutdown.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */



/*******************************************************************************************
 * Includes
 *******************************************************************************************/
#include "FreeRTOS.h"
#include "semphr.h"
#include "main.h"
#include "proxyHostMsgInf.h"
#include "orion_config.h"
#include "error.h"
#include <string.h>

/*******************************************************************************************
 * Defines 
 ******************************************************************************************/
#define DEFAULT_PAN_ID               0xC07A      ///< The default value for the PAN ID 
#define DEFAULT_PROXY_ID             0xC07A      ///< The default value for the proxy ID
#define DEFAULT_COMM_CHANNEL         26          ///< The default communication channel for the transmitter
#define DEFAULT_DISC_PERIOD          10000       ///< The default discovery period (milliseconds)

///@todo The names vary between cotacommon and Venus settings. Will have to confirm later. 
#define DEFAULT_TONE_SLOT_DURATION   600         ///< The default duration in microseconds of a tone slot (length of beacon measurement duration)
#define DEFAULT_POWER_SLOT_DURATION  19400       ///< The default duration in microseconds of a power slot

/**
 *  The number of power slots between two adjacent tone slots.
 *  Total power section is gPowerSlotDur * gPowerSlotCount
 **/
#define DEFAULT_POWER_SLOT_COUNT     1           
#define DEFAULT_TOTAL_TONES          50          ///< The default amount of tone/power slots (beacons) in TPS
/*******************************************************************************************
 * Private Variable Declarations
 *******************************************************************************************/
/*
 * Free RTOS variables
 */
static SemaphoreHandle_t gOrionConfigMutex = NULL;

/*
 * Global Static Resources
 */
uint16_t      gPanId          = DEFAULT_PAN_ID;               ///< A Pan ID that defines the ID of the network
uint16_t      gProxyId        = DEFAULT_PROXY_ID;             ///< A ShrtAddr for the proxy 
uint8_t       gCommCh         = DEFAULT_COMM_CHANNEL;         ///< The IEEE 802.15.4 MAC communication channel for proxy to client communication
uint16_t      gDiscPeriod     = DEFAULT_DISC_PERIOD;          ///< The period in seconds that a Discovery signal is sent from the proxy
uint16_t      gToneSlotDur    = DEFAULT_TONE_SLOT_DURATION;   ///< The duration of the tone section in the tone/power pair (beacon beat)
uint16_t      gPowerSlotDur   = DEFAULT_POWER_SLOT_DURATION;  ///< The duration of a power slot in microseconds in a beacon beat
uint8_t       gPowerSlotCnt   = DEFAULT_POWER_SLOT_COUNT;     ///< The number of power slots in a beacon beat. 
uint8_t       gTotalTones     = DEFAULT_TOTAL_TONES;          ///< The total number of tone/power cycles in a TPS (number of beacons)
/*******************************************************************************************
 * Private Function Declarations 
 *******************************************************************************************/

/**
 * @brief Initializes all resources for the orion config module
 */
void OrionConfigInit(void)
{
    /// @todo add code to read config files from EEPROM  code.
    
    while((gOrionConfigMutex = xSemaphoreCreateMutex()) == NULL );
}

/**
 * @brief Stores the pan id in thread safe global memory
 * @param panId  The pan id of the device
 */
void SetPanId(uint16_t panId)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    gPanId = panId;
    
    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Retrieves the pan id from thread safe global memory
 * @param panId pointer to a value to receive the pan id.
 */
void GetPanId(uint16_t *panId)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    (*panId) = gPanId;
    
    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Stores the proxy id in thread safe global memory
 * @param proxyId  The proxy id
 */
void SetProxyId(uint16_t proxyId)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    gPanId = proxyId;
    
    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Retrieves the pan id from thread safe global memory
 * @param proxyId pointer to a value to receive the proxy id.
 */
void GetProxyId(uint16_t *proxyId)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    (*proxyId) = gProxyId;
    
    xSemaphoreGive(gOrionConfigMutex);        
}

/**
 * @brief Stores the com channel in thread safe global memory
 * @param channel The com channel
 */
void SetCommChannel(uint8_t channel)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    gCommCh = channel;
    
    xSemaphoreGive(gOrionConfigMutex); 
}

/**
 * @brief Retrieves the com channel from thread safe global memory
 * @param channel pointer to a value to receive the com channel
 */
void GetCommChannel(uint8_t *channel)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    (*channel) = gCommCh;

    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Stores the discovery in thread safe global memory
 * @param discPeriod The discovery period
 */
void SetDiscPeriod(uint16_t discPeriod)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    gDiscPeriod = discPeriod;
    
    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Retrieves the discovery period from thread safe global memory
 * @param discPeriod pointer to a value to receive the discovery period.
 */
void GetDiscPeriod(uint16_t *discPeriod)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    (*discPeriod) = gDiscPeriod;
    
    xSemaphoreGive(gOrionConfigMutex);    
}

/**
 * @brief Stores the tone slot duration in thread safe global memory
 * @param toneSlotDur The tone slot duration.
 */
void SetToneSlotDuration(uint16_t toneSlotDur)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    gToneSlotDur = toneSlotDur;
    
    xSemaphoreGive(gOrionConfigMutex);    
}

/**
 * @brief Retrieves the tone slot duration from thread safe global memory
 * @param toneSlotDur pointer to a value to receive the tone slot duration
 */
void GetToneSlotDuration(uint16_t *toneSlotDur)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);
    
    (*toneSlotDur) = gToneSlotDur;
    
    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Stores the power slot duration in thread safe global memory
 * @param powerSlotDur The power slot duration.
 */
void SetPowerSlotDuration(uint16_t powerSlotDur)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    gPowerSlotDur = powerSlotDur;
    
    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Retrieves the power slot duration from thread safe global memory
 * @param powerSlotDur pointer to a value to receive the power slot duration
 */
void GetPowerSlotDuration(uint16_t *powerSlotDur)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);
    
    (*powerSlotDur) = gPowerSlotDur;
    
    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Stores the power slot count in thread safe global memory
 * @param powerSlotCnt The power slot count.
 */
void SetPowerSlotCount(uint8_t powerSlotCnt)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    gPowerSlotCnt = powerSlotCnt;
    
    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Retrieves the power slot count from thread safe global memory
 * @param powerSlotDur pointer to a value to receive the power slot duration
 */
void GetPowerSlotCount(uint8_t *powerSlotCnt)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    (*powerSlotCnt) = gPowerSlotCnt;
    
    xSemaphoreGive(gOrionConfigMutex);        
}

/**
 * @brief Stores the total tones in thread safe global memory
 * @param totalTone The total tones
 */
void SetTotalTones(uint8_t totalTones)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    gTotalTones = totalTones;
    
    xSemaphoreGive(gOrionConfigMutex);
}

/**
 * @brief Retrieves the total tones from thread safe global memory
 * @param totalTones pointer to a value to receive the total tones.
 */
void GetTotalTones(uint8_t *totalTones)
{
    xSemaphoreTake(gOrionConfigMutex, portMAX_DELAY);

    (*totalTones) = gTotalTones;
    
    xSemaphoreGive(gOrionConfigMutex);
}
