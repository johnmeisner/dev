/**
 * @file       ctrl_task.c
 *
 * @brief      The control task is the master control of the system. It processes the main state machine that 
 *             controls the operation of the transmitter. 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <string.h>
#include "stm32h7xx_hal.h"
#include "FreeRTOS.h"
#include "projdefs.h"
#include "timers.h"
#include "semphr.h"
#include "task.h"
#include "ctrl_task.h"
#include "state_machine.h"
#include "ctrl_state_machine.h"
#include "orion_config.h"
#include "uart_driver.h"
#include "error.h"
#include "proxy_msg_interface.h"
#include "client.h"
#include "client_interface.h"
#include "ctrl_interface.h"
#include "proxyHostMsgInf.h"
#include "cli_task.h"
#include "proxy.h"

// Semaphores
#define CTRL_CNT_SEM_MAX     10      ///< The maximum count that the gCtrlSem can iterate to.
#define CTRL_CNT_SEM_MIN     0       ///< The minimum count that the gCtrlSem can decrement to.

// Timers 
#define DISCOVERY_TIMER      8000    ///< time in milliseconds before the next discovery message is sent
#define JOIN_TIMER           10000   ///< time in milliseconds before the next join message is sent
#define TPC_TIMER            20000   ///< time in milliseconds before the next TPC message is sent
#define QUERY_TIMER          5000    ///< time in milliseconds before the next query message is sent
#define DISC_ACK_TIMER       20      ///< ack timers are used to tell the state machine to transition
#define JOIN_ACK_TIMER       10      ///< ack timers are used to tell the state machine to transition
#define TPC_ACK_TIMER        10      ///< ack timers are used to tell the state machine to transition
#define QUERY_ACK_TIMER      10      ///< ack timers are used to tell the state machine to transition
#define TIMER_BLOCK_MS       1       ///< time in milliseconds that the timer will block to start
#define NO_WAIT              0

// Misc. Defines

#define CTRL_STATE_DEBUG
#ifdef CTRL_STATE_DEBUG
// Debug messages 
#define ERROR_STATE_REACHED          "The control task has entered it's error state.\n\r"
#define DEBUG_STATE_REACHED          "The control task has entered it's debug state.\n\r"
#define INIT_STATE_REACHED           "The control task has entered it's initialization state.\n\r"
#define READY_STATE_REACHED          "The control task has entered it's ready state.\n\r"
#define TPS_STATE_REACHED            "The control task has entered it's charging state.\n\r"
#define DISC_STATE_REACHED           "The control task has entered it's disc state.\n\r"
#define JOIN_STATE_REACHED           "The control task has entered it's join state\n\r"
#define TPC_STATE_REACHED            "The control task has entered it's TPC state\n\r"
#define QUERY_STATE_REACHED          "The control task has entered it's query state.\n\r"
#define CALIBRATION_STATE_REACHED    "The control task has entered it's calibration state.\n\r"
#define SCHEDULE_STATE_REACHED       "The control task has entered it's schedule state.\n\r"
#define TIMEOUT                      "Timeout has occured!\n\r"
#define TIMER_START_ERROR            "Timer failed to start!\n\r"
#define TIMER_STOP_ERROR             "Timer failed to stop!\n\r"
#define CLIENT_DISCONNECT            "Client has disconnected!\n\r"
#define DISC_REC                     "Disc confirmation was received!\n\r"
#define JOIN_REC                     "Join confirmation was received!\n\r"
#define TPC_REC                      "Tpc confirmation was received!\n\r"
#define QUERY_CFN_REC                "Query confirmation was received!\n\r"
#define QUERY_DATA_REC               "Query data was received!\n\r"

#define PRINT_DEBUG(string)   CotaUartTransmit(string, strlen(string), HAL_MAX_DELAY);
#else
#define PRINT_DEBUG(string)   
#endif  // #ifdef CTRL_STATE_DEBUG

static BaseType_t CtrlTaskInit(void);
static void CtrlTask(void *pvParameters);
static void processProxyMessages(ctrlStates_t currState);
static void processCtrlCommands(ctrlStates_t currState);

/*
 * Timer callback functions 
 */
static void discTimerCallback(TimerHandle_t timer);
static void ackDiscTimerCallback(TimerHandle_t timer);

static void joinTimerCallback(TimerHandle_t timer);
static void ackJoinTimerCallback(TimerHandle_t timer);

static void tpcTimerCallback(TimerHandle_t timer);
static void ackTpcTimerCallback(TimerHandle_t  timer);

static void queryTimerCallback(TimerHandle_t timer);
static void ackQueryTimerCallback(TimerHandle_t timer);
                              
static TaskHandle_t   gCtrlTaskHandle      = NULL; ///< FreeRTOS task handle for the control task
static TimerHandle_t  gDiscTimerHandle     = NULL; ///< FreeRTOS timer handle for the discover timer
static TimerHandle_t  gDiscAckTimerHandle  = NULL; ///< FreeRTOS timer handle for the disc acknowledgment timer
static TimerHandle_t  gJoinTimerHandle     = NULL; ///< FreeRTOS timer handle for the join timer
static TimerHandle_t  gJoinAckTimerHandle  = NULL; ///< FreeRTOS timer handle for the join acknowledgment timer
static TimerHandle_t  gTpcTimerHandle      = NULL; ///< FreeRTOS timer handle for the tpc timer
static TimerHandle_t  gTpcAckTimerHandle   = NULL; ///< FreeRTOS timer handle for the tpc acknowledgment timer
static TimerHandle_t  gQueryTimerHandle    = NULL; ///< FreeRTOS timer handle for the query timer 
static TimerHandle_t  gQueryAckTimerHandle = NULL; ///< FreeRTOS timer handle for the query acknowledgment timer
static SemaphoreHandle_t    gCtrlSem       = NULL; ///< FreeRTOS semaphore handle that begins the control task
static StateMachineState_t* gCtrlSmState   = NULL; ///< Keeps track of the current state of the SM

/*
 * System Control Flags and State Machines
 */
static bool gDebugFlag        = false; ///< Set when the system should enter debug mode of operation. 
static bool gSystemInit       = false; ///< true when the control task has successfully initialized
static bool gNewScheduleReady = false; ///< true when the schedule is ready to be sent
static bool gSendDisc         = false; ///< true when a discovery message must be sent
static bool gDiscAckTimeout   = false; ///< true when an disc acknowledgment was not received before timeout
static bool gSendJoin         = false; ///< true when a join message must be sent
static bool gJoinAckTimeout   = false; ///< true when an join acknowledgment was not received before timeout
static bool gSendTpc          = false; ///< true when a tpc message must be sent
static bool gTpcAckTimeout    = false; ///< true when an tpc acknowledgment was not received before timeout
static bool gSendQuery        = false; ///< true when a query message must be sent
static bool gQueryAckTimeout  = false; ///< true when an query acknowledgment was not received before timeout
static bool gCalNeeded        = false; ///< true when the system must be calibrated
static bool gTpsDone          = false; ///< true when the system has finished TPS


/**
 * @brief Runs the system-level state machine for controlling what the transmitter is 
 *          doing at any given moment.
 * @param pvParameters  All FreeRTOS tasks must have a pvParameters pointer parameter 
 */
static void CtrlTask(void *pvParameters)
{
    while(CtrlTaskInit() != pdPASS);

    // We must run the first entry function now b/c RunStateMachine() will not.
    gCtrlSmState->entryFcn();
    gCtrlSmState = RunStateMachine(gCtrlSmState);
    for (;;)
    {
        /*
          This task has two major processes to it.
            1) Process for all inter-task communication
            2) Run through the state machine and run whatever
               processes that entails.

          Here, we process queue messages before running the state machine
          as the state machine's actions might change depending on what
          we get from a message. 
          
        */
        xSemaphoreTake(gCtrlSem, portMAX_DELAY);
        processProxyMessages((ctrlStates_t) gCtrlSmState->state);

        /*
          I was working on this code at home while I was snowed in and
          accidentally fried my development board... oops...

          I was in the middle of debugging this function while it was happening,
          and unfortunately never finished debugging. I need to migrate
          the code to the CCB, so I want to merge thus now acknowledging
          that I cannot finish my debugging.
        */
        // processCtrlCommands ((ctrlStates_t) gCtrlSmState->state);
        gCtrlSmState = RunStateMachine(gCtrlSmState);
    }
}

/**
 * @brief Creates the instance of the CtrlTask in FreeRTOS
 */
void CreateCtrlTask(void)
{
  BaseType_t xReturned;
  
   /*
  * TPS Task Creation
  */
  xReturned = xTaskCreate(
                  CtrlTask,               // Function that implements the task. 
                  "Control Task",         // Text name for the task. 
                  CTRL_TASK_STACK_SIZE,   // Stack size in bytes. 
                  NULL,                   // Parameter passed into the task. 
                  CTRL_TASK_PRIORITY,     // Priority at which the task is created. 
                  &gCtrlTaskHandle );     // Used to pass out the created task's handle. 
  
  while (xReturned != pdPASS);
    
}

/**
 * @brief Initializes all of the resources that are used by the CtrlTask
 * @return Returns pdPASS on success, and pdFAIL on failure. 
 */
static BaseType_t CtrlTaskInit(void)
{
    gCtrlSem = xSemaphoreCreateCounting(CTRL_CNT_SEM_MAX, CTRL_CNT_SEM_MIN);
    if (gCtrlSem == NULL)
        return pdFAIL;

    /* Software Timer Initialization */

    /*
      Disc Timer
     */
    gDiscTimerHandle = xTimerCreate(
        "Discovery Timer",
        pdMS_TO_TICKS(DISCOVERY_TIMER),
        pdFALSE,
        (void *) 0,
        discTimerCallback);

    if ( gDiscTimerHandle == NULL )
        return pdFAIL;

    gDiscAckTimerHandle = xTimerCreate(
        "Disc Acknowledgment Timer",
        pdMS_TO_TICKS(DISC_ACK_TIMER),
        pdFALSE,
        (void *) 0,
        ackDiscTimerCallback);

    if ( gDiscAckTimerHandle == NULL )
        return pdFAIL;

    /*
      Join Timer
     */
    gJoinTimerHandle = xTimerCreate(
        "Join Timer",
        pdMS_TO_TICKS(JOIN_TIMER),
        pdFALSE,
        (void *) 0,
        joinTimerCallback);
    
    if ( gJoinTimerHandle == NULL )
        return pdFAIL;


    gJoinAckTimerHandle = xTimerCreate(
        "Join Acknowledgment Timer",
        pdMS_TO_TICKS(JOIN_ACK_TIMER),
        pdFALSE,
        (void *) 0,
        ackJoinTimerCallback);

    if ( gJoinAckTimerHandle == NULL )
        return pdFAIL;
    
    /*
      TPC Timer
     */
    gTpcTimerHandle = xTimerCreate(
        "Tpc Timer",
        pdMS_TO_TICKS(TPC_TIMER),
        pdFALSE,
        (void *) 0,
        tpcTimerCallback);
    
    if ( gTpcTimerHandle == NULL )
        return pdFAIL;


    gTpcAckTimerHandle = xTimerCreate(
        "Tpc Acknowledgment Timer",
        pdMS_TO_TICKS(TPC_ACK_TIMER),
        pdFALSE,
        (void *) 0,
        ackTpcTimerCallback);

    if ( gTpcAckTimerHandle == NULL )
        return pdFAIL;


    /*
      Query Timer
     */    
    gQueryTimerHandle = xTimerCreate(
        "Query Timer",
        pdMS_TO_TICKS(QUERY_TIMER),
        pdFALSE,
        (void *) 0,
        queryTimerCallback);

    if ( gQueryTimerHandle == NULL )
        return pdFAIL;
    
    gQueryAckTimerHandle = xTimerCreate(
        "Query Acknowledgment Timer",
        pdMS_TO_TICKS(QUERY_ACK_TIMER),
        pdFALSE,
        (void *) 0,
        ackQueryTimerCallback);

    if ( gQueryAckTimerHandle == NULL )
        return pdFAIL;


    // State machine initialization 
    gCtrlSmState = CtrlSmInit();
    if ( gCtrlSmState == NULL )
        return pdFAIL;

    return pdPASS;
}

/**
 * @brief This function will process all queue messages (AKA inter-task communication).
 *        The types of messages we process will differ depending on the current state
 *        of the control state machine.
 * @param currState The current state of the control task state machine #ctrlStates_t
 */
static void processProxyMessages(ctrlStates_t currState)
{
    uint8_t buf[CTRL_TASK_MAX_MSG_SIZE];

    if (PrxMsgRdy())
    {
        switch (PrxGetMsg(buf, CTRL_TASK_MAX_MSG_SIZE))
        {
        case(PRX_HOST_CLIENT_DISCOVERED):
        {
            /// @todo remove Start/Stop discovery behavior in the proxy. Change to just SendDisc and timeout
            PrxHostDiscoveryMsg_t discResult;
            PrxExtractDiscoveryResp(buf, CTRL_TASK_MAX_MSG_SIZE, &discResult);
            CmClientDiscovered(discResult);

            // Used for debugging before cli and rpi commands are implemented
            POST_COTA_ERROR(CmRegisterClient((ExtdAddr_t) {0xFE, 0x00, 0xBA, 0x07, 0x00, 0x4B, 0x12, 0x00}));
        }
        break;
        
        case(PRX_HOST_CLIENT_JOINED):
        {
            PrxHostClientJoinRspMsg_t joinResult;
            PrxExtractJoinResp(buf, CTRL_TASK_MAX_MSG_SIZE, &joinResult);
            CmClientJoined(joinResult);
            CmTpcClientList();
            PRINT_DEBUG(JOIN_REC);
        }
        break;
        
        case(PRX_HOST_TPC_CFN):
        {
            PrxHostTpcCfn_t tpcResult;
            PrxExtractTpcResp(buf, CTRL_TASK_MAX_MSG_SIZE, &tpcResult);
            CmClientTpcCfn(tpcResult);
            PRINT_DEBUG(TPC_REC);
            CmRdyClientList();
        }
        break;
        
        case(PRX_HOST_CLIENT_QUERY_CFN):
        {
            PrxHostCqRecCfn_t queryCfn;
            PrxExtractQueryCfnResp(buf, CTRL_TASK_MAX_MSG_SIZE, &queryCfn);
            PRINT_DEBUG(QUERY_CFN_REC);
        }
        break;
        case(PRX_HOST_CLIENT_QUERY_DATA):
        {
            ClientQueryDataStd_t queryData;
            PrxExtractQueryDataResp(buf, CTRL_TASK_MAX_MSG_SIZE, &queryData);
            CmClientQueryData(queryData);
            PRINT_DEBUG(QUERY_DATA_REC);
            CmRdyClientList();
        }   
        case(PRX_HOST_DISCONNECT_CFN):
        {
            PrxHostLeaveNetworkCfn_t disconnectResult;
            PrxExtractDisconnectResp(buf, CTRL_TASK_MAX_MSG_SIZE, &disconnectResult);
            PRINT_DEBUG(CLIENT_DISCONNECT);
        }
        break;
        
        case(PRX_HOST_PROXY_DATA):
        {
            PrxExtractPrxCmdResp(buf, sizeof(buf));
        }
        break;
        
        default:
            // This shouldn't happen, indicates an error in the system
            POST_COTA_ERROR(COTA_ERROR_FAILED_TO_PROXY_MSG);
            break;
        }
    }
}

/**
 * @brief This function is called when the discovery timer finishes. It will set the
 *        #gSendDisc flag and post to the ctrl semaphore that it is time to send
 *        a discovery.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void discTimerCallback(TimerHandle_t timer)
{
    uint32_t callbackCnt;
    static BaseType_t wasHigherPriorityTaskWoken;

    wasHigherPriorityTaskWoken = pdFALSE;

    // Increment the timer count, which is the number of time the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);

    // Indicates to the state machine that a discovery message must be sent when appropriate
    gSendDisc = true;

    xSemaphoreGiveFromISR(gCtrlSem, &wasHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( wasHigherPriorityTaskWoken );
}

/**
 * @brief This function is called when the join timer finishes. It will set the
 *        #gSendJoin flag and post to the ctrl semaphore that it is time to send
 *        a join message.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void joinTimerCallback(TimerHandle_t timer)
{
    uint32_t callbackCnt;
    static BaseType_t wasHigherPriorityTaskWoken;

    wasHigherPriorityTaskWoken = pdFALSE;
        
    // Increment the timer count, which is the number of time the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);

    // Indicates to the state machine that a join message must be sent when appropiate
    gSendJoin = true;

    xSemaphoreGiveFromISR(gCtrlSem, &wasHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( wasHigherPriorityTaskWoken );
}


/**
 * @brief This function is called when the tpc timer finishes. It will set the
 *        #gSendTpc flag and post to the ctrl semaphore that it is time to send
 *        a tpc message.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void tpcTimerCallback(TimerHandle_t timer)
{
    uint32_t callbackCnt;
    static BaseType_t wasHigherPriorityTaskWoken;

    wasHigherPriorityTaskWoken = pdFALSE;
        
    // Increment the timer count, which is the number of time the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);

    // Indicates to the state machine that a tpc message must be sent when appropiate
    gSendTpc = true;

    xSemaphoreGiveFromISR(gCtrlSem, &wasHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( wasHigherPriorityTaskWoken );
}



/**
 * @brief This function is called when the query timer finishes. It will set the
 *        #gSendQuery flag and post to the ctrl semaphore that it is time to send
 *        a query.
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void queryTimerCallback(TimerHandle_t timer)
{
    uint32_t callbackCnt;
    static BaseType_t wasHigherPriorityTaskWoken;

    wasHigherPriorityTaskWoken = pdFALSE;
    
    // Increment the timer count, which is the number of time the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);

    // Indicates to the state machine that a query message must be sent when appropiate
    gSendQuery = true;
    
    xSemaphoreGiveFromISR(gCtrlSem, &wasHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( wasHigherPriorityTaskWoken );
}

/**
 * @brief The discovery acknowledgment timer is called after a discovery message is sent.
 *        If an acknowledgement message is not received before this timer expires, then
 *        the discovery message has timed out. 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void ackDiscTimerCallback(TimerHandle_t  timer)
{
    uint32_t callbackCnt;
    static BaseType_t wasHigherPriorityTaskWoken;
    
    wasHigherPriorityTaskWoken = pdFALSE;
    
    // Increment the timer count, which is the number of time the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);

    // Indicates to the state machine that a acknowledgment message must be sent when appropiate
    gDiscAckTimeout = true;
    PRINT_DEBUG(TIMEOUT);
    xSemaphoreGiveFromISR(gCtrlSem, &wasHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( wasHigherPriorityTaskWoken );
}

/**
 * @brief The join acknowledgment timer is called after a join message is sent.
 *        If an acknowledgement message is not received before this timer expires, then
 *        the join message has timed out. 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void ackJoinTimerCallback(TimerHandle_t  timer)
{
    uint32_t callbackCnt;
    static BaseType_t wasHigherPriorityTaskWoken;
    
    wasHigherPriorityTaskWoken = pdFALSE;
    
    // Increment the timer count, which is the number of time the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);

    // Indicates to the state machine that a acknowledgment message must be sent when appropriate
    gJoinAckTimeout = true;
    PRINT_DEBUG(TIMEOUT);
    xSemaphoreGiveFromISR(gCtrlSem, &wasHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( wasHigherPriorityTaskWoken );
}


/**
 * @brief The tpc acknowledgment timer is called after a tpc message is sent.
 *        If an acknowledgement message is not received before this timer expires, then
 *        the tpc message has timed out. 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void ackTpcTimerCallback(TimerHandle_t  timer)
{
    uint32_t callbackCnt;
    static BaseType_t wasHigherPriorityTaskWoken;
    
    wasHigherPriorityTaskWoken = pdFALSE;
    
    // Increment the timer count, which is the number of time the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);

    // Indicates to the state machine that a acknowledgment message must be sent when appropriate
    gTpcAckTimeout = true;
    PRINT_DEBUG(TIMEOUT);
    xSemaphoreGiveFromISR(gCtrlSem, &wasHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( wasHigherPriorityTaskWoken );
}

/**
 * @brief The query acknowledgment timer is called after a query message is sent.
 *        If an acknowledgement message is not received before this timer expires, then
 *        the query message has timed out. 
 * @param timer The timer handle allows the callback to get information on the timer
 */
static void ackQueryTimerCallback(TimerHandle_t  timer)
{
    uint32_t callbackCnt;
    static BaseType_t wasHigherPriorityTaskWoken;
    
    wasHigherPriorityTaskWoken = pdFALSE;
    
    // Increment the timer count, which is the number of time the timer expired.
    callbackCnt = (uint32_t) pvTimerGetTimerID(timer);
    callbackCnt++;
    vTimerSetTimerID(timer, (void *) callbackCnt);

    // Indicates to the state machine that a acknowledgment message must be sent when appropriate
    gQueryAckTimeout = true;
    PRINT_DEBUG(TIMEOUT);
    xSemaphoreGiveFromISR(gCtrlSem, &wasHigherPriorityTaskWoken);
    portYIELD_FROM_ISR( wasHigherPriorityTaskWoken );
}

/*
 * State machine definitions - Entries
 */
void ErrorEntry(void)
{
    PRINT_DEBUG(ERROR_STATE_REACHED);
}

void DebugEntry(void)
{
    PRINT_DEBUG(DEBUG_STATE_REACHED);
}

void InitEntry(void)
{
    PRINT_DEBUG(INIT_STATE_REACHED);
}

void ReadyEntry(void)
{
    PRINT_DEBUG(READY_STATE_REACHED);
}

void TpsEntry(void)
{
    PRINT_DEBUG(TPS_STATE_REACHED);
}

void DiscEntry (void)
{
    BaseType_t retVal;
    PrxSendDisc();

    // Begin the disc timer
    gDiscAckTimeout = false;
    retVal = xTimerStart( gDiscAckTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS));
    if ( retVal != pdPASS )
    {
        PRINT_DEBUG(TIMER_START_ERROR);
    }
    
    PRINT_DEBUG(DISC_STATE_REACHED);
}

void JoinEntry  (void)
{
    ExtdAddr_t longId;
    ShrtAddr_t shortId;
    BaseType_t retVal;

    if (CmGetJoinClient(&longId, &shortId))
    {
        PrxSendJoin(longId, shortId);
    }
    
    // Begin the join timer
    gJoinAckTimeout = false;
    retVal = xTimerStart( gJoinAckTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS));
    if ( retVal != pdPASS )
    {
        PRINT_DEBUG(TIMER_START_ERROR);
    }
    
    PRINT_DEBUG(JOIN_STATE_REACHED);
}

void TpcEntry  (void)
{
    ShrtAddr_t shortId;
    BaseType_t retVal;

    if (CmGetTpcClient(&shortId))
    {
        PrxSendTpc(shortId);
    }
    
    // Begin the tpc timer
    gTpcAckTimeout = false;
    retVal = xTimerStart( gTpcAckTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS));
    if ( retVal != pdPASS )
    {
        PRINT_DEBUG(TIMER_START_ERROR);
    }
    
    PRINT_DEBUG(TPC_STATE_REACHED);
}

void QueryEntry (void)
{
    /// @todo Send Query message
    ShrtAddr_t shortId; 
    BaseType_t retVal;
    
    if (CmGetRdyClient(&shortId))
    {
        PrxSendQuery(shortId);
    }
    
    // Begin the query timer
    gQueryAckTimeout = false;
    retVal = xTimerStart( gQueryAckTimerHandle, pdMS_TO_TICKS(TIMER_BLOCK_MS) );
    if ( retVal != pdPASS )
    {
       PRINT_DEBUG(TIMER_START_ERROR);
    }
    
    PRINT_DEBUG(QUERY_STATE_REACHED);
}

void CalEntry   (void)
{
    PRINT_DEBUG(CALIBRATION_STATE_REACHED);
}

void SchedEntry (void)
{
    PRINT_DEBUG(SCHEDULE_STATE_REACHED);
}

/*
 * State machine definitions - Entries
 */
bool InitToRdyCheck   (void)
{
    gSystemInit = true;
    
    // Start Communication timers 
    gSystemInit = (gSystemInit == true ) ? ( xTimerStart( gDiscTimerHandle, NO_WAIT ) == pdPASS ) : false;
    gSystemInit = (gSystemInit == true ) ? ( xTimerStart( gJoinTimerHandle, NO_WAIT ) == pdPASS ) : false;
    gSystemInit = (gSystemInit == true ) ? ( xTimerStart( gTpcTimerHandle, NO_WAIT ) == pdPASS ) : false;
    gSystemInit = (gSystemInit == true ) ? ( xTimerStart( gQueryTimerHandle, NO_WAIT ) == pdPASS ): false;

    if(gSystemInit)
    {
        CotaUartTransmit("CtrlTask Init Success, Timers started\r\n", 0, HAL_MAX_DELAY);
    }
    else
    {
        CotaUartTransmit("CtrlTask Init Failure, Timers not started!\r\n", 0, HAL_MAX_DELAY);
    }
        
    
    return gSystemInit;
}

/*
 * State machine definitions - Check conditions
 */
bool InitToDebugCheck (void){ return gDebugFlag; }
bool DebugToInitCheck (void){ return !gDebugFlag; }
bool RdyToDebugCheck  (void){ return gDebugFlag; }

bool RdyToTpsCheck    (void){ return gNewScheduleReady; }

bool RdyToDiscCheck   (void){ return gSendDisc; }
bool RdyToJoinCheck   (void){ return gSendJoin; }
bool RdyToTpcCheck    (void){ return gSendTpc; }
bool RdyToQueryCheck  (void){ return gSendQuery; }
bool RdyToCalCheck    (void){ return gCalNeeded; /* Check for calibration based on Temp and Time*/ }
bool RdyToSchedCheck  (void){ return !gNewScheduleReady /*&& !scheduleListIsEmpty()*/; }
bool TpsToRdyCheck    (void){ return gTpsDone; }

bool DiscToRdyCheck   (void){ return !gSendDisc  | gDiscAckTimeout; }
bool JoinToRdyCheck   (void){ return !gSendJoin  | gJoinAckTimeout; }
bool TpcToRdyCheck    (void){ return !gSendTpc   | gTpcAckTimeout; }
bool QueryToRdyCheck  (void){ return !gSendQuery | gQueryAckTimeout; }
bool CalToRdyCheck    (void){ return gCalNeeded; }
bool SchedToRdyCheck  (void){ return gNewScheduleReady; }

/*
 * State machine definitions - Exits
 */
void DiscToRdyExit   (void)
{
    BaseType_t retVal;
    gSendDisc = false;
    
    retVal = xTimerStop( gDiscAckTimerHandle, NO_WAIT );
    if ( retVal != pdPASS )
    {
       PRINT_DEBUG(TIMER_STOP_ERROR);
    }
    retVal = xTimerStart( gDiscTimerHandle, NO_WAIT );
    if ( retVal != pdPASS )
    {
       PRINT_DEBUG(TIMER_START_ERROR);
    }
}

void JoinToRdyExit   (void)
{
    BaseType_t retVal;
    gSendJoin = false;
    
    retVal = xTimerStop( gJoinAckTimerHandle, NO_WAIT );
    if ( retVal != pdPASS )
    {
       PRINT_DEBUG(TIMER_STOP_ERROR);
    }
    retVal = xTimerStart( gJoinTimerHandle, NO_WAIT );
    if ( retVal != pdPASS )
    {
       PRINT_DEBUG(TIMER_START_ERROR);
    }
}

void TpcToRdyExit   (void)
{
    BaseType_t retVal;
    gSendTpc = false;
    
    retVal = xTimerStop( gTpcAckTimerHandle, NO_WAIT );
    if ( retVal != pdPASS )
    {
       PRINT_DEBUG(TIMER_STOP_ERROR);
    }
    retVal = xTimerStart( gTpcTimerHandle, NO_WAIT );
    if ( retVal != pdPASS )
    {
       PRINT_DEBUG(TIMER_START_ERROR);
    }
}

void QueryToRdyExit  (void)
{
    BaseType_t retVal;
    gSendQuery = false;
    
    retVal = xTimerStop( gQueryAckTimerHandle, NO_WAIT );
    if ( retVal != pdPASS )
    {
       PRINT_DEBUG(TIMER_STOP_ERROR);
    }
    retVal = xTimerStart( gQueryTimerHandle, NO_WAIT );
    if ( retVal != pdPASS )
    {
       PRINT_DEBUG(TIMER_START_ERROR);
    }
}

/**
 * @brief This function will process all client queue messages from CLI (AKA inter-task communication).
 *        The types of messages we process will differ depending on the current state
 *        of the control state machine.
 *
 *        This function currently has a bug in it and it could not be resolved before merge.
 *        This is because I am working from home and broke me development board (oops)
 *
 * @param currState The current state of the control task state machine #ctrlStates_t
 */
static void processCtrlCommands(ctrlStates_t currState)
{
    uint8_t buf[CTRL_TASK_MAX_CMD_SIZE];
    uint8_t size;
    
    if (gCtrlSmState->state != CTRL_STATE_TPS)
    {
        size = CtrlGetMsg(buf, sizeof(buf));
        if (size > 0)
        {
            switch((CtrlMsgId_t) buf[0])
            {
            case(CTRL_REGISTER_CLIENT):
            {
                regClient_t *reg;
                cotaError_t error;
                reg = (regClient_t *) &buf[1];

                if ((error = CmRegisterClient(reg->longId)) == COTA_ERROR_NONE)
                {
                    PRINT_DEBUG("SUCCESS\r\n");
                }
                else
                {
                    POST_COTA_ERROR(error);
                }
            }    
            break;
            
            case(CTRL_REMOVE_CLIENT):
            {
                rmClient_t *rm;
                cotaError_t error;
                rm = (rmClient_t *) &buf[1];

                if ( (error = CmRemoveClient(rm->longId)) == COTA_ERROR_NONE)
                {
                    PRINT_DEBUG("SUCCESS\r\n");
                }
                else
                {
                    POST_COTA_ERROR(error);
                }
            }   
            break;
            
            case(CTRL_CLIENT_LIST_DISC):
            {
                CmDiscClientList();
            }
            break;
            
            case(CTRL_CLIENT_LIST_JOIN):
            {
                CmJoinClientList();                
            }
            break;
            
            case(CTRL_CLIENT_LIST_TPC):
            {
                CmTpcClientList();
            }
            break;
            
            case(CTRL_CLIENT_LIST_RDY):
            {
                CmRdyClientList();
            }
            break;
            
            /* case HOST_PRX_PROXY_CMD: */
            /*     ProxySendMessage(buf, size); */
            /*     break; */
            default:
                
                break;
            }
        }     
    }
    else
    {
        // Need to post to semaphore again after a delay?
        // Not sure of a architecture decision here. 
    }
}
