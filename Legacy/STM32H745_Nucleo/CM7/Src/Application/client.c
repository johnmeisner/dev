/**
* @file       client.c
*
* @brief      Defines helper functions for working with clients. 
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*
* @note       A client has two methods of identification, a longId (#ExtdAddr_t) and 
*             a shortId (#ShrtAddr_t). 
*               - A longId is assigned when the client is manufactured, and is given 
*                   in accordance with the EUI-64 IP standard, and is assigned through
*                   IEEE.
*               - A shortId is assigned by the transmitter when the client is registered 
*                   with the system. Having a shortId is important because it reduces 
*                   network traffic, and is just overall easier to work with.
*
*/

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "orion_config.h"
#include "CotaCommonTypes.h"
#include "client.h"

#define INVALID_SHORT_ID_0        0      ///< The lower bound for shortIds. This is considered invalid.
#define INVALID_SHORT_ID_FFFF     0xFFFF ///< The upper bound for shortIds. This is considered invalid.

/**
 * @brief Determines if the longId is valid I.e non-zero
 * @param longId The longId that will be examined
 * @return true if the longId is valid, false if otherwise.
 */
bool IsLongValid(ExtdAddr_t longId)
{
    bool retVal;
    uint8_t i;

    retVal = false;
    
    for ( i = 0; i < sizeof(ExtdAddr_t); i++ )
    {
        // An invalid longId is all 0s
        if ( longId.bytes[i] != 0 )
        {
            retVal = true;
            break;
        }
    }

    return retVal;
}

/**
 * @brief compares two longIds and compares the values
 * @param longId1 The first longId for comparison
 * @param longId2 The second longId for comparison
 * @return 0 if equal, < 0 if longId1 < longId2, and 0 > if longId1 > longId2
 */
int LongCmp(ExtdAddr_t longId1, ExtdAddr_t longId2)
{
    return memcmp((void * ) &longId1, (void * ) &longId2, sizeof(ExtdAddr_t));
}

/**
 * @brief determines if the shortId is valid
 * @param shortId The shortId we are checking for validity
 * @return true if the shortId is valid, not true if otherwise
 */
bool IsShortValid(ShrtAddr_t shortId)
{
    return ( shortId != INVALID_SHORT_ID_0 ) && ( shortId != INVALID_SHORT_ID_FFFF );
}
