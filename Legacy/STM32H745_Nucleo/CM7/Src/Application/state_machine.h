/**
 * @file       state_machine.h
 *
 * @brief      State machine header file
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 *
 * @section README
 *
 * This module describes a state machine abstraction. All information for progressing
 *  through the state machine is held within statically defined struct of type
 *  #StateMachineState_t. Each instance of #StateMachineState_t contains an array of
 *  possible transition options as defined in #StateMachineTransition_t.
 *
 * The code ends up looking like the following:
 *
 * @code{.c}
 *    // In state0
 *    entryFcn();
 *
 *    if (conditionFcn1()){
 *        exitFcn1();
 *        nextState = STATE1;
 *    }
 *    if (conditionFcn2()){
 *        exitFcn2();
 *        state = nextState;
 *    }
 *    if (conditionFcn3()){
 *        exitFcn3();
 *        state = nextState;
 *    }
 *
 *    // ... etc.
 * @endcode
 *
 * This behavior described above is expected given the static state
 *  declarations shown below:
 *
 * @code{.c}
 *    static StateMachineState_t state1 = {
 *      .state             = STATE0,
 *      .entryFcn          = State0,
 *      .stateName         = "State 0 of the SM",
 *      .transitionOptions = State0TxnOptions
 *    };
 *    
 *    static StateMachineTransition_t State0TxnOptions[] = {
 *      {
 *        .checkCondition = conditionFcn1,
 *        .exitFcn        = exitFcn1,
 *        .nextState      = &STATE1
 *      },
 *      {
 *        .checkCondition = conditionFcn2,
 *        .exitFcn        = exitFcn2,
 *        .nextState      = &STATE2
 *      },
 *      {
 *        .checkCondition = conditionFcn3,
 *        .exitFcn        = exitFcn3,
 *        .nextState      = &STATE3
 *      },
 *      NULL
 *    };
 * @endcode
 *
 * The state machine is based of the static declarations, then uses RunStateMachine() to
 *  execute on the state machine. 
 */

#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_

/**
 * Null structure for terminating an array of state transition options #StateMachineTransition_t 
 */
#define NULL_TRANSITION_STRUCT          \
    {                                   \
     .checkCondition = NULL,            \
     .exitFcn        = NULL,            \
     .nextState      = NULL,            \
    },                                  \

#include "state_machine.h"
#include <stdbool.h>

typedef int State_Id_t;
typedef struct _StateMachineState_t StateMachineState_t;
typedef struct _StateMachineTransition_t StateMachineTransition_t;

typedef void (*EntryFcn_t)(void);
typedef bool (*CheckFcn_t)(void);
typedef void (*ExitFcn_t) (void);

struct _StateMachineTransition_t
{
  CheckFcn_t           checkCondition;  ///< Condition check function for transition
  ExitFcn_t            exitFcn;         ///< Exit function
  StateMachineState_t* nextState;       ///< next state machine state after transition
};

struct _StateMachineState_t
{
  State_Id_t                state;              ///< State ID
  EntryFcn_t                entryFcn;           ///< State entry function
  char*                     stateName;          ///< State name string
  StateMachineTransition_t* transitionOptions;  ///< State transition options
};

StateMachineState_t* RunStateMachine(StateMachineState_t* currState);

#endif   // #ifndef _STATE_MACHINE_H_
