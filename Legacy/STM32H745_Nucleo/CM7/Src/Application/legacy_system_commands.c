/**
 * @file       legacy_system_commands.c
 *
 * @brief      The same as system_commands.c, but contains the backwards compatibility commands
 *             for Venus support in Orion. 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifdef VENUS_BW_COMPATIBILITY

/*******************************************************************************************
 * Includes
 *******************************************************************************************/
#include "cli_task.h"
#include "main.h"
#include "string.h"
#include "cmsis_os.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "stm32h7xx_hal.h"
#include "uart_driver.h"
#include "amb_control.h"
#include "legacy_system_commands.h"

/*******************************************************************************************
 * Defines 
 ******************************************************************************************/

/*******************************************************************************************
 * Typedefs
 ******************************************************************************************/


static BaseType_t AmbOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AmbOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t UvpOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t UvpOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t PuRaiseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t PuDropCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t TxOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t TxOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RxOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RxOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SelectUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSelectedUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetPdDoneCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AmbSpiEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AmbSpiDisCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t CalibrateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t AddClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RegisterClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RemoveClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t IdentifyClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientFwUpdateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientFwUpdateRespCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientSleepCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientLeaveCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetGoodChannelsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetComChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RunCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t PauseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RebootCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ResetCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ResetFpgaCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ResetProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t CheckProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);

static BaseType_t ProxyCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ProxyCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t RestartCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ShutdownCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SendCqtCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SendDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SendTpcCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SendTpsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StopDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t InfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetGoodChannelsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetComChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetTxFreqCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ChannelInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ProxyInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t VersionsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientListCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientDetailCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ClientConfigCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t DevicesInRangeCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t DevicesStatusCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StartChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StopChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetChargingInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetChargerIdCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetMotionEngineCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetMotionEngineCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t MotionDevicesCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSystemTempCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t GetSystemStateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StatusCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t IdentifyChargerCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StartSendingDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t StopSendingDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t NormalizePhaseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t DebugLogCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetLedCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetSpiSingleCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetAmbEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetUvpEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetUvpTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t SetUvpRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);
static BaseType_t ReadUvpPdCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString);

/** This is an array of CLI_Command_Definition_t used to initialize the command CLI list
 *
 *  This is used in StartCliTask()
 * 
 * @todo optimize to move this to flash, and keep it there.  
 * @todo if memory optimizatoin is need, consider reducin/eliminating the about command verbage or generate the leading cmd name & arg names on the fly???  
 *
 */
static const  CLI_Command_Definition_t  gCliCmds[] = 
{
    {"amb_on",                  "\r\namb_on                 [AMB Number | all] Turn on specified AMB or all",  AmbOnCmdCallback, 1},
    {"amb_off",                 "\r\namb_off                [AMB Number | all] Turn off specified AMB or all",  AmbOffCmdCallback, 1},
    {"uvp_on",                  "\r\nuvp_on                 [AMB Number | all] Turn on UVP's on a specified AMB or all",  UvpOnCmdCallback, 1},
    {"uvp_off",                 "\r\nuvp_off                [AMB Number | all] Turn off UVP's on a specified AMB or all",  UvpOffCmdCallback, 1},
    {"pu_raise",                "\r\npu_raise               [AMB Number | all] Raise specified AMB's phase update pin or all",  PuRaiseCmdCallback, 1},
    {"pu_drop",                 "\r\npu_drop                [AMB Number | all] Drop specified AMB's phase update pin or all",  PuDropCmdCallback, 1},
    {"tx_on",                   "\r\ntx_on                  [] Turn on the TX pin",  TxOnCmdCallback, 0},
    {"tx_off",                  "\r\ntx_off                 [] Turn off the TX pin",  TxOffCmdCallback, 0},
    {"get_tx",                  "\r\nget_tx                 [] Gets the TX pin state",  GetTxCmdCallback, 0},
    {"rx_on",                   "\r\nrx_on                  [] Turn on the RX pin",  RxOnCmdCallback, 0},
    {"rx_off",                  "\r\nrx_off                 [] Turn off the RX pin",  RxOffCmdCallback, 0},
    {"get_rx",                  "\r\nget_rx                 [] Gets the RX pin state",  GetRxCmdCallback, 0},
    {"amb_spi_en",              "\r\namb_spi_en             [AMB Number 0-3 | all] Enable SPI transfers for the specified AMB or all",  AmbSpiEnCmdCallback, 1},
    {"amb_spi_dis",             "\r\namb_spi_dis            [AMB Number 0-3 | all] Disable SPI transfers for the specified AMB or all",  AmbSpiDisCmdCallback, 1},
    {"select_uvp",              "\r\nselect_uvp             [UVP Number 0-15 | all] Selects UVP's to interact with",  SelectUvpCmdCallback, 1},
    {"get_selected_uvp",        "\r\nget_select_uvp         [] Get the selected UVP number",  GetSelectedUvpCmdCallback, 0},
    {"get_pd_done",             "\r\nget_pd_done            [] Get the state of the phase detect pins for every AMB",  GetPdDoneCmdCallback, 0},
    {"calibrate",               "\r\ncalibrate              [] Calibrate the array only if the array is already on",  CalibrateCmdCallback, 1},
    {"add_client",              "\r\nadd_client             [Client ID] Add a new client to the client list",  AddClientCmdCallback, 0},
    {"register_client",         "\r\nregister_client        [Client ID][QueryType][Priority] Register a new client with the system. QueryType and Priority are optional.",  RegisterClientCmdCallback, 1},
    {"remove_client",           "\r\nremove_client          [Client ID] Remove a client from the client list",  RemoveClientCmdCallback, 3},
    {"identify_client",         "\r\nidentify_client        [Client ID][Duration] Identify the client (blink client's LED)",  IdentifyClientCmdCallback, 1},
    {"client_command",          "\r\nclient_command         [Client ID][Data] Send a generic command to a client (or all clients)",  ClientCommandCmdCallback, 2},
    {"client_command_data",     "\r\nclient_command_data    [Client ID] Get client command data",  ClientCommandDataCmdCallback, 2},
    {"client_fw_update",        "\r\nclient_fw_update       [Client ID][Data] Send a fw update packet to a client (or all clients)",  ClientFwUpdateCmdCallback, 1},
    {"client_fw_update_resp",   "\r\nclient_fw_update_resp  [Client ID] Get fw update response from a specific client",  ClientFwUpdateRespCmdCallback, 2},
    {"client_sleep",            "\r\nclient_sleep           [Client ID | all] Command a client (or all) to enter deep sleep mode",  ClientSleepCmdCallback, 1},
    {"client_leave",            "\r\nclient_leave           [Client ID | all] Command a client (or all) to leave the network",  ClientLeaveCmdCallback, 1},
    {"set_good_channels",       "\r\nset_good_channels      [0x0-0xFFFFFFFF] Set good channels to use as default settings",  SetGoodChannelsCmdCallback, 1},
    {"set_com_channel",         "\r\nset_com_channel        [24-26] Set COM channel for the system",  SetComChannelCmdCallback, 1},
    {"run",                     "\r\nrun                    [] Start running the Power Cycle automatically",  RunCmdCallback, 1},
    {"pause",                   "\r\npause                  [] Pause the Power Cycle and enter debug mode",  PauseCmdCallback, 0},
    {"reboot",                  "\r\nreboot                 [] Reboot the system (graceful channels shutdown and reboot)",  RebootCmdCallback, 0},
    {"reset",                   "\r\nreset                  [] Reset the Array and reload the configuration",  ResetCmdCallback, 0},
    {"reset_FPGA",              "\r\nreset_FPGA             [] Reset the FPGA (graceful channels shutdown then FPGA reset)",  ResetFpgaCmdCallback, 0},
    {"reset_proxy",             "\r\nreset_proxy            [] Reset the Proxy and resend a discovery message",  ResetProxyCmdCallback, 0},
    {"check_proxy",             "\r\ncheck_proxy            [] Check if the proxy is responding",  CheckProxyCmdCallback, 0},
    {"proxy_command",           "\r\nproxy_command          [Data] Send a generic command to the proxy",  ProxyCommandCmdCallback, 0},
    {"proxy_command_data",      "\r\nproxy_command_data     [] Get proxy command data",  ProxyCommandDataCmdCallback, 1},
    {"restart",                 "\r\nrestart                [Client ID | all] Restart the Cota Daemon (optionally tell clients to leave and rejoin)",  RestartCmdCallback, 0},
    {"shutdown",                "\r\nshutdown               [Client ID | all] Shutdown the system (optionally put clients to sleep as well)",  ShutdownCmdCallback, 1},
    {"send_cqt",                "\r\nsend_cqt               [] Send a one shot CQT.",  SendCqtCmdCallback, 1},
    {"send_disc",               "\r\nsend_disc              [Channel] Send a client discovery message. Channel is optional",  SendDiscCmdCallback, 0},
    {"send_tpc",                "\r\nsend_tpc               [Client ID] Send a one shot TPC(Tone Power Config)",  SendTpcCmdCallback, 1},
    {"send_tps",                "\r\nsend_tps               [] Send a one shot TPS",  SendTpsCmdCallback, 1},
    {"stop_disc",               "\r\nstop_disc              [] Command the Proxy to stop discovering clients",  StopDiscCmdCallback, 0},
    {"info",                    "\r\ninfo                   [] Get info of all channels in the system",  InfoCmdCallback, 0},
    {"get_good_channels",       "\r\nget_good_channels      [] Get good channel settings",  GetGoodChannelsCmdCallback, 0},
    {"get_com_channel",         "\r\nget_com_channel        [] Get current COM channel",  GetComChannelCmdCallback, 0},
    {"get_tx_freq",             "\r\nget_tx_freq            [] Get current Tx frequency setting",  GetTxFreqCmdCallback, 0},
    {"channel_info",            "\r\nchannel_info           [Channel (0-31)] Get specified channel information",  ChannelInfoCmdCallback, 0},
    {"proxy_info",              "\r\nproxy_info             [] Get Proxy information",  ProxyInfoCmdCallback, 1},
    {"versions",                "\r\nversions               [] Get FPGA, Driver, Daemon, OS, and build versions",  VersionsCmdCallback, 0},
    {"client_list",             "\r\nclient_list            [Client Status] Get a list of all clients. Client status is optional",  ClientListCmdCallback, 0},
    {"client_detail",           "\r\nclient_detail          [Client ID] Get detailed information for a specific client",  ClientDetailCmdCallback, 1},
    {"client_data",             "\r\nclient_data            [Client ID] Get custom data for a specific client",  ClientDataCmdCallback, 1},
    {"client_config",           "\r\nclient_config          [Client ID][QueryType(5=STD, 6=EXT)] Set configuration for a specific client",  ClientConfigCmdCallback, 1},
    {"devices_in_range",        "\r\ndevices_in_range       [] Get a list of all devices (clients)",  DevicesInRangeCmdCallback, 2},
    {"devices_status",          "\r\ndevices_status         [Client ID] Get devices (clients) status",  DevicesStatusCmdCallback, 0},
    {"start_charging",          "\r\nstart_charging         [Client ID | all] Start charging a specific client or 'all' clients",  StartChargingCmdCallback, 1},
    {"stop_charging",           "\r\nstop_charging          [Client ID | all] Stop charging a specific client or 'all' clients",  StopChargingCmdCallback, 1},
    {"get_charging_info",       "\r\nget_charging_info      [Client ID | all] Get client charging info(RF power, battery level..etc)",  GetChargingInfoCmdCallback, 1},
    {"get_charger_id",          "\r\nget_charger_id         [] Get the charger unique ID",  GetChargerIdCmdCallback, 1},
    {"set_motion_engine",       "\r\nset_motion_engine      [sensitivity(1-5)] Set motion detection sensitivity",  SetMotionEngineCmdCallback, 0},
    {"get_motion_engine",       "\r\nget_motion_engine      [] Get motion detection sensitivity",  GetMotionEngineCmdCallback, 1},
    {"motion_devices",          "\r\nmotion_devices         [Client ID] Configure devices participating in motion detection",  MotionDevicesCmdCallback, 0},
    {"set_power_level",         "\r\nset_power_level        [PowerLevel(12-21)]  Configure system power level. Default = 20",  SetPowerLevelCmdCallback, 1},
    {"get_power_level",         "\r\nget_power_level        [] Get current system power level",  GetPowerLevelCmdCallback, 1},
    {"get_system_temp",         "\r\nget_system_temp        [] Get the current system temperature",  GetSystemTempCmdCallback, 0},
    {"get_system_state",        "\r\nget_system_state       [] Get the current system state",  GetSystemStateCmdCallback, 0},
    {"status",                  "\r\nstatus                 [] Get the current system status",  StatusCmdCallback, 0},
    {"identify_charger",        "\r\nidentify_charger       [] Set the light ring to signal charger presence.",  IdentifyChargerCmdCallback, 0},
    {"start_sending_data",      "\r\nstart_sending_data     [Interval][Client ID] Start sending data to the cloud for these clients",  StartSendingDataCmdCallback, 0},
    {"stop_sending_data",       "\r\nstop_sending_data      [Client ID | all] Stop sending data to the cloud for these clients",  StopSendingDataCmdCallback, 2},
    {"normalize_phase",         "\r\nnormalize_phase        [Client ID] Start establishing phase references. Queue Size(1-10) parameter is optional",  NormalizePhaseCmdCallback, 1},
    {"debug_log",               "\r\ndebug_log              [1=MM, 2=DAE, 3=both][0-4][0x0-0xFF]  Turn on/off MessageManager/Daemon logs",  DebugLogCmdCallback, 1},
        
    //new commands added for Orion
    {"set_led",                 "\r\nset_led               [LED#][0=off | 1=on]  Turn on/off LED",  SetLedCmdCallback, 2},
    {"set_spi_single",          "\r\nset_spi_single        [0=single, 1=all]",  SetSpiSingleCmdCallback, 1},
    {"set_amb_enable",          "\r\nset_amb_enable        [Address: 0-3] [o=off, 1=on] Set AMB LDO Enable line on or off",  SetAmbEnCmdCallback, 2},
    {"set_uvp_enable",          "\r\nset_uvp_enable        [Address: 0-3] [o=off, 1=on] Set uVP Enable Line on or off",  SetUvpEnCmdCallback, 2},

    {"set_uvp_tx",              "\r\nset_uvp_tx             [state: 0-1] Set uVP Tx:",  SetUvpTxCmdCallback, 1},
    {"set_uvp_rx",              "\r\nset_uvp_rx             [state: 0-1] Set uVP Rx:",  SetUvpRxCmdCallback, 1},
    {"read_uvp_pd",             "\r\nread_uvp_done          []  Read uVP PD lines\r\n",  ReadUvpPdCmdCallback, 0},
    
};
/*******************************************************************************************
 * Private Variable Declarations
 *******************************************************************************************/

/*******************************************************************************************
 * Private Function Declarations 
 *******************************************************************************************/

const CLI_Command_Definition_t *GetCliCmds(void)
{
    return gCliCmds;
}

uint16_t GetCliCmdSize(void)
{
    return sizeof(gCliCmds)/sizeof(gCliCmds[0]);
}


/**
 *   @brief set the debug LEDs
 *
 *
 *
 */
static BaseType_t SetLedCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;    // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
    ( void ) xWriteBufferLen;           //fixes compiler warnings
    char const *pcParameter1, *pcParameter2;
    BaseType_t xParameter1StringLength, xParameter2StringLength;
    pcParameter1 = FreeRTOS_CLIGetParameter( pcCommandString, 1,&xParameter1StringLength);      //ARG 1 - amb numeral
    pcParameter2 = FreeRTOS_CLIGetParameter( pcCommandString, 2,&xParameter2StringLength);      //ARG 2 - BOOL on or off
    if ( (xParameter1StringLength > 1) || (xParameter1StringLength < 1) || (pcParameter1[0] < '0') || (pcParameter1[0] > '4') )
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR:  Invalid LED number  Hint: use 0 to 4\r\n");
        return retVal;
    }
    if ( (xParameter2StringLength != 1) || (pcParameter2[0] < '0') || (pcParameter2[0] > '1') ){ 
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR:  Invalid LED state - hint: 0 = off or 1 = on\r\n");
        return retVal;      
    }
        
    switch (pcParameter1[0])
    {       
    case '0':
        //as the input is the ascii value from the CLI, the code below takes the offset of the ascii value for zero, '0', from the ascii value inputted by the user to calculate the value
        HAL_GPIO_WritePin(LED0_GPIO_Port, LED0_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);      
        break;
    case '1':
        HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);
        break;
    case '2':
        HAL_GPIO_WritePin(LED2_GPIO_Port, LED2_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);
        break;
    case '3':
        HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);
        break;
    case '4':
        HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, pcParameter2[0] == '0' ? GPIO_PIN_RESET : GPIO_PIN_SET);
        break;
    default:
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "ERROR:  Invalid LED number\r\n");
        return retVal;
    }
    
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "  LED%i %s\r\n", (pcParameter1[0]-'0'), (pcParameter2[0] == '0' ? "Cleared" : "Set") );
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}


///There functions are @todo and be built-out
static BaseType_t SetSpiSingleCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    return pdFALSE;
}
static BaseType_t SetAmbEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    return pdFALSE;
}
static BaseType_t SetUvpEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    return pdFALSE;
}

static BaseType_t SetUvpTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    return pdFALSE;
}
static BaseType_t SetUvpRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    return pdFALSE;
}
static BaseType_t ReadUvpPdCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    return pdFALSE;
}


/**
 * @brief Tests if an argument is 'all' in a case insensitive manner
 *
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 *
 * @return true if 'all', otherwise false 
 */
static bool isArgAll(const char* pArg, BaseType_t argLen)
{
    return (strncasecmp(pArg, "all", argLen) == 0);
}

/**
 * @brief Retrieves a number argument from a string
 * 
 * @param pArg   The argument string
 * @param argLen The length of an argument string
 * @param num    Pointer to type long to receive the value of the string.
 *
 * @return true if the conversion is successful, false otherwise
 */
static bool getNumArg(const char* pArg, BaseType_t argLen, long* num)
{
    char* endPtr;
    *num = strtol(pArg, &endPtr,  0);
    return ((pArg + argLen) == endPtr);   //This tests there was no stray characters in the arg
}

/**
 * @brief Fill the buffer with the current state of AMB enable pins.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithAmbEnState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    ambMask_t ambMask = AmbGetEnabled();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "On" : "Off");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
}

/**
 * @brief Fill the buffer wth the current UVP selection number
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithSelectedUvpState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    uvpNum_t uvpNum = AmbGetSelectedUvp();
    
    if (uvpNum == ALL_UVP_NUM)
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "SELECTED UVP: all\r\n");
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "SELECTED UVP: %d\r\n", uvpNum);      
    }
}

/**
 * @brief Fill the buffer with the current state of the UVP enable pins.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithUvpEnState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    ambMask_t ambMask = AmbGetUvpEnabled();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "UVP EN FOR AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "On" : "Off");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
}

/**
 * @brief Fill the buffer with the current state of the phase update pins.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithAPUEnState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    ambMask_t ambMask = AmbGetPuState();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "PU for AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "Raised" : "Dropped");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
}

/**
 * @brief Fill the buffer with the current state of the spi enabled pins.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 */
static void fillBufferWithASpiEnState(char *pcWriteBuffer, size_t xWriteBufferLen)
{
    ambMask_t ambMask = AmbGetSpiEnabled();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "SPI EN for AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "On" : "Off");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
}

/**
 * @brief Process the amb_on command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AmbOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbEnable(ALL_AMB_MASK);
        fillBufferWithAmbEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetEnabled();
        ambMask |= NUM_TO_MASK(num);
        AmbEnable(ambMask);
        fillBufferWithAmbEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
    return retVal;
}

/**
 * @brief Process the amb_off command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AmbOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbEnable(AMB_DISABLE_ALL);
        fillBufferWithAmbEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetEnabled();
        ambMask &= (~(NUM_TO_MASK(num)));
        AmbEnable(ambMask);
        fillBufferWithAmbEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
    return retVal;
}

/**
 * @brief Process the uvp_on command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t UvpOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        if (AmbUvpEnable(ALL_AMB_MASK) == COTA_ERROR_NONE)
        {
            fillBufferWithUvpEnState(pcWriteBuffer, xWriteBufferLen);
        }
        else
        {
            snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: One of the AMB's was off and it's UVP could not be enabled\r\n");
        }
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetUvpEnabled();
        ambMask |= NUM_TO_MASK(num);
        if (AmbUvpEnable(ambMask) == COTA_ERROR_NONE)
        {
            fillBufferWithUvpEnState(pcWriteBuffer, xWriteBufferLen);
        }
        else
        {
            snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Attempt to enable UVP on AMB that is off.\r\n");
        }
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
    return retVal;
}



/**
 * @brief Process the uvp_off command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t UvpOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbUvpEnable(AMB_DISABLE_ALL);
        fillBufferWithUvpEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetUvpEnabled();
        ambMask &= (~(NUM_TO_MASK(num)));
        AmbUvpEnable(ambMask);
        fillBufferWithUvpEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
    return retVal;
}


/**
 * @brief Process the pu_raise command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t PuRaiseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbSetPUState(ALL_AMB_MASK);
        fillBufferWithAPUEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetPuState();
        ambMask |= NUM_TO_MASK(num);
        AmbSetPUState(ambMask);
        fillBufferWithAPUEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
    return retVal;
}

/**
 * @brief Process the pu_drop command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t PuDropCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbSetPUState(AMB_DISABLE_ALL);
        fillBufferWithAPUEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetPuState();
        ambMask &= (~(NUM_TO_MASK(num)));
        AmbSetPUState(ambMask);
        fillBufferWithAPUEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
    return retVal;
}

/**
 * @brief Process the tx_on command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t TxOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    
    AmbEnableTx(true);
    
    if (AmbInTxMode())
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, SUCCESS_MSG);
    }
    else
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);     
    }
    
    return retVal;
}

/**
 * @brief Process the tx_off command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t TxOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    
    AmbEnableTx(false);
    
    if (!AmbInTxMode())
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, SUCCESS_MSG);
    }
    else
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);     
    }
    
    return retVal;
}

/**
 * @brief Process the get_tx command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetTxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
   
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\nTx is %s\r\n", AmbInTxMode() ? ENABLED_MSG : DISABLED_MSG);

    return retVal;
}

/**
 * @brief Process the rx_on command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t RxOnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    
    AmbEnableRx(true);
    
    if (AmbInRxMode())
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, SUCCESS_MSG);
    }
    else
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);     
    }
    
    return retVal;
}

/**
 * @brief Process the rx_off command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t RxOffCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    
    AmbEnableRx(false);
    
    if (!AmbInRxMode())
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, SUCCESS_MSG);
    }
    else
    {
        snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);     
    }
    
    return retVal;
}

/**
 * @brief Process the get_rx command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetRxCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
   
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\nRx is %s\r\n", AmbInRxMode() ? ENABLED_MSG : DISABLED_MSG);

    return retVal;
}

/**
 * @brief Select the UVP number to interact with.
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t SelectUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        if (AmbSelectUvp(ALL_UVP_NUM) == COTA_ERROR_NONE)
        {
            fillBufferWithSelectedUvpState(pcWriteBuffer, xWriteBufferLen);
        }
        else
        {
            snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);  
        }
    }
    else if (getNumArg(pArg, argLen, &num) && (num < UVPS_PER_AMB))
    {
        if (AmbSelectUvp(num) == COTA_ERROR_NONE)
        {
            fillBufferWithSelectedUvpState(pcWriteBuffer, xWriteBufferLen);
        }
        else
        {
            snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, FAILURE_MSG);  
        }
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
    return retVal;  
}

/**
 * @brief Retrieves the UVP number that is currently selected
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetSelectedUvpCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    fillBufferWithSelectedUvpState(pcWriteBuffer, xWriteBufferLen);
    return pdFALSE;
}

/**
 * @brief Retrieves the state of phase detect of each AMB
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t GetPdDoneCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    ambMask_t ambMask = AmbGetPhaseDetect();
    ambNum_t ii;
    int numRet;
    
    numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "\r\n");
    pcWriteBuffer += numRet;
    xWriteBufferLen -= numRet;
    
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        numRet = snprintf(pcWriteBuffer, xWriteBufferLen, "PD FOR AMB %d : %s\r\n", ii, BIT_IN_MASK(ii,ambMask) ? "Done" : "In Progress");
        pcWriteBuffer += numRet;
        xWriteBufferLen -= numRet;       
    }
    return pdFALSE;
}

/**
 * @brief Process the amb_spi_en command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AmbSpiEnCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString)
{
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbSpiEnable(ALL_AMB_MASK);
        fillBufferWithASpiEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetSpiEnabled();
        ambMask |= NUM_TO_MASK(num);
        AmbSpiEnable(ambMask);
        fillBufferWithASpiEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
    return retVal;
}

/**
 * @brief Process the amb_spi_dis command
 * 
 * @param pcWriteBuffer     Pointer to a buffer to receive a response
 * @param xWriteBufferLen   The length of the buffer to receive the response
 * @param pcCommandString   The full line of the command
 * 
 * @return Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
 */
static BaseType_t AmbSpiDisCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    BaseType_t argLen;
    long num;
    ambMask_t ambMask;
    
    const char* pArg = FreeRTOS_CLIGetParameter( pcCommandString, 1, &argLen);
    
    if (isArgAll(pArg,argLen))
    {
        AmbSpiEnable(AMB_DISABLE_ALL);
        fillBufferWithASpiEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else if (getNumArg(pArg, argLen, &num) && (num < MAX_NUM_AMB))
    {
        ambMask = AmbGetSpiEnabled();
        ambMask &= (~(NUM_TO_MASK(num)));
        AmbSpiEnable(ambMask);
        fillBufferWithASpiEnState(pcWriteBuffer, xWriteBufferLen);
    }
    else
    {
        snprintf(pcWriteBuffer, xWriteBufferLen, "\r\nERROR: Bad argument.\r\n");
    }
    
    return retVal;
}


static BaseType_t CalibrateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t AddClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RegisterClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RemoveClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t IdentifyClientCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientFwUpdateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientFwUpdateRespCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientSleepCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientLeaveCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SetGoodChannelsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SetComChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RunCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t PauseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RebootCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ResetCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ResetFpgaCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ResetProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t CheckProxyCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ProxyCommandCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ProxyCommandDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t RestartCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ShutdownCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SendCqtCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SendDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SendTpcCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SendTpsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StopDiscCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t InfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetGoodChannelsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetComChannelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetTxFreqCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ChannelInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ProxyInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t VersionsCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientListCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientDetailCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t ClientConfigCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t DevicesInRangeCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t DevicesStatusCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StartChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StopChargingCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetChargingInfoCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetChargerIdCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SetMotionEngineCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetMotionEngineCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t MotionDevicesCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t SetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetPowerLevelCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetSystemTempCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t GetSystemStateCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StatusCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t IdentifyChargerCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StartSendingDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t StopSendingDataCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t NormalizePhaseCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}
static BaseType_t DebugLogCmdCallback(char *pcWriteBuffer, size_t xWriteBufferLen, const char *pcCommandString){
    BaseType_t retVal = pdFALSE;
    ( void ) xWriteBufferLen;       //fixes compiler warnings
    snprintf(pcWriteBuffer, MAX_OUTPUT_LENGTH, "\r\n" "WARNING:  Command Not Implemented!!\r\n");
    return retVal;                  // Return pdFALSE to indicate there is no more data to return; pdTRUE otherwise
}

#endif // #ifndef VENUS_BW_COMPATIBILITY 
