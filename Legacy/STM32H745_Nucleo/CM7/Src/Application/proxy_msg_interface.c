/** 
 * @file       proxy_msg_interface.c
 *
 * @brief      This module defines all messages types that can be sent to the proxy. 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/*
  There is a known issue with the way the proxy extracts messages from
  the proxy driver where the discovery message extracts correctly
  (when it shouldn't) and the join message is extracted incorrectly
  w/o a bandage (which corrects a behavior). This is likely caused by
  a byte alignment issue somewhere in the code path. (at the time of
  writing this, only disc and join had been sufficiently tested,
  unknown about parsing TPC cfns, and query cfn/data).

  As stated, all messages will receive band-aides until the proxy
  driver (proxy.c) receives a more intimate protocol change. This may
  or may not affect affect the code in VenusB_proxy repo.
 */

/*******************************************************************************************
 * Includes
 *******************************************************************************************/
#include <stdio.h>
#include <string.h>
#include "proxyHostMsgInf.h"
#include "proxyClientMsgInf.h"
#include "error.h"
#include "orion_config.h"
#include "proxy.h"
#include "proxy_msg_interface.h"
#include "ctrl_task.h"
#include "cli_task.h"

/*******************************************************************************************
 * Defines 
 ******************************************************************************************/
#define MSG_SIZE_DISCONNECT        (sizeof(PrxHostDisconnectClientMsg_t) + sizeof(ProxyHostMsgType_t))
#define MSG_SIZE_QUERY             (sizeof(PrxHostCqTablePartialMsg_t) + sizeof(ProxyHostMsgType_t))
#define MSG_SIZE_TPC               (sizeof(PrxHostTonePowerConfig_t) + sizeof(ProxyHostMsgType_t))
#define MSG_SIZE_JOIN              (sizeof(PrxHostJoinClientMsg_t) + sizeof(ProxyHostMsgType_t))
#define MSG_SIZE_DISC              (sizeof(PrxHostStartDiscMsg_t) + sizeof(ProxyHostMsgType_t))
#define MSG_SIZE_PRX_CMD           (sizeof(PrxHostProxyCmdMsg_t) + sizeof(ProxyHostMsgType_t))

/* These values summed give the offset value needed for the band-aid
     for the data being brought in from the proxy driver (proxy.c). By
     offsetting the buffer by MSG_LENGTH_SIZE + MSG_TYPE_SIZE, only
     the data payload is obtained in the 'extract' functions.

   For more information for this bug, check the comment at the head of
     this module, and at the head of the proxy driver (proxy.c).
*/
#define MSG_LENGTH_SIZE            1
#define MSG_TYPE_SIZE              1

/*******************************************************************************************
 * Typedefs
 ******************************************************************************************/

/*******************************************************************************************
 * Private Variable Declarations
 *******************************************************************************************/

/*******************************************************************************************
 * Private Function Declarations 
 *******************************************************************************************/

/**
 * @brief Determines if a Proxy message is ready to be processed by the control task
 * @return true if a message is ready to be processed, false if otherwise.
 */
bool PrxMsgRdy(void)
{
    return !ProxyIsRxQueueEmpty();
}

/**
 * @brief Gets the message from the proxy driver, and return the type the message is.
 * @param buf  An output buffer for the proxy data to be copied to.
 * @param size The size of the output buffer
 * @return The type of message that was just obtained from the proxy driver.
 */
ProxyHostMsgType_t PrxGetMsg(uint8_t *buf, uint8_t size)
{
    uint32_t proxyMsgSize;  ///< The size of the message coming from the proxy driver
    
    size = MIN(size, CTRL_TASK_MAX_MSG_SIZE);

    proxyMsgSize = ProxyGetMessage(buf, size);

    if (proxyMsgSize == 0) ///< Indicates that message tried to be received because the queue was empty.
        return PRX_HOST_ERROR;

    // The 1st Element is the message type.
    return ((ProxyHostMsgType_t) buf[1]);
}

/**
 * @brief Extracts a discovery type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param discResult An output buffer that will get populated with discovered client information
 */ 
void PrxExtractDiscoveryResp(uint8_t *buf, uint8_t size, PrxHostDiscoveryMsg_t *discResult)
{
    size = MIN(size, sizeof(PrxHostDiscoveryMsg_t));

    memcpy((void *) discResult, (void*)buf, size);
}

/**
 * @brief Extracts a discovery type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param discResult An output buffer that will get populated with join response information
 */ 
void PrxExtractJoinResp(uint8_t *buf, uint8_t size, PrxHostClientJoinRspMsg_t *joinResult)
{
    uint8_t *temp = &buf[MSG_LENGTH_SIZE + MSG_TYPE_SIZE];
    size = MIN(size, sizeof(PrxHostClientJoinRspMsg_t));
    
    memcpy((void *) joinResult, (void*)temp, size);
}

/**
 * @brief Extracts a 'Tone Power Config' (TPC) type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param tpcResult An output field that will get populated with the tpc response information
 */ 
void PrxExtractTpcResp(uint8_t *buf, uint8_t size, PrxHostTpcCfn_t *tpcResult)
{
    uint8_t *temp = &buf[MSG_LENGTH_SIZE + MSG_TYPE_SIZE];
    size = MIN(size, sizeof(PrxHostTpcCfn_t));
    
    memcpy((void *) tpcResult, (void*)temp, size);
}

/**
 * @brief Extracts a Query response type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param queryResult An output field that will get populated with the query cfn response information
 */ 
void PrxExtractQueryCfnResp(uint8_t *buf, uint8_t size, PrxHostCqRecCfn_t *queryResult)
{
    uint8_t *temp = &buf[MSG_LENGTH_SIZE + MSG_TYPE_SIZE];
    size = MIN(size, sizeof(PrxHostCqRecCfn_t));
    
    memcpy((void *) queryResult, (void*)temp, size);
}


/**
 * @brief Extracts a Query data type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param queryResult An output field that will get populated with the query data response information
 */ 
void PrxExtractQueryDataResp(uint8_t *buf, uint8_t size, ClientQueryDataStd_t *queryResult)
{
    uint8_t *temp = &buf[MSG_LENGTH_SIZE + MSG_TYPE_SIZE];
    size = MIN(size, sizeof(ClientQueryDataStd_t));
    
    memcpy((void *) queryResult, (void*)temp, size);
}

/**
 * @brief Extracts a Disconnect response type message from the proxy queue.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer
 * @param disconnectResult An output field that will get populated with the query response information
 */ 
void PrxExtractDisconnectResp(uint8_t *buf, uint8_t size, PrxHostLeaveNetworkCfn_t *disconnectResult)
{
    size = MIN(size, sizeof(PrxHostLeaveNetworkCfn_t));
    
    memcpy((void *) disconnectResult, (void*)buf, size);
}

/**
 * @brief Extracts the response from a proxy command sends it to the CLI task.
 * @todo This function will later need to be modified for frequency agility, since
 *       it will be needed to process rssi values of different channels.
 * @param buf  An input buffer that contains the data that will be parsed
 * @param size The size of the input buffer (used to gaurd against buffer overruns)
 */
void PrxExtractPrxCmdResp(uint8_t *buf, uint8_t size)
{
    uint8_t respSize = buf[0];
    uint8_t ii;
    uint8_t cliBuf[32];
    uint8_t sendSize;
    
    size = MIN(size,respSize);
    
    for (ii = 0; ii < (size - 1); ii++)
    {
        if (ii % 8)
        {
            sendSize = snprintf((char*)cliBuf, sizeof(cliBuf), " 0x%02x", buf[ii+1]);
            PostCliMsgForTransmit((uint8_t*)cliBuf, sendSize);
        }
        else
        {
            sendSize = snprintf((char*) cliBuf, sizeof(cliBuf), "\r\n0x%02x : 0x%02x", ii, buf[ii+1]);
            PostCliMsgForTransmit(cliBuf, sendSize);
        }
    }
    sendSize = snprintf((char*)cliBuf, sizeof(cliBuf), "\r\n");
    PostCliMsgForTransmit((uint8_t*)cliBuf, sendSize);
}

/**
 * @brief Send message to the proxy that it needs to transmit a discovery message
 *        and respond with the clients that it has found.
 *
 * @todo  The PrxHostStartDiscMsg_t message tells the proxy to do one of two things.
 *          1. It will send a discovery message to the clients when the PrxHostStartDiscMsg_t
 *             is received at the proxy.
 *          2. It will send a discovery message again at a time in accordance to the time
 *             as set in the 'period' field in PrxHostStartDiscMsg_t.
 *        Orion has a finer scope of control over timing than it's predecessors and this periodic
 *        behavior becomes less desirable. For this reason, this function will send the maximum
 *        period to the proxy and it 'should' just send messages when the host sends a discovery
 *        message via this function call. The PrxHostStartDiscMsg_t periodic behavior should
 *        be removed for simplicity's sake and give full control of timing to the host.
 *
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
cotaError_t PrxSendDisc(void)
{
    PrxHostStartDiscMsg_t *discMsg;
    uint16_t     proxyId;
    uint16_t     panId;
    uint8_t      commCh;
    //uint16_t     discPeriod;
    uint8_t      buf[MSG_SIZE_DISC];

    // Offset by one to make space in the buff for the message ID
    discMsg = (PrxHostStartDiscMsg_t *) &buf[1];

    GetPanId(&panId);
    GetProxyId(&proxyId);
    GetCommChannel(&commCh);
    // GetDiscPeriod(&discPeriod);

    buf[0] = HOST_PRX_START_DISCOVERY;
    memcpy((void*) &discMsg->shortAddr  , (void*) &proxyId   , sizeof(proxyId));   
    memcpy((void*) &discMsg->panId      , (void*) &panId     , sizeof(panId));  
    // memcpy((void*) &discMsg->period     , (void*) &discPeriod, sizeof(discPeriod)); 
    memcpy((void*) &discMsg->commChannel, (void*) &commCh    , sizeof(commCh));

    // Hard-code the period to the maximum period value.
    discMsg->period = 0xffff; // period is a 16-bit value.

    return ((ProxySendMessage(buf, MSG_SIZE_DISC) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

/**
 * @brief Send message to proxy that it needs to transmit a join message to a
 *        particular client and get an acknowledgment from the client that
 *        it has properly joined the network.
 * @param clientLongId  The long id associated with the target client
 * @param clientShortId The short id associated with the target client
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
cotaError_t PrxSendJoin(ExtdAddr_t clientLongId, ShrtAddr_t clientShortId)
{
    PrxHostJoinClientMsg_t *joinMsg;
    uint8_t buf[MSG_SIZE_JOIN];

    // Offset by one to make space in the buff for the message ID
    joinMsg = (PrxHostJoinClientMsg_t *) &buf[1];
    
    // Message ID
    buf[0]  = HOST_PRX_JOIN_CLIENT;

    // Memcpy the extended ID
    memcpy((void*) &joinMsg->extAddr  , (void *) &clientLongId , sizeof(ExtdAddr_t));
    memcpy((void*) &joinMsg->shortAddr, (void *) &clientShortId, sizeof(ShrtAddr_t));

    return ((ProxySendMessage(buf, MSG_SIZE_JOIN) == true) ?
            COTA_ERROR_NONE :
            COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}


/**
 * @brief Tell the proxy that it needs to transmit a Tpc message to a specified client.
 *        An acknowledgment is expected from the proxy after sending this message.
 *
 *        This is step where we would send a beacon encoding to a client. But, because this
 *        has never been properly implemented or planned, the encoding is set to 0.
 *
 * @param clientShortId The short id associated with the target client.
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
cotaError_t PrxSendTpc(ShrtAddr_t clientShortId)
{
    PrxHostTonePowerConfig_t *tpcMsg;
    CotaTonePowerConfig_t    *tpcPay;
    uint16_t                  toneSlotDur;
    uint16_t                  powerSlotDur;
    uint8_t                   powerSlotCnt;
    uint8_t                   totalTones;  
    uint8_t                   beaconEncoding[NUM_ENCODING_BYTES];
    uint8_t                   buf[MSG_SIZE_TPC];

    // Offset by one to make space in the buff for the message ID
    tpcMsg = (PrxHostTonePowerConfig_t *) &buf[1];
    tpcPay = (CotaTonePowerConfig_t *)    &tpcMsg->data;

    GetToneSlotDuration(&toneSlotDur);   
    GetPowerSlotDuration(&powerSlotDur); 
    GetPowerSlotCount(&powerSlotCnt);         
    GetTotalTones(&totalTones);          

    // Since the beacon encoding has not been designed, set the array to 0
    memset((void *)beaconEncoding, 0, NUM_ENCODING_BYTES);

    // Offset by one to make space in the buff for the message ID
    buf[0] = HOST_PRX_TONE_POWER_CONFIG;
    memcpy((void*) &tpcMsg->shortAddress     , (void*) &clientShortId , sizeof(clientShortId));   
    memcpy((void*) &tpcPay->toneSlotDuration , (void*) &toneSlotDur   , sizeof(toneSlotDur));   
    memcpy((void*) &tpcPay->powerSlotDuration, (void*) &powerSlotDur  , sizeof(powerSlotDur));  
    memcpy((void*) &tpcPay->powerSlotCount   , (void*) &powerSlotCnt  , sizeof(powerSlotCnt));
    memcpy((void*) &tpcPay->totalTones       , (void*) &totalTones    , sizeof(totalTones));
    memcpy((void*) &tpcPay->beaconEncoding   , (void*) &beaconEncoding, NUM_ENCODING_BYTES);

    return ((ProxySendMessage(buf, MSG_SIZE_TPC) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

/**
 * @brief Tell the proxy that it needs to transmit a Query message to as specific client.
 *        An acknowledgment is expected from the proxy after sending this message.
 *
 * @todo  The current communication network is based off of a query table. That is,
 *        multiple clients worth of data can be sent to the proxy, the proxy will
 *        separate it, and the x messages will be transmitted to the clients where x
 *        is the number of clients whose data is in the query table (with x client responses).
 *        For simplicity's sake, this function will only send one client's data
 *        in the query table. This should be updated in the proxy-host communication layer.
 *
 * @param clientShortId The shortId of the client that a query message will be sent to.
 * @return returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
cotaError_t PrxSendQuery(ShrtAddr_t clientShortId)
{
    PrxHostCqTablePartialMsg_t *queryMsg;
    PrxHostCqTableEntry_t       clientQueryEntries[MIN_CQT_CLIENTS];
    uint8_t                     buf[MSG_SIZE_QUERY];

    // Offset by one to make space in the buff for the message ID
    queryMsg = (PrxHostCqTablePartialMsg_t *) &buf[1];
    
    // Only setting the data for the 0th client
    clientQueryEntries[0].shortAddr = clientShortId;
    clientQueryEntries[0].queryType = (uint8_t) PRX_CLT_MSG_STATUS_STD;

    memset((void *)buf, 0, (sizeof(buf)/sizeof(buf[0])));
    
    // Format the message. 
    buf[0] = HOST_PRX_CQ_TABLE;
    queryMsg->header.totalNumClients = 1; // Hard-coded to 1 client for a query. 
    memcpy((void*) queryMsg->clientQueryEntry, (void*) clientQueryEntries, MIN_CQT_CLIENTS);

    /*
     * The PrxHostCqTablePartialMsg_t is bigger than what is formatted here.
     * This is because the current iteration of the Query table is designed to
     * handle query requests to multiple clients at one time. This function has
     * purposely limited that to 1 client for simplicity's sake. 
     */
    
    return ((ProxySendMessage(buf, MSG_SIZE_QUERY) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

/**
 * @brief Tell the proxy that it needs to transmit a disconnect message to a specific client.
 *        An acknowledgment is expected from the proxy after sending this message.
 * @param clientShortId The shortId of the client that we are removing from the network.
 * @result returns COTA_ERROR_NONE on success, COTA_ERROR_FAILED_TO_SEND_PROXY_MSG on failure
 */
cotaError_t PrxSendDisconnect(ShrtAddr_t clientShortId)
{
    PrxHostDisconnectClientMsg_t *disconnectMsg;
    uint8_t buf[MSG_SIZE_DISCONNECT];

    disconnectMsg = (PrxHostDisconnectClientMsg_t *) &buf[1];
    
    // Message ID
    buf[0] = HOST_PRX_DISCONNECT_CLIENT;
    memcpy((void*) &disconnectMsg->shortAddr, (void*) &clientShortId, sizeof(clientShortId));
    
    return ((ProxySendMessage(buf, MSG_SIZE_DISCONNECT) == true) ? COTA_ERROR_NONE : COTA_ERROR_FAILED_TO_SEND_PROXY_MSG);
}

