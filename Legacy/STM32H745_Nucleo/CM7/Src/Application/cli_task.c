/** 
 *
 * @file       cli_task.c
 *
 * @brief      Cota CLI task implementation file
 *

 * @note that the snprintf function requires more RAM than the defaul CubeMx configuration allows
 *
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "cli_task.h"
#include "main.h"
#include "string.h"
#include "cmsis_os.h"
#include "task.h"
#include "FreeRTOS_CLI.h"
#include <stdio.h>
#include <stdlib.h>
#include "stm32h7xx_hal.h"
#include "uart_driver.h"
#include "amb_control.h"
#include "variable_queue.h"
#include "debug_off.h"
#include "proxy_msg_interface.h"
#include "orion_config.h"

/*
  In order to reduce clutter of the commands, and to one day
  create a seamless message interface between the control task
  and a user interface, the commands that haven't been supported
  and inherited from Venus have been placed in a legacy command module. 
 */
//#define VENUS_BW_COMPATIBILITY
#ifdef VENUS_BW_COMPATIBILITY
#include "legacy_system_commands.h"
#else
#include "system_commands.h"
#endif

/** snprintf function requires more RAM than the defaul CubeMx configuration allows */
#define MAX_INPUT_LENGTH    50                  ///< Limited input to save memory
#define STRING_TERM         "\r\n"              ///< The character sequence that terminates a string and starts a new one
#define CLI_COMMAND_STORAGE_AREA_SIZE  1024     ///< The size of the storage to hold client commands to be passed on to the control task
#define CLI_TX_MESSAGE_BUFFER_SIZE 1024         ///< The size of the storage area for uart message queue.
static osThreadId_t gCliTaskHandle;             ///< CLI task handle

static const char * WELCOME_MSG = "\n\r\033[2JWelcome to Cota CLI!" "\r\n" "Type Help to view a list of registered commands."  "\r\n" ; // Note the weird escape value in string is the ANSI temrinal code to clear screen

//Message buffer from uart to control task
/**
 *  Contains the information to control the variable message queue
 *  used for storing the received messages from UART
 */
static VariableQueueControl_t gCotaCliCommandQueueControl;          
static uint8_t gCotaCliCommandStorageArea[CLI_COMMAND_STORAGE_AREA_SIZE];       ///< Storage area used by the variable message queue

//Message buffer to uart
static VariableQueueControl_t gCotaCliTxQueueControl;                          ///< Contains the information to control the variable message queue
static uint8_t gCotaCliTxMsgBufStorageArea[CLI_TX_MESSAGE_BUFFER_SIZE];        ///< Storage area used by the tx message queue 
static TaskHandle_t gCliDriverTaskHandle = NULL;                               ///< The uart driver task handle

static void StartCliTask(void *argument);

static const CLI_Command_Definition_t *gCliCmds;
static uint16_t gCliCmdSize;

uint16_t CliSendCommand(uint8_t* buf, uint8_t size)
{
    return VariableQueueSend(&gCotaCliCommandQueueControl, buf, size);
}

/**
 * @brief A task to monitor and receive uart messages from the cli message buffer
 *        and then send them to the uart.
 * @param pvParameters not used
 */
static void cliSendTask(void *pvParameters)
{
    uint16_t msgSize;
    uint8_t readBuff[MAX_UART_MSG_SIZE];
    
    for (;;)
    {
        msgSize = VariableQueueReceive(&gCotaCliTxQueueControl,
                     readBuff, MAX_UART_MSG_SIZE, portMAX_DELAY);
                      
        CotaUartTransmit(readBuff, msgSize, portMAX_DELAY);  
    }
}

/** 
 * @brief This creates the CLI processing task/thread
 */
void CreateCliTask(void)
{
    BaseType_t xReturned;
    cotaError_t err;
    
    // Define the CLI processing task
    const osThreadAttr_t defaultTask_attributes = {
      .name = "CLI Task",
      .priority = (osPriority_t) osPriorityNormal,
      .stack_size = 1024
    };
       
    err = VariableQueueCreateStatic(sizeof(gCotaCliCommandStorageArea),
        gCotaCliCommandStorageArea,
        &gCotaCliCommandQueueControl );
    
    while (err != COTA_ERROR_NONE); 
    
    // Create the CLI task
    gCliTaskHandle = osThreadNew(StartCliTask, NULL, &defaultTask_attributes);
    (void)gCliTaskHandle; // Avoid unused variable compiler warning
  
    err = VariableQueueCreateStatic(sizeof(gCotaCliTxMsgBufStorageArea),
        gCotaCliTxMsgBufStorageArea,
        &gCotaCliTxQueueControl );
    while (err != COTA_ERROR_NONE);
      
    xReturned = xTaskCreate(
              cliSendTask,                                      /* Function that implements the task. */
              "Monitors Tx cli queue and sends msgs to UART",   /* Text name for the task. */
              CLI_SEND_TASK_STACK_SIZE,                         /* Stack size in bytes. */
              NULL,                                             /* Parameter passed into the task. */
              CLI_SEND_TASK_PRIORITY,                           /* Priority at which the task is created. */
              &gCliDriverTaskHandle );                          /* Used to pass out the created task's handle. */
  
    while (xReturned != pdPASS);

    // Get the command list from the included module
    gCliCmds =    GetCliCmds();
    gCliCmdSize = GetCliCmdSize();
}


/**
 * @brief Transmits CLI data to the UART by putting the message in a buffer
 *        so the cli task can send it.
 *
 * @param pData    Data to transmit
 * @param size     size of the transmitted data in bytes
 * 
 * @return COTA_ERROR_NONE on success,otherwise a failure has occurred.
 */
cotaError_t PostCliMsgForTransmit(uint8_t *pData, uint16_t size)
{
    uint16_t sizeSent;
    cotaError_t error = COTA_ERROR_NONE;
    
    sizeSent = VariableQueueSend(&gCotaCliTxQueueControl, pData, size);
    
    if (sizeSent != size)
    {
       error = COTA_ERROR_CLI_TX_FAILED;
    }
    
    return error;
}

/**
 * @brief Checks for a message posted by the cli task in the variable length queue
 *        and then removes it.
 *
 * @param buf  The buffer to receive the message
 * @param size The size of the buffer to receive the message 
 * 
 * @return The number of bytes received
 */
uint16_t CliGetMessage(uint8_t * buf, uint8_t size)
{
    return VariableQueueReceive(&gCotaCliCommandQueueControl, buf, size, 0);
}

/** The CLI processing task that's registered to be executed by the OS
 *
 * @todo move the storage of the command list from ram to flash 
 */
static void StartCliTask(void *argument)
{
    //Peripheral_Descriptor_t xConsole;
    uint8_t cRxedChar;
    uint8_t cInputIndex = 0;
    BaseType_t xMoreDataToFollow;
    /* The input and output buffers are declared static to keep them off the stack. */
    static char pcOutputString[ MAX_OUTPUT_LENGTH ];
    static char pcInputString[ MAX_INPUT_LENGTH ];
    DBG1_RESET();
     // Register the CLI commands from the table
    for (uint16_t i = 0; i <  gCliCmdSize; i++)
    {
      FreeRTOS_CLIRegisterCommand(&gCliCmds[i]);
    }
    
    /* This code assumes the peripheral being used as the console has already
    been opened and configured, and is passed into the task as the task
    parameter.  Cast the task parameter to the correct type. */
    //xConsole = ( Peripheral_Descriptor_t ) pvParameters;

    /* Send a welcome message to the user knows they are connected. */
    //FreeRTOS_write( xConsole, pcWelcomeMessage, strlen( pcWelcomeMessage ) );
    PostCliMsgForTransmit((uint8_t*)WELCOME_MSG, strlen(WELCOME_MSG));
    DBG1_SET();
    for ( ;; )
    {
        /* This implementation reads a single character at a time.  Wait in the
        Blocked state until a character is received. */
        //FreeRTOS_read( xConsole, &cRxedChar, sizeof( cRxedChar ) );
        DBG2_SET();
        CotaUartWaitForRx(&cRxedChar, sizeof(cRxedChar), portMAX_DELAY);
        DBG2_RESET();

        if ( cRxedChar == '\r' )
        {
            /* A newline character was received, so the input command string is
            complete and can be processed.  Transmit a line separator, just to
            make the output easier to read. */
            //FreeRTOS_write( xConsole, "\r\n", strlen( \r\n );
            PostCliMsgForTransmit((uint8_t*)STRING_TERM, strlen(STRING_TERM));

            /* The command interpreter is called repeatedly until it returns
            pdFALSE.  See the Implementing a command documentation for an
            exaplanation of why this is. */
            do
            {
                /* Send the command string to the command interpreter.  Any
                output generated by the command interpreter will be placed in the
                pcOutputString buffer. */
                DBG3_SET();
                xMoreDataToFollow = FreeRTOS_CLIProcessCommand
                                    (
                                        pcInputString,   /* The command string.*/
                                        pcOutputString,  /* The output buffer. */
                                        MAX_OUTPUT_LENGTH/* The size of the output buffer. */
                                    );
                DBG3_RESET();

                /* Write the output generated by the command interpreter to the
                console. */
                //FreeRTOS_write( xConsole, pcOutputString, strlen( pcOutputString ) );
                PostCliMsgForTransmit((uint8_t*)pcOutputString, strlen(pcOutputString));

            } while ( xMoreDataToFollow != pdFALSE );

            /* All the strings generated by the input command have been sent.
            Processing of the command is complete.  Clear the input string ready
            to receive the next command. */
            cInputIndex = 0;
            memset( pcInputString, 0x00, MAX_INPUT_LENGTH );
        }
        else
        {
            /* The if() clause performs the processing after a newline character
            is received.  This else clause performs the processing if any other
            character is received. */
            DBG4_SET();
            if ( cRxedChar == '\n' )
            {
                // Ignore carriage returns
                // Note that depending on how your terminal program is set up, you may need to swap \n and \r processing
            }
            else if ( cRxedChar == '\b' )
            {
                /* Backspace was pressed.  Erase the last character in the input
                buffer - if there are any. */
                if ( cInputIndex > 0 )
                {
                    cInputIndex--;
                    pcInputString[ cInputIndex ] = '\0';
                }
            }
            else
            {
                /* A character was entered.  It was not a new line, backspace
                or carriage return, so it is accepted as part of the input and
                placed into the input buffer.  When a n is entered the complete
                string will be passed to the command interpreter. */
                if ( cInputIndex < MAX_INPUT_LENGTH )
                {
                    pcInputString[ cInputIndex ] = cRxedChar;
                    cInputIndex++;
                }
            }
            DBG4_RESET();
        }
    }
    DBG1_RESET();
}




