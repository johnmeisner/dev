/**
 * @file       client_avl_tree.h
 *
 * @brief      This is the header file for the AVL BST for organizing clients.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#ifndef _CLIENT_AVL_TREE_H_
#define _CLIENT_AVL_TREE_H_

/*******************************************************************************************
 * Includes
 *******************************************************************************************/
#include <stdint.h>
#include "error.h"
#include "client.h"
/*******************************************************************************************
 * Defines 
 ******************************************************************************************/

/*******************************************************************************************
 * Typedefs
 ******************************************************************************************/

/**
   Forward declaration for clientNode_t.
   Allows for the struct to hold clientNode_t pointers
*/
#pragma diag_suppress=Pe301
typedef struct _clientNode_t  clientNode_t;

typedef struct _clientNode_t
{
    client_t client;
    clientNode_t *left;
    clientNode_t *right;
} clientNode_t;


/*
  Client trees were originally designed to allow the user to
  create a variable size tree. However, because vanilla
  malloc/calloc memory will allocate memory on the stack,
  the memory will be allocated statically at a fixed
  size. Later, we will want to have allocated memory go to a
  specific location for...

  "statically allocated"-- allocated dynamically ;)
*/
#define CES_FIX
#ifndef CES_FIX
typedef struct _clientTree_t
{
    clientNode_t *nodeArray;                   ///< The array that contains all static nodes
    uint16_t      size;                        ///< The size of nodeArray in the client tree
    uint16_t      lastClient;                  ///< Counter that indexes the unused clients in nodeArray
    clientNode_t *head;                        ///< The head nodes for each tree
} clientTree_t;
#else
typedef struct _clientTree_t
{
    clientNode_t  nodeArray[MAX_NUM_CLIENTS];  ///< The array that contains all static nodes
    uint16_t      size;                        ///< The size of nodeArray in the client tree
    uint16_t      lastClient;                  ///< Counter that indexes the unused clients in nodeArray
    clientNode_t *head;                        ///< The head nodes for each tree
} clientTree_t;
#endif //#ifndef CES_FIX

//void CreateStaticTree(clientTree_t *tree, uint16_t size);
void DestroyTree(clientTree_t *tree);
cotaError_t InsertNode(client_t key, clientNode_t **node, clientTree_t *tree);
cotaError_t DeleteNode(ExtdAddr_t key, clientNode_t **node, client_t *oclient);
bool SearchTreeShortId(ShrtAddr_t key, clientNode_t *node, client_t *oclient);
bool        SearchTree(ExtdAddr_t key, clientNode_t *node, client_t *oclient);
cotaError_t UpdateClient(ExtdAddr_t key, clientNode_t *node, client_t *cdata);
void WalkTree(clientNode_t *node, void (*func)(client_t client));

#endif // _CLIENT_AVL_TREE_H_
