/**
 * @file       client_interface.h
 *
 * @brief      Header for client interface
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CLIENT_INTERFACE_H_
#define _CLIENT_INTERFACE_H_

#include "error.h"
#include "client.h"
#include "CotaCommonTypes.h"
#include "client_manager.h"

/*
 * Client Management operations
 */
cotaError_t CmClientDiscovered(PrxHostDiscoveryMsg_t discAck);
cotaError_t CmRegisterClient(ExtdAddr_t longId);
cotaError_t CmClientJoined(PrxHostClientJoinRspMsg_t joinAck);
cotaError_t CmClientTpcCfn(PrxHostTpcCfn_t tpcAck);
cotaError_t CmClientQueryData(ClientQueryDataStd_t queryAck);
cotaError_t CmRemoveClient(ExtdAddr_t longId);
cotaError_t CmGetSchedulde(clientSchedule_t *outSchedule);
cotaError_t CmSetBatteryLevel(ShrtAddr_t shortId, uint8_t newBatteryLevel);
cotaError_t CmGetClientInfoByShortId(ShrtAddr_t shortId, client_t *outClient);

void CmDiscClientList(void);
void CmJoinClientList(void);
void CmTpcClientList(void);
void CmRdyClientList(void);

bool CmGetJoinClient(ExtdAddr_t *longId, ShrtAddr_t *shortId);
bool CmGetTpcClient(ShrtAddr_t *shortId);
bool CmGetRdyClient(ShrtAddr_t *shortId);

#ifdef NO_COMP
cotaError_t CmAddClient(ExtdAddr_t newClientId, ShrtAddr_t *shortId);
cotaError_t CmRemoveClient(ExtdAddr_t longIdToRemove, ShrtAddr_t shortIdToRemove);
cotaError_t CmGetClientList(clientList_t* outClientList);
cotaError_t CmGetClientInfoByShortId(ShrtAddr_t shortId, client_t *outClient);
cotaError_t CmUpdateClientState(ShrtAddr_t shortId, clientState_t newState);
cotaError_t CmGetClientInfoByLongId(ExtdAddr_t longId, client_t *outClient);
cotaError_t CmGetSchedule(clientSchedule_t *outSchedule);
cotaError_t CmSetBatteryLevel(ShrtAddr_t shortId, uint8_t newBatteryLevel);

cotaError_t CmClientDiscovered(PrxHostDiscoveryMsg_t clientDiscovered);
cotaError_t CmClientJoined(PrxHostClientJoinRspMsg_t clientJoined);
cotaError_t CmClientTpcCfn(PrxHostTocCfn_t clientTpcAck);
cotaError_t CmClientQuery(PrxHostCqRecCfn_t clientQueryAck);
#endif

#endif // #endif _CLIENT_INTERFACE_H_
