/**
 * @file       tps_state_machine.c
 *
 * @brief      TPS state machine implementation
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#include "tps_state_machine.h"
#include "stm32h7xx_hal.h"
#include "cmsis_os.h"
#include "timers.h"
#include "tps_task.h"   
#include "main.h"
#include "debug_on.h"

#define TIME_BETWEEN_TPS_CYCLES_MS  30  ///< Time between TPS cycles in ms used for the demo state machine (not taking into account the TPS delay time)

extern TIM_HandleTypeDef htim1;

TimerHandle_t idleTimer;

/*
 * State Machine entry functions
 * */
static void ErrorEntry       (void);
static void WaitForStartEntry(void);
static void InterTpsEntry    (void);
static void IdleEntry        (void);
static void RxRdyEntry       (void);
static void PuPulseEntry     (void);
static void TxRdyEntry       (void);
static void SendPowerEntry   (void);

/*
 * State Machine check functions
 */

static bool WaitToInterTpsCheck(void);

static bool InterTpsToWaitCheck(void);
static bool InterTpsToIdleCheck(void);

static bool IdleToRxCheck      (void);
static bool IdleToTxCheck      (void);
static bool IdleToInterTpsCheck(void);

static bool RxToPuCheck        (void);
static bool RxToIdleCheck      (void);

static bool PuToRxCheck        (void);

static bool TxToSendPowerCheck (void);
static bool TxToIdleCheck      (void);

static bool SendPowerToTxCheck (void);

/*
 * State Machine exit functions
 */
static void WaitToInterTpsExit(void);

static void InterTpsToWaitExit(void);
static void InterTpsToIdleExit(void);

static void IdleToRxExit      (void);
static void IdleToTxExit      (void);
static void IdleToInterTPSExit(void);


static void RxToPuExit  (void);
static void RxToIdleExit(void);

static void PuToRxExit(void);

static void TxToIdleExit     (void);
static void TxToSendPowerExit(void);


static void SendPowerToTxExit(void);

static bool tpsMachineError(void);


static StateMachineState_t tpsWaitForStart;
static StateMachineState_t tpsInterTps;
static StateMachineState_t tpsIdle;
static StateMachineState_t tpsRxReady;
static StateMachineState_t tpsPuPulse;
static StateMachineState_t tpsTxReady;
static StateMachineState_t tpsSendPower;


/*
* TPS Error State Definition
*/
extern StateMachineTransition_t tpsErrorTxnOptions[];

static StateMachineState_t tpsError = {
  .state             = TPS_STATE_ERROR,
  .entryFcn          = ErrorEntry,
  .stateName         = "TPS State Machine Error",
  .transitionOptions = tpsErrorTxnOptions
};

static StateMachineTransition_t tpsErrorTxnOptions[] = {
  {
    .checkCondition = tpsMachineError,
    .exitFcn        = NULL,
    .nextState      = &tpsError
  },
  NULL_TRANSITION_STRUCT
};


/*
* TPS Wait for Start State Definition
*/
extern StateMachineTransition_t tpsWaitForStartTxnOptions[];

static StateMachineState_t tpsWaitForStart = {
  .state             = TPS_STATE_WAIT_FOR_START,
  .entryFcn          = WaitForStartEntry,
  .stateName         = "Not charging",
  .transitionOptions = tpsWaitForStartTxnOptions
};

static StateMachineTransition_t tpsWaitForStartTxnOptions[] = {
  {
    .checkCondition = WaitToInterTpsCheck,
    .exitFcn        = WaitToInterTpsExit,
    .nextState      = &tpsInterTps
  },
  NULL_TRANSITION_STRUCT
};


/*
* InterTPS State Definition
*/
extern StateMachineTransition_t tpsInterTpsTxnOptions[];

static StateMachineState_t tpsInterTps = {
  .state             = TPS_STATE_INTER_TPS,
  .entryFcn          = InterTpsEntry,
  .stateName         = "TPS Between TPS cycles",
  .transitionOptions = tpsInterTpsTxnOptions
};

static StateMachineTransition_t tpsInterTpsTxnOptions[] = {
  {
    .checkCondition = InterTpsToWaitCheck,
    .exitFcn        = InterTpsToWaitExit,
    .nextState      = &tpsWaitForStart
  },
  {
    .checkCondition = InterTpsToIdleCheck,
    .exitFcn        = InterTpsToIdleExit,
    .nextState      = &tpsIdle
  },
  NULL_TRANSITION_STRUCT
};



/*
* Idle State Definition
*/
extern StateMachineTransition_t tpsIdleTxnOptions[];

static StateMachineState_t tpsIdle = {
  .state             = TPS_STATE_IDLE,
  .entryFcn          = IdleEntry,
  .stateName         = "TPS Idle State",
  .transitionOptions = tpsIdleTxnOptions
};

static StateMachineTransition_t tpsIdleTxnOptions[] = {
  {
    .checkCondition = IdleToRxCheck,
    .exitFcn        = IdleToRxExit,
    .nextState      = &tpsRxReady
  },
  {
    .checkCondition = IdleToTxCheck,
    .exitFcn        = IdleToTxExit,
    .nextState      = &tpsTxReady
  },
  {
    .checkCondition = IdleToInterTpsCheck,
    .exitFcn        = IdleToInterTPSExit,
    .nextState      = &tpsInterTps
  },
  NULL_TRANSITION_STRUCT
};



/*
* Rx Ready State Definition
*/
extern StateMachineTransition_t tpsRxReadyTxnOptions[];

static StateMachineState_t tpsRxReady = {
  .state             = TPS_STATE_RX_READY,
  .entryFcn          = RxRdyEntry,
  .stateName         = "TPS Rx Ready State",
  .transitionOptions = tpsRxReadyTxnOptions
};

static StateMachineTransition_t tpsRxReadyTxnOptions[] = {
  {
    .checkCondition = RxToPuCheck,
    .exitFcn        = RxToPuExit,
    .nextState      = &tpsPuPulse
  },
  {
    .checkCondition = RxToIdleCheck,
    .exitFcn        = RxToIdleExit,
    .nextState      = &tpsIdle
  },
  NULL_TRANSITION_STRUCT
};


/*
* Pu Pulse State Definition
*/
extern StateMachineTransition_t tpsPuPulseTxnOptions[];

static StateMachineState_t tpsPuPulse = {
  .state             = TPS_STATE_PU_PULSE,
  .entryFcn          = PuPulseEntry,
  .stateName         = "TPS Phase Update Pulse",
  .transitionOptions = tpsPuPulseTxnOptions
};

static StateMachineTransition_t tpsPuPulseTxnOptions[] = {
  {
    .checkCondition = PuToRxCheck,
    .exitFcn        = PuToRxExit,
    .nextState      = &tpsRxReady
  },
  NULL_TRANSITION_STRUCT
};


/*
* Tx Ready State Definition
*/
extern StateMachineTransition_t tpsTxReadyTxnOptions[];

static StateMachineState_t tpsTxReady = {
  .state             = TPS_STATE_RX_READY,
  .entryFcn          = TxRdyEntry,
  .stateName         = "TPS Tx Ready State",
  .transitionOptions = tpsTxReadyTxnOptions
};

static StateMachineTransition_t tpsTxReadyTxnOptions[] = {
  {
    .checkCondition = TxToSendPowerCheck,
    .exitFcn        = TxToSendPowerExit,
    .nextState      = &tpsSendPower
  },
  {
    .checkCondition = TxToIdleCheck,
    .exitFcn        = TxToIdleExit,
    .nextState      = &tpsIdle
  },
  NULL_TRANSITION_STRUCT
};



/*
* Send Power State Definition
*/
extern StateMachineTransition_t tpsSendPowerTxnOptions[];

static StateMachineState_t tpsSendPower = {
  .state             = TPS_STATE_SEND_POWER,
  .entryFcn          = SendPowerEntry,
  .stateName         = "TPS Sending Power",
  .transitionOptions = tpsSendPowerTxnOptions
};

static StateMachineTransition_t tpsSendPowerTxnOptions[] = {
  {
    .checkCondition = SendPowerToTxCheck,
    .exitFcn        = SendPowerToTxExit,
    .nextState      = &tpsTxReady
  },
  NULL_TRANSITION_STRUCT
};


extern StateMachineState_t DemoSmInit;


/**
* @brief   Initializes the State Machine and returns the address of the initial state
* @note    Because function addresses are not known at compile time, function pointerts 
*          cannot be assigned to structs statically. All assignment must be made 
*          at compile time.
* @return  The function returns the inital state for the state machine. 
*/
StateMachineState_t* TpsStateMachineInit(void)
{
  StateMachineState_t* initState = &DemoSmInit;
  //StateMachineState_t* initState = &tpsWaitForStart;
  
  if (initState->entryFcn)
  {
    initState->entryFcn();
  }

  return initState;
}

/*
* State Machine Entry Functions
*/
__weak static void ErrorEntry       (void) {return;}
__weak static void WaitForStartEntry(void) {return;}
__weak static void InterTpsEntry    (void) {return;}
__weak static void IdleEntry        (void) {return;}
__weak static void RxRdyEntry       (void) {return;}
__weak static void PuPulseEntry     (void) {return;}
__weak static void TxRdyEntry       (void) {return;}
__weak static void SendPowerEntry   (void) {return;}

/*
 * State Machine check functions
 */
__weak static bool WaitToInterTpsCheck(void) {return false;}
__weak static bool InterTpsToWaitCheck(void) {return false;}
__weak static bool InterTpsToIdleCheck(void) {return false;}
__weak static bool IdleToRxCheck      (void) {return false;}
__weak static bool IdleToTxCheck      (void) {return false;}
__weak static bool IdleToInterTpsCheck(void) {return false;}
__weak static bool RxToPuCheck        (void) {return false;}
__weak static bool RxToIdleCheck      (void) {return false;}
__weak static bool PuToRxCheck        (void) {return false;}
__weak static bool TxToSendPowerCheck (void) {return false;}
__weak static bool TxToIdleCheck      (void) {return false;}
__weak static bool SendPowerToTxCheck (void) {return false;}

/*
* State Machine exit functions 
*/
__weak static void WaitToInterTpsExit(void) {return;}
__weak static void InterTpsToWaitExit(void) {return;}
__weak static void InterTpsToIdleExit(void) {return;}
__weak static void IdleToRxExit      (void) {return;}
__weak static void IdleToTxExit      (void) {return;}
__weak static void IdleToInterTPSExit(void) {return;}
__weak static void RxToPuExit        (void) {return;}
__weak static void RxToIdleExit      (void) {return;}
__weak static void PuToRxExit        (void) {return;}
__weak static void TxToIdleExit      (void) {return;}
__weak static void TxToSendPowerExit (void) {return;}
__weak static void SendPowerToTxExit (void) {return;}
__weak static bool tpsMachineError (void) {while (1);}





void IdleTimerCallback(TimerHandle_t xTimer);
void IdleTimerCallback(TimerHandle_t xTimer)
{
  DBG1_RESET();
  
  // Timer callback simply runs the TPS state machine after the idle period expires
  TpsRunStateMachineFromIsr();
}




//                      |----------------------|
//                      |                      |
//                     \|/                     |
//   DemoSmInit -> DemoSmIdle -> DemoSmInTps --|
//       |              |             |
//      \|/            \|/           \|/
//  DemoSmError    DemoSmError   DemoSmError


/*
* Demo Error State Definition
*/
extern StateMachineTransition_t DemoSmErrorTxnOptions[];
__weak static void DemoSmErrorEntry(void) {return;}

static StateMachineState_t DemoSmError = {
  .state             = TPS_STATE_ERROR,
  .entryFcn          = DemoSmErrorEntry,
  .stateName         = "Demo State Machine Error",
  .transitionOptions = DemoSmErrorTxnOptions
};

static StateMachineTransition_t DemoSmErrorTxnOptions[] = {
  NULL_TRANSITION_STRUCT
};

/*
* Demo Init State Definition
*/
extern StateMachineTransition_t DemoSmInitTxnOptions[];
extern StateMachineState_t DemoSmIdle;
static void DemoSmInitEntry  (void)
{
  idleTimer = xTimerCreate("Init timer",
                           pdMS_TO_TICKS(TIME_BETWEEN_TPS_CYCLES_MS),
                           pdFALSE,
                           ( void * ) 0,
                           IdleTimerCallback);
}
static bool DemoSmErrorCheck (void) {return false;}
static bool DemoSmIdleCheck  (void) {return true;}
static void DemoSmErrorExit  (void) {return;}
static void DemoSmIdleExit   (void)
{
  // Stop a TPS cycle when transitioning from In_TPS state to Idle state
  HAL_TIM_PWM_Stop_IT(&htim1, TIM_CHANNEL_1);
  HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_1);
  HAL_TIM_PWM_Stop_IT(&htim1, TIM_CHANNEL_2);
  
  // Show that TPS on TIM1 has stopped
  DBG2_RESET();
}

static StateMachineState_t DemoSmInit = {
  .state             = TPS_STATE_WAIT_FOR_START,
  .entryFcn          = DemoSmInitEntry,
  .stateName         = "Demo State Init",
  .transitionOptions = DemoSmInitTxnOptions
};

static StateMachineTransition_t DemoSmInitTxnOptions[] = {
  {
    .checkCondition = DemoSmErrorCheck,
    .exitFcn        = DemoSmErrorExit,
    .nextState      = &DemoSmError
  },
  {
    .checkCondition = DemoSmIdleCheck,
    .exitFcn        = DemoSmIdleExit,
    .nextState      = &DemoSmIdle
  },
  NULL_TRANSITION_STRUCT
};

/*
* Demo Idle State Definition
*/
extern StateMachineTransition_t DemoSmIdleTxnOptions[];
extern StateMachineState_t DemoSmInTps;
static void DemoSmIdleEntry  (void)
{
  xTimerStart(idleTimer, 0);
  
  // Start DBG1 pulse to show idle timer duration
  DBG1_SET();
}
static bool DemoSmInTpsCheck (void) {return true;}
static void DemoSmInTpsExit  (void) {return;}

static StateMachineState_t DemoSmIdle = {
  .state             = TPS_STATE_IDLE,
  .entryFcn          = DemoSmIdleEntry,
  .stateName         = "Demo State in Idle",
  .transitionOptions = DemoSmIdleTxnOptions
};

static StateMachineTransition_t DemoSmIdleTxnOptions[] = {
  {
    .checkCondition = DemoSmErrorCheck,
    .exitFcn        = DemoSmErrorExit,
    .nextState      = &DemoSmError
  },
  {
    .checkCondition = DemoSmInTpsCheck,
    .exitFcn        = DemoSmInTpsExit,
    .nextState      = &DemoSmInTps
  },
  NULL_TRANSITION_STRUCT
};

/*
* Demo In TPS State Definition
*/
extern StateMachineTransition_t DemoSmInTpsTxnOptions[];
static void DemoSmInTpsEntry(void)
{
  // TPS cycle will start after the proxy generates a pulse on the Proxy_Go pin

  // For the demo, toggle the PROXY_GO pin to emulate the proxy. This will start the TPS delay timer TIM2 in HW
  HAL_GPIO_WritePin(PROXY_GO_TEST_GPIO_Port, PROXY_GO_TEST_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(PROXY_GO_TEST_GPIO_Port, PROXY_GO_TEST_Pin, GPIO_PIN_RESET);
}

static StateMachineState_t DemoSmInTps = {
  .state             = TPS_STATE_SEND_POWER,
  .entryFcn          = DemoSmInTpsEntry,
  .stateName         = "Demo State in TPS",
  .transitionOptions = DemoSmInTpsTxnOptions
};

static StateMachineTransition_t DemoSmInTpsTxnOptions[] = {
  {
    .checkCondition = DemoSmErrorCheck,
    .exitFcn        = DemoSmErrorExit,
    .nextState      = &DemoSmError
  },
  {
    .checkCondition = DemoSmIdleCheck,
    .exitFcn        = DemoSmIdleExit,
    .nextState      = &DemoSmIdle
  },
  NULL_TRANSITION_STRUCT
};

// TODO:  Add a state to print the current state ID to UART
