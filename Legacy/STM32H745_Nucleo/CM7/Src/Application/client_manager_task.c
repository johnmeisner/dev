/**
 * @file       client_manager_task.c
 *
 * @brief      Task for managing the client manager data structures 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "FreeRTOS.h"
#include "semphr.h"
#include "task.h"
#include "client_manager_task.h"
#include "client_manager.h"
#include "orion_config.h"

#define NUM_QUEUE_ITEMS_JOIN      ((UBaseType_t) 10)   ///< The number of queued items that can be on queue
#define NUM_QUEUE_ITEMS_TPC       ((UBaseType_t) 10)   ///< The number of queued items that can be on queue
#define NUM_QUEUE_ITEMS_QUERY     ((UBaseType_t) 10)   ///< The number of queued items that can be on queue

/*
 * Task Architecture Variables
 */
SemaphoreHandle_t    gClientManagerSem        = NULL;  ///< The Client Manager Task semaphore 
static TaskHandle_t  gClientManagerTaskHandle = NULL;  ///< The Client Manager Task handle

static QueueHandle_t  gIncomingMsgQueue  = NULL; ///< Incoming message queue to the client manager.
static QueueHandle_t  gJoinQueue         = NULL; ///< The queue for #ExtdAddr_t that need a Join message sent to it
static QueueHandle_t  gTpcQueue          = NULL; ///< The queue for #ExtdAddr_t that need a TPC message sent to it
static QueueHandle_t  gQueryQueue        = NULL; ///< The queue for #ExtdAddr_t that need a Query message sent to it

static IncomingMessage_t msgBuf;

static void ClientManagerTask (void *pvParameters);
static cotaError_t ClientManagerTaskInit(void);
static void ClientManagerTask (void *pvParameters)
{
    uint8_t msgBuf[];
    for (;;)
    {
        VariableQueueReceive(gIncomingMsgQueue, (void *) &msgBuf, portMAX_DELAY);

        switch (msgBuf.type)
        {
        case(CLI_MAN_DISC_RESPONSE):
        {
            
        }
        case(CLI_MAN_):
        {
            
        }
        case(CLI_MAN_REMOVE_CLIENT):
        }
    }
}

/**
 * @brief This function creates the GatherAndVote task
 */
void CreateClientManagerTask(void)
{
    BaseType_t xReturned;
  
    while (ClientManagerTaskInit() != NO_ERROR);
  
    /*
     * TPS Task Creation
     */
    xReturned = xTaskCreate(
        ClientManagerTask,                    /* Function that implements the task. */
        "Manages all Client Manager data",    /* Text name for the task. */
        CLIENT_MANAGER_STACK_SIZE,            /* Stack size in bytes. */
        NULL,                                 /* Parameter passed into the task. */
        CLIENT_MANAGER_TASK_PRIORITY,         /* Priority at which the task is created. */
        &gClientManagerTaskHandle );          /* Used to pass out the created task's handle. */
  
    while (xReturned != pdPASS);
    
}

static cotaError_t ClientManagerTaskInit(void)
{
    BaseType_t retSuccess;
    cotaError_t cotaError;
  
    retSuccess = pdPASS;
  
    gClientManagerSem = xSemaphoreCreateBinary();
    if (gClientManagerSem == NULL)
    {
        retSuccess = pdFAIL;
    }

    // Initialize queues
    gJoinQueue  = xQueueCreate( NUM_QUEUE_ITEMS_JOIN, sizeof(CManJoinMessage_t));
    if(gJoinQueue == NULL)
        retSuccess = pdFAIL;
  
    gTpcQueue = xQueueCreate( NUM_QUEUE_ITEMS_TPC, sizeof(CManTpcMessage_t));
    if(gTpcQueue == NULL)
        retSuccess = pdFAIL;
  
    gQueryQueue = xQueueCreate( NUM_QUEUE_ITEMS_QUERY, sizeof(CManQueryMessage_t));
    if(gQueryQueue == NULL)
        retSuccess = pdFAIL;
  
    cotaError = InitClientTree();
  
    return ((retSuccess == pdPASS) && (cotaError == NO_ERROR)) ? NO_ERROR : COTA_ERROR_CLIENT_MANAGER_INIT_FAILED;

  
}
