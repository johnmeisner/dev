/**
 * @FILE       debug_off.h
 *
 * @BRIEF      Debug pin control macros for modules with debug pins disabled
 *
 * @COPYRIGHT  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#define DBG1_SET()
#define DBG2_SET()
#define DBG3_SET()
#define DBG4_SET()
#define DBG5_SET()
#define DBG1_RESET()
#define DBG2_RESET()
#define DBG3_RESET()
#define DBG4_RESET()
#define DBG5_RESET()

