/**
 * @file       client.h
 *
 * @brief      Header for the client type 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <stdint.h>
#include <stdbool.h>
#include "orion_config.h"
#include "CotaCommonTypes.h"

#define NUM_CLIENT_STATES    11     ///< The number of states that exist in #clientState_t
#define BATTERY_LEVEL_MASK   0x7F   ///< A Battery level has a maximum of 100(%), the bit-field gets 7-bits

typedef struct _client_t 
{
    ExtdAddr_t           longId;            ///< A client's MAC address assigned at manufacturing time
    ShrtAddr_t           shortId;           ///< A client's shortId assigned when added to the system
    uint8_t              batteryLevel:7,    ///< The battery level of the client, obtained during the client query
                         isValid:1;         ///< A bit that indicates that client information is out of date
    uint8_t              linkQuality;       ///< Link quality between the proxy and the client
    int8_t               rssi;              ///< RSSI value (dbm), the strength of the signal from the client.
    CotaStatusStd_t      qStatus;           ///< The status directly from the client.
} client_t;


typedef struct _clientSchedule_t
{
    client_t            clientList[MAX_TPS_CLIENTS]; ///< The list of schedulable clients
    uint16_t            numClients;                  ///< The number of clients that was put in the schedule
} clientSchedule_t;

bool IsLongValid(ExtdAddr_t longId);
int  LongCmp(ExtdAddr_t longId1, ExtdAddr_t longId2);
bool IsShortValid(ShrtAddr_t shortId);

#endif // #ifndef _CLIENT_H_
