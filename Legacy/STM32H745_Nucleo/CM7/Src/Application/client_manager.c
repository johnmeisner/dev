/**
 * @file       client_manager.c
 *
 * @brief      Client data structure manager 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "client.h"
#include "client_manager.h"
#include "client_avl_tree.h"
#include "CotaCommonTypes.h"
#include "orion_config.h"
#include "client_interface.h"
#include "client_avl_tree.h"

/* The most recent ShortID assigned to a client. This is just a bookmark to save search time. */
static ShrtAddr_t gMostRecentShortId;  

/*
 * Helper functions
 */
// static void insertionSort(client_t*, uint16_t);
static bool isShortIdUsed(clientNode_t *clientNode, ShrtAddr_t shortId);

/*
  As far as the transmitter is concerned, the clients can be put into the following
  states. Each state holds not only the state description, but also points to the
  storage for those clients. For detailed descriptions on each state, see the state
  declaration sections. Note, this is the transmitter's knowledge of the state of
  the client in question and may or may not concur with the state the client believes
  it should be in.
  
  
                               --------------------
                               |                   |
  Remove Via timer timeout <---|    Discovered     |
                               |                   |
                               --------------------
                                         |
                                         | Register the client
                                         |
                                        \|/
                               --------------------
                               |                   |  _______________    
                               |       Join        | /                 V 
     Remove device command <---|                   ||    Send Joins    | 
                               |   Confirmation    |^_________________/  
                               |                   |
                               --------------------
                                         |
                                         | Send Join msg, receive acknowledgment
                                         |
                                        \|/
                               --------------------
                               |                   |  _______________    
                               |       TPC         | /                 V 
  Remove Via timer timeout <---|                   ||    Send TPCs     | 
                               |   Confirmation    |^_________________/  
                               |                   |
                               --------------------
                                        | 
                                        | Send TPC msg, receive acknowledgment 
                                        | 
                                       \|/
                               --------------------   _______________    
                               |                   | /                 V 
     Remove device command <---|      Ready        ||   Send Queries   | 
                               |                   |^_________________/  
                               --------------------                                            

  The job of the client manager is to manage the database of clients on the transmitter, and determine
  which clients need to receive messages (which is done implicitly given the state
  that the client resides in). The control task (ctrl_task.c) determines WHEN a client gets these
  messages, and passes the acknowledgments to the client via client manager. This will also cause
  the client to move states.
  
**/

static clientState_t stateDisc;
static clientState_t stateJoin;
static clientState_t stateTpc;
static clientState_t stateRdy;

static clientTree_t treeDisc;

static clientState_t stateDisc =
{
    .type = CLIENT_STATE_DISC,
    .cname = "Discovered clients",
    .tree  = &treeDisc
};

static clientTree_t treeJoin;

static clientState_t stateJoin = 
{
    .type = CLIENT_STATE_JOINED,
    .cname = "Joined clients",
    .tree  = &treeJoin
};

static clientTree_t treeTpc;

static clientState_t stateTpc  = 
{
    .type = CLIENT_STATE_TPC_CFN,
    .cname = "TPC clients",
    .tree  = &treeTpc
};

static clientTree_t treeRdy;

static clientState_t stateRdy = 
{
    .type = CLIENT_STATE_READY,
    .cname = "Readied clients",
    .tree  = &treeRdy
};

static clientState_t *allStates[] =
{
        &stateDisc,
        &stateJoin,
        &stateTpc,
        &stateRdy
};

const uint8_t NUM_OF_CLIENT_STATES = sizeof(allStates)/sizeof(allStates[0]);

/**
 * @brief Initializes every component the clientManager relies on for initialization
 * @return COTA_ERROR_NONE on success, COTA_ERROR_CLIENT_MANAGER_INIT_FAILED on failure
 */
cotaError_t InitClientManager(void)
{
    #ifndef CES_FIX
    // Initialize all the states
    CreateStaticTree(&treeDisc, NUM_DISC_CLIENTS);
    CreateStaticTree(&treeJoin, NUM_JOIN_CLIENTS);
    CreateStaticTree(&treeTpc, NUM_TPC_CLIENTS);
    CreateStaticTree(&treeRdy, NUM_RDY_CLIENTS);
    #else
    memset(&treeDisc.nodeArray, 0, sizeof(clientNode_t)*MAX_NUM_CLIENTS);
    memset(&treeJoin.nodeArray, 0, sizeof(clientNode_t)*MAX_NUM_CLIENTS);
    memset(&treeTpc.nodeArray , 0, sizeof(clientNode_t)*MAX_NUM_CLIENTS);
    memset(&treeRdy.nodeArray , 0, sizeof(clientNode_t)*MAX_NUM_CLIENTS);

    treeDisc.size = MAX_NUM_CLIENTS;
    treeJoin.size = MAX_NUM_CLIENTS;
    treeTpc.size = MAX_NUM_CLIENTS;
    treeRdy.size = MAX_NUM_CLIENTS;

    treeDisc.lastClient = 0;
    treeJoin.lastClient = 0;
    treeTpc.lastClient = 0;
    treeRdy.lastClient = 0;


    treeDisc.head = NULL;
    treeJoin.head = NULL;
    treeTpc.head = NULL;
    treeRdy.head = NULL;

    #endif 

    // Will want to do something smarter later.
    gMostRecentShortId = 1;
  
    return COTA_ERROR_NONE;
}

clientState_t *GetDiscState(void)
{
    return &stateDisc;
}

clientState_t *GetJoinState(void)
{
    return &stateJoin;
}

clientState_t *GetTpcState(void)
{
    return &stateTpc;
}

clientState_t *GetReadyState(void)
{
    return &stateRdy;
}

/**
 * @brief This function organizes an array of clients based on the batteryLevel
 * @param clientArray An array of clients that are contained in the tree. 
 * @param clientArraySize The number of clients that are contained in the tree.
 */
// static void insertionSort(client_t *clientArray, uint16_t clientArraySize)
// {
//     int16_t i, j;
//     client_t key;
//   
//     for (i = 1; i < clientArraySize; i++)
//     {
//         key = clientArray[i];
//         j--;
//     
//         while ( (j >= 0) && (clientArray[j].batteryLevel > key.batteryLevel) )
//         {
//             clientArray[j + 1] = clientArray[j];
//             j--;
//         }
//         clientArray[j + 1] = key; 
//     }
// }

/**
 * @brief returns the next unused shortId 
 * @return Returns a new and unused shortId
 */
ShrtAddr_t GetNewShortId(void)
{
    // Because we don't know which database contains the shortId in question,
    //  we need to check them all. We start with Active clients because
    //  that is the most likely place. 

    for (int i = 0; i < NUM_OF_CLIENT_STATES; i++)
    {
        /// @todo This is VERY inefficient in certain cases that shouldn't occur
        ///         very often, but will happen! A smarter, more efficient approach
        ///         would be to use linked lists of available shortIds
        while (isShortIdUsed(allStates[i]->tree->head, gMostRecentShortId) &&
               IsShortValid(gMostRecentShortId))
        {
            gMostRecentShortId++;
        }
    }
    return gMostRecentShortId;
}
        
/**
 * @brief This function walks the tree and determines if the shortId has been by any client in the tree.
 * @param clientNode the clientNode that is recursed on by the function.
 * @param shortId The shortId identifier that is being checked
 * @return returns true if the shortId is used, false if otherwise
 */
static bool isShortIdUsed(clientNode_t *node, ShrtAddr_t shortId)
{
    bool ret;

    ret = false;
    
    if (node != NULL)
    {
        if(node->client.shortId == shortId)
            return true;

        ret = isShortIdUsed(node->left, shortId);

        // If going down the left node returned true, don't
        //  overwrite it the output by searching down the
        //  right path
        if (!ret)
            ret = isShortIdUsed(node->right, shortId);
    }
    else
    {
        ret = false;
    }

    return ret;
}
