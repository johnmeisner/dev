/**
 * @file       client_manager.h
 *
 * @brief      Header file for the client manager.
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#ifndef _CLIENT_MANAGER_H_
#define _CLIENT_MANAGER_H_


#include "error.h"
#include "CotaCommonTypes.h"
#include "client.h"
#include "client_avl_tree.h"

// Several assumptions are made in the code based on
//   the fact that all of these states have an
//   equal number of clients. This reduced complexity
//   and programming time.
#define NUM_DISC_CLIENTS   MAX_NUM_CLIENTS       
#define NUM_JOIN_CLIENTS   MAX_NUM_CLIENTS      
#define NUM_TPC_CLIENTS    MAX_NUM_CLIENTS      
#define NUM_RDY_CLIENTS    MAX_NUM_CLIENTS

typedef enum _clientStateType_t
{
    CLIENT_STATE_DISC = 0,
    CLIENT_STATE_JOINED,
    CLIENT_STATE_TPC_CFN,
    CLIENT_STATE_READY
} clientStateType_t;

/**
 * @brief A client state contains state information (duh).  That data
 *         structure that holds and organizes the clients, and a
 *         c-style name that is useful for debugging.
 */
typedef struct _clientState_t
{
    clientStateType_t  type;        ///< A Type identifier 
    char              *cname;       ///< C string name
    clientTree_t      *tree;        ///< Points to the head of the tree.
} clientState_t;

clientState_t *GetDiscState(void);
clientState_t *GetJoinState(void);
clientState_t *GetTpcState(void);
clientState_t *GetReadyState(void);


cotaError_t       InitClientManager(void);
ShrtAddr_t        GetNewShortId(void);



#endif // #ifndef _CLIENT_MANAGER_H_
