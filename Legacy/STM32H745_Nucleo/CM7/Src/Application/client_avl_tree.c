/**
 * @file       client_avl_tree.c
 *
 * @brief      An AVL binary search module for organizing clients 
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/*
  If this were C++, this would be a fantastic use for templates. But, it isn't, so the tree
  is hard-coded to work with clients and client nodes. It was deemed undesired to abstract
  that away, in the interest of development time and simplicity of result.

  For this code to work, a client MUST always have a shortId and a longId. The tree is
  organized by longId, and shortId is assigned outside of the scope of this module.
 */

/*******************************************************************************************
 * Includes
 *******************************************************************************************/
#include <stdlib.h>
#include <string.h> 
#include "client_avl_tree.h"
#include "error.h"
#include "orion_config.h"
/*******************************************************************************************
 * Defines 
 ******************************************************************************************/

/*******************************************************************************************
 * Typedefs
 ******************************************************************************************/

/*******************************************************************************************
 * Private Variable Declarations
 *******************************************************************************************/

/*******************************************************************************************
 * Private Function Declarations 
 *******************************************************************************************/

/*
 * Memory management functions
 */
static inline bool isNullNode(clientNode_t *node);
static cotaError_t createNode(clientNode_t **node, clientTree_t *tree);
static cotaError_t destroyNode(clientNode_t **nodeToDestroy);
static cotaError_t getNextValidIndex(clientTree_t *tree);
static void copyClosestNodeToCurrentNode(clientNode_t **node);

/*
 * Tree functions - Used for balancing the AVL tree. 
 */
static int16_t       treeMaxHeight         (clientNode_t * );
static int16_t       treeHeightDifference  (clientNode_t * );
static clientNode_t* treeRotateRR          (clientNode_t * );
static clientNode_t* treeRotateLL          (clientNode_t * );
static clientNode_t* treeRotateLR          (clientNode_t * );
static clientNode_t* treeRotateRL          (clientNode_t * );
static clientNode_t* treeBalance           (clientNode_t **);

/*
  A major consideration for a client tree is that we don't want to worry about
  memory fragmentation with sequential calls to malloc() and free(). To get
  around this, we create a static "looking" tree with a call to malloc. It is
  then understood that programmers shall not free() this memory during operation,
  thus eliminating fragmentation concerns.

  Commented out for now, post-CES we will want to make malloc calls to place memory
  at specified locations in memory. 
 */
//void CreateStaticTree(clientTree_t *tree, uint16_t size)
//{
//    // Allocate space in the tree's node array. This is where all 'static' data is held
//    tree->nodeArray = (clientNode_t *) calloc(size, sizeof(clientNode_t));
//
//    // Due to a weird-ism of IAR, calloc initializing it's data to zero
//    // So, we will do it manually 
//    memset((void *)tree->nodeArray, 0, sizeof(clientNode_t)*size);
//    
//    for (uint16_t i = 0; i < size; i++)
//    {
//        if (!isNullNode(&tree->nodeArray[i]))
//            while(1);
//    }
//        
//    tree->size = size;
//    tree->lastClient = 0;
//    tree->head = NULL;
//}

/*
  Destroying a tree DOES NOT de-allocate the memory for a tree.
  Instead, it deletes all the data from the nodeArray. Was useful for
  debugging.
 */
void DestroyTree(clientTree_t *tree)
{   
    memset(((void *)tree->nodeArray), 0, (sizeof(clientNode_t) * tree->size));
    tree->head = NULL;
}

/**
 * @brief This function inserts a node into a client tree. If the tree is empty,
 *        that node will be inserted at the tree's head.
 *
 * @note  This is an implementation of an AVL tree, An AVL tree will re-balance itself
 *          when the node is inserted. The simple equation is to traverse the tree until 
 *          a place to insert has been found, insert the node, then re-balance the tree.
 *          As we move back up the stack, we re-balance along the way.
 *
 * @param key The client data that is being inserted into the tree
 * @param node The node that this function is recursing to search the tree.
 * @param tree The tree that holds where the memory should be allocated.
 * @return return COTA_ERROR_NONE on success, and COTA_ERROR_CLIENT_ALREADY_REGISTERED on failure
 */
cotaError_t InsertNode(client_t key, clientNode_t **node, clientTree_t *tree)
{
    cotaError_t   retVal;
    
    if ( *node == NULL) // This indicates a root node
    {
        // This step finds the next chunk of allocated memory. 
        retVal = createNode(node, tree);
        if (retVal != COTA_ERROR_NONE)
            return retVal;
    
        (*node)->client = key;
        (*node)->left   = NULL;
        (*node)->right  = NULL;
    }
    else if (LongCmp(key.longId, (*node)->client.longId) < 0) // key < node.client
    {
        retVal = InsertNode(key, &(*node)->left, tree);
        (*node) = treeBalance(node);
    }
    else if (LongCmp(key.longId, (*node)->client.longId) > 0) // key > node.client
    {
        retVal = InsertNode(key, &(*node)->right, tree);
        (*node) = treeBalance(node);
    }else{ // key.longId == *node->client.longId
        retVal = COTA_ERROR_CLIENT_ALREADY_REGISTERED;
    }
  
    return retVal;  
}


/**
 * @brief The recursive function that searches a tree for the target client to remove.
 * @param nodeToRemove The node that is recursively acted on to search the tree.

 * @param oclient If not NULL, The client data will be copied to this pointer
 *        before being destroyed. 
 *
 * @return Returns the client node that needs to be assigned up that chain.
 */
cotaError_t DeleteNode(ExtdAddr_t key, clientNode_t **node, client_t *oclient)
{
    clientNode_t *tempNode; 
  
    if ( *node != NULL )
    {
        if (LongCmp(key, (*node)->client.longId) < 0)
        {
            DeleteNode(key, &(*node)->left, oclient);
            (*node) = treeBalance(node);
        }
        else if (LongCmp(key, (*node)->client.longId) > 0)
        {
            DeleteNode(key, &(*node)->right, oclient);
            (*node) = treeBalance(node);
        }
        else // The node to be removed is found in the tree 
        {
            if (((*node)->left == NULL) && ((*node)->right == NULL ))
            {
                if(oclient != NULL)
                    memcpy((void *) oclient,(void *) &(*node)->client, sizeof(client_t));
                
                destroyNode(node);
                
            } 
            else if ((*node)->left == NULL)
            {
                if(oclient != NULL)
                    memcpy((void *) oclient,(void *) &(*node)->client, sizeof(client_t));
                
                tempNode = (*node);
                (*node) = (*node)->right;
                destroyNode(&tempNode);
                
                
            }
            else if ((*node)->right == NULL)
            {
                if(oclient != NULL)
                    memcpy((void *) oclient, (void *) &(*node)->client, sizeof(client_t));
                
                tempNode = (*node);
                (*node) = (*node)->left;
                destroyNode(&tempNode);
                
            }
            else
            {
                copyClosestNodeToCurrentNode(node);
        
                /*
                 * node now its equivalent to left-most node of it's right branch.
                 *   This is the node whose data is closest to the node that we are deleting and 
                 *   the tree remains correct after removal. After we re-balance up the stack,
                 *   the tree will also remain balanced.
                 */

                DeleteNode((*node)->client.longId, &(*node)->right, oclient);                
                (*node) = treeBalance(node);
                /*
                 * After the step above, the tree has two copies of the same node. We 
                 *   must remove the node we copied from. 
                 */
            }
        }
    }
    else
    {
        return COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND;
    }
    
    return COTA_ERROR_NONE;
}

/**
 * @brief This is a helper function for DeleteNode. This fcn gets the closest node to the param node
 *         from within it's right branch.
 * @param node The right node of the curent node. The node to the furthest left of this point contains 
 *          the closest neighbor.
 */
static void copyClosestNodeToCurrentNode(clientNode_t **node)
{
    clientNode_t *temp;
  
    temp = (*node)->right;
 
    /*
     * We assume that that node is not NULL because it is called from a 
     *   function that already checks that.
     */
    while (true)
    {
        if (temp->left != NULL)
        {
            temp = temp->left;
        }
        else
        {
            break;
        }
    }

    // Pointer arithmetic is avoided by copying node clientData.
    memcpy((void *)(&(*node)->client), (void *)(&temp->client), sizeof(client_t));
}

// Because the tree is organized by longId, this function must walk through the tree
//  to check the client's shortId. This inefficiency is the reason searching by longId
//  is preferred. 
bool SearchTreeShortId(ShrtAddr_t key, clientNode_t *node, client_t *oclient)
{
    bool ret = false;
    
    if( node != NULL )
    {
        if (key == node->client.shortId)
        {
            if (oclient != NULL)
                memcpy((void *) oclient, (void *) &(node->client), sizeof(client_t));
            
            ret = true;
        }
        
        ret = SearchTreeShortId(key, node->left, oclient);

        // If going down the left node returned true, don't
        //  overwrite it the output by searching down the
        //  right path
        if (!ret)
            ret = SearchTreeShortId(key, node->right, oclient);
    }

    return ret;
}

/**
 * @brief This function searches the tree by shortId and copys the found client
 * @param key The id of the client that the user wishes to locate.
 * @param node  The node that the function is recursively searched on. Start with a head node
 * @param oclient When a client is found, it's data will be memcpy'd to this address unless null
 * @return COTA_ERROR_NONE on success, COTA_ERROR_COULD_NOT_BE_FOUND on failure to find the desired node
 */
bool SearchTree(ExtdAddr_t key, clientNode_t *node, client_t *oclient)
{
    bool ret = false;
    
    if ( node != NULL )
    {    
        if ( LongCmp(key, node->client.longId) == 0 )
        {
            if(oclient != NULL)
                memcpy(((void *)oclient), (&node->client), sizeof(client_t));

            ret = true;
        }
        else if ( LongCmp(key, node->client.longId) < 0 ) // key < client.longId
        {
            ret = SearchTree( key, node->left, oclient);
        }
        else if ( LongCmp(key, node->client.longId) > 0 ) // key > client.longId
        {
            ret = SearchTree( key, node->right, oclient);
        }
    }
    
    return ret;
}


/**
 * @brief Updates all data in a client object. Handle with care.
 * @param key The shortId that distinquishes a client
 * @param node The node that this function is recursively searched on. Start with a head node.
 * @param client pointer to the client that has contains all reference data to update with. If
 *         NULL is passed in, no action is taken. 
 * @return  COTA_ERROR_NONE on success, COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND on failure 
 */
cotaError_t UpdateClient(ExtdAddr_t key, clientNode_t *node, client_t *client)
{
    cotaError_t ret; 

    ret = COTA_ERROR_NONE;
    
    if ( node != NULL)
    {    
        if (LongCmp(key, node->client.longId) == 0)
        {
            // We all copy all data from the passed in client. This puts the power
            //  in the programmers hands to break everything. 
            if (client != NULL)
                memcpy((void*) &(node->client), (void *) &client, sizeof(client_t));
        }
        else if (LongCmp(key, node->client.longId) < 0)
        {
            ret = UpdateClient( key, node->left, client);
        }
        else if (LongCmp(key, node->client.longId) > 0)
        {
            ret = UpdateClient( key, node->right, client);
        }
    }
    else
    {
        ret = COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND;
    }
    
    return ret;
}

/*
  This function is designed to be a general-use "do the thing to all the clients"
    function in this API. This function traverses the tree and does 'func' onto
    each client it finds.
 */
void WalkTree(clientNode_t *node, void (*func)(client_t client))
{
    if (node != NULL)
    {
        func(node->client);

        WalkTree(node->left, func);
        WalkTree(node->right, func);
    }
}

/**
 * @brief Sets #clientTree_t.lastClient to the next avaliable client location
 * @return COTA_ERROR_NONE on success, COTA_ERROR_MAX_NUM_CLIENTS_STORED on failure. 
 */
static cotaError_t getNextValidIndex(clientTree_t *tree)
{
    uint16_t init;
  
    init = tree->lastClient;
    
    while (!isNullNode(&tree->nodeArray[tree->lastClient]))
    {
        if (tree->lastClient < tree->size)
        {
            tree->lastClient++;

            // If we reach here, we've looped over all index less
            //  than MAX_NUM_CLIENTS and a open space for a new
            //  client was not found.
            if (tree->lastClient == init)
            {
                return COTA_ERROR_MAX_NUM_CLIENTS_STORED;
            }
        }
        else
        {
            tree->lastClient = 0;
        }
    }
  
    return COTA_ERROR_NONE;
}

/**
 * @brief Determines if a node is a termination node 
 * @param node A node that we want to know if it is a termination node
 * @return true if its a null node (all bytes are 0), false if non-zero.
 */
static inline bool isNullNode(clientNode_t *node)
{
    uint8_t *temp = (uint8_t *) node;
    for (uint8_t i = 0; i < sizeof(clientNode_t); i++)
    {
        if( temp[i] != 0 )
            return false;
    }

    return true;
}

/**
 * @brief Searches through a node array locates the next non-term node available,
 *         then sets *node to point at that spot in memory. 
 * @note We are working with statically allocated memory and doing our own memory management. 
 *         For this reason, the memory management will be abstracted by functions that look
 *         like they are dynamically allocating memory. (like this one)
 * @param node A pointer of a node that needs to be allocated
 * @param tree The tree that holds where the data is allocated
 * @return COTA_ERROR_NONE on success, and COTA_ERROR_MAX_NUM_CLIENTS_STORED if memory could not be allocated
 */
static cotaError_t createNode(clientNode_t **node, clientTree_t *tree)
{
    cotaError_t ret;
  
    ret = getNextValidIndex(tree);
    if (ret != COTA_ERROR_NONE)
        return ret;

    *node = &(tree->nodeArray[tree->lastClient]);
  
    return COTA_ERROR_NONE;
}

/**
 * @brief Makes the inputted node a termination node and sets the node to null
 * @param nodeToDestroy The node that needs to be destroyed
 * @return COTA_ERROR_NONE
 */
static cotaError_t destroyNode(clientNode_t **nodeToDestroy)
{
    cotaError_t ret;

    ret = COTA_ERROR_NONE;
    
    if ( *nodeToDestroy != NULL)
    {
        // Set all data in the clientNode_t to 0 to mark it as
        //  unset data.
        memset((void *)*nodeToDestroy, 0, sizeof(clientNode_t));
        (*nodeToDestroy) = NULL;
        ret = COTA_ERROR_NONE;
    }
    else
    {
        ret = COTA_ERROR_FAILED_TO_REMOVE_NODE;
    }
    return ret;
}

/**
 * @brief Calculate the maximum height of a tree starting at a node.
 * @param node The root node the function recursively works on. 
 * @return the maximum height between a nodes left and right tree
 */
static int16_t treeMaxHeight(clientNode_t *node)
{
    int16_t maxHeight;
    int16_t leftHeight;
    int16_t rightHeight;
  
    maxHeight = 0;
  
    if (node != NULL)
    {
        leftHeight  = treeMaxHeight(node->left);
        rightHeight = treeMaxHeight(node->right);
      
        maxHeight = MAX(leftHeight, rightHeight) + 1;
    }
    
    return maxHeight;
}

/**
 * @brief Returns the difference between the maximum height of the left and right branches
 * @param node The node that the function recurses on 
 * @return the difference between the left and right branches of a node
 */
static int16_t treeHeightDifference(clientNode_t *node)
{
    return (treeMaxHeight(node->left) - treeMaxHeight(node->right));
}

/**
 * @brief Performs a RR rotation on a given clientNode
 * @param clientNode The node that the function recurses on 
 * @return The clientNode that becomes the new node from where this function was called.
 */
static clientNode_t* treeRotateRR(clientNode_t *node)
{
    clientNode_t *temp;
    temp = node->right;
    node->right = temp->left;
    temp->left  = node;
  
    return temp;
}

/**
 * @brief Performs a LL rotation on a given clientNode
 * @param clientNode The node that the function recurses on 
 * @return The clientNode that becomes the new node from where this function was called.
 */
static clientNode_t* treeRotateLL(clientNode_t *node)
{
    clientNode_t *temp;
    temp = node->left;
    node->left = temp->right;
    temp->right = node;
  
    return temp;
}

/**
 * @brief Performs a LR rotation on a given clientNode
 * @param clientNode The node that the function recurses on 
 * @return The clientNode that becomes the new node from where this function was called.
 */
static clientNode_t* treeRotateLR(clientNode_t *node)
{
    clientNode_t *temp;
    temp = node->left;
    node->left = treeRotateRR(temp);
  
    return treeRotateLL(node);
}

/**
 * @brief Performs a RL rotation on a given clientNode
 * @param clientNode The node that the function recurses on 
 * @return The clientNode that becomes the new node from where this function was called.
 */
static clientNode_t* treeRotateRL(clientNode_t *node)
{
    clientNode_t *temp;
    temp = node->right;
    node->right = treeRotateLL(temp);
  
    return treeRotateRR(node);
}

/**
 * @brief This function balances a tree (AVL tree)
 * @param clientNode The node that the function recurses on 
 * @return The clientNode that becomes the new node from where this function was called.
 */
static clientNode_t* treeBalance(clientNode_t **node)
{
    int16_t balanceFactor;
  
    balanceFactor = treeHeightDifference(*node);
    if (balanceFactor > 1)
    {
        if ((treeHeightDifference((*node)->left) > 0 ))
        {
            (*node) = treeRotateLL(*node);
        }
        else
        {
            (*node) = treeRotateLR(*node);
        }
    }
    else if ( balanceFactor < -1 )
    {
        if ((treeHeightDifference((*node)->right) > 0 ))
        {
            (*node) = treeRotateRL(*node);
        }
        else
        {
            (*node) = treeRotateRR(*node);
        }
    }
  
    return (*node);
}
