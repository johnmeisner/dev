/****************************************************************************//**
* @file      cli_task.h
*
* @brief     A header for the cli task that is much better than Scott's.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _CLI_TASK_H
#define _CLI_TASK_H
#include "FreeRTOS.h"
#include "error.h"

#define MAX_OUTPUT_LENGTH   MAX_UART_MSG_SIZE

static const char * SUCCESS_MSG = "SUCCESS\r\n";
static const char * FAILURE_MSG = "FAILURE\r\n";
static const char * ENABLED_MSG = "enabled";
static const char * DISABLED_MSG = "disabled";

uint16_t CliSendCommand(uint8_t* buf, uint8_t size);
void CreateCliTask(void);
uint16_t CliGetMessage(uint8_t * buf, uint8_t size);
cotaError_t PostCliMsgForTransmit(uint8_t *pData, uint16_t size);
#endif
