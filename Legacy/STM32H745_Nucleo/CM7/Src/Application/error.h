/**
* @file       error.h
*
* @brief      Cota error logging header file.
*
* @note       Do not use prototypes defined in this header.  Use the MACROS.
*             We want to able to save memory by removing error handling.  
*             By using the MACRO's, we can easily remove error handling by not 
*             including COTA_ERROR_ENABLED in the compile line.
*
* @todo This is just a simple shell of what should be a larger debug message handler that includes
        a separate task to queue up messages, and masks to prioritize messages.
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
*             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
*/

#ifndef _COTA_ERROR_INCLUDE_H_
#define _COTA_ERROR_INCLUDE_H_

typedef enum _cotaError_t
{
    COTA_ERROR_NONE                           = 0,  ///< Indicates a Non-Error from a function call
    COTA_ERROR_HAL                            = 1,  ///< Indicates there was an error from a HAL driver
    COTA_ERROR_QUEUE_FAILED                   = 2,  ///< Indicates the error handler failed to create its queue
    COTA_ERROR_TASK_FAILED                    = 3,  ///< Indicates the error handler failed to create its task.
    COTA_ERROR_SPI_TRANSFER_FAILED            = 4,  ///< A SPI transfered has failed
    COTA_ERROR_SPI_SIMUL_TRANS_FAILED         = 5,  ///< A simlultaneous SPI transfered has failed
    COTA_ERROR_UVP_SIMUL_READ_TO_LARGE        = 6,  ///< A simulataneous read of several UVP's has a buffer that is too large.
    COTA_ERROR_UART_TX_FAILED                 = 7,  ///< A UART transmit failed
    COTA_ERROR_UVP_LOCK_FAILED                = 8,  ///< An attempt to lock a UVP clock failed 
    COTA_ERROR_UVP_TEST_FAILED                = 9,  ///< The UVP test failed.
    COTA_ERROR_UVP_REG_VALUE_NOT_IN_MASK      = 10, ///< A UVP register value did not fit in it's mask.
    COTA_ERROR_AMB_NOT_ENABLED                = 11, ///< An attempt was made to enable a UVP on an AMB not enabled.
    COTA_ERROR_UVP_UNKNOWN                    = 12, ///< An attempt was made to select an unknown UVP number
    COTA_ERROR_UVP_WAIT_FOR_TRANSMIT_TIMEOUT  = 13, ///< An error occurred while waiting for a UVP to transmit
    COTA_ERROR_UVP_WAIT_FOR_PHASE_DET_TIMEOUT = 14, ///< An error occurred while wait for a phase detect from a UVP
    COTA_ERROR_UVP_ENABLE                     = 15, ///< An error occurred while enabling UVP's
    COTA_ERROR_SYSCLK_FAILED_TO_WRITE_PAGE    = 16, ///< An error occurred while selecting the page for the system clock
    COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG     = 17, ///< An error occurred while writing a register to the system clock
    COTA_ERROR_SYSCLK_FAILED_TO_READ_REG      = 18, ///< An error occurred while reading a register from the system clock
    COTA_ERROR_I2C_INVALID_MULTIPLEXER        = 19, ///< An error occurred because an invalid i2c multiplexer was initialized
    COTA_ERROR_I2C_INVLAID_MULTI_CHANNEL      = 20, ///< An error occurred beacuse an invalid i2c multiplexer channel was intialized
    COTA_ERROR_I2C_FAILED_TO_WRITE_MULTI_CHAN = 21, ///< An error occurred while selecting the channel on an i2c multiplexer
    COTA_ERROR_I2C_FAILED_TO_WRITE            = 22, ///< A failue occurred while writing to an i2c device.
    COTA_ERROR_I2C_FAILED_TO_READ             = 23, ///< A failure occurred while reading from an i2c device
    COTA_ERROR_NOT_IMPLEMENTED                = 24, ///< A function was called that is not implemented.
    COTA_ERROR_INVALID_CCB_TERMOMETEER        = 25, ///< An attempt was made to read a temperature from a thermometer that doesn't exist. 
    COTA_ERROR_BAD_PARAMETER                  = 26, ///< A bad parameter was passed to a function.
    COTA_ERROR_OVER_CRITICAL_TEMPERATURE      = 27, ///< A critical temperature has been exceeded
    COTA_ERROR_CLIENT_MANAGER_INIT_FAILED     = 28, ///< Indicated that Client Manager failed to properly load clients an initialization
    COTA_ERROR_MAX_NUM_CLIENTS_STORED         = 29, ///< The maximum number of clients avaliable has been reached.
    COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND      = 30, ///< Indicates that the client could not be found in the database
    COTA_ERROR_CLIENT_ALREADY_REGISTERED      = 31, ///< The client has already been registered in the database
    COTA_ERROR_NULL_VALUE_PASSED              = 32, ///< Indicates that the function was passed a null value
    COTA_ERROR_FAILED_TO_REMOVE_NODE          = 33, ///< An error that indicates that the clientManager failed to remove the node
    COTA_ERROR_COMM_ACK_TIMER_START_FAIL      = 34, ///< The acknowledgment timers failed to start for the message acknowledgment timeout
    COTA_ERROR_FAILED_TO_CREATE_VAR_QUEUE     = 35, ///< A variable length queue could not be created
    COTA_ERROR_FAILED_TO_SEND_TO_VAR_QUEUE    = 36, ///< A message could not be sent to a variable queue
    COTA_ERROR_CLI_TX_FAILED                  = 37, ///< A message to the CLI could not be sent.
    COTA_ERROR_CONFIG_MUTEX_FAILED_TAKE       = 38, ///< The mutex in the #orion_config.c file failed to take
    COTA_ERROR_CONFIG_MUTEX_FAILED_GIVE       = 39, ///< The mutex in the #orion_config.c file failed to give
    COTA_ERROR_FAILED_TO_SEND_PROXY_MSG       = 40, ///< Failed to send a message to the proxy
    COTA_ERROR_FAILED_TO_PROXY_MSG            = 41, ///< The Ctrl Task failed to parse a proxy message
} cotaError_t;

#ifdef COTA_ERROR_ENABLED
cotaError_t PostCotaError(cotaError_t cotaError);
#define POST_COTA_ERROR(num)      (PostCotaError(num))
#else
#define POST_COTA_ERROR(num) (num)
#endif

#endif
