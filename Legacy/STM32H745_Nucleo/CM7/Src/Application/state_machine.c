/**
 * @file       state_machine.c
 *
 * @brief      State machine implementation
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

#include "state_machine.h"
#include "stdlib.h"

StateMachineState_t* RunStateMachine(StateMachineState_t* currState)
{
  StateMachineState_t*      nextState;
  StateMachineTransition_t* transitionOption;

  nextState = currState;

  transitionOption = currState->transitionOptions;

  while ( transitionOption != NULL)
  {
    // NULL check Condition indicates a termination option
    if (transitionOption->checkCondition == NULL)
    {
        break;
    }
    
    if (transitionOption->checkCondition())
    {
      if (transitionOption->exitFcn)
      {
        transitionOption->exitFcn();
      }
      nextState = transitionOption->nextState;
      if (nextState->entryFcn)
      {
        nextState->entryFcn();
      }
      break;
    }
    transitionOption++;
  }

  return nextState;
}
