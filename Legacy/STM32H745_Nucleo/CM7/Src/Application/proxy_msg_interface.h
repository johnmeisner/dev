/** 
 * @file       proxy_msg_interface.h
 *
 * @brief      This is the header file for proxy_msg_interface.c
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#ifndef _PROXY_MSG_INTERFACE_H_
#define _PROXY_MSG_INTERFACE_H_

#include "error.h"
#include "CotaCommonTypes.h"
#include "proxyHostMsgInf.h"

bool PrxMsgRdy(void);
ProxyHostMsgType_t PrxGetMsg(uint8_t *buf, uint8_t size);

void PrxExtractDiscoveryResp(uint8_t *buf, uint8_t size, PrxHostDiscoveryMsg_t *discResult);
void PrxExtractJoinResp(uint8_t *buf, uint8_t size, PrxHostClientJoinRspMsg_t *joinResult);
void PrxExtractTpcResp(uint8_t *buf, uint8_t size, PrxHostTpcCfn_t *tpcResult);
void PrxExtractQueryCfnResp(uint8_t *buf, uint8_t size, PrxHostCqRecCfn_t *queryResult);
void PrxExtractQueryDataResp(uint8_t *buf, uint8_t size, ClientQueryDataStd_t *queryData);
void PrxExtractDisconnectResp(uint8_t *buf, uint8_t size, PrxHostLeaveNetworkCfn_t *disconnectResult);
void PrxExtractPrxCmdResp(uint8_t *buf, uint8_t size);

cotaError_t PrxSendDisc(void);
cotaError_t PrxSendJoin(ExtdAddr_t clientLongId, ShrtAddr_t clientShortId);
cotaError_t PrxSendTpc(ShrtAddr_t clientShortId);
cotaError_t PrxSendQuery(ShrtAddr_t clientShortId);
cotaError_t PrxSendDisconnect(ShrtAddr_t clientShortId);

#endif
