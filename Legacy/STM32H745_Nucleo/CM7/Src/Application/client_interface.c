/**
 * @file       client_interface.c
 *
 * @brief      Interface for managing clients
 *
 * @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */

/* The work presented in this file is the first example in a change in direction for the
 * architecture methodology of the Orion architecture. Instead of the original intent
 * of mimicking Venus's features and network design, the Orion design will be doing it's
 * own networking and API style.
 *
 * This was chosen for a few reasons:
 *  1) It keeps things simple.
 *     The networking stack is likely to change in rather dramatic ways in the near future.
 *     It is not worth putting a lot of investment making complicated code that will
 *     be thrown away.
 *
 *  2) Better for a COTA API
 *     Simplicity will expose more of the nuts and bolts to the customers/coworkers that
 *     will use the system. At the price of enhanced training, this will create an
 *     API that yields a higher quality of control of the transmitter.
 *
 *  3) Quicker Programming
 *     This code will like be obsolete soon when we start looking at ESLs charging 1000 clients
 *     (to just name one).
 *
 * Two notable repercussions to this decision include increased need for training of the engineers
 * using the system, and breaking the Web Admin. For the sake of creating clean code that can
 * meet out business target, backwards compatibility will considered less vital of a design
 * consideration.
 */

#include <string.h>
#include <stdio.h>
#include "FreeRTOS.h"
#include "client.h"
#include "client_interface.h"
#include "client_manager.h"
#include "client_avl_tree.h"
#include "error.h"
#include "CotaCommonTypes.h"
#include "uart_driver.h"

static void printClient(client_t client);

// Client discovered is the first step, and the first time they are
//  recorded by the transmitter. 
cotaError_t CmClientDiscovered(PrxHostDiscoveryMsg_t discAck)
{
    client_t       client;
    clientState_t *state;

    state = GetDiscState();
    
    client.longId = discAck.extAddr;

    InsertNode(client, &(state->tree->head), state->tree);

    return COTA_ERROR_NONE;
}



// To begin the client's journey through all of the states,
//  the client must decidedly become 'registered' with the device.
//  Once this process begins, the process should be auto magically
//  get the client into the Ready state through a series of
//  message exchanges between the client and the proxy.
//
// TODO add 'CmAddClient' to register a client regardless
//  of whether the client was discovered first, then
//  CmRegisterClient should register clients iff they
//  have been discovered first. 
cotaError_t CmRegisterClient(ExtdAddr_t longId)
{
    client_t client;
    clientState_t *entState;
    clientState_t *extState;

    if (IsLongValid(longId))
    {
        extState = GetDiscState();
        entState = GetJoinState();

        /*
          If the clients was in the discovered clients before
          registering it, the client must first be removed
          from the discovered tree.
         */
        if (SearchTree(longId, extState->tree->head, NULL))
        {
            DeleteNode(longId, &(extState->tree->head), &client);
        
            client.shortId = GetNewShortId();
            client.batteryLevel = 0;
            client.isValid = 1;
    
            InsertNode(client, &(entState->tree->head), entState->tree);
        }
        else
        {
            return COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND;
        }
    }

    return COTA_ERROR_NONE;
}

// Joined clients are input into the system by being discovered.
//  The must first be removed from the Discovery state, and then
//  put into the Joined state.
// todo Remove Join state, it's useless.
cotaError_t CmClientJoined(PrxHostClientJoinRspMsg_t joinAck)
{
    client_t client;
    clientState_t *entState;
    clientState_t *extState;

    if (joinAck.status == 1) // Indicates successful join
    {
        extState = GetJoinState();
        entState = GetTpcState();

        if (DeleteNode(joinAck.extAddr, &(extState->tree->head), &client) == COTA_ERROR_NONE)
        {
            InsertNode(client, &(entState->tree->head), entState->tree);
            return COTA_ERROR_NONE;
        }
        return COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND;
    }
    return COTA_ERROR_BAD_PARAMETER;
}

cotaError_t CmClientTpcCfn(PrxHostTpcCfn_t tpcAck)
{
    client_t client;
    clientState_t *entState;
    clientState_t *extState;

    if (tpcAck.status == 1) // Indicates successful tpc
    {
        extState = GetTpcState();
        entState = GetReadyState();

        SearchTreeShortId(tpcAck.shortAddr, extState->tree->head, &client);

        if (DeleteNode(client.longId, &(extState->tree->head), &client) == COTA_ERROR_NONE)
        {
            InsertNode(client, &(entState->tree->head), entState->tree);
            return COTA_ERROR_NONE;
        }
        return COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND;
    }

    return COTA_ERROR_BAD_PARAMETER;
}

/*
  There are two types of query responses. 
  1. When the ClientQueryTable is sent out, a confirmation message
      is sent from the proxy to the host (PrxHostCqRecCfn_t). This confirmation says
      that the client sent an acknowledgment for the QueryTable.
  2. Sometime time later, the client will send out it's data requested as part of
     the proxy. ClientQueryDataStd_t (got Query type 5) and ClientQueryDataCstm_t
     (for query type 6, which is type 5 but with a buffer full of custom data).
     This is the data that contains the battery levels, received power levels, etc.

  The second one is much more interesting to us.
*/
cotaError_t CmClientQueryData(ClientQueryDataStd_t queryAck)
{
    client_t client;
    clientState_t *state;
    
    state = GetReadyState();

    // Make sure the client is in the data structure before copying it out 
    if (SearchTreeShortId(queryAck.header.clientShortAddr, state->tree->head, &client))
    {
        // Update all client information
        client.isValid = 1;

        // Populate the client query data on a specified client.

        client.qStatus.peakPower       = queryAck.status.peakPower;      
        client.qStatus.avgPower        = queryAck.status.avgPower;
        client.qStatus.peakNetCurrent  = queryAck.status.peakNetCurrent;
        client.qStatus.batteryCharge   = queryAck.status.batteryCharge;
        client.qStatus.statusFlags     = queryAck.status.statusFlags;
              
        client.rssi        = queryAck.header.rssi;
        client.linkQuality = queryAck.header.linkQuality;

        UpdateClient(client.longId, state->tree->head, &client);
    }else{
        // This client was not in the Ready state.
    }
    
    return COTA_ERROR_NONE;
}

cotaError_t CmRemoveClient(ExtdAddr_t longId)
{
    return COTA_ERROR_NONE;
}

static void printClient(client_t client)
{
    char buf[MAX_UART_MSG_SIZE];
    uint16_t size;

    snprintf(buf, MAX_UART_MSG_SIZE, "print longId : 0x%11X\r\n",
             *((uint64_t *) &client.longId));
    
    size = (uint16_t) strlen(buf);

    CotaUartTransmit((uint8_t *) buf, size, portMAX_DELAY);
}

/*
  For now, these functions just print the client lists through
  the UART.

  Later, this data will need to be passed up to other tasks, not
   to mention printing like this isn't great for emb systems
*/
void CmDiscClientList(void)
{
    clientState_t *state;

    state = GetDiscState();
    CotaUartTransmit((uint8_t*) "Disc State:\r\n", 0, portMAX_DELAY);
    WalkTree(state->tree->head, printClient);
}

void CmJoinClientList(void)
{
    clientState_t *state;

    state = GetJoinState();

    CotaUartTransmit((uint8_t*) "Join State:\r\n", 0, portMAX_DELAY);
    WalkTree(state->tree->head, printClient);
}

void CmTpcClientList(void)
{
    clientState_t *state;

    state = GetTpcState();

    CotaUartTransmit((uint8_t*) "Tpc State:\r\n", 0, portMAX_DELAY);
    WalkTree(state->tree->head, printClient);
}

void CmRdyClientList(void)
{
    clientState_t *state;

    state = GetReadyState();

    CotaUartTransmit((uint8_t*) "Ready State:\r\n", 0, portMAX_DELAY);
    WalkTree(state->tree->head, printClient);
}


/*
  For now, determining the client that receives a join, tpc or query
  message we will simply 'pop' the head off of the tree. In cases of
  unstable communication, this is not a reliable method for send
  messages to each client and something smarter will need to be
  done. If the head doesn't respond, then no other clients within the
  tree will receive messages with this code.

  TODO be smarter, avoid deadlock!
 */
bool CmGetJoinClient(ExtdAddr_t *longId, ShrtAddr_t *shortId)
{
    clientState_t *state = GetJoinState();

    if (longId != NULL && shortId != NULL)
    {
        if (state->tree->head != NULL)
        {
            memcpy((void *) longId, (void *) &(state->tree->head->client.longId), sizeof(ExtdAddr_t));
            memcpy((void *) shortId, (void *) &(state->tree->head->client.shortId), sizeof(ShrtAddr_t));

            return true;
        }
    }
    
    return false;
}

bool CmGetTpcClient(ShrtAddr_t *shortId)
{
    clientState_t *state = GetTpcState();

    if (shortId != NULL)
    {
        if (state->tree->head != NULL)
        {
            memcpy((void *) shortId, (void *) &(state->tree->head->client.shortId), sizeof(ShrtAddr_t));
            return true;
        }
    }

    return false;
}

/*
  Ready clients are different and significantly more complicated than
  the other 'getClient()' functions. This will not be implemented
  now. Later, we will need to send query messages to clients based on
  the last time they've responded to queries (or something else, idk).
 */
bool CmGetRdyClient(ShrtAddr_t *shortId)
{
    clientState_t *state = GetReadyState();

    if (shortId != NULL)
    {
        if (state->tree->head != NULL)
        {
            memcpy((void *) shortId, (void *) &(state->tree->head->client.shortId), sizeof(ShrtAddr_t));
            return true;
        }
    }

    return false;
}


#ifdef NO_COMP
/**
 * @brief Adds a new client to the clientManager.
 * @param newClientId the longId of the client we want to add to the database.
 * @param shortId - An optional output parameter that returns the  short ID that the client was set to.
 * @return COTA_ERROR_NONE for success, COTA_ERROR_CLIENT_ALREADY_REGISTERED if client is already in the tree, 
 *           or COTA_ERROR_MAX_NUM_CLIENTS_STORED if memory is full
 */
cotaError_t CmAddClient(ExtdAddr_t newClientId, ShrtAddr_t *shortId)
{
    client_t tempClient;

    tempClient.longId = newClientId;
    tempClient.shortId = GetNewShortId();
    tempClient.batteryLevel = 0;
    tempClient.state = CLIENT_ERROR;
    tempClient.isValid = 1;

    (*shortId) = tempClient.shortId;
  
    return InsertNode(tempClient, GetTreeHead());
}

/**
 * @brief this function removes clients from the clientManager.
 * 
 *        A user can insert 0 for a shortId, or a 0 for a longId.
 *          If a user gives 0 for both shortId and longId, a error will be
 *          returned. 
 *
 * @param longIdToRemove - A longId that needs to be removed. 
 * @param shortIdToRemove - A shortId that needs to be removed.
 * @return COTA_ERROR_NONE on success, COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND on error
 */
cotaError_t CmRemoveClient(ExtdAddr_t longId, ShrtAddr_t shortId)
{
    cotaError_t retVal;
    client_t    client;
    bool        longIsValid;
    bool        shortIsValid;

    longIsValid = IsLongValid(longId);
    shortIsValid = IsShortValid(shortId);
    
    retVal = COTA_ERROR_NONE;
    
    if ( !longIsValid && !shortIsValid )
    {
        retVal = COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND;
    }
    else if ( !longIsValid )
    {
        retVal = CmGetClientInfoByShortId(shortId, &client);
        retVal = (retVal == COTA_ERROR_NONE) ? DeleteNode(client, GetTreeHead()) : retVal;
    }
    else if ( !shortIsValid )
    {
        retVal = CmGetClientInfoByLongId(longId, &client);
        retVal = (retVal == COTA_ERROR_NONE) ? DeleteNode(client, GetTreeHead()) : retVal;
    }
    else // Both ids are valid
    {
        // The choice to pick longId is aribtrary, but necesarry to ensure the longId is correctly
        //   associated with the shortId.
        retVal = CmGetClientInfoByLongId(longId, &client);
        if (client.shortId == shortId)
        {
            retVal = (retVal == COTA_ERROR_NONE) ? DeleteNode(client, GetTreeHead()) : retVal;
        }
        else
        {
            retVal = COTA_ERROR_CLIENT_COULD_NOT_BE_FOUND;
        }
    }
    
    return retVal;
}

/**
 * @brief Copies the local (protected) version of the client list to a local copy of the user.
 * @param outClientList pointer that points the users local copy of the clientList
 * @return COTA_ERROR_NONE on success, COTA_ERROR_NULL_VALUE_PASSED on failure.
 */
cotaError_t CmGetClientList(clientList_t* outClientList)
{
    if (outClientList == NULL)
        return COTA_ERROR_NULL_VALUE_PASSED;
  
    PopulateClientList(GetTreeHead());
  
    memcpy((void*) outClientList, (void *) GetClientList(), sizeof(clientList_t));
  
    return COTA_ERROR_NONE;
}

/**
 * @brief Given a shortId, this function will fill an output client pointer with it's client data.
 * @param shortId The shortId of the client that we want to get data on.
 * @param  outClient A pointer that points to a users local client that the data will be copied to.
 * @return COTA_ERROR_NONE on success, COTA_ERROR_COULD_NOT_BE_FOUND on failure
 */
cotaError_t CmGetClientInfoByShortId(ShrtAddr_t shortId, client_t *outClient)
{
    clientHead_t *head;

    head = GetTreeHead();
    
    return SearchTreeByShortId(shortId, head[BRANCH_SHORT_ID].node, outClient);
}

/**
 * @brief Given a longId, this function will fill an output client pointer with it's client data.
 * @param longId The longId of the client that we want to get data on.
 * @param  outClient A pointer that points to a users local client that the data will be copied to.
 * @return COTA_ERROR_NONE on success, COTA_ERROR_COULD_NOT_BE_FOUND on failure
 */
cotaError_t CmGetClientInfoByLongId(ExtdAddr_t longId, client_t *outClient)
{
    clientHead_t *head;

    head = GetTreeHead();
    
    return SearchTreeByLongId(longId, head[BRANCH_LONG_ID].node, outClient);
}

/**
 * @brief This function will update the state of the client with the corresponding shortId 
 * @param shortId shortId The shortId of the client that we want to get data on
 * @param newState This function updates the state of a client.
 * @return COTA_ERROR_NONE on success, COTA_ERROR_COULD_NOT_BE_FOUND on failure
 */
cotaError_t CmUpdateClientState(ShrtAddr_t shortId, clientState_t newState)
{
    clientHead_t *head;

    head = GetTreeHead();
    
    return ChangeClientState(shortId, head[BRANCH_SHORT_ID].node, newState);
}

/**
 * @brief Calculates and populates a client schedule and copys it to an output parameter
 * @param outSchedule An output pointer to a schedule type that will be used to calculate TPS
 * @return COTA_ERROR_NONE
 */
cotaError_t CmGetSchedulde(clientSchedule_t *outSchedule)
{
    PopulateSchedulableClients();
 
    memcpy((void*) outSchedule, (void *) GetClientSchedule(), sizeof(clientSchedule_t));
  
    return COTA_ERROR_NONE;
}


/**
 * @brief Searches for a client by shortId and sets it's battery level. 
 */
cotaError_t CmSetBatteryLevel(ShrtAddr_t shortId, uint8_t newBatteryLevel)
{
    clientHead_t *head;
    
    head = GetTreeHead();

    return ChangeClientBattery(shortId, head[BRANCH_SHORT_ID].node, newBatteryLevel);
}
#endif //#ifdef 0/1
