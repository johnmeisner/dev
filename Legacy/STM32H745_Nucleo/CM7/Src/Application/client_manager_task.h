/**
* @file       client_manager_task.c
*
* @brief      Header for the client_manager task
*
* @copyright  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
*             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
*             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
*    
*/


#ifndef _CLIENT_MANAGER_TASK_H_
#define _CLIENT_MANAGER_TASK_H_

#define "CotaCommonTypes.h"

/** The maximum amount of space a single message to the client manager can be.*/
#define CLI_MAN_MAX_SIZE  24  

typedef enum _CManMsgTypes_t
{
    CLI_MAN_DISC_RESP = 0,
    CLI_MAN_JOIN_RESP,
    CLI_MAN_TPC_RESP,
    CLI_MAN_QUERY_RESP,
} CliManMsgTypes_t;

typedef struct _CManMsg_t
{
    CliManMsgTypes_t   type;
    uint8_t            msgStruct[CLI_MAN_MAX_SIZE];
} CliManMsg_t;

typedef struct _CManJoinMessage_t
{
    ExtdAddr_t longId;
} CManJoinMessage_t;

typedef struct _CManTpcMessage_t
{
    ExtdAddr_t longId;
} CManTpcMessage_t;

typedef struct _CManQueryMessage_t
{
    ExtdAddr_t longId;
} CManQueryMessage_t;

void CreateClientManagerTask(void);


#endif // #ifndef _CLIENT_MANAGER_TASK_H_

