/****************************************************************************//**
* @file      sysclk_driver.h
*
* @brief     Header file to control the system clock that is sent to the AMB's
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _SYSCLK_DRIVER_H_
#define _SYSCLK_DRIVER_H_
#include "error.h"
void InitSysClkDriver(void);
cotaError_t SystemClockInit(void);
cotaError_t ChangeFreq(uint32_t freq);
void SysClock_I2C_TxCpltCallback(I2C_HandleTypeDef *hi2c);
void SysClock_I2C_RxCpltCallback(I2C_HandleTypeDef *hi2c);
#endif // #ifndef _SYSCLK_DRIVER_H_
