/****************************************************************************//**
* @file      fan_driver.h
*
* @brief     Header file to read and write to the fan driver chip
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _FAN_DRIVER_H_
#define _FAN_DRIVER_H_
#include "error.h"

void InitializeFanDriver(void);
ambMask_t FansOk(void);

#endif  //#ifndef _FAN_DRIVER_H_

