/****************************************************************************//**
* @file      uvp_driver.c
*
* @brief     Driver to read and write to UVP's (The asic that controls receive and transmission)
*            The UvpInit function is geared toward SWS1411 UVPv2 Variant 1.
*
*            @todo Uvp temperatures are needed for fan speed updates and
*                  to initiate calibration.    We do not want to get UVP temperatures during TPS,
*                  so we need the top level state machine to call UpdateUvpTemperatureCache(int16_t* maxDelta) 
periodically. (At least once every 25 seconds)
*                  This will cause the temperatures to be read and cached so the fan driver
*                  can use them.  Also, the maxDelta can be used to initiate a calibration if
*                  necessary.
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "FreeRTOS.h"
#include "task.h"
#include "string.h"
#include "main.h"
#include "semphr.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "spi_driver.h"
#include "amb_control.h"
#include "uart_driver.h"
#include "stdio.h"
#include "stm32h7xx_hal_uart.h"
#include "error.h"
#include "stdlib.h"

#define UVP_PAGE_BUF_SIZE               2       ///< The size of the message we need to send to set the page index
#define UVP_32BIT_CHUNK_BUF_SIZE        5       ///< The size of the data in bytes to write or receive a 32 bit chunk of UVP memory.  5 bytes are needed because the first byte specfies the column address in UVP memory to read and write from.
#define UVP_64BIT_CHUNK_BUF_SIZE        9       ///< The size of the data in bytes to write or receive a 64 bit chunk of UVP memory.  9 bytes are needed because the first byte specfies the column address in UVP memory to read and write from.
#define UVP_READ_MASK                   0x80    ///< The mask on the first byte that indicates a read operation is initiated
#define UVP_WRITE_PAGE_INDEX            0x7F    ///< The column address of the page index.
#define VCOCFS_VALUE_START              32      ///< Low value for the voltage control oscilator capcitor frequency selection bits
#define VCOCFS_VALUE_END                48      ///< High value for the voltage control oscilator capcitor frequency selection bits
#define AMU_ENABLED                     1       ///< Value of the AMU enable register indicating the AMU is enabled.
#define AMU_DISABLED                    0       ///< Value of the AMU enable register indicating the AMU is disabled.
#define UVP_TEMP_SIGN_BIT               0x1000  ///< The sign bit for temperature values
#define UVP_TEMP_SIGN_EXT               0xE000  ///< The sign extension for temperature values
#define UVP_TEMP_SLOPE_NUM              155     ///< The numerator of the slope needed to convert UVP temperatures to centidegrees C
#define UVP_TEMP_SLOPE_DEN              100     ///< The denomentator of the slope needed to convert UVP temperatures to centidegrees C
#define UVP_TEMP_OFFSET                 4355    ///< The offset needed to convert UVP temperatures to centidegrees C

extern UART_HandleTypeDef huart3;

static SemaphoreHandle_t gUvpRecursiveMutex;      ///< A recursive mutex used to make this driver thread safe
static StaticSemaphore_t gUvpRecursiveMutexBuff;  ///< A buffer holding the information about #gUvpRecursiveMutex
static int16_t uvpTempCache[TOTAL_UVPS];          ///< A cache to hold the last read temperatures from the UVP

/**
 * @brief Intializes the mutex used to keep the UVP driver thread safe
 */
void InitUvpDriver(void)
{
    gUvpRecursiveMutex = xSemaphoreCreateRecursiveMutexStatic( &gUvpRecursiveMutexBuff);
    while (gUvpRecursiveMutex == NULL);  
}


/**
 * @brief Writes the page index number to UVP's on a particular AMB.
 * 
 * @param pageIndex   The number of the page that subsequent UVP reads and writes will be referring to.
 * @param ambNum      The zero based number of the AMB containing the UVP's to write to.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
static cotaError_t uvpWritePage(uint8_t pageIndex, ambNum_t ambNum)
{
    cotaError_t retVal = COTA_ERROR_NONE;
    uint8_t inBuff[UVP_PAGE_BUF_SIZE];
    uint8_t outBuff[UVP_PAGE_BUF_SIZE];
    uint32_t ambSelMask = NUM_TO_MASK(ambNum);
      
    AmbSpiEnable(ambSelMask);
    
    outBuff[0] = UVP_WRITE_PAGE_INDEX;
    outBuff[1] = pageIndex;

    retVal = SpiTransfer(AmbGetSpiHandle(ambNum), outBuff, inBuff, UVP_PAGE_BUF_SIZE);
    
    AmbSpiEnable(AMB_DISABLE_ALL);
    
    return retVal;
}

/**
 * @brief Reads a 32 bit chunck of UVP memory
 *
 * @param pageIndex   The number of the page that subsequent uvp read and writes will be referring to.
 * @param colAddr     The column address of to read the 32 bit chunck from
 * @param pData       Point to 32 bit memory to receive the chunk
 * @param ambNum      The number of the AMB's containing the UVP's to read from.
 * @param uvpNum      The number of the UVP to read the data from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
static cotaError_t uvpRegisterRead32(int pageIndex, uint16_t colAddr, uint32_t* pData, ambNum_t ambNum, uvpNum_t uvpNum)
{
    cotaError_t retVal = COTA_ERROR_NONE;
    uint8_t inBuff[UVP_32BIT_CHUNK_BUF_SIZE];
    uint8_t outBuff[UVP_32BIT_CHUNK_BUF_SIZE];
    uint32_t ambSelMask = NUM_TO_MASK(ambNum);
 
    retVal = AmbSelectUvp(uvpNum);
    
    retVal = (retVal == COTA_ERROR_NONE) ? uvpWritePage(pageIndex, ambNum) : retVal;

    if (retVal == COTA_ERROR_NONE)
    {
        memset(inBuff, 0, UVP_32BIT_CHUNK_BUF_SIZE);
        memset(outBuff, 0, UVP_32BIT_CHUNK_BUF_SIZE);
        outBuff[0] = (colAddr | UVP_READ_MASK);

        AmbSpiEnable(ambSelMask);

        retVal = SpiTransfer(AmbGetSpiHandle(ambNum), outBuff, inBuff, UVP_32BIT_CHUNK_BUF_SIZE);

        if (retVal == COTA_ERROR_NONE)
        {
            *pData = inBuff[1] | (inBuff[2] << 8) | (inBuff[3] << 16) | (inBuff[4] << 24);
        }
    }

    AmbSpiEnable(AMB_DISABLE_ALL);
    
    return retVal;
}

/**
 * @brief Reads a register from a UVP
 *
 * @param reg           A structure of type #uvp_reg_t containing the location of the register in UVP memory.
 * @param pData         Pointer to 32 bit memory to receive the register value
 * @param ambNum        The number of the AMB containing the UVP's to read from.
 * @param uvpNum        The number of the UVP to read the data from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t UvpRegisterRead(uvp_reg_t reg, uint32_t* pData, ambNum_t ambNum, uvpNum_t uvpNum)
{
    cotaError_t retVal = COTA_ERROR_NONE;
    uint32_t regMask = 0;
    
    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);

    regMask = GEN_SOLID_MASK(reg.size);
    
    retVal = uvpRegisterRead32(reg.pageIndex, reg.colAddr, pData, ambNum, uvpNum);
    
    *pData = (*pData >> reg.offset) & regMask;
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);    

    return POST_COTA_ERROR(retVal);
}

/**
 * @brief Reads a register from a UVP
 *
 * @param reg         A structure of type #uvp_reg_t containing the location of the register in UVP memory.
 * @param data        The value to write to the register
 * @param ambNum      The zero based number of the AMB containing the UVP's to read from.
 * @param uvpNum      The zero based number of the UVP to read the data from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t UvpRegisterWrite(uvp_reg_t reg, uint32_t data, ambNum_t ambNum, uvpNum_t uvpNum)
{
    cotaError_t retVal = COTA_ERROR_NONE;
    uint32_t readData = 0;
    uint32_t mask = 0;
    uint8_t inBuff[UVP_32BIT_CHUNK_BUF_SIZE];
    uint8_t outBuff[UVP_32BIT_CHUNK_BUF_SIZE];
    uint32_t ambSelMask = NUM_TO_MASK(ambNum);

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);

    mask = GEN_SOLID_MASK(reg.size);
    
    if (IS_NUMBER_IN_MASK(data, mask))
    {
        data = data << reg.offset;
        mask = mask << reg.offset;

        retVal = uvpRegisterRead32(reg.pageIndex, reg.colAddr, &readData, ambNum, uvpNum);

        if (retVal == COTA_ERROR_NONE)
        {
            readData = readData & (~mask);
            data = (data & mask) | readData;

            retVal = uvpWritePage(reg.pageIndex, ambNum);  

            if (retVal == COTA_ERROR_NONE)
            {
                memset(outBuff, 0, UVP_32BIT_CHUNK_BUF_SIZE);
                outBuff[0] = reg.colAddr;
                outBuff[1] = (data & 0xff);
                outBuff[2] = ((data >> 8) & 0xff);
                outBuff[3] = ((data >> 16) & 0xff);
                outBuff[4] = ((data >> 24) & 0xff);      

                AmbSpiEnable(ambSelMask);

                retVal = SpiTransfer(AmbGetSpiHandle(ambNum), outBuff, inBuff, UVP_32BIT_CHUNK_BUF_SIZE);
            }
        }
        AmbSpiEnable(AMB_DISABLE_ALL);
    }
    else
    {
        retVal = COTA_ERROR_UVP_REG_VALUE_NOT_IN_MASK;
    }
    
    AmbSpiEnable(AMB_DISABLE_ALL);
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);   
    
    return POST_COTA_ERROR(retVal);
}

/**
 * @brief Reads a register from many UVP's and puts them in an array.
 *
 * @param reg         A structure of type #uvp_reg_t containing the location of the register in UVP memory (Pass a define from uvpDriver.h)
 * @param pData       An array of uint32_t to receive the values. The location of values in the array are determined by the #UVP_INDEX macro.
 * @param numValues   The number of uint32_t's in the array.
 * @param ambMask     A mask indicating the AMB's where the targer UVP's reside
 * @param uvpMask     A mask indicating which UVP's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t ReadArrayUvps(uvp_reg_t pReg, uint32_t* pData, uint16_t numValues, ambMask_t ambMask, uvpMask_t uvpMask)
{
    cotaError_t retVal = COTA_ERROR_NONE;
    uint16_t ambNum;
    uint16_t uvpNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
    {    
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
        {    
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            if (UVP_INDEX(ambNum, uvpNum) > numValues)
            {
                continue;
            }

            retVal = UvpRegisterRead(pReg, &pData[UVP_INDEX(ambNum, uvpNum)], ambNum, uvpNum);
        }
    }
  
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return POST_COTA_ERROR(retVal);
}

/**
 * @brief This function will retrieve chunks of UVP data from one UVP on multiple AMB's.  
 *        The routine has been optimized to read UVP data four at time, one from each AMB
 * 
 * @param pData     A byte array to receive the memory from the UVP. It's size should be MAX_AMB_NUM times the size. Data from each amb is stored as follows:
 AMB0 starts at pData[0],  AMB1 start at pData[size], AMB2 start at pData[2*size], etc.
 * @param size      The size of the data to receive from each UVP. Maximum 8.
 * @param colAddr   The column address where memory to read is located in the UVP
 * @param ambMask   A mask indicating which AMB's to read from
 * @param uvpNum    The number of the UVP to read from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
static cotaError_t readSimulUvps(uint8_t* pData, uint8_t size, uint16_t colAddr, ambMask_t ambMask, uvpNum_t uvpNum)
{
    cotaError_t retVal = COTA_ERROR_UVP_SIMUL_READ_TO_LARGE;
    uint8_t inBuff[UVP_64BIT_CHUNK_BUF_SIZE*MAX_NUM_AMB];
    uint8_t outBuff[UVP_64BIT_CHUNK_BUF_SIZE];
    uint8_t ii;
 
    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    if (size < UVP_64BIT_CHUNK_BUF_SIZE)
    {       
        //Set the address pins so we read from the correct IVP
        AmbSelectUvp(uvpNum);

        //Clear local buffers
        memset(inBuff, 0, UVP_64BIT_CHUNK_BUF_SIZE*MAX_NUM_AMB);
        memset(outBuff, 0, UVP_64BIT_CHUNK_BUF_SIZE);
        outBuff[0] = (colAddr | UVP_READ_MASK);

        //Select the SPI port chip we want to send to.
        AmbSpiEnable(ambMask);
          
        //Initiate a simultaneous tranfer over all buses selected in ambMask, we must use size + 1 to make room for the address byte.
        retVal = SpiSimulTransfer(outBuff, inBuff, size + 1, ambMask);

        if (retVal == COTA_ERROR_NONE)
        {
            //Since the results are spaced out in the outBuff at size +1 length,
            //we need to copy them individually into pData which is space at size length.
            for (ii = 0; ii < MAX_NUM_AMB; ii++)
            {
                memcpy(&pData[ii*size], &outBuff[ii*(size+1)+1], size);
            }
        }

        //Make sure the SPI port are no longer selected.
        AmbSpiEnable(AMB_DISABLE_ALL);
    }
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return retVal;
}

/**
 * @brief This function will broadcast a page index to all UVP's on select AMB's
 * 
 * @param pageIndex The page index where the memory to read is located in the UVP
 * @param ambMask   A mask indicating which AMB's to broadcast to
 * @param uvpMask   A mask indicating which UVP's to broadcast to
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
static cotaError_t broadcastPageIndex(uint8_t pageIndex, ambMask_t ambMask)
{
    uint8_t outBuff[UVP_PAGE_BUF_SIZE];
    cotaError_t retVal = COTA_ERROR_NONE;  

    AmbSelectUvp(ALL_UVP_NUM);
    AmbSpiEnable(ambMask);

    outBuff[0] = UVP_WRITE_PAGE_INDEX;
    outBuff[1] = pageIndex;  

    retVal = SpiSimulTransfer(outBuff, NULL, UVP_PAGE_BUF_SIZE, ambMask);

    AmbSpiEnable(AMB_DISABLE_ALL);

    return retVal;
}

/**
 * @brief This function will retrieve 32 bit chunks of UVP data from multiple UVP's on multiple AMB's.  
 *        The routine has been optimized to read UVP data four at time, one for each AMB
 * 
 * @param pData     An array of 32 bit unsigned integers to receive the data
 * @param numValues The number of values in the array.  Recommend MAX_NUM_AMB*UVP_PER_AMB
 * @param pageIndex The page index where the memory to read is located in the UVP
 * @param colAddr   The column address where memory to read is located in the UVP
 * @param ambMask   A mask indicating which AMB's to read from
 * @param uvpMask   A mask indicating which UVP's to read from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t ReadArrayUvps32(uint32_t* pData, uint16_t numValues, uint8_t pageIndex, uint16_t colAddr, ambMask_t ambMask, uvpMask_t uvpMask) 
{
    cotaError_t retVal = COTA_ERROR_NONE;  
    uint8_t readBuff[sizeof(uint32_t)*MAX_NUM_AMB];
    uvpNum_t uvpNum;
    ambNum_t ambNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    // Broadcast page index to all UVP's
    retVal = broadcastPageIndex(pageIndex, ambMask);

    // Loop through every UVP
    for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
    {      
        //Ignore UVP's not selected.
        if (!BIT_IN_MASK(uvpNum, uvpMask))
        {
            continue;
        }

        //Read the selected UVP on the selected AMB's
        retVal = readSimulUvps(readBuff, sizeof(uint32_t), colAddr, ambMask, uvpNum);

        //We need to grab the data from each AMB and put it in the appropriate locaation.
        for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
        {
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }
            if (UVP_INDEX(ambNum, uvpNum) > numValues)
            {
                continue;
            }
            pData[UVP_INDEX(ambNum, uvpNum)] = *(uint32_t*)&readBuff[ambNum*sizeof(uint32_t)];
        }
    }

    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return POST_COTA_ERROR(retVal);  
}

/**
 * @brief This function will retrieve 64 bit chunks of UVP data from multiple UVP's on multiple AMB's.  
 *        The routine has been optimized to read UVP data four at time, one for each AMB
 * 
 * @param pData     An array of 64 bit unsigned integers to receive the data
 * @param numValues The number of values in the array.  Recommend MAX_NUM_AMB*MAX_NUM_UVP
 * @param pageIndex The page index where the memory to read is located in the UVP
 * @param colAddr   The column address where memory to read is located in the UVP
 * @param ambMask   A mask indicating which AMB's to read from
 * @param uvpMask   A mask indicating which UVP's to read from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t ReadArrayUvps64(uint64_t* pData, uint16_t numValues, uint8_t pageIndex, uint16_t colAddr, ambMask_t ambMask, uvpMask_t uvpMask) 
{
    cotaError_t retVal =COTA_ERROR_NONE;  
    uint8_t readBuff[sizeof(uint64_t)*MAX_NUM_AMB];
    uvpNum_t uvpNum;
    ambNum_t ambNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    // Broadcast page index to all UVP's
    retVal = broadcastPageIndex(pageIndex, ambMask);

    // Loop through every UVP  
    for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
    {     
        //Ignore UVP's not selected.
        if (!BIT_IN_MASK(uvpNum, uvpMask))
        {
            continue;
        }

        //Read the selected UVP on the selected AMB's    
        retVal = readSimulUvps(readBuff, sizeof(uint64_t), colAddr, ambMask, uvpNum);

        //We need to grab the data from each AMB and put it in the appropiate locatation.    
        for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
        {
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }
            if (UVP_INDEX(ambNum, uvpNum) > numValues)
            {
                continue;
            }
            pData[UVP_INDEX(ambNum, uvpNum)] = *(uint64_t*)&readBuff[ambNum*sizeof(uint64_t)];
        }
    }
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return POST_COTA_ERROR(retVal);  
}

/**
 * @brief Write a register to all UVP's
 *       
 * @note This is not a broadcast operation, it writes to each UVP one at a time.
 *
 * @param reg         A structure of type #uvp_reg_t containing the location of the register in UVP memory (Pass a define from uvpDriver.h)
 * @param data        The value to write to the register
 * @param ambMask     A mask indicating the AMB's where the target UVP's reside
 * @param uvpMask     A mask indicating which UVP's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t WriteManyUvps(uvp_reg_t pReg, uint32_t data, ambMask_t ambMask, uvpMask_t uvpMask)
{
    cotaError_t retVal = COTA_ERROR_NONE;
    ambNum_t ambNum;
    uvpNum_t uvpNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
    {    
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
        {      
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            retVal = UvpRegisterWrite(pReg, data, ambNum, uvpNum);
        }
    }
  
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);  
    
    return POST_COTA_ERROR(retVal);
}

/**
 * @brief Write an array of register values to many UVP's.
 *
 * @param reg         A structure of type #uvp_reg_t containing the location of the register in UVP memory (Pass a define from uvpDriver.h)
 * @param pData       An array of uint32_t of register values. The location of values in the array are determined by the #UVP_INDEX macro.
 * @param numValues   The number of uint32_t's in the array.
 * @param ambMask     A mask indicating the AMB's where the target UVP's reside
 * @param uvpMask     A mask indicating which UVP's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t WriteArrayUvps(uvp_reg_t pReg, uint32_t* pData, uint32_t numValues, ambMask_t ambMask, uvpMask_t uvpMask)
{
    cotaError_t retVal = COTA_ERROR_NONE;
    uint16_t ambNum;
    uint16_t uvpNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (retVal == COTA_ERROR_NONE); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (retVal == COTA_ERROR_NONE); uvpNum++)
        {      
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            if (UVP_INDEX(ambNum, uvpNum) > numValues)
            {
                continue;
            }
            retVal = UvpRegisterWrite(pReg, pData[UVP_INDEX(ambNum, uvpNum)], ambNum, uvpNum);
        }
    }
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);    
  
    return POST_COTA_ERROR(retVal);
}

/**
 * @brief Enable or disable AMUs based on their mask.
 *
 * @param ambNum  Number indicating which AMB the UVP  is on.
 * @param uvpNum  Number of UVP to enable/disable AMU's on.
 * @param amuMask Indicates which AMUs should be enabled and which should be disabled.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t UvpAmuEnable(ambNum_t ambNum, uvpNum_t uvpNum, amuMask_t amuMask)
{
    int amuNum;
    cotaError_t ret = COTA_ERROR_NONE;
    uvp_reg_t enAmu[MAX_NUM_AMU] = {EN_AMU1, EN_AMU2, EN_AMU3, EN_AMU4};

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (amuNum = 0; (amuNum < MAX_NUM_AMU) && (ret == COTA_ERROR_NONE); amuNum++)
    {
        if (BIT_IN_MASK(amuNum, amuMask))
        {
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(enAmu[amuNum], AMU_ENABLED, ambNum, uvpNum) : ret;
        }
        else
        {
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(enAmu[amuNum], AMU_DISABLED, ambNum, uvpNum) : ret;
        }
    }
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);   
    
    return ret;
}

/**
 * @brief Enable or disable AMUs based on their mask for many UVPS'
 *
 * @param ambMask A mask indicating which AMB's to enable AMU's on
 * @param uvpMask A mask indicating which UVP's to enable AMU's on
 * @param amuMask Indicates which AMUs should be enabled and which should be disabled.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t UvpManyAmuEnable(ambMask_t ambMask, uvpMask_t uvpMask, amuMask_t amuMask)
{
    cotaError_t ret = COTA_ERROR_NONE;
    ambNum_t ambNum; 
    uvpNum_t uvpNum;

    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {      
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            ret = UvpAmuEnable(ambNum, uvpNum, amuMask);
        }
    }

    xSemaphoreGiveRecursive(gUvpRecursiveMutex);   
    
    return ret;
}

/**
 * @brief Reads the UVP temperature in units of centiCelsius into an array of int16_t
 * 
 * @param pTemp     The int16_t temperature array that receives the values
 * @param numValues Number of floating point values in the array
 * @param ambMask   A mask indicating which AMB's to read the UVP temperatures from
 * @param uvpMask   A mask indicating which UVP's to read temperatures from
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t  
 */
cotaError_t UvpGetTemperatureArray(int16_t* pTemp, uint16_t numValues, ambMask_t ambMask, uvpMask_t uvpMask)
{
    cotaError_t ret = COTA_ERROR_NONE;
    uint32_t digTemps[TOTAL_UVPS];
    ambNum_t ambNum;
    uvpNum_t uvpNum;
    int16_t digTemp;
    uint32_t temp;
    uint32_t mask = GEN_SOLID_MASK(TEMPADC_OUT.size);
    
    //Let's read all the 32 bit chunks containing the temperature into an array
    ret = ReadArrayUvps32(digTemps, TOTAL_UVPS, TEMPADC_OUT.pageIndex, TEMPADC_OUT.colAddr, ambMask, uvpMask); 

    if (ret != COTA_ERROR_NONE)
    {
        //Now we loop through the array and convert the 32 bit chunks into actual temperatures in degrees
        for (ambNum = 0; ambNum < MAX_NUM_AMB; ambNum++)
        {
            if (!BIT_IN_MASK(ambNum, ambMask))
            {
                continue;
            }
            for (uvpNum = 0; uvpNum < UVPS_PER_AMB; uvpNum++)
            {
                if (!BIT_IN_MASK(uvpNum, uvpMask))
                {
                    continue;
                }
              
                if (UVP_INDEX(ambNum, uvpNum) < numValues)
                {
                    temp =    ((digTemps[UVP_INDEX(ambNum, uvpNum)] >> TEMPADC_OUT.offset) && mask);      //Extract the temperature bits from the 32 bit chunk
                    digTemp = (int16_t)((temp & UVP_TEMP_SIGN_BIT) ? (temp | UVP_TEMP_SIGN_EXT) : temp);  //Convert to a signed 16 bit value
                    pTemp[UVP_INDEX(ambNum, uvpNum)] = (int16_t)(((UVP_TEMP_SLOPE_NUM * ((int32_t)digTemp))/UVP_TEMP_SLOPE_DEN) + UVP_TEMP_OFFSET);        //Convert to a value in centidegrees C
                }
            }      
        }
    }
    
    return ret;
}

/**
 *  @brief This will cause all the UVP temperatures to read and cached in memory.
 *         It will also calculate that maximum delta since the last cache
 *         if an argument is specified
 *
 * @param  maxDelta  A pointer to a int16_t to receive the maximum absolute temperature change since the last cache
 *                   If NULL, this calculation is not done.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t  
 */
cotaError_t UpdateUvpTemperatureCache(int16_t* maxDelta)
{
    cotaError_t ret = COTA_ERROR_NONE;
    int16_t uvpTemps[TOTAL_UVPS];
    ambNum_t ambNum;
    uvpNum_t uvpNum;
    int16_t del;
  
    ret = UvpGetTemperatureArray(uvpTemps, TOTAL_UVPS, VALID_AMBS, ALL_UVP_MASK);
    
    if (ret == COTA_ERROR_NONE)
    {
        if (maxDelta)
        {
            *maxDelta = 0;
            //Let's determine that maximum absolute change since the last reading
            for (ambNum = 0; ambNum < MAX_NUM_AMB; ambNum++)
            {
                if (!BIT_IN_MASK(ambNum, VALID_AMBS))
                {
                    continue;
                }

                for (uvpNum = 0; uvpNum < UVPS_PER_AMB; uvpNum++)
                {
                    del = abs(uvpTemps[UVP_INDEX(ambNum, uvpNum)] - uvpTempCache[UVP_INDEX(ambNum, uvpNum)]);
                    if (del > *maxDelta) 
                    {
                        *maxDelta = del;
                    }
                }              
            }
        }
      
        //Let's update the cache
        memcpy(uvpTempCache, uvpTemps, sizeof(uvpTempCache));      
    }
    
    return ret;
}


/**
 * @brief Gets the average UVP temperature on each AMB using the last read uvp temperature cache
 * 
 * @param uvpAvgTemps A int16_t point array that receives the average temperatures in centidegree C
 * @param numAvgTemps The number of floating point values in the array.
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t 
 */
cotaError_t GetAverageUvpTempsFromCache(int16_t* uvpAvgTemps, uint8_t numAvgTemps)
{
    cotaError_t ret = COTA_ERROR_NONE;
    int32_t tempAcc;
    ambNum_t ambNum;
    uvpNum_t uvpNum;
    
    if (ret == COTA_ERROR_NONE)
    {
        for (ambNum = 0; ambNum < MAX_NUM_AMB; ambNum++)
        {
            if (!BIT_IN_MASK(ambNum, VALID_AMBS))
            {
                continue;
            }
            
            tempAcc = 0;
            //Average UVP temps for this AMB
            for (uvpNum = 0; uvpNum < UVPS_PER_AMB; uvpNum++)
            {
                tempAcc += uvpTempCache[UVP_INDEX(ambNum, uvpNum)];
            }
            uvpAvgTemps[ambNum] = (int16_t)(tempAcc/UVPS_PER_AMB);
        }
    }
    return ret;
}

/**
 * @brief Scans the value  voltage control oscilator capcitor frequency selection 
 bits (VCOCFS) of the UVP until locking of the clock is attained.
 * @param ambNum Number indicating which AMB the UVP  is on.
 * @param uvpNum Number of UVP to enable/disable AMU's on.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
static cotaError_t lockDetect(ambNum_t ambNum, uvpNum_t uvpNum)
{
    cotaError_t ret = COTA_ERROR_NONE;
    uint32_t i = 0;
    uint32_t data = 0;
    bool lockDetected = false;

    ret = (ret == COTA_ERROR_NONE) ?  UvpRegisterWrite(PLLLFMODE,     0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ?  UvpRegisterWrite(PLLDACEN,      1, ambNum, uvpNum) : ret;

    //Try scanning VCOCFS using a narrow voltage window.
    ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(PLLLCKRNGCTRL, 1, ambNum, uvpNum) : ret;   
    
    for (i = VCOCFS_VALUE_START; (i <= VCOCFS_VALUE_END) && (ret == COTA_ERROR_NONE) && (!lockDetected); i++)
    {
        ret = UvpRegisterWrite(VCOCFS, i, ambNum, uvpNum);
        vTaskDelay( pdMS_TO_TICKS(1));
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(LF_LOCKDET, &data, ambNum, uvpNum) : ret;  

        if ((data == 1) && (ret == COTA_ERROR_NONE))  //locked
        {
            lockDetected = true;
        }
    }
    
    //Try scanning VCOCFS using with large voltage window. 
    ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(PLLLCKRNGCTRL, 0, ambNum, uvpNum) : ret;
    
    for (i = VCOCFS_VALUE_START; (i <= VCOCFS_VALUE_END) && (ret == COTA_ERROR_NONE) && (!lockDetected); i++)
    {
        ret = UvpRegisterWrite(VCOCFS, i, ambNum, uvpNum);
        vTaskDelay( pdMS_TO_TICKS(1));
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(LF_LOCKDET, &data, ambNum, uvpNum) : ret;

        if ((data == 1) && (ret == COTA_ERROR_NONE))  //locked 
        {
            lockDetected = true;
        }
    }

    return lockDetected ? COTA_ERROR_NONE : COTA_ERROR_UVP_LOCK_FAILED;
}

/**
 * @brief Confirm lock detect on many UVP's
 *
 * @param ambMask A mask indicating which AMB's to attain clock lock for.
 * @param uvpMask A mask indicating which UVP's to attain clock lock for.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
static cotaError_t uvpConfirmLocks(ambMask_t ambMask, uvpMask_t uvpMask)
{
    cotaError_t ret = COTA_ERROR_NONE;
    ambNum_t ambNum; 
    uvpNum_t uvpNum;

    for (ambNum = 0; (ambNum < MAX_NUM_AMB) && (ret == COTA_ERROR_NONE); ambNum++)
    {
        if (!BIT_IN_MASK(ambNum, ambMask))
        {
            continue;
        }

        for (uvpNum = 0; (uvpNum < UVPS_PER_AMB) && (ret == COTA_ERROR_NONE); uvpNum++)
        {      
            if (!BIT_IN_MASK(uvpNum, uvpMask))
            {
                continue;
            }

            ret = lockDetect(ambNum, uvpNum);
        }
    }
  
    return ret;
}

/**
 * @brief Initialize the specified UVP's on the specified AMB's.
 *        This is designed for SWS1411 UVPv2 Variant 1
 *
 * @param ambMask A mask indicating which AMB's to initialize UVP's for
 * @param uvpMask A mask indicating which UVP's to initialize on the AMB's
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t UvpInit(ambMask_t ambMask, uvpMask_t uvpMask)
{
    cotaError_t ret = COTA_ERROR_NONE;

    /*
      This register-value list was pulled directly from the Venus code base and is
      not well-documented since migrating to this iteration of the Orion code base.
      We will need to revisit this initialization sequence if for no other reason
      than properly documenting this sequence.

      TODO Determine this initialization sequence is correct and comment the major steps
      properly.
     */
    xSemaphoreTakeRecursive(gUvpRecursiveMutex, portMAX_DELAY);
    
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LFR3CTRL,    1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LFR1CTRL,    1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LFC1CTRL,    4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LFC2CTRL,    1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(CPLKEN,      1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(CPIVAL,      4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(NDIVINT, 8, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(EN_AMU1,       1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(EN_AMU2,       1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(EN_AMU3,       1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(EN_AMU4,       1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(REFOSCAMPMODE, 0, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123PDRBST,  3, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PDRBST,    3, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123PDIBST,  0, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PDIBST,    0, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123AAFGAIN, 2, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4AAFGAIN,   2, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCPSEN,1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCPSEN,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCCOMPSELEN, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCCOMPSELEN,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCCOMPENCNT, 4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCCOMPENCNT,   4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCEN_TEST,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCEN_TEST,     1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU123SADCCMBUFFEN_TEST, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4SADCCMBUFFEN_TEST,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LOWIF_I_SYMBLI,   49, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LOWIF_I_SYMBLQ,   76, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LOWIF_Q_SYMBLI,  196, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LOWIF_Q_SYMBLQ,   49, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_I_GAIN_AMU1,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_I_GAIN_AMU2,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_I_GAIN_AMU3,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_I_GAIN_AMU4,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_Q_GAIN_AMU1,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_Q_GAIN_AMU2,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_Q_GAIN_AMU3,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ADCINTF_Q_GAIN_AMU4,  32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU1PAHIGHPOWER,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU2PAHIGHPOWER,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU3PAHIGHPOWER,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU4PAHIGHPOWER,  1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ENCODE_SIZE,      32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(RSSI_GAIN_SCALE,  1024, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(RSSI_LOWTHRESH,   0,    ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PA_COUNTER_VALID,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PA_POWERON_DEL,     32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PA_SEQUENCE_DEL,    4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PA_OFFSET_DEL,      2, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(PHDETECT_PIN_SEL,   1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(ARB_PHASE_SEL,      0, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(SEL_PHASE_CONJUGATE, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(AMU_PHASE_CALIBRATE,15, ambMask, uvpMask) : ret;
    //The following registers are used to enable the thermometers on the uvp's
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LDOADC1EN_IDLE, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TEMPADC_GAIN, 32, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TMPSNSCMBUFFEN, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TMPSNSCOMPENCNT, 4, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TMPSNSCOMPSELEN, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TMPSNSEN, 1 , ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(TEMPADCEN, 1, ambMask, uvpMask) : ret;
    ret = (ret == COTA_ERROR_NONE) ? WriteManyUvps(LDOADC1EN_SENDPWR, 1, ambMask, uvpMask) : ret;

    ret = (ret == COTA_ERROR_NONE) ? uvpConfirmLocks(ambMask, uvpMask) : ret;
    
    xSemaphoreGiveRecursive(gUvpRecursiveMutex);   
    
    return ret;
}

#ifdef UVP_TEST_ENABLED
volatile uint32_t someValue; ///< A value that gives the debugger something to show in testUvpWriteRead

/**
 * @brief A test that writes a value to the RSSI_GAIN_SCALE register and then reads it back.
 *
 * @param wdata  The value to write to the register
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
static cotaError_t testUvpWriteRead(uint32_t wdata)
{
    cotaError_t ret = COTA_ERROR_NONE;
    uint32_t data = 0;

    ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(RSSI_GAIN_SCALE, wdata, 1, 1) : ret;
    ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(RSSI_GAIN_SCALE, &data, 1, 1) : ret;

    if ((ret == COTA_ERROR_NONE) && (data == wdata))
    {
        someValue = data;
    }
    else
    {
        ret = COTA_ERROR_HAL;
        someValue = 0xffffffff;
    } 
    return ret;
}

volatile uint32_t raw; ///< Volatile used to help debug things.
/*
 * @brief Reads a register to see if it is the correct value
 * 
 * @param reg      A structure of type #uvp_reg_t containing the location of the register in UVP memory (Pass a define from uvpDriver.h)
 * @param pData    The value to test the value read from the register against.
 * @param ambNum   Number indicating which AMB the UVP  is on.
 * @param uvpNum   Number of UVP to enable/disable AMU's on.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
static cotaError_t uvpTestReg(uvp_reg_t reg, uint32_t data, ambNum_t ambNum, uvpNum_t uvpNum)
{
    cotaError_t ret = COTA_ERROR_NONE;
    uint32_t dataRead = 0;
    uint32_t* pData = &dataRead;    
    uint32_t regMask = 0;
        
    regMask = GEN_SOLID_MASK(reg.size);
    
    ret = uvpRegisterRead32(reg.pageIndex, reg.colAddr, pData, ambNum, uvpNum);
    raw = *pData;
    *pData = (*pData >> reg.offset) & regMask;
    
    
    if ((ret != COTA_ERROR_NONE) || (data != dataRead))
    {
        ret = COTA_ERROR_NONE;
    }
    
    return ret;
}

/**
 * @brief A simple test of the UVP that writes a values to a register and then reads them back.
 *
 * @param ambNum Number indicating which AMB the UVP  is on.
 * @param uvpNum Number of UVP to enable/disable AMU's on.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
static cotaError_t uvpInitReadBackTest(ambNum_t ambNum, uvpNum_t uvpNum)
{
    cotaError_t ret = COTA_ERROR_NONE;

    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LFR3CTRL,    1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LFR1CTRL,    1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LFC1CTRL,    4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LFC2CTRL,    1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(CPLKEN,      1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(CPIVAL,      4, ambNum, uvpNum) : ret;  
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(NDIVINT,     8, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(EN_AMU1,       1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(EN_AMU2,       1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(EN_AMU3,       1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(EN_AMU4,       1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(REFOSCAMPMODE, 0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123PDRBST,  3, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4PDRBST,    3, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123PDIBST,  0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4PDIBST,    0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123AAFGAIN, 2, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4AAFGAIN,   2, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCPSEN,1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCPSEN,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCCOMPSELEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCCOMPSELEN,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCCOMPENCNT, 4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCCOMPENCNT,   4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCEN_TEST,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCEN_TEST,     1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU123SADCCMBUFFEN_TEST, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4SADCCMBUFFEN_TEST,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LOWIF_I_SYMBLI,   49, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LOWIF_I_SYMBLQ,   76, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LOWIF_Q_SYMBLI,  196, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LOWIF_Q_SYMBLQ,   49, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_I_GAIN_AMU1,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_I_GAIN_AMU2,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_I_GAIN_AMU3,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_I_GAIN_AMU4,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_Q_GAIN_AMU1,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_Q_GAIN_AMU2,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_Q_GAIN_AMU3,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ADCINTF_Q_GAIN_AMU4,  32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU1PAHIGHPOWER,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU2PAHIGHPOWER,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU3PAHIGHPOWER,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU4PAHIGHPOWER,  1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ENCODE_SIZE,      32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(RSSI_GAIN_SCALE,  1024, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(RSSI_LOWTHRESH,   0,    ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PA_COUNTER_VALID,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PA_POWERON_DEL,     32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PA_SEQUENCE_DEL,    4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PA_OFFSET_DEL,      2, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(PHDETECT_PIN_SEL,   1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(ARB_PHASE_SEL,      0, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(SEL_PHASE_CONJUGATE, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(AMU_PHASE_CALIBRATE,15, ambNum, uvpNum) : ret;
  
    //The following registers are used to enable the thermometers on the uvp's
  
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LDOADC1EN_IDLE, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TEMPADC_GAIN, 32, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TMPSNSCMBUFFEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TMPSNSCOMPENCNT, 4, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TMPSNSCOMPSELEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TMPSNSEN, 1 , ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(TEMPADCEN, 1, ambNum, uvpNum) : ret;
    ret = (ret == COTA_ERROR_NONE) ? uvpTestReg(LDOADC1EN_SENDPWR, 1, ambNum, uvpNum) : ret;

    return ret;
}

#define TEST_PASSED "UvpTestsPassed!!\r\n"
/**
 * @brief A simple test of the UVP that writes a values to a register and then reads them back.
 */
void TestUvp(void)
{
    cotaError_t ret = COTA_ERROR_NONE;

    uint32_t tempData;
    int16_t  temp;
    uint8_t count = 0;
    uint32_t ready;
    uint32_t vcoVal = 0;
    char out[64];

    //Enable the uvp, using AMB 0
    AmbEnable(AMB0_MASK);
    AmbUvpEnable(AMB_DISABLE_ALL);  
    vTaskDelay( pdMS_TO_TICKS(1000));
    AmbUvpEnable(AMB0_MASK);  
    vTaskDelay( pdMS_TO_TICKS(1000));

    //Let's do some simple readback testing of a UVP register
    //The values passed to testUvpWriteRead are picked at random for the sake of testing.
    ret = testUvpWriteRead(0xCD);

    ret = (ret == COTA_ERROR_NONE) ? testUvpWriteRead(0x12) : ret;
    ret = (ret == COTA_ERROR_NONE) ? testUvpWriteRead(0x04) : ret;
    ret = (ret == COTA_ERROR_NONE) ? testUvpWriteRead(0x106) : ret;
  
  
    if (ret == COTA_ERROR_NONE)
    {
        //Let's now test to see if we can initialize all the necessary UVP registers, and read them back, then attain lockDetect
        ret = (ret == COTA_ERROR_NONE) ? UvpInit(AMB0_MASK, 0x1) : ret;
        ret = (ret == COTA_ERROR_NONE) ? uvpInitReadBackTest(1, 1) : ret;
        ret = (ret == COTA_ERROR_NONE) ? lockDetect(1, 1) : ret;
    
        ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(AMB0_MASK, 0x1, ALL_AMU_MASK) : ret;   
    
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, 1 , AMB0_MASK, 0x1) : ret;
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(POWER_ON_PA, 1, AMB0_MASK, 0x1) : ret;

        vTaskDelay( pdMS_TO_TICKS(100));  
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(SEND_POWER_RDY, &ready, AMB0_MASK, 0x1) : ret;
        if (ready != 1)
        {
            ret = COTA_ERROR_UVP_TEST_FAILED;
        }
        //Let's print out the value of VCOCFS to see if it is reasonable.
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(VCOCFS, &vcoVal, AMB0_MASK, 0x1) : ret;
        snprintf(out, 64, "VCOFS %d\n\r", vcoVal);
        CotaUartTransmit((uint8_t*)out, strlen(out),  pdMS_TO_TICKS(1000)); 
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(POWER_ON_PA, &vcoVal, AMB0_MASK, 0x1) : ret;
        snprintf(out, 64, "powerOnPA %d\n\r", vcoVal);
        CotaUartTransmit((uint8_t*)out, strlen(out),  pdMS_TO_TICKS(1000));   
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, 1 , AMB0_MASK, 0x1) : ret; 
        ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_PUCTRL, 0 , AMB0_MASK, 0x1) : ret; 
    
        //Let's rotate TX across the AMU's in the UVP.
        while (ret == COTA_ERROR_NONE)
        {
            vTaskDelay( pdMS_TO_TICKS(5000));
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, 0 , AMB0_MASK, 0x1) : ret; 
            CotaUartTransmit((uint8_t*)"T", 1,  pdMS_TO_TICKS(1000));
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterRead(TEMPADC_OUT, &tempData, AMB0_MASK, 0x1) : ret;
            temp = (int16_t)((tempData & 0x1000) ? (tempData | 0xE000) : tempData);
            snprintf(out, 64, " %f C AMU %d\n\r",(0.0155*temp + 43.55), count);
            CotaUartTransmit((uint8_t*)out, strlen(out),  pdMS_TO_TICKS(1000));
            ret = (ret == COTA_ERROR_NONE) ? UvpAmuEnable(AMB0_MASK, 0x1, NUM_TO_MASK(count)) : ret;  
            ret = (ret == COTA_ERROR_NONE) ? UvpRegisterWrite(UVP_TXCTRL, 1 , AMB0_MASK, 0x1) : ret;
            count++;
            count = count % 4;    
        }
    
    }
  
    if (ret != COTA_ERROR_NONE)
    {
        POST_COTA_ERROR(COTA_ERROR_UVP_TEST_FAILED);
    }
    else
    {
        POST_COTA_ERROR(COTA_ERROR_NONE);
        CotaUartTransmit(TEST_PASSED, strlen(TEST_PASSED), portMAX_DELAY);
    }

}

#endif
