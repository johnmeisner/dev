/****************************************************************************//**
* @file      i2c_multiplex.h
*
* @brief     Header file to control i2c communications through the multiplexer
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _I2C_MULTIPLEX_DRIVER_H_
#define _I2C_MULTIPLEX_DRIVER_H_
#include "error.h"

/**
 * @brief  A structure containing the information the i2c multiplexer
 *         driver needs to send or receive a message from an i2c device.
 *         It is initialized by the InitI2CMultiplexer function, and is
 *         passed like a handle to the other multiplexer API.
 */
typedef struct _I2cMultiplexerInfo_t
{
    uint8_t multiNum;        ///< The number of the multiplexer the device is attached to.
    uint8_t chan;            ///< The channel the device is attached to on the multiplexer
    uint8_t devAddr;         ///< The 7 bit address of the i2c device
} I2CMultiplexerInfo_t;


void InitI2CMultipler(void);
cotaError_t InitI2CMultiplexer(I2CMultiplexerInfo_t* multiInfo, uint8_t multiNum, uint8_t multiChan, uint8_t devAddr);
cotaError_t I2CMultiplexWrite(I2CMultiplexerInfo_t* multiInfo, uint8_t* pData, uint32_t size, uint32_t timeout);
cotaError_t I2CMultiplexRead(I2CMultiplexerInfo_t* multiInfo, uint8_t* pData, uint32_t size, uint32_t timeout);
cotaError_t RegisterI2CMultiplexerSemaphore(I2CMultiplexerInfo_t* multiInfo, SemaphoreHandle_t* semaPhore);

#endif // #ifndef _I2C_MULTIPLEX_DRIVER_H_
