/****************************************************************************//**
* @file      spi_driver.c
*
* @brief     Driver to read and write data over SPI
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "FreeRTOS.h"
#include "string.h"
#include "main.h"
#include "amb_control.h"
#include "uvp_driver.h"
#include "spi_driver.h"
#include "semphr.h"
#include "task.h"
#include "debug_off.h"

#if defined( __ICCARM__ )
  #define DMA_BUFFER \
      _Pragma("location=\".dma_buffer\"")
#else
  #define DMA_BUFFER
#endif

/**
 * @note  We need to explicity disable pages in the D-cache while using DMA so that will load from memory on the next read.
 *        Since D-cache pages can only be disabled on a 32 byte basis, we need to make sure the globals below are aligned to 
 *        32 bytes and consume entire pages.
 */
#define SPI_SIMUL_READ_SIZE 9                                   ///< If reading from multiple AMB's simulataneously, this is the maximum bytes that be read from any single UVP
#define SPI_DMA_BUF_SIZE    (((SPI_MAX_NUM_BYTES-1)/32+1)*32)   ///< This is designed to make sure arrays lengths are aligned to 32 bytes.  For example, a size of 31 bytes evaluates to 32 in this expression.  A size of 1 also evaluates to 32.
#define SPI_WAIT_TIMEOUT    1                                   ///< Number of millseconds to wait for SPI transaction before timing out.      
/**
 * Globals used for DMA are best alligned to 32 bytes and kept a length of 32 bytes.
 *  The reason is that we must clear the cache for writes and invalidate pages in the D-cache for reads.
 *  Pages in the D-cache are 32 bytes. Problems can develop if the other variables share the same page with
 *  these globals.  A read of another variable for example could validate a page that needs to state
 *  invalid for example.
 */
#pragma data_alignment=32 
DMA_BUFFER static uint8_t gMasterRxBuf[SPI_DMA_BUF_SIZE];   ///< A DMA enabled buffer receiving data over the master SPI port.

#pragma data_alignment=32
DMA_BUFFER static uint8_t gMasterTxBuf[SPI_DMA_BUF_SIZE];   ///< A DMA enabled buffer containing data to send over the master SPI port.

#pragma data_alignment=32
DMA_BUFFER static uint8_t gSlaveRxBuf[SPI_DMA_BUF_SIZE];    ///< A DMA enabled buffer receiving data over one of the slave SPI ports

#pragma data_alignment=32
DMA_BUFFER static uint8_t gSlaveTxBuf[SPI_DMA_BUF_SIZE];    ///< A DMA enabled buffer containing data to send over one of the slave SPI ports.

static SemaphoreHandle_t  gSpiSemphoreHandle;               ///< Counting semaphore used to signal complete spi transactions
static StaticSemaphore_t  gSpiSemphoreBuffer;               ///< Buffer containing information about gSpiSemphoreHandle

/**
 * @brief A callback indicating a SPI transaction has completed. It gives the SPI sempahore, incrementing the count by one.
 * 
 * @param hpsi Handle to the SPI port
 */
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    DBG1_RESET();
    xSemaphoreGiveFromISR(gSpiSemphoreHandle, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @brief Initializes the SPI driver
 */
void InitSpiDriver(void)
{
    gSpiSemphoreHandle = xSemaphoreCreateCountingStatic( MAX_NUM_AMB, 0, &gSpiSemphoreBuffer);
    while (gSpiSemphoreHandle == NULL);
}

/**
 * @brief This function will send data to a specified SPI and then retrieve the data
 *        it sends back to you.
 *
 * @param hSpi          A pointer to the handle of the SPI port to transfer data on
 * @param pTxData       Pointer to buffer holding data to send
 * @param pRxData       Pointer to buffer receiving data
 * @param Size          The number of bytes of data to exchange
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t SpiTransfer(SPI_HandleTypeDef *hSpi, uint8_t *pTxData, uint8_t *pRxData, uint16_t size)
{
    HAL_StatusTypeDef retVal = HAL_ERROR;

    if (size < SPI_MAX_NUM_BYTES)
    {
        AmbEnableWrmhlCsb(true);

        if (hSpi == AmbGetMasterSpiHandle())
        {      
            memcpy(gMasterTxBuf,pTxData,size);
            SCB_CleanDCache_by_Addr((uint32_t*)gMasterTxBuf, SPI_DMA_BUF_SIZE);
            SCB_InvalidateDCache_by_Addr((uint32_t*)gMasterRxBuf, SPI_DMA_BUF_SIZE);
            retVal = HAL_SPI_TransmitReceive_DMA(hSpi, gMasterTxBuf, gMasterRxBuf, size);
            if (retVal == HAL_OK)
            {
                DBG1_SET();
                if (xSemaphoreTake(gSpiSemphoreHandle, pdMS_TO_TICKS(SPI_WAIT_TIMEOUT)) == pdPASS)
                {
                    retVal = (hSpi->State == HAL_SPI_STATE_READY) ? HAL_OK : HAL_ERROR;
                }
                else
                {
                    retVal = HAL_ERROR;
                }
            }    
        memcpy(pRxData, gMasterRxBuf, size);
        }
        else
        {
            memcpy(gSlaveTxBuf,pTxData,size);   
            SCB_CleanDCache_by_Addr((uint32_t*)gSlaveTxBuf, SPI_DMA_BUF_SIZE);
            SCB_InvalidateDCache_by_Addr((uint32_t*)gSlaveRxBuf, SPI_DMA_BUF_SIZE);   
            retVal = HAL_SPI_TransmitReceive_DMA(hSpi, gSlaveTxBuf, gSlaveRxBuf, size);
            retVal = (retVal == HAL_OK) ? HAL_SPI_TransmitReceive_DMA(AmbGetMasterSpiHandle(), gMasterTxBuf, gMasterRxBuf,  size) : retVal;   
            if (retVal == HAL_OK)
            {
                retVal = (xSemaphoreTake(gSpiSemphoreHandle, pdMS_TO_TICKS(SPI_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
                if (retVal == HAL_OK)
                {
                    retVal = (xSemaphoreTake(gSpiSemphoreHandle, pdMS_TO_TICKS(SPI_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
                }
                if (retVal == HAL_OK)
                {
                    retVal = ((hSpi->State == HAL_SPI_STATE_READY) && (AmbGetMasterSpiHandle()->State == HAL_SPI_STATE_READY)) ? HAL_OK : HAL_ERROR;
                }
            }
            memcpy(pRxData, gSlaveRxBuf, size);
        }

        AmbEnableWrmhlCsb(false);
    }
    return (retVal == HAL_OK) ? COTA_ERROR_NONE : POST_COTA_ERROR(COTA_ERROR_SPI_TRANSFER_FAILED);
}

/**
 * @brief This function will read data from multiple AMB's simulataneously  (max 64 bit data chunks)
 * 
 * @note  Before calling this function, the address pins need to be set to one UVP per AMB, otherwise the read makes no sense.
 *
 * @param pTxData       The data to be written to all four AMB's   
 * @param pRxData       Pointer to the receive buffer.  The buffer must be 4 times the size of the data because each AMB's data is written sequentially.
 *                      Meaning, the data from AMB0 starts at byte 0, data from AMB1 starts at byte size, data from AMB2 starts at byte 2*size, etc.
 *                      pRxData can be NULL, which will cause received data to not be processed.
 * @param size          The number of bytes of data to read from each AMB (max 9)
 *
 * @param ambMask       The mask that will select which AMB's to transfer data to and from.
 *
 * @return COTA_ERROR_NONE on success; otherwise error code of type #HAL_StatusTypeDef
 */
cotaError_t SpiSimulTransfer(uint8_t* pTxData, uint8_t *pRxData, uint16_t size, ambMask_t ambMask)
{
    HAL_StatusTypeDef retVal = HAL_ERROR;
    uint8_t ii;
    uint8_t spiMasterIndex = 0;

    //Size cannot be greater than 9
    if (size < SPI_SIMUL_READ_SIZE)
    {
        AmbEnableWrmhlCsb(true);
        //First we schedule all slave transfers
        for (ii = 0; (ii < MAX_NUM_AMB) && (retVal == HAL_OK); ii++)
        {
            if (BIT_IN_MASK(ii, ambMask))
            {
                memcpy(&gSlaveTxBuf[ii*size], pTxData, size);

                if (AmbGetSpiHandle(ii) == AmbGetMasterSpiHandle())
                {
                    spiMasterIndex = ii;  //Let's skip the master, but note its index so we can schedule it last and trigger the transfer
                }
                else
                {
                    retVal = HAL_SPI_TransmitReceive_DMA(AmbGetSpiHandle(ii), &gSlaveTxBuf[ii*size], &gSlaveRxBuf[ii*size], size);
                }
            }
        }
        //Let's make sure D-cache does not interfere  
        SCB_CleanDCache_by_Addr((uint32_t*)gSlaveTxBuf, SPI_DMA_BUF_SIZE);
        SCB_InvalidateDCache_by_Addr((uint32_t*)gSlaveRxBuf, SPI_DMA_BUF_SIZE);

        //Schedule the master tranfer
        retVal = (retVal == HAL_OK) ? HAL_SPI_TransmitReceive_DMA(AmbGetMasterSpiHandle(), &gSlaveTxBuf[spiMasterIndex*size], &gSlaveRxBuf[spiMasterIndex*size], size) : retVal; 

        if (retVal == HAL_OK)
        {
            //Let's wait on confirmation on that all the transfers completed.
            for (ii = 0; (ii < MAX_NUM_AMB) && (retVal == HAL_OK); ii++)
            {
                if (BIT_IN_MASK(ii, ambMask))
                {
                    retVal = (xSemaphoreTake(gSpiSemphoreHandle, pdMS_TO_TICKS(SPI_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
                }
            }
            //Let's check the state of the SPI ports to see if the transfers are completed.
            for (ii = 0; (ii < MAX_NUM_AMB) && (retVal == HAL_OK); ii++)
            {
                if (BIT_IN_MASK(ii, ambMask))
                {
                    if (AmbGetSpiHandle(ii)->State != HAL_SPI_STATE_READY)
                    {
                        retVal = HAL_ERROR;
                    }
                }
            }
        }  
        //If requested, let's copy the received data to the passed in buffer
        if ((pRxData != NULL) && (retVal == HAL_OK))
        {
            memcpy(pRxData, gSlaveRxBuf, MAX_NUM_AMB*size); 
        }
        AmbEnableWrmhlCsb(false);
    }    

  return (retVal == HAL_OK) ? COTA_ERROR_NONE : POST_COTA_ERROR(COTA_ERROR_SPI_SIMUL_TRANS_FAILED);
}






