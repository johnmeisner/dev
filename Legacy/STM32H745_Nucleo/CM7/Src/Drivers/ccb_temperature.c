/****************************************************************************//**
* @file      ccb_temperature.c
*
* @brief     Implements the API needed to read the ccb temperatures. (Using MAX31875)
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "FreeRTOS.h"
#include "semphr.h"
#include "string.h"
#include "main.h"
#include "sysclk_driver.h"
#include "orion_config.h"
#include "i2c_multiplex.h"
#include "ccb_temperature.h"
#include "amb_control.h"
#include "debug_off.h"

#define TEMP_CCB_I2C_ADDR_START     0x48    ///< The i2c address of the temperature sensor
#define TEMP_CCB_MULTIPLEX_NUM      0       ///< The number of the i2c multiplexer that the temperature sensors are connected to.
#define TEMP_CCB_MULI_CHAN          1       ///< The lowest channel number connected to a temperature sensor.  The temperature sensors are connected consecutively to multiplexer channels 1-3

//defines used to set the temperature sensor's configuration register
#define TEMP_CONFIG_OVER_TEMP_POS  15       ///< The bit position of the over temperature bit
#define TEMP_CONF_FAULT_QUEUE_MASK 0x3      ///< The mask for fault queue
#define TEMP_CONF_FAULT_QUEUE_POS  11       ///< The bit position of the fault queue
#define TEMP_CONF_COMP_INT_POS     9        ///< The bit position of the comparator or interrupt mode bit
#define TEMP_CONF_SHUTDOWN_POS     8        ///< The bit position of the shutdown bit
#define TEMP_CONF_DATA_FORMAT_POS  7        ///< The bit position of data format extension bit
#define TEMP_CONF_RESOLUTION_MASK  0x3      ///< The resolution mask for the temperate reading
#define TEMP_CONF_RESOLUTION_POS   5        ///< The bit position of the resoltion values
#define TEMP_CONF_BUS_TIMEOUT_POS  4        ///< The bit position of the bus timeout bit
#define TEMP_CONF_PEC_POS          3        ///< The bit position of the CRC enable bit
#define TEMP_CONF_CONV_RATE_MASK   0x3      ///< The mask for converstion rate setting
#define TEMP_CONF_CONV_RATE_POS    1        ///< The bit position of the conversion rate setting
#define TEMP_CONF_ONE_SHOT_POS     0        ///< The one shot bit position


#define TEMP_CONF_SET_CONV_RATE(val)   ((val & TEMP_CONF_CONV_RATE_MASK) << TEMP_CONF_CONV_RATE_POS)    ///< Creates the conversion rate field for the configuration register
#define TEMP_CONF_SET_RESOLUTION(val)  ((val & TEMP_CONF_RESOLUTION_MASK) << TEMP_CONF_RESOLUTION_POS)  ///< Creates the resolution setting for the configuration register
#define TEMP_CONF_EN_EXT_FORMAT        (1 << TEMP_CONF_DATA_FORMAT_POS)                                 ///< Set the extended format configuration bit in the configuration register
#define TEMP_CONF_DISABLE_BUS_TIMEOUT  (1 << TEMP_CONF_BUS_TIMEOUT_POS)                                 ///< Disables the bus timeout in the configuration register

//Define used to convert the digital temperature to an analog temperature in degrees.
#define TEMP_VAL_SHIFT                 7        ///< The value of the firth bit used in centidegrees 
#define TEMP_VAL_SIGN_BIT              15       ///< The sign bit number  (We use maximum resolution and maximum range)

#define TEMP_READ_WRITE_TIMEOUT        50       ///< The maximum wait time to wait for a register read and write to a temperature sensor

/**
 * @brief Temperature resolution settings
 */
typedef enum _tempResolution_t
{
    TEMP_RESOLUTION_8BIT  = 0x0,    ///< The temperature resolution setting for 8 bits            
    TEMP_RESOLUTION_9BIT  = 0x1,    ///< The temperature resolution setting for 9 bits
    TEMP_RESOLUTION_10BIT = 0x2,    ///< The temperature resolution setting for 10 bits
    TEMP_RESOLUTION_12BIT = 0x3     ///< The temperature resoltuion setting for 12 bits
} tempResolution_t;

/**
 *  @brief Temperature registers
 */
typedef enum _tempRegAddr_t
{
    TEMP_REG_VAL =  0,              ///< The location of the digital temperature value
    TEMP_REG_CONF = 1,              ///< The location of the configuration register
    TEMP_REG_HYST = 2,              ///< The Hysterisis temperature 
    TEMP_REG_OS   = 3,              ///< The overload temperature value.
} tempRegAddr_t;

static I2CMultiplexerInfo_t gCcbMultiInfo[TEMP_CCB_NUM_OF_THERM];  ///< The multiplexer structure for each temperature sensor

/**
 * @brief Initializes the multiplexer structure for each temperature sensor
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t
 */
cotaError_t InitializeCCBTemperatures(void)
{
    uint8_t ii;
    cotaError_t ret = COTA_ERROR_NONE;
    
    for (ii = 0; (ii < TEMP_CCB_NUM_OF_THERM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = InitI2CMultiplexer(&gCcbMultiInfo[ii], TEMP_CCB_MULTIPLEX_NUM, TEMP_CCB_MULI_CHAN, TEMP_CCB_I2C_ADDR_START + ii);      
    }
  
    return ret;  
}

/**
 * @brief Starts periodic sampling of the temperature by temperature sensors
 *
 * @param convRate A value of type #tempConvRate_t
 * 
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t
 */
cotaError_t StartCCBTemperatureAcquisition(tempConvRate_t convRate)
{
    uint16_t confReg = 0;
    uint8_t ii;
    cotaError_t ret = COTA_ERROR_NONE;
    uint8_t sendData[3];
    
    confReg =   TEMP_CONF_SET_CONV_RATE(convRate) |                 //Set the conversion rate.
                TEMP_CONF_SET_RESOLUTION(TEMP_RESOLUTION_12BIT) |   //Maximum Resoltuion
                TEMP_CONF_EN_EXT_FORMAT |                           //Extended temperature range
                TEMP_CONF_DISABLE_BUS_TIMEOUT;                      //No bus resets
  
    sendData[0] = TEMP_REG_CONF;
    sendData[1] = (confReg >> 8);
    sendData[2] = (confReg & 0xff);
    for (ii = 0; (ii < TEMP_CCB_NUM_OF_THERM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = I2CMultiplexWrite(&gCcbMultiInfo[ii], sendData, sizeof(sendData), TEMP_READ_WRITE_TIMEOUT);      
    }
    
    return ret;    
}

/**
 * @brief Converts a digitial temperature read from the temperature sensor to a int16_t value in centidegrees.
 * 
 * @note  Assume the highest resolution and extended range.  
 * 
 * The digital temperature is arranged this way in the register.
 * D15  D14   D13  D12  D11  D10 D9  D8  D7  D6    D5     D4      D3       D2 D1 D0
 * Sign 128�C 64�C 32�C 16�C 8�C 4�C 2�C 1�C 0.5�C 0.25�C 0.125�C 0.0625�C 0  0  0 
 *
 * @param reg  The value read from the TEMP_REG_VAL register
 * 
 * @return The temperature value in centidegrees Celsius. 
 */
static int16_t convertToCentiDegrees(uint16_t reg)
{
    bool isSignBitOn = (reg & TEMP_VAL_SIGN_BIT);
    int32_t temp;
    
    //Let's remove the sign bit and add it later
    reg &= (reg & (~TEMP_VAL_SIGN_BIT));
    
    temp = reg;
    
    //We need to shift the bits over 7, so bit 7 lines up with bit 0 and then multiply by 100 to convert to centicelsius
    //but we don't want to lose the information in bits D6 through D3.  It turns out, it doesn't matter which order
    //we do this in. So let's multiply by 100 first, and then do the bit shift.
    temp = CELSIUS_TO_CENTICELSIUS(temp);  
    temp >>= TEMP_VAL_SHIFT;
  
    temp = isSignBitOn ? -temp: temp;
   
    return (int16_t)temp;
}

/**
 * @brief Read a temperature from a CCB temperature sensor
 *        To read a temperature you must first write the register address you want to read.
 *        Then you read the address.
 * @param tempNum     The number of the sensor to sample
 * @param temperature A pointer to a int16_t to receive the temperature value in centidegrees C
 * 
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t
 */
cotaError_t ReadCCBTemperature(tempNum_t tempNum, int16_t* temperature)
{
    cotaError_t ret = COTA_ERROR_NONE;
    uint8_t data[sizeof(uint16_t)];
    uint16_t reg;
    
    if (tempNum < TEMP_CCB_NUM_OF_THERM)
    {
        data[0] = TEMP_REG_VAL;
        ret = I2CMultiplexWrite(&gCcbMultiInfo[tempNum], data, sizeof(uint8_t), TEMP_READ_WRITE_TIMEOUT);          
         
        ret = (ret == COTA_ERROR_NONE) ? I2CMultiplexRead(&gCcbMultiInfo[tempNum], data, sizeof(data), TEMP_READ_WRITE_TIMEOUT): ret;     
        
        if (ret == COTA_ERROR_NONE)
        {
            reg = (data[0] << 8) | data[1];
            *temperature = convertToCentiDegrees(reg);
        }
    }
    else
    {
        ret = COTA_ERROR_INVALID_CCB_TERMOMETEER;      
    }
    return ret;
}
