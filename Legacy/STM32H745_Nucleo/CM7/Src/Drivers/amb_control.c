/****************************************************************************//**
* @file      amb_control.c
*
* @brief     Implements functions to control the AMB's
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/

#include "main.h"
#include "orion_config.h"
#include "amb_control.h"

#if 0
/// @todo The following defines are for development on the disco board and should be deleted when we get the new ccb
#define PD_DONE_TEST_GPIO_Port  AMB_EN_TEST_GPIO_Port
#define PD_DONE_TEST_Pin        AMB_EN_TEST_Pin
#define AMB0_SPI_DISABLE_GPIO_Port  SPI_DISABLE_TEST_GPIO_Port
#define AMB1_SPI_DISABLE_GPIO_Port  SPI_DISABLE_TEST_GPIO_Port
#define AMB2_SPI_DISABLE_GPIO_Port  SPI_DISABLE_TEST_GPIO_Port
#define AMB3_SPI_DISABLE_GPIO_Port  SPI_DISABLE_TEST_GPIO_Port
#define AMB0_SPI_DISABLE_Pin        SPI_DISABLE_TEST_Pin
#define AMB1_SPI_DISABLE_Pin        SPI_DISABLE_TEST_Pin
#define AMB2_SPI_DISABLE_Pin        SPI_DISABLE_TEST_Pin
#define AMB3_SPI_DISABLE_Pin        SPI_DISABLE_TEST_Pin
#define AMB0_PD_DONE_GPIO_Port      PD_DONE_TEST_GPIO_Port
#define AMB1_PD_DONE_GPIO_Port      PD_DONE_TEST_GPIO_Port
#define AMB2_PD_DONE_GPIO_Port      PD_DONE_TEST_GPIO_Port
#define AMB3_PD_DONE_GPIO_Port      PD_DONE_TEST_GPIO_Port
#define AMB0_PD_DONE_Pin            PD_DONE_TEST_Pin
#define AMB1_PD_DONE_Pin            PD_DONE_TEST_Pin
#define AMB2_PD_DONE_Pin            PD_DONE_TEST_Pin
#define AMB3_PD_DONE_Pin            PD_DONE_TEST_Pin
#define AMB0_UVP_EN_GPIO_Port       AMB_UVP_EN_TEST_GPIO_Port
#define AMB1_UVP_EN_GPIO_Port       AMB_UVP_EN_TEST_GPIO_Port
#define AMB2_UVP_EN_GPIO_Port       AMB_UVP_EN_TEST_GPIO_Port
#define AMB3_UVP_EN_GPIO_Port       AMB_UVP_EN_TEST_GPIO_Port
#define AMB0_UVP_EN_Pin             AMB_UVP_EN_TEST_Pin
#define AMB1_UVP_EN_Pin             AMB_UVP_EN_TEST_Pin
#define AMB2_UVP_EN_Pin             AMB_UVP_EN_TEST_Pin
#define AMB3_UVP_EN_Pin             AMB_UVP_EN_TEST_Pin
#define AMB0_EN_GPIO_Port           AMB_EN_TEST_GPIO_Port
#define AMB1_EN_GPIO_Port           AMB_EN_TEST_GPIO_Port
#define AMB2_EN_GPIO_Port           AMB_EN_TEST_GPIO_Port
#define AMB3_EN_GPIO_Port           AMB_EN_TEST_GPIO_Port
#define AMB0_EN_Pin                 AMB_EN_TEST_Pin
#define AMB1_EN_Pin                 AMB_EN_TEST_Pin
#define AMB2_EN_Pin                 AMB_EN_TEST_Pin
#define AMB3_EN_Pin                 AMB_EN_TEST_Pin
#define AMB_ADD1_GPIO_Port          AMB_EN_TEST_GPIO_Port
#define AMB_ADD2_GPIO_Port          AMB_EN_TEST_GPIO_Port
#define AMB_ADD3_GPIO_Port          AMB_EN_TEST_GPIO_Port
#define AMB_ADD4_GPIO_Port          AMB_EN_TEST_GPIO_Port
#define AMB_ADD1_Pin                AMB_EN_TEST_Pin
#define AMB_ADD2_Pin                AMB_EN_TEST_Pin
#define AMB_ADD3_Pin                AMB_EN_TEST_Pin
#define AMB_ADD4_Pin                AMB_EN_TEST_Pin
#endif
#define hspi1                       hspi2
#define hspi3                       hspi2
#define hspi4                       hspi2
///@todo the above defines need to be deleted when we move to the ccb\

#define AMB_ENABLE_ON               GPIO_PIN_RESET          ///< State of gpio to enable an AMB
#define AMB_ENABLE_OFF              GPIO_PIN_SET            ///< State of gpio to disable an AMB
#define UVP_ENABLE_ON               GPIO_PIN_RESET          ///< State of gpio to enable an UVP
#define UVP_ENABLE_OFF              GPIO_PIN_SET            ///< State of gpio to disable an UVP
#define SPI_ENABLE_ON               GPIO_PIN_SET            ///< State of gpio to enable an SPI
#define SPI_ENABLE_OFF              GPIO_PIN_RESET          ///< State of gpio to disable an SPI
#define PU_RAISE                    GPIO_PIN_RESET          ///< State of gpio to raise an PU pin on UVP's
#define PU_DROP                     GPIO_PIN_SET            ///< State of gpio to lower an PU pin on UVP's
#define UVP_CS_ADDR_ON              GPIO_PIN_SET            ///< Indicates a UVP chip select pin is active.
#define UVP_CS_ADDR_OFF             GPIO_PIN_RESET          ///< Indicates a UVP chip select pin is inactive.
#define UVP_CS_MASK_0               0x1                     ///< Mask for UVP chip select pin 0
#define UVP_CS_MASK_1               0x2                     ///< Mask for UVP chip select pin 1
#define UVP_CS_MASK_2               0x4                     ///< Mask for UVP chip select pin 2
#define UVP_CS_MASK_3               0x8                     ///< Mask for UVP chip select pin 3 
#define SINGLE_MODE_ON              GPIO_PIN_SET            ///< State of gpio to enable single UVP interactions
#define SINGLE_MODE_OFF             GPIO_PIN_RESET          ///< State of gpio to disable single UVP interactions
#define TX_ON                       GPIO_PIN_SET            ///< State of gpio to enable transmitting on UVP's
#define TX_OFF                      GPIO_PIN_RESET          ///< State of gpio to disable transmitting on UVP's
#define RX_ON                       GPIO_PIN_SET            ///< State of gpio to enable receiving on UVP's
#define RX_OFF                      GPIO_PIN_RESET          ///< State of gpio to disable receiving on UVP's
#define PD_DONE                     GPIO_PIN_SET            ///< State of gpio that occurs when phase detect is done
#define PD_IN_PROGRESS              GPIO_PIN_RESET          ///< State of gpio that occurs when phase detect is in progress
#define WRMHL_CSB_ENABLED           GPIO_PIN_RESET          ///< State of WRMHL_CSB to allow SPI transfers to AMB
#define WRMHL_CSB_DISABLED          GPIO_PIN_SET            ///< State of WRMHL_CSB to stop SPI transfers to AMB


/**
 * @brief This structure allows us to map AMB's to their SPI handles and GPIO pins
 */
typedef struct _ambMap
{
    GPIO_TypeDef*         spiDisablePort; //< The port of the AMB's disable GPIO
    uint16_t              spiDisablePin;  //< The pin of the AMB's disable GPIO
    GPIO_TypeDef*         ambEnPort;      //< The port of the AMB's LDO enable GPIO           
    uint16_t              ambEnPin;       //< The pin of the AMB's LDO enable GPIO
    GPIO_TypeDef*         uvpEnPort;      //< The port of the AMB's UVP enable GPIO      
    uint16_t              uvpEnPin;       //< The pin of the AMB's UVP enable GPIO
    GPIO_TypeDef*         pdDonePort;     //< The port of the AMB's phase detect done GPIO       
    uint16_t              pdDonePin;      //< The pin of the AMB's phase detect done GPIO
    GPIO_TypeDef*         puPort;         //< The port of the AMB's phase detect done GPIO       
    uint16_t              puPin;          //< The pin of the AMB's phase detect done GPIO  
    SPI_HandleTypeDef*    spi;            //< The handle to the AMB's SPI port
} ambMap_t;

/// @todo The following variable may need to be changed when the new ccb arrives
#if MASTER_AMB_SPI_PORT == 0
static SPI_HandleTypeDef* masterSpi = &hspi1; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 1
static SPI_HandleTypeDef* masterSpi = &hspi2; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 2
static SPI_HandleTypeDef* masterSpi = &hspi3; //< The handle of the master SPI port, which will control the SCLK
#elif MASTER_AMB_SPI_PORT == 3
static SPI_HandleTypeDef* masterSpi = &hspi4; //< The handle of the master SPI port, which will control the SCLK
#else
#error "MASTER_AMB_SPI_PORT is not valid"
#endif

/**
 * This array maps each AMB to its SPI handle and GPIO pins
 */
static ambMap_t ambInfo[] = 
{
    {SPI_DISABLE_0_GPIO_Port, SPI_DISABLE_0_Pin, AMB_EN_0_GPIO_Port, AMB_EN_0_Pin, UVP_EN_0_GPIO_Port, UVP_EN_0_Pin, PD_DONE_0_GPIO_Port, PD_DONE_0_Pin, PU_0_GPIO_Port, PU_0_Pin, &hspi1},
    {SPI_DISABLE_1_GPIO_Port, SPI_DISABLE_1_Pin, AMB_EN_1_GPIO_Port, AMB_EN_1_Pin, UVP_EN_1_GPIO_Port, UVP_EN_1_Pin, PD_DONE_1_GPIO_Port, PD_DONE_1_Pin, PU_1_GPIO_Port, PU_1_Pin, &hspi2},
    {SPI_DISABLE_2_GPIO_Port, SPI_DISABLE_2_Pin, AMB_EN_2_GPIO_Port, AMB_EN_2_Pin, UVP_EN_2_GPIO_Port, UVP_EN_2_Pin, PD_DONE_2_GPIO_Port, PD_DONE_2_Pin, PU_2_GPIO_Port, PU_2_Pin, &hspi3},
    {SPI_DISABLE_3_GPIO_Port, SPI_DISABLE_3_Pin, AMB_EN_3_GPIO_Port, AMB_EN_3_Pin, UVP_EN_3_GPIO_Port, UVP_EN_3_Pin, PD_DONE_3_GPIO_Port, PD_DONE_3_Pin, PU_3_GPIO_Port, PU_3_Pin,  &hspi4},
};

/**
 * @brief Enable the clock for a GPIO port
 * 
 * @note We needed to add this because CubeMx didn't generate the clock enable for GPIO's
 *
 * @param portBase    The port base of the GPIO (see #GPIO_TypeDef)
 */ 
static void enableGPIOClock(GPIO_TypeDef* portBase)
{
  switch((uint32_t)portBase)
  { 
  case (uint32_t)GPIOA:
    __HAL_RCC_GPIOA_CLK_ENABLE();
    break;
  case (uint32_t)GPIOB:
    __HAL_RCC_GPIOB_CLK_ENABLE();
    break;
  case (uint32_t)GPIOC:
    __HAL_RCC_GPIOC_CLK_ENABLE();
    break;
  case (uint32_t)GPIOD:
    __HAL_RCC_GPIOD_CLK_ENABLE();
    break;
  case (uint32_t)GPIOE:
    __HAL_RCC_GPIOE_CLK_ENABLE();
    break;
  case (uint32_t)GPIOF:
    __HAL_RCC_GPIOF_CLK_ENABLE();
    break;
  case (uint32_t)GPIOG:
    __HAL_RCC_GPIOG_CLK_ENABLE();
    break;
  case (uint32_t)GPIOH:
     __HAL_RCC_GPIOH_CLK_ENABLE();
    break;
  case (uint32_t)GPIOI:
    __HAL_RCC_GPIOI_CLK_ENABLE();
    break;
  case (uint32_t)GPIOJ:
    __HAL_RCC_GPIOJ_CLK_ENABLE();
    break;
  case (uint32_t)GPIOK:
    __HAL_RCC_GPIOK_CLK_ENABLE();
    break;
  default:
    break;   
  }
}

/**
 * @brief Configures an single output GPIO
 *        @note  A fully functional CubeMx would have configured GPIO ports for you if it was working correctly. We only need to do this because CubeMx forgets to.
 * @param portBase    The port base of the GPIO (see #GPIO_TypeDef)
 * @param gpioPinMask The pin of the GPIO
 */ 
static void configureOutputGPIO(GPIO_TypeDef* portBase, uint16_t gpioPinMask)
{
    GPIO_InitTypeDef gpioInit = {0};

    enableGPIOClock(portBase);
    gpioInit.Pin = gpioPinMask;
    gpioInit.Mode = GPIO_MODE_OUTPUT_PP;
    gpioInit.Pull = GPIO_NOPULL;
    gpioInit.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

    HAL_GPIO_Init(portBase, &gpioInit);
}  
  
/**
 * @brief Configures an single input GPIO
 *        @note A fully functional CubeMx would have configured GPIO ports for you if it was working correctly. We only need to do this because CubeMx forgets to.
 * @param portBase    The port base of the GPIO (see #GPIO_TypeDef)
 * @param gpioPinMask The pin of the GPIO
 */ 
static void configureInputGPIO(GPIO_TypeDef* portBase, uint16_t gpioPinMask)
{
    GPIO_InitTypeDef gpioInit = {0};

    enableGPIOClock(portBase);
    gpioInit.Pin = gpioPinMask;
    gpioInit.Mode = GPIO_MODE_INPUT;
    gpioInit.Pull = GPIO_NOPULL;

    HAL_GPIO_Init(portBase, &gpioInit);
}  
  
/**
 * @brief Performs necessary intialization of GPIO pins
 */
void AmbControlInit(void)
{
    uint8_t ii;

    ///@todo Add TX_CTRL, RXCTL, and PU control pins
    configureOutputGPIO(CSB_ADD0_GPIO_Port, CSB_ADD0_Pin);
    configureOutputGPIO(CSB_ADD1_GPIO_Port, CSB_ADD1_Pin);
    configureOutputGPIO(CSB_ADD2_GPIO_Port, CSB_ADD2_Pin);
    configureOutputGPIO(CSB_ADD3_GPIO_Port, CSB_ADD3_Pin);
    configureOutputGPIO(AMB_SINGLE_GPIO_Port,  AMB_SINGLE_Pin);
    configureOutputGPIO(TX_CTRL_GPIO_Port,      TX_CTRL_Pin);
    configureOutputGPIO(RX_CTRL_GPIO_Port,      RX_CTRL_Pin);

    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        configureOutputGPIO(ambInfo[ii].spiDisablePort, ambInfo[ii].spiDisablePin);
        configureOutputGPIO(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin);
        configureOutputGPIO(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin);
        configureOutputGPIO(ambInfo[ii].puPort, ambInfo[ii].puPin);
        configureInputGPIO(ambInfo[ii].pdDonePort, ambInfo[ii].pdDonePin);    
    }

    AmbEnableRx(false);
    AmbEnableTx(false);
    AmbSetPUState(AMB_DISABLE_ALL);
    AmbSpiEnable(AMB_DISABLE_ALL);
    AmbEnable(AMB_DISABLE_ALL);
}


/**
 * @brief Sets up UVP's for sending/receiving SPI traffic
 *
 * @param ambMask   A mask specifying which AMB's to enble SPI traffic on.
 *                  1 will enable the SPI by pushing the disable pin low at the UVP,
 *                  0 will disable the SPI by pushing the disable pin high at the UVP
 */
void AmbSpiEnable(ambMask_t ambMask)
{
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
            HAL_GPIO_WritePin(ambInfo[ii].spiDisablePort, ambInfo[ii].spiDisablePin, SPI_ENABLE_ON);  
        }
        else
        {
            HAL_GPIO_WritePin(ambInfo[ii].spiDisablePort, ambInfo[ii].spiDisablePin, SPI_ENABLE_OFF);  
        }
    }
}

/**
 * @brief Gets the state of the SPI enable gpios for all the AMBs
 *
 * @return A mask specifying which AMB's have SPI enabled, 1 for on, 0 or off
 */
ambMask_t AmbGetSpiEnabled(void)
{
    ambMask_t ambMask = 0;
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
       if (HAL_GPIO_ReadPin(ambInfo[ii].spiDisablePort, ambInfo[ii].spiDisablePin) == SPI_ENABLE_ON)
       {
            ambMask |= (1 << ii);
       }   
    } 
    return ambMask;
}


/**
 * @brief Sets the state of the UVP enable gpio for all the AMB's specified by the mask.  
 *        The UVP enable pin will not be set for an AMB unless the AMB enable
 *        pin for that UVP is also set.
 *
 * @param ambMask A mask specifying which AMB's to set the UVP enable gpio for, 1 for on, 0 or off
 *
 * @return COTA_ERROR_NONE on success; otherwise COTA_ERROR_AMB_NOT_ENABLED
 */ 
cotaError_t AmbUvpEnable(ambMask_t ambMask)
{
  cotaError_t ret = COTA_ERROR_NONE;
  uint8_t ii;
  for (ii = 0; ii < MAX_NUM_AMB; ii++)
  {
    if (BIT_IN_MASK(ii,ambMask))
    {
      if (HAL_GPIO_ReadPin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin) == AMB_ENABLE_ON)
      {
        HAL_GPIO_WritePin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin, UVP_ENABLE_ON);
      }
      else
      {
        //We cannot enable a UVP unless the AMB is also enabled.
        ret = COTA_ERROR_AMB_NOT_ENABLED;
      }
    }
    else
    {
      HAL_GPIO_WritePin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin, UVP_ENABLE_OFF);      
    }
  }
  
  return ret;
}

/**
 * @brief Gets the state of the UVP enable gpios for all the AMBs
 *        The UVP enable pin will not be set for an AMB unless the AMB enable
 *        pin for that uvp also set.
 *
 * @param ambMask A mask specifying which AMB's have their UVP enabled, 1 for on, 0 or off
 */
ambMask_t AmbGetUvpEnabled(void)
{
    uint8_t ii;
    ambMask_t ambMask = 0;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
       if (HAL_GPIO_ReadPin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin) == UVP_ENABLE_ON)
       {
            ambMask |= (1 << ii);
       }   
    } 
    return ambMask;
}

/**
 * @brief Sets the state of the AMB enable GPIO for all the AMB's specified by the mask.  
 *        If an AMB is disabled, the UVP enable pins will also be disabled.
 *       
 * @param ambMask   A mask specifying which AMB's to set the LDO enable gpio for, and which to not enable. 1 for enable, 0 for disable.
 */ 
void AmbEnable(ambMask_t ambMask)
{
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
             HAL_GPIO_WritePin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin, AMB_ENABLE_ON);    
        }
        else
        {
            HAL_GPIO_WritePin(ambInfo[ii].uvpEnPort, ambInfo[ii].uvpEnPin, UVP_ENABLE_OFF);
            HAL_GPIO_WritePin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin, AMB_ENABLE_OFF);      
        }
    }
}

/**
 * @brief Gets the state of the AMB enable GPIO's
 *
 * @return A mask specifying which AMB's have their LDO;s enabled. 1 for enable, 0 for disable.
 */
ambMask_t AmbGetEnabled(void)
{
    uint8_t ii;
    ambMask_t ambMask = 0;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
       if (HAL_GPIO_ReadPin(ambInfo[ii].ambEnPort, ambInfo[ii].ambEnPin) == AMB_ENABLE_ON)
       {
            ambMask |= (1 << ii);
       }
       
    } 
    return ambMask;
}
/**
 * @brief Sets the state of the Phase update pin for each AMB
 *       
 * @param ambMask   A mask specifying which AMB's to set PU gpio for, and which to not enable. 1 for raise, 0 for drop.
 */ 
void AmbSetPUState(ambMask_t ambMask)
{
    uint8_t ii;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (BIT_IN_MASK(ii,ambMask))
        {
            HAL_GPIO_WritePin(ambInfo[ii].puPort, ambInfo[ii].puPin, PU_RAISE);    
        }
        else
        {
            HAL_GPIO_WritePin(ambInfo[ii].puPort, ambInfo[ii].puPin, PU_DROP);      
        }
    }
}

/**
 * @brief Gets the state of the Phase Update for every AMB
 *
 * @return A mask specifying which state of PU update pins. 1 for raised, 0 for droped.
 */
ambMask_t AmbGetPuState(void)
{
    uint8_t ii;
    ambMask_t ambMask = 0;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
       if (HAL_GPIO_ReadPin(ambInfo[ii].puPort, ambInfo[ii].puPin) == PU_RAISE)
       {
            ambMask |= (1 << ii);
       }
       
    } 
    return ambMask;
}

/**
 * @brief Select the UVP number to interact with.  This also enables "Single" mode to allow individual UVP interactions
 *
 * @param uvpNum The UVP number to interact with.
 * @return COTA_ERROR_NONE on success; otherwise COTA_ERROR_UVP_UNKNOWN
 */
cotaError_t AmbSelectUvp(uvpNum_t uvpNum)
{ 
    cotaError_t ret = COTA_ERROR_NONE;
    
    if (uvpNum < UVPS_PER_AMB)
    {    
        HAL_GPIO_WritePin(AMB_SINGLE_GPIO_Port, AMB_SINGLE_Pin, SINGLE_MODE_OFF); 
        
        HAL_GPIO_WritePin(CSB_ADD0_GPIO_Port, CSB_ADD0_Pin, (uvpNum & UVP_CS_MASK_0) ? UVP_CS_ADDR_ON : UVP_CS_ADDR_OFF);
        HAL_GPIO_WritePin(CSB_ADD1_GPIO_Port, CSB_ADD1_Pin, (uvpNum & UVP_CS_MASK_1) ? UVP_CS_ADDR_ON : UVP_CS_ADDR_OFF);
        HAL_GPIO_WritePin(CSB_ADD2_GPIO_Port, CSB_ADD2_Pin, (uvpNum & UVP_CS_MASK_2) ? UVP_CS_ADDR_ON : UVP_CS_ADDR_OFF);
        HAL_GPIO_WritePin(CSB_ADD3_GPIO_Port, CSB_ADD3_Pin, (uvpNum & UVP_CS_MASK_3) ? UVP_CS_ADDR_ON : UVP_CS_ADDR_OFF);
    }
    else if (uvpNum == ALL_UVP_NUM)
    {
        HAL_GPIO_WritePin(AMB_SINGLE_GPIO_Port, AMB_SINGLE_Pin, SINGLE_MODE_ON);        
    }
    else
    {
        ret = COTA_ERROR_UVP_UNKNOWN;
    }
    
    return ret;
}

/**
 * @brief Gets selected UVP number
 */
uint8_t AmbGetSelectedUvp(void)
{
    uint8_t ret = 0;
    
    if (HAL_GPIO_ReadPin(AMB_SINGLE_GPIO_Port, AMB_SINGLE_Pin) == SINGLE_MODE_ON)
    {
        ret = ALL_UVP_NUM;
    }
    else
    {
        if (HAL_GPIO_ReadPin(CSB_ADD0_GPIO_Port, CSB_ADD0_Pin) == UVP_CS_ADDR_ON)
        {
            ret |= UVP_CS_MASK_0;  
        }
        if (HAL_GPIO_ReadPin(CSB_ADD1_GPIO_Port, CSB_ADD1_Pin) == UVP_CS_ADDR_ON)
        {
            ret |= UVP_CS_MASK_1;  
        }
        if (HAL_GPIO_ReadPin(CSB_ADD2_GPIO_Port, CSB_ADD2_Pin) == UVP_CS_ADDR_ON)
        {
            ret |= UVP_CS_MASK_2;  
        }
        if (HAL_GPIO_ReadPin(CSB_ADD3_GPIO_Port, CSB_ADD3_Pin) == UVP_CS_ADDR_ON)
        {
            ret |= UVP_CS_MASK_3;
        }    
    }
    return ret;
}

/**
 * @brief Enable or disable the Tx pin to the AMB's
 *
 * @param on If true, enable the pin, otherwise disable the pin.
 */
void AmbEnableTx(bool on)
{
    HAL_GPIO_WritePin(TX_CTRL_GPIO_Port, TX_CTRL_Pin, on ? TX_ON : TX_OFF);
}

/**
 * @brief Enable or disable the Rx pin to the AMB's
 *
 * @param on If true, enable the pin, otherwise disable the pin.
 */
void AmbEnableRx(bool on)
{
    HAL_GPIO_WritePin(RX_CTRL_GPIO_Port, RX_CTRL_Pin, on ? RX_ON : RX_OFF);  
}

/**
 * @brief Enable or disable the WRMHLH_CSB pin to the AMB's
 *
 * @param on If true, enable the pin, otherwise disable the pin.
 */
void AmbEnableWrmhlCsb(bool on)
{
    HAL_GPIO_WritePin(WRMHL_CSB_GPIO_Port, WRMHL_CSB_Pin, on ? WRMHL_CSB_ENABLED : WRMHL_CSB_DISABLED);  
}


/**
 * @brief Determines if Tx mode is enabled.
 *
 * @return true if TX pin is enabled, false if not.
 */
bool AmbInTxMode(void)
{
    return (HAL_GPIO_ReadPin(TX_CTRL_GPIO_Port, TX_CTRL_Pin) == TX_ON);
}

/**
 * @brief Determines if Rx mode is enabled.
 * 
 * @return true if RX pin is enabled, false if not.
 */
bool AmbInRxMode(void)
{
    return (HAL_GPIO_ReadPin(RX_CTRL_GPIO_Port, RX_CTRL_Pin) == RX_ON);
}


/**
 * @brief Determines if WRMHL_CSB mode is enabled.
 * 
 * @return true if WRMHL_CSB pin is enabled, false if not.
 */
bool AmbIsWrmhlCsbEnabled(void)
{
    return (HAL_GPIO_ReadPin(WRMHL_CSB_GPIO_Port, WRMHL_CSB_Pin) == WRMHL_CSB_ENABLED);
}


/**
 * @brief Gets the state of the Phase Dedect for every AMB
 *
 * @return A mask specifying which state of PS update pins. 1 for phase detect done, 0 for phase detect in progress.
 */
ambMask_t AmbGetPhaseDetect(void)
{
    uint8_t ii;
    ambMask_t ambMask = 0;
    for (ii = 0; ii < MAX_NUM_AMB; ii++)
    {
        if (HAL_GPIO_ReadPin(ambInfo[ii].pdDonePort, ambInfo[ii].pdDonePin) == PD_DONE)
        {
            ambMask |= (1 << ii);
        }
    } 
    return ambMask;
}

/**
 * @brief  Retrieves the SPI handle for a single AMB
 * 
 * @param  The AMB number of the AMB
 *
 * @return A pointer to the SPI handle for the AMB
 */
SPI_HandleTypeDef* AmbGetSpiHandle(ambNum_t ambNum)
{
  SPI_HandleTypeDef* spiHandle = NULL;
  
  if ( ambNum < MAX_NUM_AMB)
  {
    return ambInfo[ambNum].spi;
  }
  
  return spiHandle;
}

/**
 * @brief  Retrieves the SPI handle for the master SPI
 *
 * @return A pointer to the SPI handle for master SPI
 */
SPI_HandleTypeDef* AmbGetMasterSpiHandle(void)
{
  return masterSpi;
}