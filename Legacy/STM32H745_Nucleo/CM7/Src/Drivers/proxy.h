/**
 * @FILE       proxy.h
 *
 * @BRIEF      Proxy communication driver header file
 *
 * @COPYRIGHT  THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
 *             ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
 *             STRICTLY PROHIBITED.  COPYRIGHT 2019 OSSIA INC. (SUBJECT TO LIMITED
 *             DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
 */


#ifndef _PROXY_DRIVER_HEADER_
#define _PROXY_DRIVER_HEADER_

#include "stdint.h"
#include "stdbool.h"
#include "ctrl_task.h"

bool     ProxyIsRxQueueEmpty(void);
void     ProxyDriverInit(void);
bool     ProxySendMessage(uint8_t * buf, uint8_t size);
uint32_t ProxyGetMessage(uint8_t * buf, uint8_t size);

#endif   // ifndef _PROXY_DRIVER_HEADER_
