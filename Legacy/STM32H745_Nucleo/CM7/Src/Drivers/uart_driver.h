/****************************************************************************//**
* @file      uart_driver.h
*
* @brief     Header file containing prototypes to provide thread safe access to the UART
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/


#ifndef _UART_DRIVER_H_
#define _UART_DRIVER_H_
#include "error.h"

#define MAX_UART_MSG_SIZE                       256         ///< The maximum size of a uart message

void InitCotaUart(void);

cotaError_t CotaUartTransmit(uint8_t *pData, uint16_t size, uint32_t timeout);
cotaError_t CotaUartWaitForRx(uint8_t *pData, uint16_t size, uint32_t timeout);

#ifdef UART_DRIVER_TEST
void InitUartTest(void);
#endif

#endif  //#ifndef _UART_DRIVER_H_