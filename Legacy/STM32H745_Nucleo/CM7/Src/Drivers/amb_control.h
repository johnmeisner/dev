/****************************************************************************//**
* @file      amb_control.h
*
* @brief     Header file containing prototypes to control the AMB's
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _AMB_CONTROL_H_
#define _AMB_CONTROL_H_
#include "orion_config.h"
#include "stdbool.h"
#include "error.h"


#define UVPS_PER_AMB            16                                            ///< The number of UVP's available on a single AMB.
#define AMUS_PER_UVP            4                                             ///< The number of AMUs available on a UVP.
#define TOTAL_UVPS              (UVPS_PER_AMB * MAX_NUM_AMB)                  ///< The total number of UVPS on all AMB's
#define MAX_ANTENNAS            (TOTAL_UVPS * AMUS_PER_UVP)                   ///< The maximum number of antennas per AMB
#define MAX_NUM_ANTS            (MAX_NUM_AMB * MAX_CHANNEL_ANTS)              ///< The total number of antennas possible on a transmitter with the maximum number of AMBs
#define ALL_AMB_MASK            ((1UL << (MAX_NUM_AMB)) - 1)                  ///< The mask for selecting all AMB's
#define ALL_UVP_MASK            ((1UL << (UVPS_PER_AMB)) - 1)                 ///< The mask for selecting all UVP's
#define ALL_AMU_MASK            ((1UL << (MAX_NUM_AMU)) - 1)                  ///< The mask for selecting all AMU's
#define ALL_UVP_NUM             UVPS_PER_AMB                                  ///< A UVP number that indicates all UVPS selected.
#define BIT_IN_MASK(bit, mask)  ((1 << (bit)) & (mask))                       ///< Checks if a bit is in a mask.
#define NUM_TO_MASK(num)        (1 << (num))                                  ///< converts a number to a one bit mask
#define MASK_TO_NUM(mask)       (31-__CLZ(mask))                              ///< converts a one bit mask to a number
#define GEN_SOLID_MASK(size)    ((1 << (size)) - 1)                           ///< Generates a solid mask of ones of a given size in bits
#define IS_NUMBER_IN_MASK(data, mask) (!((data) & (~(mask))))                 ///< Returns true if all bits that are set in data are also set in mask
#define IS_SINGLE_ITEM_SELECTED(mask)   (mask && ((mask & (mask - 1)) == 0))  ///< Determines that only a single bit is used as an input mask

#define AMU0                    (0)                                           ///< Number value for a specific AMU index
#define AMU1                    (1)                                           ///< Number value for a specific AMU index 
#define AMU2                    (2)                                           ///< Number value for a specific AMU index 
#define AMU3                    (3)                                           ///< Number value for a specific AMU index

#define AMB0                    (0)                                           ///< Number value for a specific AMB 0
#define AMB1                    (1)                                           ///< Number value for a specific AMB 1
#define AMB2                    (2)                                           ///< Number value for a specific AMB 2
#define AMB3                    (3)                                           ///< Number value for a specific AMB 3

#define AMB0_MASK               (NUM_TO_MASK(AMB0))                           ///< Mask for AMU0
#define AMB1_MASK               (NUM_TO_MASK(AMB1))                           ///< Mask for AMU1
#define AMB2_MASK               (NUM_TO_MASK(AMB2))                           ///< Mask for AMU2
#define AMB3_MASK               (NUM_TO_MASK(AMB3))                           ///< Mask for AMU3
#define AMB_DISABLE_ALL         0                                             ///< Indicates that a line is to be disabled for all AMB's

typedef uint8_t  ambNum_t;
typedef uint8_t  uvpNum_t;
typedef uint8_t  amuNum_t;
typedef uint8_t  ambMask_t;
typedef uint16_t uvpMask_t;
typedef uint16_t amuMask_t;

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;
extern SPI_HandleTypeDef hspi3;
extern SPI_HandleTypeDef hspi4;

void AmbControlInit(void);

void AmbEnable(ambMask_t ambMask);
cotaError_t AmbUvpEnable(ambMask_t ambMask);
void AmbSpiEnable(ambMask_t ambMask);
void AmbSetPUState(ambMask_t ambMask);
cotaError_t AmbSelectUvp(uvpNum_t uvpNum);
void AmbEnableTx(bool on);
void AmbEnableRx(bool on);
void AmbEnableWrmhlCsb(bool on);
ambMask_t AmbGetEnabled(void);
ambMask_t AmbGetUvpEnabled(void);
ambMask_t AmbGetSpiEnabled(void);
ambMask_t AmbGetPuState(void);
ambMask_t AmbGetPhaseDetect(void);
uint8_t  AmbGetSelectedUvp(void);
bool AmbInRxMode(void);
bool AmbInTxMode(void);
bool AmbIsWrmhlCsbEnabled(void);

SPI_HandleTypeDef* AmbGetSpiHandle(ambNum_t ambNum);
SPI_HandleTypeDef* AmbGetMasterSpiHandle(void);

#endif //#ifndef _AMB_CONTROL_H_
