/****************************************************************************//**
* @file      sysclk_driver.c
*
* @brief     Control the system clock that is sent to the UVP's
*            @todo We were unable to test the clock fully with development board
                   We need to verify that we can set the correct initial frequency
                   and change the transmit frequency.  
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "FreeRTOS.h"
#include "semphr.h"
#include "string.h"
#include "main.h"
#include "sysclk_driver.h"
#include "orion_config.h"
#include "debug_off.h"

extern I2C_HandleTypeDef hi2c1;                         ///The handle to the I2C hal driver for the system clock

#define SYSCLK_I2C_ADDR         (0x75)                  ///< The 7 bit I2C address of system clock
#define SYSCLK_ADDR_BYTE        (SYSCLK_I2C_ADDR << 1)  ///< The byte that is passed as the address to the hal driver, which requires the address sit in the upper 7 bits
#define PAGE_REG_ADDR           0x1                     ///< The register address in every page that sets the page number
#define MAX_NUM_REG             1                       ///< The maximum number of clock configurations
#define I2C_WAIT_TIMEOUT        10                      ///< A timeout to wait to I2C Tx or Rx completion in milliseconds
#define INVALID_PAGE            0xFF                    ///< An invalid page number to the system clock.  Indicates the page number must be updated on the last call.
#define VARIANT_1_CLOCK         0                       ///< The index in the register array for the VARIANT 1 clock settings
#define SYSCLK_DMA_BUFFER_LEN   32                      ///< The system clock buffer length, which we align to 32 bytes
#define N_DIV_NUM_REG_SIZE      6                       ///< The size in bytes of a N divider numerator register
#define DELAY_REG_SIZE          2                       ///< The size in bytes the N delay register
#define MAX_NUM_NDIV            5                       ///< The maximum number of the N dividers
#define FREQ_RATIO_V1           16                      ///< Used to adjust tx frequency
#define IN_FREQ_MHZ             50                      ///< Used to adjust tx frequency
#define SYSCLK_PAGE_ADDR(addr)  (addr >> 8)             ///< Gets the page addr from a register address
#define SYSCLK_REG_ADDR(addr)   (addr & 0xff)           ///< Get the position in the page from a register address

static SemaphoreHandle_t gSysClkI2CTxSem;               ///< Semaphore signaling a complete i2c transaction
static StaticSemaphore_t gSysClkI2CTxSemBuff;           ///< The buffer holding the information for the #gSysClkI2CSem semaphore

static SemaphoreHandle_t gSysClkI2CRxSem;               ///< Semaphore signaling a complete i2c transaction
static StaticSemaphore_t gSysClkI2CRxSemBuff;           ///< The buffer holding the information for the #gSysClkI2CSem semaphore
static uint8_t gLastPageWritten = INVALID_PAGE;         ///< We need to keep track of the last page of addresses we write to in the system clock so we don't have to keep re-writing it.

/**
 * @brief This structure maps the clock register address to their values. 
   @note  Even though there is only one set of register values at this point,
          we maintain the array structure, so we can add more later if needed.
 */
typedef struct _clockTreeRegister_t
{
    uint16_t addr;                      ///< The address in the clock to write to
    uint8_t  regs[MAX_NUM_REG];         ///< The values to write to the clock register

} clockTreeRegister_t;

/**
 * @brief the preamble registers are written first 
 */
static clockTreeRegister_t preambleList[] = {
    {0x0B24, {0xC0}},
    {0x0B25, {0x00}},
    {0x0502, {0x01}},
    {0x0505, {0x03}},
    {0x0957, {0x17}},
    {0x0B4E, {0x1A}}
};
const int preambleListSize = (sizeof(preambleList)/sizeof(clockTreeRegister_t));  ///< Number of registers to write in the preambleList

/**
 * @brief These register values are written after the pre-amble
 */
static clockTreeRegister_t registerList[] = {
    {0x0006, {0x00}},
    {0x0007, {0x00}},
    {0x0008, {0x00}},
    {0x000B, {0x74}},
    {0x0017, {0xD0}},
    {0x0018, {0xFD}},
    {0x0021, {0x0E}},
    {0x0022, {0x00}},
    {0x002B, {0x02}},
    {0x002C, {0x22}},
    {0x002D, {0x04}},
    {0x002E, {0x00}},
    {0x002F, {0x00}},
    {0x0030, {0x88}},
    {0x0031, {0x00}},
    {0x0032, {0x00}},
    {0x0033, {0x00}},
    {0x0034, {0x00}},
    {0x0035, {0x00}},
    {0x0036, {0x00}},
    {0x0037, {0x00}},
    {0x0038, {0x88}},
    {0x0039, {0x00}},
    {0x003A, {0x00}},
    {0x003B, {0x00}},
    {0x003C, {0x00}},
    {0x003D, {0x00}},
    {0x0041, {0x00}},
    {0x0042, {0x05}},
    {0x0043, {0x00}},
    {0x0044, {0x00}},
    {0x009E, {0x00}},
    {0x0102, {0x01}},
    {0x0108, {0x06}},
    {0x0109, {0x09}},
    {0x010A, {0x33}},
    {0x010B, {0x08}},
    {0x010D, {0x06}},
    {0x010E, {0x09}},
    {0x010F, {0x33}},
    {0x0110, {0x09}},
    {0x0112, {0x06}},
    {0x0113, {0x09}},
    {0x0114, {0x33}},
    {0x0115, {0x09}},
    {0x0117, {0x06}},
    {0x0118, {0x09}},
    {0x0119, {0x33}},
    {0x011A, {0x08}},
    {0x011C, {0x01}},
    {0x011D, {0x09}},
    {0x011E, {0x33}},
    {0x011F, {0x08}},
    {0x0121, {0x01}},
    {0x0122, {0x09}},
    {0x0123, {0x33}},
    {0x0124, {0x08}},
    {0x0126, {0x01}},
    {0x0127, {0x09}},
    {0x0128, {0x33}},
    {0x0129, {0x08}},
    {0x012B, {0x01}},
    {0x012C, {0x09}},
    {0x012D, {0x33}},
    {0x012E, {0x08}},
    {0x0130, {0x06}},
    {0x0131, {0x09}},
    {0x0132, {0x33}},
    {0x0133, {0x0B}},
    {0x013A, {0x06}},
    {0x013B, {0x09}},
    {0x013C, {0x33}},
    {0x013D, {0x0C}},
    {0x013F, {0x00}},
    {0x0140, {0x00}},
    {0x0141, {0x40}},
    {0x0206, {0x00}},
    {0x0208, {0x00}},
    {0x0209, {0x00}},
    {0x020A, {0x00}},
    {0x020B, {0x00}},
    {0x020C, {0x00}},
    {0x020D, {0x00}},
    {0x020E, {0x00}},
    {0x020F, {0x00}},
    {0x0210, {0x00}},
    {0x0211, {0x00}},
    {0x0212, {0x01}},
    {0x0213, {0x00}},
    {0x0214, {0x00}},
    {0x0215, {0x00}},
    {0x0216, {0x00}},
    {0x0217, {0x00}},
    {0x0218, {0x01}},
    {0x0219, {0x00}},
    {0x021A, {0x00}},
    {0x021B, {0x00}},
    {0x021C, {0x00}},
    {0x021D, {0x00}},
    {0x021E, {0x00}},
    {0x021F, {0x00}},
    {0x0220, {0x00}},
    {0x0221, {0x00}},
    {0x0222, {0x00}},
    {0x0223, {0x00}},
    {0x0224, {0x00}},
    {0x0225, {0x00}},
    {0x0226, {0x00}},
    {0x0227, {0x00}},
    {0x0228, {0x00}},
    {0x0229, {0x00}},
    {0x022A, {0x00}},
    {0x022B, {0x00}},
    {0x022C, {0x00}},
    {0x022D, {0x00}},
    {0x022E, {0x00}},
    {0x022F, {0x00}},
    {0x0235, {0x00}},
    {0x0236, {0x00}},
    {0x0237, {0x00}},
    {0x0238, {0x00}},
    {0x0239, {0x84}},
    {0x023A, {0x00}},
    {0x023B, {0x00}},
    {0x023C, {0x00}},
    {0x023D, {0x00}},
    {0x023E, {0x80}},
    {0x024A, {0x00}},
    {0x024B, {0x00}},
    {0x024C, {0x00}},
    {0x024D, {0x00}},
    {0x024E, {0x00}},
    {0x024F, {0x00}},
    {0x0250, {0x00}},
    {0x0251, {0x00}},
    {0x0252, {0x00}},
    {0x0253, {0x00}},
    {0x0254, {0x00}},
    {0x0255, {0x00}},
    {0x0256, {0x00}},
    {0x0257, {0x00}},
    {0x0258, {0x00}},
    {0x0259, {0x00}},
    {0x025A, {0x00}},
    {0x025B, {0x00}},
    {0x025C, {0x00}},
    {0x025D, {0x00}},
    {0x025E, {0x00}},
    {0x025F, {0x00}},
    {0x0260, {0x00}},
    {0x0261, {0x00}},
    {0x0262, {0x00}},
    {0x0263, {0x00}},
    {0x0264, {0x00}},
    {0x0268, {0x00}},
    {0x0269, {0x00}},
    {0x026A, {0x00}},
    {0x026B, {0x53}},
    {0x026C, {0x51}},
    {0x026D, {0x52}},
    {0x026E, {0x5F}},
    {0x026F, {0x43}},
    {0x0270, {0x43}},
    {0x0271, {0x42}},
    {0x0272, {0x00}},
    {0x0302, {0x00}},
    {0x0303, {0x00}},
    {0x0304, {0x00}},
    {0x0305, {0x00}},
    {0x0306, {0x21}},
    {0x0307, {0x00}},
    {0x0308, {0x00}},
    {0x0309, {0x00}},
    {0x030A, {0x00}},
    {0x030B, {0xC4}},
    {0x030C, {0x00}},
    {0x030D, {0x00}},
    {0x030E, {0x00}},
    {0x030F, {0x00}},
    {0x0310, {0x00}},
    {0x0311, {0x21}},
    {0x0312, {0x00}},
    {0x0313, {0x00}},
    {0x0314, {0x00}},
    {0x0315, {0x00}},
    {0x0316, {0xC4}},
    {0x0317, {0x00}},
    {0x0318, {0x00}},
    {0x0319, {0x00}},
    {0x031A, {0x00}},
    {0x031B, {0x00}},
    {0x031C, {0x00}},
    {0x031D, {0x00}},
    {0x031E, {0x00}},
    {0x031F, {0x00}},
    {0x0320, {0x00}},
    {0x0321, {0x00}},
    {0x0322, {0x00}},
    {0x0323, {0x00}},
    {0x0324, {0x00}},
    {0x0325, {0x00}},
    {0x0326, {0x00}},
    {0x0327, {0x42}},
    {0x0328, {0x00}},
    {0x0329, {0x00}},
    {0x032A, {0x00}},
    {0x032B, {0x00}},
    {0x032C, {0x80}},
    {0x032D, {0x00}},
    {0x032E, {0x00}},
    {0x032F, {0x00}},
    {0x0330, {0x00}},
    {0x0331, {0x00}},
    {0x0332, {0x42}},
    {0x0333, {0x00}},
    {0x0334, {0x00}},
    {0x0335, {0x00}},
    {0x0336, {0x00}},
    {0x0337, {0x80}},
    {0x0338, {0x00}},
    {0x0339, {0x1F}},
    {0x033B, {0xA6}},
    {0x033C, {0x29}},
    {0x033D, {0x02}},
    {0x033E, {0x00}},
    {0x033F, {0x00}},
    {0x0340, {0x00}},
    {0x0341, {0xA6}},
    {0x0342, {0x29}},
    {0x0343, {0x02}},
    {0x0344, {0x00}},
    {0x0345, {0x00}},
    {0x0346, {0x00}},
    {0x0347, {0x00}},
    {0x0348, {0x00}},
    {0x0349, {0x00}},
    {0x034A, {0x00}},
    {0x034B, {0x00}},
    {0x034C, {0x00}},
    {0x034D, {0x00}},
    {0x034E, {0x00}},
    {0x034F, {0x00}},
    {0x0350, {0x00}},
    {0x0351, {0x00}},
    {0x0352, {0x00}},
    {0x0353, {0x00}},
    {0x0354, {0x00}},
    {0x0355, {0x00}},
    {0x0356, {0x00}},
    {0x0357, {0x00}},
    {0x0358, {0x00}},
    {0x0359, {0x19}},
    {0x035A, {0x2B}},
    {0x035B, {0x00}},
    {0x035C, {0x00}},
    {0x035D, {0x00}},
    {0x035E, {0x00}},
    {0x035F, {0x00}},
    {0x0360, {0x00}},
    {0x0361, {0x00}},
    {0x0362, {0xBE}},
    {0x0802, {0x00}},
    {0x0803, {0x00}},
    {0x0804, {0x00}},
    {0x0805, {0x00}},
    {0x0806, {0x00}},
    {0x0807, {0x00}},
    {0x0808, {0x00}},
    {0x0809, {0x00}},
    {0x080A, {0x00}},
    {0x080B, {0x00}},
    {0x080C, {0x00}},
    {0x080D, {0x00}},
    {0x080E, {0x00}},
    {0x080F, {0x00}},
    {0x0810, {0x00}},
    {0x0811, {0x00}},
    {0x0812, {0x00}},
    {0x0813, {0x00}},
    {0x0814, {0x00}},
    {0x0815, {0x00}},
    {0x0816, {0x00}},
    {0x0817, {0x00}},
    {0x0818, {0x00}},
    {0x0819, {0x00}},
    {0x081A, {0x00}},
    {0x081B, {0x00}},
    {0x081C, {0x00}},
    {0x081D, {0x00}},
    {0x081E, {0x00}},
    {0x081F, {0x00}},
    {0x0820, {0x00}},
    {0x0821, {0x00}},
    {0x0822, {0x00}},
    {0x0823, {0x00}},
    {0x0824, {0x00}},
    {0x0825, {0x00}},
    {0x0826, {0x00}},
    {0x0827, {0x00}},
    {0x0828, {0x00}},
    {0x0829, {0x00}},
    {0x082A, {0x00}},
    {0x082B, {0x00}},
    {0x082C, {0x00}},
    {0x082D, {0x00}},
    {0x082E, {0x00}},
    {0x082F, {0x00}},
    {0x0830, {0x00}},
    {0x0831, {0x00}},
    {0x0832, {0x00}},
    {0x0833, {0x00}},
    {0x0834, {0x00}},
    {0x0835, {0x00}},
    {0x0836, {0x00}},
    {0x0837, {0x00}},
    {0x0838, {0x00}},
    {0x0839, {0x00}},
    {0x083A, {0x00}},
    {0x083B, {0x00}},
    {0x083C, {0x00}},
    {0x083D, {0x00}},
    {0x083E, {0x00}},
    {0x083F, {0x00}},
    {0x0840, {0x00}},
    {0x0841, {0x00}},
    {0x0842, {0x00}},
    {0x0843, {0x00}},
    {0x0844, {0x00}},
    {0x0845, {0x00}},
    {0x0846, {0x00}},
    {0x0847, {0x00}},
    {0x0848, {0x00}},
    {0x0849, {0x00}},
    {0x084A, {0x00}},
    {0x084B, {0x00}},
    {0x084C, {0x00}},
    {0x084D, {0x00}},
    {0x084E, {0x00}},
    {0x084F, {0x00}},
    {0x0850, {0x00}},
    {0x0851, {0x00}},
    {0x0852, {0x00}},
    {0x0853, {0x00}},
    {0x0854, {0x00}},
    {0x0855, {0x00}},
    {0x0856, {0x00}},
    {0x0857, {0x00}},
    {0x0858, {0x00}},
    {0x0859, {0x00}},
    {0x085A, {0x00}},
    {0x085B, {0x00}},
    {0x085C, {0x00}},
    {0x085D, {0x00}},
    {0x085E, {0x00}},
    {0x085F, {0x00}},
    {0x0860, {0x00}},
    {0x0861, {0x00}},
    {0x090E, {0x03}},
    {0x091C, {0x04}},
    {0x0943, {0x01}},
    {0x0949, {0x02}},
    {0x094A, {0x20}},
    {0x094E, {0x49}},
    {0x094F, {0x02}},
    {0x095E, {0x00}},
    {0x0A02, {0x00}},
    {0x0A03, {0x1B}},
    {0x0A04, {0x18}},
    {0x0A05, {0x1B}},
    {0x0A14, {0x00}},
    {0x0A1A, {0x00}},
    {0x0A20, {0x00}},
    {0x0A26, {0x00}},
    {0x0A2C, {0x00}},
    {0x0B44, {0x0F}},
    {0x0B4A, {0x04}},
    {0x0B57, {0x03}},
    {0x0B58, {0x01}}
};
const int registerListSize = (sizeof(registerList)/sizeof(registerList[0]));   ///< Number of registers to write in the registerList

/**
 * @brief These register values are written after all the registers above
 */
static clockTreeRegister_t postambleList[] = {
    {0x001C, {0x01}},  //soft reset
    {0x0B24, {0xC3}},  //Post amble
    {0x0B25, {0x02}}   //Post amble
};
const int postambleListSize = (sizeof(postambleList)/sizeof(postambleList[0])); ///< Number of registers to write in the postAmbleList

/**
 * @brief An event that occurs when transmit over i2c is complete
 *
 * @param hi2c handle to i2c bus generating event
 */ 
void SysClock_I2C_TxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(gSysClkI2CTxSem, &xHigherPriorityTaskWoken);
    DBG1_RESET();
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @brief An event that occurs when receive over i2c is complete
 * 
 * @param hi2c handle to i2c bus generating event
 */ 
void SysClock_I2C_RxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(gSysClkI2CRxSem, &xHigherPriorityTaskWoken); 
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @brief Write to the page address of the system clock
 * 
 * @param page The page value to write
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_WRITE_PAGE on failure.
 */
static cotaError_t sysClkPageWrite(uint8_t page)
{
    HAL_StatusTypeDef ret = HAL_OK;
    uint8_t sendData[2];
    
    if (page != gLastPageWritten)
    {
        sendData[0] = PAGE_REG_ADDR;
        sendData[1] = page;

        ret = HAL_I2C_Master_Transmit_IT(&SYSCLK_I2C_HANDLE, SYSCLK_ADDR_BYTE, sendData, sizeof(sendData));
        
        if (ret == HAL_OK)
        {
            ret = (xSemaphoreTake(gSysClkI2CTxSem, pdMS_TO_TICKS(I2C_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
        }
        
        gLastPageWritten = (ret == HAL_OK) ? page : INVALID_PAGE;       
    }
    return (ret == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_SYSCLK_FAILED_TO_WRITE_PAGE;
}

/**
 * @brief Write a system clock register to its address
 * 
 * @param addr      The address of the register
 * @param regVal    The value to write to the register
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG on failure.
 */
static cotaError_t sysClkWriteReg(uint16_t addr, uint8_t regVal)
{
    cotaError_t ret;
    HAL_StatusTypeDef retHal;
    uint8_t sendData[2];

    DBG2_RESET();
    ret = sysClkPageWrite(SYSCLK_PAGE_ADDR(addr));
    DBG2_SET();
    retHal = HAL_OK;
    
    if (ret == COTA_ERROR_NONE)
    {
        sendData[0] = SYSCLK_REG_ADDR(addr);
        sendData[1] = regVal;
        
        DBG3_RESET();   
        retHal = HAL_I2C_Master_Transmit_IT(&SYSCLK_I2C_HANDLE, SYSCLK_ADDR_BYTE, sendData, sizeof(sendData));  
        DBG3_SET();
        
        if (retHal == HAL_OK)
        {
            DBG4_RESET();
            retHal = (xSemaphoreTake(gSysClkI2CTxSem, pdMS_TO_TICKS(I2C_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
            DBG4_SET();
        }
    
        ret = (retHal == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG;
    }
    DBG1_SET();
    return ret;
}

/**
 * @brief Read a system clock register
 *        A clock register is read by first writing the page, then writing the 
 *        register location in the page, then reading the value.
 *        
 * @param addr      The address of the register
 * @param regVal    The value to read from the register
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_READ_REG on failure.
 */
static cotaError_t sysClkReadReg(uint16_t addr, uint8_t* regVal)
{
    cotaError_t ret = sysClkPageWrite(SYSCLK_PAGE_ADDR(addr));
    HAL_StatusTypeDef retHal = HAL_OK;
    uint8_t regAddr = SYSCLK_REG_ADDR(addr);
    
    if (ret == COTA_ERROR_NONE)
    {                  
        retHal = HAL_I2C_Master_Transmit_IT(&SYSCLK_I2C_HANDLE, SYSCLK_ADDR_BYTE, &regAddr, sizeof(uint8_t));  
      
        if (retHal == HAL_OK)
        {
            retHal = (xSemaphoreTake(gSysClkI2CTxSem, pdMS_TO_TICKS(I2C_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
        }
        
        retHal = (retHal == HAL_OK) ? HAL_I2C_Master_Receive_IT(&SYSCLK_I2C_HANDLE, SYSCLK_ADDR_BYTE, regVal, sizeof(uint8_t)) : retHal;  
        
        if (retHal == HAL_OK)
        {
            retHal = (xSemaphoreTake(gSysClkI2CRxSem, pdMS_TO_TICKS(I2C_WAIT_TIMEOUT)) == pdPASS) ? HAL_OK : HAL_ERROR;
        }
                    
        ret = (retHal == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_SYSCLK_FAILED_TO_READ_REG;

    }
    return POST_COTA_ERROR(ret);
}

/**
 * @brief Writes a list of registers to the system clock.
 * 
 * @param regList   An array of registers to write to.
 * @param numRegs   The number of registers to write to.
 * @param index     The index in the regs field to use.
 *
 * @return COTA_ERROR_NONE on success; COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG on failure.
 */
static cotaError_t sysClkWriteRegList(clockTreeRegister_t* regList, uint32_t numRegs, uint8_t index)
{
    uint32_t ii = 0;
    cotaError_t ret = COTA_ERROR_NONE;
#ifdef SYSCLK_TEST
    uint8_t regRead;
#endif
    
    for (ii = 0; (ii < numRegs) && (ret == COTA_ERROR_NONE); ii++)
    {
         ret = sysClkWriteReg(regList[ii].addr, regList[ii].regs[index]);
#if SYSCLK_TEST    
         // When testing, we read back what we wrote to see if the clock got the value
         if (ret != COTA_ERROR_NONE)
         {
               POST_COTA_ERROR(ret);         
         }
         else if (regList[ii].addr != 0x001C)
         {           
             ret = (ret == COTA_ERROR_NONE) ? sysClkReadReg(regList[ii].addr, &regRead) : ret;
                         
             if ((ret == COTA_ERROR_NONE) && (regRead != regList[ii].regs[index]))
             {
                 ret = COTA_ERROR_SYSCLK_FAILED_TO_WRITE_REG;
                 POST_COTA_ERROR(ret);   
             }
             else if (ret != COTA_ERROR_NONE)
             {
                 POST_COTA_ERROR(ret);         
             }            
         }
#endif
    }  
    
    return ret;
}

/**
 * @brief Initialize the system clock by writing many registers over I2c
 * 
 * @return On success, COTA_ERROR_NONE; otherwise on error code of type #cotaError_t
 */
cotaError_t SystemClockInit(void)
{
    cotaError_t ret = COTA_ERROR_NONE;
    
    DBG1_SET();
    DBG2_SET();
    DBG3_SET();
    DBG4_SET();
    
    ret = sysClkWriteRegList(preambleList, preambleListSize, VARIANT_1_CLOCK);
    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteRegList(registerList, registerListSize, VARIANT_1_CLOCK) : ret;
    ret = (ret == COTA_ERROR_NONE) ? sysClkWriteRegList(postambleList, postambleListSize, VARIANT_1_CLOCK) : ret;
    
#if SYSCLK_TEST     
    ret = (ret == COTA_ERROR_NONE) ? ChangeFreq(2460) : ret;
    ret = (ret == COTA_ERROR_NONE) ? ChangeFreq(2450) : ret;
    ret = (ret == COTA_ERROR_NONE) ? ChangeFreq(2440) : ret;
#endif
       
    return ret;
}

/**
 * @brief Creates the semaphores needed to communicate with the system clock
 */
void InitSysClkDriver(void)
{ 
  gSysClkI2CTxSem = xSemaphoreCreateBinaryStatic(&gSysClkI2CTxSemBuff);
  while (gSysClkI2CTxSem == NULL);
  
  gSysClkI2CRxSem = xSemaphoreCreateBinaryStatic(&gSysClkI2CRxSemBuff);
  while (gSysClkI2CRxSem == NULL);
}



/**
 * @brief Read M divider numerator registers
 * @param mNum[out] 48-bit M divider numerator register value
 * @return On success, COTA_ERROR_NONE; otherwise on error code of type #cotaError_t
 */
static cotaError_t readMNum(uint64_t *mNum)
{
    uint8_t ii;
    cotaError_t ret = COTA_ERROR_NONE;
    uint16_t regAddr[] = {0x235, 0x236, 0x237, 0x238, 0x239, 0x23A};
    uint8_t *data = (uint8_t*)mNum;
    
    *mNum = 0;
    
    for (ii = 0; (ii < sizeof(regAddr)/sizeof(uint16_t)) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = sysClkReadReg(regAddr[ii], &data[ii]);
    }
    
    return ret;
}

/**
 * @brief Read M divider denominator registers
 * @param mDen[out] 32-bit M divider denominator register value
 * @return On success, COTA_ERROR_NONE; otherwise on error code of type #cotaError_t
 */
static cotaError_t readMDen(uint32_t *mDen)
{
    uint8_t ii;
    cotaError_t ret = COTA_ERROR_NONE;
    uint16_t regAddr[] = {0x23B, 0x23C, 0x23D, 0x23E};
    uint8_t *data = (uint8_t*)mDen;
    
    *mDen = 0;
    
    for (ii = 0; (ii < sizeof(regAddr)/sizeof(regAddr[0])) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = sysClkReadReg(regAddr[ii], &data[ii]);
    }
    
    return ret;
}

/**
 * @brief Read R divider registers
 * @param rReg[out] 24-bit R divider register value
 * @return On success, COTA_ERROR_NONE; otherwise on error code of type #cotaError_t
 */
static cotaError_t readRReg(uint32_t *rReg)
{
    int ii;
    cotaError_t ret = COTA_ERROR_NONE;
    uint16_t regAddr[] = {0x24A, 0x24B, 0x24C};
    uint8_t *data = (uint8_t*)rReg;

    *rReg = 0;
    
    for (ii = 0; (ii < sizeof(regAddr)/sizeof(regAddr[0])) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = sysClkReadReg(regAddr[ii], &data[ii]);
    }
    
    return ret;
}

/**
 * @brief Read N divider denominator registers
 * @param nDen[out] 32-bit N divider denominator register value
 * @return On success, COTA_ERROR_NONE; otherwise on error code of type #cotaError_t
 */
static cotaError_t readNDen(uint32_t *nDen)
{
    uint8_t ii;
    cotaError_t ret = COTA_ERROR_NONE;
    uint16_t regAddr[] = {0x308, 0x309, 0x30A, 0x30B};
    uint8_t *data = (uint8_t*)nDen;
    
    *nDen = 0;
    
    for (ii = 0; (ii < sizeof(regAddr)/sizeof(regAddr[0])) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = sysClkReadReg(regAddr[ii], &data[ii]);
    }
    
    return ret;
}

/**
 * @brief Write N divider numerator registers.
 * @param n N divider to write
 * @param nNum N divider numerator value
 * @return On success, COTA_ERROR_NONE; otherwise on error code of type #cotaError_t
 */
static cotaError_t writeNNum(uint8_t n, uint64_t nNum)
{
    int ii;
    cotaError_t ret = COTA_ERROR_NONE;
    uint16_t regAddr[MAX_NUM_NDIV][N_DIV_NUM_REG_SIZE] =   {{0x302, 0x303, 0x304, 0x305, 0x306, 0x307},  //N0
                                                            {0x30D, 0x30E, 0x30F, 0x310, 0x311, 0x312},  //N1
                                                            {0x318, 0x319, 0x31A, 0x31B, 0x31C, 0x31D},  //N2
                                                            {0x323, 0x324, 0x325, 0x326, 0x327, 0x328},  //N3
                                                            {0x32E, 0x32F, 0x330, 0x331, 0x332, 0x333}}; //N4
    uint8_t *data = (uint8_t*)&nNum;
    
    if (n >= MAX_NUM_NDIV)
    {
        ret = COTA_ERROR_BAD_PARAMETER;
    }
    else
    {
        for (ii = 0; (ii < N_DIV_NUM_REG_SIZE) && (ret == COTA_ERROR_NONE); ii++)
        {
            ret = sysClkWriteReg(regAddr[n][ii], data[ii]);
        }
    }
    
    return ret;
}

/**
 * @brief Write N delay register.
 * @param n N delay to write
 * @param nDelay Actual delay value to be written to delay register
 * @return On success, COTA_ERROR_NONE; otherwise on error code of type #cotaError_t
 */
static cotaError_t writeNDelay(uint8_t n, uint16_t nDelay)
{
    uint8_t ii;
    cotaError_t ret = COTA_ERROR_NONE;
    uint16_t regAddr[MAX_NUM_NDIV][DELAY_REG_SIZE] = {{0x359, 0x35A}, {0x35B, 0x35C}, {0x35D, 0x35E}, {0x35F, 0x360}, {0x361, 0x362}};
    uint8_t *data = (uint8_t*)&nDelay;
    
    if (n >= MAX_NUM_NDIV)
    {
        ret = COTA_ERROR_BAD_PARAMETER;
    }
    else
    {
        for (ii = 0; (ii < DELAY_REG_SIZE) && (ret == COTA_ERROR_NONE); ii++)
        {
            ret = sysClkWriteReg(regAddr[n][ii], data[ii]);
        }
    }
    
    return ret;
}

/**
 * @brief Write the Nth divider update register for the new frequency to take effect
 * @param n N divider to write
 * @return On success, COTA_ERROR_NONE; otherwise on error code of type #cotaError_t
 */
static cotaError_t writeNUpdate(uint8_t n)
{
    cotaError_t ret;
    uint16_t regAddr[MAX_NUM_NDIV] = {0x30C, 0x317, 0x322, 0x32D, 0x338};
    uint8_t data = 1;
    
    if (n >= MAX_NUM_NDIV)
    {
        ret = COTA_ERROR_BAD_PARAMETER;
    }
    else
    {
        ret = sysClkWriteReg(regAddr[n], data);
    }
    return ret;
}


/**
 *  @brief Change the clock frequency thru clock register configurations
 *  @param freq The desired frequency in MHZ
 *  @return On success, COTA_ERROR_NONE; otherwise on error code of type #cotaError_t
 */
cotaError_t ChangeFreq(uint32_t freq)
{
    double   refFreq = 0;
    double   freqVco = 0;
    double   nSkew;
    uint64_t nNum;
    uint32_t nDen;
    uint64_t mNum;
    uint32_t mDen;
    uint32_t rReg;
    uint32_t rVal;
    uint16_t nDelay;
    cotaError_t ret;
    
    ret = readMNum(&mNum);
    
    ret = (ret == COTA_ERROR_NONE) ? readMDen(&mDen) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ? readNDen(&nDen) : ret;
    
    ret = (ret == COTA_ERROR_NONE) ? readRReg(&rReg) : ret;
    
    if (ret == COTA_ERROR_NONE)
    {
        refFreq = ((double)freq) / FREQ_RATIO_V1;
            
        //Adjust the reference frequency up to 1 HZ accuracy 
        refFreq = (uint64_t)(refFreq * 1000000) / 1000000.0;
        
        freqVco = (double)(mNum / mDen) * IN_FREQ_MHZ;
        
        nSkew  = (1 / refFreq) / 2;                   //clock skew is 1/2 cycle in us
        nDelay = (uint16_t)(nSkew * 256 * freqVco);  //The formula is coming from the clock tree document
        
        rVal = (rReg + 1) * 2;  //R_VALUE = (R_REG + 1) x 2
        
        //Calculate the new N divider Numerator value
        nNum = (uint64_t)((nDen * freqVco) / (refFreq * rVal));
        
        //We only need to change the N_NUM to move to the frequency
        //N0 and N1 dividers are used in this case
        ret = writeNNum(0, nNum);        
        ret = (ret == COTA_ERROR_NONE) ? writeNNum(1, nNum) : ret;        

        //Update the delay register corresponding to the new frequency
        //Only N0 which connects to AMB0 and AMB3 need delay adjustment      
        ret = (ret == COTA_ERROR_NONE) ? writeNDelay(0, nDelay) : ret;

        //Write the update regiser (N0 and N1) for the change to take effect
        ret = (ret == COTA_ERROR_NONE) ? writeNUpdate(0) : ret;
        ret = (ret == COTA_ERROR_NONE) ? writeNUpdate(1) : ret;
    }
    
    return ret;
}

