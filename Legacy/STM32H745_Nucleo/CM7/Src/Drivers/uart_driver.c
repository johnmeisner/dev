/****************************************************************************//**
* @file      uart_driver.c
*
* @brief     Code implementing prototypes to provide thread safe access to the UART
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#include "FreeRTOS.h"
#include "orion_config.h"
#include "semphr.h"
#include "task.h"
#include "main.h"
#include "error.h"
#include "string.h"
#include "uart_driver.h"
#include "stream_buffer.h"
#include "message_buffer.h"
#include "projdefs.h"
#include <stdio.h>
#include "variable_queue.h"
#include "debug_off.h"

extern UART_HandleTypeDef huart3;                 ///< Pointer to UART structure
static SemaphoreHandle_t gCotaUartMutex;          ///< Mutex protecting the UART calls
static StaticSemaphore_t gCotaUartMutexBuff;      ///< The buffer holding the information for the mutex
static SemaphoreHandle_t gCotaUartRxSem;          ///< Semaphore signaling a complete receive
static StaticSemaphore_t gCotaUartRxSemBuff;      ///< The buffer holding the information for the #gCotaUartRxSem semaphore
static SemaphoreHandle_t gCotaUartWaitForTx;      ///< Semaphore waiting for Uart transmit to complete
static StaticSemaphore_t gCotaUartWaitForTxBuff;  ///< The buffer holding the information for the #gCotaUartWaitForTx semaphore

#ifdef PROXY_TEST_ENABLED
    extern void TxCpltCallback(void);
#endif

/**
 * @brief Transmits data over UART in a thread safe way.
 *        This will block until the uart message is sent.
 *
 * @note  You cannot post to the error handler here because these functions are used by the error handler
 *
 * @param pData    null terminated string of ascii
 * @param size     size of the transmitted data in bytes, if 0, then a strlen will be calculated on pData
 * @param timeout  Timeout to block on transfer in ticks
 * 
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t CotaUartTransmit(uint8_t *pData, uint16_t size, uint32_t timeout)
{
    HAL_StatusTypeDef ret = HAL_ERROR;

    if (size == 0)
        size = strlen(pData);
  
    if (xSemaphoreTake( gCotaUartMutex, timeout ) == pdPASS)
    { 
        ret = HAL_UART_Transmit_IT(&huart3, pData, size);
        if (ret == HAL_OK)
        {
            xSemaphoreTake(gCotaUartWaitForTx, portMAX_DELAY);
        }

        xSemaphoreGive( gCotaUartMutex );
    }
    return (ret == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_UART_TX_FAILED;
}

/**
 * @brief An interrupt handler that indicates data has been transmitted by the UART
 *
 * @param huart Handle to the UART receiving data
 */ 
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(gCotaUartWaitForTx, &xHigherPriorityTaskWoken);
  
#ifdef PROXY_TEST_ENABLED
    TxCpltCallback();
#endif
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @brief An interrupt handler that indicates data has been received by the UART
 *
 * @param huart Handle to the UART receiving data
 */ 
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    xSemaphoreGiveFromISR(gCotaUartRxSem, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
 * @brief An interrupt handler that indicates an error occurs on the UART
 *
 * @param huart Handle to the UART receiving data
 */ 
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    //Errors can stop the ability of UART to process receives.  Let's make sure
    //this doesn't block us.
    xSemaphoreGiveFromISR(gCotaUartRxSem, &xHigherPriorityTaskWoken); 
}

/**
 * @brief Enables the receive of data over UART in a thread safe way.  Then waits for the data to arrive.
 *
 * @note  You cannot post to the error handler here because these functions are used by the error handler
 * @note  Currently, this only being used by CLI.
 *
 * @param pData    Pointer to buffer to receive data.
 * @param size     size of data to receive
 * @param timeout  Timeout to block on transfer in ticks
 * 
 * @return COTA_ERROR_NONE on success; otherwise error code of type #cotaError_t
 */
cotaError_t  CotaUartWaitForRx(uint8_t *pData, uint16_t size, uint32_t timeout)
{
    HAL_StatusTypeDef ret = HAL_ERROR;
    DBG1_SET();
    if (xSemaphoreTake( gCotaUartMutex, timeout ) == pdPASS)
    {
        DBG3_SET();
        ret = HAL_UART_Receive_IT(&huart3, pData, size);
        DBG3_RESET();
        xSemaphoreGive( gCotaUartMutex );
        DBG3_SET();
        if (xSemaphoreTake(gCotaUartRxSem, portMAX_DELAY) != pdPASS)
        {
            ret = HAL_ERROR;
        }
        DBG3_RESET();
    }
    DBG1_RESET();
    return (ret == HAL_OK) ? COTA_ERROR_NONE : COTA_ERROR_HAL;
  
}


#ifdef UART_BUFFER_TEST
void uartBufTaskTest(void *pvParameters);
static void InitUartBufTest(void);
#endif

/**
 * @brief Create the mutex, semahore, message queues, and task needed
 *        to make the uart driver work.    
 */
void InitCotaUart(void)
{
    gCotaUartMutex = xSemaphoreCreateMutexStatic(&gCotaUartMutexBuff);
    while (gCotaUartMutex == NULL);
    xSemaphoreGive( gCotaUartMutex );

    gCotaUartRxSem = xSemaphoreCreateBinaryStatic(&gCotaUartRxSemBuff);
    while (gCotaUartRxSem == NULL);  

    gCotaUartWaitForTx = xSemaphoreCreateBinaryStatic(&gCotaUartWaitForTxBuff);
    while (gCotaUartWaitForTx == NULL);
    
#ifdef UART_BUFFER_TEST    
    InitUartBufTest();
#endif
}



#ifdef UART_BUFFER_TEST
volatile uint32_t blah;
static void InitUartBufTest(void)
{
    BaseType_t xReturned;
    TaskHandle_t xHandle1;

    xReturned = xTaskCreate(uartBufTask, "Uart Test 1", 2048, NULL,  2, &xHandle1 ); 
    while (xReturned != pdPASS);
}

void uartBufTaskTest(void *pvParameters)
{
    char str[] = "Start buffer task\n\r";
    char str2[MAX_UART_MSG_SIZE];
    uint32_t count =0;
    CotaUartTransmit((uint8_t*)str, strlen(str), portMAX_DELAY);
    while (1)
    {
        snprintf(str2, MAX_UART_MSG_SIZE, "Bigggggggggggggggggggggggggggg Message number %d size %d\r\n", count++, VariableQueueSpacesAvailable(&gCotaUartTxQueueControl));
        
        if (PostUartMsgForTransmit((uint8_t*)str2, strlen(str2)) != COTA_ERROR_NONE)
        {
            blah = count;
        }
        
        snprintf(str2, MAX_UART_MSG_SIZE, "sh%d\r\n",VariableQueueSpacesAvailable(&gCotaUartTxQueueControl));
        
        if (PostCliMsgForTransmit((uint8_t*)str2, strlen(str2)) != COTA_ERROR_NONE)
        {
            blah = count;
        }
              
        vTaskDelay( pdMS_TO_TICKS(4));
        
    }   
}


#endif

#ifdef UART_DRIVER_TEST 
void uart1Task(void *pvParameters);
void uart2Task(void *pvParameters);

/**
 * @brief A task sending a string to uart, used for testing
 * @param pvParameters not used
 */
void uart1Task(void *pvParameters)
{
  char str[] = "Task 1 to Task 2: No, You're ugly.\n\r";
  while (1)
  {
    CotaUartTransmit((uint8_t*)str, strlen(str), portMAX_DELAY);
    vTaskDelay( pdMS_TO_TICKS(30));
  } 
}

/**
 * @brief A task sending a string to uart, used for testing
 * @param pvParameters not used
 */
void uart2Task(void *pvParameters)
{
  char str[] = "Task 2 to Task 1: No, you're a bonehead.\n\r";
  while (1)
  {
    CotaUartTransmit((uint8_t*)str, strlen(str), portMAX_DELAY);
    vTaskDelay( pdMS_TO_TICKS(20));
  } 
}
              
/**
 * @brief Start two tasks sending UART to see the the uart_driver is thread safe.
 */
void InitUartTest(void)
{

  TaskHandle_t xHandle1;
  TaskHandle_t xHandle2;  

  xTaskCreate(uart1Task, "Uart Test 1", 2048, NULL,  2, &xHandle1 );  
  xTaskCreate(uart2Task, "Uart Test 2", 2048, NULL,  2, &xHandle2 );  
}

#endif
