/****************************************************************************//**
* @file      uvp_driver.h
*
* @brief     Header file to read and write to uvp's (The asic that control receive and transmission
*
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/
#ifndef _UVP_DRIVER_H_
#define _UVP_DRIVER_H_
#include "error.h"
#include "amb_control.h"

/**
 * The following are register definitions that are passed to UvpRegisterRead and
 * UvpRegisterWrite.
 * Currently we are using SWS1411 UVPv2 Variant 1, but these definition also apply to Variant 3
 */
#define  ENCODE_SIZE                      (struct uvp_reg){ 3,  12,  6,  0,  0, 0, 0,  12 }
#define  DETECT_PH_CLIENT_ID_1            (struct uvp_reg){ 4,  16,  3,  0,  0, 0, 0,  16 }
#define  CLIENT_SELECT_OUT                (struct uvp_reg){ 4,  16,  3,  0,  0, 0, 3,  16 }
#define  PHASE_DETECT_DONE                (struct uvp_reg){ 5,  20,  1,  0,  1, 0, 4,  20 }
#define  SEND_POWER_RDY                   (struct uvp_reg){ 5,  20,  1,  0,  1, 0, 5,  20 }
#define  RSSi_AMU1                        (struct uvp_reg){ 5,  20,  13, 0,  1, 0, 6,  20 }
#define  RSSi_AMU2                        (struct uvp_reg){ 5,  22,  13, 0,  1, 0, 19, 20 }
#define  RSSi_AMU3                        (struct uvp_reg){ 6,  24,  13, 0,  1, 0, 0,  24 }
#define  RSSi_AMU4                        (struct uvp_reg){ 6,  25,  13, 0,  1, 0, 13, 24 }
#define  ADC_SAT                          (struct uvp_reg){ 6,  27,  4,  0,  1, 0, 26, 24 }
#define  SYSCTRL_STATE                    (struct uvp_reg){ 9,  39,  3,  0,  1, 0, 29, 36 }
#define  ARB_PHASE_SEL                    (struct uvp_reg){ 9,  36,  8,  0,  0, 0, 0,  36 }
#define  ARBIT_PHASE_AMU1_CLNT1           (struct uvp_reg){ 10, 40,  9,  0,  0, 0, 0,  40 }
#define  ARBIT_PHASE_AMU1_CLNT2           (struct uvp_reg){ 10, 41,  9,  0,  0, 0, 9,  40 }
#define  ARBIT_PHASE_AMU1_CLNT3           (struct uvp_reg){ 10, 42,  9,  0,  0, 0, 18, 40 }
#define  ARBIT_PHASE_AMU1_CLNT4           (struct uvp_reg){ 11, 44,  9,  0,  0, 0, 0,  44 }
#define  ARBIT_PHASE_AMU1_CLNT5           (struct uvp_reg){ 11, 45,  9,  0,  0, 0, 9,  44 }
#define  ARBIT_PHASE_AMU1_CLNT6           (struct uvp_reg){ 11, 46,  9,  0,  0, 0, 18, 44 }
#define  ARBIT_PHASE_AMU1_CLNT7           (struct uvp_reg){ 12, 48,  9,  0,  0, 0, 0,  48 }
#define  ARBIT_PHASE_AMU1_CLNT8           (struct uvp_reg){ 12, 49,  9,  0,  0, 0, 9,  48 }
#define  ARBIT_PHASE_AMU2_CLNT1           (struct uvp_reg){ 12, 50,  9,  0,  0, 0, 18, 48 }
#define  ARBIT_PHASE_AMU2_CLNT2           (struct uvp_reg){ 13, 52,  9,  0,  0, 0, 0,  52 }
#define  ARBIT_PHASE_AMU2_CLNT3           (struct uvp_reg){ 13, 53,  9,  0,  0, 0, 9,  52 }
#define  ARBIT_PHASE_AMU2_CLNT4           (struct uvp_reg){ 13, 54,  9,  0,  0, 0, 18, 52 }
#define  ARBIT_PHASE_AMU2_CLNT5           (struct uvp_reg){ 14, 56,  9,  0,  0, 0, 0,  56 }
#define  ARBIT_PHASE_AMU2_CLNT6           (struct uvp_reg){ 14, 57,  9,  0,  0, 0, 9,  56 }
#define  ARBIT_PHASE_AMU2_CLNT7           (struct uvp_reg){ 14, 58,  9,  0,  0, 0, 18, 56 }
#define  ARBIT_PHASE_AMU2_CLNT8           (struct uvp_reg){ 15, 60,  9,  0,  0, 0, 0,  60 }
#define  ARBIT_PHASE_AMU3_CLNT1           (struct uvp_reg){ 15, 61,  9,  0,  0, 0, 9,  60 }
#define  ARBIT_PHASE_AMU3_CLNT2           (struct uvp_reg){ 15, 62,  9,  0,  0, 0, 18, 60 }
#define  ARBIT_PHASE_AMU3_CLNT3           (struct uvp_reg){ 16, 64,  9,  0,  0, 0, 0,  64 }
#define  ARBIT_PHASE_AMU3_CLNT4           (struct uvp_reg){ 16, 65,  9,  0,  0, 0, 9,  64 }
#define  ARBIT_PHASE_AMU3_CLNT5           (struct uvp_reg){ 16, 66,  9,  0,  0, 0, 18, 64 }
#define  ARBIT_PHASE_AMU3_CLNT6           (struct uvp_reg){ 17, 68,  9,  0,  0, 0, 0,  68 }
#define  ARBIT_PHASE_AMU3_CLNT7           (struct uvp_reg){ 17, 69,  9,  0,  0, 0, 9,  68 }
#define  ARBIT_PHASE_AMU3_CLNT8           (struct uvp_reg){ 17, 70,  9,  0,  0, 0, 18, 68 }
#define  ARBIT_PHASE_AMU4_CLNT1           (struct uvp_reg){ 18, 72,  9,  0,  0, 0, 0,  72 }
#define  ARBIT_PHASE_AMU4_CLNT2           (struct uvp_reg){ 18, 73,  9,  0,  0, 0, 9,  72 }
#define  ARBIT_PHASE_AMU4_CLNT3           (struct uvp_reg){ 18, 74,  9,  0,  0, 0, 18, 72 }
#define  ARBIT_PHASE_AMU4_CLNT4           (struct uvp_reg){ 19, 76,  9,  0,  0, 0, 0,  76 }
#define  ARBIT_PHASE_AMU4_CLNT5           (struct uvp_reg){ 19, 77,  9,  0,  0, 0, 9,  76 }
#define  ARBIT_PHASE_AMU4_CLNT6           (struct uvp_reg){ 19, 78,  9,  0,  0, 0, 18, 76 }
#define  ARBIT_PHASE_AMU4_CLNT7           (struct uvp_reg){ 20, 80,  9,  0,  0, 0, 0,  80 }
#define  ARBIT_PHASE_AMU4_CLNT8           (struct uvp_reg){ 20, 81,  9,  0,  0, 0, 9,  80 }
#define  TEMPADC_OUT                      (struct uvp_reg){ 20, 82,  13, 0,  1, 0, 18, 80 }
#define  SYSNOTRESETED                    (struct uvp_reg){ 20, 83,  1,  0,  0, 0, 31, 80 }
#define  UVP_RXCTRL                       (struct uvp_reg){ 29, 119, 1,  0,  0, 0, 30, 116}
#define  UVP_TXCTRL                       (struct uvp_reg){ 29, 119, 1,  0,  0, 0, 31, 116}
#define  UVP_PUCTRL                       (struct uvp_reg){ 30, 120, 1,  0,  0, 0, 0,  120}
#define  REGFILE_PAGE                     (struct uvp_reg){ 31, 127, 8,  0,  0, 0, 24, 124}
#define  OUTPHASE_CLNT1_AMU1              (struct uvp_reg){ 32, 128, 10, 0,  1, 1, 0,  0 }
#define  OUTPHASE_CLNT2_AMU1              (struct uvp_reg){ 32, 129, 10, 0,  1, 1, 10, 0 }
#define  OUTPHASE_CLNT3_AMU1              (struct uvp_reg){ 32, 130, 10, 0,  1, 1, 20, 0 }
#define  OUTPHASE_CLNT4_AMU1              (struct uvp_reg){ 33, 132, 10, 0,  1, 1, 0,  4 }
#define  OUTPHASE_CLNT5_AMU1              (struct uvp_reg){ 33, 133, 10, 0,  1, 1, 10, 4 }
#define  OUTPHASE_CLNT6_AMU1              (struct uvp_reg){ 33, 134, 10, 0,  1, 1, 20, 4 }
#define  OUTPHASE_CLNT7_AMU1              (struct uvp_reg){ 34, 136, 10, 0,  1, 1, 0,  8 }
#define  OUTPHASE_CLNT8_AMU1              (struct uvp_reg){ 34, 137, 10, 0,  1, 1, 10, 8 }
#define  OUTPHASE_CLNT1_AMU2              (struct uvp_reg){ 34, 138, 10, 0,  1, 1, 20, 8 }
#define  OUTPHASE_CLNT2_AMU2              (struct uvp_reg){ 35, 140, 10, 0,  1, 1, 0,  12 }
#define  OUTPHASE_CLNT3_AMU2              (struct uvp_reg){ 35, 141, 10, 0,  1, 1, 10, 12 }
#define  OUTPHASE_CLNT4_AMU2              (struct uvp_reg){ 35, 142, 10, 0,  1, 1, 20, 12 }
#define  OUTPHASE_CLNT5_AMU2              (struct uvp_reg){ 36, 144, 10, 0,  1, 1, 0,  16 }
#define  OUTPHASE_CLNT6_AMU2              (struct uvp_reg){ 36, 145, 10, 0,  1, 1, 10, 16 }
#define  OUTPHASE_CLNT7_AMU2              (struct uvp_reg){ 36, 146, 10, 0,  1, 1, 20, 16 }
#define  OUTPHASE_CLNT8_AMU2              (struct uvp_reg){ 37, 148, 10, 0,  1, 1, 0,  20 }
#define  OUTPHASE_CLNT1_AMU3              (struct uvp_reg){ 37, 149, 10, 0,  1, 1, 10, 20 }
#define  OUTPHASE_CLNT2_AMU3              (struct uvp_reg){ 37, 150, 10, 0,  1, 1, 20, 20 }
#define  OUTPHASE_CLNT3_AMU3              (struct uvp_reg){ 38, 152, 10, 0,  1, 1, 0,  24 }
#define  OUTPHASE_CLNT4_AMU3              (struct uvp_reg){ 38, 153, 10, 0,  1, 1, 10, 24 }
#define  OUTPHASE_CLNT5_AMU3              (struct uvp_reg){ 38, 154, 10, 0,  1, 1, 20, 24 }
#define  OUTPHASE_CLNT6_AMU3              (struct uvp_reg){ 39, 156, 10, 0,  1, 1, 0,  28 }
#define  OUTPHASE_CLNT7_AMU3              (struct uvp_reg){ 39, 157, 10, 0,  1, 1, 10, 28 }
#define  OUTPHASE_CLNT8_AMU3              (struct uvp_reg){ 39, 158, 10, 0,  1, 1, 20, 28 }
#define  OUTPHASE_CLNT1_AMU4              (struct uvp_reg){ 40, 160, 10, 0,  1, 1, 0,  32 }
#define  OUTPHASE_CLNT2_AMU4              (struct uvp_reg){ 40, 161, 10, 0,  1, 1, 10, 32 }
#define  OUTPHASE_CLNT3_AMU4              (struct uvp_reg){ 40, 162, 10, 0,  1, 1, 20, 32 }
#define  OUTPHASE_CLNT4_AMU4              (struct uvp_reg){ 41, 164, 10, 0,  1, 1, 0,  36 }
#define  OUTPHASE_CLNT5_AMU4              (struct uvp_reg){ 41, 165, 10, 0,  1, 1, 10, 36 }
#define  OUTPHASE_CLNT6_AMU4              (struct uvp_reg){ 41, 166, 10, 0,  1, 1, 20, 36 }
#define  OUTPHASE_CLNT7_AMU4              (struct uvp_reg){ 42, 168, 10, 0,  1, 1, 0,  40 }
#define  OUTPHASE_CLNT8_AMU4              (struct uvp_reg){ 42, 169, 10, 0,  1, 1, 10, 40 }
#define  PHDETECT_PIN_SEL                 (struct uvp_reg){ 46, 187, 2,  0,  0, 1, 26, 56 }
#define  SYS_RST                          (struct uvp_reg){ 46, 187, 1,  0,  0, 1, 28, 56 }
#define  LF_LOCKDET                       (struct uvp_reg){ 46, 187, 1,  0,  1, 1, 29, 56 }
#define  LF_LOCKHI                        (struct uvp_reg){ 46, 187, 1,  0,  1, 1, 30, 56 }
#define  RSSI_GAIN_SCALE                  (struct uvp_reg){ 50, 200, 11, 0,  0, 1, 0,  72 }
#define  PA_POWERON_DEL                   (struct uvp_reg){ 50, 201, 10, 0,  0, 1, 11, 72 }
#define  PA_OFFSET_DEL                    (struct uvp_reg){ 50, 202, 5,  0,  0, 1, 21, 72 }
#define  PA_SEQUENCE_DEL                  (struct uvp_reg){ 50, 203, 5,  0,  0, 1, 26, 72 }
#define  PA_SEQUENCE                      (struct uvp_reg){ 50, 203, 1,  0,  0, 1, 31, 72 }
#define  PA_COUNTER_VALID                 (struct uvp_reg){ 51, 204, 1,  0,  0, 1, 0,  76 }
#define  TEMPADCEN                        (struct uvp_reg){ 51, 204, 1,  0,  0, 1, 1,  76 }
#define  LOWIF0P25FSEN                    (struct uvp_reg){ 51, 204, 1,  1,  0, 1, 2,  76 }
#define  LOWIF_EN                         (struct uvp_reg){ 51, 204, 1,  1,  0, 1, 3,  76 }
#define  LOWIF0P25FSPH_I                  (struct uvp_reg){ 51, 204, 7,  1,  0, 1, 4,  76 }
#define  LOWIF0P25FSPW_I                  (struct uvp_reg){ 51, 205, 7,  65, 0, 1, 11, 76 }
#define  LOWIF0P25FSPH_Q                  (struct uvp_reg){ 51, 206, 7,  33, 0, 1, 18, 76 }
#define  LOWIF0P25FSPW_Q                  (struct uvp_reg){ 51, 207, 7,  97, 0, 1, 25, 76 }
#define  LOWIF_I_SYMBLI                   (struct uvp_reg){ 52, 208, 8,  0,  0, 1, 0,  80 }
#define  LOWIF_I_SYMBLQ                   (struct uvp_reg){ 52, 209, 8,  0,  0, 1, 8,  80 }
#define  LOWIF_Q_SYMBLI                   (struct uvp_reg){ 52, 210, 8,  0,  0, 1, 16, 80 }
#define  LOWIF_Q_SYMBLQ                   (struct uvp_reg){ 52, 211, 8,  0,  0, 1, 24, 80 }
#define  SEL_PHASE_CONJUGATE              (struct uvp_reg){ 53, 212, 1,  0,  0, 1, 0,  84 }
#define  ADCINTF_I_OFFST_AMU1             (struct uvp_reg){ 53, 212, 13, 0,  0, 1, 1,  84 }
#define  PHASE_OFFSET_AMU1                (struct uvp_reg){ 53, 213, 9,  0,  0, 1, 14, 84 }
#define  PHASE_OFFSET_AMU2                (struct uvp_reg){ 53, 214, 9,  0,  0, 1, 23, 84 }
#define  PHASE_OFFSET_AMU3                (struct uvp_reg){ 54, 216, 9,  0,  0, 1, 0,  88 }
#define  PHASE_OFFSET_AMU4                (struct uvp_reg){ 54, 217, 9,  0,  0, 1, 9,  88 }
#define  ADCINTF_Q_OFFST_AMU1             (struct uvp_reg){ 54, 218, 13, 0,  0, 1, 18, 88 }
#define  CORRELATOR_EN                    (struct uvp_reg){ 54, 219, 1,  1,  0, 1, 31, 88 }
#define  AMU_PHASE_CALIBRATE              (struct uvp_reg){ 55, 220, 4,  0,  0, 1, 1,  92 }
#define  ADCINTF_I_OFFST_AMU2             (struct uvp_reg){ 55, 220, 13, 0,  0, 1, 5,  92 }
#define  ADCINTF_I_GAIN_AMU1              (struct uvp_reg){ 55, 222, 7,  1,  0, 1, 18, 92 }
#define  ADCINTF_Q_GAIN_AMU1              (struct uvp_reg){ 55, 223, 7,  1,  0, 1, 25, 92 }
#define  ADCINTF_I_GAIN_AMU2              (struct uvp_reg){ 56, 224, 7,  1,  0, 1, 0,  96 }
#define  ADCINTF_Q_GAIN_AMU2              (struct uvp_reg){ 56, 224, 7,  1,  0, 1, 7,  96 }
#define  ADCINTF_Q_OFFST_AMU2             (struct uvp_reg){ 56, 225, 13, 0,  0, 1, 14, 96 }
#define  ADCINTF_I_GAIN_AMU3              (struct uvp_reg){ 57, 228, 7,  1,  0, 1, 5,  100 }
#define  ADCINTF_Q_GAIN_AMU3              (struct uvp_reg){ 57, 229, 7,  1,  0, 1, 12, 100 }
#define  ADCINTF_I_OFFST_AMU3             (struct uvp_reg){ 57, 230, 13, 0,  0, 1, 19, 100 }
#define  ADCINTF_I_GAIN_AMU4              (struct uvp_reg){ 58, 232, 7,  1,  0, 1, 0,  104 }
#define  ADCINTF_Q_GAIN_AMU4              (struct uvp_reg){ 58, 232, 7,  1,  0, 1, 7,  104 }
#define  ADCINTF_Q_OFFST_AMU3             (struct uvp_reg){ 58, 233, 13, 0,  0, 1, 14, 104 }
#define  EN_AMU1                          (struct uvp_reg){ 58, 235, 1,  1,  0, 1, 31, 104 }
#define  EN_AMU2                          (struct uvp_reg){ 59, 236, 1,  1,  0, 1, 0,  108 }
#define  EN_AMU3                          (struct uvp_reg){ 59, 236, 1,  1,  0, 1, 1,  108 }
#define  EN_AMU4                          (struct uvp_reg){ 59, 236, 1,  1,  0, 1, 2,  108 }
#define  ADCINTF_I_OFFST_AMU4             (struct uvp_reg){ 59, 238, 13, 0,  0, 1, 17, 108 }
#define  ADCINTF_Q_OFFST_AMU4             (struct uvp_reg){ 60, 240, 13, 0,  0, 1, 0,  112 }
#define  PAD_SPI_IN_PD                    (struct uvp_reg){ 61, 244, 1,  0,  0, 1, 0,  116 }
#define  PAD_SPI_IN_CS                    (struct uvp_reg){ 61, 244, 1,  0,  0, 1, 1,  116 }
#define  PAD_SPI_OUT_PU                   (struct uvp_reg){ 61, 244, 1,  0,  0, 1, 2,  116 }
#define  PAD_SPI_OUT_PD                   (struct uvp_reg){ 61, 244, 1,  0,  0, 1, 3,  116 }
#define  PAD_SPI_OUT_CS                   (struct uvp_reg){ 61, 244, 1,  0,  0, 1, 4,  116 }
#define  PAD_CSB_CS                       (struct uvp_reg){ 61, 244, 1,  0,  0, 1, 5,  116 }
#define  PAD_CSB_PU                       (struct uvp_reg){ 61, 244, 1,  0,  0, 1, 6,  116 }
#define  PAD_CSB_PD                       (struct uvp_reg){ 61, 244, 1,  0,  0, 1, 7,  116 }
#define  PAD_PADOUT_SL                    (struct uvp_reg){ 61, 245, 1,  0,  0, 1, 8,  116 }
#define  PAD_PADOUT_PDRV0                 (struct uvp_reg){ 61, 245, 1,  0,  0, 1, 9,  116 }
#define  PAD_PADOUT_PDRV1                 (struct uvp_reg){ 61, 245, 1,  0,  0, 1, 10, 116 }
#define  VCOBUFFEN_IDLE                   (struct uvp_reg){ 61, 245, 1,  1,  0, 1, 11, 116 }
#define  VCOBUFFEN_RXRDY                  (struct uvp_reg){ 61, 245, 1,  1,  0, 1, 12, 116 }
#define  VCOBUFFEN_PHDETECT               (struct uvp_reg){ 61, 245, 1,  1,  0, 1, 13, 116 }
#define  VCOBUFFEN_TXRDY                  (struct uvp_reg){ 61, 245, 1,  1,  0, 1, 14, 116 }
#define  VCOBUFFEN_SENDPWR                (struct uvp_reg){ 61, 245, 1,  1,  0, 1, 15, 116 }
#define  NDIVSEL                          (struct uvp_reg){ 61, 246, 1,  0,  0, 1, 16, 116 }
#define  TMPSNSCMBUFFEN                   (struct uvp_reg){ 61, 246, 1,  0,  0, 1, 17, 116 }
#define  TMPSNSEN                         (struct uvp_reg){ 61, 246, 1,  0,  0, 1, 18, 116 }
#define  RESERVED09                       (struct uvp_reg){ 61, 246, 1,  1,  0, 1, 19, 116 }
#define  TMPSNSCOMPENCNT                  (struct uvp_reg){ 61, 246, 5,  4,  0, 1, 20, 116 }
#define  TMPSNSCOMPSELEN                  (struct uvp_reg){ 61, 247, 1,  1,  0, 1, 25, 116 }
#define  PHASEREFBUFFSIIBSTBUFF1          (struct uvp_reg){ 61, 247, 2,  3,  0, 1, 26, 116 }
#define  PHASEREFBUFFSIIBSTBUFF2          (struct uvp_reg){ 61, 247, 2,  3,  0, 1, 28, 116 }
#define  SSBBUFFSIIBSTBUFF1               (struct uvp_reg){ 61, 247, 2,  3,  0, 1, 30, 116 }
#define  AMU123PDRBST                     (struct uvp_reg){ 62, 249, 2,  1,  0, 1, 14, 120 }
#define  AMU123PDIBST                     (struct uvp_reg){ 62, 250, 3,  3,  0, 1, 16, 120 }
#define  AMU4PDRBST                       (struct uvp_reg){ 62, 250, 2,  1,  0, 1, 19, 120 }
#define  AMU4PDIBST                       (struct uvp_reg){ 62, 250, 3,  3,  0, 1, 21, 120 }
#define  AMU3PAPOWERMODE                  (struct uvp_reg){ 62, 251, 2,  2,  0, 1, 28, 120 }
#define  SSBMODCTRL                       (struct uvp_reg){ 63, 252, 2,  3,  0, 1, 2,  124 }
#define  AMU123AAFGAIN                    (struct uvp_reg){ 63, 252, 2,  1,  0, 1, 6,  124 }
#define  AMU4AAFGAIN                      (struct uvp_reg){ 63, 253, 2,  1,  0, 1, 12, 124 }
#define  AMU4AAFBW                        (struct uvp_reg){ 63, 253, 2,  2,  0, 1, 14, 124 }
#define  AMU123SADCPSEN                   (struct uvp_reg){ 63, 254, 1,  1,  0, 1, 18, 124 }
#define  AMU123SADCCOMPENCNT              (struct uvp_reg){ 63, 254, 5,  4,  0, 1, 19, 124 }
#define  AMU123SADCCOMPSELEN              (struct uvp_reg){ 64, 256, 1,  1,  0, 2, 0,  0 }
#define  AMU4SADCPSEN                     (struct uvp_reg){ 64, 256, 1,  1,  0, 2, 1,  0 }
#define  AMU4SADCCOMPENCNT                (struct uvp_reg){ 64, 256, 5,  4,  0, 2, 2,  0 }
#define  AMU4SADCCOMPSELEN                (struct uvp_reg){ 64, 256, 1,  1,  0, 2, 7,  0 }
#define  AMU123RFAMPLOADCTRL              (struct uvp_reg){ 64, 257, 4,  3,  0, 2, 8,  0 }
#define  AMU123RFAMPRESCTRL               (struct uvp_reg){ 64, 257, 1,  0,  0, 2, 12, 0 }
#define  AMU123RFAMPGMRESCTRL             (struct uvp_reg){ 64, 257, 2,  1,  0, 2, 13, 0 }
#define  AMU4PAPOWERMODE                  (struct uvp_reg){ 64, 259, 2,  2,  0, 2, 28, 0 }
#define  CPLKEN                           (struct uvp_reg){ 64, 259, 1,  0,  0, 2, 31, 0 }
#define  PDIVCTRL                         (struct uvp_reg){ 65, 260, 8,  15, 0, 2, 0,  4 }
#define  NDIVINT                          (struct uvp_reg){ 65, 261, 8,  50, 0, 2, 8,  4 }
#define  NDIVFRAC                         (struct uvp_reg){ 65, 262, 16, 0,  0, 2, 16, 4 }
#define  CPIVAL                           (struct uvp_reg){ 66, 264, 3,  2,  0, 2, 0,  8 }
#define  CPLK                             (struct uvp_reg){ 66, 264, 3,  0,  0, 2, 3,  8 }
#define  VCOCFS                           (struct uvp_reg){ 66, 264, 6,  0,  0, 2, 6,  8 }
#define  VCOUCFS                          (struct uvp_reg){ 66, 265, 2,  1,  0, 2, 12, 8 }
#define  VCOIBST                          (struct uvp_reg){ 66, 265, 2,  2,  0, 2, 14, 8 }
#define  VCOBUFFIBST                      (struct uvp_reg){ 66, 266, 2,  0,  0, 2, 16, 8 }
#define  LFR1CTRL                         (struct uvp_reg){ 66, 266, 3,  7,  0, 2, 18, 8 }
#define  LFR3CTRL                         (struct uvp_reg){ 66, 266, 3,  7,  0, 2, 21, 8 }
#define  LFC1CTRL                         (struct uvp_reg){ 66, 267, 3,  4,  0, 2, 24, 8 }
#define  LFC2CTRL                         (struct uvp_reg){ 66, 267, 3,  6,  0, 2, 27, 8 }
#define  LFC3CTRL                         (struct uvp_reg){ 66, 267, 2,  2,  0, 2, 30, 8 }
#define  PLLLCKRNGCTRL                    (struct uvp_reg){ 67, 268, 2,  1,  0, 2, 3,  12 }
#define  LDOPLLANATRM                     (struct uvp_reg){ 68, 272, 3,  4,  0, 2, 0,  16 }
#define  LDOPLLDIGTRM                     (struct uvp_reg){ 68, 272, 3,  3,  0, 2, 3,  16 }
#define  LDOPLLSDTRM                      (struct uvp_reg){ 68, 272, 3,  3,  0, 2, 6,  16 }
#define  LDODIGTRM                        (struct uvp_reg){ 68, 273, 3,  1,  0, 2, 9,  16 }
#define  LDOBIASTRM                       (struct uvp_reg){ 68, 273, 3,  1,  0, 2, 12, 16 }
#define  LDODIGPADSTRM                    (struct uvp_reg){ 68, 273, 3,  1,  0, 2, 15, 16 }
#define  LDOBIASEN_IDLE                   (struct uvp_reg){ 68, 274, 1,  1,  0, 2, 18, 16 }
#define  RESERVED12                       (struct uvp_reg){ 68, 274, 1   0,  1, 2, 19, 16 }
#define  LDOPLLANAEN_IDLE                 (struct uvp_reg){ 68, 274, 1,  1,  0, 2, 20, 16 }
#define  LDOPLLDIGEN_IDLE                 (struct uvp_reg){ 68, 274, 1,  1,  0, 2, 21, 16 }
#define  LDOPLLSDEN_IDLE                  (struct uvp_reg){ 68, 274, 1,  1,  0, 2, 22, 16 }
#define  LDOADC1EN_IDLE                   (struct uvp_reg){ 68, 274, 1,  0,  0, 2, 23, 16 }
#define  LDOADC2EN_IDLE                   (struct uvp_reg){ 68, 275, 1,  0,  0, 2, 24, 16 }
#define  LDOADC3EN_IDLE                   (struct uvp_reg){ 68, 275, 1,  0,  0, 2, 25, 16 }
#define  REFGENEN_IDLE                    (struct uvp_reg){ 68, 275, 1,  1,  0, 2, 26, 16 }
#define  LDOADC4EN_IDLE                   (struct uvp_reg){ 68, 275, 1,  0,  0, 2, 27, 16 }
#define  CPEN_IDLE                        (struct uvp_reg){ 68, 275, 1,  1,  0, 2, 28, 16 }
#define  NDIVEN_IDLE                      (struct uvp_reg){ 68, 275, 1,  1,  0, 2, 29, 16 }
#define  DIV4EN_IDLE                      (struct uvp_reg){ 68, 275, 1,  1,  0, 2, 30, 16 }
#define  CML2CMOSEN_IDLE                  (struct uvp_reg){ 68, 275, 1,  1,  0, 2, 31, 16 }
#define  LDOPLLANAEN_SENDPWR              (struct uvp_reg){ 74, 296, 1,  1,  0, 2, 0,  40 }
#define  LDOPLLDIGEN_SENDPWR              (struct uvp_reg){ 74, 296, 1,  1,  0, 2, 1,  40 }
#define  LDOPLLSDEN_SENDPWR               (struct uvp_reg){ 74, 296, 1,  1,  0, 2, 2,  40 }
#define  LDOADC1EN_SENDPWR                (struct uvp_reg){ 74, 296, 1,  0,  0, 2, 3,  40 }
#define  LDOADC2EN_SENDPWR                (struct uvp_reg){ 74, 296, 1,  0,  0, 2, 4,  40 }
#define  LDOADC3EN_SENDPWR                (struct uvp_reg){ 74, 296, 1,  0,  0, 2, 5,  40 }
#define  REFGENEN_SENDPWR                 (struct uvp_reg){ 74, 296, 1,  1,  0, 2, 6,  40 }
#define  LDOADC4EN_SENDPWR                (struct uvp_reg){ 74, 296, 1,  0,  0, 2, 7,  40 }
#define  CPEN_SENDPWR                     (struct uvp_reg){ 74, 297, 1,  1,  0, 2, 8,  40 }
#define  NDIVEN_SENDPWR                   (struct uvp_reg){ 74, 297, 1,  1,  0, 2, 9,  40 }
#define  DIV4EN_SENDPWR                   (struct uvp_reg){ 74, 297, 1,  1,  0, 2, 10, 40 }
#define  CML2CMOSEN_SENDPWR               (struct uvp_reg){ 74, 297, 1,  1,  0, 2, 11, 40 }
#define  VCOEN_SENDPWR                    (struct uvp_reg){ 74, 297, 1,  1,  0, 2, 12, 40 }
#define  REFOSCEN_SENDPWR                 (struct uvp_reg){ 74, 297, 1,  0,  0, 2, 13, 40 }
#define  SDEN_SENDPWR                     (struct uvp_reg){ 74, 297, 1,  0,  0, 2, 14, 40 }
#define  PDIVEN_SENDPWR                   (struct uvp_reg){ 74, 297, 1,  1,  0, 2, 15, 40 }
#define  AMU123RFAMPEN_SENDPWR            (struct uvp_reg){ 74, 298, 1,  0,  0, 2, 16, 40 }
#define  AMU4RFAMPEN_SENDPWR              (struct uvp_reg){ 74, 298, 1,  1,  0, 2, 17, 40 }
#define  AMU123RFAMPRFSWITCHCTRL_SENDPWR  (struct uvp_reg){ 74, 298, 1,  1,  0, 2, 18, 40 }
#define  AMU4RFAMPRFSWITCHCTRL_SENDPWR    (struct uvp_reg){ 74, 298, 1,  1,  0, 2, 19, 40 }
#define  AMU123PDEN_SENDPWR               (struct uvp_reg){ 74, 298, 1,  1,  0, 2, 20, 40 }
#define  AMU4PDEN_SENDPWR                 (struct uvp_reg){ 74, 298, 1,  1,  0, 2, 21, 40 }
#define  AMU123AAFEN_SENDPWR              (struct uvp_reg){ 74, 298, 1,  1,  0, 2, 22, 40 }
#define  AMU4AAFEN_SENDPWR                (struct uvp_reg){ 74, 298, 1,  1,  0, 2, 23, 40 }
#define  AMU123SADCEN_SENDPWR             (struct uvp_reg){ 74, 299, 1,  1,  0, 2, 24, 40 }
#define  AMU123SADCCMBUFFEN_SENDPWR       (struct uvp_reg){ 74, 299, 1,  1,  0, 2, 25, 40 }
#define  AMU4SADCEN_SENDPWR               (struct uvp_reg){ 74, 299, 1,  1,  0, 2, 26, 40 }
#define  AMU4SADCCMBUFFEN_SENDPWR         (struct uvp_reg){ 74, 299, 1,  1,  0, 2, 27, 40 }
#define  AMU123PIEN_SENDPWR               (struct uvp_reg){ 74, 299, 1,  1,  0, 2, 28, 40 }
#define  AMU4PIEN_SENDPWR                 (struct uvp_reg){ 74, 299, 1,  1,  0, 2, 29, 40 }
#define  AMU123GAINSTGEN_SENDPWR          (struct uvp_reg){ 74, 299, 1,  1,  0, 2, 30, 40 }
#define  RESERVED29                       (struct uvp_reg){ 74, 299, 1,  1,  0, 2, 31, 40 }
#define  AMU4GAINSTGEN_SENDPWR            (struct uvp_reg){ 75, 300, 1,  0,  0, 2, 0,  44 }
#define  RESERVED30                       (struct uvp_reg){ 75, 300, 1,  1,  0, 2, 1,  44 }
#define  SSBMIXEN_SENDPWR                 (struct uvp_reg){ 75, 300, 1,  0,  0, 2, 2,  44 }
#define  RESERVED31                       (struct uvp_reg){ 75, 300, 1,  0,  0, 2, 3,  44 }
#define  SSBMIXSTARTUP_SENDPWR            (struct uvp_reg){ 75, 300, 1,  0,  0, 2, 4,  44 }
#define  PHASEREFBUFFSEN_SENDPWR          (struct uvp_reg){ 75, 300, 1,  1,  0, 2, 5,  44 }
#define  PHASEREFBUFFSSTRT_SDPWR          (struct uvp_reg){ 75, 300, 1,  0,  0, 2, 6,  44 }
#define  SSBBUFFSEN_SENDPWR               (struct uvp_reg){ 75, 300, 1,  0,  0, 2, 7,  44 }
#define  SSBBUFFSSTARTUP_SENDPWR          (struct uvp_reg){ 75, 301, 1,  0,  0, 2, 8,  44 }
#define  TEMPADC_OFFST                    (struct uvp_reg){ 75, 301, 13, 0,  0, 2, 9,  44 }
#define  TEMPADC_GAIN                     (struct uvp_reg){ 75, 302, 7,  1,  0, 2, 22, 44 }
#define  RESERVED32                       (struct uvp_reg){ 75, 303, 1,  0,  0, 2, 29, 44 }
#define  RESERVED33                       (struct uvp_reg){ 75, 275, 2,  0,  1, 2, 30, 44 }
#define  RSSI_LOWTHRESH                   (struct uvp_reg){ 76, 304, 12, 0,  0, 2, 0,  48 }
#define  RSSI_HIGHTHRESH                  (struct uvp_reg){ 76, 305, 12, 0,  0, 2, 12, 48 }
#define  REFOSCAMPMODE                    (struct uvp_reg){ 78, 312, 1,  1,  0, 2, 22, 56 }
#define  REFOSCAMPGAIN                    (struct uvp_reg){ 78, 312, 2,  1,  0, 2, 23, 56 }
#define  AMU1PAPOWERMODE                  (struct uvp_reg){ 78, 312, 2,  2,  0, 2, 27, 56 }
#define  AMU2PAPOWERMODE                  (struct uvp_reg){ 78, 312, 2,  2,  0, 2, 29, 56 }
#define  AMU1PAHIGHPOWER                  (struct uvp_reg){ 78, 312, 1,  0,  0, 2, 8,  56 }
#define  AMU2PAHIGHPOWER                  (struct uvp_reg){ 78, 312, 1,  0,  0, 2, 9,  56 }
#define  AMU3PAHIGHPOWER                  (struct uvp_reg){ 78, 312, 1,  0,  0, 2, 10, 56 }
#define  AMU4PAHIGHPOWER                  (struct uvp_reg){ 78, 312, 1,  0,  0, 2, 11, 56 }
#define  POWER_ON_PA                      (struct uvp_reg){ 80, 320, 1,  0,  0, 2, 0,  64 }
#define  ADCSAT_CHECK_DISABLE             (struct uvp_reg){ 80, 320, 1,  0,  0, 2, 2,  64 }
#define  QOB_CHECK_DISABLE                (struct uvp_reg){ 80, 320, 1,  0,  0, 2, 3,  64 }
#define  AMU123RFAMPRFSWITCHCTRL_TEST     (struct uvp_reg){ 80, 322, 1,  0,  0, 2, 23, 64 }
#define  AMU4RFAMPRFSWITCHCTRL_TEST       (struct uvp_reg){ 80, 323, 1,  0,  0, 2, 25, 64 }
#define  AMU123PAEN_TEST                  (struct uvp_reg){ 80, 323, 1,  0,  0, 2, 26, 64 }
#define  AMU4PAEN_TEST                    (struct uvp_reg){ 80, 323, 1,  0,  0, 2, 28, 64 }
#define  AMU123PIEN_TEST                  (struct uvp_reg){ 80, 323, 1,  0,  0, 2, 30, 64 }
#define  AMU4PIEN_TEST                    (struct uvp_reg){ 80, 323, 1,  0,  0, 2, 31, 64 }
#define  AMU123SADCCMBUFFEN_TEST          (struct uvp_reg){ 81, 324, 1,  0,  0, 2, 2,  68 }
#define  AMU123SADCEN_TEST                (struct uvp_reg){ 81, 324, 1,  0,  0, 2, 3,  68 }
#define  AMU4SADCCMBUFFEN_TEST            (struct uvp_reg){ 81, 324, 1,  0,  0, 2, 4,  68 }
#define  AMU4SADCEN_TEST                  (struct uvp_reg){ 81, 324, 1,  0,  0, 2, 5,  68 }
#define  PLLLFMODE                        (struct uvp_reg){ 81, 327, 1,  0,  0, 2, 27, 68 }
#define  PLLLFDACOUT                      (struct uvp_reg){ 81, 327, 4,  7,  0, 2, 28, 68 }
#define  PLLLCKDETEN                      (struct uvp_reg){ 82, 328, 1,  1,  0, 2, 0,  72 }
#define  PLLDACEN                         (struct uvp_reg){ 82, 328, 1,  0,  0, 2, 1,  72 }

#define MAX_NUM_AMU             4                                           ///< The number of AMUs available on a UVP.
#define ALL_AMU_MASK            ((1UL << (MAX_NUM_AMU)) - 1)                ///< The mask for selecting all AMU's
#define UVP_INDEX(amb, uvp)     ((((uint32_t)amb) << 4) | ((uint32_t)uvp))  ///< Maps a amb, uvp number combo to a single index

/**
 * @brief Contains all the information necessary to update a uvp register.
 */
typedef struct uvp_reg
{
    uint16_t rowAddr;       ///< The row address is the result of floor(byteAddr/32)
    uint16_t byteAddr;      ///< The location of the register
    int size;               ///< Size of the value in bits
    int defaultValue;       ///< The default value of the register
    int isReadonly;         ///< Indicates if the register is read only
    int pageIndex;          ///< The page is the rowAddr divided by 32
    int offset;             ///< Offset from bit0 in bits
    uint16_t colAddr;       ///< The column address is the remainder of rowAddr divided by 32, then multipied by 4 (because each address points to 32 bits).
} uvp_reg_t;


#ifdef UVP_TEST_ENABLED
void TestUvp(void);
#endif

void InitUvpDriver(void);
cotaError_t UvpRegisterRead(uvp_reg_t reg, uint32_t* pData, ambNum_t ambNum, uvpNum_t uvpNum);
cotaError_t UvpRegisterWrite(uvp_reg_t reg, uint32_t data, ambNum_t ambNum, uvpNum_t uvpNum);
cotaError_t ReadArrayUvps(uvp_reg_t pReg, uint32_t* pData, uint16_t numValues, ambMask_t ambMask, uvpMask_t uvpMask);
cotaError_t WriteManyUvps(uvp_reg_t pReg, uint32_t data, ambMask_t ambMask, uvpMask_t uvpMask);
cotaError_t UvpInit(ambMask_t ambMask, uvpMask_t uvpMask);
cotaError_t UvpAmuEnable(ambNum_t ambNum, uvpNum_t uvpNum, amuMask_t amuMask);
cotaError_t UvpManyAmuEnable(ambMask_t ambMask, uvpMask_t uvpMask, amuMask_t amuMask);
cotaError_t WriteArrayUvps(uvp_reg_t pReg, uint32_t* pData, uint32_t numValues, ambMask_t ambMask, uvpMask_t uvpMask);
cotaError_t UvpGetTemperatureArray(int16_t* pTemp, uint16_t numValues, ambMask_t ambMask, uvpMask_t uvpMask);
cotaError_t UpdateUvpTemperatureCache(int16_t* maxDelta);
cotaError_t GetAverageUvpTempsFromCache(int16_t* uvpAvgTemps, uint8_t numAvgTemps);
cotaError_t ReadArrayUvps32(uint32_t* pData, uint16_t numValues, uint8_t pageIndex, uint16_t colAddr, ambMask_t ambMask, uvpMask_t uvpMask);
cotaError_t ReadArrayUvps64(uint64_t* pData, uint16_t numValues, uint8_t pageIndex, uint16_t colAddr, ambMask_t ambMask, uvpMask_t uvpMask);
#endif  //#ifndef _UVP_DRIVER_H_

