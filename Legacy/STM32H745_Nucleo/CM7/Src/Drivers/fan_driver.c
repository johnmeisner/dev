/****************************************************************************//**
* @file      fan_driver.c
*
* @brief     The fan driver is meant to expose access to the MAX31790 which
*            controls four fans, one for each AMB.
*
*            The fan driver has a task that does most of the work of monitoring
*            the fans and adjusting the speed for higher temperature AMB's
*            but the control task must periodically call the functoin 
*            ambMask_t FansOk(void).  If the returned mask
*            has a zero bit for a valid AMB, it must stop charging that AMB.
*
*            @todo Need to map fan numbers to amb numbers
*            @todo Need to map average uvp temperatures to fan speeds
* 
* @copyright THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC. 
*            ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS 
*            STRICTLY PROHIBITED. COPYRIGHT 2019 OSSIA INC.
*            (SUBJECT TO LIMITED DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) 
*            ALL RIGHTS RESERVED.
********************************************************************************/


#include "FreeRTOS.h"
#include "semphr.h"
#include "main.h"
#include "semphr.h"
#include "orion_config.h"
#include "i2c_multiplex.h"
#include "ccb_temperature.h"
#include "amb_control.h"
#include "fan_driver.h"
#include "uvp_driver.h"
#include "task.h"
#include "debug_off.h"


#define FAN_I2C_ADDRESS         0x40                ///< The 7 bit fan i2c slave address.
#define FAN_MULTIPLEX_NUM       0                   ///< The number of i2c multiplexer that the fan chip is connected to
#define FAN_MULTIPLEX_CHAN      0                   ///< The channel on the multiplexer that the fan chip is connected to.
#define FAN_READ_WRITE_TIMEOUT  10                  ///< The maximum allowed time to wait for a FAN read or write i2c transaction.
#define FAN_REG_SET_BIT         1                   ///< For a bit value, set to 1
#define FAN_REG_RESET_BIT       0                   ///< For a bit value, set to 0
#define FAN_MAX_NUM             4                   ///< The number of fans
#define FAN_MAX_PWM             511                 ///< The maximum target duty cycle.
#define FAN_LOW_TARGET_PWM      (FAN_MAX_PWM/5+1)   ///< The lowest allowed setting is a 20% duty cycle.
#define FAN_MAX_RPM             2200                ///< The maximum RPM of the fan
#define FAN_SECS_IN_MIN         60                  ///< Seconds in a minute
#define FAN_TACH_CLOCK_HZ       8192                ///< The clock frequency used to create a tachometer count between tachometer ticks.
#define FAN_MIN_RPM_PERCENT     20                  ///< Tne minimum operational RPM percentage for the FAN.
#define FAN_FULL_SPEED_ON       GPIO_PIN_SET        ///< State of the gpio to enable full fans on
#define FAN_FULL_SPEED_OFF      GPIO_PIN_RESET      ///< State of the gpio to disnable full fans on
#define FAN_TASK_WAIT_INTERVAL  25000               ///< The wait interval for the fan task in milliseconds.  Since the watchdog is set at 30 seconds, let's poll the fan driver every 25 seconds to give us an extra 5 seconds of buffer
#define FAN_CRITICAL_TEMP_SPEED 0xff                ///< An imaginary speed that maps to the critical temperature.

/** 
 * The maximum allowed tachometer count before a failure is generated.
 * It's calculated the following way using the 20% of the the maximum 2200 rpm for the fan
 * (fan rots per second) = (20/100)*(2200rpm)/60 = 7.33 rots/sec
 * (tachs per second) = (fan rots per second) * 2 = 14.666 tachs/sec
 * (max count) = 8192 Hz * (2 tach meas periods)/ 14.6666 = 1117
 */
#define FAN_MAX_TACH_COUNT      1117        
#define FAN_TACH_FAIL_COUNT     (FAN_MAX_TACH_COUNT + FAN_MAX_TACH_COUNT/20)  //If we are trying to use FAN_MAX_TACH_COUNT to detect failures, let's add five percent

#define  FAN_GEN_CONFIG_ADDRESS           0x0                                               ///< Address of the general configuration register
#define  GC_WATCHDOG_STATUS               (fanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  0}      ///< Used to enable watchdog status
#define  GC_WATCHDOG_PERIOD               (fanReg_t){ FAN_GEN_CONFIG_ADDRESS,   2,  1}      ///< Sets the watchdog period
#define  GC_OSC_SELECTION                 (fanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  3}      ///< Sets the use of an internal or external oscilator
#define  GC_I2C_BUS_TIMEOUT_DIS           (fanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  5}      ///< Enables a i2c bus timeout
#define  GC_RESET_REGISTERS               (fanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  6}      ///< Resets the registers
#define  GC_STANDY_ENABLE                 (fanReg_t){ FAN_GEN_CONFIG_ADDRESS,   1,  7}      ///< Enables standby

#define  FAN_PWM_FREQUENCY_ADDRESS        0x1                                               ///< Address of PWM frequency address register
#define  FAN_PWM_FREQ_25KHZ               0xB                                               ///< Value to set the PWM frequency to 25 kHz
#define  PWM_FREQUENCY_1_THRU_3           (fanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   4,  0}   ///< PWM frequencies for fan's 1-3
#define  PWM_FREQUENCY_4_THRU_6           (fanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   4,  4}   ///< PWM frequencies for fan's 4-6


//The following register is repeated 6 times, one for each possible fan
#define  FAN_CONFIG_ADDRESSS              0x2                                               ///< Fan configuration address  (first of 6 consecutive registers)
#define  FAN_CONF_PWM_TACH_IN_EN          (fanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   1,  0}   ///< Bit changes the PWM pin for fan to a tach input.
#define  FAN_CONF_LOCKED_ROTOR_POL        (fanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   1,  1}   ///< Bit sets the expected polarity of the tach when the fan is locked and can't rotate
#define  FAN_CONF_TACH_IN_LOCK_ROT_EN     (fanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   1,  2}   ///< Bit puts the tach in locked rotor mode
#define  FAN_CONF_TACH_INPUT_EN           (fanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   1,  3}   ///< Enables the tach input
#define  FAN_CONF_MONITOR_ONLY_EN         (fanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   1,  4}   ///< Puts the fan in monitor only mode
#define  FAN_CONF_SPINUP_TIME             (fanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   2,  5}   ///< Options for fan spin up
#define  FAN_CONF_RPM_MODE_EN             (fanReg_t){ FAN_PWM_FREQUENCY_ADDRESS,   1,  7}   ///< Selects RPM mode

//The following register is repeated 6 times, one for each possible fan
#define  FAN_DYNAMICS_ADDRESSS            0x8                                               ///< The fan dynamics register  (first of 6 consecutive registers)
#define  FAN_DYN_ASYM_RATE_OF_CHG_EN      (fanReg_t){ FAN_DYNAMICS_ADDRESSS,   1,  1}       ///< Enables asymetric speed changes
#define  FAN_DYN_PWM_RATE_OF_CHANGE       (fanReg_t){ FAN_DYNAMICS_ADDRESSS,   3,  2}       ///< Sets the rate of change for fan speed changes
#define  FAN_DYN_SPEED_RANGE              (fanReg_t){ FAN_DYNAMICS_ADDRESSS,   3,  5}       ///< Determines the speed range of the tach

#define  FAN_FAULT_ADDRESS                0x11                                              ///< The FAN fault register
#define  FAN_FAULT_MASK(num)              (1 << num)                                        ///< Generates the bit returned from #CheckFanFailures that would indicate this fan has failed.

#define  FAN_FAILED_ADDRESS               0x14                                              ///< The fan failure options address
#define  FAN_FAILED_QUEUE                 (fanReg_t){ FAN_FAILED_ADDRESS,  2,  0}           ///< The number of failures to detect before setting a fan fail
#define  FAN_FAILED_OPTIONS               (fanReg_t){ FAN_FAILED_ADDRESS,  2,  2}           ///< Options on what to do during a fan faulure
#define  FAN_FAILED_START_DELAY           (fanReg_t){ FAN_FAILED_ADDRESS,  2,  2}           ///< Sets a delay between sequential fan starts


//The folowing pair of registers is repeated 12 times because we can have up to 12 tachometers
#define  FAN_TACH_COUNT_HIGH_ADDRESS       0x18                                             ///< Register contains the upper 8 bits of the tach count (first pair of 6 consecutive register pairs)
#define  FAN_TACH_COUNT_LOW_ADDRESS        0x19                                             ///< Register contains the lower 3 bits of the tach count

//The following pair of registers is repeated 6 times, one for each possible fan
#define  FAN_PWM_DUTY_CYCLE_HIGH_ADDRESS  0x30                                              ///< Register contains the upper 8 bits of the pwm duty cycle (first pair of 6 consecutive register pairs)
#define  FAN_PWM_DUTY_CYCLE_LOW_ADDRESS   0x31                                              ///< Register contains the lower 1 bit of the pwm duty cycle 

//The following pair of registers is repeated 6 times, one for each possible fan
#define  FAN_TARGET_DUTY_CYCLE_HIGH_ADDR  0x40                                              ///< Register contains the upper 8 bits of the target duty cycle (first pair of 6 consecutive register pairs)
#define  FAN_TARGET_DUTY_CYCLE_LOW_ADDR   0x41                                              ///< Register contains the lower 1 bit of the target duty cycle 
#define  FAN_TARGET_DUTY_CYCLE_HIGH(val)  (((val << 7)  & 0xff00) >> 8)                     ///< Gets the high bits for the target duty cycle
#define  FAN_TARGET_DUTY_CYCLE_LOW(val)   ((val << 7)  & 0xff)                              ///< Gets the low bit for the target dut cycle

//The following pair of registers is repeated 6 times, one for each possible fan
#define  FAN_TARGET_COUNT_HIGH_ADDRESS    0x50                                              ///< Register contains the upper 8 bits of the target count (first pair of 6 consecutive register pairs)
#define  FAN_TARGET_COUNT_LOW_ADDRESS     0x51                                              ///< Register contains the lower 3 bits of the target count
#define  FAN_TARGET_COUNT_HIGH(val)       (((val << 5)  & 0xff00) >> 8)                     ///< Gets the high bits for the target count
#define  FAN_TARGET_COUNT_LOW(val)        ((val << 5)  & 0xff)                              ///< Gets the low bits for the target count

/**
 * @brief Contains all the information necessary to update a fan register.
 */
typedef struct _fanReg_t
{
    uint8_t addr;           ///< The address of the register
    uint8_t size;           ///< The size of the field in the register to update in bits
    uint8_t offset;         ///< The offset of the field in the register to update in bit
} fanReg_t;

/**
 * @brief enum defines the allowed watchdog periods.
 */ 
typedef enum _watchDogPeriods_t
{
    NO_WATCHDOG         = 0,   ///< No watchdog is enabled 
    WATCHDOG_5_SECONDS  = 1,   ///< An i2c message must be received within 5 seconds or the fans will turn on full
    WATCHDOG_10_SECONDS = 2,   ///< An i2c message must be received within 10 seconds or the fans will turn on full
    WATCHDOG_30_SECONDS = 3,   ///< An i2c message must be received within 30 seconds or the fans will turn on full
} watchDogPeriods_t;
   
/**
 * @brief The spead range is the number tachometer pulses where the 8192Hz is counted.
 */
typedef enum _speedRange_t
{
    COUNT_BETWEEN_1_TACH_PERIOD     = 0,   ///< Count 8192 Hz ticks over 1 tach period
    COUNT_BETWEEN_2_TACH_PERIODS    = 1,   ///< Count 8192 Hz ticks over 2 tach periods
    COUNT_BETWEEN_4_TACH_PERIODS    = 2,   ///< Count 8192 Hz ticks over 4 tach periods
    COUNT_BETWEEN_8_TACH_PERIODS    = 3,   ///< Count 8192 Hz ticks over 8 tach periods
    COUNT_BETWEEN_16_TACH_PERIODS   = 4,   ///< Count 8192 Hz ticks over 16 tach periods
    COUNT_BETWEEN_32_TACH_PERIODS   = 5,   ///< Count 8192 Hz ticks over 32 tach periods
} speedRange_t;

/**
 * @brief Transition timing settings
 */
typedef enum _pwrRateOfChange
{
    PWM_RATE_OF_CHANGE_0_33   = 0,  ///< Cause changes from 33% to 100% Max RPM in 0.33s
    PWM_RATE_OF_CHANGE_0_67   = 1,  ///< Cause changes from 33% to 100% Max RPM in 0.67s
    PWM_RATE_OF_CHANGE_1_34   = 2,  ///< Cause changes from 33% to 100% Max RPM in 1.34s
    PWM_RATE_OF_CHANGE_2_70   = 3,  ///< Cause changes from 33% to 100% Max RPM in 2.70s 
    PWM_RATE_OF_CHANGE_5_30   = 4,  ///< Cause changes from 33% to 100% Max RPM in 5.30s
    PWM_RATE_OF_CHANGE_10_7   = 5,  ///< Cause changes from 33% to 100% Max RPM in 10.7s
    PWM_RATE_OF_CHANGE_21_7   = 6,  ///< Cause changes from 33% to 100% Max RPM in 21.7s
    PWM_RATE_OF_CHANGE_42_8   = 7   ///< Cause changes from 33% to 100% Max RPM in 42.8s
} pwrRateOfChange;

/**
 * @brief Spin up time settings
 */
typedef enum _spinUp_t 
{
    SPIN_UP_OFF  = 0,    ///< Disable spin up
    SPIN_UP_0_5  = 1,    ///< Max spin up time is 0.5s
    SPIN_UP_1_0  = 2,    ///< Max spin up time is 1.0s
    SPIN_UP_2_0  = 3     ///< Max spin up time is 2.0s
} spinUp_t;

/**
 * @brief A structure used to map UVP temperature to fan speed.
 */    
typedef struct _mapTempToSpeed_t
{
    float temp;             ///< The average UVP temperature for an AMB
    uint8_t speed;          ///< The speed (in percent) to set the fans to for temps exceeding the temperature in this structure (If set to 0xff, temp represent a critical temperature)
} mapTempToSpeed_t;


static I2CMultiplexerInfo_t gCcbFanInfo;                    ///< The i2c multiplexer structure for the fan driver chip
static TaskHandle_t gFanDriverTaskHandle = NULL;            ///< The fan driver task handle
static ambMask_t gFansOkMask;                               ///< A mask indication which AMB's fan is ok
static SemaphoreHandle_t gFanDriverMutex;                   ///< Semaphore used to make this driver thread safe
static StaticSemaphore_t gFanDriverMutexBuff;               ///< The buffer holding the information for the #gFanDriverMutex semaphore

static ambNum_t gFanMap[FAN_MAX_NUM] = {0, 1, 2, 3};        ///< List out which ambNum corresponds to which fan  @todo we need to adjust this array when we know how these actually map out.

/**
 * @brief This array maps fan speed to UVP temperature
 * 
 * @todo  This structure needs to be refined experimentally with the CCB and AMB's
 */
static mapTempToSpeed_t mapTempToSpeed[] = {
    {-273.0,                            0 },
    {  20.0,                            20},
    {  30.0,                            60},
    {  50.0,                            100},
    {  CRITICAL_UVP_TEMPERATURE,        FAN_CRITICAL_TEMP_SPEED}
};
  
const uint8_t tempSpeedMapSize = sizeof(mapTempToSpeed)/sizeof(mapTempToSpeed[0]);  ///< Number of entries in the temperature speed map

/**
 * @brief Retrieves a mask indicating which AMB's fans are ok.  If 
 *        an AMB's fan is not ok, charging should not be done with that AMB.
 *        The mask will be updated by the driver every 25 seconds.
 *
 * @return A mask of type #ambMask_t indicating which AMB's have fans that are ok
 */
ambMask_t FansOk(void)
{
    ambMask_t fanMask;
    xSemaphoreTake( gFanDriverMutex, portMAX_DELAY );    
    fanMask =  gFansOkMask;
    xSemaphoreGive( gFanDriverMutex);
    return fanMask;
}

/**
 *  @brief Used to create a field in a register that can be OR'd in
 *  
 *  @param fanReg Contains the information about the register and its field
 *  @param val    The value to place in the field
 * 
 *  @return The value stuck in the appropiate field of the register, ready to be OR'd
 */
static uint8_t createFanRegField(fanReg_t fanReg, uint8_t val)
{
    uint8_t mask = GEN_SOLID_MASK(fanReg.size);

    return (mask & val) << fanReg.offset;  
}

/**
 * @brief  Writes an 8 bit register to the fan driver
 *         To write a register, we simply write the register addresss and follow it with the value.
 * @param  regAddr  The register address to write to
 * @param  val      The value to write to the register
 * 
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t
 */
static cotaError_t writeFanRegister(uint8_t regAddr, uint8_t val)
{
    uint8_t sendData[2];
    sendData[0] = regAddr;
    sendData[1] = val;
    
    return I2CMultiplexWrite(&gCcbFanInfo, sendData, sizeof(sendData), FAN_READ_WRITE_TIMEOUT);  
}

/**
 * @brief  Read an 8 bit register to the fan driver
 *         To read a register, we first write the register address,
 *         then follow it with a read.

 * @param  regAddr  The register address to read from
 * @param  val      Receives value read from the register
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t
 */
static cotaError_t readFanRegister(uint8_t regAddr, uint8_t* val)
{
    cotaError_t ret;
    
    ret = I2CMultiplexWrite(&gCcbFanInfo, &regAddr, sizeof(regAddr), FAN_READ_WRITE_TIMEOUT);
    
    ret = (ret == COTA_ERROR_NONE) ? I2CMultiplexRead(&gCcbFanInfo, val, sizeof(uint8_t), FAN_READ_WRITE_TIMEOUT) : ret; 
    
    return ret;
}

/**
 * @brief This function will modify a field in a register on the fan control chip
 *        It will first read the chip's register value, change the field to
 *        the correct value, and then write the register back.
 *
 * @param fanReg A structure of type #fanReg_t that describes the register and it's field 
 * @param val    The value to set the field to. 

 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t
 */
static cotaError_t writeFanRegField(fanReg_t fanReg, uint8_t val)
{
    uint8_t mask = GEN_SOLID_MASK(fanReg.size) << fanReg.offset;
    uint8_t regVal;
    cotaError_t ret;
    
    val <<= fanReg.offset;   
    val &= mask;
    
    ret = readFanRegister(fanReg.addr, &regVal);
    
    regVal &= (~mask);
    regVal |= val;
    
    return (ret == COTA_ERROR_NONE) ? writeFanRegister(fanReg.addr, regVal) : ret;
}

/**
 * @brief The target count register controls the fan in RPM mode, or 
 *        detects faults in PWM mode.  This function will set it for a fan
 *
 * @param fanNum      The number of the fan to set this for. Select 0-3
 * @param targetCount The 11 bit target count value 
 * 
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t
 */
static cotaError_t setFanTachTargetCount(uint8_t fanNum, uint16_t targetCount)
{
    cotaError_t ret = COTA_ERROR_NONE;

    ret = writeFanRegister(FAN_TARGET_COUNT_HIGH_ADDRESS+2*fanNum, FAN_TARGET_COUNT_HIGH(targetCount));
    ret = writeFanRegister(FAN_TARGET_COUNT_LOW_ADDRESS+2*fanNum, FAN_TARGET_COUNT_LOW(targetCount));

    return ret;
}

/**
 *  @brief Sets the target tachometer count to a value calculated
 *         as a percentage of the maximum RPM.
 *         The lowest value is 20%.  Setting the value to 1-19% will
 *         cause the value to be set to 20%.  Setting the value
 *         to zero turns the fan off.
 *         
 *                    60 * SR * 8192
 *      tach count  = --------------
 *                    NP * RPM 
 *
 *          NP = 2 - the number of tachs per revolution
 *          SR = 2 - The number of tachs to count 8192 cycles over
 *          RPM = desired RPM = (((100 - percent)/100) * 2200 rpm) 
 *
 * @param fanNum   The number of the fan to set this for.  Select 0-3
 * @param percent  0% is off, 20% is minimum RPM, 100% is max RPM
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t 
 */
static cotaError_t setFanSpeed(uint8_t fanNum, uint8_t percent)
{
    uint32_t numerator;
    uint32_t denominator;
    uint32_t count;
 
    if ((percent < FAN_MIN_RPM_PERCENT) && (percent > 0))
    {
        percent = FAN_MIN_RPM_PERCENT;
    }
    
    numerator = FAN_SECS_IN_MIN * 2 * FAN_TACH_CLOCK_HZ * 100;
    denominator = (100 - percent) * 2 * FAN_MAX_RPM;
    count = numerator/denominator;
 
    if (percent == 0)
    {
        count +=1;  //Let's add 1 to overcome error from interger arithmetic and make sure the fan turns off.
    }
    
    return setFanTachTargetCount(fanNum, count);  
}

/**
 * @brief Changes all the fans to RPM mode 
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t 
 */
static cotaError_t chgFansToRpmMode(void)
{
    fanReg_t fanReg = FAN_CONF_RPM_MODE_EN;
    uint8_t ii;
    cotaError_t ret = COTA_ERROR_NONE;
  
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = writeFanRegField(fanReg, FAN_REG_SET_BIT);
        fanReg.addr++;
    }   
  
    return ret;
}


/**
 * @brief Fans can be set to full on by setting a GPIO in case there is an issue with I2C
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t 
 */
static void setEmergencyFanOn(bool fullFanOn)
{ 
    HAL_GPIO_WritePin(FAN_FULL_GPIO_Port, FAN_FULL_Pin, fullFanOn ? FAN_FULL_SPEED_ON : FAN_FULL_SPEED_OFF); 
}


/**
 * @brief Initializes the fan controller for operation.
 *
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t 
 */
static cotaError_t initFan(void)
{
    uint8_t regVal;
    uint16_t regVal16;
    uint8_t ii;
    cotaError_t ret = InitI2CMultiplexer(&gCcbFanInfo, FAN_I2C_ADDRESS, FAN_MULTIPLEX_NUM, FAN_MULTIPLEX_CHAN);
    
    //Make sure the full fan gpio is set high
    setEmergencyFanOn(false);
    
    //Let's set the general configuration register
    regVal = createFanRegField(GC_WATCHDOG_PERIOD,WATCHDOG_30_SECONDS) |       ///< If no I2C traffic detected in 30 seconds, the watchdog turns on all fans to the max.
             createFanRegField(GC_I2C_BUS_TIMEOUT_DIS, FAN_REG_SET_BIT);       ///< Disable I2C reset
    
    ret = (ret == COTA_ERROR_NONE) ? writeFanRegister(FAN_GEN_CONFIG_ADDRESS, regVal) : ret;
    
    //Set the PWM's to 25kHz
    regVal = createFanRegField(PWM_FREQUENCY_1_THRU_3, FAN_PWM_FREQ_25KHZ) |
             createFanRegField(PWM_FREQUENCY_4_THRU_6, FAN_PWM_FREQ_25KHZ);
    
    ret = (ret == COTA_ERROR_NONE) ? writeFanRegister(FAN_GEN_CONFIG_ADDRESS, regVal) : ret;
    
    //We choose to use 2 tach periods (1 rotations) to count because we want to make the best use of 
    //the 11 bit tach count regiseter.   The maximum value occurs at 20% fan speed 
    //which is the lowest supported fan speed.  At 2200 rpm, the maximum fan speed,
    //the tach count should be 223 counts.  At 20% rpm, it should 1117. 
    regVal = createFanRegField(FAN_DYN_SPEED_RANGE, COUNT_BETWEEN_2_TACH_PERIODS) |
             createFanRegField(FAN_DYN_PWM_RATE_OF_CHANGE, PWM_RATE_OF_CHANGE_2_70);   //Set rate of change from 33% to 100% to 2.7s
    
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = writeFanRegister(FAN_DYNAMICS_ADDRESSS+ii, regVal);   
    }
    
    //We now need to set the the maximum tach count.  If the count is exceeded, fan faults will be generated.
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = setFanTachTargetCount(ii, FAN_TACH_FAIL_COUNT);
    }    
    
    //Now we set the fan configuration registers for each fan. Most settings are set to default
    //expept for spin up time and tach input enable
    regVal = createFanRegField(FAN_CONF_RPM_MODE_EN, FAN_REG_RESET_BIT) |           //Let's stay in PWM mode for now, we will switch to RPM mode later in initialization
             createFanRegField(FAN_CONF_SPINUP_TIME, SPIN_UP_0_5) |                 //Choose a maximum of 0.5 second spin up time.
             createFanRegField(FAN_CONF_MONITOR_ONLY_EN, FAN_REG_RESET_BIT) |       //Let's leave the fan in control mode
             createFanRegField(FAN_CONF_TACH_INPUT_EN, FAN_REG_SET_BIT) |           //Let's enable the tachometer input so we can control the fan
             createFanRegField(FAN_CONF_TACH_IN_LOCK_ROT_EN, FAN_REG_RESET_BIT) |   //Let's not lock fan rotation
             createFanRegField(FAN_CONF_LOCKED_ROTOR_POL, FAN_REG_RESET_BIT) |      //Fan tach will go low when stopped.
             createFanRegField(FAN_CONF_PWM_TACH_IN_EN, FAN_REG_RESET_BIT);         //Do not configure the fan output as tach input.
    
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = writeFanRegister(FAN_CONFIG_ADDRESSS+ii, regVal);   
    }
    
    //Hopefully, the fans are already going, but's let's start them at the lowest setting of 20% to keep the sound under control
    regVal16 = FAN_LOW_TARGET_PWM;
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = writeFanRegister(FAN_TARGET_DUTY_CYCLE_HIGH_ADDR + 2*ii, FAN_TARGET_DUTY_CYCLE_HIGH(regVal16));
        ret = writeFanRegister(FAN_TARGET_DUTY_CYCLE_LOW_ADDR + 2*ii, FAN_TARGET_DUTY_CYCLE_HIGH(regVal16));
    }
       
    //Once we are setup in the PWM mode, which we now are, we can switch to RPM mode.
    //In RPM mode, we don't control the fan by setting the PWM duty cycle, we set the target 
    //tachometer count and the driver will match the rpm's to produce that tachometer count
    for (ii = 0; (ii < FAN_MAX_NUM) && (ret == COTA_ERROR_NONE); ii++)
    {
        ret = setFanSpeed(ii, FAN_MIN_RPM_PERCENT);
    }    
    
    ret = chgFansToRpmMode();
    
    if (ret != COTA_ERROR_NONE)
    {
        setEmergencyFanOn(true);  //Turn on fans full if there is an error
    }
    
    return ret;
}

/**
 * @brief Retrieves the fan fault register so fan faults can be detected.
 *        
 * @note  Fan fault bits are cleared by setting the fan's speed
 * 
 * @param reg pointer to a #uint8_t to retrieve the fan fault register 
 */
static cotaError_t checkFanFailures(uint8_t* reg)
{
    return readFanRegister(FAN_FAULT_ADDRESS, reg);
}
 


/**
 * @brief Updates the gFansOkMask which is read by the main thread. Also updates
 *        fan speed based on UVP temperature.
 * 
 * @return COTA_ERROR_NONE on success; otherwise an error code of type #cotaError_t 
 */
static cotaError_t updateFansOkMaskAndSpeed(void)
{
    cotaError_t err = COTA_ERROR_NONE;
    uint8_t fanFault = 0;
    uint8_t ii;
    uint8_t jj;
    uint8_t speed;
    ambMask_t workMask = 0;
    int16_t temp;
    int16_t uvpAvgTemps[MAX_NUM_AMB];
   
    err = checkFanFailures(&fanFault);

    if (err == COTA_ERROR_NONE)
    {
        //loops through fans and check for failures
        for (ii = 0; ii < FAN_MAX_NUM; ii++)
        {     
            if (!BIT_IN_MASK(gFanMap[ii], VALID_AMBS))
            {
                continue;
            }

            if (!(fanFault & FAN_FAULT_MASK(ii)))
            {
                workMask |= NUM_TO_MASK(gFanMap[ii]);
            }       
        }
    }
   
    //Loops through ccb temps and checks if the cititical temperature has been exceeded
    for (ii = 0; (ii < TEMP_CCB_NUM_OF_THERM) && (err == COTA_ERROR_NONE); ii++)
    {
        err = ReadCCBTemperature(ii, &temp);
        if ((err == COTA_ERROR_NONE) && (temp > CELSIUS_TO_CENTICELSIUS(CRITICAL_CCB_TEMPERATURE)))
        {
            err = COTA_ERROR_OVER_CRITICAL_TEMPERATURE;
        }
    }  
   
    if (err == COTA_ERROR_NONE)
    {
        //Let's get the UVP temperatures
        err =  GetAverageUvpTempsFromCache(uvpAvgTemps, MAX_NUM_AMB);

        if (err == COTA_ERROR_NONE)
        {
            //Let's loop over the fans and adjust the speed accordingly
            for (ii = 0; (ii < FAN_MAX_NUM) && (err == COTA_ERROR_NONE); ii++)
            {     
                if (!BIT_IN_MASK(gFanMap[ii], VALID_AMBS))
                {
                    continue;
                }
                //Loop through temp speed map to get correct speed
                for (jj = 0; jj < tempSpeedMapSize; jj++)
                {
                    if (uvpAvgTemps[gFanMap[ii]] > mapTempToSpeed[jj].temp)
                    {
                        speed = mapTempToSpeed[jj].speed;
                    }
                }
                
                if (speed == FAN_CRITICAL_TEMP_SPEED)
                {
                    err = COTA_ERROR_OVER_CRITICAL_TEMPERATURE;
                }
                else
                {
                    err = setFanSpeed(ii, speed);                  
                }
            }
        }
    }

    // If there is an error, shit has hit the fans. Set gFansOkMask to zero
    // so the control task knows to stop charging.  Also turn the fans on full
    // with a gpio.
    if (err != COTA_ERROR_NONE)
    {
        workMask = 0;
        setEmergencyFanOn(true);  //Turn on fans full if there is an error
    }
   
   xSemaphoreTake( gFanDriverMutex, portMAX_DELAY );    
   gFansOkMask = workMask;
   xSemaphoreGive( gFanDriverMutex);
   
   return POST_COTA_ERROR(err);
}


/**
 * @brief A task to monitor the fans, report errors, and mask speed changes based 
 *        on temerature.
 * @param pvParameters not used
 */
static void fanDriverTask(void *pvParameters)
{
    cotaError_t err = initFan();
    
    for (;;)
    {
        vTaskDelay(pdMS_TO_TICKS(FAN_TASK_WAIT_INTERVAL));
        if (err != COTA_ERROR_NONE)
        {
            err = initFan();
        }
        else
        {
            err = updateFansOkMaskAndSpeed(); 
        }
    }
}

/**
 * @brief Intitialize the fan driver task
 */
void InitializeFanDriver(void)
{
    BaseType_t xReturned;
    
    gFanDriverMutex = xSemaphoreCreateMutexStatic(&gFanDriverMutexBuff); 
    while (gFanDriverMutex == NULL);
    xSemaphoreGive( gFanDriverMutex );
    
    xReturned = xTaskCreate(
                  fanDriverTask,                  /* Function that implements the task. */
                  "Monitors the fans",            /* Text name for the task. */
                  FAN_DRIVER_TASK_STACK_SIZE,     /* Stack size in bytes. */
                  NULL,                           /* Parameter passed into the task. */
                  FAN_DRIVER_TASK_PRIORITY,       /* Priority at which the task is created. */
                  &gFanDriverTaskHandle );    /* Used to pass out the created task's handle. */
  
    while (xReturned != pdPASS);
}


