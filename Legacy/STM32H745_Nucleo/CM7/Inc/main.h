/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32h7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PROXY_SCK_Pin GPIO_PIN_2
#define PROXY_SCK_GPIO_Port GPIOE
#define SPI_DISABLE_0_Pin GPIO_PIN_3
#define SPI_DISABLE_0_GPIO_Port GPIOE
#define PROXY_MISO_Pin GPIO_PIN_5
#define PROXY_MISO_GPIO_Port GPIOE
#define TIM15_PWM_OUTPUT_Pin GPIO_PIN_6
#define TIM15_PWM_OUTPUT_GPIO_Port GPIOE
#define USER_BUTTON_Pin GPIO_PIN_13
#define USER_BUTTON_GPIO_Port GPIOC
#define USER_BUTTON_EXTI_IRQn EXTI15_10_IRQn
#define OSC32_IN_Pin GPIO_PIN_14
#define OSC32_IN_GPIO_Port GPIOC
#define OSC32_OUT_Pin GPIO_PIN_15
#define OSC32_OUT_GPIO_Port GPIOC
#define LED0_Pin GPIO_PIN_6
#define LED0_GPIO_Port GPIOF
#define LED4_Pin GPIO_PIN_7
#define LED4_GPIO_Port GPIOF
#define SPI_DISABLE_1_Pin GPIO_PIN_8
#define SPI_DISABLE_1_GPIO_Port GPIOF
#define OSC_IN_Pin GPIO_PIN_0
#define OSC_IN_GPIO_Port GPIOH
#define OSC_OUT_Pin GPIO_PIN_1
#define OSC_OUT_GPIO_Port GPIOH
#define AMB_SINGLE_Pin GPIO_PIN_0
#define AMB_SINGLE_GPIO_Port GPIOC
#define WRMHL_MISO_Pin GPIO_PIN_2
#define WRMHL_MISO_GPIO_Port GPIOC
#define WRMHL_MOSI_Pin GPIO_PIN_3
#define WRMHL_MOSI_GPIO_Port GPIOC
#define LED1_Pin GPIO_PIN_0
#define LED1_GPIO_Port GPIOB
#define WRMHL_CSB_Pin GPIO_PIN_15
#define WRMHL_CSB_GPIO_Port GPIOF
#define DBG3_Pin GPIO_PIN_7
#define DBG3_GPIO_Port GPIOE
#define TPS_TX_Pin GPIO_PIN_8
#define TPS_TX_GPIO_Port GPIOE
#define PROXY_MR_Pin GPIO_PIN_9
#define PROXY_MR_GPIO_Port GPIOE
#define PROXY_SR_Pin GPIO_PIN_10
#define PROXY_SR_GPIO_Port GPIOE
#define PROXY_SR_EXTI_IRQn EXTI15_10_IRQn
#define PRX_SR_TEST_Pin GPIO_PIN_11
#define PRX_SR_TEST_GPIO_Port GPIOE
#define PRX_MR_TEST_Pin GPIO_PIN_12
#define PRX_MR_TEST_GPIO_Port GPIOE
#define SPI_DISABLE_3_Pin GPIO_PIN_13
#define SPI_DISABLE_3_GPIO_Port GPIOE
#define PROXY_MOSI_Pin GPIO_PIN_14
#define PROXY_MOSI_GPIO_Port GPIOE
#define WRMHL_SCLK_Pin GPIO_PIN_10
#define WRMHL_SCLK_GPIO_Port GPIOB
#define DBG4_Pin GPIO_PIN_11
#define DBG4_GPIO_Port GPIOB
#define RSSI_VOTING_DONE_Pin GPIO_PIN_12
#define RSSI_VOTING_DONE_GPIO_Port GPIOB
#define LED3_Pin GPIO_PIN_14
#define LED3_GPIO_Port GPIOB
#define VCP_TX_Pin GPIO_PIN_8
#define VCP_TX_GPIO_Port GPIOD
#define VCP_RX_Pin GPIO_PIN_9
#define VCP_RX_GPIO_Port GPIOD
#define PROXY_GO_TEST_Pin GPIO_PIN_10
#define PROXY_GO_TEST_GPIO_Port GPIOD
#define DBG1_Pin GPIO_PIN_12
#define DBG1_GPIO_Port GPIOD
#define DBG2_Pin GPIO_PIN_13
#define DBG2_GPIO_Port GPIOD
#define TX_CTRL_Pin GPIO_PIN_15
#define TX_CTRL_GPIO_Port GPIOD
#define RX_CTRL_Pin GPIO_PIN_6
#define RX_CTRL_GPIO_Port GPIOG
#define USB_OTG_FS_OVCR_Pin GPIO_PIN_7
#define USB_OTG_FS_OVCR_GPIO_Port GPIOG
#define FAN_FULL_Pin GPIO_PIN_8
#define FAN_FULL_GPIO_Port GPIOG
#define PD_DONE_0_Pin GPIO_PIN_6
#define PD_DONE_0_GPIO_Port GPIOC
#define PD_DONE_1_Pin GPIO_PIN_7
#define PD_DONE_1_GPIO_Port GPIOC
#define PD_DONE_2_Pin GPIO_PIN_8
#define PD_DONE_2_GPIO_Port GPIOC
#define PD_DONE_3_Pin GPIO_PIN_9
#define PD_DONE_3_GPIO_Port GPIOC
#define TPS_RX_Pin GPIO_PIN_8
#define TPS_RX_GPIO_Port GPIOA
#define PROXY_GO_IN_Pin GPIO_PIN_15
#define PROXY_GO_IN_GPIO_Port GPIOA
#define SPI_DISABLE_2_Pin GPIO_PIN_10
#define SPI_DISABLE_2_GPIO_Port GPIOC
#define UVP_EN_0_Pin GPIO_PIN_11
#define UVP_EN_0_GPIO_Port GPIOC
#define UVP_EN_1_Pin GPIO_PIN_12
#define UVP_EN_1_GPIO_Port GPIOC
#define UVP_EN_2_Pin GPIO_PIN_0
#define UVP_EN_2_GPIO_Port GPIOD
#define UVP_EN_3_Pin GPIO_PIN_1
#define UVP_EN_3_GPIO_Port GPIOD
#define PU_0_Pin GPIO_PIN_2
#define PU_0_GPIO_Port GPIOD
#define PU_1_Pin GPIO_PIN_3
#define PU_1_GPIO_Port GPIOD
#define PU_2_Pin GPIO_PIN_4
#define PU_2_GPIO_Port GPIOD
#define PU_3_Pin GPIO_PIN_5
#define PU_3_GPIO_Port GPIOD
#define CSB_ADD0_Pin GPIO_PIN_6
#define CSB_ADD0_GPIO_Port GPIOD
#define CSB_ADD1_Pin GPIO_PIN_7
#define CSB_ADD1_GPIO_Port GPIOD
#define CSB_ADD2_Pin GPIO_PIN_9
#define CSB_ADD2_GPIO_Port GPIOG
#define CSB_ADD3_Pin GPIO_PIN_10
#define CSB_ADD3_GPIO_Port GPIOG
#define AMB_EN_0_Pin GPIO_PIN_4
#define AMB_EN_0_GPIO_Port GPIOB
#define AMB_EN_1_Pin GPIO_PIN_5
#define AMB_EN_1_GPIO_Port GPIOB
#define AMB_EN_2_Pin GPIO_PIN_6
#define AMB_EN_2_GPIO_Port GPIOB
#define AMB_EN_3_Pin GPIO_PIN_7
#define AMB_EN_3_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_1
#define LED2_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
