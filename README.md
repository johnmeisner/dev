# Orion

Welcome to the Orion Transmitter codebase. 

# Repository Layout

## HAL Code

Most of the code is built on ST's HAL. As a result, be careful when
modifying code that it automatically generated. The auto-generated
code can be found in...

`./CubeMx745/CM7/Core`

This is also where `Main` is located. Ossia has some code in this
directory, including task initalizaition, and post-boot logic. 

The driver modules can be found in the following directory

`CubeMx745/Drivers`

## Ossia Code 

Ossia's source code can be found in...

`./CubeMx745/CM7/Src`

Where `Applications/` is reserved for higher level modules, and
`Drivers/` consists of lower-level modules that control the
hardware. In general, this location is referred to as the
'source directory'.

## State Machine Code

In the Orion system there is a task/thread which calls the Control
Task (`ctrl_task.c`). This is the task that controls each action of the
Orion transmitter. Such actions include deciding when to send messages
to receivers, when to calibrate the system, and when to transmit
power.

The software implementation can vary greatly given the design, and the
control task has been written in such a way that state machine that
governs transmitter behavior can be exchanged easily.

### SM Files

Within `Applications/` of the source directory, there are a series of
directories which contain the state machine code. These files are
conditionally compiled given the contents of `config_system.h` in the
`Applications/` directory. When a `configure_x_build.bat` where 'x'
is the implementation in question, `config_system.h` will be populated
with the necessary symbols.

#### Common

`Common` is all the states that are common to most implmentations and
are unlikely to see significant change. These states could be
overwritten at a later point if an implementation requires a complete
overhall. The common folder does not have an implementation of its
own; it is simply used by the other configurations of Orion.

#### Demo

Demo is all the states required for a Demo system. This is the system
that is inherited from the Venus implementation. To configure this
build, run `config_demo_build.bat`

#### Xirgo Forever Tracker/CotaBox

The forever tracker implementation is found in the `ForeverTracker`
folder. This build is specific to the CotaBox implementation. To
configure this build, run `config_forever_tracker_build.bat`

### Compilation

This project only supports IAR 8.40.1 compiler found with the
associated IAR Embedded Workbench. You can use the workbench to
compile the project, or run the following command from the command
line.

`IarBuild.exe CubeMx745/EWARM/CubeMx745.ewp -make CubeMx745_CM7 -log warnings`


# Copyright 

THIS PROGRAM IS THE CONFIDENTIAL AND PROPRIETARY PRODUCT OF OSSIA INC.
ANY UNAUTHORIZED USE, REPRODUCTION OR TRANSFER OF THIS PROGRAM IS
STRICTLY PROHIBITED.  COPYRIGHT 2020 OSSIA INC. (SUBJECT TO LIMITED
DISTRIBUTION AND RESTRICTED DISCLOSURE ONLY.) ALL RIGHTS RESERVED.
